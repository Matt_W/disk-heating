#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 23 16:36:50 2021

@author: matt
"""
from functools import lru_cache
# from functools import cache

import os

import numpy as np

import h5py as h5

from scipy.optimize import brentq
from scipy.optimize import minimize
from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.interpolate import interp1d
# from scipy.interpolate import UnivariateSpline
from scipy.integrate import quad
from scipy.integrate import dblquad
from scipy.signal import savgol_filter
from scipy.signal import convolve
from scipy.stats import binned_statistic
from scipy.stats import binned_statistic_2d
import scipy.stats
from scipy.spatial.transform import Rotation
from scipy.special import erf
from scipy.special import erfinv
from scipy.special import gamma
from scipy.special import gammainc
from scipy.special import kn, iv
from scipy.special import lambertw
from scipy.spatial import distance_matrix

import matplotlib

# default
# matplotlib.use('Qt5Agg')
# stop resizing plots
matplotlib.use('TkAgg')

import matplotlib.pyplot as plt
import matplotlib.cm
from matplotlib import ticker
import matplotlib.patheffects as path_effects

# from   matplotlib.colors import LogNorm
# from matplotlib.cm import viridis
# from matplotlib.cm import ScalarMappable
# from matplotlib.colors import Normalize
# from matplotlib.legend_handler import HandlerTuple
# import matplotlib.lines as lines

import seaborn as sns
from seaborn import desaturate

from sphviewer.tools import QuickView
from sphviewer.tools import Blend

import emcee
import corner

import vegas

import imageio

# import splotch as splt

# import halo_calculations as halo
import load_nbody

from my_shape import find_abc
from my_shape import find_abc_with_outer_bias

import halo_calculations

from matplotlib import rcParams

import twobody

rcParams['ps.useafm'] = True
rcParams['pdf.use14corefonts'] = False
rcParams['text.usetex'] = False
rcParams['font.sans-serif'] = ['cmr10', 'Times-Roman']
rcParams['font.weight'] = 'normal'
rcParams['font.size'] = 20
rcParams['xtick.labelsize'] = 20
rcParams['ytick.labelsize'] = 20
rcParams['axes.labelsize'] = 22
rcParams['axes.linewidth'] = 1
rcParams['axes.unicode_minus'] = False
# rcParams['axes.grid'] = True
rcParams['axes.grid'] = False
rcParams['xtick.minor.width'] = 1
rcParams['ytick.minor.width'] = 1
rcParams['xtick.major.width'] = 1
rcParams['ytick.major.width'] = 1
rcParams['xtick.minor.size'] = 4
rcParams['ytick.minor.size'] = 4
rcParams['xtick.major.size'] = 5
rcParams['ytick.major.size'] = 5
rcParams['xtick.direction'] = 'in'
rcParams['ytick.direction'] = 'in'
rcParams['xtick.top'] = True
rcParams['ytick.right'] = True
rcParams['mathtext.fontset'] = 'cm'

import matplotlib.lines as lines
from matplotlib.legend_handler import HandlerTuple

# GRAV_CONST = 4.301e4  # kpc (km/s)^2 / (10^10Msun) (source ??)
GRAV_CONST = 43007.1 # kpc (km/s)^2 / (10^10Msun) (gadget4)
HUBBLE_CONST = 0.06777  # km/s/kpc (Planck 2018)
GALIC_HUBBLE_CONST = 0.1  # h km/s/kpc
RHO_CRIT = 3 * HUBBLE_CONST ** 2 / (8 * np.pi * GRAV_CONST)  # 10^10Msun/kpc^3

PC_ON_M = 3.08568e16  # pc/m = kpc/km (gadget4)
GYR_ON_S = 3.15576e16  # gyr/s

T_TOT = 9.777929880599285  # Gyr  # 10 * PC_ON_M / GYR_ON_S / CM_ON_S

BURN_IN = 10

like_fail_value = np.inf


def find_least_log_squares_best_fit():
    '''Best fit values in paper
    '''
    return([82.3928 * 2 * np.sqrt(2) * np.pi,
            20.18624 * np.sqrt(2) * np.pi,
            20.17192 * np.sqrt(2) * np.pi,
            9.40073 * np.sqrt(2) * np.pi],
           [-0.47021, -0.30776, -0.18912, -0.11541],
           [0.0, 0.0, 0.0, 0.0],
           [0.0, 0.0, 0.0, 0.0])

    # return([564.3748199899212, 82.64213087100012, 60.984519592054625, 40.12939951094592],
    #        [-0.46052367429549157, -0.3094046972119288, -0.1869320831441747, -0.11884686914195164],
    #        [0.0, 0.0, 0.0, 0.0],
    #        [-0.02929631651853935, -0.012536084958202545, -0.048400086368460965, -0.009475834951981645])


def get_spherical_coords(pos, vel):

    r = np.linalg.norm(pos, axis=1)
    phi = np.arctan2(pos[:, 1], pos[:, 0])
    theta = np.arccos(pos[:, 2] / (r + 1e-6))

    v_r = vel[:, 0] * np.sin(theta) * np.cos(phi) + vel[:, 1] * np.sin(theta) * np.sin(phi) + vel[:, 2] * np.cos(theta)
    v_phi = -vel[:, 0] * np.sin(phi) + vel[:, 1] * np.cos(phi)
    v_theta = vel[:, 0] * np.cos(theta) * np.cos(phi) + vel[:, 1] * np.cos(theta) * np.sin(phi) - vel[:, 2] * np.sin(theta)

    return r, phi, theta, v_r, v_phi, v_theta


def get_cylindrical(pos_stars, vel_stars):
    '''Calculates cylindrical coordinates.
  '''
    rho = np.sqrt(pos_stars[:, 0] ** 2 + pos_stars[:, 1] ** 2)
    phi = np.arctan2(pos_stars[:, 1], pos_stars[:, 0])
    zax = pos_stars[:, 2]

    v_rho = vel_stars[:, 0] * np.cos(phi) + vel_stars[:, 1] * np.sin(phi)
    v_phi = -vel_stars[:, 0] * np.sin(phi) + vel_stars[:, 1] * np.cos(phi)

    # v_rho = (pos_stars[:,0] * vel_stars[:,0] + pos_stars[:,1] * vel_stars[:,1]) / rho
    # v_phi = (pos_stars[:,0] * vel_stars[:,1] - pos_stars[:,1] * vel_stars[:,0]) / rho

    v_z = vel_stars[:, 2]

    return (rho, phi, zax, v_rho, v_phi, v_z)


def get_radii_that_are_interesting(r):
    '''Find stellar quarter, half mass and 3 quarter mass assuming equal mass stars.
  '''
    rs = np.sort(r)

    quarter = rs[int(round(len(rs) / 4))]
    half = rs[int(round(len(rs) / 2))]
    three = rs[int(round(3 * len(rs) / 4))]

    return (quarter, half, three)


def get_dex_bins(bin_centres, dex=0.2):
    '''Finds bin edges from centres that span a given dex.
  '''
    edges = np.zeros(2 * len(bin_centres))

    for i, centre in enumerate(bin_centres):
        edges[2 * i] = centre * 10 ** (-dex / 2)
        edges[2 * i + 1] = centre * 10 ** (dex / 2)

    return (edges)


def get_sigma_N(vs, N, zero=False, super=1):

    # length = (len(vs) - N) // super
    length = len(vs[N//2 : -N//2 : super])
    sigma2 = np.zeros(length)

    #TODO do without for loop
    for i in range(length):
        if zero:
            mean = np.sum(vs[i*super:i*super+N]) / N
            sigma2[i] = np.sum((vs[i*super:i*super+N]-mean)**2) / N
        else:
            sigma2[i] = np.sum(vs[i*super:i*super+N]**2) / N

    return np.sqrt(sigma2)


########################################################################################################################

def get_exponential_spherical_density(R, M_star, R_d):
    rho_0 = M_star / (4 * np.pi * R_d ** 2)
    rho_R = rho_0 * np.exp(-R / R_d) / R
    return rho_R


def get_exponential_surface_density(R, M_star, R_d):
    Sigma_0 = M_star / (2 * np.pi * R_d ** 2)
    Sigma_R = Sigma_0 * np.exp(-R / R_d)
    return Sigma_R


def get_analytic_exponential_mass(r_measure, Mstar, R_d):
    M_r = Mstar * (1 - (r_measure/R_d + 1) * np.exp(-r_measure / R_d))
    return M_r


def get_exponential_disk_velocity(r_measure, Mstar, R_d):
    y = r_measure / (2 * R_d)
    vc = np.sqrt(GRAV_CONST * Mstar * r_measure ** 2 / (2 * R_d ** 3) * (iv(0, y) * kn(0, y) - iv(1, y) * kn(1, y)))
    return vc


def get_exponential_spherical_potential(r_measure, M_star, R_d):
    Phi = - M_star * GRAV_CONST / r_measure * (1 - np.exp(-r_measure / R_d))
    return Phi


def get_analytic_spherical_exponential_potential_derivative(r_measure, M_star, r_d):
    dPhi_dR = M_star * GRAV_CONST / r_measure**2 * (1 - np.exp(-r_measure / r_d) * (1 + r_measure / r_d))
    return dPhi_dR

########################################################################################################################

def get_analytic_hernquist_density(r_measure, M, a):
    rho_r = M  / (2 * np.pi) * a / r_measure / (r_measure + a) ** 3
    return rho_r


def get_analytic_hernquist_mass(r_measure, M, a):
    M_r = M * (r_measure / (r_measure + a)) ** 2
    return M_r


def get_analytic_hernquist_potential(r_measure, M, a):
    Phi = - GRAV_CONST * M / (r_measure + a)
    return (Phi)


def get_analytic_hernquist_dispersion(r_measure, M, a):
    disp_1d = np.sqrt(GRAV_CONST * M / (12 * a) * (
        12 * r_measure * (r_measure + a) ** 3 / a ** 4 * np.log1p(a / r_measure) -
        r_measure / (r_measure + a) * (25 + 52 * r_measure / a +
                                     42 * (r_measure / a) ** 2 + 12 * (r_measure / a) ** 3)))
    return disp_1d


def get_analytic_hernquist_circular_velocity(r_measure, M, a):

    v_c = np.sqrt(GRAV_CONST * M * r_measure / (r_measure + a) ** 2)

    return v_c


def get_analytic_hernquist_potential_derivative(r_measure, M, a):
    dPhi_dR = GRAV_CONST * M / (r_measure + a)**2
    return dPhi_dR


def get_analytic_hernquist_df(E, M, a):

    v_g = np.sqrt(GRAV_CONST * M / a)
    q = np.sqrt(-E * a / (GRAV_CONST * M))
    f_E = M / (8 * np.sqrt(2) * np.pi**3 * a**3 * v_g**3) * 1 / (1 - q**2)**(5/2) * (
            3 * np.arcsin(q) + q * (1 - q**2)**(1/2) * (1 - 2*q**2) * (8 * q**4 - 8 * q**2 - 3))

    # q = -E * a / (GRAV_CONST * M)
    # f_E = 1 / (np.sqrt(2) * (2*np.pi)**3 * (GRAV_CONST * M * a)**(3/2)) * np.sqrt(q) / (1 - q)**2 * (
    #         (1 - 2*q) * (8*q**2 - 8*q - 3) + 3 * np.arcsin(np.sqrt(q)) / np.sqrt(q * (1 - q)) )

    return f_E


def get_analytic_hernquist_density_of_states(E, M, a):

    v_g = np.sqrt(GRAV_CONST * M / a)
    q = np.sqrt(-E * a / (GRAV_CONST * M))
    g = 2 * np.sqrt(2) * np.pi**2 * a**3 * v_g / (3 * q**5) * (
        3 * (8*q**4 - 4*q**2 + 1) * np.arccos(q) - q * (1 - q**2)**(1/2) * (4*q**2 - 1)*(2*q**2 + 3))

    # A = GRAV_CONST * M / (a * np.abs(E))
    # g = (4 * np.pi)**2 * a**3 * np.sqrt(2 * np.abs(E)) * (
    #         np.sqrt(A - 1) * (A**2 / 8 - A * 5/12 - 1/3) + A/8 * (A**2 - 4*A + 8) * np.arccos(1/np.sqrt(A)))

    return g

def get_hernquist_circular_distance_from_energy(E, M, a):

    # phi = - GRAV_CONST * M / (r + a) #get_analytic_hernquist_potential(r, M, a)
    # half_vc2 = 0.5 * GRAV_CONST * M * r / (r + a)**2
    #
    # E = phi + half_vc2

    # E = -E

    # print(round(E))
    # print(round(-GRAV_CONST * M / a))
    #
    # r = (np.sqrt(GRAV_CONST**2*M**2 - 8*GRAV_CONST*M*E*a) - 4*E*a - GRAV_CONST*M) / (4*E)
    # print(r)

    r = - (np.sqrt(GRAV_CONST**2*M**2 - 8*GRAV_CONST*M*E*a) + 4*E*a + GRAV_CONST*M) / (4*E)
    # print(r)

    return r

########################################################################################################################

def get_thin_disk_SG_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a):
    # Ludlow et al. 2021 Equation (11)
    h_SG = sigma_z ** 2 / (np.pi * GRAV_CONST * Sigma_R_star)

    # #Benitez-Llambey et al. 2018
    # z_SG = 0.55 * h_SG
    # me
    z_SG = np.log(3) / 2 * h_SG

    return z_SG

def get_thin_disk_NSG_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a):
    # Ludlow et al. 2021 Equation (10)
    h_NSG = np.sqrt(2) * sigma_z * R / v_c_halo

    # #Benitez-Llambey et al. 2018
    # z_NSG = 0.477 * h_NSG
    # me
    z_NSG = erfinv(0.5) * h_NSG

    return z_NSG

def get_thin_disk_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a):
    z_SG = get_thin_disk_SG_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a)

    z_NSG = get_thin_disk_NSG_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a)

    # Ludlow et al. 2021 Equation (18)
    z_thin_half = (1 / z_SG ** 2 + 1 / (2 * z_SG * z_NSG) + 1 / z_NSG ** 2) ** (-1 / 2)

    return z_thin_half


@lru_cache(maxsize=100_000)
def get_thick_disk_NSG_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a):
    # Ludlow et al. 2021 Equation (20)
    nu_2inv = v200 * v200 / (sigma_z * sigma_z)
    rho = lambda z, nu_2inv, R, r200: np.exp(nu_2inv * (1 / (np.sqrt((R / r200) ** 2 + (z / r200) ** 2) + a / r200)
                                                        - 1 / (R / r200 + a / r200)))

    tot = quad(rho, 0, 200, args=(nu_2inv, R, r200))[0]

    half = lambda z_half, nu_2inv, R, r200: quad(rho, 0, z_half, args=(nu_2inv, R, r200))[0] - tot / 2

    out = brentq(half, 0, 200, args=(nu_2inv, R, r200))

    return out

@lru_cache(maxsize=100_000)
def get_disk_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a):
    z_thin_half = get_thin_disk_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a)

    z_thick_NSG = get_thick_disk_NSG_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a)

    z_NSG = get_thin_disk_NSG_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a)

    # Ludlow et al. 2021 Equation (21)
    z_thick_half = z_thin_half + (z_thick_NSG - z_NSG)

    return z_thick_half

def get_disk_dispersion(z_half, R, Sigma_R_star, v_c_halo, v200, r200, a):

    f = lambda sigma_z: z_half - get_disk_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a)

    # def f(sigma_z):
    #     out = z_half - get_disk_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a)
    #     print(z_half)
    #     print(out)
    #     print()
    #     return out

    try:
        out = brentq(f, 1e-3, 1e4)
    except ValueError:
        out = np.nan

    return out

########################################################################################################################

def get_constants(bin_edges, save_name_append, MassDM):
    '''Get all the scaled quantities.
    '''

    #make it not break in the centre
    bin_edges[bin_edges==0] = 1e-1
    r_measures = 10 ** ((np.log10(bin_edges[0::2]) + np.log10(bin_edges[1::2])) / 2)

    c = 10
    V200 = 200
    GalIC_H = 0.1
    r200 = V200 / (10 * GalIC_H)
    a = r200 / c * np.sqrt(2 * (np.log(1 + c) - c / (1 + c)))
    fbary = 0.01
    M = V200 ** 3 / (10 * GRAV_CONST * GalIC_H)
    Mdm = M * (1 - fbary)

    rho_dm = get_analytic_hernquist_density(r_measures, Mdm, a)

    sigma_dm = get_analytic_hernquist_dispersion(r_measures, Mdm, a)

    v_circ = get_analytic_hernquist_circular_velocity(r_measures, Mdm, a)

    rho_200 = M / (4 / 3 * np.pi * r200 ** 3)

    # t_c_dm = sigma_dm ** 3 / (GRAV_CONST ** 2 * rho_dm * MassDM) * GYR_ON_S / PC_ON_M  # 1/Gyr
    # t_c_circ = v_circ**3   / (GRAV_CONST**2 * rho_dm * MassDM) * GYR_ON_S / PC_ON_M # 1/Gyr
    t_c = V200 ** 3 / (GRAV_CONST ** 2 * rho_dm * MassDM) * GYR_ON_S / PC_ON_M  # 1/Gyr

    delta_dm = rho_dm / rho_200
    upsilon_dm = sigma_dm / V200
    upsilon_circ = v_circ / V200

    # return(t_c_dm , t_c_circ, delta_dm, upsilon_dm, upsilon_circ)
    return (t_c, 0, delta_dm, upsilon_dm, upsilon_circ)


def get_tau_array(time_array, time_scale_array):
    # e.g. Wilkinson et al. eqn. 10
    tau_array = time_array / time_scale_array

    return (tau_array)


def get_tau_heat_array(scale_density_ratio_array, scale_velocity_ratio_array,
                       ln_Lambda_k_i, alpha_i, beta_i, gamma_i):

    # e.g. Wilkinson et al. eqn. 9, 13
    tau_heat_array = np.power((ln_Lambda_k_i * np.power(scale_density_ratio_array, alpha_i) *
                               np.power(scale_velocity_ratio_array, -beta_i) / scale_velocity_ratio_array**2), -1.0 / (1 + gamma_i))

    return (tau_heat_array)


def get_tau_zero_array(velocity_scale2_array, inital_velocity2_array, scale_velocity_ratio_array):

    # e.g. Wilkinson et al. eqn. 11, 14
    sigma_dm_2 = velocity_scale2_array * scale_velocity_ratio_array**2
    tau_0_on_tau_heat = np.log(
        sigma_dm_2 / (sigma_dm_2 - inital_velocity2_array))

    return (tau_0_on_tau_heat)


def get_theory_velocity2_array(velocity_scale2_array, inital_velocity2_array,
                               time_array, time_scale_array,
                               scale_density_ratio_array, scale_velocity_ratio_array,
                               ln_Lambda_k_i, alpha_i, beta_i, gamma_i):

    tau_heat_array = get_tau_heat_array(scale_density_ratio_array, scale_velocity_ratio_array,
                                        ln_Lambda_k_i, alpha_i, beta_i, gamma_i)

    tau_0_on_tau_heat = get_tau_zero_array(velocity_scale2_array, inital_velocity2_array, scale_velocity_ratio_array)

    # e.g. Wilkinson et al. eqn. 8, 12
    theory_velocity2_array = velocity_scale2_array * scale_velocity_ratio_array**2 * (1 - np.exp(
        - np.power(((time_array / time_scale_array) / tau_heat_array) +
                   tau_0_on_tau_heat, 1 + gamma_i)))

    return (theory_velocity2_array)


def get_powerlaw_velocity2_array(velocity_scale2_array, inital_velocity2_array,
                                 time_array, time_scale_array,
                                 scale_density_ratio_array, scale_velocity_ratio_array,
                                 ln_Lambda_k_i, alpha_i, beta_i, gamma_i):
    tau_heat_array = get_tau_heat_array(scale_density_ratio_array, scale_velocity_ratio_array,
                                        ln_Lambda_k_i, alpha_i, beta_i, gamma_i) / scale_velocity_ratio_array**2

    # e.g. Wilkinson et al. eqn. 7
    theory_velocity2_array = (velocity_scale2_array - inital_velocity2_array) * (
        np.power(((time_array / time_scale_array) / tau_heat_array),
                 1 + gamma_i)) + inital_velocity2_array

    return (theory_velocity2_array)


########################################################################################################################

def load_snapshot(galaxy_name, snap):

    fname = galaxy_name + '/data/snapshot_{0:03d}.hdf5'.format(int(snap))

    # if 'spherical' in galaxy_name:
    #     fname = '../galaxies/spherical/snap_{0:03d}.hdf5'.format(int(snap))

    with h5.File(fname, 'r') as fs:

        try:
            MassDM = fs['PartType1/Masses'][:]
            PosDMs = fs['PartType1/Coordinates'][:]
            VelDMs = fs['PartType1/Velocities'][:]
            IDDMs = fs['PartType1/ParticleIDs'][:]
            try:
                PotDMs = fs['PartType1/Potential'][:]
            except KeyError:
                PotDMs = np.ones(len(IDDMs)) * np.nan
                pass
        except KeyError:
            MassDM = np.ones(1) * np.nan
            PosDMs = np.ones((1, 3)) * np.nan
            VelDMs = np.ones((1, 3)) * np.nan
            IDDMs = np.ones(1) * np.nan
            PotDMs = np.ones(1) * np.nan

        try:
            MassStar = fs['PartType2/Masses'][:]
            PosStars = fs['PartType2/Coordinates'][:]
            VelStars = fs['PartType2/Velocities'][:]
            IDStars = fs['PartType2/ParticleIDs'][:]
            try:
                PotStars = fs['PartType2/Potential'][:]
            except KeyError:
                PotStars = np.ones(len(IDStars)) * np.nan
                pass
        except KeyError:
            pass

        try:
            MassStar = fs['PartType3/Masses'][:]
            PosStars = fs['PartType3/Coordinates'][:]
            VelStars = fs['PartType3/Velocities'][:]
            IDStars = fs['PartType3/ParticleIDs'][:]
            try:
                PotStars = fs['PartType3/Potential'][:]
            except KeyError:
                PotStars = np.ones(len(IDStars)) * np.nan
                pass
        except KeyError:
            pass

    return MassDM, PosDMs, VelDMs, IDDMs, PotDMs, MassStar, PosStars, VelStars, IDStars, PotStars


def shrinking_spheres(MassDMs, PosDMs, MassStars, PosStars, n=11, max_units=100, min_units=1, discard=True):

    # 300 kpc to 1 kpc
    shrink_radii = np.logspace(np.log10(max_units), np.log10(min_units), n)
    total_offset = np.zeros(3)

    if discard:
        r_stars = np.linalg.norm(PosStars, axis=1)
        r_dms = np.linalg.norm(PosDMs, axis=1)
        mask_stars = (r_stars < 3 * max_units)
        mask_dms = (r_dms < 3 * max_units)

        ps = PosStars[mask_stars]
        ms = MassStars[mask_stars]
        pd = PosDMs[mask_dms]
        md = MassDMs[mask_dms]

        com = (np.sum(ms[:, np.newaxis] * ps, axis=0) +
               np.sum(md[:, np.newaxis] * pd, axis=0)) / (
                      np.sum(ms) + np.sum(md))

        ps -= com
        pd -= com
        total_offset += com

        for sphere_radii in shrink_radii:
            r_stars = np.linalg.norm(ps, axis=1)
            r_dms   = np.linalg.norm(pd, axis=1)
            mask_stars = (r_stars < sphere_radii)
            mask_dms   = (r_dms   < sphere_radii)

            if np.sum(mask_stars) + np.sum(mask_dms) == 0:
                # print(ps)
                # print(pd)
                # print(com)
                #
                # plt.figure()
                # plt.plot(r_dms, pd[:,2], marker='o', linestyle='')
                # plt.plot(r_stars, ps[:,2], marker='o', linestyle='')
                #
                # plt.figure()
                # plt.plot(r_dms, pd[:,1], marker='o', linestyle='')
                # plt.plot(r_stars, ps[:,1], marker='o', linestyle='')
                #
                # plt.figure()
                # plt.plot(r_dms, pd[:,0], marker='o', linestyle='')
                # plt.plot(r_stars, ps[:,0], marker='o', linestyle='')
                #
                # plt.show()

                raise ValueError(f'Shrinking Spheres failed. No particles left in {sphere_radii}units sphere. Offset at failure {total_offset}.')

            ps = ps[mask_stars]
            ms = ms[mask_stars]
            pd = pd[mask_dms]
            md = md[mask_dms]

            com = (np.sum(ms[:, np.newaxis] * ps, axis=0) +
                   np.sum(md[:, np.newaxis] * pd, axis=0)) / (
                          np.sum(ms) + np.sum(md))

            ps -= com
            pd -= com
            total_offset += com

    else:
        for sphere_radii in shrink_radii:
            r_stars = np.linalg.norm(PosStars, axis=1)
            r_dms   = np.linalg.norm(PosDMs, axis=1)
            mask_stars = (r_stars < sphere_radii)
            mask_dms   = (r_dms   < sphere_radii)

            if np.sum(mask_stars) + np.sum(mask_dms) == 0:
                raise ValueError(f'Shrinking Spheres failed. No particles left in {sphere_radii}units sphere. Offset at failure {total_offset}.')

            com = (np.sum(MassStars[mask_stars, np.newaxis] * PosStars[mask_stars], axis=0) +
                   np.sum(MassDMs[mask_dms, np.newaxis] * PosDMs[mask_dms], axis=0)) / (
                          np.sum(MassStars) + np.sum(MassDMs))

            PosStars -= com
            PosDMs -= com
            total_offset += com

    return(total_offset)


def find_rotaion_matrix(j_vector):
  '''Returns a scipy.spatial.transform.Rotation object.
  '''
  #rotate until x coord = 0
  fy = lambda y : Rotation.from_euler('y', y, degrees=True).apply(j_vector)[0]
  y = brentq(fy, 0, 180)

  #rotate until y coord = 0
  fx = lambda x : Rotation.from_euler('yx', [y,x], degrees=True).apply(j_vector)[1]
  x = brentq(fx, 0, 180)

  #check it isn't upsidedown
  j_tot = Rotation.from_euler('yx', [y,x], degrees=True).apply(j_vector)

  if j_tot[2] < 0:
    x += 180

  return(Rotation.from_euler('yx', [y,x], degrees=True))

# @lru_cache(maxsize=5)
def load_and_centre(galaxy_name, snap, align=False, min_potential=True, min_pot_N=1):
    (MassDMs, PosDMs, VelDMs, IDDMs, PotDMs, MassStars, PosStars, VelStars, IDStars, PotStars
     ) = load_snapshot(galaxy_name, snap)

    if min_potential:
        if np.sum(np.isnan(PotDMs)) == 0 and np.sum(PotDMs == 0) == 0:

            if min_pot_N == 1:
                arg_dm = np.argmin(PotDMs)
                total_offset = PosDMs[arg_dm]

            else:
                #potential weighted offset based on N most bound DM particles
                arg_dm = np.argsort(PotDMs)
                total_offset = np.sum(PotDMs[arg_dm][:min_pot_N][:,np.newaxis] * PosDMs[arg_dm][:min_pot_N],
                                      axis=0) / np.sum(PotDMs[arg_dm][:min_pot_N])

        else:
            print(np.sum(np.isnan(PotDMs)), np.sum(PotDMs == 0))
            print(f'Something went wrong with the potentials for {galaxy_name} \nTrying shrinking spheres centering.')
            return load_and_centre(galaxy_name, snap, align, min_potential=False)

    else:
        try:
            total_offset = shrinking_spheres(MassDMs, PosDMs, MassStars, PosStars, n=11, discard=True)
        except ValueError as e:
            print('Caught', e)
            print('Trying more spheres')
            try:
                total_offset = shrinking_spheres(MassDMs, PosDMs, MassStars, PosStars, n=31, discard=True)
            except ValueError:
                print('Caught', e)
                print('Trying no discards')
                try:
                    total_offset = shrinking_spheres(MassDMs, PosDMs, MassStars, PosStars, n=31, discard=False)
                except ValueError as e:
                    raise e

            print(f'Success with offset {total_offset}.')

    PosStars -= total_offset
    PosDMs -= total_offset

    # # #correct positions
    # PosStars -= total_offset
    # PosDMs -= total_offset

    # find velocity offset
    velocity_apature = 10 #kpc
    r_stars = np.linalg.norm(PosStars, axis=1)
    r_dms = np.linalg.norm(PosDMs, axis=1)
    mask_stars = r_stars < velocity_apature
    mask_dms = r_dms < velocity_apature
    # mask_stars = np.ones(len(PotStars), dtype=bool)
    # mask_dms   = np.ones(len(PotDMs),   dtype=bool)

    weighted_central_velocity = (np.sum(MassStars[mask_stars, np.newaxis] * VelStars[mask_stars], axis=0) +
                                 np.sum(MassDMs[mask_dms, np.newaxis] * VelDMs[mask_dms], axis=0)) / (
                                 np.sum(MassStars[mask_stars]) + np.sum(MassDMs[mask_dms]))

    # correct velocity
    VelStars -= weighted_central_velocity
    VelDMs -= weighted_central_velocity

    if align:
        #align orientation of disk
        j_tot = np.sum(np.cross(PosStars, VelStars) * MassStars[:, np.newaxis], axis=0)
        #find rotation
        rotation = find_rotaion_matrix(j_tot)

        #rotate DM
        PosDMs = rotation.apply(PosDMs)
        VelDMs = rotation.apply(VelDMs)
        #rotate stars
        PosStars = rotation.apply(PosStars)
        VelStars = rotation.apply(VelStars)

    return MassDMs, PosDMs, VelDMs, IDDMs, PotDMs, MassStars, PosStars, VelStars, IDStars, PotStars

########################################################################################################################

def plot_vc_profile_evolution(galaxy_name='', save=False, name='', valid=[0,1,2,8]):

    fig = plt.figure(figsize=(4,8))
    ax1 = plt.subplot(2,1,1)
    ax2 = plt.subplot(2,1,2)

    ax2.set_xlabel(r'$R$ [kpc]')
    ax1.set_ylabel(r'$V_c$ [km/s]')
    ax2.set_ylabel(r'$V_c$ [km/s]')

    ax1.set_xlim([0.5, 200])
    ax2.set_xlim([0.5, 200])
    ax1.set_ylim([0, 250])
    ax2.set_ylim([0, 250])

    ax1.semilogx()
    ax2.semilogx()
    ax1.set_xticklabels([])

    cmap = 'viridis'

    n = len(valid)

    for i, snap in enumerate(valid):

        (MassDM, PosDMs, VelDMs, IDDMs, PotDMs, MassStar, PosStars, VelStars, IDStars, PotStars
         ) = load_and_centre(galaxy_name, snap)

        if n > 1: c = matplotlib.cm.get_cmap(cmap)(i / (n-1))
        else: c = matplotlib.cm.get_cmap(cmap)(0.5)

        rs = np.linalg.norm(PosStars, axis=1)
        rs = np.sort(rs)
        ms = np.cumsum(MassStar)
        vcs = np.sqrt(GRAV_CONST * ms / rs)

        ax1.errorbar(rs, vcs, c=c, zorder = -snap)

        rsd = np.linalg.norm(PosDMs, axis=1)
        rsd = np.sort(rsd)
        msd = np.cumsum(MassDM)
        vcsd = np.sqrt(GRAV_CONST * msd / rsd)

        ax2.errorbar(rsd, vcsd, c=c, zorder = -snap)

    ms_tot = np.sum(MassStar)
    md_tot = np.sum(MassDM)

    r = np.logspace(-1, 2.5)

    c = 10
    V200 = 200
    GalIC_H = 0.1
    r_200 = V200 / (10 * GalIC_H)
    a = r_200 / c * np.sqrt(2 * (np.log(1 + c) - c / (1 + c)))
    rd = 4

    vc = np.sqrt(GRAV_CONST * get_analytic_exponential_mass(r, ms_tot, rd) / r)
    vcd = np.sqrt(GRAV_CONST * get_analytic_hernquist_mass(r, md_tot, a) / r)
    vct = np.sqrt(GRAV_CONST * (get_analytic_exponential_mass(r, ms_tot, rd) +
                                get_analytic_hernquist_mass(r, md_tot, a)) / r)

    ax1.errorbar(r, vc, c='orange', ls='--')
    # ax1.errorbar(r, vcd, c='magenta', ls='--')
    # ax1.errorbar(r, vct, c='red', ls='--')
    ax2.errorbar(r, vc, c='orange', ls='--')
    ax2.errorbar(r, vcd, c='magenta', ls='--')
    ax2.errorbar(r, vct, c='red', ls='--')

    for ax in [ax1, ax2]:
        Bbox = ax.get_position()

        cax = plt.axes([Bbox.x1, Bbox.y0, (Bbox.x1 - Bbox.x0) * 0.08, Bbox.y1 - Bbox.y0])

        x_ = np.linspace(1e-3, 1-1e-3, n)
        x, _ = np.meshgrid(x_, np.array([0, 1]))
        plt.imshow(x.T, cmap=cmap, aspect=2 / n / 0.08)
        cax.yaxis.set_label_position('right')
        cax.yaxis.tick_right()
        cax.set_yticks(np.arange(0, n))
        cax.set_yticklabels(valid)
        cax.set_xticks([])
        cax.set_xticklabels([])
        cax.set_ylabel(r'Snap')
        plt.gca().invert_yaxis()

    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return


def plot_mass_profile_comparison(galaxy_name_list, snaps_list, save=False, name=''):

    fig = plt.figure(figsize=(4*2,4*3))
    ax00 = plt.subplot(3,2,1)
    ax01 = plt.subplot(3,2,2)
    ax10 = plt.subplot(3,2,3)
    ax11 = plt.subplot(3,2,4)
    ax20 = plt.subplot(3,2,5)
    ax21 = plt.subplot(3,2,6)

    plt.subplots_adjust(wspace=0.02, hspace=0.02)

    axs = [ax00, ax01, ax10, ax11, ax20, ax21]

    for ax in axs:
        ax.set_xlim([-2.45, -0.05])
        # ax.set_ylim([-2.5, 0.5])
        ax.set_ylim([-4.05, -1.45])
        # if not ax in [ax20, ax21]: ax.set_xticklabels([])
        if not ax in [ax20, ax11]: ax.set_xticklabels([])
        # else: ax.set_xlabel(r'$\log \, r / r_{200}$')
        # else: ax.set_xlabel(r'$r / r_{200}$')
        if ax in [ax01, ax11, ax21]: ax.set_yticklabels([])
        # else: ax.set_ylabel(r'$\log \, \frac{\mathrm{d M}}{\mathrm{d} r} \frac{r_{200}}{\mathrm{M}_{200}} $')
        else: ax.set_ylabel(r'$\log \, \mathrm{M} (<r) / \mathrm{M}_{200} $')

    colors = np.array([matplotlib.colors.to_rgba('C3')] * 256)
    colors[:128, :3] = 1 - (1-colors[:128, :3]) * np.column_stack([np.linspace(0.5, 1, 128)] * 3)
    colors[128:, :3] = colors[128:, :3] * np.column_stack([np.linspace(1, 0.5, 128)] * 3)
    red = matplotlib.colors.ListedColormap(colors)

    colors = np.array([matplotlib.colors.to_rgba('C0')] * 256)
    colors[:128, :3] = 1 - (1-colors[:128, :3]) * np.column_stack([np.linspace(0.5, 1, 128)] * 3)
    colors[128:, :3] = colors[128:, :3] * np.column_stack([np.linspace(1, 0.5, 128)] * 3)
    blue = matplotlib.colors.ListedColormap(colors)

    times = np.array([0,2,4,6,8])
    # times = np.array([0.1])

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = get_name_kwargs(galaxy_name)
        v200 = 200 #kwargs['v200']
        r200 = 200 #kwargs['r200']
        M200 = v200 ** 3 / (10 * GRAV_CONST * GALIC_HUBBLE_CONST)

        axs[i].text(-1.5, -3.5, r'$m_{\mathrm{DM}} =$' + kwargs['lab'])
        # axs[i].text(-0.8, -0.1, kwargs['mu'])
        # if 'bulge' in galaxy_name:
        #     axs[i].text(-1.0, -0.3, 'Spherical')
        # if 'disk' in galaxy_name:
        #     axs[i].text(-1.0, -0.3, 'Disk')
        # if 'inter' in galaxy_name:
        #     axs[i].text(-1.0, -0.3, 'Intermediate')

        sim_times = np.linspace(0, T_TOT, snaps)

        for j, time in enumerate(times):

            snap = np.argmin((sim_times - time)**2)

            (MassDM, PosDMs, VelDMs, IDDMs, PotDMs, MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_and_centre(galaxy_name, snap)
             # ) = load_snapshot(galaxy_name, snap)

            # #super sample
            # for k in range(9):
            #     (MassDMi, PosDMsi, VelDMsi, IDDMsi, PotDMsi, MassStari, PosStarsi, VelStarsi, IDStarsi, PotStarsi
            #      ) = load_and_centre(galaxy_name + f'_seed{k+1}', snap)
            #
            #     MassDM = np.concatenate((MassDM, MassDMi))
            #     PosDMs = np.concatenate((PosDMs, PosDMsi))
            #     MassStar = np.concatenate((MassStar, MassStari))
            #     PosStars = np.concatenate((PosStars, PosStarsi))
            #
            # MassDM /= 10
            # MassStar /= 10

            rs = np.linalg.norm(PosStars, axis=1)
            rs = np.sort(rs) / r200
            ms = np.cumsum(MassStar)

            rsd = np.linalg.norm(PosDMs, axis=1)
            rsd = np.sort(rsd) / r200
            msd = np.cumsum(MassDM)

            axs[i].errorbar(np.log10(rs), np.log10(ms / M200), c=red((j / (len(times) -1))))
            axs[i].errorbar(np.log10(rsd), np.log10(msd / M200), c=blue((j / (len(times) -1))))

            # # n = 301
            # n = 26
            # r_dm_smooth = np.logspace(-2.5, 0, n)
            # # r_dm_smooth = np.linspace(0.01, 1, n)
            # fd = interp1d(rsd, msd, bounds_error=False, fill_value=0)#np.nan)
            # MassDMSmooth = fd(r_dm_smooth)
            #
            # r_star_smooth = np.logspace(-2.5, 0, n)
            # # r_star_smooth = np.linspace(0.01, 1, n)
            # fs = interp1d(rs, ms, bounds_error=False, fill_value=0)#np.nan)
            # MassStarSmooth = fs(r_star_smooth)
            #
            # axs[i].errorbar(np.log10(0.5 * (r_dm_smooth[:-1] + r_dm_smooth[1:])),
            #                 #0.5 * (r_dm_smooth[:-1] + r_dm_smooth[1:]),
            #                 np.log10(np.diff(MassDMSmooth) / np.diff(r_dm_smooth) / M200), #/ md_tot,
            #                 c=blue(j / (len(times) -1)))
            # axs[i].errorbar(np.log10(0.5 * (r_star_smooth[:-1] + r_star_smooth[1:])),
            #                 #0.5 * (r_star_smooth[:-1] + r_star_smooth[1:]),
            #                 np.log10(np.diff(MassStarSmooth) / np.diff(r_star_smooth) / M200), #/ ms_tot,
            #                 c=red(j / (len(times) -1)))

    ms_tot = np.sum(MassStar)
    md_tot = np.sum(MassDM)

    r = np.logspace(-2.5, 0)
    # r = np.linspace(0.01, 1)

    c = 10
    V200 = 200
    GalIC_H = 0.1
    r200 = V200 / (10 * GalIC_H)
    a = r200 / c * np.sqrt(2 * (np.log(1 + c) - c / (1 + c)))
    rd = 4

    ms = get_analytic_exponential_mass(r * r200, ms_tot, rd)
    msd = get_analytic_hernquist_mass(r * r200, md_tot, a)

    for ax in axs:
        ax.errorbar(np.log10(r),
                    # np.log10(0.5 * (r[1:] + r[:-1])),
                    #0.5 * (r[:-1] + r[1:]),
                    # np.log10(np.diff(ms) / np.diff(r) / M200),
                    np.log10(ms / M200),
                    c=red(0.99), ls='--', zorder=-1)
        ax.errorbar(np.log10(r),
                    # np.log10(0.5 * (r[1:] + r[:-1])),
                    #0.5 * (r[:-1] + r[1:]),
                    # np.log10(np.diff(msd) / np.diff(r) / M200),
                    np.log10(msd / M200),
                    c=blue(0.99), ls='--', zorder=-1)

    Bbox_top = ax01.get_position()
    Bbox_bot = ax21.get_position()

    width = 0.07

    cax = plt.axes([Bbox_bot.x1, Bbox_bot.y0, (Bbox_bot.x1 - Bbox_top.x0) * width, Bbox_top.y1 - Bbox_bot.y0])

    x_ = np.linspace(1e-3, 1-1e-3, len(times))
    x, _ = np.meshgrid(x_, np.array([0, 1]))
    plt.imshow(x.T, cmap=red, aspect = 3 * 2 / len(times) / width)
    cax.yaxis.set_label_position('right')
    cax.yaxis.tick_left()
    cax.set_yticks(np.arange(0, len(times)))
    cax.set_yticklabels([])
    cax.set_xticks([])
    cax.set_xticklabels([])
    plt.gca().invert_yaxis()

    cax2 = plt.axes([Bbox_bot.x1 + (Bbox_bot.x1 - Bbox_top.x0) * width, Bbox_bot.y0,
                     (Bbox_bot.x1 - Bbox_top.x0) * width, Bbox_top.y1 - Bbox_bot.y0])

    x_ = np.linspace(1e-3, 1-1e-3, len(times))
    x, _ = np.meshgrid(x_, np.array([0, 1]))
    plt.imshow(x.T, cmap=blue, aspect = 3 * 2 / len(times) / width)
    cax2.yaxis.set_label_position('right')
    cax2.yaxis.tick_right()
    cax2.set_yticks(np.arange(0, len(times)))
    cax2.set_yticklabels(times)
    cax2.set_xticks([])
    cax2.set_xticklabels([])
    cax2.set_ylabel(r'time [Gyr]')
    plt.gca().invert_yaxis()

    ax21.set_visible(False)

    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return


def plot_surface_density_comparison(galaxy_name_list, snaps_list, save=False, name=''):

    fig = plt.figure(figsize=(4*2,4*3))
    ax00 = plt.subplot(3,2,1)
    ax01 = plt.subplot(3,2,2)
    ax10 = plt.subplot(3,2,3)
    ax11 = plt.subplot(3,2,4)
    ax20 = plt.subplot(3,2,5)
    ax21 = plt.subplot(3,2,6)

    plt.subplots_adjust(wspace=0.02, hspace=0.02)

    axs = [ax00, ax01, ax10, ax11, ax20, ax21]

    for ax in axs:
        ax.set_xlim([0, 0.2])
        ax.set_ylim([-4.2, 0.2])
        # if not ax in [ax20, ax21]: ax.set_xticklabels([])
        if not ax in [ax20, ax11]: ax.set_xticklabels([])
        else: ax.set_xlabel(r'$r / r_{200}$')
        # else: ax.set_xlabel(r'$r / r_{200}$')
        if ax in [ax01, ax11, ax21]: ax.set_yticklabels([])
        else: ax.set_ylabel(r'$\log \, \Sigma(r) / \Sigma_0 $')

    colors = np.array([matplotlib.colors.to_rgba('C3')] * 256)
    colors[:128, :3] = 1 - (1-colors[:128, :3]) * np.column_stack([np.linspace(0.5, 1, 128)] * 3)
    colors[128:, :3] = colors[128:, :3] * np.column_stack([np.linspace(1, 0.5, 128)] * 3)
    red = matplotlib.colors.ListedColormap(colors)

    colors = np.array([matplotlib.colors.to_rgba('C0')] * 256)
    colors[:128, :3] = 1 - (1-colors[:128, :3]) * np.column_stack([np.linspace(0.5, 1, 128)] * 3)
    colors[128:, :3] = colors[128:, :3] * np.column_stack([np.linspace(1, 0.5, 128)] * 3)
    blue = matplotlib.colors.ListedColormap(colors)

    times = np.array([0,2,4,6,8])
    # times = np.array([0.1])

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = get_name_kwargs(galaxy_name)
        v200 = 200 #kwargs['v200']
        r200 = 200 #kwargs['r200']
        M200 = v200 ** 3 / (10 * GRAV_CONST * GALIC_HUBBLE_CONST)
        f_star = 0.01
        M_d = M200 * f_star
        R_d = 4.2
        Sigma0 = M_d / (2 * np.pi * R_d ** 2)

        axs[i].text(0.08, -1, r'$m_{\mathrm{DM}} =$' + kwargs['lab'])
        # axs[i].text(-0.8, -0.1, kwargs['mu'])
        # if 'bulge' in galaxy_name:
        #     axs[i].text(-1.0, -0.3, 'Spherical')
        # if 'disk' in galaxy_name:
        #     axs[i].text(-1.0, -0.3, 'Disk')
        # if 'inter' in galaxy_name:
        #     axs[i].text(-1.0, -0.3, 'Intermediate')

        sim_times = np.linspace(0, T_TOT, snaps)

        for j, time in enumerate(times):

            snap = np.argmin((sim_times - time)**2)

            (MassDM, PosDMs, VelDMs, IDDMs, PotDMs, MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_and_centre(galaxy_name, snap)
             # ) = load_snapshot(galaxy_name, snap)

            # #super sample
            # for k in range(9):
            #     (MassDMi, PosDMsi, VelDMsi, IDDMsi, PotDMsi, MassStari, PosStarsi, VelStarsi, IDStarsi, PotStarsi
            #      ) = load_and_centre(galaxy_name + f'_seed{k+1}', snap)
            #
            #     MassDM = np.concatenate((MassDM, MassDMi))
            #     PosDMs = np.concatenate((PosDMs, PosDMsi))
            #     MassStar = np.concatenate((MassStar, MassStari))
            #     PosStars = np.concatenate((PosStars, PosStarsi))
            #
            # MassDM /= 10
            # MassStar /= 10

            Rs = np.sqrt(PosStars[:, 0]**2 + PosStars[:, 1]**2)
            Rs = np.sort(Rs) / r200
            ms = np.cumsum(MassStar)

            # Rsd = np.sqrt(PosDMs[:, 0]**2 + PosDMs[:, 1]**2)
            # Rsd = np.sort(Rsd) / r200
            # msd = np.cumsum(MassDM)

            # n = 301
            n = 26
            # R_dm_smooth = np.logspace(-2.5, 0, n)
            # # r_dm_smooth = np.linspace(0.01, 1, n)
            # fd = interp1d(rsd, msd, bounds_error=False, fill_value=0)#np.nan)
            # MassDMSmooth = fd(r_dm_smooth)

            R_star_smooth = np.logspace(-2.5, 0, n)
            # r_star_smooth = np.linspace(0.01, 1, n)
            fs = interp1d(Rs, ms, bounds_error=False, fill_value=0)#np.nan)
            MassStarSmooth = fs(R_star_smooth)

            # axs[i].errorbar(np.log10(0.5 * (r_dm_smooth[:-1] + r_dm_smooth[1:])),
            #                 #0.5 * (r_dm_smooth[:-1] + r_dm_smooth[1:]),
            #                 np.log10(np.diff(MassDMSmooth) / np.diff(r_dm_smooth) / M200), #/ md_tot,
            #                 c=blue(j / (len(times) -1)))
            axs[i].errorbar(0.5 * (R_star_smooth[:-1] + R_star_smooth[1:]),
                            #0.5 * (r_star_smooth[:-1] + r_star_smooth[1:]),
                            np.log10(np.diff(MassStarSmooth) / (Sigma0 * r200**2 * np.pi *
                                                                (R_star_smooth[1:]**2 - R_star_smooth[:-1]**2))), #/ ms_tot,
                            c=red(j / (len(times) -1)))

    ms_tot = np.sum(MassStar)
    md_tot = np.sum(MassDM)

    r = np.logspace(-2.5, 0)
    # r = np.linspace(0.01, 1)

    c = 10
    V200 = 200
    GalIC_H = 0.1
    r200 = V200 / (10 * GalIC_H)
    a = r200 / c * np.sqrt(2 * (np.log(1 + c) - c / (1 + c)))
    M200 = v200 ** 3 / (10 * GRAV_CONST * GALIC_HUBBLE_CONST)
    f_star = 0.01
    M_d = M200 * f_star
    rd = 4.2
    Sigma0 = M_d / (2 * np.pi * rd ** 2)

    # ms = get_analytic_exponential_mass(r * r200, ms_tot, rd)
    # msd = get_analytic_hernquist_mass(r * r200, md_tot, a)
    Ss = get_exponential_surface_density(r * r200, ms_tot, rd)

    for ax in axs:
        ax.errorbar(#np.log10(r),
                    r,
                    #0.5 * (r[:-1] + r[1:]),
                    np.log10(Ss / Sigma0),
                    #np.log10(ms / M200),
                    c=red(0.99), ls='--', zorder=-1)

    Bbox_top = ax01.get_position()
    Bbox_bot = ax21.get_position()

    width = 0.07

    cax = plt.axes([Bbox_bot.x1, Bbox_bot.y0, (Bbox_bot.x1 - Bbox_top.x0) * width, Bbox_top.y1 - Bbox_bot.y0])

    x_ = np.linspace(1e-3, 1-1e-3, len(times))
    x, _ = np.meshgrid(x_, np.array([0, 1]))
    plt.imshow(x.T, cmap=red, aspect = 3 * 2 / len(times) / width)
    cax.yaxis.set_label_position('right')
    cax.yaxis.tick_left()
    cax.set_yticks(np.arange(0, len(times)))
    cax.set_yticklabels([])
    cax.set_xticks([])
    cax.set_xticklabels([])
    plt.gca().invert_yaxis()

    cax2 = plt.axes([Bbox_bot.x1 + (Bbox_bot.x1 - Bbox_top.x0) * width, Bbox_bot.y0,
                     (Bbox_bot.x1 - Bbox_top.x0) * width, Bbox_top.y1 - Bbox_bot.y0])

    x_ = np.linspace(1e-3, 1-1e-3, len(times))
    x, _ = np.meshgrid(x_, np.array([0, 1]))
    plt.imshow(x.T, cmap=blue, aspect = 3 * 2 / len(times) / width)
    cax2.yaxis.set_label_position('right')
    cax2.yaxis.tick_right()
    cax2.set_yticks(np.arange(0, len(times)))
    cax2.set_yticklabels(times)
    cax2.set_xticks([])
    cax2.set_xticklabels([])
    cax2.set_ylabel(r'time [Gyr]')
    plt.gca().invert_yaxis()

    ax21.set_visible(False)

    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return


def plot_vc_profile_comparison(galaxy_name_list, snaps_list, save=False, name=''):

    fig = plt.figure(figsize=(4*2,4*3))
    ax00 = plt.subplot(3,2,1)
    ax01 = plt.subplot(3,2,2)
    ax10 = plt.subplot(3,2,3)
    ax11 = plt.subplot(3,2,4)
    ax20 = plt.subplot(3,2,5)
    ax21 = plt.subplot(3,2,6)

    plt.subplots_adjust(wspace=0.02, hspace=0.02)

    axs = [ax00, ax01, ax10, ax11, ax20, ax21]

    for ax in axs:
        ax.set_xlim([-2.05, -0.05])
        ax.set_ylim([-0.9, 0.3])
        # if not ax in [ax20, ax21]: ax.set_xticklabels([])
        if not ax in [ax20, ax11]: ax.set_xticklabels([])
        else: ax.set_xlabel(r'$\log \, r / r_{200}$')
        if ax in [ax01, ax11, ax21]: ax.set_yticklabels([])
        else: ax.set_ylabel(r'$\log \, V_c / V_{200}$')

    colors = np.array([matplotlib.colors.to_rgba('C3')] * 256)
    colors[:128, :3] = 1 - (1-colors[:128, :3]) * np.column_stack([np.linspace(0.5, 1, 128)] * 3)
    colors[128:, :3] = colors[128:, :3] * np.column_stack([np.linspace(1, 0.5, 128)] * 3)
    red = matplotlib.colors.ListedColormap(colors)

    colors = np.array([matplotlib.colors.to_rgba('C0')] * 256)
    colors[:128, :3] = 1 - (1-colors[:128, :3]) * np.column_stack([np.linspace(0.5, 1, 128)] * 3)
    colors[128:, :3] = colors[128:, :3] * np.column_stack([np.linspace(1, 0.5, 128)] * 3)
    blue = matplotlib.colors.ListedColormap(colors)

    times = np.array([0,2,4,6,8])
    # times = np.array([0.1])

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = get_name_kwargs(galaxy_name)
        v200 = 200 #kwargs['v200']
        r200 = 200 #kwargs['r200']

        axs[i].text(-1.5, -0.3, r'$m_{\mathrm{DM}} =$' + kwargs['lab'])
        # axs[i].text(-0.8, -0.1, kwargs['mu'])
        # if 'bulge' in galaxy_name:
        #     axs[i].text(-1.0, -0.3, 'Spherical')
        # if 'disk' in galaxy_name:
        #     axs[i].text(-1.0, -0.3, 'Disk')
        # if 'inter' in galaxy_name:
        #     axs[i].text(-1.0, -0.3, 'Intermediate')

        sim_times = np.linspace(0, T_TOT, snaps)

        for j, time in enumerate(times):

            snap = np.argmin((sim_times - time)**2)

            (MassDM, PosDMs, VelDMs, IDDMs, PotDMs, MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_and_centre(galaxy_name, snap)
             # ) = load_snapshot(galaxy_name, snap)

            # #super sample
            # for k in range(9):
            #     (MassDMi, PosDMsi, VelDMsi, IDDMsi, PotDMsi, MassStari, PosStarsi, VelStarsi, IDStarsi, PotStarsi
            #      ) = load_and_centre(galaxy_name + f'_seed{k+1}', snap)
            # 
            #     MassDM = np.concatenate((MassDM, MassDMi))
            #     PosDMs = np.concatenate((PosDMs, PosDMsi))
            #     MassStar = np.concatenate((MassStar, MassStari))
            #     PosStars = np.concatenate((PosStars, PosStarsi))
            # 
            # MassDM /= 10
            # MassStar /= 10

            rs = np.linalg.norm(PosStars, axis=1)
            rs = np.sort(rs)
            ms = np.cumsum(MassStar)
            vcs = np.sqrt(GRAV_CONST * ms / rs)

            rsd = np.linalg.norm(PosDMs, axis=1)
            rsd = np.sort(rsd)
            msd = np.cumsum(MassDM)
            vcsd = np.sqrt(GRAV_CONST * msd / rsd)

            axs[i].errorbar(np.log10(rs/r200), np.log10(vcs/v200), c=red((j / (len(times) -1))))
            axs[i].errorbar(np.log10(rsd/r200), np.log10(vcsd/v200), c=blue((j / (len(times) -1))))

    ms_tot = np.sum(MassStar)
    md_tot = np.sum(MassDM)

    r = np.logspace(-1, 4)

    c = 10
    V200 = 200
    GalIC_H = 0.1
    r_200 = V200 / (10 * GalIC_H)
    a = r_200 / c * np.sqrt(2 * (np.log(1 + c) - c / (1 + c)))
    rd = 4

    vc = np.sqrt(GRAV_CONST * get_analytic_exponential_mass(r, ms_tot, rd) / r)
    vcd = np.sqrt(GRAV_CONST * get_analytic_hernquist_mass(r, md_tot, a) / r)
    vct = np.sqrt(GRAV_CONST * (get_analytic_exponential_mass(r, ms_tot, rd) +
                                get_analytic_hernquist_mass(r, md_tot, a)) / r)

    for ax in axs:
        ax.errorbar(np.log10(r / r200), np.log10(vc / v200), c=red(0.99), ls='--', zorder=-1)
        ax.errorbar(np.log10(r / r200), np.log10(vcd / v200), c=blue(0.99), ls='--', zorder=-1)
        # ax.errorbar(r, vct, c='red', ls='--')

    Bbox_top = ax01.get_position()
    Bbox_bot = ax21.get_position()

    width = 0.07

    cax = plt.axes([Bbox_bot.x1, Bbox_bot.y0, (Bbox_bot.x1 - Bbox_top.x0) * width, Bbox_top.y1 - Bbox_bot.y0])

    x_ = np.linspace(1e-3, 1-1e-3, len(times))
    x, _ = np.meshgrid(x_, np.array([0, 1]))
    plt.imshow(x.T, cmap=red, aspect = 3 * 2 / len(times) / width)
    cax.yaxis.set_label_position('right')
    cax.yaxis.tick_left()
    cax.set_yticks(np.arange(0, len(times)))
    cax.set_yticklabels([])
    cax.set_xticks([])
    cax.set_xticklabels([])
    plt.gca().invert_yaxis()

    cax2 = plt.axes([Bbox_bot.x1 + (Bbox_bot.x1 - Bbox_top.x0) * width, Bbox_bot.y0,
                     (Bbox_bot.x1 - Bbox_top.x0) * width, Bbox_top.y1 - Bbox_bot.y0])

    x_ = np.linspace(1e-3, 1-1e-3, len(times))
    x, _ = np.meshgrid(x_, np.array([0, 1]))
    plt.imshow(x.T, cmap=blue, aspect = 3 * 2 / len(times) / width)
    cax2.yaxis.set_label_position('right')
    cax2.yaxis.tick_right()
    cax2.set_yticks(np.arange(0, len(times)))
    cax2.set_yticklabels(times)
    cax2.set_xticks([])
    cax2.set_xticklabels([])
    cax2.set_ylabel(r'time [Gyr]')
    plt.gca().invert_yaxis()

    ax21.set_visible(False)

    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return


def plot_vc_profile_ICs(galaxy_name_list, snaps_list, save=False, name=''):

    # galaxy_name_list = ['../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps',
    #                     '../galaxies/mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps']
    galaxy_name_list = [
                        # '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps',
                        # '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed1',
                        # '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed2',
                        # '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed3',
                        # '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed4',
                        # '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed5',
                        # '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed6',
                        # '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed7',
                        # '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed8',
                        # '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed9',
                        # '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps',
                        # '../galaxies/mu_2/fdisk_0p01_lgMdm_8_V200_200kmps_seed1',
                        # '../galaxies/mu_2/fdisk_0p01_lgMdm_8_V200_200kmps_seed2',
                        # '../galaxies/mu_2/fdisk_0p01_lgMdm_8_V200_200kmps_seed3',
                        # '../galaxies/mu_2/fdisk_0p01_lgMdm_8_V200_200kmps_seed4',
                        # '../galaxies/mu_2/fdisk_0p01_lgMdm_8_V200_200kmps_seed5',
                        # '../galaxies/mu_2/fdisk_0p01_lgMdm_8_V200_200kmps_seed6',
                        # '../galaxies/mu_2/fdisk_0p01_lgMdm_8_V200_200kmps_seed7',
                        # '../galaxies/mu_2/fdisk_0p01_lgMdm_8_V200_200kmps_seed8',
                        # '../galaxies/mu_2/fdisk_0p01_lgMdm_8_V200_200kmps_seed9',
                        '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps',
                        '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed1',
                        '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed2',
                        '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed3',
                        '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed4',
                        '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed5',
                        '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed6',
                        '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed7',
                        '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed8',
                        '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed9',
                        '../galaxies/mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        '../galaxies/mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed1',
                        '../galaxies/mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed2',
                        '../galaxies/mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed3',
                        '../galaxies/mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed4',
                        '../galaxies/mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed5',
                        '../galaxies/mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed6',
                        '../galaxies/mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed7',
                        '../galaxies/mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed8',
                        '../galaxies/mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed9',
                        ]

    fig = plt.figure(figsize=(8,6))
    ax = plt.subplot(3,1,1)
    ax_in = plt.subplot(3,1,2)
    ax_in2 = plt.subplot(3,1,3)

    plt.subplots_adjust(wspace=0.02, hspace=0.02)

    ax.set_xlim([-3.05, -0.05])
    ax.set_ylim([-0.9, 0.3])

    ax_in.set_xlim([-3.05, -0.05])
    ax_in2.set_xlim([-3.05, -0.05])
    ax_in.set_ylim([-0.1, 0.1])
    ax_in2.set_ylim([-0.1, 0.1])
    ax.set_xticklabels([])
    ax_in.set_xticklabels([])

    ax_in2.set_xlabel(r'$\log \, r / r_{200}$')
    ax.set_ylabel(r'$\log \, V_c / V_{200}$')

    ax_in.set_ylabel(r'$ \Delta V_{c, \star} / V_c$')
    ax_in2.set_ylabel(r'$ \Delta V_{c, \mathrm{DM}} / V_c$')

    v200 = 200  # kwargs['v200']
    r200 = 200  # kwargs['r200']

    c = 10
    V200 = 200
    GalIC_H = 0.1
    r_200 = V200 / (10 * GalIC_H)
    a = r_200 / c * np.sqrt(2 * (np.log(1 + c) - c / (1 + c)))
    rd = 4

    for i, galaxy_name in enumerate(galaxy_name_list):

        # (MassDM, PosDMs, VelDMs, IDDMs, PotDMs, MassStar, PosStars, VelStars, IDStars, PotStars
        #  ) = load_and_centre(galaxy_name, 0)
        (MassDM, PosDMs, VelDMs, IDDMs, PotDMs, MassStar, PosStars, VelStars, IDStars, PotStars
         ) = load_snapshot(galaxy_name, 0)

        rs = np.linalg.norm(PosStars, axis=1)
        rs = np.sort(rs)
        ms = np.cumsum(MassStar)
        vcs = np.sqrt(GRAV_CONST * ms / rs)

        rsd = np.linalg.norm(PosDMs, axis=1)
        rsd = np.sort(rsd)
        msd = np.cumsum(MassDM)
        vcsd = np.sqrt(GRAV_CONST * msd / rsd)

        if 'disk' in galaxy_name: c = 'C0'
        if 'bulge' in galaxy_name: c = 'C1'
        lw = 2
        alpha = 0.5

        ax.errorbar(np.log10(rs/r200), np.log10(vcs/v200), c=c, lw=lw, alpha=alpha)
        ax.errorbar(np.log10(rsd/r200), np.log10(vcsd/v200), c=c, lw=lw, alpha=alpha, ls=':')

        vc = np.sqrt(GRAV_CONST * get_analytic_exponential_mass(rs, ms[-1], rd) / rs)
        vcd = np.sqrt(GRAV_CONST * get_analytic_hernquist_mass(rsd, msd[-1], a) / rsd)

        ax_in.errorbar(np.log10(rs/r200), (vcs - vc) / vc, c=c, lw=lw, alpha=alpha)
        ax_in2.errorbar(np.log10(rsd/r200), (vcsd - vcd) / vcd, c=c, lw=lw, alpha=alpha, ls=':')

        if 'bulge' in galaxy_name:
            # print(galaxy_name)
            print(rsd[10000:10006])
            # print(rs[100:106])


    for j in range(2):
        rs = []
        ms = []
        rsd = []
        msd = []
        for i in range(10):
            # (MassDM, PosDMs, VelDMs, IDDMs, PotDMs, MassStar, PosStars, VelStars, IDStars, PotStars
            #  ) = load_and_centre(galaxy_name, 0)
            (MassDM, PosDMs, VelDMs, IDDMs, PotDMs, MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_snapshot(galaxy_name_list[i + 10*j], 0)

            r_ = np.linalg.norm(PosStars, axis=1)

            rs.append(np.linalg.norm(PosStars, axis=1))
            ms.append(MassStar)
            rsd.append(np.linalg.norm(PosDMs, axis=1))
            msd.append(MassDM)

        rs = np.reshape(rs, np.shape(rs)[0] * np.shape(rs)[1])
        ms = np.reshape(ms, np.shape(ms)[0] * np.shape(ms)[1]) / 10
        rsd = np.reshape(rsd, np.shape(rsd)[0] * np.shape(rsd)[1])
        msd = np.reshape(msd, np.shape(msd)[0] * np.shape(msd)[1]) / 10

        ms = np.cumsum(ms)
        rs = np.sort(rs)
        vcs = np.sqrt(GRAV_CONST * ms / rs)

        rsd = np.sort(rsd)
        msd = np.cumsum(msd)
        vcsd = np.sqrt(GRAV_CONST * msd / rsd)

        if 'disk' in galaxy_name_list[10*j - 1]: c = 'C3'
        if 'bulge' in galaxy_name_list[10*j - 1]: c = 'C2'

        ls = '--'
        lw = 3
        alpha = 1

        pth_w = [path_effects.Stroke(linewidth=4, foreground='k'), path_effects.Normal()]
        ax.errorbar(np.log10(rs/r200), np.log10(vcs/v200), c=c, lw=lw, alpha=alpha, ls=ls, path_effects=pth_w)
        ax.errorbar(np.log10(rsd/r200), np.log10(vcsd/v200), c=c, lw=lw, alpha=alpha, ls=ls, path_effects=pth_w)

        vc = np.sqrt(GRAV_CONST * get_analytic_exponential_mass(rs, ms[-1], rd) / rs)
        vcd = np.sqrt(GRAV_CONST * get_analytic_hernquist_mass(rsd, msd[-1], a) / rsd)

        ax_in.errorbar(np.log10(rs/r200), (vcs - vc) / vc, c=c, lw=lw, alpha=alpha, ls=ls, path_effects=pth_w)
        ax_in2.errorbar(np.log10(rsd/r200), (vcsd - vcd) / vcd, c=c, lw=lw, alpha=alpha, ls=ls, path_effects=pth_w)

    #model
    ms_tot = np.sum(MassStar)
    md_tot = np.sum(MassDM)

    r = np.logspace(-1, 4)

    c = 10
    V200 = 200
    GalIC_H = 0.1
    r_200 = V200 / (10 * GalIC_H)
    a = r_200 / c * np.sqrt(2 * (np.log(1 + c) - c / (1 + c)))
    rd = 4

    vc = np.sqrt(GRAV_CONST * get_analytic_exponential_mass(r, ms_tot, rd) / r)
    vcd = np.sqrt(GRAV_CONST * get_analytic_hernquist_mass(r, md_tot, a) / r)
    vct = np.sqrt(GRAV_CONST * (get_analytic_exponential_mass(r, ms_tot, rd) +
                                get_analytic_hernquist_mass(r, md_tot, a)) / r)

    pth_w = [path_effects.Stroke(linewidth=3, foreground='white'), path_effects.Normal()]
    ax.errorbar(np.log10(r / r200), np.log10(vc / v200), c='k', ls='--', zorder=2, path_effects=pth_w)
    ax.errorbar(np.log10(r / r200), np.log10(vcd / v200), c='k', ls='--', zorder=2, path_effects=pth_w)

    ax_in.errorbar(np.log10(r / r200), np.zeros(len(r)), c='k', ls='--', zorder=2, path_effects=pth_w)
    ax_in2.errorbar(np.log10(r / r200), np.zeros(len(r)), c='k', ls='--', zorder=2, path_effects=pth_w)

    for a_ in [ax, ax_in, ax_in2]:
        a_.axvline(np.log10(0.05 / r200),  0, 1, c='k', ls='-', linewidth=3)
        a_.axvline(np.log10(0.05 * 2.8 / r200),  0, 1, c='k', ls='-', linewidth=3)

    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return


def plot_sigma_profile_comparison(galaxy_name_list, snaps_list, save=False, name='', super=True):

    mean=False
    if 'bulk' in name: mean = True

    # mean=False
    N_star = 100
    N_star_super = 10
    N_DM = 400
    N_DM_super = 100

    fig = plt.figure(figsize=(4*2,4*3))
    ax00 = plt.subplot(3,2,1)
    ax01 = plt.subplot(3,2,2)
    ax10 = plt.subplot(3,2,3)
    ax11 = plt.subplot(3,2,4)
    ax20 = plt.subplot(3,2,5)
    ax21 = plt.subplot(3,2,6)

    plt.subplots_adjust(wspace=0.02, hspace=0.02)

    axs = [ax00, ax01, ax10, ax11, ax20, ax21]

    for ax in axs:
        ax.set_xlim([-2.05, -0.05])
        ax.set_ylim([-0.9, 0.3])
        # if 'bulk' in name:
        #     ax.set_ylim([-1.7, 0.1])
        if not ax in [ax20, ax21]: ax.set_xticklabels([])
        else: ax.set_xlabel(r'$\log \, r / r_{200}$')
        if ax in [ax01, ax11, ax21]: ax.set_yticklabels([])
        else: ax.set_ylabel(r'$\log \, \sigma_{1D} / V_{200}$')

    colors = np.array([matplotlib.colors.to_rgba('C3')] * 256)
    colors[:128, :3] = 1 - (1-colors[:128, :3]) * np.column_stack([np.linspace(0.33, 1, 128)] * 3)
    colors[128:, :3] = colors[128:, :3] * np.column_stack([np.linspace(1, 0.33, 128)] * 3)
    red = matplotlib.colors.ListedColormap(colors)

    colors = np.array([matplotlib.colors.to_rgba('C0')] * 256)
    colors[:128, :3] = 1 - (1-colors[:128, :3]) * np.column_stack([np.linspace(0.33, 1, 128)] * 3)
    colors[128:, :3] = colors[128:, :3] * np.column_stack([np.linspace(1, 0.33, 128)] * 3)
    blue = matplotlib.colors.ListedColormap(colors)

    times = np.array([0,1,2,3,4,5,6,7,8,9])

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = get_name_kwargs(galaxy_name)

        axs[i].text(-1.9, -0.6, kwargs['mu'])
        if 'bulge' in galaxy_name:
            axs[i].text(-1.9, -0.8, 'Spherical')
        if 'disk' in galaxy_name:
            axs[i].text(-1.9, -0.8, 'Disk')

        v200 = 200 #kwargs['v200']
        r200 = 200 #kwargs['r200']

        sim_times = np.linspace(0, T_TOT, snaps)

        for j, time in enumerate(times):

            snap = np.argmin((sim_times - time)**2)

            (MassDM, PosDMs, VelDMs, IDDMs, PotDMs, MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_and_centre(galaxy_name, snap)
             # ) = load_snapshot(galaxy_name, snap)

            print(len(MassDM))
            print(len(MassStar))
            print(MassDM[0] / MassStar[0])
            print(len(MassStar) / len(MassDM))
            print(np.sum(MassStar) / np.sum(MassDM))
            print()

            if super:
                #super sample
                for k in range(9):
                    (MassDMi, PosDMsi, VelDMsi, IDDMsi, PotDMsi, MassStari, PosStarsi, VelStarsi, IDStarsi, PotStarsi
                     ) = load_and_centre(galaxy_name + f'_seed{k+1}', snap)

                    MassDM = np.concatenate((MassDM, MassDMi))
                    PosDMs = np.concatenate((PosDMs, PosDMsi))
                    VelDMs = np.concatenate((VelDMs, VelDMsi))
                    MassStar = np.concatenate((MassStar, MassStari))
                    PosStars = np.concatenate((PosStars, PosStarsi))
                    VelStars = np.concatenate((VelStars, VelStarsi))

                MassDM /= 10
                MassStar /= 10

            rs = np.linalg.norm(PosStars, axis=1)
            r_arg = np.argsort(rs)
            r, phi, theta, v_r, v_p, v_t = get_spherical_coords(PosStars[r_arg], VelStars[r_arg])

            sigma_r = get_sigma_N(v_r, N=N_star, super=N_star_super, zero=mean)
            sigma_p = get_sigma_N(v_p, N=N_star, super=N_star_super, zero=mean)
            sigma_t = get_sigma_N(v_t, N=N_star, super=N_star_super, zero=mean)
            sigma_1d = np.sqrt((sigma_r**2 + sigma_p**2 + sigma_t**2)/3)

            if j == 0 and 'disk' in galaxy_name:
                print(np.median(np.abs(PosStars[:, 2])))

            # rs = np.sqrt(PosStars[:,0] ** 2 + PosStars[:,1]**2)
            # r_arg = np.argsort(rs)
            # r, phi, zax, v_rho, v_phi, v_z = get_cylindrical(PosStars[r_arg], VelStars[r_arg])
            #
            # sigma_1d = get_sigma_N(v_z, N=N_star, super=N_star_super, zero=mean)

            axs[i].errorbar(np.log10(r[N_star//2 : -N_star//2 : N_star_super]/r200), np.log10(sigma_1d/v200),
                            c=red((j / (len(times) -1))))

            rs = np.linalg.norm(PosDMs, axis=1)
            r_arg = np.argsort(rs)
            r, phi, theta, v_r, v_p, v_t = get_spherical_coords(PosDMs[r_arg], VelDMs[r_arg])

            sigma_r = get_sigma_N(v_r, N=N_DM, super=N_DM_super, zero=mean)
            sigma_p = get_sigma_N(v_p, N=N_DM, super=N_DM_super, zero=mean)
            sigma_t = get_sigma_N(v_t, N=N_DM, super=N_DM_super, zero=mean)

            sigma_1d = np.sqrt((sigma_r ** 2 + sigma_p ** 2 + sigma_t ** 2) / 3)

            axs[i].errorbar(np.log10(r[N_DM//2 : -N_DM//2: N_DM_super]/r200), np.log10(sigma_1d/v200),
                            c=blue((j / (len(times) -1))))

    ms_tot = np.sum(MassStar)
    md_tot = np.sum(MassDM)

    r = np.logspace(-1, 4)

    c = 10
    V200 = 200
    GalIC_H = 0.1
    r_200 = V200 / (10 * GalIC_H)
    a = r_200 / c * np.sqrt(2 * (np.log(1 + c) - c / (1 + c)))
    rd = 4
    zd = 0.1
    z_half = zd * 2 / np.log(3)

    rho = lambda r: get_exponential_spherical_density(r, ms_tot, rd)
    # acc = lambda r: get_analytic_spherical_exponential_potential_derivative(r, ms_tot, rd)
    acc = lambda r: get_analytic_hernquist_potential_derivative(r, md_tot, a) + get_analytic_spherical_exponential_potential_derivative(r, ms_tot, rd)
    integrad = lambda r: rho(r) * acc(r)
    sigma_r = np.sqrt([quad(integrad, r_, 10*a)[0] / rho(r_) for r_ in r])

    rho = lambda r: get_analytic_hernquist_density(r, md_tot, a)
    # acc = lambda r: get_analytic_hernquist_potential_derivative(r, md_tot, ad)
    acc = lambda r: get_analytic_hernquist_potential_derivative(r, md_tot, a) + get_analytic_spherical_exponential_potential_derivative(r, ms_tot, rd)
    integrad = lambda r: rho(r) * acc(r)
    sigma_rd = np.sqrt([quad(integrad, r_, 10*a)[0] / rho(r_) for r_ in r])

    #thin disk
    # vc = np.sqrt(GRAV_CONST * get_analytic_hernquist_mass(r, md_tot, a)/ r)
    vc = np.sqrt(GRAV_CONST * (get_analytic_hernquist_mass(r, md_tot, a) + get_analytic_exponential_mass(r, ms_tot, rd))/ r)
    Sigma = get_exponential_surface_density(r, ms_tot, rd)

    sigma_z_hydro = np.array([get_disk_dispersion(z_half, r_, Sigma_, vc_, v200, r200, a) for r_, vc_, Sigma_ in
                              zip(r, vc, Sigma)])

    sigma_tot = np.sqrt(sigma_z_hydro**2 + vc**2 / 3)

    for ax in axs:
        ax.errorbar(np.log10(r / r200), np.log10(sigma_rd / v200), c=blue(0.99), ls='--', zorder=-1)

        ax.errorbar(np.log10(r / r200), np.log10(sigma_r / v200), c=red(0.99), ls='--', zorder=-1)
        ax.errorbar(np.log10(r / r200), np.log10(sigma_z_hydro / v200), c='darkorchid', ls='--', zorder=-1)
        ax.errorbar(np.log10(r / r200), np.log10(sigma_tot / v200), c='sandybrown', ls='--', zorder=-1)

    Bbox_top = ax01.get_position()
    Bbox_bot = ax21.get_position()

    width = 0.07

    cax = plt.axes([Bbox_bot.x1, Bbox_bot.y0, (Bbox_bot.x1 - Bbox_top.x0) * width, Bbox_top.y1 - Bbox_bot.y0])

    x_ = np.linspace(1e-3, 1-1e-3, len(times))
    x, _ = np.meshgrid(x_, np.array([0, 1]))
    plt.imshow(x.T, cmap=red, aspect = 3 * 2 / len(times) / width)
    cax.yaxis.tick_left()
    cax.set_yticks(np.arange(0, len(times)))
    cax.set_yticklabels([])
    cax.set_xticks([])
    cax.set_xticklabels([])
    plt.gca().invert_yaxis()

    cax2 = plt.axes([Bbox_bot.x1 + (Bbox_bot.x1 - Bbox_top.x0) * width, Bbox_bot.y0,
                     (Bbox_bot.x1 - Bbox_top.x0) * width, Bbox_top.y1 - Bbox_bot.y0])

    x_ = np.linspace(1e-3, 1-1e-3, len(times))
    x, _ = np.meshgrid(x_, np.array([0, 1]))
    plt.imshow(x.T, cmap=blue, aspect = 3 * 2 / len(times) / width)
    cax2.yaxis.set_label_position('right')
    cax2.yaxis.tick_right()
    cax2.set_yticks(np.arange(0, len(times)))
    cax2.set_yticklabels(times)
    cax2.set_xticks([])
    cax2.set_xticklabels([])
    cax2.set_ylabel(r'time [Gyr]')
    plt.gca().invert_yaxis()

    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return


def plot_energy_pdf_comparison(galaxy_name_list, snaps_list, save=False, name='', super=False):

    eps = 1e-10

    fig = plt.figure(figsize=(5*2,5*3))

    ax00 = plt.subplot(3,2,1)
    ax01 = plt.subplot(3,2,2)
    ax10 = plt.subplot(3,2,3)
    ax11 = plt.subplot(3,2,4)
    ax20 = plt.subplot(3,2,5)
    ax21 = plt.subplot(3,2,6)

    plt.subplots_adjust(wspace=0.02, hspace=0.02)

    axs = [ax00, ax01, ax10, ax11, ax20, ax21]

    for ax in axs:
        ax.set_xlim([-1.2, 0])
        ax.set_ylim([-3, 1])
        # ax.set_ylim([0, 5])
        # ax.set_ylim([0, 200])

        if not ax in [ax20, ax21]: ax.set_xticklabels([])
        else: ax.set_xlabel(r'$E / E_h$')
        if ax in [ax01, ax11, ax21]: ax.set_yticklabels([])
        # else: ax.set_ylabel(r'PDF [10$^10$ M$_\odot$ / $10^5$ / km$^2$ / s$^2$ ]')
        else: ax.set_ylabel(r'$\log \, \frac{dM(E)}{dE} \times \frac{E_h}{M_h}$')
        # else: ax.set_ylabel(r'CDF')

    colors = np.array([matplotlib.colors.to_rgba('C3')] * 256)
    colors[:128, :3] = 1 - (1-colors[:128, :3]) * np.column_stack([np.linspace(0.33, 1, 128)] * 3)
    colors[128:, :3] = colors[128:, :3] * np.column_stack([np.linspace(1, 0.33, 128)] * 3)
    red = matplotlib.colors.ListedColormap(colors)

    colors = np.array([matplotlib.colors.to_rgba('C0')] * 256)
    colors[:128, :3] = 1 - (1-colors[:128, :3]) * np.column_stack([np.linspace(0.33, 1, 128)] * 3)
    colors[128:, :3] = colors[128:, :3] * np.column_stack([np.linspace(1, 0.33, 128)] * 3)
    blue = matplotlib.colors.ListedColormap(colors)

    times = np.array([0,1,2,3,4,5,6,7,8,9])
    # times = np.array([0,9])

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
    # for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list[:2], snaps_list[:2])):
        kwargs = get_name_kwargs(galaxy_name)

        if '_seed0' in galaxy_name:
            galaxy_name = galaxy_name[:-len('_seed0')]

        axs[i].text(-0.4, -0.6, kwargs['mu'])
        if 'bulge' in galaxy_name:
            axs[i].text(-0.4, -1.0, 'Spherical')
        if 'disk' in galaxy_name:
            axs[i].text(-0.4, -1.0, 'Disk')

        c = 10
        V200 = 200
        GalIC_H = 0.1
        r_200 = V200 / (10 * GalIC_H)
        a = r_200 / c * np.sqrt(2 * (np.log(1 + c) - c / (1 + c)))
        fbary = 0.01
        Mdm = V200**3 / (10 * GRAV_CONST * GalIC_H) * (1 - fbary)
        EhDM = GRAV_CONST * Mdm / a

        sim_times = np.linspace(0, T_TOT, snaps)

        for j, time in enumerate(times):

            snap = np.argmin((sim_times - time)**2)

            (MassDM, PosDMs, VelDMs, IDDMs, PotDMs, MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_and_centre(galaxy_name, snap)
             # ) = load_snapshot(galaxy_name, snap)

            if super:
                #super sample
                for k in range(9):
                    (MassDMi, PosDMsi, VelDMsi, IDDMsi, PotDMsi, MassStari, PosStarsi, VelStarsi, IDStarsi, PotStarsi
                     ) = load_and_centre(galaxy_name + f'_seed{k+1}', snap)

                    MassDM = np.concatenate((MassDM, MassDMi))
                    # PosDMs = np.concatenate((PosDMs, PosDMsi))
                    VelDMs = np.concatenate((VelDMs, VelDMsi))
                    PotDMs = np.concatenate((PotDMs, PotDMsi))
                    MassStar = np.concatenate((MassStar, MassStari))
                    # PosStars = np.concatenate((PosStars, PosStarsi))
                    VelStars = np.concatenate((VelStars, VelStarsi))
                    PotStars = np.concatenate((PotStars, PotStarsi))

                MassDM /= 10
                MassStar /= 10

            #TODO guiding radius?

            # md_tot = np.sum(MassDM)
            # ms_tot = np.sum(MassStar)

            EnergyDMs = PotDMs + 0.5 * np.linalg.norm(VelDMs, axis=1)**2
            EnergyStars = PotStars + 0.5 * np.linalg.norm(VelStars, axis=1)**2

            EnergyDMs = np.sort(EnergyDMs)
            EnergyStars = np.sort(EnergyStars)

            # n = 301
            n = 26
            # EnergyDMsSmooth = np.linspace(EnergyDMs[0], EnergyDMs[-1], n)
            EnergyDMsSmooth = np.linspace(-3e5 , 0, n)
            fd = interp1d(EnergyDMs, np.cumsum(MassDM), bounds_error=False, fill_value=eps)#np.nan)
            MassDMSmooth = fd(EnergyDMsSmooth)
            # EnergyStarsSmooth = np.linspace(EnergyStars[0], EnergyStars[-1], n)
            EnergyStarsSmooth = np.linspace(-3e5 , 0, n)
            fs = interp1d(EnergyStars, np.cumsum(MassStar), bounds_error=False, fill_value=eps)#np.nan)
            MassStarSmooth = fs(EnergyStarsSmooth)

            axs[i].errorbar(0.5 * (EnergyDMsSmooth[:-1] + EnergyDMsSmooth[1:]) / EhDM,
                            np.log10(np.diff(MassDMSmooth) / np.diff(EnergyDMsSmooth) * EhDM / Mdm + eps), #/ md_tot,
                            c=blue(j / (len(times) -1)))
            axs[i].errorbar(0.5 * (EnergyStarsSmooth[:-1] + EnergyStarsSmooth[1:]) / EhDM,
                            np.log10(np.diff(MassStarSmooth) / np.diff(EnergyStarsSmooth) * EhDM / Mdm + eps), #/ ms_tot,
                            c=red(j / (len(times) -1)))

            # axs[i].errorbar((EnergyDMs[:-1] + EnergyDMs[1:]) * 0.5e-5,
            #                 np.diff(np.cumsum(MassDM)) / np.diff(EnergyDMs) * 1e5,
            #                 c=blue((j / (len(times) -1))), alpha=0.1)
            # axs[i].errorbar((EnergyStars[:-1] + EnergyStars[1:]) * 0.5e-5,
            #                 np.diff(np.cumsum(MassStar)) / np.diff(EnergyStars) * 1e5,
            #                 c=red((j / (len(times) -1))), alpha=0.1)

            # # cumulative
            # axs[i].errorbar(EnergyDMs * 1e-5, np.cumsum(MassDM),
            #                 c=blue((j / (len(times) -1))))
            # axs[i].errorbar(EnergyStars * 1e-5, np.cumsum(MassStar),
            #                 c=red((j / (len(times) -1))))

    md_tot = np.sum(MassDM)
    ms_tot = np.sum(MassStar)
    m_tot = md_tot + ms_tot
    # md_tot = m_tot

    c = 10
    V200 = 200
    GalIC_H = 0.1
    r_200 = V200 / (10 * GalIC_H)
    a = r_200 / c * np.sqrt(2 * (np.log(1 + c) - c / (1 + c)))
    rd = 4
    zd = 0.1
    z_half = zd * 2 / np.log(3)

    Es = np.linspace(-(GRAV_CONST * md_tot) / a, 0, 101)

    # integrad = lambda E: get_analytic_hernquist_df(E, m_tot, a) * get_analytic_hernquist_density_of_states(E, m_tot, a)
    # out = np.array([quad(integrad, Es[i], Es[i+1])[0] for i in range(len(Es)-1)]) / np.diff(Es) * 1e5

    integrad = lambda E: get_analytic_hernquist_df(E, md_tot, a) * get_analytic_hernquist_density_of_states(E, md_tot, a)
    out_d = np.array([quad(integrad, Es[i], Es[i+1])[0] for i in range(len(Es)-1)]) / np.diff(Es)

    # out = np.array([quad(integrad, Es[0]+1, Es[i+1])[0] for i in range(len(Es)-1)])

    Es = 0.5 * (Es[:-1] + Es[1:])
    for ax in axs:
        # ax.errorbar(Es * 1e-5, out, c=blue(0.99), ls='--', zorder=1)
        ax.errorbar(Es / EhDM, np.log10(out_d * EhDM / Mdm), c=blue(0.99), ls='--', zorder=1)

    Bbox_top = ax01.get_position()
    Bbox_bot = ax21.get_position()

    width = 0.07

    cax = plt.axes([Bbox_bot.x1, Bbox_bot.y0, (Bbox_bot.x1 - Bbox_top.x0) * width, Bbox_top.y1 - Bbox_bot.y0])

    x_ = np.linspace(1e-3, 1-1e-3, len(times))
    x, _ = np.meshgrid(x_, np.array([0, 1]))
    plt.imshow(x.T, cmap=red, aspect = 3 * 2 / len(times) / width)
    cax.yaxis.tick_left()
    cax.set_yticks(np.arange(0, len(times)))
    cax.set_yticklabels([])
    cax.set_xticks([])
    cax.set_xticklabels([])
    plt.gca().invert_yaxis()

    cax2 = plt.axes([Bbox_bot.x1 + (Bbox_bot.x1 - Bbox_top.x0) * width, Bbox_bot.y0,
                     (Bbox_bot.x1 - Bbox_top.x0) * width, Bbox_top.y1 - Bbox_bot.y0])

    x_ = np.linspace(1e-3, 1-1e-3, len(times))
    x, _ = np.meshgrid(x_, np.array([0, 1]))
    plt.imshow(x.T, cmap=blue, aspect = 3 * 2 / len(times) / width)
    cax2.yaxis.set_label_position('right')
    cax2.yaxis.tick_right()
    cax2.set_yticks(np.arange(0, len(times)))
    cax2.set_yticklabels(times)
    if '_long' in galaxy_name:
        cax2.set_yticklabels(10 * times)
    cax2.set_xticks([])
    cax2.set_xticklabels([])
    cax2.set_ylabel(r'time [Gyr]')
    plt.gca().invert_yaxis()

    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return

def get_particle_energy_diff(galaxy_name, snaps, delta_next_snap, overwrite=False, save=False, remove=True):
    if os.path.exists('../galaxies/' + galaxy_name + '_seed4'):

        (EStars, DeltaEStars, EDMs, DeltaEDMs
         ) = get_particle_energy_diff(galaxy_name + '_seed0', snaps, delta_next_snap, overwrite, save)

        n_snap = np.shape(EStars)[0]
        N_star = np.shape(EStars)[1]
        N_dm   = np.shape(EDMs)[1]

        max_seed = 5
        if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

        EnergyStars = np.zeros((n_snap, N_star * max_seed))
        DeltaEnergyStars = np.zeros((n_snap, N_star * max_seed))
        EnergyDMs = np.zeros((n_snap, N_dm * max_seed))
        DeltaEnergyDMs = np.zeros((n_snap, N_dm * max_seed))

        EnergyStars[:, :N_star] = EStars
        DeltaEnergyStars[:, :N_star] = DeltaEStars
        EnergyDMs[:, :N_dm] = EDMs
        DeltaEnergyDMs[:, :N_dm] = DeltaEDMs

        for i in range(max_seed - 1):
            galaxy_name_i = galaxy_name + f'_seed{i + 1}'

            (EStars, DeltaEStars, EDMs, DeltaEDMs
             ) = get_particle_energy_diff(galaxy_name_i, snaps, delta_next_snap, overwrite, save)

            EnergyStars[:, N_star*(i+1):N_star*(i+2)] = EStars
            DeltaEnergyStars[:, N_star*(i+1):N_star*(i+2)] = DeltaEStars
            EnergyDMs[:, N_dm*(i+1):N_dm*(i+2)] = EDMs
            DeltaEnergyDMs[:, N_dm*(i+1):N_dm*(i+2)] = DeltaEDMs

        return EnergyStars, DeltaEnergyStars, EnergyDMs, DeltaEnergyDMs

    if galaxy_name[-6:] == '_seed0':
        galaxy_name = galaxy_name[:-6]
    file_name = f'../galaxies/{galaxy_name}/particle_energy_diff_d{delta_next_snap}'
    if not overwrite and os.path.isfile(file_name + '.npz'):
        # load
        ped = np.load(file_name + '.npz')
        (EnergyStars, DeltaEnergyStars, EnergyDMs, DeltaEnergyDMs) = (
            ped['arr_0'], ped['arr_1'], ped['arr_2'], ped['arr_3'])

        if remove:
            os.remove(file_name + '.npz')

    else:
        print('Calculating Energy Diffs')
        print(galaxy_name)

        (MassDM, PosDMs, VelDMs, IDDMs, PotDMs, MassStar, PosStars, VelStars, IDStars, PotStars
         ) = load_and_centre(galaxy_name, 0)

        N_star = len(MassStar)
        N_dm   = len(MassDM)

        # set up arrays
        EnergyDMs        = np.zeros((snaps + 1 - delta_next_snap, N_dm), dtype=np.float64)
        DeltaEnergyDMs   = np.zeros((snaps + 1 - delta_next_snap, N_dm), dtype=np.float64)
        EnergyStars      = np.zeros((snaps + 1 - delta_next_snap, N_star), dtype=np.float64)
        DeltaEnergyStars = np.zeros((snaps + 1 - delta_next_snap, N_star), dtype=np.float64)

        # loop through all snaps
        for i in range(snaps + 1 - delta_next_snap):
            (MassDM, PosDMs, VelDMs, IDDMs, PotDMs, MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_and_centre(galaxy_name, i)

            EDMs = PotDMs + 0.5 * np.linalg.norm(VelDMs, axis=1)**2
            EStars = PotStars + 0.5 * np.linalg.norm(VelStars, axis=1)**2

            IDStar_order = np.argsort(IDStars)
            IDDM_order = np.argsort(IDDMs)

            (MassDM_p1, PosDMs_p1, VelDMs_p1, IDDMs_p1, PotDMs_p1,
             MassStar_p1, PosStars_p1, VelStars_p1, IDStars_p1, PotStars_p1
             ) = load_and_centre(galaxy_name, i + delta_next_snap)

            EDMs_p1 = PotDMs_p1 + 0.5 * np.linalg.norm(VelDMs_p1, axis=1)**2
            EStars_p1 = PotStars_p1 + 0.5 * np.linalg.norm(VelStars_p1, axis=1)**2

            IDStar_order_p1 = np.argsort(IDStars_p1)
            IDDM_order_p1 = np.argsort(IDDMs_p1)

            # EnergyDMs[i, :] = 0.5 * (EDMs_p1[IDDM_order_p1] + EDMs[IDDM_order])
            EnergyDMs[i, :] = EDMs[IDDM_order]
            DeltaEnergyDMs[i, :] = EDMs_p1[IDDM_order_p1] - EDMs[IDDM_order]

            # EnergyStars[i, :] = 0.5*(EStars_p1[IDStar_order_p1] + EStars[IDStar_order])
            EnergyStars[i, :] = EStars[IDStar_order]
            DeltaEnergyStars[i, :] = EStars_p1[IDStar_order_p1] - EStars[IDStar_order]

        # save when done with loop
        if save:
            #actually ends up being significant size
            np.savez(file_name, EnergyStars, DeltaEnergyStars, EnergyDMs, DeltaEnergyDMs)

    return EnergyStars, DeltaEnergyStars, EnergyDMs, DeltaEnergyDMs


def get_binned_particle_energy_diff(galaxy_name, snaps, delta_next_snap, N_E_bins = 10, E_min = -3e5, N_quartiles=11,
                                    overwrite=False, save=True):

    file_name = f'../galaxies/{galaxy_name}/binned_energy_diff_d{delta_next_snap}'
    if not overwrite and os.path.isfile(file_name + '.npz'):
        # load
        ped = np.load(file_name + '.npz')
        (energy_quartiles_DM, energy_mean_DM, energy_std_DM, energy_quartiles_Star, energy_mean_Star, energy_std_Star) = (
            ped['arr_0'], ped['arr_1'], ped['arr_2'], ped['arr_3'], ped['arr_4'], ped['arr_5'])

    else:
        #seed stacking is done here:
        (EnergyStars, DeltaEnergyStars, EnergyDMs, DeltaEnergyDMs
         ) = get_particle_energy_diff(galaxy_name, snaps, delta_next_snap)

        print('Binning energy diffs')
        print(galaxy_name)

        energy_quartiles_DM = np.nan * np.zeros((N_E_bins, N_quartiles, snaps - delta_next_snap))
        energy_quartiles_Star = np.nan * np.zeros((N_E_bins, N_quartiles, snaps - delta_next_snap))

        energy_mean_DM = np.nan * np.zeros((N_E_bins, snaps - delta_next_snap))
        energy_mean_Star = np.nan * np.zeros((N_E_bins, snaps - delta_next_snap))
        energy_std_DM = np.nan * np.zeros((N_E_bins, 6, snaps - delta_next_snap))
        energy_std_Star = np.nan * np.zeros((N_E_bins, 6, snaps - delta_next_snap))

        quartiles = np.linspace(0, 1, N_quartiles)

        qts = [0.16, 0.5, 0.84]

        EnergySmooth = np.linspace(E_min, 0, N_E_bins + 1)

        for j in range(snaps - delta_next_snap):

            E_inds = np.digitize(EnergyDMs[j], bins=EnergySmooth)

            for me in range(N_E_bins):
                binned_energies = DeltaEnergyDMs[j][E_inds == me+1]
                for pq in range(N_quartiles):
                    energy_quartiles_DM[me, pq, j] = np.nanquantile(binned_energies, quartiles[pq])

                energy_std_DM[me, 0, j] = np.std(binned_energies)

                energy_std_DM[me, 2, j] = np.sqrt(np.sum(binned_energies[binned_energies > 0]**2) /
                                                  len(binned_energies[binned_energies > 0]))
                energy_std_DM[me, 3, j] = np.sqrt(np.sum(binned_energies[binned_energies < 0]**2) /
                                                  len(binned_energies[binned_energies < 0]))
                energy_std_DM[me, 4, j] = np.nanquantile(binned_energies, qts[0])
                energy_std_DM[me, 5, j] = np.nanquantile(binned_energies, qts[2])

                energy_std_DM[me, 1, j] = (energy_std_DM[me, 5, j] - energy_std_DM[me, 4, j]) / 2

                energy_mean_DM[me, j] = np.nanmean(binned_energies)

            E_inds = np.digitize(EnergyStars[j], bins=EnergySmooth)

            for me in range(N_E_bins):
                binned_energies = DeltaEnergyStars[j][E_inds == me+1]
                for pq in range(N_quartiles):
                    energy_quartiles_Star[me, pq, j] = np.nanquantile(binned_energies, quartiles[pq])

                energy_std_Star[me, 0, j] = np.std(binned_energies)

                energy_std_Star[me, 2, j] = np.sqrt(np.sum(binned_energies[binned_energies > 0]**2) /
                                                  len(binned_energies[binned_energies > 0]))
                energy_std_Star[me, 3, j] = np.sqrt(np.sum(binned_energies[binned_energies < 0]**2) /
                                                  len(binned_energies[binned_energies < 0]))
                energy_std_Star[me, 4, j] = np.nanquantile(binned_energies, qts[0])
                energy_std_Star[me, 5, j] = np.nanquantile(binned_energies, qts[2])

                energy_std_Star[me, 1, j] = (energy_std_Star[me, 5, j] - energy_std_Star[me, 4, j]) / 2

                energy_mean_Star[me, j] = np.nanmean(binned_energies)

        # save when done with loop
        if save:
            #actually ends up being significant size
            np.savez(file_name, energy_quartiles_DM, energy_mean_DM, energy_std_DM,
                     energy_quartiles_Star, energy_mean_Star, energy_std_Star)

    return energy_quartiles_DM, energy_mean_DM, energy_std_DM, energy_quartiles_Star, energy_mean_Star, energy_std_Star


def plot_energy_exchange_comparison(galaxy_name_list, snaps_list, save=False, name='', delta_next_snap=1):

    fig = plt.figure(figsize=(5*2,5*3))
    ax00 = plt.subplot(3,2,1)
    ax01 = plt.subplot(3,2,2)
    ax10 = plt.subplot(3,2,3)
    ax11 = plt.subplot(3,2,4)
    ax20 = plt.subplot(3,2,5)
    ax21 = plt.subplot(3,2,6)

    plt.subplots_adjust(wspace=0.02, hspace=0.02)

    axs = [ax00, ax01, ax10, ax11, ax20, ax21]

    for ax in axs:
        ax.set_xlim([-1.2, 0])
        ax.set_ylim([-0.12, 0.12])

        if not ax in [ax20, ax21]: ax.set_xticklabels([])
        else: ax.set_xlabel(r'$E / E_h$')
        if ax in [ax01, ax11, ax21]: ax.set_yticklabels([])
        else: ax.set_ylabel(r'$\frac{\Delta E}{\sqrt{\Delta t}} \times \frac{\sqrt{t_h}}{E_h}$')

    colors = np.array([matplotlib.colors.to_rgba('C3')] * 256)
    colors[:128, :3] = 1 - (1-colors[:128, :3]) * np.column_stack([np.linspace(0.33, 1, 128)] * 3)
    colors[128:, :3] = colors[128:, :3] * np.column_stack([np.linspace(1, 0.33, 128)] * 3)
    red = matplotlib.colors.ListedColormap(colors)

    colors = np.array([matplotlib.colors.to_rgba('C0')] * 256)
    colors[:128, :3] = 1 - (1-colors[:128, :3]) * np.column_stack([np.linspace(0.33, 1, 128)] * 3)
    colors[128:, :3] = colors[128:, :3] * np.column_stack([np.linspace(1, 0.33, 128)] * 3)
    blue = matplotlib.colors.ListedColormap(colors)

    #TODO combine times?
    # times = np.array([0,1,2,3,4,5,6,7,8,9])
    time = np.array([5])

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = get_name_kwargs(galaxy_name)

        axs[i].text(-0.4, -0.06, kwargs['mu'])
        if 'bulge' in galaxy_name:
            axs[i].text(-0.4, -0.09, 'Spherical')
        if 'disk' in galaxy_name:
            axs[i].text(-0.4, -0.09, 'Disk')

        c = 10
        V200 = 200
        GalIC_H = 0.1
        r_200 = V200 / (10 * GalIC_H)
        a = r_200 / c * np.sqrt(2 * (np.log(1 + c) - c / (1 + c)))
        fbary = 0.01
        Mdm = V200**3 / (10 * GRAV_CONST * GalIC_H) * (1 - fbary)
        EhDM = GRAV_CONST * Mdm / a
        va = 0.5 * np.sqrt(GRAV_CONST * Mdm / a)
        th = 2 * np.pi * a / va * GYR_ON_S / PC_ON_M #Gyr # = 4 * np.pi * a**3/2 / np.sqrt(GRAV_CONST * Mdm)

        sim_times = np.linspace(0, T_TOT, snaps)
        delta_t = sim_times[delta_next_snap] #Gyr
        snap = np.argmin((sim_times - time)**2)

        N_E_bins = 10
        E_min = -3e5
        N_quartiles = 11

        (energy_quartiles_DM, energy_mean_DM, energy_std_DM, energy_quartiles_Star, energy_mean_Star, energy_std_Star
         ) = get_binned_particle_energy_diff(galaxy_name, snaps, delta_next_snap, N_E_bins, E_min, N_quartiles)

        EnergyDMsSmooth = np.linspace(E_min, 0, N_E_bins + 1)
        EnergyDMsSmooth = 0.5 * (EnergyDMsSmooth[1:] + EnergyDMsSmooth[:-1])

        for pq in range(N_quartiles // 2):
            c = blue(pq / (N_quartiles - 1) * 2)
            ca = (c[0], c[1], c[2], 0.2)
            axs[i].fill_between(EnergyDMsSmooth / EhDM,
                                energy_quartiles_DM[:, pq, snap] / EhDM * np.sqrt(th) / np.sqrt(delta_t),
                                energy_quartiles_DM[:, N_quartiles - pq - 1, snap] / EhDM * np.sqrt(th) / np.sqrt(delta_t),
                                color=ca, edgecolor=c)

        pth_w = [path_effects.Stroke(linewidth=2, foreground='k'), path_effects.Normal()]
        axs[i].errorbar(EnergyDMsSmooth / EhDM, energy_quartiles_DM[:, N_quartiles // 2, snap] / EhDM * np.sqrt(th) / np.sqrt(delta_t),
                        color=blue(0.80), lw=2, path_effects=pth_w)

        #stars
        for pq in range(N_quartiles//2):
            c = red(pq / (N_quartiles-1) * 2)
            ca = (c[0], c[1], c[2], 0.2)
            axs[i].fill_between(EnergyDMsSmooth / EhDM,
                                energy_quartiles_Star[:, pq, snap] / EhDM * np.sqrt(th) / np.sqrt(delta_t),
                                energy_quartiles_Star[:, N_quartiles-pq-1, snap] / EhDM * np.sqrt(th) / np.sqrt(delta_t),
                                color=ca, edgecolor=c)

        axs[i].errorbar(EnergyDMsSmooth / EhDM, energy_quartiles_Star[:, N_quartiles//2, snap] / EhDM * np.sqrt(th) / np.sqrt(delta_t),
                        color=red(0.80), lw=2, path_effects=pth_w)

    for ax in axs:
        ax.errorbar([-1.2,0],[0,0],c='k',ls=':')

    # dEf  = (E[:-1, :]  - E[1:, :])  / E[:-1, :]
    #
    # n =  101#101
    # contour_fracs = np.linspace(0,1,n)
    # contour_fracs[0]  = 0.01
    # contour_fracs[-1] = 0.99
    #
    # dEf_cont  = np.zeros((snaps-1, n))
    #
    # for snap in range(snaps-1):
    #     for i, c in enumerate(contour_fracs):
    #         dEf_cont[snap, i]  = np.quantile(dEf[snap],  c)
    #
    # ax.fill_between(t, dEf_cont[:, i],  dEf_cont[:, n-i-1],  color=c)


    # Bbox_top = ax01.get_position()
    # Bbox_bot = ax21.get_position()
    #
    # width = 0.07
    #
    # cax = plt.axes([Bbox_bot.x1, Bbox_bot.y0, (Bbox_bot.x1 - Bbox_top.x0) * width, Bbox_top.y1 - Bbox_bot.y0])
    #
    # x_ = np.linspace(1e-3, 1-1e-3, len(times))
    # x, _ = np.meshgrid(x_, np.array([0, 1]))
    # plt.imshow(x.T, cmap=red, aspect = 3 * 2 / len(times) / width)
    # cax.yaxis.tick_left()
    # cax.set_yticks(np.arange(0, len(times)))
    # cax.set_yticklabels([])
    # cax.set_xticks([])
    # cax.set_xticklabels([])
    # plt.gca().invert_yaxis()
    #
    # cax2 = plt.axes([Bbox_bot.x1 + (Bbox_bot.x1 - Bbox_top.x0) * width, Bbox_bot.y0,
    #                  (Bbox_bot.x1 - Bbox_top.x0) * width, Bbox_top.y1 - Bbox_bot.y0])
    #
    # x_ = np.linspace(1e-3, 1-1e-3, len(times))
    # x, _ = np.meshgrid(x_, np.array([0, 1]))
    # plt.imshow(x.T, cmap=blue, aspect = 3 * 2 / len(times) / width)
    # cax2.yaxis.set_label_position('right')
    # cax2.yaxis.tick_right()
    # cax2.set_yticks(np.arange(0, len(times)))
    # cax2.set_yticklabels(times)
    # cax2.set_xticks([])
    # cax2.set_xticklabels([])
    # cax2.set_ylabel(r'time [Gyr]')
    # plt.gca().invert_yaxis()

    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return


def plot_energy_exchange_evolution(galaxy_name_list, snaps_list, save=False, name='', delta_next_snaps=[1,2,4,8]):#[1,]

    fig = plt.figure(figsize=(10*1,5*3))
    ax0 = plt.subplot(3,1,1)
    ax1 = plt.subplot(3,1,2)
    ax2 = plt.subplot(3,1,3)

    plt.subplots_adjust(wspace=0.02, hspace=0.02)

    axs = [ax0, ax1, ax2]

    for ax in axs:
        ax.set_xlim([0, T_TOT / 0.9258556])

    ax0.set_ylim([-0.028, 0.028])
    ax1.set_ylim([-0.028, 0.028])
    ax2.set_ylim([0, 0.06])

    ax2.set_xlabel(r'$t$ [Gyr]')
    # ax2.set_xlabel(r'$t / t_h$')
    # ax0.set_ylabel(r'$\int \frac{\Delta E}{\sqrt{\Delta t}} \times \frac{\sqrt{t_h}}{E_h} dE = $ mean')
    # ax1.set_ylabel(r'Median $\frac{\Delta E}{\sqrt{\Delta t}} \times \frac{\sqrt{t_h}}{E_h}$')
    # ax2.set_ylabel(r'Percentiles $\frac{\Delta E}{\sqrt{\Delta t}} \times \frac{\sqrt{t_h}}{E_h}$')
    ax2.set_ylabel(r'Dispersion $\frac{\Delta E}{\sqrt{\Delta t}} \times \frac{\sqrt{t_h}}{E_h}$')
    ax0.set_ylabel(r'$\int \, \frac{\Delta E}{\Delta t} \times \frac{t_h}{E_h} \, dE \, = $ mean')
    ax1.set_ylabel(r'Median $\frac{\Delta E}{\Delta t} \times \frac{t_h}{E_h}$')
    # ax2.set_ylabel(r'Percentiles $\frac{\Delta E}{\Delta t} \times \frac{t_h}{E_h}$')
    # ax2.set_ylabel(r'Dispersion $\frac{\Delta E}{\Delta t} \times \frac{t_h}{E_h}$')

    ax0.set_xticklabels([])
    ax1.set_xticklabels([])

    colors = np.array([matplotlib.colors.to_rgba('C3')] * 256)
    colors[:128, :3] = 1 - (1-colors[:128, :3]) * np.column_stack([np.linspace(0.33, 1, 128)] * 3)
    colors[128:, :3] = colors[128:, :3] * np.column_stack([np.linspace(1, 0.33, 128)] * 3)
    red = matplotlib.colors.ListedColormap(colors)

    colors = np.array([matplotlib.colors.to_rgba('C0')] * 256)
    colors[:128, :3] = 1 - (1-colors[:128, :3]) * np.column_stack([np.linspace(0.33, 1, 128)] * 3)
    colors[128:, :3] = colors[128:, :3] * np.column_stack([np.linspace(1, 0.33, 128)] * 3)
    blue = matplotlib.colors.ListedColormap(colors)

    ls = ['-', '--', '-.', ':']

    for l, delta_next_snap in enumerate(delta_next_snaps):

        for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
            kwargs = get_name_kwargs(galaxy_name)

            c = 10
            V200 = 200
            GalIC_H = 0.1
            r_200 = V200 / (10 * GalIC_H)
            a = r_200 / c * np.sqrt(2 * (np.log(1 + c) - c / (1 + c))) #kpc
            fbary = 0.01
            Mdm = V200**3 / (10 * GRAV_CONST * GalIC_H) * (1 - fbary)
            EhDM = GRAV_CONST * Mdm / a
            va = 0.5 * np.sqrt(GRAV_CONST * Mdm / a) #km / s
            th = 2 * np.pi * a / va * GYR_ON_S / PC_ON_M #Gyr # = 4 * np.pi * a**3/2 / np.sqrt(GRAV_CONST * Mdm)
            print(th)

            sim_times = np.linspace(0, T_TOT, snaps)
            delta_t = sim_times[delta_next_snap] #Gyr
            ts = (0.5 * (sim_times[:-delta_next_snap] + sim_times[delta_next_snap:]))

            #load
            _index = 3

            N_E_bins = 10
            E_min = -3e5
            N_quartiles = 11

            (energy_quartiles_DM, energy_mean_DM, energy_std_DM, energy_quartiles_Star, energy_mean_Star, energy_std_Star
             ) = get_binned_particle_energy_diff(galaxy_name, snaps, delta_next_snap, N_E_bins, E_min, N_quartiles)

            alpha = 0.5
            lwt = 3
            lw = 2
            window_div = 4
            window = snaps // window_div + 1
            poly_n = 5
            if window % 2 == 0: window += 1

            # c = matplotlib.cm.get_cmap('inferno')((i+1) / (len(galaxy_name_list)+1))
            c = blue(i / (len(galaxy_name_list) - 1))
            # y0 = energy_mean_DM[_index, :] / EhDM * np.sqrt(th) / np.sqrt(delta_t)
            # y1 = energy_quartiles_DM[_index, N_quartiles//2, :] / EhDM * np.sqrt(th) / np.sqrt(delta_t)
            y2 = energy_std_DM[_index, 0, :] / EhDM * np.sqrt(th) / np.sqrt(delta_t)
            y22 = energy_std_DM[_index, 1, :] / EhDM * np.sqrt(th) / np.sqrt(delta_t)
            y0 = energy_mean_DM[_index, :] / EhDM * th / delta_t
            y1 = energy_quartiles_DM[_index, N_quartiles//2, :] / EhDM * th / delta_t
            # y2 = energy_std_DM[_index, 0, :] / EhDM * th / delta_t
            # y22 = energy_std_DM[_index, 1, :] / EhDM * th / delta_t
            # ax0.errorbar(ts / th, y0, c=c, ls=ls[l], lw=lwt, alpha=alpha)
            ax0.errorbar(ts / th, savgol_filter(y0, window, poly_n, mode='interp'), c=c, ls=ls[l], lw=lw)
            # ax1.errorbar(ts / th, y1, c=c, ls=ls[l], lw=lwt, alpha=alpha)
            ax1.errorbar(ts / th, savgol_filter(y1, window, poly_n, mode='interp'), c=c, ls=ls[l], lw=lw)
            # ax2.errorbar(ts / th, y2, c=c, ls=ls[l], lw=lw)
            # ax2.errorbar(ts, y2, c=c, ls='--', lw=lw)
            ax2.errorbar(ts / th, savgol_filter(y2, window, poly_n, mode='interp'), c=c, ls=ls[l], lw=lw)
            # ax2.errorbar(ts / th, y22, c=c, ls=ls[l], lw=lw)
            # ax2.errorbar(ts / th, savgol_filter(y22, window, poly_n, mode='interp'), c=c, ls=ls[l], lw=lw)

            # c = matplotlib.cm.get_cmap('inferno')((i+1) / (len(galaxy_name_list)+1))
            c = red(i / (len(galaxy_name_list) - 1))
            # y0 = energy_mean_Star[_index, :] / EhDM * np.sqrt(th) / np.sqrt(delta_t)
            # y1 = energy_quartiles_Star[_index, N_quartiles//2, :] / EhDM * np.sqrt(th) / np.sqrt(delta_t)
            y2 = energy_std_Star[_index, 0, :] / EhDM * np.sqrt(th) / np.sqrt(delta_t)
            y22 = energy_std_Star[_index, 1, :] / EhDM * np.sqrt(th) / np.sqrt(delta_t)
            y0 = energy_mean_Star[_index, :] / EhDM * th / delta_t
            y1 = energy_quartiles_Star[_index, N_quartiles//2, :] / EhDM * th / delta_t
            # y2 = energy_std_Star[_index, 0, :] / EhDM * th / delta_t
            # y22 = energy_std_Star[_index, 1, :] / EhDM * th / delta_t
            # ax0.errorbar(ts / th, y0, c=c, ls=ls[l], lw=lwt, alpha=alpha)
            ax0.errorbar(ts / th, savgol_filter(y0, window, poly_n, mode='interp'), c=c, ls=ls[l], lw=lw)
            # ax1.errorbar(ts / th, y1, c=c, ls=ls[l], lw=lwt, alpha=alpha)
            ax1.errorbar(ts / th, savgol_filter(y1, window, poly_n, mode='interp'), c=c, ls=ls[l], lw=lw)
            # ax2.errorbar(ts, y2, c=c, ls=ls[l], lw=lw)
            ax2.errorbar(ts / th, savgol_filter(y2, window, poly_n, mode='interp'), c=c, ls=ls[l], lw=lw)
            # ax2.errorbar(ts / th, y22, c=c, ls=ls[l], lw=lw)
            # ax2.errorbar(ts / th, savgol_filter(y22, window, poly_n, mode='interp'), c=c, ls=ls[l], lw=lw)

    ax0.errorbar([0,T_TOT / 0.9258556],[0,0],c='k',ls=':')
    ax1.errorbar([0,T_TOT / 0.9258556],[0,0],c='k',ls=':')

    # sim_time_unit = T_TOT / 10
    # for i in range(100):
    #     ax2.axvline(0.025 * i * sim_time_unit, 0, 1, c='grey', ls='--', lw=2)
    #
    # # for i in range(100):
    # #     ax2.axvline(0.005 * i * sim_time_unit, 0, 1, c='grey', ls=':', lw=1)
    # # for i in range(100):
    # #     ax2.axvline(0.01 * i * sim_time_unit, 0, 1, c='grey', ls=':', lw=2)
    # for i in range(100):
    #     # ax2.axvline(0.02 * i * sim_time_unit, 0, 1, c='k', ls=':', lw=1)
    #     ax2.axvline(0.02 * i, 0, 1, c='k', ls=':', lw=1)

    if save:
        plt.savefig(name, bbox_inches='tight')
        # plt.close()

    return


def plot_energy_exchange_scaling(galaxy_name_list, snaps_list, save=False, name='', delta_next_snaps=[1,2,4,8]):#[1,]

    fig = plt.figure()

    # fig = plt.figure(figsize=(10*1,5*3))
    # ax0 = plt.subplot(3,1,1)
    # ax1 = plt.subplot(3,1,2)
    # ax2 = plt.subplot(3,1,3)
    #
    # plt.subplots_adjust(wspace=0.02, hspace=0.02)
    #
    # axs = [ax0, ax1, ax2]
    #
    # for ax in axs:
    #     ax.set_xlim([0, T_TOT / 0.9258556])
    #
    # ax0.set_ylim([-0.028, 0.028])
    # ax1.set_ylim([-0.028, 0.028])
    # ax2.set_ylim([0, 0.06])
    #
    # ax2.set_xlabel(r'$t$ [Gyr]')
    # # ax2.set_xlabel(r'$t / t_h$')
    # # ax0.set_ylabel(r'$\int \frac{\Delta E}{\sqrt{\Delta t}} \times \frac{\sqrt{t_h}}{E_h} dE = $ mean')
    # # ax1.set_ylabel(r'Median $\frac{\Delta E}{\sqrt{\Delta t}} \times \frac{\sqrt{t_h}}{E_h}$')
    # # ax2.set_ylabel(r'Percentiles $\frac{\Delta E}{\sqrt{\Delta t}} \times \frac{\sqrt{t_h}}{E_h}$')
    # ax2.set_ylabel(r'Dispersion $\frac{\Delta E}{\sqrt{\Delta t}} \times \frac{\sqrt{t_h}}{E_h}$')
    # ax0.set_ylabel(r'$\int \, \frac{\Delta E}{\Delta t} \times \frac{t_h}{E_h} \, dE \, = $ mean')
    # ax1.set_ylabel(r'Median $\frac{\Delta E}{\Delta t} \times \frac{t_h}{E_h}$')
    # # ax2.set_ylabel(r'Percentiles $\frac{\Delta E}{\Delta t} \times \frac{t_h}{E_h}$')
    # # ax2.set_ylabel(r'Dispersion $\frac{\Delta E}{\Delta t} \times \frac{t_h}{E_h}$')
    #
    # ax0.set_xticklabels([])
    # ax1.set_xticklabels([])

    colors = np.array([matplotlib.colors.to_rgba('C3')] * 256)
    colors[:128, :3] = 1 - (1-colors[:128, :3]) * np.column_stack([np.linspace(0.33, 1, 128)] * 3)
    colors[128:, :3] = colors[128:, :3] * np.column_stack([np.linspace(1, 0.33, 128)] * 3)
    red = matplotlib.colors.ListedColormap(colors)

    colors = np.array([matplotlib.colors.to_rgba('C0')] * 256)
    colors[:128, :3] = 1 - (1-colors[:128, :3]) * np.column_stack([np.linspace(0.33, 1, 128)] * 3)
    colors[128:, :3] = colors[128:, :3] * np.column_stack([np.linspace(1, 0.33, 128)] * 3)
    blue = matplotlib.colors.ListedColormap(colors)

    ls = ['-', '--', '-.', ':']

    for l, delta_next_snap in enumerate(delta_next_snaps):

        for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
            kwargs = get_name_kwargs(galaxy_name)

            c = 10
            V200 = 200
            GalIC_H = 0.1
            r_200 = V200 / (10 * GalIC_H)
            a = r_200 / c * np.sqrt(2 * (np.log(1 + c) - c / (1 + c))) #kpc
            fbary = 0.01
            Mdm = V200**3 / (10 * GRAV_CONST * GalIC_H) * (1 - fbary)
            EhDM = GRAV_CONST * Mdm / a
            va = 0.5 * np.sqrt(GRAV_CONST * Mdm / a) #km / s
            th = 2 * np.pi * a / va * GYR_ON_S / PC_ON_M #Gyr # = 4 * np.pi * a**3/2 / np.sqrt(GRAV_CONST * Mdm)

            sim_times = np.linspace(0, T_TOT, snaps)
            delta_t = sim_times[delta_next_snap] #Gyr
            ts = (0.5 * (sim_times[:-delta_next_snap] + sim_times[delta_next_snap:]))

            #load
            _index = 3

            N_E_bins = 10
            E_min = -3e5
            N_quartiles = 11

            (energy_quartiles_DM, energy_mean_DM, energy_std_DM, energy_quartiles_Star, energy_mean_Star, energy_std_Star
             ) = get_binned_particle_energy_diff(galaxy_name, snaps, delta_next_snap, N_E_bins, E_min, N_quartiles)

            alpha = 0.5
            lwt = 3
            lw = 2
            window_div = 4
            window = snaps // window_div + 1
            poly_n = 5
            if window % 2 == 0: window += 1

            for _index in range(N_E_bins):

                # dts = sim_times[delta_next_snap:] - sim_times[:-delta_next_snap]

                EnergySmooth = np.linspace(E_min, 0, N_E_bins + 1)
                Ehr = np.abs(EnergySmooth[_index] + EnergySmooth[_index + 1]) / 2

                # vchr = np.sqrt(Ehr) #km / s
                # rhr = 4 * GRAV_CONST * Mdm / Ehr - a #kpc
                # thr = 2 * np.pi * rhr / vchr * GYR_ON_S / PC_ON_M #Gyr

                # r = get_hernquist_circular_distance_from_energy(-Ehr, Mdm, a)
                # vc = np.sqrt(GRAV_CONST * Mdm * r / (r+a)**2)
                # thr = 2 * np.pi * r / vc

                # c = matplotlib.cm.get_cmap('inferno')((i+1) / (len(galaxy_name_list)+1))
                c = blue(i / (len(galaxy_name_list) - 1))

                y0 = energy_mean_Star[_index, :] / Ehr
                y1 = energy_quartiles_Star[_index, N_quartiles // 2, :] / Ehr
                y2 = energy_std_Star[_index, 0, :] / Ehr
                y22 = energy_std_Star[_index, 1, :] / Ehr

                x = np.log10(delta_t / th) #* np.ones(snaps - delta_next_snap)
                y = np.log10(np.mean(y2))

                plt.errorbar(x, y, c=c, marker = 'o')
                # plt.errorbar(np.log10(dts / th), np.log10(savgol_filter(y22, window, poly_n, mode='interp')),
                #              c=c, ls=ls[l], lw=lw)

    plt.plot([-3, 0], [-3, 0], ls='-', c='k')
    # plt.plot([-2, 0], [-4, 0], ls='-', c='k')
    plt.plot([-6, 0], [-3, 0], ls='-', c='k')
    plt.plot([-6, 0], [-2, 0], ls='-', c='k')
    plt.plot([-6, 0], [-4, 0], ls='-', c='k')
    # plt.plot([-6, 0], [-1.5, 0], ls='-', c='k')

    plt.xlim([-5, 0])
    plt.ylim([-2, 0])

    if save:
        plt.savefig(name, bbox_inches='tight')

    return


def plot_r_dispersion_N_profile_evolution(galaxy_name='', N=24, Nd=100, save=False, name='', valid=[0,1,2,8]):

    fig = plt.figure(figsize=(4,8))
    ax1 = plt.subplot(2,1,1)
    ax2 = plt.subplot(2,1,2)

    ax2.set_xlabel(r'$R$ [kpc]')
    ax1.set_ylabel(r'$\sigma_r$ [km/s]')
    ax2.set_ylabel(r'$\sigma_r$ [km/s]')

    ax1.set_xlim([0.5, 200])
    ax2.set_xlim([0.5, 200])

    ax1.semilogx()
    ax2.semilogx()
    ax1.set_xticklabels([])

    cmap = 'viridis'

    n = len(valid)

    for i, snap in enumerate(valid):

        (MassDM, PosDMs, VelDMs, IDDMs, PotDMs, MassStar, PosStars, VelStars, IDStars, PotStars
         ) = load_snapshot(galaxy_name, snap)

        rs = np.linalg.norm(PosStars, axis=1)
        vrs = np.sum(VelStars * PosStars / rs[:, np.newaxis], axis=1)

        args = np.argsort(rs)

        sigma_r = get_sigma_N(vrs[args], N=N)

        if n > 1: c = matplotlib.cm.get_cmap(cmap)(i / (n-1))
        else: c = matplotlib.cm.get_cmap(cmap)(0.5)
        ax1.errorbar(rs[args][N//2 : -N//2], sigma_r,
                     c=c, zorder = -snap, alpha=0.2)

        r_bins = np.logspace(-1, 2, 13)
        r_centres = 10 ** ((np.log10(r_bins[:-1]) + np.log10(r_bins[:-1])) / 2)
        stds, _, _ = binned_statistic(rs[args], vrs[args], bins=r_bins, statistic=lambda x : np.sqrt(np.mean(np.power(x,2))))
        ax1.errorbar(r_centres, stds,
                     c=c, zorder = -snap)

        rsd = np.linalg.norm(PosDMs, axis=1)
        vrsd = np.sum(VelDMs * PosDMs / rsd[:, np.newaxis], axis=1)

        argsd = np.argsort(rsd)

        sigma_rd = get_sigma_N(vrsd[argsd], N=Nd)

        ax2.errorbar(rsd[argsd][Nd//2 : -Nd//2], sigma_rd,
                     c=c, zorder = -snap, alpha=0.2)

        r_bins = np.logspace(-1, 3, 17)
        r_centres = 10 ** ((np.log10(r_bins[1:]) + np.log10(r_bins[:-1])) / 2)
        stdsd, _, _ = binned_statistic(rsd[argsd], vrsd[argsd], bins=r_bins, statistic=lambda x : np.sqrt(np.mean(np.power(x,2))) )
        ax2.errorbar(r_centres, stdsd,
                     c=c, zorder = -snap)

    ms_tot = np.sum(MassStar)
    md_tot = np.sum(MassDM)

    print(md_tot)

    c = 10
    V200 = 200
    GalIC_H = 0.1
    r_200 = V200 / (10 * GalIC_H)
    ad = r_200 / c * np.sqrt(2 * (np.log(1 + c) - c / (1 + c)))
    rd = 4

    r = np.logspace(-1, 2.5)

    rho = lambda r: get_exponential_spherical_density(r, ms_tot, rd)
    # acc = lambda r: get_analytic_spherical_exponential_potential_derivative(r, ms_tot, rd)
    acc = lambda r: get_analytic_hernquist_potential_derivative(r, md_tot, ad) + get_analytic_spherical_exponential_potential_derivative(r, ms_tot, rd)
    integrad = lambda r: rho(r) * acc(r)
    sigma_r = np.sqrt([quad(integrad, r_, 10*ad)[0] / rho(r_) for r_ in r])

    rho = lambda r: get_analytic_hernquist_density(r, md_tot, ad)
    # acc = lambda r: get_analytic_hernquist_potential_derivative(r, md_tot, ad)
    acc = lambda r: get_analytic_hernquist_potential_derivative(r, md_tot, ad) + get_analytic_spherical_exponential_potential_derivative(r, ms_tot, rd)
    integrad = lambda r: rho(r) * acc(r)
    sigma_rd = np.sqrt([quad(integrad, r_, 10*ad)[0] / rho(r_) for r_ in r])

    rho = lambda r: get_analytic_hernquist_density(r, md_tot, ad) + get_exponential_spherical_density(r, ms_tot, rd)
    acc = lambda r: get_analytic_hernquist_potential_derivative(r, md_tot, ad) + get_analytic_spherical_exponential_potential_derivative(r, ms_tot, rd)
    integrad = lambda r: rho(r) * acc(r)
    sigma_rt = np.sqrt([quad(integrad, r_, 10*ad)[0] / rho(r_) for r_ in r])

    ax1.errorbar(r, sigma_rd, c='magenta', ls='--')
    ax1.errorbar(r, sigma_r, c='orange', ls='--')
    ax1.errorbar(r, sigma_rt, c='red', ls='--')

    ax2.errorbar(r, sigma_rd, c='magenta', ls='--')
    ax2.errorbar(r, sigma_r, c='orange', ls='--')
    ax2.errorbar(r, sigma_rt, c='red', ls='--')

    for ax in [ax1, ax2]:
        Bbox = ax.get_position()

        cax = plt.axes([Bbox.x1, Bbox.y0, (Bbox.x1 - Bbox.x0) * 0.08, Bbox.y1 - Bbox.y0])

        x_ = np.linspace(1e-3, 1-1e-3, n)
        x, _ = np.meshgrid(x_, np.array([0, 1]))
        plt.imshow(x.T, cmap=cmap, aspect=2 / n / 0.08)
        cax.yaxis.set_label_position('right')
        cax.yaxis.tick_right()
        cax.set_yticks(np.arange(0, n))
        cax.set_yticklabels(valid)
        cax.set_xticks([])
        cax.set_xticklabels([])
        cax.set_ylabel(r'Snap')
        plt.gca().invert_yaxis()

    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return


def plot_all_seed_bulge_projections(save=True):
    #
    # galaxy_names = [['../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps',
    #                  '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_seed1',
    #                  '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_seed2',
    #                  '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_seed3',
    #                  '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_seed4',
    #                  '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_seed5',
    #                  '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_seed6',
    #                  '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_seed7',
    #                  '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_seed8',
    #                  '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_seed9',],
    #                 ['../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps',
    #                  '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_seed1',
    #                  '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_seed2',
    #                  '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_seed3',
    #                  '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_seed4',
    #                  '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_seed5',
    #                  '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_seed6',
    #                  '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_seed7',
    #                  '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_seed8',
    #                  '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_seed9', ],
    #                 ['../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps',
    #                  '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed1',
    #                  '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed2',
    #                  '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed3',
    #                  '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed4',
    #                  '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed5',
    #                  '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed6',
    #                  '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed7',
    #                  '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed8',
    #                  '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed9', ],
    #                 ['../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps',
    #                  '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed1',
    #                  '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed2',
    #                  '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed3',
    #                  '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed4',
    #                  '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed5',
    #                  '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed6',
    #                  '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed7',
    #                  '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed8',
    #                  '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed9', ],
    #                 ['../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps',
    #                  '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_seed1',
    #                  '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_seed2',
    #                  '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_seed3',
    #                  '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_seed4',
    #                  '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_seed5',
    #                  '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_seed6',
    #                  '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_seed7',
    #                  '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_seed8',
    #                  '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_seed9', ],
    #                 ['../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps',
    #                  '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed1',
    #                  '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed2',
    #                  '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed3',
    #                  '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed4',
    #                  '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed5',
    #                  '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed6',
    #                  '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed7',
    #                  '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed8',
    #                  '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed9', ],]

    # xs = 10
    # ys = 6

    galaxy_names = [['../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_static',
                     '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps',],
                    ['../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_static',
                     '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps', ],
                    ['../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_static',
                     '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps', ],
                    ['../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_static',
                     '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps', ],
                    ['../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_static',
                     '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps', ],]

    xs = 2
    ys = 5

    for snap in range(0, 401):

        # plt.figure(figsize=(20, 12), constrained_layout=True)
        plt.figure(figsize=(xs, ys))
        plt.subplots_adjust(wspace=0.00, hspace=0.00)

        for j, mu_list in enumerate(galaxy_names):
            for i, galaxy_name in enumerate(mu_list):

                ax = plt.subplot(ys, xs, j*xs + i +1)

                ax.set_xlim([-20, 20])
                ax.set_ylim([-20, 20])
                ax.set_aspect('equal')

                ax.set_xticklabels([])
                ax.set_yticklabels([])

                (MassDM, PosDMs, VelDMs, IDDMs, PotDMs, MassStar, PosStars, VelStars, IDStars, PotStars
                 # ) = load_snapshot(galaxy_name, snap)
                 ) = load_and_centre(galaxy_name, snap)

                # with h5.File(galaxy_name + '/data/snapshot_{0:03d}.hdf5'.format(int(snap)), 'r') as fs:
                #     PosStars = fs['PartType3/Coordinates'][:]
                #     IDStars = fs['PartType3/ParticleIDs'][:]

                IDStar_order = np.argsort(IDStars)
                # IDStar_order_order = np.argsort(IDStar_order)
                n_stars = len(IDStars)
                PosStars = PosStars[IDStar_order]

                c = matplotlib.cm.get_cmap('turbo')((np.arange(n_stars) + 1) / (n_stars + 2))
                s = 2
                ax.scatter(PosStars[:, 0], PosStars[:, 1], c=c, s=s)

        # print('saving')

        if save:
            name = '../galaxies/spherical/test_run/projections/smooth{0:03d}'.format(int(snap))
            plt.savefig(name, bbox_inches='tight')
            plt.close()
        else:
            plt.show()

        print('Done', snap)

    return

def make_movie():

    fps = 10
    frames = 400

    fname = '../galaxies/spherical/test_run/smoothmovie'

    with imageio.get_writer(fname + '.mp4', format='FFMPEG', mode='I', fps=fps) as w:
        for i in range(frames):
            w.append_data(imageio.imread('../galaxies/spherical/test_run/projections/smooth{0:03d}.png'.format(int(i))))

    return

########################################################################################################################

def get_name_kwargs(galaxy_name):
    V200 = 0
    if '50kmps' in galaxy_name:  V200 = 50
    if '100kmps' in galaxy_name: V200 = 100
    if '200kmps' in galaxy_name: V200 = 200
    if '300kmps' in galaxy_name: V200 = 300
    if '400kmps' in galaxy_name: V200 = 400

    vmarker='.'
    if '50kmps' in galaxy_name:  vmarker = 'o'
    if '100kmps' in galaxy_name: vmarker = 'D'
    if '200kmps' in galaxy_name: vmarker = 's'
    if '300kmps' in galaxy_name: vmarker = '^'
    if '400kmps' in galaxy_name: vmarker = 'P'

    mun = 0
    if 'mu_0p2' in galaxy_name:  mun = 0.2
    if 'mu_0p5' in galaxy_name:  mun = 0.5
    if 'mu_1' in galaxy_name:  mun = 1
    if 'mu_2' in galaxy_name:  mun = 2
    if 'mu_5' in galaxy_name:  mun = 5
    if 'mu_25' in galaxy_name: mun = 25

    mu = ''
    if 'mu_0p2' in galaxy_name:  mu = r'$\mu=0.2$'
    if 'mu_0p5' in galaxy_name:  mu = r'$\mu=0.5$'
    if 'mu_1' in galaxy_name:  mu = r'$\mu=1$'
    if 'mu_2' in galaxy_name:  mu = r'$\mu=2$'
    if 'mu_5' in galaxy_name:  mu = r'$\mu=5$'
    if 'mu_25' in galaxy_name: mu = r'$\mu=25$'

    # in 10^10 Msun
    mdm = 0
    if 'Mdm_4p0' in galaxy_name: mdm = 1e-6
    if 'Mdm_4p5' in galaxy_name: mdm = 10 ** (-5.5)
    if 'Mdm_5p0' in galaxy_name: mdm = 1e-5
    if 'Mdm_5p5' in galaxy_name: mdm = 10 ** (-4.5)
    if 'Mdm_6p0' in galaxy_name: mdm = 1e-4
    if 'Mdm_6p5' in galaxy_name: mdm = 10 ** (-3.5)
    if 'Mdm_7p0' in galaxy_name: mdm = 1e-3
    if 'Mdm_7p5' in galaxy_name: mdm = 10 ** (-2.5)
    if 'Mdm_8p0' in galaxy_name: mdm = 1e-2
    if 'Mdm_8p5' in galaxy_name: mdm = 10 ** (-1.5)
    if 'Mdm_9p0' in galaxy_name: mdm = 1e-1
    if 'Mdm_6' in galaxy_name: mdm = 1e-4
    if 'Mdm_7' in galaxy_name: mdm = 1e-3
    if 'Mdm_8' in galaxy_name: mdm = 1e-2

    lab = ''
    if 'Mdm_6' in galaxy_name: lab = r'$10^{6.0} $ M$_\odot$'
    if 'Mdm_7' in galaxy_name: lab = r'$10^{7.0} $ M$_\odot$'
    if 'Mdm_8' in galaxy_name: lab = r'$10^{8.0} $ M$_\odot$'
    if 'Mdm_4p0' in galaxy_name: lab = r'$10^{4.0} $ M$_\odot$'
    if 'Mdm_4p5' in galaxy_name: lab = r'$10^{4.5} $ M$_\odot$'
    if 'Mdm_5p0' in galaxy_name: lab = r'$10^{5.0} $ M$_\odot$'
    if 'Mdm_5p5' in galaxy_name: lab = r'$10^{5.5} $ M$_\odot$'
    if 'Mdm_6p0' in galaxy_name: lab = r'$10^{6.0} $ M$_\odot$'
    if 'Mdm_6p5' in galaxy_name: lab = r'$10^{6.5} $ M$_\odot$'
    if 'Mdm_7p0' in galaxy_name: lab = r'$10^{7.0} $ M$_\odot$'
    if 'Mdm_7p5' in galaxy_name: lab = r'$10^{7.5} $ M$_\odot$'
    if 'Mdm_8p0' in galaxy_name: lab = r'$10^{8.0} $ M$_\odot$'
    if 'Mdm_8p5' in galaxy_name: lab = r'$10^{8.5} $ M$_\odot$'
    if 'Mdm_9p0' in galaxy_name: lab = r'$10^{9.0} $ M$_\odot$'

    labn = ''
    if 'Mdm_6p0' in galaxy_name: labn = r'$1.8 \times 10^{6}$'
    if 'Mdm_6p5' in galaxy_name: labn = r'$5.8 \times 10^{5}$'
    if 'Mdm_7p0' in galaxy_name: labn = r'$1.8 \times 10^{5}$'
    if 'Mdm_7p5' in galaxy_name: labn = r'$5.8 \times 10^{4}$'
    if 'Mdm_8p0' in galaxy_name: labn = r'$1.8 \times 10^{4}$'

    conc = 10
    if 'c_4' in galaxy_name: conc = 4
    if 'c_7' in galaxy_name: conc = 7
    if 'c_15' in galaxy_name: conc = 15
    if 'c_25' in galaxy_name: conc = 25

    scale_height = 1
    if '2z0' in galaxy_name: scale_height = 2
    if '4z0' in galaxy_name: scale_height = 4

    # plot qunatities
    ls='-'
    if 'mu_0p2' in galaxy_name:  ls = '-'
    if 'mu_0p5' in galaxy_name:  ls = '-'
    if 'mu_1' in galaxy_name:  ls = ':'
    if 'mu_2' in galaxy_name:  ls = '-'
    if 'mu_5' in galaxy_name:  ls = '-'
    if 'mu_25' in galaxy_name: ls = '--'

    marker = '.'
    if 'mu_0p2' in galaxy_name:  marker = 'v'
    if 'mu_0p5' in galaxy_name:  marker = '^'
    if 'mu_1' in galaxy_name:  marker = 's'
    if 'mu_2' in galaxy_name:  marker = 'P'
    if 'mu_5' in galaxy_name:  marker = 'o'
    if 'mu_25' in galaxy_name: marker = 'h'

    col = 'C0'
    if 'Mdm_4p0' in galaxy_name: col = 'C8'
    if 'Mdm_4p5' in galaxy_name: col = 'C7'
    if 'Mdm_5p0' in galaxy_name: col = 'C6'
    if 'Mdm_5p5' in galaxy_name: col = 'C5'
    if 'Mdm_6p0' in galaxy_name: col = 'C4'
    if 'Mdm_6p5' in galaxy_name: col = 'C3'
    if 'Mdm_7p0' in galaxy_name: col = 'C2'
    if 'Mdm_7p5' in galaxy_name: col = 'C1'
    if 'Mdm_8p0' in galaxy_name: col = 'C0'
    if 'Mdm_8p5' in galaxy_name: col = 'C9'
    if 'Mdm_9p0' in galaxy_name: col = 'C8'
    if 'Mdm_6' in galaxy_name: col = 'C4'
    if 'Mdm_7' in galaxy_name: col = 'C2'
    if 'Mdm_8' in galaxy_name: col = 'C0'

    cmap = matplotlib.cm.get_cmap('Blues')
    if 'Mdm_4p0' in galaxy_name: cmap = matplotlib.cm.get_cmap('Purples')
    if 'Mdm_4p5' in galaxy_name: cmap = matplotlib.cm.get_cmap('Purples')
    if 'Mdm_5p0' in galaxy_name: cmap = matplotlib.cm.get_cmap('Purples')
    if 'Mdm_5p5' in galaxy_name: cmap = matplotlib.cm.get_cmap('Purples')
    # if 'Mdm_6p0' in galaxy_name: cmap = matplotlib.cm.get_cmap('Purples')
    # if 'Mdm_6p5' in galaxy_name: cmap = matplotlib.cm.get_cmap('Reds')
    # if 'Mdm_7p0' in galaxy_name: cmap = matplotlib.cm.get_cmap('Greens')
    # if 'Mdm_7p5' in galaxy_name: cmap = matplotlib.cm.get_cmap('Oranges')
    # if 'Mdm_8p0' in galaxy_name: cmap = matplotlib.cm.get_cmap('Blues')
    if 'Mdm_8p5' in galaxy_name: cmap = matplotlib.cm.get_cmap('Blues')
    if 'Mdm_9p0' in galaxy_name: cmap = matplotlib.cm.get_cmap('Blues')

    # colors = np.array([matplotlib.colors.to_rgba(col)] * 256)
    # colors[:, :3] = colors[:, :3] * np.column_stack([np.linspace(0, 1, 256)] * 3)
    # colors[:, :3] = 1 - (1-colors[:, :3]) * np.column_stack([np.linspace(0, 1, 256)] * 3)

    colors = np.array([matplotlib.colors.to_rgba(col)] * 256)
    # colors[:, :3] = colors[:, :3] / np.linalg.norm(colors[0, :3])

    colors[:128, :3] = 1 - (1-colors[:128, :3]) * np.column_stack([np.linspace(0, 1, 128)] * 3)
    colors[128:, :3] = colors[128:, :3] * np.column_stack([np.linspace(1, 0, 128)] * 3)

    cmap = matplotlib.colors.ListedColormap(colors)

    # if 'Mdm_6p0' in galaxy_name: cmap = matplotlib.cm.get_cmap('Purples')
    # if 'Mdm_6p5' in galaxy_name: cmap = matplotlib.cm.get_cmap('Reds')
    # if 'Mdm_7p0' in galaxy_name: cmap = matplotlib.cm.get_cmap('Greens')
    # if 'Mdm_7p5' in galaxy_name: cmap = matplotlib.cm.get_cmap('Oranges')
    # if 'Mdm_8p0' in galaxy_name: cmap = matplotlib.cm.get_cmap('Blues')

    yh = 0
    if 'Mdm_6p0' in galaxy_name: yh = -1.2
    if 'Mdm_6p5' in galaxy_name: yh = -1.0
    if 'Mdm_7p0' in galaxy_name: yh = -0.8
    if 'Mdm_7p5' in galaxy_name: yh = -0.6
    if 'Mdm_8p0' in galaxy_name: yh = -0.4

    # devisible by 48
    combine_jzonc_bins = 1
    if 'Mdm_8p0' in galaxy_name and 'mu_1' in galaxy_name: combine_jzonc_bins = 8
    if 'Mdm_8p0' in galaxy_name and 'mu_5' in galaxy_name: combine_jzonc_bins = 4  # 6
    if 'Mdm_8p0' in galaxy_name and 'mu_25' in galaxy_name: combine_jzonc_bins = 4
    if 'Mdm_7p5' in galaxy_name and 'mu_1' in galaxy_name: combine_jzonc_bins = 6
    if 'Mdm_7p5' in galaxy_name and 'mu_5' in galaxy_name: combine_jzonc_bins = 4  # 4
    if 'Mdm_7p5' in galaxy_name and 'mu_25' in galaxy_name: combine_jzonc_bins = 2
    if 'Mdm_7p0' in galaxy_name and 'mu_5' in galaxy_name: combine_jzonc_bins = 3
    if 'Mdm_6p5' in galaxy_name and 'mu_5' in galaxy_name: combine_jzonc_bins = 2

    # devisible by 200
    combine_vphi_bins = 1
    if 'Mdm_8p0' in galaxy_name and 'mu_1' in galaxy_name: combine_vphi_bins = 20
    if 'Mdm_8p0' in galaxy_name and 'mu_5' in galaxy_name: combine_vphi_bins = 8  # 10
    if 'Mdm_8p0' in galaxy_name and 'mu_25' in galaxy_name: combine_vphi_bins = 5
    if 'Mdm_7p5' in galaxy_name and 'mu_1' in galaxy_name: combine_vphi_bins = 10
    if 'Mdm_7p5' in galaxy_name and 'mu_5' in galaxy_name: combine_vphi_bins = 4  # 5
    if 'Mdm_7p5' in galaxy_name and 'mu_25' in galaxy_name: combine_vphi_bins = 4
    if 'Mdm_7p0' in galaxy_name and 'mu_1' in galaxy_name: combine_vphi_bins = 4
    if 'Mdm_7p0' in galaxy_name and 'mu_5' in galaxy_name: combine_vphi_bins = 1  # 2

    return (
    {'ls': ls, 'c': col, 'marker': marker, 'vmarker': vmarker, 'lab': lab, 'labn': labn, 'yh': yh, 'mu': mu, 'mun': mun,
     'mdm': mdm, 'cmap': cmap,
     'jzbins': combine_jzonc_bins, 'vphibins': combine_vphi_bins, 'V200': V200, 'conc': conc,
     'scale_height': scale_height})

########################################################################################################################

def get_spherical_dispersion_profiles(PosStars, VelStars, bin_edges, cylindrical=False):

    (r, p, t, v_r, v_p, v_t) = get_spherical_coords(PosStars, VelStars)

    if cylindrical: r = np.sqrt(PosStars[:, 0] ** 2 + PosStars[:, 1] ** 2)

    number_of_bins = len(bin_edges) // 2

    # set up arrays
    sigma_r2 = np.zeros(number_of_bins, dtype=np.float64)
    sigma_p2 = np.zeros(number_of_bins, dtype=np.float64)
    sigma_t2 = np.zeros(number_of_bins, dtype=np.float64)

    # faster than calling binned statistic a bunch
    for i in range(number_of_bins):
        mask = np.logical_and((r > bin_edges[2 * i]), (r < bin_edges[2 * i + 1]))
        bin_N_i = 1 / np.sum(mask)

        # these are the squared values
        sigma_r2[i] = np.sum(np.square(v_r[mask])) * bin_N_i
        sigma_p2[i] = np.sum(np.square(v_p[mask])) * bin_N_i
        sigma_t2[i] = np.sum(np.square(v_t[mask])) * bin_N_i

    sigma_r = np.sqrt(sigma_r2)
    sigma_p = np.sqrt(sigma_p2)
    sigma_t = np.sqrt(sigma_t2)

    return sigma_r, sigma_p, sigma_t


def get_spherical_dispersions(galaxy_name, save_name_append, snaps, bin_edges,
                              cylindrical=False, overwrite=False, save=True):
    '''Read or load galaxies, calculate velocity dispersions and then save them.
    '''
    if os.path.exists('../galaxies/' + galaxy_name + '_seed4'):
        max_seed = 5
        if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

        sigma_r_list = []
        sigma_p_list = []
        sigma_t_list = []

        for i in range(max_seed):
            galaxy_name_i = galaxy_name + '_seed' + str(i)
            (sigma_r, sigma_p, sigma_t
             ) = get_spherical_dispersions(galaxy_name_i, save_name_append, snaps, bin_edges, cylindrical, overwrite, save)

            sigma_r_list.append(sigma_r)
            sigma_p_list.append(sigma_p)
            sigma_t_list.append(sigma_t)

        sigma_r_list = np.array(sigma_r_list)
        sigma_p_list = np.array(sigma_p_list)
        sigma_t_list = np.array(sigma_t_list)

        sigma_r = np.nanmedian(sigma_r_list, axis=0)
        sigma_p = np.nanmedian(sigma_p_list, axis=0)
        sigma_t = np.nanmedian(sigma_t_list, axis=0)

        return sigma_r, sigma_p, sigma_t

    if galaxy_name[-6:] == '_seed0':
        galaxy_name = galaxy_name[:-6]
    file_name = '../galaxies/' + galaxy_name + '/spherical_dispersion_profile_' + save_name_append
    if cylindrical: file_name += '_cylindrical'
    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        (sigma_r, sigma_p, sigma_t) = np.load(file_name + '.npy')

    else:
        print('Calculating Dispersions')
        print(galaxy_name)
        print(bin_edges)

        # set up arrays
        n_dim = len(bin_edges) // 2
        sigma_r = np.zeros((snaps + 1, n_dim), dtype=np.float64)
        sigma_p = np.zeros((snaps + 1, n_dim), dtype=np.float64)
        sigma_t = np.zeros((snaps + 1, n_dim), dtype=np.float64)

        # loop through all snaps
        for i in range(snaps + 1):
            (MassDM, PosDMs, VelDMs, IDDMs, PotDMs, MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_and_centre(galaxy_name, i)

            # calculate
            (sigma_r[i, :], sigma_p[i, :], sigma_t[i, :]
             ) = get_spherical_dispersion_profiles(PosStars, VelStars, bin_edges, cylindrical)

        # save when done with loop
        if save:
            np.save(file_name, (sigma_r, sigma_p, sigma_t))

    return sigma_r, sigma_p, sigma_t


def get_dispersion_profiles(PosStars, VelStars, bin_edges, cylindrical=True):
    '''Calculates dipserions in 3 dimensions, binned by radius.
    Assumes mean(v_z) = 0 and mean(v_R) = 0.
    Only for equal mass particles
    '''
    # cylindrical
    R, _, _, v_R, v_phi, v_z = get_cylindrical(PosStars, VelStars)

    if not cylindrical: R = np.linalg.norm(PosStars, axis=1)

    number_of_bins = len(bin_edges) // 2

    # set up arrays
    mean_v_R = np.zeros(number_of_bins, dtype=np.float64)
    mean_v_phi = np.zeros(number_of_bins, dtype=np.float64)

    sigma_z2 = np.zeros(number_of_bins, dtype=np.float64)
    sigma_R2 = np.zeros(number_of_bins, dtype=np.float64)
    sigma_phi2 = np.zeros(number_of_bins, dtype=np.float64)

    # faster than calling binned statistic a bunch
    for i in range(number_of_bins):
        mask = np.logical_and((R > bin_edges[2 * i]), (R < bin_edges[2 * i + 1]))
        bin_N_i = 1 / np.sum(mask)

        # sigmas
        mean_v_R[i] = np.sum(v_R[mask]) * bin_N_i
        mean_v_phi[i] = np.sum(v_phi[mask]) * bin_N_i

        # these are the squared values
        sigma_z2[i] = np.sum(np.square(v_z[mask])) * bin_N_i
        sigma_R2[i] = np.sum(np.square(v_R[mask])) * bin_N_i
        sigma_phi2[i] = np.sum((v_phi[mask] - mean_v_phi[i]) ** 2) * bin_N_i

    sigma_z = np.sqrt(sigma_z2)
    sigma_R = np.sqrt(sigma_R2)
    sigma_phi = np.sqrt(sigma_phi2)

    return mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi


def get_dispersions(galaxy_name, save_name_append, snaps, bin_edges, cylindrical=True, overwrite=False, save=True):
    '''Read or load galaxies, calculate velocity dispersions and then save them.
    '''
    if os.path.exists('../galaxies/' + galaxy_name + '_seed4'):
        max_seed = 5
        if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

        mean_v_R_list = []
        mean_v_phi_list = []
        sigma_z_list = []
        sigma_R_list = []
        sigma_phi_list = []

        for i in range(max_seed):
            galaxy_name_i = galaxy_name + '_seed' + str(i)
            (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
             ) = get_dispersions(galaxy_name_i, save_name_append, snaps, bin_edges, cylindrical, overwrite, save)

            mean_v_R_list.append(mean_v_R)
            mean_v_phi_list.append(mean_v_phi)
            sigma_z_list.append(sigma_z)
            sigma_R_list.append(sigma_R)
            sigma_phi_list.append(sigma_phi)

        mean_v_R_list = np.array(mean_v_R_list)
        mean_v_phi_list = np.array(mean_v_phi_list)
        sigma_z_list = np.array(sigma_z_list)
        sigma_R_list = np.array(sigma_R_list)
        sigma_phi_list = np.array(sigma_phi_list)

        mean_v_R = np.median(mean_v_R_list, axis=0)
        mean_v_phi = np.median(mean_v_phi_list, axis=0)
        sigma_z = np.median(sigma_z_list, axis=0)
        sigma_R = np.median(sigma_R_list, axis=0)
        sigma_phi = np.median(sigma_phi_list, axis=0)

        return (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi)

    if galaxy_name[-6:] == '_seed0':
        galaxy_name = galaxy_name[:-6]
    file_name = '../galaxies/' + galaxy_name + '/dispersion_profile_' + save_name_append
    if cylindrical: file_name += '_cylindrical'
    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        try:
            (mean_v_R, mean_v_phi,
            sigma_z, sigma_R, sigma_phi
            ) = np.load(file_name + '.npy')

        #not sure but seem to have done a bad run that causes errors
        except ValueError:
            (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
             ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges,
                                 cylindrical, overwrite=True, save=save)

    else:
        print('Calculating Dispersions')
        print(galaxy_name)
        print(bin_edges)

        # set up arrays
        n_dim = len(bin_edges) // 2
        mean_v_R = np.zeros((snaps + 1, n_dim), dtype=np.float64)
        mean_v_phi = np.zeros((snaps + 1, n_dim), dtype=np.float64)
        sigma_z = np.zeros((snaps + 1, n_dim), dtype=np.float64)
        sigma_R = np.zeros((snaps + 1, n_dim), dtype=np.float64)
        sigma_phi = np.zeros((snaps + 1, n_dim), dtype=np.float64)

        # loop through all snaps
        for i in range(snaps + 1):
            snap = '{0:03d}'.format(int(i))
            # read a file
            (MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_and_centre(galaxy_name, snap, align=True)

            # calculate
            (mean_v_R[i, :], mean_v_phi[i, :],
             sigma_z[i, :], sigma_R[i, :], sigma_phi[i, :]
             ) = get_dispersion_profiles(PosStars, VelStars, bin_edges, cylindrical)

        # save when done with loop
        if save:
            np.save(file_name, (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi))

    return (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi)


def get_r_half(galaxy_name, save_name_append, snaps, bin_edges=[], overwrite=False):
    if os.path.exists('../galaxies/' + galaxy_name + '_seed4'):
        max_seed = 5
        if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

        x_list = []
        for i in range(max_seed):
            galaxy_name_i = galaxy_name + '_seed' + str(i)
            x = get_r_half(galaxy_name_i, save_name_append, snaps, bin_edges, overwrite)

            x_list.append(x)

        x_list = np.array(x_list)

        x = np.median(x_list, axis=0)

        return x

    if galaxy_name[-6:] == '_seed0':
        galaxy_name = galaxy_name[:-6]
    file_name = '../galaxies/' + galaxy_name + '/r_half' + save_name_append
    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        r_half = np.load(file_name + '.npy')

    else:
        print('Calculating r half')
        print(galaxy_name)

        r_half = np.zeros(snaps + 1)

        for i in range(snaps + 1):
            (MassDM, PosDMs, VelDMs, IDDMs, PotDMs, MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_and_centre(galaxy_name, i)

            r = np.linalg.norm(PosStars, axis=1)

            r_half[i] = np.median(r)

        # save when done with loop
        np.save(file_name, r_half)

    return r_half


def get_stacked_r_half(galaxy_name, save_name_append, snaps, bin_edges=[], overwrite=False):

    if galaxy_name[-6:] == '_seed0':
        galaxy_name = galaxy_name[:-6]

    file_name = '../galaxies/' + galaxy_name + '/r_half_stacked' + save_name_append
    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        r_half = np.load(file_name + '.npy')

    else:
        print('Calculating r half')
        print(galaxy_name)

        r_half = np.zeros(snaps + 1)

        for i in range(snaps + 1):
            (MassDM, PosDMs, VelDMs, IDDMs, PotDMs, MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_and_centre(galaxy_name, i)

            r = np.linalg.norm(PosStars, axis=1)

            if os.path.exists('../galaxies/' + galaxy_name + '_seed4'):
                max_seed = 5
                if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

                lr = len(r)
                rs = np.zeros(lr * max_seed)
                rs[:lr] = r

                for seed in range(max_seed-1):

                    (MassDM, PosDMs, VelDMs, IDDMs, PotDMs, MassStar, PosStars, VelStars, IDStars, PotStars
                     ) = load_and_centre(galaxy_name + '_seed' + str(seed + 1), i)

                    r = np.linalg.norm(PosStars, axis=1)

                    rs[lr * (seed + 1): lr * (seed + 2)] = r

                r = rs

            r_half[i] = np.median(r)

        # save when done with loop
        np.save(file_name, r_half)

    return r_half

########################################################################################################################

def plot_sigma_vs_time(galaxy_name_list, snaps_list, name='', save=False):

    _index = 0

    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(6, 24, forward=True)

    ax0 = plt.subplot(4, 1, 1)
    ax1 = plt.subplot(4, 1, 2)
    ax2 = plt.subplot(4, 1, 3)
    ax3 = plt.subplot(4, 1, 4)

    fig.subplots_adjust(hspace=0, wspace=0)

    xlim = [0, T_TOT]
    for galaxy_name in galaxy_name_list:
        if '_long' in galaxy_name:
            xlim = [0, 10*T_TOT]
    vlim = [0, 1.3]
    slim = [0, 1.1]

    for ax in [ax0, ax1, ax2, ax3]:
        ax.set_xlim(xlim)
        ax.set_ylim(slim)
        if not ax is ax3:
            ax.set_xticklabels([])

    ax0.set_ylabel(r'$\sigma_r / V_{200}$')
    ax1.set_ylabel(r'$\sigma_\phi / V_{200}$')
    ax2.set_ylabel(r'$\sigma_\theta / V_{200}$')
    ax3.set_ylabel(r'$\sigma_{1D} / V_{200}$')

    ax3.set_xlabel(r'$t$ [Gyr]')

    # ax1.set_yticks([0,0.2,0.4,0.6,0.8,1,1.2])

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = get_name_kwargs(galaxy_name)
        v200 = 200

        bin_edges = get_dex_bins([4 * -(lambertw(-0.5 / np.exp(1), -1) + 1).real])

        if 'static' in galaxy_name:
            (sigma_r, sigma_p, sigma_t
             ) = get_spherical_dispersions(galaxy_name, 'diff', snaps, bin_edges=bin_edges, overwrite=False)
        else:
            (sigma_r, sigma_p, sigma_t
            ) = get_spherical_dispersions(galaxy_name, 'diff', snaps, bin_edges=bin_edges)

        # #try doubling bin edges
        # for snap in np.arange(0, snaps)[np.isnan(sigma_r)]:
        #     print('Warning: No particles in 0.2 dex. Trying to double range.')
        #     dbl_bin_edges = get_dex_bins([4 * -(lambertw(-0.5 / np.exp(1), -1) + 1).real, 0.4])
        # 
        #     # print(bin_edges)
        #     # print(sigma_r[598], sigma_p[598], sigma_t[598])
        #     (MassDM, PosDMs, VelDMs, IDDMs, PotDMs, MassStar, PosStars, VelStars, IDStars, PotStars
        #      ) = load_and_centre(galaxy_name, snap)
        #     # print(get_spherical_dispersion_profiles(PosStars, VelStars, bin_edges))
        #     (sigma_r[snap, :], sigma_p[snap, :], sigma_t[snap, :]
        #      ) = get_spherical_dispersion_profiles(PosStars, VelStars, dbl_bin_edges)
        #     # r = np.linalg.norm(PosStars, axis=1)
        #     # print(np.sum(np.logical_and(bin_edges[0] < r, r < bin_edges[1])))

        sigma_1d = np.sqrt((sigma_r**2 + sigma_p**2 + sigma_t**2)/3)

        # time
        time = np.linspace(0, T_TOT, snaps + 1)
        if '_long' in galaxy_name:
            time = np.linspace(0, 10*T_TOT, snaps + 1)

        alpha = 0.2
        lwt = 3#6
        lw = 2
        window = snaps // 5 + 1
        if window % 2 == 0: window += 1
        poly_n = 5

        kwargs['ls'] = '-'
        kwargs['c'] = matplotlib.cm.get_cmap('inferno')((i+1) / (len(galaxy_name_list) + 1))

        # vs time
        y = sigma_r[:, _index] / v200
        ax0.errorbar(time, y,
                     c=kwargs['c'], marker='', ls='-', lw=lwt, alpha=alpha)
        try:
            ax0.errorbar(time, savgol_filter(y, window, poly_n),
                         c=kwargs['c'], marker='', lw=lw, ls=kwargs['ls'])
        except np.linalg.LinAlgError:
            ax0.errorbar(time, y,
                         c=kwargs['c'], marker='', lw=lw, ls=kwargs['ls'])

        y = sigma_p[:, _index] / v200
        ax1.errorbar(time, y,
                     c=kwargs['c'], marker='', ls='-', lw=lwt, alpha=alpha)
        try:
            ax1.errorbar(time, savgol_filter(y, window, poly_n),
                         c=kwargs['c'], marker='', lw=lw, ls=kwargs['ls'])
        except np.linalg.LinAlgError:
            ax1.errorbar(time, y,
                         c=kwargs['c'], marker='', lw=lw, ls=kwargs['ls'])

        y = sigma_t[:, _index] / v200
        ax2.errorbar(time, y,
                     c=kwargs['c'], marker='', ls='-', lw=lwt, alpha=alpha)
        try:
            ax2.errorbar(time, savgol_filter(y, window, poly_n),
                         c=kwargs['c'], marker='', lw=lw, ls=kwargs['ls'])
        except np.linalg.LinAlgError:
            ax2.errorbar(time, y,
                         c=kwargs['c'], marker='', lw=lw, ls=kwargs['ls'])

        y = sigma_1d[:, _index] / v200
        ax3.errorbar(time, y,
                     c=kwargs['c'], marker='', ls='-', lw=lwt, alpha=alpha)
        try:
            ax3.errorbar(time, savgol_filter(y, window, poly_n),
                         c=kwargs['c'], marker='', lw=lw, ls=kwargs['ls'])
        except np.linalg.LinAlgError:
            ax3.errorbar(time, y,
                         c=kwargs['c'], marker='', lw=lw, ls=kwargs['ls'])

    r = 4 * -(lambertw(-0.5 / np.exp(1), -1) + 1).real

    c = 10
    V200 = 200
    GalIC_H = 0.1
    r_200 = V200 / (10 * GalIC_H)
    a = r_200 / c * np.sqrt(2 * (np.log(1 + c) - c / (1 + c)))
    fbary = 0.01
    Mdm = V200 ** 3 / (10 * GRAV_CONST * GalIC_H) * (1 - fbary)
    EhDM = GRAV_CONST * Mdm / a

    sigma = get_analytic_hernquist_dispersion(r, Mdm, a)

    for ax in [ax0, ax1, ax2, ax3]:
        ax0.axhline(sigma / v200, 0, 1, c='k', ls=':')
        ax0.axhline(sigma / v200 * np.sqrt(2), 0, 1, c='k', ls=':')

    # # asymptote max
    # ax0.errorbar([0, T_TOT], [analytic_dispersion[_index] / v200] * 2, color='k', ls='-')
    # ax1.errorbar([0, T_TOT], [analytic_v_circ[_index] / v200] * 2, color='k', ls='-', zorder=-10)

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return


def plot_size_vs_time(galaxy_name_list, snaps_list, name='', save=False):

    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(6, 6, forward=True)

    ax0 = plt.subplot(1, 1, 1)

    fig.subplots_adjust(hspace=0, wspace=0)

    # xlim = [0, T_TOT]
    # slim = [-2.4, -1.1]

    xlim = [0, 1]
    slim = [-1.6, -1.4]

    ax0.set_xlim(xlim)
    ax0.set_ylim(slim)

    ax0.set_ylabel(r'$\log \, r_{1/2} / r_{200}$')
    ax0.set_xlabel(r'$t$ [Gyr]')

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = get_name_kwargs(galaxy_name)
        r200 = 200

        # load data
        # r_half = get_r_half(galaxy_name, 'diff', snaps)
        r_half = get_stacked_r_half(galaxy_name, 'diff', snaps)

        # time
        time = np.linspace(0, T_TOT, snaps + 1)

        alpha = 0.2
        lwt = 6
        lw = 2
        window = snaps // 5 + 1
        if window % 2 == 0: window += 1
        poly_n = 5

        kwargs['ls'] = '-'
        if 'disk' in galaxy_name:
            kwargs['ls'] = '--'
        
        # kwargs['c'] = matplotlib.cm.get_cmap('inferno')((i+1) / (len(galaxy_name_list) + 1))
        kwargs['c'] = matplotlib.cm.get_cmap('inferno')((i//2 + 1) / (len(galaxy_name_list)//2 + 1))

        # vs time
        y = np.log10(r_half / r200)
        ax0.errorbar(time, y,
                     c=kwargs['c'], marker='', ls='-', lw=lwt, alpha=alpha)
        try:
            ax0.errorbar(time, savgol_filter(y, window, poly_n),
                         c=kwargs['c'], marker='', lw=lw, ls=kwargs['ls'])
        except np.linalg.LinAlgError:
            ax0.errorbar(time, y,
                         c=kwargs['c'], marker='', lw=lw, ls=kwargs['ls'])

    # # asymptote max
    # ax0.errorbar([0, T_TOT], [analytic_dispersion[_index] / v200] * 2, color='k', ls='-')
    # ax1.errorbar([0, T_TOT], [analytic_v_circ[_index] / v200] * 2, color='k', ls='-', zorder=-10)

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return


def smooth_burn_ins(galaxy_name, save_name_append, snaps, bin_edges, BURN_IN=BURN_IN, cylindrical=False):
    window = 41
    poly = 5

    (_, mean_v_phi, sigma_z, sigma_R, sigma_phi
     ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges, cylindrical)
    (sigma_r, sigma_p, sigma_t
     ) = get_spherical_dispersions(galaxy_name, save_name_append, snaps, bin_edges, cylindrical)

    burn_max_v = np.zeros(len(bin_edges) // 2)
    burn_min_z = np.zeros(len(bin_edges) // 2)
    burn_min_R = np.zeros(len(bin_edges) // 2)
    burn_min_phi = np.zeros(len(bin_edges) // 2)
    burn_min_r = np.zeros(len(bin_edges) // 2)
    burn_min_t = np.zeros(len(bin_edges) // 2)
    # burn_min_p = np.zeros(len(bin_edges) // 2)

    for i in range(len(bin_edges) // 2):
        mean_v_phi[:, i] = savgol_filter(mean_v_phi[:, i], window, poly)
        sigma_z[:, i] = savgol_filter(sigma_z[:, i], window, poly)
        sigma_R[:, i] = savgol_filter(sigma_R[:, i], window, poly)
        sigma_phi[:, i] = savgol_filter(sigma_phi[:, i], window, poly)
        sigma_r[:, i] = savgol_filter(sigma_r[:, i], window, poly)
        sigma_t[:, i] = savgol_filter(sigma_t[:, i], window, poly)

        spline_v = InterpolatedUnivariateSpline(np.arange(len(mean_v_phi)), mean_v_phi[:, i])
        spline_z = InterpolatedUnivariateSpline(np.arange(len(sigma_z)), sigma_z[:, i])
        spline_R = InterpolatedUnivariateSpline(np.arange(len(sigma_R)), sigma_R[:, i])
        spline_phi = InterpolatedUnivariateSpline(np.arange(len(sigma_phi)), sigma_phi[:, i])
        spline_r = InterpolatedUnivariateSpline(np.arange(len(sigma_r)), sigma_r[:, i])
        spline_t = InterpolatedUnivariateSpline(np.arange(len(sigma_t)), sigma_t[:, i])

        burn_max_v[i] = spline_v(BURN_IN)
        burn_min_z[i] = spline_z(BURN_IN)
        burn_min_R[i] = spline_R(BURN_IN)
        burn_min_phi[i] = spline_phi(BURN_IN)
        burn_min_r[i] = spline_R(BURN_IN)
        burn_min_t[i] = spline_phi(BURN_IN)

    return burn_max_v, burn_min_z, burn_min_R, burn_min_phi, burn_min_r, burn_min_t


def plot_2022_heating(save_name_append='diff', save=True, name='', overwrite=False):
    galaxy_name_list = [
        '../galaxies/mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps',
        # '../galaxies/mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
        '../galaxies/mu_25/fdisk_0p01_lgMdm_7p0_V200-200kmps',
        # '../galaxies/mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
        # '../galaxies/mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps',
        '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps',
        '../galaxies/mu_25/fbulge_0p01_lgMdm_7_V200_200kmps',
    ]
    # snaps_list = [400, 400, 101, 101, 101, 400, 100]
    snaps_list = [400, 101, 400, 100]

    fig = plt.figure(constrained_layout=True)
    # fig.set_size_inches(18, 10, forward=True)
    fig_x = 3
    fig_y = 10
    fig.set_size_inches(5 * fig_x, 5 * fig_y, forward=True)
    widths = [1] * fig_x
    heights = [1] * fig_y
    spec = fig.add_gridspec(ncols=fig_x, nrows=fig_y, width_ratios=widths, height_ratios=heights)
    
    pth_w = [path_effects.Stroke(linewidth=4, foreground='white'), path_effects.Normal()]

    eps = 1e-3
    # xlims = [-eps, 0.06 - eps]
    # xlims = [-3 + eps, -0.5 - eps]
    # ylims = [-1.4 + eps, 0.1 - eps]
    xlims = [-4 + eps, -0.5 - eps]
    ylims = [-2 + eps, 0.1 - eps]

    axs = []
    for row in range(len(heights)):
        axs.append([])
        for col in range(len(widths)):
            axs[row].append(fig.add_subplot(spec[row, col]))
            axs[row][col].set_xlim(xlims)
            axs[row][col].set_ylim(ylims)
            # axs[row][col].set_aspect('equal')
            if col != 0: axs[row][col].set_yticklabels([])
            if row != len(heights) - 1:
                axs[row][col].set_xticklabels([])
            # axs[row][col].set_yticks([-2, -1, 0])

    fig.subplots_adjust(hspace=0.02, wspace=0.02)

    run_lsf = find_least_log_squares_best_fit()

    run_ls = '--'
    run_c = 'k'
    run_funcs = get_theory_velocity2_array
    # run_labels0 = r'$\sigma_\phi^2 \propto t$', r'equation (6)'
    # run_labels1 = r'$\overline{v}_\phi \propto t$', r'equation (11)'
    run_label = r'Asymptotic Heating'
    run_zorder = 10

    # (inital_velocity2_array_v, inital_velocity2_array_z, inital_velocity2_array_r, inital_velocity2_array_p
    #  ) = smooth_burn_ins(1e-2, 400, save_name_append, burn_in, bin_edges)

    galaxy_name = 'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps'
    kwargs = get_name_kwargs(galaxy_name)
    snaps = 400

    c = 10
    V200 = 200
    GalIC_H = 0.1
    r200 = V200 / (10 * GalIC_H)
    a = r200 / c * np.sqrt(2 * (np.log(1 + c) - c / (1 + c)))
    fbary = 0.01
    M = V200 ** 3 / (10 * GRAV_CONST * GalIC_H)
    Mdm = M * (1 - fbary)
    rd = 4
    Mstar = M * fbary

    bin_centres = np.array([rd * -(lambertw(-(1-0.25) / np.exp(1), -1) + 1).real,
                            rd * -(lambertw(-(1-0.50) / np.exp(1), -1) + 1).real,
                            rd * -(lambertw(-(1-0.75) / np.exp(1), -1) + 1).real])
    bin_edges = get_dex_bins(bin_centres)

    analytic_dispersion = get_analytic_hernquist_dispersion(bin_centres, M, a)
    analytic_v_circ = np.sqrt(get_analytic_hernquist_circular_velocity(bin_centres, M, a)**2 +
                              get_exponential_disk_velocity(bin_centres, Mstar, rd)**2)

    (t_c_dm, _, delta_dm, upsilon_dm, upsilon_circ
     ) = get_constants(bin_edges, save_name_append, 1e-2)

    theory_times = np.logspace(-5, 3, 1000)
    log_theory_times = np.log10(theory_times)
    fifty = len(theory_times)
    o = np.ones(fifty)
    tau_t = get_tau_array(theory_times, t_c_dm[:, np.newaxis])
    # tau_t = [get_tau_array(theory_times, t_c_dm[i]) for i in range(len(bin_edges)//2)]

    # (inital_velocity_v, inital_velocity_z, inital_velocity_r, inital_velocity_p
    #  ) = smooth_burn_ins(kwargs['mdm'], snaps, save_name_append, BURN_IN, bin_edges, galaxy_name=galaxy_name)
    inital_velocity_v = 0
    inital_velocity_z = 0
    inital_velocity_r = 0
    inital_velocity_p = 0

    test_v = []

    (ln_Lambda_ks, alphas, betas, gammas) = run_lsf

    # yaxis`
    # theory_best_v_list.append([run_funcs[j](inital_velocity_v[i]*o, 0*o, #analytic_v_circ[i] - inital_velocity_v[i], #0*o,
    # theory_best_v_list.append([run_funcs[j](analytic_v_circ[i] * o, 0 * o,
    theory_best_v = [get_theory_velocity2_array(V200 * o, 0 * o,
                                                theory_times, t_c_dm[i] * o, delta_dm[i] * o,
                                                np.sqrt(upsilon_circ[i]) * o,
                                                ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
                               for i in range(len(bin_edges) // 2)]

    theory_best_p2 = [get_theory_velocity2_array(V200 ** 2 * o, 0 * o,
                                             theory_times, t_c_dm[i] * o, delta_dm[i] * o, upsilon_dm[i] * o,
                                             ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])
                                for i in range(len(bin_edges) // 2)]

    theory_best_z2 = [get_theory_velocity2_array(V200 ** 2 * o, 0 * o,
                                             theory_times, t_c_dm[i] * o, delta_dm[i] * o, upsilon_dm[i] * o,
                                             ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
                                for i in range(len(bin_edges) // 2)]

    theory_best_R2 = [get_theory_velocity2_array(V200 ** 2 * o, 0 * o,
                                             theory_times, t_c_dm[i] * o, delta_dm[i] * o, upsilon_dm[i] * o,
                                             ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
                                for i in range(len(bin_edges) // 2)]

    theory_best_tot2 = [(theory_best_p2[i] + theory_best_z2[i] + theory_best_R2[i]) / 3
                        for i in range(len(bin_edges) // 2)]
    theory_best_e2 = [((analytic_v_circ[i, np.newaxis] - theory_best_v[i])**2 +
                       theory_best_p2[i] + theory_best_z2[i] + theory_best_R2[i]) / 3
                      for i in range(len(bin_edges) // 2)]

    theory_best_delta = [1 - (theory_best_p2[i] + theory_best_z2[i])  / (2 * theory_best_R2[i])
                         for i in range(len(bin_edges) // 2)]

    # tau_heat_z2 = np.array([get_tau_heat_array(delta_dm[i], upsilon_dm[i],
    #                                            ln_Lambda_ks[1], alphas[1], betas[1], gammas[1]) for i in
    #                         range(len(bin_edges) // 2)])
    # tau_heat_r2 = np.array([get_tau_heat_array(delta_dm[i], upsilon_dm[i],
    #                                            ln_Lambda_ks[2], alphas[2], betas[2], gammas[2]) for i in
    #                         range(len(bin_edges) // 2)])
    tau_heat_p2 = np.array([get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                               ln_Lambda_ks[3], alphas[3], betas[3], gammas[3]) for i in
                            range(len(bin_edges) // 2)])

    tau_heat_test = tau_heat_p2 * 0.74987599

    tau_0_on_tau_heat = np.array([get_tau_zero_array(V200 * o, 0 * o,
                                                     np.sqrt(upsilon_circ[i])) for i in range(len(bin_edges) // 2)])

    test_v = [V200 * np.sqrt(upsilon_circ[i]) ** 2 * (1 - np.exp(
        - np.power(((theory_times / t_c_dm[i]) / tau_heat_test[i]) + tau_0_on_tau_heat[i], 1 + gammas[0])))
                            for i in range(len(bin_edges) // 2)]

    # plot
    for i in range(len(bin_edges) // 2):
        axs[0][i].errorbar(np.log10(tau_t[i]), np.log10(theory_best_v[i]) - np.log10(V200),
                           ls=run_ls, lw=3, c=run_c, label=run_label, path_effects=pth_w, zorder=run_zorder)
        # axs[1][i].errorbar(np.log10(tau_t[i]), np.log10(test_v[i]) - np.log10(V200),
        #                    ls=run_ls, lw=3, c=run_c, label=run_label, path_effects=pth_w, zorder=run_zorder)
        axs[1][i].errorbar(np.log10(tau_t[i]), np.log10(theory_best_p2[i]) - 2 * np.log10(V200),
                           ls=run_ls, lw=3, c=run_c, label=run_label, path_effects=pth_w, zorder=run_zorder)
        axs[2][i].errorbar(np.log10(tau_t[i]), np.log10(theory_best_z2[i]) - 2*np.log10(V200),
                           ls=run_ls, lw=3, c=run_c, label=run_label, path_effects=pth_w, zorder=run_zorder)
        axs[3][i].errorbar(np.log10(tau_t[i]), np.log10(theory_best_R2[i]) - 2*np.log10(V200),
                           ls=run_ls, lw=3, c=run_c, label=run_label, path_effects=pth_w, zorder=run_zorder)
        axs[4][i].errorbar(np.log10(tau_t[i]), np.log10(theory_best_tot2[i]) - 2*np.log10(V200),
                           ls=run_ls, lw=3, c=run_c, label=run_label, path_effects=pth_w, zorder=run_zorder)
        axs[5][i].errorbar(np.log10(tau_t[i]), np.log10(theory_best_tot2[i]) - 2*np.log10(V200),
                           ls=run_ls, lw=3, c=run_c, label=run_label, path_effects=pth_w, zorder=run_zorder)
        axs[6][i].errorbar(np.log10(tau_t[i]), np.log10(theory_best_tot2[i]) - 2*np.log10(V200),
                           ls=run_ls, lw=3, c=run_c, label=run_label, path_effects=pth_w, zorder=run_zorder)
        axs[7][i].errorbar(np.log10(tau_t[i]), np.log10(theory_best_e2[i]) - 2*np.log10(V200),
                           ls=run_ls, lw=3, c=run_c, label=run_label, path_effects=pth_w, zorder=run_zorder)
        axs[8][i].errorbar(np.log10(tau_t[i]), np.log10(theory_best_delta[i]),
                           ls=run_ls, lw=3, c=run_c, label=run_label, path_effects=pth_w, zorder=run_zorder)
        axs[9][i].errorbar(np.log10(tau_t[i]), np.log10(theory_best_delta[i]),
                           ls=run_ls, lw=3, c=run_c, label=run_label, path_effects=pth_w, zorder=run_zorder)

    test_v = analytic_v_circ[:, np.newaxis] - test_v
    theory_best_v = analytic_v_circ[:, np.newaxis] - theory_best_v

    # maximum cut off
    ls = '--'
    c = 'grey'
    alpha = 0.5
    linewidth = 5
    for i in range(len(bin_edges) // 2):
        axs[0][i].errorbar(log_theory_times, 0 * o + np.log10(analytic_v_circ[i]) - np.log10(V200),
                           # axs[1][i].errorbar(log_theory_times, 0*o + np.log10(inital_velocity_v[i]) - np.log10(V200),
                           ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
        axs[1][i].errorbar(log_theory_times, 0 * o + 2 * np.log10(analytic_dispersion[i]) - 2 * np.log10(V200),
                           ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
        axs[2][i].errorbar(log_theory_times, 0*o + 2*np.log10(analytic_dispersion[i]) - 2*np.log10(V200),
                           ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
        axs[3][i].errorbar(log_theory_times, 0*o + 2*np.log10(analytic_dispersion[i]) - 2*np.log10(V200),
                           ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
        axs[4][i].errorbar(log_theory_times, 0*o + 2*np.log10(analytic_dispersion[i]) - 2*np.log10(V200),
                           ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
        axs[5][i].errorbar(log_theory_times, 0*o + 2*np.log10(analytic_dispersion[i]) - 2*np.log10(V200),
                           ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
        axs[6][i].errorbar(log_theory_times, 0*o + 2*np.log10(analytic_dispersion[i]) - 2*np.log10(V200),
                           ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
        axs[7][i].errorbar(log_theory_times, 0*o + 2*np.log10(analytic_dispersion[i]) - 2*np.log10(V200),
                           ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
        axs[8][i].errorbar(log_theory_times, 0*o,
                           ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
        axs[9][i].errorbar(log_theory_times, 0*o,
                           ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)

    # put data on plots
    for l, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = get_name_kwargs(galaxy_name)

        #load
        (mean_v_R, mean_v_phi,
         sigma_z, sigma_R, sigma_phi
         ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges, cylindrical=False, overwrite=overwrite)

        (sigma_r, sigma_p, sigma_t
         ) = get_spherical_dispersions(galaxy_name, save_name_append, snaps, bin_edges, cylindrical=False, overwrite=overwrite)

        # work out constatns
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, kwargs['mdm'])

        mean_v_phi = mean_v_phi.T
        sigma_z2 = sigma_z.T ** 2
        sigma_R2 = sigma_R.T ** 2
        sigma_p2 = sigma_phi.T ** 2

        sigma_r2 = sigma_r.T ** 2
        # sigma_p2 = sigma_p.T ** 2
        sigma_t2 = sigma_t.T ** 2

        (inital_velocity_v, inital_velocity2_z, inital_velocity2_R, inital_velocity2_p, inital_velocity2_r, inital_velocity2_t,
         ) = smooth_burn_ins(galaxy_name, save_name_append, snaps, bin_edges, BURN_IN)
        inital_velocity2_z **= 2
        inital_velocity2_R **= 2
        inital_velocity2_p **= 2
        inital_velocity2_r **= 2
        inital_velocity2_t **= 2

        # (final_velocity_v, final_velocity2_z, final_velocity2_r, final_velocity2_p
        #  ) = smooth_burn_ins(galaxy_name, save_name_append, snaps, bin_edges, snaps - BURN_IN)
        # final_velocity2_z **= 2
        # final_velocity2_r **= 2
        # final_velocity2_p **= 2

        # inital_velocity_v = np.mean(mean_v_phi[:4], axis=0)
        # inital_velocity2_z = np.mean(sigma_z2[:4], axis=0)
        # inital_velocity2_r = np.mean(sigma_r2[:4], axis=0)
        # inital_velocity2_p = np.mean(sigma_p2[:4], axis=0)

        cs = ['C0', 'C2', 'C1', 'C3']
        lss = ['-', '-', '-.', '-.']
        mks = ['s', 's', 'o', 'o']

        for i in range(len(bin_edges) // 2):
            time = np.linspace(0, T_TOT, snaps + 1)
            time = time - time[BURN_IN]
            # time = time - time[snaps - BURN_IN]
            # time = 10**np.log10(time) #remove negative times

            tau = get_tau_array(time, t_c_dm[i])

            alpha = 1#0.25
            ms = 3

            tau_vir_mean = get_tau_heat_array(delta_dm[i], np.sqrt(upsilon_circ[i]),
                                              ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
            tau_0 = tau_vir_mean * np.log(analytic_v_circ[i] / inital_velocity_v[i])
            x = np.log10(tau + tau_0)
            y = np.log10((analytic_v_circ[i] - mean_v_phi[i]) / V200)
            # y = np.log10((inital_velocity_v[i] - mean_v_phi[i]) / V200)
            axs[0][i].errorbar(x, y, c=cs[l], marker=mks[l], ls=lss[l], alpha=alpha, ms=ms)

            tau_vir_phi = get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                         ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])
            tau_0 = tau_vir_phi * np.log(analytic_dispersion[i] ** 2 / (analytic_dispersion[i] ** 2 - inital_velocity2_p[i]))
            x = np.log10(tau + tau_0)
            y = np.log10(sigma_p2[i] / V200 ** 2)
            axs[1][i].errorbar(x, y, c=cs[l], marker=mks[l], ls=lss[l], alpha=alpha, ms=ms)

            tau_vir_z = get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                         ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
            tau_0 = tau_vir_z * np.log(analytic_dispersion[i]**2 / (analytic_dispersion[i]**2 - inital_velocity2_z[i]))
            x = np.log10(tau + tau_0)
            y = np.log10(sigma_z2[i] / V200**2)
            axs[2][i].errorbar(x, y, c=cs[l], marker=mks[l], ls=lss[l], alpha=alpha, ms=ms)

            tau_vir_R = get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                         ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
            tau_0 = tau_vir_R * np.log(analytic_dispersion[i]**2 / (analytic_dispersion[i]**2 - inital_velocity2_R[i]))
            x = np.log10(tau + tau_0)
            y = np.log10(sigma_R2[i] / V200**2)
            axs[3][i].errorbar(x, y, c=cs[l], marker=mks[l], ls=lss[l], alpha=alpha, ms=ms)


            tau_vir_tot = (tau_vir_phi + tau_vir_z + tau_vir_R) / 3

            tau_0 = tau_vir_tot * np.log(analytic_dispersion[i]**2 / (analytic_dispersion[i]**2 - inital_velocity2_r[i]))
            x = np.log10(tau + tau_0)
            y = np.log10(sigma_r2[i] / V200**2)
            axs[4][i].errorbar(x, y, c=cs[l], marker=mks[l], ls=lss[l], alpha=alpha, ms=ms)

            tau_0 = tau_vir_tot * np.log(analytic_dispersion[i]**2 / (analytic_dispersion[i]**2 - inital_velocity2_t[i]))
            x = np.log10(tau + tau_0)
            y = np.log10(sigma_t2[i] / V200**2)
            axs[5][i].errorbar(x, y, c=cs[l], marker=mks[l], ls=lss[l], alpha=alpha, ms=ms)

            initial = (inital_velocity2_p[i] + inital_velocity2_z[i] + inital_velocity2_R[i]) / 3
            tau_0 = tau_vir_tot * np.log(analytic_dispersion[i]**2 / (analytic_dispersion[i]**2 - initial))
            x = np.log10(tau + tau_0)
            y = np.log10((sigma_z2[i] + sigma_R2[i] + sigma_p2[i]) / V200**2 / 3)
            axs[6][i].errorbar(x, y, c=cs[l], marker=mks[l], ls=lss[l], alpha=alpha, ms=ms)

            # initial = ((inital_velocity_v[i])**2 +
            #            inital_velocity2_p[i] + inital_velocity2_z[i] + inital_velocity2_R[i]) / 3
            # initial_dm = (analytic_v_circ[i]**2 + 2 * analytic_dispersion[i]**2)/3
            # tau_0 = tau_vir_tot * np.log(analytic_v_circ[i]**2 / (analytic_v_circ[i]**2 - initial))
            # tau_0 = tau_vir_tot * np.log(analytic_dispersion[i]**2 / (analytic_dispersion[i]**2 - initial))
            # tau_0 = tau_vir_tot * np.log(initial_dm / (initial_dm - initial))

            initial = (inital_velocity2_p[i] + inital_velocity2_z[i] + inital_velocity2_R[i]) / 3
            tau_0 = tau_vir_tot * np.log(analytic_dispersion[i]**2 / (analytic_dispersion[i]**2 - initial))
            x = np.log10(tau + tau_0)
            # x = np.log10(tau)
            # y = np.log10((sigma_t2[i] + sigma_r2[i] + sigma_p.T[i]**2) / V200**2 / 3)
            y = np.log10((mean_v_phi[i]**2 + sigma_z2[i] + sigma_R2[i] + sigma_p2[i]) / V200**2 / 3)
            axs[7][i].errorbar(x, y, c=cs[l], marker=mks[l], ls=lss[l], alpha=alpha, ms=ms)

            initial = (inital_velocity2_p[i] + inital_velocity2_z[i] + inital_velocity2_R[i]) / 3
            tau_0 = tau_vir_tot * np.log(analytic_dispersion[i]**2 / (analytic_dispersion[i]**2 - initial))
            x = np.log10(tau + tau_0)
            y = np.log10( 1 - (sigma_t2[i] + sigma_p2[i]) / (2 * sigma_r2[i]) )
            axs[8][i].errorbar(x, y, c=cs[l], marker=mks[l], ls=lss[l], alpha=alpha, ms=ms)

            initial = (inital_velocity2_p[i] + inital_velocity2_z[i] + inital_velocity2_R[i]) / 3
            tau_0 = tau_vir_tot * np.log(analytic_dispersion[i]**2 / (analytic_dispersion[i]**2 - initial))
            x = np.log10(tau + tau_0)
            y = np.log10( 1 - (sigma_z2[i] + sigma_p2[i]) / (2 * sigma_R2[i]) )
            axs[9][i].errorbar(x, y, c=cs[l], marker=mks[l], ls=lss[l], alpha=alpha, ms=ms)

    axs[0][0].legend(loc='lower right', frameon=False,
                     handletextpad=0.5, borderpad=0.2)
    # axs[1][0].legend(loc='lower right', frameon=False,
    #                  handletextpad=0.5, borderpad=0.2)

    p1 = 0.05
    xx = xlims[0] + p1 * (xlims[1] - xlims[0])
    yy = ylims[0] + (1 - p1) * (ylims[1] - ylims[0])
    axs[0][0].text(xx, yy, r'$r = r_{1/4}$', va='top')
    axs[0][1].text(xx, yy, r'$r = r_{1/2}$', va='top')
    axs[0][2].text(xx, yy, r'$r = r_{3/4}$', va='top')

    axs[0][0].text(xx, -0.2, r'$V_c$', va='bottom')
    axs[1][0].text(xx, -0.03, r'$\sigma_{\mathrm{DM}}$', va='bottom')

    axs[0][0].set_ylabel(r'$\log \, [V_c - \overline{v}_\phi] / V_{200}$')
    axs[1][0].set_ylabel(r'$\log \, \sigma_\phi^2 / V_{200}^2$')
    axs[2][0].set_ylabel(r'$\log \, \sigma_z^2 / V_{200}^2$')
    axs[3][0].set_ylabel(r'$\log \, \sigma_R^2 / V_{200}^2$')
    axs[4][0].set_ylabel(r'$\log \, \sigma_r^2 / V_{200}^2$')
    axs[5][0].set_ylabel(r'$\log \, \sigma_\theta^2 / V_{200}^2$')
    axs[6][0].set_ylabel(r'$\log \, \sigma_{\rm total}^2 / 3 V_{200}^2$')
    axs[7][0].set_ylabel(r'$\log \, 2 T / 3 V_{200}^2$')
    axs[8][0].set_ylabel(r'$ \log \, \beta = 1 - (\sigma_\phi^2 + \sigma_\theta^2) / 2 \sigma_r^2 $')
    axs[9][0].set_ylabel(r'$ \log \, \delta = 1 - (\sigma_\phi^2 + \sigma_z^2) / 2 \sigma_R^2 $')

    # axs[3][0].set_xlabel(r'$t / t_c $')
    # axs[3][1].set_xlabel(r'$t / t_c $')
    # axs[3][2].set_xlabel(r'$t / t_c $')
    axs[9][0].set_xlabel(r'$\log \tau + \tau_0 $')
    axs[9][1].set_xlabel(r'$\log \tau + \tau_0 $')
    axs[9][2].set_xlabel(r'$\log \tau + \tau_0 $')

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    # np.seterr(all='warn')

    return


if __name__ == '__main__':

    # galaxy_name_list = [
    #                  '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_seed0',
    #                  '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_seed1',
    #                  '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_seed2',
    #                  '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_seed3',
    #                  '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_seed4',
    #                  '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_seed5',
    #                  '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_seed6',
    #                  '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_seed7',
    #                  '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_seed8',
    #                  '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_seed9',
    #                  '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_seed0',
    #                  '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_seed1',
    #                  '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_seed2',
    #                  '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_seed3',
    #                  '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_seed4',
    #                  '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_seed5',
    #                  '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_seed6',
    #                  '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_seed7',
    #                  '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_seed8',
    #                  '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_seed9',
    #                  '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed0',
    #                  '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed1',
    #                  '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed2',
    #                  '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed3',
    #                  '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed4',
    #                  '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed5',
    #                  '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed6',
    #                  '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed7',
    #                  '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed8',
    #                  '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed9',
    #                  '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed0',
    #                  '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed1',
    #                  '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed2',
    #                  '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed3',
    #                  '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed4',
    #                  '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed5',
    #                  '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed6',
    #                  '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed7',
    #                  '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed8',
    #                  '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed9',
    #                  '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_seed0',
    #                  '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_seed1',
    #                  '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_seed2',
    #                  '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_seed3',
    #                  '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_seed4',
    #                  '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_seed5',
    #                  '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_seed6',
    #                  '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_seed7',
    #                  '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_seed8',
    #                  '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_seed9',
    #                  '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed0',
    #                  '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed1',
    #                  '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed2',
    #                  '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed3',
    #                  '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed4',
    #                  '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed5',
    #                  '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed6',
    #                  '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed7',
    #                  '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed8',
    #                  '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed9', ]
    #
    # snaps_list = [400] * 10 * 6

    # galaxy_name_list = ['../galaxies/spherical/fbulge_0p01_lgMdm_8_V200_200kmps_timestep_0p005',
    #                     '../galaxies/spherical/fbulge_0p01_lgMdm_8_V200_200kmps_timestep_0p01',
    #                     '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed0',
    #                     '../galaxies/spherical/fbulge_0p01_lgMdm_8_V200_200kmps_timestep_0p04']
    # snaps_list = [400,400,400,256]

    # galaxy_name_list = [
    #                  # '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed0',
    #                  # '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed1',
    #                  # '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed2',
    #                  # '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed3',
    #                  '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed4',
    #                  '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed5',
    #                  '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed6',
    #                  '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed7',
    #                  '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed8',
    #                  '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed9',
    #     ]
    #
    #
    # galaxy_name_list = [
    #                  # '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed0',
    #                  # '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed1',
    #                  # '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed2',
    #                  # '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed3',
    #                  '../galaxies/mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed4',
    #                  '../galaxies/mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed5',
    #                  '../galaxies/mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed6',
    #                  '../galaxies/mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed7',
    #                  '../galaxies/mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed8',
    #                  '../galaxies/mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed9',
    #     ]
    #
    # snaps_list = [400] * 6

    # galaxy_name_list = ['../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps',
    #                     '../galaxies/mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps',
    #                     '../galaxies/mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps',
    #                     '../galaxies/mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps']
    # snaps_list = [400,400,400,400,400,400]

    # galaxy_name_list = ['../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps',
    #                     '../galaxies/mu_0p2/fdisk_0p01_lgMdm_8_V200_200kmps',
    #                     '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps',
    #                     '../galaxies/mu_0p5/fdisk_0p01_lgMdm_8_V200_200kmps',
    #                     '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps',
    #                     '../galaxies/mu_2/fdisk_0p01_lgMdm_8_V200_200kmps']
    # snaps_list = [400,400,400,400,400,400]

    # galaxy_name_list = ['../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps',
    #                     '../galaxies/mu_0p2/fdisk_0p01_lgMdm_8_V200_200kmps',
    #                     '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps',
    #                     '../galaxies/mu_0p5/fdisk_0p01_lgMdm_8_V200_200kmps',
    #                     '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps',
    #                     '../galaxies/mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps',
    #                     '../galaxies/mu_2/fdisk_0p01_lgMdm_8_V200_200kmps',
    #                     '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps',
    #                     '../galaxies/mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps',
    #                     '../galaxies/mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps']
    # snaps_list = [400] * 12

    # galaxy_name_list = ['../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_seed0',
    #                     '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_seed0',
    #                     '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed0',
    #                     '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed0',
    #                     '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_seed0',
    #                     '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed0']
    # snaps_list = [400,400,400,400,400,400]
    
    # galaxy_name_list = ['../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps',
    #                     '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps',
    #                     '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps',
    #                     '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps',
    #                     '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps',
    #                     '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps']
    # snaps_list = [400,400,400,400,400,400]

    # galaxy_name_list = ['../galaxies/mu_0p2/fbulge_0p01_lgMdm_7_V200_200kmps',
    #                     '../galaxies/mu_0p5/fbulge_0p01_lgMdm_7_V200_200kmps',
    #                     '../galaxies/mu_1/fbulge_0p01_lgMdm_7_V200_200kmps',
    #                     '../galaxies/mu_2/fbulge_0p01_lgMdm_7_V200_200kmps',
    #                     '../galaxies/mu_5/fbulge_0p01_lgMdm_7_V200_200kmps',
    #                     '../galaxies/mu_25/fbulge_0p01_lgMdm_7_V200_200kmps']
    # snaps_list = [100,100,100,100,100,100]

    # galaxy_name_list = ['../galaxies/mu_0p2/fdisk_0p01_lgMdm_8_V200_200kmps',
    #                     '../galaxies/mu_0p5/fdisk_0p01_lgMdm_8_V200_200kmps',
    #                     '../galaxies/mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     '../galaxies/mu_2/fdisk_0p01_lgMdm_8_V200_200kmps',
    #                     '../galaxies/mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     '../galaxies/mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps']
    # snaps_list = [400,400,400,400,400,400]

    # galaxy_name_list = ['../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_long',
    #                     '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_long',
    #                     '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_long',
    #                     '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_long',
    #                     '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_long',
    #                     '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_long'
    #                     ]
    # snaps_list = [1000,
    #               1000,
    #               1000,
    #               1000,
    #               1000,
    #               1000
    #               ]

    # galaxy_name_list = [#'../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_long',
    #                     #'../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_long',
    #                     # '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_long',
    #                     '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_long',
    #                     '../galaxies/mu_2/fdisk_0p01_lgMdm_8_V200_200kmps_long',
    #                     # '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_long',
    #                     # '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_long'
    #                    ]
    # snaps_list = [#1000,
    #               #1000,1000,
    #               1000,1000,#1000
    #              ]

    save = True

    # valid = [0, 10, 20, 30, 40]
    # plot_r2rho_profile_evolution(galaxy_name=galaxy_name,
    #                           save=save, name='../galaxies/test_r2rho', valid=valid)
    # plot_vc_profile_evolution(galaxy_name=galaxy_name,
    #                           save=save, name='../galaxies/test_vc', valid=[0])
    # plot_r_dispersion_N_profile_evolution(galaxy_name=galaxy_name,
    #                           save=save, name='../galaxies/test_dispersion', valid=valid)

    # plot_all_seed_bulge_projections(save=True)
    # make_movie()

    # plot_sigma_vs_time(galaxy_name_list, snaps_list, name='../galaxies/bulge_dispersion_evolution', save=save)
    # plot_size_vs_time(galaxy_name_list, snaps_list, name='../galaxies/bulge_r_evolution', save=save)

    galaxy_name_list = ['../galaxies/mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        '../galaxies/mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                        '../galaxies/mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        '../galaxies/mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
                        '../galaxies/mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps']
    snaps_list = [400,400,101,101,101]

    # plot_mass_profile_comparison(galaxy_name_list, snaps_list, save, name='../galaxies/cumulative_mass_ideal.pdf')

    # plot_surface_density_comparison(galaxy_name_list, snaps_list, save, name='../galaxies/surface_density_ideal.pdf')

    # plot_vc_profile_comparison(galaxy_name_list, snaps_list, save, name='../galaxies/circular_velocity_ideal.pdf')

    # plot_vc_profile_ICs(galaxy_name_list, snaps_list, save, name='../galaxies/vc_ICs_25')

    plot_sigma_profile_comparison(galaxy_name_list, snaps_list, save, name='../galaxies/sigma_comparison_disk_bulk')

    # plot_energy_pdf_comparison(galaxy_name_list, snaps_list, save, name='../galaxies/energy_comparison')#_hernq')

    # plot_energy_exchange_comparison(galaxy_name_list, snaps_list, save, name='../galaxies/energy_exchange_comparison',
    #                                 delta_next_snap=1)

    # plot_energy_exchange_evolution(galaxy_name_list, snaps_list, save, name='../galaxies/energy_exchange_evolution_timesteps')

    # plot_energy_exchange_scaling(galaxy_name_list, snaps_list, save, name='../galaxies/energy_exchange_evolution_timesteps')

    # snaps_list = [400,400]
    #
    # galaxy_name_list = ['../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_seed0',
    #                     '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_static',]
    # plot_sigma_vs_time(galaxy_name_list, snaps_list, name='../galaxies/bulge_dispersion_evolution', save=save)
    # galaxy_name_list = ['../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_seed0',
    #                     '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_static',]
    # plot_sigma_vs_time(galaxy_name_list, snaps_list, name='../galaxies/bulge_dispersion_evolution', save=save)
    # galaxy_name_list = ['../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed0',
    #                     '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_static',]
    # plot_sigma_vs_time(galaxy_name_list, snaps_list, name='../galaxies/bulge_dispersion_evolution', save=save)
    # galaxy_name_list = ['../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed0',
    #                     '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_static',]
    # plot_sigma_vs_time(galaxy_name_list, snaps_list, name='../galaxies/bulge_dispersion_evolution', save=save)
    # galaxy_name_list = ['../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_seed0',
    #                     '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_static',]
    # plot_sigma_vs_time(galaxy_name_list, snaps_list, name='../galaxies/bulge_dispersion_evolution', save=save)

    # plot_2022_heating('diff', save=save, name='../galaxies/heating_model_everything')
    # plot_2022_heating('diff', save=save, name='../galaxies/heating_model_no_time')

    plt.show()

    pass