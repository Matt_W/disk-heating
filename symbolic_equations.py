#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 25 12:30:18 2020

@author: matt
"""

from sympy import *
init_printing(use_unicode=True)

import numpy as np
from scipy.integrate import quad
from scipy.integrate import tplquad
from scipy.integrate import romberg
from scipy.integrate import solve_ivp
from scipy.optimize import brentq
from scipy.special import iv, kn
from scipy.special import erfinv
from scipy.special import lambertw
import scipy.stats

from skmonaco import mcquad
from skmonaco import mcimport
from skmonaco import mcmiser

import vegas

import matplotlib.pyplot as plt

import pprint

def jzonc():

  a,   v, vc, vp,    m, r, rd, R, g, e, eps,    jz, jc = symbols(
    "a v  v_c v_\phi m  r  r'  R  g  e \epsilon j_z j_c")

  #g = (-2*a*v**2 + m - sqrt(m**2 - 4*a*v**2*m)) / (2 * v**2 * r)

  # e = 1/2 * v**2 - m / (r+a)
  # eps = - m / (4 * e)

  #mult e by 2 to get rid of float problems
  e = v**2 - 2 * m / (r+a)
  eps = - m / (2 * e)

  rd = eps - a + sqrt(eps) * sqrt(2*a + eps)

  # out = simplify(rd)

  vc = sqrt(m * rd / (rd + a)**2)

  jc = rd * vc

  jz = R * vp

  print(jz/jc)

def cosh_disp():

  z, z0 = symbols("z z_0")

  # f = z**2 * cosh(z/(2*z0))**2
  f = z**2 * 1/(2 + exp(z/z0) + exp(-z/z0))

  out = integrate(f, (z, 0, oo))

  print(out)

def sech_2_int():

  z_0 = 0.1

  # f = lambda z: z**2 / np.cosh(z/(2*z_0))**2

  # out = quad(f, 0, 10)
  # print(out)
  # out = quad(f, 0, 100)
  # print(out)
  # out = quad(f, 0, 1000)
  # print(out)

  # out = 2 * 6.579736267392906 * (z_0**3)
  # print(out)

  g = lambda z: 1 / np.cosh(z/(2*z_0))**2

  out = quad(g, -z_0/2, z_0/2)
  print(out)
  out = quad(g, 0, 100)
  print(out)
  out = quad(g, 0, 1000)
  print(out)

  out = 2 * z_0
  print(out)


  z_0 = 3
  a = 0
  b = 1

  g = lambda z: 1 / np.cosh(z/(2*z_0))**2

  out = quad(g, a, b)
  print(out)

  out = (2*z_0)*np.tanh(b/(2*z_0)) - (2*z_0)*np.tanh(a/(2*z_0))
  print(out)

def non_gravitating_profile():
  GM   = 1
  svz2 = 1
  R2   = 1
  a    = 1

  f = lambda z: np.exp( GM / (svz2 * (np.sqrt(R2 + z**2) + a) ) ) -1

  out = quad(f, -1,1)
  print(out)
  out = quad(f, -4e2,4e2)
  print(out)
  out = quad(f, -1e4,1e4)
  print(out)
  out = quad(f, -1e6,1e6)
  print(out)
  out = quad(f, -1e10,1e10, limit=1000)
  print(out)
  out = quad(f, -1e15,1e15, limit=100000)
  print(out)

def trying_j_zonc():

  import matplotlib.pyplot as plt
  from mpl_toolkits.mplot3d import Axes3D
  import numpy as np
  from scipy.spatial.transform import Rotation as R
  from scipy.special import erf
  from scipy.optimize import brentq

  n=10000
  #random positions
  x = 2*np.random.rand(n, 3) - 1
  x = x[np.linalg.norm(x, axis=1) < 1]
  x = (x.T / np.linalg.norm(x, axis=1)).T

  x *= 1

  #random velocities
  v = 2*np.random.rand(n, 3) - 1
  v = v[np.linalg.norm(v, axis=1) < 1]
  v = (v.T / np.linalg.norm(v, axis=1)).T

  if len(v) > len(x):
    v = v[:len(x)]
  else:
    x = x[:len(v)]

  #give velocities thermalized velocities
  sigma_tot = 0.7
  m  = 1
  kb = 1
  T  = m / kb * sigma_tot**2 / 3

  n = len(v)
  rands = np.random.rand(n)
  maxwell_cum = lambda v, r: erf(v * np.sqrt(m / (2 * kb * T))) - np.sqrt(
    2 * m / (np.pi * kb * T)) * v * np.exp(-m * v**2 / (2 * kb * T)) - r
  v_abs = np.array([brentq(maxwell_cum, 0, 800, args=r) for r in rands])

  # plt.figure()
  # plt.hist(v_abs)

  v = (v.T * v_abs).T

  j = np.cross(x, v)

  plt.figure()
  plt.hist(j[:,2], bins=np.linspace(-1,1,21))

  # n=10000
  # #random positions
  # x = 2*np.random.rand(n, 3) - 1
  # x = x[np.linalg.norm(x, axis=1) < 1]
  # x = (x.T / np.linalg.norm(x, axis=1)).T

  # #velocity perpendicular to position facing +ve y
  # v_y = 1/np.sqrt(1+(x[:,1]/x[:,0])**2) * np.sign(x[:,1])
  # v_x = - np.sqrt(1 - v_y**2) * np.sign(x[:,0])
  # v = np.zeros((len(x), 3))
  # v[:,0] = v_x
  # v[:,1] = v_y

  # #random orientation
  # angle = 2*np.pi * np.random.rand(len(x))
  # r = R.from_rotvec((angle * x.T).T)
  # v = r.apply(v)

  # #give velocities thermalized velocities
  # sigma_tot = 0.5
  # m  = 1
  # kb = 1
  # T  = m / kb * sigma_tot**2 / 3

  # n = len(v)
  # rands = np.random.rand(n)
  # maxwell_cum = lambda v, r: erf(v * np.sqrt(m / (2 * kb * T))) - np.sqrt(
  #   2 * m / (np.pi * kb * T)) * v * np.exp(-m * v**2 / (2 * kb * T)) - r
  # v_abs = np.array([brentq(maxwell_cum, 0, 800, args=r) for r in rands])

  # # plt.figure()
  # # plt.hist(v_abs)

  # v = (v.T * v_abs).T

  # j = np.cross(x, v)

  # # fig = plt.figure()
  # # ax = fig.gca(projection='3d')
  # # ax.quiver(x[:,0], x[:,1], x[:,2], v[:,0], v[:,1], v[:,2])

  # plt.figure()
  # plt.hist(j[:,2], bins=np.linspace(-1,1,21))

def old_stuff():

  z, z0 = symbols("z z_0")
# f = z**2 * cosh(z/(2*z0))**2
  f = z * 1/(2 + exp(z/z0) + exp(-z/z0))

  out = integrate(f, (z, 0, oo))

  print(out)

  z_0 = 1
  a = 0
  b = 100

  g = lambda z: 2 * z / np.cosh(z/(2*z_0))**2

  out = quad(g, a, b)
  print(out[0])

  # out = 1.3852956059474084 * (2*z_0)**2
  out = 2*np.log(2) * (2*z_0)**2
  print(out)

  # out = (2*z_0)*np.tanh(b/(2*z_0)) - (2*z_0)*np.tanh(a/(2*z_0))
  # print(out)

  h = lambda z: z**2 / np.cosh(z/(2*z_0))**2

  out = quad(h, a, b)
  print(out[0])

  out = np.pi * np.log(8)
  print(out)

def henrquist_integrated_dispersion():

  r, a, p, m = symbols("r a p m")

  # # integrand = r**2 * (12 * r * (r+a)**3 / a**4 * ln(1 + a/r) -
  # #                     r/(r+a) * (25 + 52 * r/a + 42 * (r/a)**2 + 12 * (r/a)**3))

  # integrand = r / (r+a)**3 * (12 * r * (r+a)**3 / a**4 * ln(1 + a/r) -
  #                             r/(r+a) * (25 + 52 * r/a + 42 * (r/a)**2 + 12 * (r/a)**3))

  p = m * (a/r) / (r+a)**3

  integrand = p**2 * r**2

  out = integrate(integrand, r)
  print(out)

  # r, a, d, h, t, s, p = symbols("r a d h t s p")

  # #Hernquist 1d DM dispersion * 12 * a / (G * M)
  # s = (12 * r * (r+a)**3 / a**4 * ln(1 + a/r) -
  #       r/(r+a) * (25 + 52 * r/a + 42 * (r/a)**2 + 12 * (r/a)**3))

  # #Thin disk density * 2 * pi * d^2 / Md
  # # p = exp(-r/d)
  # p = 1 - r/d + r**2/2/d**2 - r**3/6/d**3 + r**4/24/d**4 - r**5/120/d**5

  # integrand = r * p * s

  # out = integrate(integrand, r)
  # print(out)

  # #Thick disk density * 4 * pi * h * d^2 / Md
  # p = exp(-r * cos(t) /d) * sech( r * sin(t) / h)

  # integrand = r * p * s

  # out = integrate(integrand, r)
  # print(out)

def profile_stuff():
  r, R, b, d, n = symbols("r R b d n")

  integrand = exp((R/d)**(n)) * R

  out = integrate(integrand, (R, 0, r))

  print(out)


def kepler_asymmetric_drift(epsilon):

  #j_z / j_c #fixed
  sigma_r2_on_vc2 = 1 - epsilon

  #eccentricty
  e = np.sqrt(1 - epsilon**2)

  #half radial period
  T_r_on_2 = 1

  #guiding radius
  r_dash = 1

  #peri and appo centre radius
  r_max_on_r_dash_2 = (1 + e)**2
  r_min_on_r_dash_2 = (1 - e)**2

  #mean annomaly from mean eccentricity
  M_of_E = lambda E, M : E - e * np.sin(E) - M

  #mean eccentricity from time
  E_of_t = lambda t : brentq(M_of_E, 0, np.pi, args = (t * np.pi / T_r_on_2))

  # #radius from time
  # R_of_t = lambda t : np.sqrt(r_max_on_r_dash_2 * np.cos(E_of_t(t))**2 +
  #                             r_min_on_r_dash_2 * np.sin(E_of_t(t))**2)
  #radius from time
  R_of_t = lambda t : 1 - e * np.cos(E_of_t(t))

  out = r_dash * quad(R_of_t, 0, T_r_on_2)[0] / T_r_on_2

  # return([R_of_t(t) for t in np.linspace(0, T_r_on_2, 1_000)])

  return(out)

####

def kepler_v_c2(r, args):
  GM = args[0]

  v_c2 = GM / r

  return v_c2

def kepler_potential(r, args):
  GM = args[0]

  phi = - GM / r

  return phi

####

def hernquist_v_c2(r, args):
  (GM, a) = args

  v_c2 = GM * r / (r + a)**2

  return v_c2

def hernquist_potential(r, args):
  (GM, a) = args

  phi = - GM / (r + a)

  return phi

####

def exponential_v_c2(r, args):
  (GM, r_disk) = args

  y = r / (2 * r_disk)

  if y > 1e2:
    return 0

  v_c2 = GM * r**2 / (2 * r_disk**3) * (iv(0,y) * kn(0,y) - iv(1,y) * kn(1,y))

  return v_c2

def exponential_potential(r, args):
  (GM, r_disk) = args

  y = r / (2 * r_disk)

  if y > 1e2:
    return 0

  #TODO check this
  phi = -GM * r / (2 * r_disk**2) * (iv(0,y) * kn(1,y) - iv(1,y) * kn(0,y))

  return phi

####

def hernquist_exponential_v_c2(r, args):

  h_v_c2 = hernquist_v_c2(r, args[:2])
  e_v_c2 = hernquist_v_c2(r, args[2:])

  return(h_v_c2 + e_v_c2)

def hernquist_exponential_potential(r, args):

  h_pot = hernquist_potential(r, args[:2])
  e_pot = hernquist_potential(r, args[2:])

  return(h_pot + e_pot)

####

def integrated_asymmetric_drift(epsilon, v_c2, potential, args, r_c=1, n=1_001):

  #j_z / j_c #fixed
  epsilon2 = epsilon**2

  # #fixed energy radius
  # r_c = 1

  if epsilon == 1:
    return(1, 0)

  #fixed v_c^2 at r_c
  v_c2_c = v_c2(r_c, args)
  #fixed potential^2 at r_c
  pot_c = potential(r_c, args)

  #the main equation
  # dr_dt2 = lambda r: GM * (-1/r_c + 2/r - epsilon2 * r_c / r**2)
  dr_dt2 = lambda r: v_c2_c + 2*(pot_c - potential(r, args)
                                 ) - epsilon2 * v_c2_c * r_c**2 / r**2

  dt_dr = lambda r: 1/np.sqrt(dr_dt2(r))

  #radii
  if epsilon == 0:
    r_min = 1e-3*r_c
  else:
    r_min = brentq(dr_dt2, 1e-3*r_c, r_c) + 1e-10
  r_max = brentq(dr_dt2, r_c,  1e2*r_c) - 1e-10

  print(v_c2(r_c, args), r_c, args)
  print(v_c2_c)
  print(dr_dt2(1e-3*r_c), dr_dt2(r_c), dr_dt2(1e2*r_c))

  #for some reason brentq breaks
  if r_max < r_c - 1e-10:
    r_max = brentq(dr_dt2, r_c,  1.1*r_c) - 1e-10
    if r_max < r_c - 1e-10:
      print(r_max)
      raise ValueError("Brentq went wrong!")

  #half radial period
  T_r_on_2 = quad(dt_dr, r_min, r_max)[0]
  t_span = (0, T_r_on_2)

  dr_dt = lambda r: np.sqrt(dr_dt2(r))
  sigma_r2 = quad(dr_dt, r_min, r_max)[0] / T_r_on_2

  y_0 = (r_min, )

  # positive_dr_dt = lambda t, r: np.sqrt(dr_dt2(r[0]))
  def positive_dr_dt(t, r):
    v = dr_dt2(r[0])
    # if v < 1e-5 * r_c/T_r_on_2:
    if v < 0:
      return 1e-10 * r_c/T_r_on_2
    return np.sqrt(v)

  # #technically this should be correct ....
  # R_of_t = lambda t: solve_ivp(positive_dr_dt, (0, t), y_0, method='LSODA',
  #                              vectorized=False).y[0][-1]
  # out = quad(R_of_t, 0, T_r_on_2)[0] / T_r_on_2
  # return(out, sigma_r2 / v_c2_c)

  rs = solve_ivp(positive_dr_dt, t_span, y_0, method='LSODA',
                  t_eval=np.linspace(*t_span, n), vectorized=False).y[0]

  r = np.mean(rs) / r_c

  return(r, sigma_r2 / v_c2_c)

# if __name__ == '__main__':

#   jz_on_jc = np.flip(np.linspace(0, 1, 21))

#   #output
#   h_expect_r = np.zeros(len(jz_on_jc))
#   h_dispersion_ratio = np.zeros(len(jz_on_jc))

#   he_expect_r = np.zeros(len(jz_on_jc))
#   he_dispersion_ratio = np.zeros(len(jz_on_jc))

#   e_expect_r = np.zeros(len(jz_on_jc))
#   e_dispersion_ratio = np.zeros(len(jz_on_jc))

#   #analytic kepler of comparison
#   kepler_r = np.zeros(len(jz_on_jc))

#   kepler_args = (np.pi**2, )
#   hernquist_args = (1, 34.5115)
#   he_args = (1, 34.5115, 0.01, 4.15)
#   # e_args = (0.01, 4.15)
#   e_args = (0.01, 0.001)

#   r_half = 6.88264561

#   for i, epsilon in enumerate(jz_on_jc):
#     kepler_r[i] = kepler_asymmetric_drift(epsilon)

#     # (expect_r[i], dispersion_ratio[i]
#     #  ) = integrated_asymmetric_drift(epsilon, kepler_v_c2, kepler_potential, kepler_args)

#     # (h_expect_r[i], h_dispersion_ratio[i]
#     #   ) = integrated_asymmetric_drift(
#     #     epsilon, hernquist_v_c2, hernquist_potential, hernquist_args, r_half)

#     # (he_expect_r[i], he_dispersion_ratio[i]
#     #   ) = integrated_asymmetric_drift(
#     #     epsilon, hernquist_exponential_v_c2, hernquist_exponential_potential, he_args, r_half)

#     (e_expect_r[i], e_dispersion_ratio[i]
#       ) = integrated_asymmetric_drift(
#         epsilon, exponential_v_c2, exponential_potential, e_args, r_half)

#     # print(he_expect_r[i], he_dispersion_ratio[i])

#   # pprint.pprint(he_dispersion_ratio)
#   # pprint.pprint(he_expect_r)

#   plt.figure()
#   plt.errorbar(h_dispersion_ratio,  h_expect_r,  label='Hernquist')
#   plt.errorbar(he_dispersion_ratio, he_expect_r, label='Hernquist + Exponential')
#   plt.errorbar(e_dispersion_ratio,  e_expect_r,  label='Exponential')

#   plt.errorbar(1 - jz_on_jc, kepler_r, label='Kepler')

#   c = 0.5
#   plt.errorbar(jz_on_jc, 1 + c - c*(1-jz_on_jc)**2, fmt=':', label=r'$x^2$')
#   # plt.errorbar(jz_on_jc, 1 + c*(np.sin(jz_on_jc*np.pi/2)), fmt=':')

#   plt.legend()

#   plt.xlabel(r'$\sigma_R^2 / v_c^2 $')
#   plt.ylabel(r'$\langle R \rangle / r$')

def chi_squared_tests():

  n = 100
  # theta = np.pi/2 * np.random.rand() #3*np.pi/16
  # print('angle =', theta)
  scatter_scale = 0.01

  # plt.figure()

  for theta in np.linspace(0, np.pi/2, 31):
    # print('angle =', theta)

    out = []

    for i in range(100):

      x = np.linspace(0, 1, n)
      y = np.random.normal(0, scatter_scale, n)

      x_rot = x * np.cos(theta) - y * np.sin(theta)
      y_rot = x * np.sin(theta) + y * np.cos(theta)

      y_model = np.tan(theta) * x_rot

      #
      # sigma_2 = np.sum((y_rot - np.mean(y_rot))**2) / n
      # print(sigma_2)
      # sigma_2 = np.sum((y_rot - y_model)**2) / n #* n / (n-1)
      # print(sigma_2)
      # sigma_2 = scatter_scale**2 * np.cos(theta)
      # print(sigma_2)
      sigma_2 = scatter_scale**2
      # sigma_2 = np.sqrt(n)

      chi_2 = np.sum((y_rot - y_model)**2) / sigma_2

      chi_2_reduced = chi_2 / (n-1)

      out.append(chi_2_reduced)

    plt.scatter(theta, np.mean(out))

    print('mean  =', np.mean(out))

  # plt.figure()

  # [plt.plot((x_rot[i], x_rot[i]), [y_model[i], y_rot[i]], c='k') for i in range(n)]
  # # [plt.plot((x_rot[i], x_rot[i]), [np.mean(y_rot), y_rot[i]], c='k') for i in range(n)]

  # plt.plot(x_rot, y_rot, ls='', marker='.')

  # plt.plot(x_rot, y_model)

def random_schwatzschild_df_rng_test():

  sigma_R = 20
  sigma_p = 20
  sigma_z = 20

  #v_max should be >> sigma_i
  v_max = 200

  n_gen = 1000

  v_stars = np.zeros((3, n_gen))

  i = 0

  while i < n_gen:

    #regular speed
    v_i = v_max * (2*np.random.rand(3) -1)

    like = np.exp(-0.5 * (v_i[0]**2 / (sigma_R**2) +
                          v_i[1]**2 / (sigma_p**2) +
                          v_i[2]**2 / (sigma_z**2)) )

    like_to_beat = np.random.rand()

    if like > like_to_beat:
      #accept velocities
      v_stars[:, i] = v_i
      i += 1

    # #speedup
    # v_i = (2*np.random.rand(3) -1)
    #
    # if v_i[0]**2 + v_i[1]**2 + v_i[2]**2 < 1:
    #   pass
    #
    # else:
    #   v_i[0] *= np.sqrt(2) * sigma_R
    #   v_i[1] *= np.sqrt(2) * sigma_p
    #   v_i[2] *= np.sqrt(2) * sigma_z
    #
    #   #accept velocities
    #   v_stars[:, i] = v_i
    #   i += 1

  print('should be:', sigma_R, sigma_p, sigma_z)
  print('got      :', np.std(v_stars, axis=1))

  return

def scale_height_integral_stuff():

    #scale
    z_0 = 1

    #intial guess
    c_2 = z_0**2 / 2

    #integration bouds need to be bigish
    large = 200 #z_0 * 1e2

    dm_frac = 1 - 0.01
    sigma_z_on_dm_2 = 0.01
    R_cyl = 4.15
    a = 34
    r200 = 200

    thick = lambda z: np.exp(1 / (dm_frac * sigma_z_on_dm_2) * (
            1 / (np.sqrt((R_cyl/r200)**2 + (z/r200)**2) + a/r200) - 1 / (R_cyl/r200 + a/r200) ))

    z_thin_SG = lambda z: 1/np.cosh(z/z_0)**2
    z_thin_NSG = lambda z: np.exp(-(z/z_0)**2)
    z_thick_NSG = thick

    tot = quad(z_thick_NSG, 0, large)[0]
    half = lambda z_half: quad(z_thick_NSG, 0, z_half)[0] - tot/2
    z_12_thick_NSG = brentq(half, 0, large)
    #sech
    z_12_thin_SG = z_0 * np.log(3) / 2
    # #exp
    z_12_thin_NSG = z_0 * erfinv(0.5)

    for z_distribution, z_12, name in zip([z_thin_SG, z_thin_NSG, z_thick_NSG],
                                          [z_12_thin_SG, z_12_thin_NSG, z_12_thick_NSG],
                                          ['thin_SG', 'thin_NSG', 'thick_NSG']):

        for i in range(100):

            top_i = lambda z: 1 / (1 + z**2/(2*c_2)) * z**2 * z_distribution(z)
            bot_i = lambda z: 1 / (1 + z**2/(2*c_2)) * z_distribution(z)

            top = quad(top_i, 0, large)[0]
            bot = quad(bot_i, 0, large)[0]

            c_2 = top / bot

          # print(np.sqrt(c_2))

        # print(np.sqrt(c_2 / d_2))
        print(name)
        print(f'c / z_0   = {np.sqrt(c_2) / z_0}')
        print(f'c / z_1/2 = {np.sqrt(c_2) / z_12}')
        print(f'(c/a) / (z_1/2/R_1/2) = {np.sqrt(c_2) * np.sqrt(2) / z_12}')
        print(f'1/(c/a) / (z_1/2/R_1/2) = {z_12 / np.sqrt(c_2) / np.sqrt(2)}')
        print()

    #thick
    #
    # plt.figure()
    #
    # for sigma_z_on_dm_2 in np.linspace(0.01, 0.5, 50): #[0.01,0.02,0.04,0.08,0.16,0.32,0.64]:
    #
    #   thick = lambda z: np.exp(1 / (dm_frac * sigma_z_on_dm_2) * (
    #           1 / (np.sqrt((R_cyl/r200)**2 + (z/r200)**2) + a/r200) - 1 / (R_cyl/r200 + a/r200) ))
    #
    #   tot = quad(thick, 0, large)[0]
    #   half = lambda z_half: quad(thick, 0, z_half)[0] - tot/2
    #   thick_median_z = brentq(half, 0, large)
    #   # print('thick median z =', thick_median_z)
    #
    #   # print('median / exp(1/nu) =', thick_median_z * np.exp(1/sigma_z_on_dm_2))
    #   # print('test =', thick_median_z / np.sqrt(sigma_z_on_dm_2))
    #
    #   plt.scatter(sigma_z_on_dm_2, thick_median_z)
    #
    # x = np.logspace(-2,-0.3)
    # plt.plot(x, x * R_cyl)
    # plt.plot(x, x**0.5 * R_cyl/2)
    # plt.plot(x, x**2 * thick_median_z)
    # plt.plot(x, np.exp(x) * thick_median_z)
    # plt.plot(x, np.log(x) * thick_median_z)

    # plt.show()

    mean_div_c_sech = 0.6159978007462151
    mean_div_c_exp = 0.5180308801010115

    # c = np.sqrt(c_2)
    # print('c =', c)
    #
    # sech_c = sech_median_z * 2  / np.log(3) * mean_div_c_sech
    # print('sech c approx =', sech_c, ' diff =', sech_c - c)
    # exp_c = exp_median_z / erfinv(0.5) * mean_div_c_exp
    # print('exp  c approx =', exp_c, '  diff =', exp_c - c)

    # print(erfinv(0.5) / np.log(3) * 2)
    # print(mean_div_c_exp / mean_div_c_sech)

    # print((erfinv(0.5) / np.log(3) * 2) / (mean_div_c_exp / mean_div_c_sech))

    # print((erfinv(0.5) / np.log(3) * 2) * (mean_div_c_sech / mean_div_c_exp))

    # n_zs = 100_000
    #
    # rands = np.random.rand(n_zs)
    #
    # zs = z_0 / 2 * np.log(rands / (1 - rands))
    #
    # c_monte = np.sum(1 / (1 + zs**2 / c_2) * zs**2) / np.sum(1 / (1 + zs**2 / c_2))
    #
    # print('monte carlo c:', np.sqrt(c_monte))
    #
    # print('monte carlo median:', np.median(np.abs(zs)))
    # print('analytic median:', np.log(3)/2*z_0)


    # plt.figure()
    # x = np.linspace(-10*z_0, 10*z_0, 41)
    # plt.hist(zs, x)
    # plt.plot(x, n_zs * z_distribution(x))
    #
    # plt.semilogy()
    # plt.show()

    # M_xx_int = lambda z, R: R**3 * 1 / (R**2 + z**2)

    #vegas interation?
    if False:

        R_d = 4.15

        # large = 20
        mc_neval = 4e4
        mc_err_tol = 0.005
        # mc_neval = 1e4
        # mc_err_tol = 0.02

        R_distribution = lambda x, y: np.exp(-np.sqrt(x**2 + y**2) / R_d)

        bin_edges = [5.46707974, 8.66473746]
        bin_min = 5.46707974
        bin_max = 8.66473746
        # bin_min = 5.9
        # bin_max = 6.1
        bin_min_2 = bin_min**2
        bin_max_2 = bin_max**2

        R = (bin_max+bin_min)/2

        c_2 *= 2 / R**2
        print()
        print('(c/a)^2', c_2)
        print('c/a', np.sqrt(c_2))

        volume = lambda x, y: bin_min_2 <  x**2 + y**2 < bin_max_2

        for _ in range(3):

            one_on_ellpitical_distance_2 = lambda x, y, z: 1 / (x**2 + y**2 + z**2/(c_2))

            top_zz_i = lambda x: one_on_ellpitical_distance_2(x[0],x[1],x[2]) * x[2]**2 * z_distribution(
                x[2]) * R_distribution(x[0], x[1]) * volume(x[0], x[1])
            top_xx_i = lambda x: one_on_ellpitical_distance_2(x[0],x[1],x[2]) * x[0]**2 * z_distribution(
                x[2]) * R_distribution(x[0], x[1]) * volume(x[0], x[1])
            bot_i = lambda x: one_on_ellpitical_distance_2(x[0],x[1],x[2]) * z_distribution(
                x[2]) * R_distribution(x[0], x[1]) * volume(x[0], x[1])

            while True:
                integ = vegas.Integrator([[0, np.sqrt(bin_max_2)], [0, np.sqrt(bin_max_2)], [0, large]])
                result = integ(top_zz_i, nitn=15, neval=mc_neval)  # nitn=10, neval=3e4)
                top_zz = result.mean
                error = result.sdev
                if np.divide(error, top_zz) < mc_err_tol:
                    break
                else:
                    print('Failed with')
                    print('top_zz', top_zz)
                    print('frac error top_zz', error / top_zz)

            print('top_zz', top_zz)
            print('frac error top_zz', error / top_zz)

            while True:
                integ = vegas.Integrator([[0, np.sqrt(bin_max_2)], [0, np.sqrt(bin_max_2)], [0, large]])
                result = integ(top_xx_i, nitn=15, neval=mc_neval)  # nitn=10, neval=3e4)
                top_xx = result.mean
                error = result.sdev
                if np.divide(error, top_xx) < mc_err_tol:
                    break
                else:
                    print('Failed with')
                    print('top_xx', top_xx)
                    print('frac error top_xx', error / top_xx)
            print('top_xx', top_xx)
            print('frac error top_xx', error / top_xx)

            c_2 = top_zz / top_xx

            print('(c/a)^2', c_2)
            print('c/a', np.sqrt(c_2))
            print()

        while True:
            integ = vegas.Integrator([[0, np.sqrt(bin_max_2)], [0, np.sqrt(bin_max_2)], [0, large]])
            result = integ(bot_i, nitn=15, neval=mc_neval)  # nitn=10, neval=3e4)
            bot = result.mean
            error = result.sdev
            if np.divide(error, bot) < mc_err_tol:
                break
            else:
                print('Failed with')
                print('bot', bot)
                print('frac error bot', error / bot)
        print('bot', bot)
        print('frac error bot', error / bot)

        print()
        print('M_zz =', top_zz / bot)
        print('M_xx =', top_xx / bot)
        print('sqrt M_zz =', np.sqrt(top_zz / bot))
        print('sech ideal sqrt M_zz =', sech_c)
        print('exp ideal sqrt M_zz =', exp_c)
        print('sqrt M_xx =', np.sqrt(top_xx / bot))
        print('R / sqrt(2)', R/np.sqrt(2))

        # exp_int = lambda R: np.exp(- R / R_disk ) / R
        exp_int = lambda R: R * np.exp(- R / R_d)
        # exp_int = lambda R: R**3 * np.exp(- R / R_disk )
        tot = quad(exp_int, bin_min, bin_max)[0]
        zero = lambda R: quad(exp_int, bin_min, R)[0] - 0.5 * tot
        R_mass_centre = brentq(zero, bin_min, bin_max)
        print('R / sqrt(2)', R_mass_centre/np.sqrt(2))

        R_log_centre = 10 ** ((np.log10(bin_min) + np.log10(bin_min)) / 2)
        print('R / sqrt(2)', R_log_centre/np.sqrt(2))

        print()
        print('c/a', np.sqrt(top_zz/top_xx))
        print('sech ideal c/a', sech_median_z * 2 * np.sqrt(2) * mean_div_c_sech / (R * np.log(3)))
        print('exp ideal c/a', exp_median_z * np.sqrt(2) * mean_div_c_exp / (R * erfinv(0.5)))

    return

def scale_height_integral_threeD():

  # scale
  z_0 = 4

  R_d = 4.15

  # integration bounds need to be bigish
  large = z_0 * 1e2

  dm_frac = 1 - 0.01
  sigma_z_on_dm_2 = 0.1
  R_cyl = 10
  a = 30
  r200 = 200

  # intial guess
  c_2 = (z_0 / R_d) ** 2

  # epsilon = 1e-4

  # thick = lambda z: np.exp(1 / (dm_frac * sigma_z_on_dm_2) * (
  #         1 / (np.sqrt((R_cyl / r200) ** 2 + (z / r200) ** 2) + a / r200) - 1 / (R_cyl / r200 + a / r200)))

  z_distribution = lambda z: 1/np.cosh(z/z_0)**2
  # z_distribution = lambda z: np.exp(-(z / z_0) ** 2)
  # z_distribution = thick

  R_distribution = lambda x, y: np.exp(-np.sqrt(x**2 + y**2) / R_d)

  # large = 20
  mc_neval = 4e4
  mc_err_tol = 0.005
  # mc_neval = 1e4
  # mc_err_tol = 0.02

  R_distribution = lambda x, y: np.exp(-np.sqrt(x**2 + y**2) / R_d)

  bin_max_2 = large
  volume = lambda x, y: x**2 + y**2 < bin_max_2

  print('intial (c/a)^2', c_2)
  print('intial c/a', np.sqrt(c_2))
  print()

  for _ in range(10):

    one_on_ellpitical_distance_2 = lambda x, y, z: 1 / (x**2 + y**2 + z**2/(c_2))

    top_zz_i = lambda x: one_on_ellpitical_distance_2(x[0],x[1],x[2]) * x[2]**2 * z_distribution(
      x[2]) * R_distribution(x[0], x[1]) * volume(x[0], x[1])
    top_xx_i = lambda x: one_on_ellpitical_distance_2(x[0],x[1],x[2]) * x[0]**2 * z_distribution(
      x[2]) * R_distribution(x[0], x[1]) * volume(x[0], x[1])
    bot_i = lambda x: one_on_ellpitical_distance_2(x[0],x[1],x[2]) * z_distribution(
      x[2]) * R_distribution(x[0], x[1]) * volume(x[0], x[1])

    while True:
      integ = vegas.Integrator([[0, np.sqrt(bin_max_2)], [0, np.sqrt(bin_max_2)], [0, large]])
      result = integ(top_zz_i, nitn=15, neval=mc_neval)  # nitn=10, neval=3e4)
      top_zz = result.mean
      error = result.sdev
      if np.divide(error, top_zz) < mc_err_tol:
        break
      else:
        print('Failed with')
        print('top_zz', top_zz)
        print('frac error top_zz', error / top_zz)

    print('top_zz', top_zz)
    print('frac error top_zz', error / top_zz)

    while True:
      integ = vegas.Integrator([[0, np.sqrt(bin_max_2)], [0, np.sqrt(bin_max_2)], [0, large]])
      result = integ(top_xx_i, nitn=15, neval=mc_neval)  # nitn=10, neval=3e4)
      top_xx = result.mean
      error = result.sdev
      if np.divide(error, top_xx) < mc_err_tol:
        break
      else:
        print('Failed with')
        print('top_xx', top_xx)
        print('frac error top_xx', error / top_xx)
    print('top_xx', top_xx)
    print('frac error top_xx', error / top_xx)

    c_2 = top_zz / top_xx

    print('(c/a)^2', c_2)
    print('c/a', np.sqrt(c_2))
    print()

  while True:
    integ = vegas.Integrator([[0, np.sqrt(bin_max_2)], [0, np.sqrt(bin_max_2)], [0, large]])
    result = integ(bot_i, nitn=15, neval=mc_neval)  # nitn=10, neval=3e4)
    bot = result.mean
    error = result.sdev
    if np.divide(error, bot) < mc_err_tol:
      break
    else:
      print('Failed with')
      print('bot', bot)
      print('frac error bot', error / bot)
  print('bot', bot)
  print('frac error bot', error / bot)

  print()
  print('M_zz =', top_zz / bot)
  print('M_xx =', top_xx / bot)
  print('sqrt M_zz =', np.sqrt(top_zz / bot))
  print('sqrt M_xx =', np.sqrt(top_xx / bot))

  print()
  print('c/a', np.sqrt(top_zz/top_xx))

  return

def integrator_tests():

  import timeit

  from numpy.random import normal, uniform
  f = lambda x: np.sqrt(2. * np.pi) * (x[2]>0.) * ((x[0]**2 + x[1]**2 + x[2]**2) < 1.)

  def distribution(size):
    xs = uniform(size=size)
    ys = uniform(size=size)
    zs = normal(size=size)
    return np.array((xs, ys, zs)).T

  npoints = 1e5
  def this():
    result, error = mcimport(f, npoints, distribution)
  print(timeit.timeit(this, number=10))
  # print(result * 8, error * 8)

  f = lambda x: np.exp(-x[2]**2/2) * ((x[0]**2 + x[1]**2 + x[2]**2) < 1.)
  def this():
    result, error = mcquad(f, npoints, xl=[-1.,-1.,-1.], xu=[1.,1.,1.])
  print(timeit.timeit(this, number=10))
  # print(result, error)

  f = lambda x: np.exp(-x[2]**2/2) * ((x[0]**2 + x[1]**2 + x[2]**2) < 1.)
  def this():
    result, error = mcmiser(f, npoints, xl=[-1.,-1.,-1.], xu=[1.,1.,1.])
  print(timeit.timeit(this, number=10))
  # print(result, error)

  # # Too impartient...
  # f = lambda x, y, z: np.exp(-z**2) * ((x**2 + y**2 + z**2) < 1.)
  # result, error = tplquad(f, -1, 1, lambda x: -1, lambda x: 1, lambda x, y: -1, lambda x, y: 1)
  # print(result, error)

  f = lambda x: np.exp(-x[2]**2/2) * ((x[0]**2 + x[1]**2 + x[2]**2) < 1.)

  def this():
    integ = vegas.Integrator([[-1, 1], [-1, 1], [-1, 1]])
    result = integ(f, nitn=10, neval=1e4)
  print(timeit.timeit(this, number=10))
  # print(result.mean, result.sdev)

  #vegas way better

  return

def integral_tests():

  x, m, s, t, z, f, g = symbols("x m s t z, f, g")

  f = (exp(-1/2*((x-m-z)/s)**2))**2
  g = (exp(-1/2*((x)/t)**2))**2

  out = integrate(f * g, (x, -oo, oo))

  print(out)

def this_test():

  # scale_height_integral_stuff()
  # scale_height_integral_threeD()

  import matplotlib.cm
  import scipy.interpolate as interpolate

  n = 10_000

  q_int_array = np.linspace(0.025, 0.975, 20)
  # pdf_array   = np.array([  0,   0,   0,   0, #0.2
  #                           0,   0, 0.1, 0.2, #0.4
  #                         0.3, 0.4, 0.5, 0.6, #0.6
  #                         0.7, 0.7, 0.7, 0.6, #0.8
  #                         0.5, 0.4, 0.3, 0.2])  #1
  bins = np.linspace(0,1,21)

  pdf_array_1 = np.array([  0,   0,   0,0.05, #0.2
                          0.2, 0.4, 0.5, 0.4, #0.4
                         0.38,0.35,0.32, 0.3, #0.6
                          0.3, 0.3, 0.3, 0.2, #0.8
                          0.1 ,0.1, 0.1,   0])  #1

  pdf_array_2 = np.array([  0,   0,   0,   0,
                         0.05, 0.2, 0.4, 0.42,
                          0.4,0.35,0.32,0.35,
                          0.4, 0.3, 0.3, 0.3,
                          0.2, 0.1, 0.1,   0])

  pdf_array_3 = np.array([  0,   0,   0,   0, #0.2
                            0,   0, 0.1, 0.2, #0.4
                          0.2, 0.4, 0.4,0.45, #0.6
                         0.45, 0.4, 0.3, 0.2, #0.8
                          0.1, 0.1, 0.1, 0.1])  #1

  pdf_array_4 = np.array([  0,   0,   0,   0, #0.2
                            0,   0,   0,   0, #0.4
                            0,   0, 0.1,0.15, #0.6
                          0.3, 0.4, 0.6, 0.7, #0.8
                         0.75, 0.8, 0.5, 0.2])  #1

  pdf_array_1 += np.random.rand(20) * pdf_array_1/10
  pdf_array_1 /= np.sum(pdf_array_1)

  cdf_1 = np.cumsum(pdf_array_1)
  cdf_1 = cdf_1 / cdf_1[-1]
  spline1 = interpolate.interp1d(cdf_1, np.linspace(0,1,20), bounds_error=True, fill_value=np.nan)
  values = np.random.rand(n)
  random_q_1 = spline1(values)

  pdf_array_2 += np.random.rand(20) * pdf_array_2/10
  pdf_array_2 *= 20/np.sum(pdf_array_2)

  cdf_2 = np.cumsum(pdf_array_2)
  cdf_2 = cdf_2 / cdf_2[-1]
  spline2 = interpolate.interp1d(cdf_2, np.linspace(0,1,20), bounds_error=True, fill_value=np.nan)
  values = np.random.rand(n)
  random_q_2 = spline2(values)

  pdf_array_3 += np.random.rand(20) * pdf_array_3/10
  pdf_array_3 *= 20/np.sum(pdf_array_3)

  cdf_3 = np.cumsum(pdf_array_3)
  cdf_3 = cdf_3 / cdf_3[-1]
  spline3 = interpolate.interp1d(cdf_3, np.linspace(0,1,20), bounds_error=True, fill_value=np.nan)
  values = np.random.rand(n)
  random_q_3 = spline3(values)

  pdf_array_4 += np.random.rand(20) * pdf_array_4/10
  pdf_array_4 *= 20/np.sum(pdf_array_4)

  cdf_4 = np.cumsum(pdf_array_4)
  cdf_4 = cdf_4 / cdf_4[-1]
  spline4 = interpolate.interp1d(cdf_4, np.linspace(0,1,20), bounds_error=True, fill_value=np.nan)
  values = np.random.rand(n)
  random_q_4 = spline4(values)

  plt.figure()
  # plt.errorbar(q_int_array, pdf_array_1, c='blue', linewidth=2)
  # plt.errorbar(q_int_array, pdf_array_2, c='green', linewidth=2)
  # plt.errorbar(q_int_array, pdf_array_3, c='orange', linewidth=2)
  # plt.errorbar(q_int_array, pdf_array_4, c='red', linewidth=2)

  plt.errorbar(q_int_array, np.histogram(random_q_1, bins=bins, density=True)[0], color='blue', linewidth=3)
  plt.errorbar(q_int_array, np.histogram(random_q_2, bins=bins, density=True)[0], color='green', linewidth=3)
  plt.errorbar(q_int_array, np.histogram(random_q_3, bins=bins, density=True)[0], color='orange', linewidth=3)
  plt.errorbar(q_int_array, np.histogram(random_q_4, bins=bins, density=True)[0], color='red', linewidth=3)

  # eleven = 5
  # alphas = np.linspace(-1, 1, eleven)
  # for i, alpha in enumerate(alphas):
  #   plt.errorbar(q_int_array * (alpha + (1 - alpha) * q_int_array), pdf_array,
  #                c=matplotlib.cm.get_cmap('inferno')(i / eleven))

  #
  mean_time = 6  # Gyr
  beta = 28.5 * 1e-10
  my_correction = lambda q_heat, time, mass: (1 - (1 - q_heat) * np.exp(beta * time * mass))

  # plt.errorbar(my_correction(q_int_array, mean_time, 2.3e8), pdf_array_4, c='k', ls='-.')

  # for time in np.array([0.2,0.4,0.8,1.6,3.2,6.4,12.8]):
  #   plt.hist(my_correction(random_q_1, time, 4.5e5), bins=bins, color='blue', density=True, histtype='step', linestyle=':', linewidth=2)
  #   plt.hist(my_correction(random_q_2, time, 3.6e6), bins=bins, color='green', density=True, histtype='step', linestyle=':', linewidth=2)
  #   plt.hist(my_correction(random_q_3, time, 2.9e7), bins=bins, color='orange', density=True, histtype='step', linestyle=':', linewidth=2)
  #   plt.hist(my_correction(random_q_4, time, 2.8e8), bins=bins, color='red', density=True, histtype='step', linestyle=':', linewidth=2)

  time=mean_time
  # plt.hist(my_correction(random_q_1, time, 4.5e5), bins=bins, color='blue', density=True, histtype='step', linestyle='--', linewidth=4)
  # plt.hist(my_correction(random_q_2, time, 3.6e6), bins=bins, color='green', density=True, histtype='step', linestyle='--', linewidth=4)
  # plt.hist(my_correction(random_q_3, time, 2.9e7), bins=bins, color='orange', density=True, histtype='step', linestyle='--', linewidth=4)
  # plt.hist(my_correction(random_q_4, time, 2.8e8), bins=bins, color='red', density=True, histtype='step', linestyle='--', linewidth=4)

  plt.errorbar(q_int_array, np.histogram(my_correction(random_q_1, time, 4.5e5), bins=bins, density=True)[0],
               linestyle='--', color='blue', linewidth=2)
  plt.errorbar(q_int_array, np.histogram(my_correction(random_q_2, time, 3.6e6), bins=bins, density=True)[0],
               linestyle='--', color='green', linewidth=2)
  plt.errorbar(q_int_array, np.histogram(my_correction(random_q_3, time, 2.9e7), bins=bins, density=True)[0],
               linestyle='--', color='orange', linewidth=2)
  plt.errorbar(q_int_array, np.histogram(my_correction(random_q_4, time, 2.8e8), bins=bins, density=True)[0],
               linestyle='--', color='red', linewidth=2)

  plt.grid()
  plt.xlim(0, 1)
  # plt.close()

  pdf_sami = np.array([  0,0.01, 0.1, 0.2, #0.2
                       0.3, 0.4, 0.4, 0.4, #0.4
                       0.4,0.45,0.45,0.45, #0.6
                       0.5, 0.5, 0.5,0.45, #0.8
                       0.4,0.35, 0.3, 0.2])  #1

  pdf_sami += np.random.rand(20) * pdf_sami/10
  pdf_sami *= 20/np.sum(pdf_sami)
  cdf_s = np.cumsum(pdf_sami)
  cdf_s = cdf_s / cdf_s[-1]
  splines = interpolate.interp1d(cdf_s, np.linspace(0,1,20), bounds_error=True, fill_value=np.nan)
  values = np.random.rand(n)
  random_q_sami = splines(values)

  plt.figure()
  # plt.errorbar(q_int_array, pdf_sami, c='magenta', linewidth=2)

  plt.errorbar(q_int_array, np.histogram(random_q_sami, bins=bins, density=True)[0], color='magenta', linewidth=3)

  plt.errorbar(q_int_array, np.histogram(random_q_1, bins=bins, density=True)[0], color='blue', linewidth=3, linestyle=':')
  plt.errorbar(q_int_array, np.histogram(random_q_2, bins=bins, density=True)[0], color='green', linewidth=3, linestyle=':')
  plt.errorbar(q_int_array, np.histogram(random_q_3, bins=bins, density=True)[0], color='orange', linewidth=3, linestyle=':')
  plt.errorbar(q_int_array, np.histogram(random_q_4, bins=bins, density=True)[0], color='red', linewidth=3, linestyle=':')

  mean_time = 8  # Gyr
  beta = 28.5 * 1e-10 * 2e3
  my_reheat = lambda q_0, time, mass: 1 + (q_0 -1) * np.exp(-beta * time * np.sqrt(mass))

  # for time in np.array([0.2,0.4,0.8,1.6,3.2,6.4,12.8]):
  #   plt.errorbar(my_reheat(q_int_array, time, 4.5e5), pdf_sami, ls=':', c='blue', alpha=0.8)
  #   plt.errorbar(my_reheat(q_int_array, time, 3.6e6), pdf_sami, ls=':', c='green', alpha=0.8)
  #   plt.errorbar(my_reheat(q_int_array, time, 2.9e7), pdf_sami, ls=':', c='orange', alpha=0.8)
  #   plt.errorbar(my_reheat(q_int_array, time, 2.3e8), pdf_sami, ls=':', c='red', alpha=0.8)

  time=mean_time
  plt.errorbar(q_int_array, np.histogram(my_reheat(random_q_sami, time, 4.5e5), bins=bins, density=True)[0],
               linestyle='--', color='blue', linewidth=2)
  plt.errorbar(q_int_array, np.histogram(my_reheat(random_q_sami, time, 3.6e6), bins=bins, density=True)[0],
               linestyle='--', color='green', linewidth=2)
  plt.errorbar(q_int_array, np.histogram(my_reheat(random_q_sami, time, 2.9e7), bins=bins, density=True)[0],
               linestyle='--', color='orange', linewidth=2)
  plt.errorbar(q_int_array, np.histogram(my_reheat(random_q_sami, time, 2.8e8), bins=bins, density=True)[0],
               linestyle='--', color='red', linewidth=2)

  plt.grid()
  plt.xlim(0, 1)
  plt.ylim(0, 5)

  plt.show()


  # n_tests = np.array([10, 100, 1_000, 10_000, 100_000])
  # n_per_n = 1000
  #
  # results = np.zeros((len(n_tests), n_per_n))
  #
  # for i, N in enumerate(n_tests):
  #   for j in range(n_per_n):
  #
  #     results[i, j] = np.std(np.random.normal(size = N))
  #
  # print(np.std(results, axis=1))
  # # print(1/np.sqrt(n_tests))
  # print(np.std(results, axis=1) * np.sqrt(n_tests))


  # x = np.arcsin(np.random.rand(100_000)*2-1)
  #
  # plt.hist(x, bins=np.linspace(-1,1))
  # plt.show()

  # n=100_000
  #
  # radius = 1
  #
  # z = (2 * np.random.rand(n) - 1) * radius
  # phi = 2*np.pi * np.random.rand(n)
  # R = np.sqrt(radius**2 - z**2) #pythagorous
  # x = R * np.cos(phi)
  # y = R * np.sin(phi)
  #
  # print(np.median(R)) #sqrt(3)/2
  # print(np.median(np.abs(z))) #1/2
  #
  # print(np.median(np.abs(z))/np.median(R)) #1/sqrt(3) = approx 0.577

  # import timeit
  #
  # integrand = lambda x,y,z : 1/np.sqrt(x**2 + y**2 + z**2)
  #
  # integrand_0 = lambda x : 1/np.sqrt(x**2)
  # integrand_1 = lambda x : 1/np.sqrt(x**2)
  # integrand_2 = lambda x : 1/np.sqrt(x**2)
  #
  # out0 = tplquad(integrand, 1, 2, 1, 2, 1, 2)
  #
  # print(out0)
  #
  # print(timeit.timeit(lambda : tplquad(integrand, 1, 2, 1, 2, 1, 2), number=1_00))
  #
  #
  # integrand = lambda x : 1/np.sqrt(x[0]**2 + x[1]**2 + x[2]**2)
  #
  # integ = vegas.Integrator([[1, 2], [1, 2], [1, 2]])
  # result = integ(integrand, nitn=10, neval=1000)
  #
  # print(result)
  #
  # print(timeit.timeit(lambda : integ(integrand, nitn=10, neval=1000), number=1_00))

  #
  # z = np.linspace(0, 2)
  # R = 1
  #
  # GM = 1
  #
  # phi = - GM / np.sqrt(R**2 + z**2)
  #
  # phi_z = phi + GM / np.sqrt(R**2)
  #
  # Omega = np.sqrt(GM / (np.sqrt(R**2 + z**2)**3))
  #
  # z2Omega2 = z**2 * Omega**2
  #
  # plt.figure()
  #
  # plt.errorbar(z, 2*phi_z)
  # plt.errorbar(z, z2Omega2)
  #
  # plt.show()

  import numpy as np
  from scipy.integrate import quad
  from scipy.special import iv

  N=100_000
  sigma = 1
  vc = sigma * np.sqrt(2)

  np.random.seed(0)
  out = np.mean( np.sqrt( (vc - sigma * np.random.normal(size=N))**2 +
                         (sigma * np.random.normal(size=N))**2 + (sigma * np.random.normal(size=N))**2 ) )

  print(out)

  ncx2_pdf = lambda x, args : (0.5 * np.exp( - (args[1] + x) / 2 ) * (x / args[1])**((args[0] - 2) /4) *
                               iv((args[0] - 2) / 2, np.sqrt(args[1] * x)))
  mean_ncx2 = lambda x, args : np.sqrt(x) * ncx2_pdf(x, args)

  theory = quad(mean_ncx2, 0, 30 * (3 + vc**2/sigma**2),  args=[3, vc**2/sigma**2])[0]

  two_point_something = quad(mean_ncx2, 0, 30 * (3 + 2), args=[3, 2])[0]

  print(theory * sigma)


  def hernquist_X(s):
    # if s <= 1:
    #   X = 1 / np.sqrt(1 - s**2) * np.arccosh(1/s)
    # else:
    #   X = 1 / np.sqrt(s**2 - 1) * np.arccos(1/s)

    X_l = 1 / np.sqrt(1 - s ** 2) * np.arccosh(1 / s)
    X_h = 1 / np.sqrt(s ** 2 - 1) * np.arccos(1 / s)

    X = np.zeros(np.shape(s))
    n_l = ~np.isnan(X_l)
    n_h = ~np.isnan(X_h)
    X[n_l] = X_l[n_l]
    X[n_h] = X_h[n_h]

    return X


  def hernquist_projected_intenisty(R, Mass, a_h):
    '''Proportional to this
  '''
    s = R / a_h

    I = Mass / (a_h ** 2 * (1 - s ** 2) ** 2) * ((2 + s ** 2) * hernquist_X(s) - 3)

    I[R==a] = Mass / a_h**2 * 4 / 15

    return (I)

  m = 1
  a = 1
  s = np.logspace(-1,1,11)

  hi = hernquist_projected_intenisty(s * a, m, a)

  rd = 10

  dvi = 4/15 *  10**(-(s * a / rd) ** (1/4))

  print(hi)
  print(dvi)
  print(hi/dvi)


  #
  # n = 100_000
  # nb = 200 + 1
  # R_d = 1
  #
  # q = np.random.rand(n)
  #
  # r = - R_d * (lambertw((q - 1) / np.exp(1), -1).real + 1)
  # r[q==0] = 0
  #
  # rs = np.linspace(0,10,nb)
  #
  # bin_ns = np.digitize(r, bins=rs)
  #
  # dMs = np.array([np.sum(bin_ns == i+1) for i in range(nb-1)])
  #
  # drhos = dMs * 3 / (4 * np.pi * (rs[1:]**3 - rs[:-1]**3)) / n
  #
  # # print(r)
  #
  # cum_M = np.cumsum(dMs) / n
  #
  # rs = 0.5*(rs[1:] + rs[:-1])
  # Ms = (1 - (rs/R_d + 1) * np.exp(-rs / R_d))
  # rhos = 1/rs * np.exp(-rs / R_d) / (4 * np.pi)
  #
  # # plt.figure()
  # #
  # # plt.errorbar(rs, rhos)
  # # plt.errorbar(rs, drhos)
  # #
  # # plt.semilogy()
  # #
  # # plt.figure()
  # #
  # # plt.errorbar(rs, cum_M)
  # # plt.errorbar(rs, Ms)
  # #
  # # plt.errorbar(rs, cum_M / Ms)
  #
  # z0 = 1
  #
  # z = z0 / 2 * np.log(q / (1 - q))
  #
  # # zs =  np.linspace(-5,5,nb)
  # zs =  np.linspace(0,10,nb)
  #
  # bin_ns = np.digitize(z, bins=rs)
  #
  # dMs = np.array([np.sum(bin_ns == i+1) for i in range(nb-1)])
  #
  # drhos = dMs / np.diff(zs) / n
  #
  # zs = 0.5*(zs[1:] + zs[:-1])
  #
  # rhos = 1/np.cosh(-zs / R_d)**2 / 2
  #
  # plt.figure()
  #
  # plt.errorbar(zs, drhos)
  # plt.errorbar(zs, rhos)
  #
  # plt.show()




if __name__ == '__main__':

    init_printing(use_unicode=True)

    # # v, s, vp, f = symbols('v, s, v_p, f')
    #
    # # h = v ** 4 * exp(-v ** 2 / (2 * s ** 2))
    #
    # # h = v ** 4 * exp(-v ** 2 / (2 * 23 ** 2)) / 23**5
    # # print(integrate(h, (v, 0, 17)))
    # #
    # # print()
    # #
    # # h = v * exp(-v ** 2 / (2 * 23 ** 2)) / 23**2
    # # print(integrate(h, (v, 17, oo)))
    # #
    # # print()
    # # print()
    #
    # # g = 3 - v ** 4 * exp(-v ** 2 / (2 * s ** 2)) / s**5
    # # print(integrate(g, (v, 0, 17)))
    # #
    # # print()
    # #
    # # g = 2 * v * exp(-v ** 2 / (2 * s ** 2)) / s**2
    # # print(integrate(g, (v, 17, oo)))
    #
    #
    # # phi, y, R, a, b, c = symbols('\phi y R a b c')
    # #
    # # phi = 1 / ((y**2 + R**2)**(1/2))
    # # print(integrate(phi, (y, -oo, oo)))
    # # print(integrate(phi, (y, b, c)))
    # #
    # # phi = 1 / (y + a)
    # # print(integrate(phi, (y, -oo, oo)))
    # # print(integrate(phi, (y, b, c)))
    # #
    # # phi = 1 / ((y**2 + R**2)**(1/2) + a)
    # # print(integrate(phi, (y, -oo, oo)))
    # # print(integrate(phi, (y, -b, c)))
    #
    # n = 1_000_000
    # R = 1
    #
    # u = np.random.rand(n)
    # x = R * np.cos(2 * np.pi * u)
    # y = R * np.sin(2 * np.pi * u)
    #
    # print(np.sum(x**2)/n)
    # print(np.sum(y**2)/n)
    #
    #
    #
    # print()
    # print()
    # print()
    # print()
    #
    # n = 1_000_000
    #
    # # u = np.random.rand(n)
    # # v = np.random.rand(n)
    # #
    # # theta = u * 2.0 * np.pi
    # # phi = np.arccos(2.0 * v - 1.0)
    # # r = np.random.rand(n)**(1/3)
    # #
    # # sinTheta = np.sin(theta)
    # # cosTheta = np.cos(theta)
    # # sinPhi = np.sin(phi)
    # # cosPhi = np.cos(phi)
    # #
    # # x = r * sinPhi * cosTheta
    # # y = r * sinPhi * sinTheta
    # # z = r * cosPhi
    #
    # x = 2*np.random.rand(n) - 1
    # y = 2*np.random.rand(n) - 1
    # z = 2*np.random.rand(n) - 1
    #
    # r = np.sqrt(x**2 + y**2 + z**2)
    #
    # do_not_reject = r <= 1
    #
    # x = x[do_not_reject]
    # y = y[do_not_reject]
    # z = z[do_not_reject]
    #
    # r = np.sqrt(x**2 + y**2 + z**2)
    #
    # R = np.sqrt(x**2 + y**2)
    #
    # # print(np.median(r))
    # print(np.median(np.abs(z)))
    # print(np.median(R))
    # print(np.median(np.abs(z))/np.median(R))
    # # print(np.median(r) / np.median(R))
    # # print(np.median(R) / np.median(r))


    #########################################################################
    print()
    print()
    print()
    print()


    scale_height_integral_stuff()

    pass