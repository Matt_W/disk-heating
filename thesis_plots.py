#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 23 16:36:50 2021

@author: matt
"""
from functools import lru_cache
# from functools import cache

import os

import numpy as np

# import h5py as h5

from scipy.optimize import brentq
from scipy.optimize import minimize
from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.interpolate import interp1d
from scipy.interpolate import RectBivariateSpline
# from scipy.interpolate import UnivariateSpline
from scipy.integrate import quad
from scipy.integrate import dblquad
from scipy.signal import savgol_filter
from scipy.signal import convolve
from scipy.stats import binned_statistic
from scipy.stats import binned_statistic_2d
import scipy.stats
from scipy.special import erf
from scipy.special import erfinv
from scipy.special import gamma
from scipy.special import gammainc
from scipy.special import kn, iv
from scipy.special import lambertw
from scipy.spatial import distance_matrix
from scipy.spatial.transform import Rotation

import matplotlib

# default
# matplotlib.use('Qt5Agg')
# stop resizing plots
matplotlib.use('TkAgg')

import matplotlib.pyplot as plt
import matplotlib.cm
from matplotlib import ticker
import matplotlib.patheffects as path_effects
from matplotlib.transforms import Bbox
import matplotlib.patches as patches

# from   matplotlib.colors import LogNorm
# from matplotlib.cm import viridis
# from matplotlib.cm import ScalarMappable
# from matplotlib.colors import Normalize
# from matplotlib.legend_handler import HandlerTuple
# import matplotlib.lines as lines

import seaborn as sns
from seaborn import desaturate

from sphviewer.tools import QuickView
from sphviewer.tools import Blend

import emcee
import corner

import vegas

import imageio

import multiprocessing

# import splotch as splt

# import halo_calculations as halo
import load_nbody

from my_shape import find_abc
from my_shape import find_abc_with_outer_bias

import halo_calculations
import recalibrate_heating_model as rcm
import simulation_kinematics_N as skn

from matplotlib import rcParams

import twobody

rcParams['ps.useafm'] = True
rcParams['pdf.use14corefonts'] = False
rcParams['text.usetex'] = False
rcParams['font.sans-serif'] = ['cmr10', 'Times-Roman']
rcParams['font.weight'] = 'normal'
rcParams['font.size'] = 20
rcParams['xtick.labelsize'] = 20
rcParams['ytick.labelsize'] = 20
rcParams['axes.labelsize'] = 22
rcParams['axes.linewidth'] = 1
rcParams['axes.unicode_minus'] = False
# rcParams['axes.grid'] = True
rcParams['axes.grid'] = False
rcParams['xtick.minor.width'] = 1
rcParams['ytick.minor.width'] = 1
rcParams['xtick.major.width'] = 1
rcParams['ytick.major.width'] = 1
rcParams['xtick.minor.size'] = 4
rcParams['ytick.minor.size'] = 4
rcParams['xtick.major.size'] = 5
rcParams['ytick.major.size'] = 5
rcParams['xtick.direction'] = 'in'
rcParams['ytick.direction'] = 'in'
rcParams['xtick.top'] = True
rcParams['ytick.right'] = True
rcParams['mathtext.fontset'] = 'cm'

import matplotlib.lines as lines
from matplotlib.legend_handler import HandlerTuple
from matplotlib.lines import Line2D

GRAV_CONST = 4.301e4  # kpc (km/s)^2 / (10^10Msun) (source ??)
HUBBLE_CONST = 0.06777  # km/s/kpc (Planck 2018)
GALIC_HUBBLE_CONST = 0.1  # h km/s/kpc
RHO_CRIT = 3 * HUBBLE_CONST ** 2 / (8 * np.pi * GRAV_CONST)  # 10^10Msun/kpc^3

PC_ON_M = 3.0857e16  # pc/m = kpc/km (wiki)
GYR_ON_S = 3.15576e16  # gyr/s

T_TOT = 9.778943899409334  # Gyr  # 10 * PC_ON_M / GYR_ON_S / CM_ON_S

BURN_IN = 2

like_fail_value = np.inf

#line style strings
alpha = 0.25
lwt = 6
lw = 3
poly_n = 5
window_n = 5
pth_6 = [path_effects.Stroke(linewidth=6, foreground='black'), path_effects.Normal()]
pth_5 = [path_effects.Stroke(linewidth=5, foreground='black'), path_effects.Normal()]
pth_4 = [path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()]
pth_3 = [path_effects.Stroke(linewidth=3, foreground='black'), path_effects.Normal()]
pth_2 = [path_effects.Stroke(linewidth=2, foreground='black'), path_effects.Normal()]
pth_1 = [path_effects.Stroke(linewidth=1, foreground='black'), path_effects.Normal()]

wpth_6 = [path_effects.Stroke(linewidth=6, foreground='white'), path_effects.Normal()]
wpth_5 = [path_effects.Stroke(linewidth=5, foreground='white'), path_effects.Normal()]
wpth_4 = [path_effects.Stroke(linewidth=4, foreground='white'), path_effects.Normal()]
wpth_3 = [path_effects.Stroke(linewidth=3, foreground='white'), path_effects.Normal()]
wpth_0 = [path_effects.Stroke(linewidth=0, foreground='white'), path_effects.Normal()]
save_name_append='diff'

def randomness_in_ics(name='', save=True):

    galaxy_name_list = [
        # 'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed0',
        # 'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed1',
        # 'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed2',
        # 'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed3',
        # 'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed4',
        # 'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed5',
        # 'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed6',
        # 'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed7',
        # 'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed8',
        # 'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed9',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed0',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed1',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed2',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed3',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed4',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed5',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed6',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed7',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed8',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed9',
        # 'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed0',
        # 'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed1',
        # 'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed2',
        # 'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed3',
        # 'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed4',
        # 'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed5',
        # 'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed6',
        # 'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed7',
        # 'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed8',
        # 'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed9',
        # 'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps_seed0',
        # 'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps_seed1',
        # 'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps_seed2',
        # 'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps_seed3',
        # 'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps_seed4',
        # 'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps_seed0',
        # 'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps_seed0',
    ]
    snaps_list = [400] * 30
    # snaps_list = [101]

    markers = ['s', 'D', 'v', '^', '8', 'h', 'o', 'P', 'p', '*'] #'X', '<', '>',]
    ms = 3
    lw = 2
    bin_n = 21

    _index = 1

    # fig = plt.figure(constrained_layout=True)
    fig, axs = plt.subplots(2, 2, figsize=(11.3, 11.3), gridspec_kw={'width_ratios': [3,1]})
    ax0 = axs[0,0]
    ax1 = axs[1,0]
    ax0r = axs[0,1]
    ax1r = axs[1,1]

    fig.subplots_adjust(hspace=0.02, wspace=0.02)
    # fig.set_size_inches(7.2, 7.2, forward=True)
    # ax0 = plt.subplot(1, 1, 1)

    # fig.set_size_inches(7.2, 11.3, forward=True)
    # ax0 = plt.subplot(2, 1, 1)
    # ax1 = plt.subplot(2, 1, 2)

    # fig.set_size_inches(11.3, 11.3, forward=True)
    # ax0 = plt.subplot(2, 2, 1)
    # ax1 = plt.subplot(2, 2, 3)
    # ax0r = plt.subplot(2, 2, 2)
    # ax1r = plt.subplot(2, 2, 4)

    xlim = [0, T_TOT] # * (101 - BURN_IN) / 101]
    ax0.set_xlim(xlim)
    ax1.set_xlim(xlim)
    ax0.set_xticklabels([])
    ax0r.set_xticklabels([])
    ax1r.set_xticklabels([])

    slim = [-0.25, 0.25]
    ax0.set_ylim(slim)
    ax1.set_ylim(slim)
    ax0r.set_ylim(slim)
    ax1r.set_ylim(slim)
    ax0r.set_yticklabels([])
    ax1r.set_yticklabels([])

    # ax0.set_ylabel(r'$[\sigma_{z, i}(t) - \sigma_{z, i}(\tilde{t})] / \sigma_{z, i}(\tilde{t})$')
    ax0.set_ylabel(r'$(\sigma_{z} - {\rm smooth} \ \sigma_{z}) / {\rm smooth} \ \sigma_{z}$')
    # ax0.set_xlabel(r'$t$ [Gyr]')
    # ax1.set_ylabel(r'$[\sigma_{z, i}(t) - \tilde{\sigma}_{z}(\tilde{t})] / \tilde{\sigma}_{z}(\tilde{t})$')
    ax1.set_ylabel(r'$(\sigma_{z} - {\rm averaged} \ \sigma_{z}) / {\rm averaged} \ \sigma_{z}$')
    ax1.set_xlabel(r'$t$ [Gyr]')
    ax0.set_xlabel('')

    ax1r.set_xlabel(r'Probability Density')

    bin_edges = rcm.get_dex_bins(rcm.get_bin_edges(save_name_append='cum')[1::2], dex=0.1)

    (med_mean_v_R, med_mean_v_phi, med_sigma_z, med_sigma_R, med_sigma_phi
     ) = rcm.get_dispersions(galaxy_name_list[0][:-6], save_name_append, snaps_list[0], bin_edges=bin_edges)
    v200 = rcm.get_v_200(galaxy_name_list[0][:-6])

    stds_0 = []
    stds_1 = []
    stds_2 = []

    smoothed_ys = []

    window = snaps_list[0] // window_n + 1
    if window % 2 == 0: window += 1
    med_ys = savgol_filter(med_sigma_z[:, _index], window, poly_n) / v200

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = rcm.get_name_kwargs(galaxy_name)
        bins = np.linspace(*slim, bin_n) + (i / len(galaxy_name_list)) * (slim[1] - slim[0]) / bin_n

        v200 = rcm.get_v_200(galaxy_name)

        # load data
        (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
         ) = rcm.get_dispersions(galaxy_name, save_name_append, snaps, bin_edges=bin_edges)

        time = np.linspace(0, T_TOT, snaps + 1)

        window = snaps // window_n + 1
        if window % 2 == 0: window += 1
        c = matplotlib.cm.get_cmap('turbo')(i / (len(galaxy_name_list) - 1 + 1e-3))

        ys = savgol_filter(sigma_z[:, _index], window, poly_n) / v200
        y = (sigma_z[:, _index] / v200 - ys) / ys
        ax0.errorbar(time, y,
                     c=c, marker=markers[i], ms=ms, ls='', label=str(i))
        # ax0.errorbar(time, y,
        #              c=c, marker='', lw=lw, ls='--', alpha=alpha)
        stds_0.append(np.std(y))

        ax0r.hist(y, bins=bins,
                  color=c, orientation='horizontal', histtype='step', linewidth=lw, density=True)

        y = (sigma_z[:, _index] / v200 - med_ys) / med_ys
        ax1.errorbar(time, y,
                     c=c, marker=markers[i], ms=ms, ls='')
        # ax1.errorbar(time, y,
        #              c=c, marker='', lw=lw, ls='--', alpha=alpha)

        ax1r.hist(y, bins=bins,
                  color=c, orientation='horizontal', histtype='step', linewidth=lw, density=True)

        ax1.errorbar(time, (ys - med_ys) / med_ys,
                     c=c, marker=markers[i], ms=ms, lw=lw, ls='', zorder=20 + i, path_effects=pth_3)
        # ax1.errorbar(time, (ys - med_ys) / med_ys,
        #              c=c, marker=markers[i], lw=lw, ls='', zorder=20 + i, ms=5, mec='k', markeredgewidth=0.1)

        smoothed_ys.append((ys - med_ys) / med_ys)

        stds_1.append(np.std(y))
        stds_2.append(np.std((ys - med_ys) / med_ys))

    smoothed_ys = np.hstack(smoothed_ys)
    bins_hr = np.linspace(*slim, bin_n*4)

    ax1r.hist(smoothed_ys, bins=bins_hr,
              color='white', orientation='horizontal', histtype='step', linewidth=lw,  density=True,
              zorder=20 + i, path_effects=pth_4)

    # # asymptote max
    # analytic_dispersion = rcm.get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
    #                    save_name_append=save_name_append, galaxy_name=galaxy_name_list[0])
    # ax0.errorbar([0, T_TOT], [analytic_dispersion[_index] / rcm.get_v_200(galaxy_name_list[0])] * 2,
    #              color='k', ls='-')
    ax0.errorbar([0, T_TOT], [0] * 2,
                 color='k', ls=':', lw=5, zorder=50)
    ax1.errorbar([0, T_TOT], [0]*2,
                 color='k', ls=':', lw=5, zorder=50)

    bins_hr = np.linspace(*slim, bin_n*100)
    ax0r.errorbar(scipy.stats.norm.pdf(bins_hr, 0, np.sqrt(1/100/2)), bins_hr,
                 c='k', ls=':', lw=5, zorder=50)
    ax1r.errorbar(scipy.stats.norm.pdf(bins_hr, 0, np.sqrt(1/100/2)), bins_hr,
                 c='k', ls=':', lw=5, zorder=50)
    ax1r.errorbar(scipy.stats.norm.pdf(bins_hr, 0, np.sqrt(1/1000/2)), bins_hr,
                 c='k', ls=':', lw=5, zorder=50)

    ax0.legend(
        title='Realisation (random seed)',
        ncol=10, numpoints=1, handlelength=0.3, handletextpad=0.3, columnspacing=0.6,
        frameon=True, loc='lower right')
    ax1.legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='', marker='o')),
                (lines.Line2D([0, 1], [0, 1], color='grey', ls='', marker='o', path_effects=pth_3))],
               ['instantaneous', 'smoothed'],
        frameon=True, loc='lower right', handlelength=1, scatterpoints=3)

    # r'$N_{\rm DM} =' + str(Ndm)
    # r'$N_*$ =' + str(Nstar)
    # r'$N_*$ at R = R_{1/2} =' + str(Nstar_half)

    kwargs = rcm.get_name_kwargs(galaxy_name_list[0][:-6])

    ax0.text(xlim[0] + 0.05 * (xlim[1] - xlim[0]), slim[0] + 0.96 * (slim[1] - slim[0]),
             r'$N_{\rm DM} =$' + str(kwargs['labn']) + ',',  # , V_{200} = 200$ km/s',
             ha='left', va='top')
    ax0.text(xlim[0] + 0.45 * (xlim[1] - xlim[0]), slim[0] + 0.95 * (slim[1] - slim[0]),
             r'$N_* =$' + str(kwargs['labnstar']) + ',',  # , V_{200} = 200$ km/s',
             ha='left', va='top')
    ax0.text(xlim[0] + 0.70 * (xlim[1] - xlim[0]), slim[0] + 0.95 * (slim[1] - slim[0]),
             r'$R = R_{1/2, 0}$',  # , V_{200} = 200$ km/s',
             ha='left', va='top')

    # ax0.text(xlim[0] + 0.95 * (xlim[1] - xlim[0]), slim[0] + 0.78 * (slim[1] - slim[0]),
    #          r'$\sigma_{\mathrm{DM}}$', ha='right', va='top')

    print(np.mean(stds_0), np.median(stds_0))
    print(np.mean(stds_1), np.median(stds_1))
    print(np.mean(stds_2), np.median(stds_2))

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return


def randomness_in_ics2(name='', save=True):

    galaxy_name_list = [
        'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed0',
        'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed1',
        'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed2',
        'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed3',
        'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed4',
        'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed5',
        'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed6',
        'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed7',
        'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed8',
        'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed9',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed0',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed1',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed2',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed3',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed4',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed5',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed6',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed7',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed8',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed9',
        'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed0',
        'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed1',
        'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed2',
        'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed3',
        'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed4',
        'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed5',
        'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed6',
        'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed7',
        'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed8',
        'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed9',
        'mu_1/fdisk_0p01_lgMdm_7p5_V200-200kmps_seed0',
        'mu_1/fdisk_0p01_lgMdm_7p5_V200-200kmps_seed1',
        'mu_1/fdisk_0p01_lgMdm_7p5_V200-200kmps_seed2',
        'mu_1/fdisk_0p01_lgMdm_7p5_V200-200kmps_seed3',
        'mu_1/fdisk_0p01_lgMdm_7p5_V200-200kmps_seed4',
        'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps_seed0',
        'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps_seed1',
        'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps_seed2',
        'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps_seed3',
        'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps_seed4',
        'mu_25/fdisk_0p01_lgMdm_7p5_V200-200kmps_seed0',
        'mu_25/fdisk_0p01_lgMdm_7p5_V200-200kmps_seed1',
        'mu_25/fdisk_0p01_lgMdm_7p5_V200-200kmps_seed2',
        'mu_25/fdisk_0p01_lgMdm_7p5_V200-200kmps_seed3',
        'mu_25/fdisk_0p01_lgMdm_7p5_V200-200kmps_seed4',
        'mu_1/fdisk_0p01_lgMdm_7p0_V200-200kmps',
        'mu_1/fdisk_0p01_lgMdm_6p5_V200-200kmps',
        # 'mu_1/fdisk_0p01_lgMdm_6p0_V200-200kmps',
        'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
        'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
        'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps',
        'mu_25/fdisk_0p01_lgMdm_7p0_V200-200kmps',
        'mu_25/fdisk_0p01_lgMdm_6p5_V200-200kmps',
        'mu_25/fdisk_0p01_lgMdm_6p0_V200-200kmps',
    ]

    snaps_list = [400] * 45 + [101] * 9

    _index = 1

    fig = plt.figure(constrained_layout=True)
    fig.subplots_adjust(hspace=0.02, wspace=0.02)
    fig.set_size_inches(7.2, 7.2, forward=True)
    ax0 = plt.subplot(1, 1, 1)

    # fig.set_size_inches(7.2, 11.3, forward=True)
    # ax0 = plt.subplot(2, 1, 1)
    # ax1 = plt.subplot(2, 1, 2)

    xlim = [1, 5]
    ax0.set_xlim(xlim)
    # ax1.set_xlim(xlim)
    ax0.set_xticklabels([])

    slim = [np.log10(np.sqrt(1/(2 * 10**xlim[1]))), np.log10(np.sqrt(1/(2 * 10**xlim[0])))]
    ax0.set_ylim(slim)
    # ax1.set_ylim(slim)

    ax0.set_ylabel(r'$\log \, \sigma_{\sigma_z}$')
    ax0.set_xlabel(r'$\log \, N_{\star}$')
    # ax1.set_ylabel(r'$[\sigma_{z, i}(t) - \tilde{\sigma}_{z}(\tilde{t})] / \tilde{\sigma}_{z}(\tilde{t})$')
    # ax1.set_xlabel(r'$\log N_{200}$')
    # ax0.set_xlabel('')

    bin_edges = rcm.get_dex_bins(rcm.get_bin_edges(save_name_append='cum')[1::2], dex=0.1)

    # (med_mean_v_R, med_mean_v_phi, med_sigma_z, med_sigma_R, med_sigma_phi
    #  ) = rcm.get_dispersions(galaxy_name_list[0][:-6], save_name_append, snaps_list[0], bin_edges=bin_edges)
    # v200 = rcm.get_v_200(galaxy_name_list[0][:-6])
    #
    # window = snaps_list[0] // window_n + 1
    # if window % 2 == 0: window += 1
    # med_ys = savgol_filter(med_sigma_z[:, _index], window, poly_n) / v200

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = rcm.get_name_kwargs(galaxy_name)

        bin_edges = rcm.get_dex_bins(rcm.get_bin_edges(save_name_append='cum', galaxy_name=galaxy_name)[1::2], dex=0.1)

        v200 = rcm.get_v_200(galaxy_name)

        # load data
        (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
         ) = rcm.get_dispersions(galaxy_name, save_name_append, snaps, bin_edges=bin_edges)

        n_binned = rcm.get_n_for_poisson(galaxy_name, save_name_append, snaps, bin_edges)

        time = np.linspace(0, T_TOT, snaps + 1)

        window = snaps // window_n + 1
        if window % 2 == 0: window += 1
        c = matplotlib.cm.get_cmap('turbo')(i / (len(galaxy_name_list) - 1))

        for _index in [0,1,2]:
            ys = savgol_filter(sigma_z[:, _index], window, poly_n) / v200
            y = (sigma_z[:, _index] / v200 - ys) / ys

            # # n_point = np.mean(n_binned[:, _index])
            # n_point = np.mean(n_binned[:, _index])
            # y_point = np.std(y)
            #
            # ax0.errorbar(np.log10(n_point), np.log10(y_point),
            #              marker=kwargs['marker'], c=kwargs['c'], ms=5)

            n_point = np.mean(n_binned[BURN_IN:, _index])
            y_point = np.std(y[BURN_IN:])

            ax0.errorbar(np.log10(n_point), np.log10(y_point),
                         marker='o', c='k', ms=6)
                         # marker=kwargs['marker'], c=kwargs['c'], ms=6)

        # y = (sigma_z[:, _index] / v200 - med_ys) / med_ys
        # ax1.errorbar(time, y,
        #              c=c, marker='.', ls='')
        # ax1.errorbar(time, (ys - med_ys) / med_ys,
        #              c=c, marker='', lw=lw, ls='-', zorder=20 + i, path_effects=pth_4)

    #monte-carlo estimate
    # samples = 400
    # Ns = np.logspace(1, 5.5, 9*2 + 1)
    Ns = np.logspace(1, 5, 8*1 + 1)
    # disp_disp = []
    # np.random.seed(1)
    # for N in Ns:
    #     data = [np.std(np.random.normal(0, 1, int(N))) for _ in range(samples)]
    #     disp_disp.append(np.std(data))
    #
    # ax0.errorbar(np.log10(Ns), np.log10(disp_disp),
    #              c='k', ls='--')

    #analytic (guess and check model)
    # log_model = - 0.5 * np.log10(Ns) - 0.15
    model = np.sqrt(1/ (2 * Ns))
    ax0.errorbar(np.log10(Ns), np.log10(model),
                 c='k', ls='-.')

    ax0.legend([(lines.Line2D([0, 1], [0, 1], color='k', ls='', marker='o', markersize=5)),
                (lines.Line2D([0, 1], [0, 1], color='k', linestyle='-.')),],
               ['Scatter around\nsmoothed evolution',
                'Random normal scatter\n' + '$\sigma_\sigma = 1 / \sqrt{2 N}$'],
        numpoints=1, handlelength=1, handletextpad=0.3, columnspacing=0.6,
        frameon=True)#, loc='lower right')

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return


def plot_time_evolution(model=False, name='', save=False):
    # if model:
    #     raise NotImplementedError
    lw=3

    galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps']
    snaps_list = [400,400,101,101,101]

    if 'soft8' in name:
        galaxy_name_list = [
                            'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_128soft',
                            'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_96soft',
                            'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_64soft',
                            'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_48soft',
                            'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_32soft',
                            # 'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_24soft',
                            # 'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_20soft',
                            'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_16soft',
                            'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_8soft',
                            # 'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_1soft',
            ]
        snaps_list = [400 for _ in galaxy_name_list]
                      # 101, 101, 101, 101, 101]
        eps_list = [128, 96, 64, 48, 32, 16, 8,] #24, 20, 16, 8,] #1]

    if 'soft7' in name:
        galaxy_name_list = [
                            'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_128soft',
                            'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_96soft',
                            'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_64soft',
                            'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_48soft',
                            'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_32soft',
                            'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_16soft',
                            'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_8soft',
                            # 'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_1soft',
            ]
        snaps_list = [100 for _ in galaxy_name_list]
        eps_list = [128, 96, 64, 48, 32, 16, 8,] #1]

    if 'soft1' in name:
        galaxy_name_list = ['eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_4soft',
                            'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_2soft',
                            'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed1',
                            'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_0p5soft',
                            'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_0p25soft',
                            'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_4soft',
                            'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_2soft',
                            'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                            'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_0p5soft',
                            'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_0p25soft',
                            ]
        snaps_list = [101, 101, 400, 101, 101,
                      101, 101, 101, 101, 101]
        eps_list = [4, 2, 1, 0.5, 0.25,
                    4, 2, 1, 0.5, 0.25]
        cmap = 'twilight_r'

    if 'seeds' in name:
        galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed1',
                            'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed2',
                            'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed3',
                            'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed4',
                            'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed5',
                            'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed6',
                            'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed7',
                            'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed8',
                            'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed9',
                            ]
        snaps_list = [400 for _ in galaxy_name_list]
        cmap = 'turbo'

    if 'mu' in name:
        galaxy_name_list = ['mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                            'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                            'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                            'mu_1/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                            'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                            'mu_25/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                            'mu_1/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                            'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                            'mu_25/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                            # 'mu_1/fdisk_0p01_lgMdm_6p5_V200-200kmps',
                            # 'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
                            # 'mu_25/fdisk_0p01_lgMdm_6p5_V200-200kmps',
                            # # 'mu_1/fdisk_0p01_lgMdm_6p0_V200-200kmps',
                            # 'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps',
                            # 'mu_25/fdisk_0p01_lgMdm_6p0_V200-200kmps',
                            ]
        snaps_list = [400, 400, 400, 400, 400, 400,
                      101, 101, 101,] #101, 101, 101, 101, 101,] #101]
        cmap = 'viridis'

    if 'mu2' in name:
        galaxy_name_list = [
            'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps',
            'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
            'mu_2/fdisk_0p01_lgMdm_8_V200_200kmps',
            'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps',
            'mu_0p77/fdisk_0p01_lgMdm_8_V200_200kmps',
            'mu_0p5/fdisk_0p01_lgMdm_8_V200_200kmps',
            'mu_0p2/fdisk_0p01_lgMdm_8_V200_200kmps',
        ]
        snaps_list = [400, 400, 400, 400, 400, 400, 400]

        galaxy_name_list = [
            'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps',
            'mu_1_k_1/fdisk_0p01_lgMdm_8_V200_200kmps',
        ]
        snaps_list = [400, 400]

        cmap = 'viridis'

    if 'streaming' in name:
        galaxy_name_list = [
            # 'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps',
            # 'mu_1_k_1/fdisk_0p01_lgMdm_8_V200_200kmps',
            'mu_1_k_-0p03/fdisk_0p01_lgMdm_8_V200_200kmps',
            'mu_1_k_-0p1/fdisk_0p01_lgMdm_8_V200_200kmps',
            'mu_1_k_-0p3/fdisk_0p01_lgMdm_8_V200_200kmps',
            'mu_1_k_-0p6/fdisk_0p01_lgMdm_8_V200_200kmps',
        ]
        snaps_list = [400, 400, 400, 400, 400, 400, 400, 400]

        cmap = 'turbo'

    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(12, 14, forward=True)
    widths = [1, 1, 1]
    heights = [1, 1, 1, 1]
    spec = fig.add_gridspec(ncols=3, nrows=4, width_ratios=widths,
                            height_ratios=heights)

    min_t = 0
    min_y = 0
    max_y = 165
    # min_y2 = -10
    min_y2 = 0
    max_y2 = 240

    axs = []
    for row in range(len(heights)):
        axs.append([])
        for col in range(len(widths)):
            axs[row].append(fig.add_subplot(spec[row, col]))
            axs[row][col].set_xlim([min_t, T_TOT])

            if row == 0:
                axs[row][col].set_ylim([min_y2, max_y2])
            else:
                axs[row][col].set_ylim([min_y, max_y])
            if col != 0: axs[row][col].set_yticklabels([])
            if row != len(heights) - 1: axs[row][col].set_xticklabels([])

            # plt.semilogy()

    fig.subplots_adjust(hspace=0.0, wspace=0.0)

    # axs[0][0].set_ylabel(r'$\overline{v_\phi}$ [km/s]')
    axs[0][0].set_ylabel(r'$\overline{v}_\phi$ [km/s]')
    axs[1][0].set_ylabel(r'$\sigma_z$ [km/s]')
    axs[2][0].set_ylabel(r'$\sigma_R$ [km/s]')
    axs[3][0].set_ylabel(r'$\sigma_\phi$ [km/s]')
    axs[3][1].set_xlabel(r'$t$ [Gyr]')

    burn_in = BURN_IN

    bin_edges = rcm.get_bin_edges(save_name_append)

    # work out constatns
    (_, _, delta_dm, upsilon_dm, upsilon_circ
     ) = rcm.get_constants(bin_edges, save_name_append, 0)

    # load analytic values
    analytic_dispersion = rcm.get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0,
                                             v200=False, dm_dispersion=False, dm_v_circ=False,
                                             analytic_dispersion=True, analytic_v_c=False,
                                             save_name_append=save_name_append)

    analytic_v_circ = rcm.get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0,
                                         v200=False, dm_dispersion=False, dm_v_circ=False,
                                         analytic_dispersion=False, analytic_v_c=True,
                                         save_name_append=save_name_append)

    for ii, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = rcm.get_name_kwargs(galaxy_name)

        if 'soft1' in name:
            # _c = ((ii%(len(galaxy_name_list) //2)) + 0.01) / (len(galaxy_name_list) // 2 - 0.98)
            _c = ((ii%(len(galaxy_name_list) //2)) + 0.01 + 1) / (len(galaxy_name_list) // 2 - 0.98 + 2)
            kwargs['c'] = matplotlib.cm.get_cmap(cmap)(_c)
            if 2*ii < len(galaxy_name_list):
                kwargs['ls'] = '--'
            else:
                kwargs['ls'] = '-'

        if 'seeds' in name:
            _c = (ii + 1) / (len(galaxy_name_list) + 2)
            kwargs['c'] = matplotlib.cm.get_cmap(cmap)(_c)
            kwargs['ls'] = '-'

        if 'mu' in name:
            mu_map = 'viridis'
            if 'mu_1' in galaxy_name: kwargs['c'] = matplotlib.cm.get_cmap(mu_map)(0.1)
            if 'mu_5' in galaxy_name: kwargs['c'] = matplotlib.cm.get_cmap(mu_map)(0.5)
            if 'mu_25' in galaxy_name: kwargs['c'] = matplotlib.cm.get_cmap(mu_map)(0.9)

            if '8p0' in galaxy_name: kwargs['ls'] = '--'
            if '7p5' in galaxy_name: kwargs['ls'] = ':'
            if '7p0' in galaxy_name: kwargs['ls'] = '-'

        if 'mu2' in name:
            if 'mu_0p2'  in galaxy_name: kwargs['c'] = matplotlib.cm.get_cmap('plasma')(0.4)
            elif 'mu_0p5'in galaxy_name: kwargs['c'] = matplotlib.cm.get_cmap('plasma')(0.17)
            elif 'mu_0p77'in galaxy_name: kwargs['c'] = matplotlib.cm.get_cmap('plasma')(0.07)
            elif 'mu_1'  in galaxy_name: kwargs['c'] = matplotlib.cm.get_cmap('viridis')(0.10)
            elif 'mu_2/' in galaxy_name: kwargs['c'] = matplotlib.cm.get_cmap('viridis')(0.27)
            elif 'mu_5'  in galaxy_name: kwargs['c'] = matplotlib.cm.get_cmap('viridis')(0.5)
            elif 'mu_25' in galaxy_name: kwargs['c'] = matplotlib.cm.get_cmap('viridis')(0.9)

            if '8p0' in galaxy_name: kwargs['ls'] = '-'

        if 'streaming' in name:
            kwargs['c'] = matplotlib.cm.get_cmap('turbo')(ii / (len(galaxy_name_list) - 1))
            kwargs['ls'] = '-'

        # load data
        (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
         ) = rcm.get_dispersions(galaxy_name, save_name_append, snaps, bin_edges=bin_edges)

        wrong = (np.sum(np.isnan(mean_v_phi)), np.sum(np.isnan(sigma_z)), np.sum(np.isnan(sigma_R)), np.sum(np.isnan(sigma_phi)))
        if wrong != (0,0,0,0):
            print(galaxy_name, wrong)

        # time
        time = np.linspace(0, T_TOT, snaps + 1)
        time = time #- time[burn_in]

        # burn ins
        (burn_max_v, _, _, _
         ) = rcm.smooth_burn_ins(kwargs['mdm'], snaps, save_name_append, burn_in, bin_edges)

        (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
         ) = rcm.get_physical_from_model(galaxy_name, snaps, save_name_append)
        (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
         ) = (theory_best_v.T, theory_best_z2.T, theory_best_r2.T, theory_best_p2.T)

        window = snaps // window_n + 1
        if window % 2 == 0: window += 1
        for i in range(3):

            if 'seeds' not in name:
                if 'mu' not in name:
                    axs[0][i].errorbar(time, mean_v_phi[:, i],
                                       c=kwargs['c'], marker='', ls=kwargs['ls'], lw=lwt, alpha=alpha)
                    axs[1][i].errorbar(time, sigma_z[:, i],
                                       c=kwargs['c'], marker='', ls=kwargs['ls'], lw=lwt, alpha=alpha)
                    axs[2][i].errorbar(time, sigma_R[:, i],
                                       c=kwargs['c'], marker='', ls=kwargs['ls'], lw=lwt, alpha=alpha)
                    axs[3][i].errorbar(time, sigma_phi[:, i],
                                       c=kwargs['c'], marker='', ls=kwargs['ls'], lw=lwt, alpha=alpha)

                try:
                    axs[0][i].errorbar(time, savgol_filter(mean_v_phi[:, i],  window, poly_n, mode='interp'),
                                       c=kwargs['c'], marker='', ls=kwargs['ls'], lw=lw)
                except Exception: pass
                try:
                    axs[1][i].errorbar(time, savgol_filter(sigma_z[:, i],  window, poly_n, mode='interp'),
                                   c=kwargs['c'], marker='', ls=kwargs['ls'], lw=lw)
                except Exception: pass
                try:
                    axs[2][i].errorbar(time, savgol_filter(sigma_R[:, i],  window, poly_n, mode='interp'),
                                   c=kwargs['c'], marker='', ls=kwargs['ls'], lw=lw)
                except Exception: pass
                try:
                    axs[3][i].errorbar(time, savgol_filter(sigma_phi[:, i],  window, poly_n, mode='interp'),
                                   c=kwargs['c'], marker='', ls=kwargs['ls'], lw=lw)
                except Exception: pass

            else:
                # axs[0][i].errorbar(time, mean_v_phi[:, i],
                #                    c=kwargs['c'], marker='.', ls='', lw=1, ms=2)
                # axs[1][i].errorbar(time, sigma_z[:, i],
                #                    c=kwargs['c'], marker='.', ls='', lw=1, ms=2)
                # axs[2][i].errorbar(time, sigma_R[:, i],
                #                    c=kwargs['c'], marker='.', ls='', lw=1, ms=2)
                # axs[3][i].errorbar(time, sigma_phi[:, i],
                #                    c=kwargs['c'], marker='.', ls='', lw=1, ms=2)
                # axs[0][i].errorbar(time, mean_v_phi[:, i],
                #                    c=kwargs['c'], marker='', ls='-', lw=0.5, ms=2)
                # axs[1][i].errorbar(time, sigma_z[:, i],
                #                    c=kwargs['c'], marker='', ls='-', lw=0.5, ms=2)
                # axs[2][i].errorbar(time, sigma_R[:, i],
                #                    c=kwargs['c'], marker='', ls='-', lw=0.5, ms=2)
                # axs[3][i].errorbar(time, sigma_phi[:, i],
                #                    c=kwargs['c'], marker='', ls='-', lw=0.5, ms=2)

                # (smooth_mean_v_R, smooth_mean_v_phi, smooth_sigma_z, smooth_sigma_R, smooth_sigma_phi
                #  ) = rcm.get_dispersions(galaxy_name[:-6], save_name_append, snaps, bin_edges=bin_edges)
                #
                # smooth_mean_v_phi = savgol_filter(smooth_mean_v_phi[:, i], window, poly_n, mode='interp')
                # smooth_sigma_z = savgol_filter(smooth_sigma_z[:, i], window, poly_n, mode='interp')
                # smooth_sigma_R = savgol_filter(smooth_sigma_R[:, i], window, poly_n, mode='interp')
                # smooth_sigma_phi = savgol_filter(smooth_sigma_phi[:, i], window, poly_n, mode='interp')

                smooth_mean_v_phi = np.zeros(401)
                smooth_sigma_z = np.zeros(401)
                smooth_sigma_R = np.zeros(401)
                smooth_sigma_phi = np.zeros(401)

                # ms = 2
                # axs[0][i].errorbar(time, mean_v_phi[:, i] - smooth_mean_v_phi,
                #                    c=kwargs['c'], marker='.', ls='', ms=ms)
                # axs[1][i].errorbar(time, sigma_z[:, i] - smooth_sigma_z,
                #                    c=kwargs['c'], marker='.', ls='', ms=ms)
                # axs[2][i].errorbar(time, sigma_R[:, i] - smooth_sigma_R,
                #                    c=kwargs['c'], marker='.', ls='', ms=ms)
                # axs[3][i].errorbar(time, sigma_phi[:, i] - smooth_sigma_phi,
                #                    c=kwargs['c'], marker='.', ls='', ms=ms)

                # lwt = 1
                axs[0][i].errorbar(time, mean_v_phi[:, i] - smooth_mean_v_phi,
                                   c=kwargs['c'], marker='', ls='-', lw=lwt, alpha=alpha)
                axs[1][i].errorbar(time, sigma_z[:, i] - smooth_sigma_z,
                                   c=kwargs['c'], marker='', ls='-', lw=lwt, alpha=alpha)
                axs[2][i].errorbar(time, sigma_R[:, i] - smooth_sigma_R,
                                   c=kwargs['c'], marker='', ls='-', lw=lwt, alpha=alpha)
                axs[3][i].errorbar(time, sigma_phi[:, i] - smooth_sigma_phi,
                                   c=kwargs['c'], marker='', ls='-', lw=lwt, alpha=alpha)

                lw=2
                axs[0][i].errorbar(time, savgol_filter(mean_v_phi[:, i] - smooth_mean_v_phi, window, poly_n),
                                   c=kwargs['c'], marker='', ls='-', lw=lw, zorder=10)
                axs[1][i].errorbar(time, savgol_filter(sigma_z[:, i] - smooth_sigma_z, window, poly_n),
                                   c=kwargs['c'], marker='', ls='-', lw=lw, zorder=10)
                axs[2][i].errorbar(time, savgol_filter(sigma_R[:, i] - smooth_sigma_R, window, poly_n),
                                   c=kwargs['c'], marker='', ls='-', lw=lw, zorder=10)
                axs[3][i].errorbar(time, savgol_filter(sigma_phi[:, i] - smooth_sigma_phi, window, poly_n),
                                   c=kwargs['c'], marker='', ls='-', lw=lw, zorder=10)
                lw=3
                # ylims = [-10,10]
                # axs[0][i].set_ylim(ylims)
                # axs[1][i].set_ylim(ylims)
                # axs[2][i].set_ylim(ylims)
                # axs[3][i].set_ylim(ylims)

            if model:
                c = 'k'
                ls = '--'
                axs[0][i].errorbar(time, theory_best_v[:, i], c=c, ls=ls)
                axs[1][i].errorbar(time, np.sqrt(theory_best_z2[:, i]), c=c, ls=ls)
                axs[2][i].errorbar(time, np.sqrt(theory_best_r2[:, i]), c=c, ls=ls)
                axs[3][i].errorbar(time, np.sqrt(theory_best_p2[:, i]), c=c, ls=ls)

        c = kwargs['c']

        # if kwargs['c'] in ['C0', 'C1', 'C2']: dx = 0.05
        # else: dx = 0.45

        if 'soft1' in name:
            if 'eps' not in galaxy_name:

                dx = 0.25

                dy = 1.24 - 0.10 * ('7p0' in galaxy_name)
                dy2 = 0.97 - 0.10 * ('7p0' in galaxy_name)

                axs[0][2].errorbar(min_t + np.array([0.05, 0.2]) * (T_TOT - min_t),
                                   min_y2 + (dy - 0.06 * np.ones(2) - 1) * (max_y2 - min_y2),
                                   ls=['-', '--'][int('8p0' in galaxy_name)], lw=lw, c='grey')
                axs[1][2].errorbar(min_t + np.array([0.05, 0.2]) * (T_TOT - min_t),
                                   min_y + (dy2 - 0.06 * np.ones(2)) * (max_y - min_y),
                                   ls=['-', '--'][int('8p0' in galaxy_name)], lw=lw, c='grey')

                axs[1][2].text(min_t + dx * (T_TOT - min_t), min_y + dy * (max_y - min_y),
                               r'$\mathrm{N}_{\mathrm{DM}} = $' + kwargs['labn'],
                               ha='left', va='top', color=kwargs['c'])
                axs[1][2].text(min_t + dx * (T_TOT - min_t), min_y + dy2 * (max_y - min_y),
                               r'$m_{\mathrm{DM}} = $' + kwargs['lab'],
                               ha='left', va='top', color=kwargs['c'])

        elif 'seeds' in name:
            dx = 0.05
            dy = 0.97

            if ii == 0:
                axs[1][1].text(min_t + dx * (T_TOT - min_t), min_y + dy * (max_y - min_y),
                               r'$\mathrm{N}_{\mathrm{DM}} = $' + kwargs['labn'],
                               ha='left', va='top', color='k')

                axs[1][2].text(min_t + dx * (T_TOT - min_t), min_y + dy * (max_y - min_y),
                               r'$m_{\mathrm{DM}} = $' + kwargs['lab'],
                               ha='left', va='top', color='k')

        #comment this out if going back to colour for res and ls for mu
        elif 'mu' in name:
            dx = 0.25
            dy = 1.13 - 0.10 * (ii//3) - 0.05 * (ii!=0)

            if ii%3 == 0:
                axs[1][1].text(min_t + dx * (T_TOT - min_t), min_y + dy * (max_y - min_y),
                               r'$\mathrm{N}_{\mathrm{DM}} = $' + kwargs['labn'],
                               ha='left', va='top', color='k')

                axs[ii!=0][1].errorbar(min_t + np.array([0.05, 0.2]) * (T_TOT - min_t),
                                       [min_y2 + (dy-1 - 0.06 * np.ones(2)) * (max_y2 - min_y2),
                                        min_y + (dy - 0.06 * np.ones(2)) * (max_y - min_y)][ii!=0],
                                       ls=['-', ':', '--'][ii//3], lw=lw, c='grey')

            #     axs[1][2].text(min_t + dx * (T_TOT - min_t), min_y + dy * (max_y - min_y),
            #                    r'$m_{\mathrm{DM}} = $' + kwargs['lab'],
            #                    ha='left', va='top', color='k')

            #
            #     axs[((ii//3)>1)][2].errorbar(min_t + np.array([0.05, 0.2]) * (T_TOT - min_t),
            #                                  [min_y + (dy - 0.06 * np.ones(2)) * (max_y - min_y),
            #                               -max_y2 - min_y2 + (dy - 0.06 * np.ones(2)) * (max_y2 - min_y2)][(ii//3)<2],
            #                                  ls=['-', ':', '--'][ii//3], lw=lw, c='grey')

            dy = 0.97 - 0.10 * (ii//3)

            if ii%3 == 0:
                # axs[1][1].text(min_t + dx * (T_TOT - min_t), min_y + dy * (max_y - min_y),
                #                r'$\mathrm{N}_{\mathrm{DM}} = $' + kwargs['labn'],
                #                ha='left', va='top', color='k')

                # axs[1][1].errorbar(min_t + np.array([0.05, 0.2]) * (T_TOT - min_t),
                #                    min_y + (dy - 0.06 * np.ones(2)) * (max_y - min_y),
                #                    ls=['-', ':', '--'][ii//3], lw=lw, c='grey')

                axs[1][2].text(min_t + dx * (T_TOT - min_t), min_y + dy * (max_y - min_y),
                               r'$m_{\mathrm{DM}} = $' + kwargs['lab'],
                               ha='left', va='top', color='k')

                axs[1][2].errorbar(min_t + np.array([0.05, 0.2]) * (T_TOT - min_t),
                                   min_y + (dy - 0.06 * np.ones(2)) * (max_y - min_y),
                                   ls=['-', ':', '--'][ii//3], lw=lw, c='grey')

        else:
            dx = 0.05
            dy = 0.93 - 0.10 * int(kwargs['c'][1]) + 0.3 - 0.05 * (kwargs['c'] in ['C2', 'C3', 'C4'])

            axs[1][1].text(min_t + dx * (T_TOT - min_t), min_y + dy * (max_y - min_y),
                           r'$\mathrm{N}_{\mathrm{DM}} = $' + kwargs['labn'],
                           ha='left', va='top', color=kwargs['c'])

            axs[1][2].text(min_t + dx * (T_TOT - min_t), min_y + dy * (max_y - min_y),
                           r'$m_{\mathrm{DM}} = $' + kwargs['lab'],
                           ha='left', va='top', color=kwargs['c'])

        if 'soft1' in name:
            if ii < 5:
                dx = 0.05 #0.53
                dy = 1.23 - 0.10 * int(4 - ii) - 0.05 * (4 - ii > 1)

                # if ii == 0:
                #     dy = 1.23
                #     axs[1][1].text(min_t + dx * (T_TOT - min_t), min_y + dy * (max_y - min_y),
                #              '\n' * 5 + r'$\epsilon / z_d = $' + str(eps_list[ii]) + r'   $\,$', ha='left', va='top',
                #              color=c,
                #              bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8), linespacing=1.02)
                # else:
                #     axs[1][1].text(min_t + dx * (T_TOT - min_t), min_y + dy * (max_y - min_y),
                #              r'$\epsilon / z_d = $' + str(eps_list[ii]), ha='left', va='top', color=c)

                if ii == 0:
                    # dy = 1.23
                    dy += 2 * 0.10
                    axs[1][1].text(min_t + dx * (T_TOT - min_t), min_y + dy * (max_y - min_y),
                             '\n' * 2 + r'$\epsilon / z_d = $' + str(eps_list[ii]), ha='left', va='top',
                             color=c,
                             bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8), linespacing=1.02)
                elif ii == 3:
                    dy += 0.10
                    axs[1][1].text(min_t + dx * (T_TOT - min_t), min_y + dy * (max_y - min_y),
                             '\n' * 1 + r'$\epsilon / z_d = $' + str(eps_list[ii]) + r' $\,$', ha='left', va='top',
                             color=c,
                             bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8), linespacing=1.02)
                else:
                    axs[1][1].text(min_t + dx * (T_TOT - min_t), min_y + dy * (max_y - min_y),
                             r'$\epsilon / z_d = $' + str(eps_list[ii]), ha='left', va='top', color=c)


        if 'mu' in name:
            if ii < 3:
                # dx = 0.25
                # dy2 = 0.83 - 0.10 * (2-ii)
                # axs[1][2].errorbar(min_t + np.array([0.05, 0.2]) * (T_TOT - min_t),
                #                    min_y + (dy2 - 0.06 * np.ones(2)) * (max_y - min_y),
                #                    ls=[':', '-', '--'][ii], lw=lw, c='grey')
                # axs[1][2].text(min_t + dx * (T_TOT - min_t), min_y + dy2 * (max_y - min_y),
                #                kwargs['mu'],
                #                ha='left', va='top')

                dx = 0.05
                dy2 = 0.33 - 0.10 * (2-ii)
                axs[0][2].text(min_t + dx * (T_TOT - min_t), min_y + dy2 * (max_y2 - min_y2),
                               kwargs['mu'],
                               c=kwargs['c'], #path_effects=pth_2,
                               ha='left', va='top')

    if 'seeds' in name:
    # if False:

        # load data
        (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
         ) = rcm.get_dispersions(galaxy_name[:-6], save_name_append, snaps, bin_edges=bin_edges)
        
        lw=1.2
        ls='-'#(0,(5,5))#'--'
        c='k'
        for i in range(3):
            # axs[0][i].errorbar(time, mean_v_phi[:, i],
            #                    c=kwargs['c'], marker='', ls=kwargs['ls'], lw=lwt, alpha=alpha)
            try:
                axs[0][i].errorbar(time, savgol_filter(mean_v_phi[:, i], window, poly_n, mode='interp'),
                                   c=c, marker='', ls=ls, lw=lw, zorder=20)
            except Exception:
                pass
            # axs[1][i].errorbar(time, sigma_z[:, i],
            #                    c='k', marker='', ls=kwargs['ls'], lw=lwt, alpha=alpha)
            try:
                axs[1][i].errorbar(time, savgol_filter(sigma_z[:, i], window, poly_n, mode='interp'),
                                   c=c, marker='', ls=ls, lw=lw, zorder=20)
            except Exception:
                pass
            # axs[2][i].errorbar(time, sigma_R[:, i],
            #                    c='k', marker='', ls=kwargs['ls'], lw=lwt, alpha=alpha)
            try:
                axs[2][i].errorbar(time, savgol_filter(sigma_R[:, i], window, poly_n, mode='interp'),
                                   c=c, marker='', ls=ls, lw=lw, zorder=20)
            except Exception:
                pass
            # axs[3][i].errorbar(time, sigma_phi[:, i],
            #                    c='k', marker='', ls=kwargs['ls'], lw=lwt, alpha=alpha)
            try:
                axs[3][i].errorbar(time, savgol_filter(sigma_phi[:, i], window, poly_n, mode='interp'),
                                   c=c, marker='', ls=ls, lw=lw, zorder=20)
            except Exception:
                pass

    # asymptote max
    ls = '--'
    c = 'grey'
    linewidth = 5

    cut0 = 0.7
    cut1 = 0.7
    if 'soft1' in name:
        cut0 = 0.5
        cut1 = 0.88
    if 'mu' in name:
        cut0 = 0.9
        cut1 = 0.9

    for i in range(3):
        axs[0][i].errorbar([0, T_TOT], [0]*2,
                           ls=ls, c=c, alpha=0.5, linewidth=linewidth, zorder=-2)
        axs[0][i].errorbar([0, T_TOT], [analytic_v_circ[i]]*2,
                           ls=ls, c=c, alpha=0.5, linewidth=linewidth, zorder=-2)

        for j in range(3):
            if  j == 0 and i == 1:
                axs[j + 1][i].errorbar([cut0 * T_TOT, T_TOT], [analytic_dispersion[i]] * 2,
                                       ls=ls, c=c, alpha=0.5, linewidth=linewidth, zorder=-2)
            elif  j == 0 and i == 2:
                axs[j + 1][i].errorbar([cut1 * T_TOT, T_TOT], [analytic_dispersion[i]] * 2,
                                       ls=ls, c=c, alpha=0.5, linewidth=linewidth, zorder=-2)
            else:
                axs[j + 1][i].errorbar([0, T_TOT], [analytic_dispersion[i]]*2,
                                       ls=ls, c=c, alpha=0.5, linewidth=linewidth, zorder=-2)

    zp25 = 0.25
    if 'seeds' in name:
        zp25 = 0.05
    zp50 = zp25
    if 'mu' in name:
        zp25 = 0.35
        zp50 = 0.15
    axs[0][0].text(min_t + 0.05 * (T_TOT - min_t), min_y2 + 0.05 * (max_y2 - min_y2),
                   r'$R = R_{1/4, 0}$',
                   ha='left', va='bottom')
    axs[0][1].text(min_t + 0.05 * (T_TOT - min_t), min_y2 + zp50 * (max_y2 - min_y2),
                   r'$R = R_{1/2, 0}$',
                   ha='left', va='bottom')
    axs[0][2].text(min_t + 0.05 * (T_TOT - min_t), min_y2 + zp25 * (max_y2 - min_y2),
                   r'$R = R_{3/4, 0}$',
                   ha='left', va='bottom')

    axs[0][0].text(min_t + 0.05 * (T_TOT - min_t), analytic_v_circ[0] + 0.03 * (max_y2 - min_y2),
                   r'$V_c$',
                   ha='left', va='bottom')
    axs[1][0].text(min_t + 0.05 * (T_TOT - min_t), analytic_dispersion[0] - 0.01 * (max_y2 - min_y2),
                   r'$\sigma_{\mathrm{DM}}$',
                   ha='left', va='top')

    axs[0][0].text(min_t + 0.95 * (T_TOT - min_t), min_y2 + 0.95 * (max_y2 - min_y2),
                   r'$V_{200} = 200$ km/s',
                   ha='right', va='top')
    #
    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return


def plot_smooth_time_evolution(model=False, name='', save=False):
    # if model:
    #     raise NotImplementedError

    galaxy_name_list2 = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                         'mu_5_smooth/fdisk_0p01_lgMdm_8p0_V200-200kmps']
    snaps_list2 = [400,101]
    galaxy_name_list1 = ['mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                         'mu_5_smooth/fdisk_0p01_lgMdm_7p0_V200-200kmps']
    snaps_list1 = [101,101]
    galaxy_name_list0 = ['mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps',
                         'mu_5_smooth/fdisk_0p01_lgMdm_6p0_V200-200kmps']
    snaps_list0 = [101,101]

    gnl = [galaxy_name_list0, galaxy_name_list1, galaxy_name_list2]
    sl = [snaps_list0, snaps_list1, snaps_list2]

    _index = 1

    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(12, 14, forward=True)
    widths = [1, 1, 1]
    heights = [1, 1, 1, 1]
    spec = fig.add_gridspec(ncols=3, nrows=4, width_ratios=widths,
                            height_ratios=heights)

    min_t = 0
    min_y = 0
    max_y = 165
    # min_y2 = -10
    min_y2 = 0
    max_y2 = 240

    axs = []
    for row in range(len(heights)):
        axs.append([])
        for col in range(len(widths)):
            axs[row].append(fig.add_subplot(spec[row, col]))
            axs[row][col].set_xlim([min_t, T_TOT])

            if row == 0:
                axs[row][col].set_ylim([min_y2, max_y2])
            else:
                axs[row][col].set_ylim([min_y, max_y])
            if col != 0: axs[row][col].set_yticklabels([])
            if row != len(heights) - 1: axs[row][col].set_xticklabels([])

            # plt.semilogy()

    fig.subplots_adjust(hspace=0.0, wspace=0.0)

    # axs[0][0].set_ylabel(r'$\overline{v_\phi}$ [km/s]')
    axs[0][0].set_ylabel(r'$\overline{v}_\phi$ [km/s]')
    axs[1][0].set_ylabel(r'$\sigma_z$ [km/s]')
    axs[2][0].set_ylabel(r'$\sigma_R$ [km/s]')
    axs[3][0].set_ylabel(r'$\sigma_\phi$ [km/s]')
    axs[3][1].set_xlabel(r'$t$ [Gyr]')

    burn_in = BURN_IN

    bin_edges = rcm.get_bin_edges(save_name_append)

    # work out constatns
    (_, _, delta_dm, upsilon_dm, upsilon_circ
     ) = rcm.get_constants(bin_edges, save_name_append, 0)

    # load analytic values
    analytic_dispersion = rcm.get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0,
                                             v200=False, dm_dispersion=False, dm_v_circ=False,
                                             analytic_dispersion=True, analytic_v_c=False,
                                             save_name_append=save_name_append)

    analytic_v_circ = rcm.get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0,
                                         v200=False, dm_dispersion=False, dm_v_circ=False,
                                         analytic_dispersion=False, analytic_v_c=True,
                                         save_name_append=save_name_append)

    for i, (galaxy_name_list, snaps_list) in enumerate(zip(gnl, sl)):
        for galaxy_name, snaps in zip(galaxy_name_list, snaps_list):
            kwargs = rcm.get_name_kwargs(galaxy_name)

            # load data
            (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
             ) = rcm.get_dispersions(galaxy_name, save_name_append, snaps, bin_edges=bin_edges)

            # time
            time = np.linspace(0, T_TOT, snaps + 1)
            time = time #- time[burn_in]

            if model:
                # burn ins
                (burn_max_v, _, _, _
                 ) = rcm.smooth_burn_ins(kwargs['mdm'], snaps, save_name_append, burn_in, bin_edges)

                (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
                 ) = rcm.get_physical_from_model(galaxy_name, snaps, save_name_append)
                (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
                 ) = (theory_best_v.T, theory_best_z2.T, theory_best_r2.T, theory_best_p2.T)

            window = snaps // window_n + 1
            if window % 2 == 0: window += 1

            axs[0][i].errorbar(time, mean_v_phi[:, _index],
                               c=kwargs['c'], marker='', ls=kwargs['ls'], lw=lwt, alpha=alpha)
            axs[0][i].errorbar(time, savgol_filter(mean_v_phi[:, _index],  window, poly_n, mode='interp'),
                               c=kwargs['c'], marker='', ls=kwargs['ls'], lw=lw)
            axs[1][i].errorbar(time, sigma_z[:, _index],
                               c=kwargs['c'], marker='', ls=kwargs['ls'], lw=lwt, alpha=alpha)
            axs[1][i].errorbar(time, savgol_filter(sigma_z[:, _index],  window, poly_n, mode='interp'),
                               c=kwargs['c'], marker='', ls=kwargs['ls'], lw=lw)
            axs[2][i].errorbar(time, sigma_R[:, _index],
                               c=kwargs['c'], marker='', ls=kwargs['ls'], lw=lwt, alpha=alpha)
            axs[2][i].errorbar(time, savgol_filter(sigma_R[:, _index],  window, poly_n, mode='interp'),
                               c=kwargs['c'], marker='', ls=kwargs['ls'], lw=lw)
            axs[3][i].errorbar(time, sigma_phi[:, _index],
                               c=kwargs['c'], marker='', ls=kwargs['ls'], lw=lwt, alpha=alpha)
            axs[3][i].errorbar(time, savgol_filter(sigma_phi[:, _index],  window, poly_n, mode='interp'),
                               c=kwargs['c'], marker='', ls=kwargs['ls'], lw=lw)

        if model:
            c = 'k'
            ls = '--'
            axs[0][i].errorbar(time, theory_best_v[:, _index], c=c, ls=ls)
            axs[1][i].errorbar(time, np.sqrt(theory_best_z2[:, _index]), c=c, ls=ls)
            axs[2][i].errorbar(time, np.sqrt(theory_best_r2[:, _index]), c=c, ls=ls)
            axs[3][i].errorbar(time, np.sqrt(theory_best_p2[:, _index]), c=c, ls=ls)

        dx = 0.05
        dy = 0.88
        axs[1][i].text(min_t + dx * (T_TOT - min_t), min_y + dy * (max_y - min_y),
                       r'$\mathrm{N}_{\star} = $' + kwargs['labnstar'],
                       ha='left', va='bottom', color=kwargs['c'])

        axs[1][i].text(min_t + dx * (T_TOT - min_t), min_y + (dy - 0.12) * (max_y - min_y),
                       r'$m_{\star} = $' + kwargs['labstar'],
                       ha='left', va='bottom', color=kwargs['c'])

    # asymptote max
    ls = '--'
    c = 'grey'
    linewidth = 5

    for i in range(3):
        axs[0][i].errorbar([0, T_TOT], [0]*2,
                           ls=ls, c=c, alpha=0.5, linewidth=linewidth, zorder=-2)
        axs[0][i].errorbar([0, T_TOT], [analytic_v_circ[_index]]*2,
                           ls=ls, c=c, alpha=0.5, linewidth=linewidth, zorder=-2)

        for j in range(3):
            if j == 0 and i != 2:
                axs[j + 1][i].errorbar([0.68 * T_TOT, T_TOT], [analytic_dispersion[_index]] * 2,
                           ls=ls, c=c, alpha=0.5, linewidth=linewidth, zorder=-2)
            elif j == 0 and i == 2:
                axs[j + 1][i].errorbar([0.52 * T_TOT, T_TOT], [analytic_dispersion[_index]] * 2,
                           ls=ls, c=c, alpha=0.5, linewidth=linewidth, zorder=-2)
            else:
                axs[j + 1][i].errorbar([0, T_TOT], [analytic_dispersion[_index]]*2,
                           ls=ls, c=c, alpha=0.5, linewidth=linewidth, zorder=-2)

    axs[0][1].legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='-', linewidth=3)),
                      (lines.Line2D([0, 1], [0, 1], color='grey', ls='-.', linewidth=3)),],
                     ['Live halo', 'Smooth halo'],
                     loc='lower left', handlelength=2, labelspacing=0.2, frameon=False)

    axs[0][0].text(min_t + 0.05 * (T_TOT - min_t), min_y2 + 0.05 * (max_y2 - min_y2),
                   r'$R = R_{1/2, 0}$',
                   ha='left', va='bottom')
    axs[0][0].text(min_t + 0.05 * (T_TOT - min_t), analytic_v_circ[_index] + 0.03 * (max_y2 - min_y2),
                   r'$V_c$',
                   ha='left', va='bottom')
    axs[2][0].text(min_t + 0.05 * (T_TOT - min_t), analytic_dispersion[_index] - 0.01 * (max_y2 - min_y2),
                   r'$\sigma_{\mathrm{DM}}$',
                   ha='left', va='top')

    axs[0][0].text(min_t + 0.95 * (T_TOT - min_t), min_y2 + 0.95 * (max_y2 - min_y2),
                   r'$V_{200} = 200$ km/s',
                   ha='right', va='top')
    #

    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return


def  plot_velocity_profiles_after_t(t=5, lab=True, show_ics=True, show_dms=False, model=True, logy=False,
                                    assym=False, eps=False, mu=False, name='', save=True):

    if eps:
        # galaxy_name_list = ['eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_0p25soft',
        #                     'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_0p5soft',
        #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
        #                     'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_2soft',
        #                     'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_4soft']
        # snaps_list = [101,101,400,101,101]

        # galaxy_name_list = ['eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_0p25soft',
        #                     'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_0p5soft',
        #                     'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
        #                     'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_2soft',
        #                     'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_4soft']
        # snaps_list = [101,101,101,101,101]

        galaxy_name_list = ['eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_4soft',
                            'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_2soft',
                            'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                            'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_0p5soft',
                            'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_0p25soft',
                            'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_4soft',
                            'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_2soft',
                            'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                            'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_0p5soft',
                            'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_0p25soft']
        snaps_list = [101,101,400,101,101, 101,101,101,101,101]
        eps_list = [4, 2, 1, 0.5, 0.25]

        cmap = 'twilight_r' #'cividis_r' #'cool' #'gist_rainbow' #

    elif mu:
        galaxy_name_list = [
            'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps',
            'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
            'mu_2/fdisk_0p01_lgMdm_8_V200_200kmps',
            'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps',
            'mu_0p77/fdisk_0p01_lgMdm_8_V200_200kmps',
            'mu_0p5/fdisk_0p01_lgMdm_8_V200_200kmps',
            'mu_0p2/fdisk_0p01_lgMdm_8_V200_200kmps',
        ]
        snaps_list = [400, 400, 400, 400, 400, 400, 400]

    else:
        galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                            'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                            'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                            'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
                            'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps']
        snaps_list = [400, 400, 101, 101, 101]


    fig = plt.figure()
    fig.set_size_inches(4, 14, forward=True)
    # fig.set_size_inches(7.2, 16, forward=True)

    # fits for comparison
    print('\nbeta=0, gamma=0 (fiducial)')
    (ln_Lambda_ks, alphas, betas, gammas
     ) = rcm.find_least_log_squares_best_fit(
        save_name_append, mcmc=False, fixed_b=0, fixed_g=0)

    ax0 = plt.subplot(4, 1, 1)
    ax1 = plt.subplot(4, 1, 2)
    ax2 = plt.subplot(4, 1, 3)
    ax3 = plt.subplot(4, 1, 4)

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    ax0.set_xticklabels([])
    ax1.set_xticklabels([])
    ax2.set_xticklabels([])

    ax0.set_ylabel(r'$\overline{v}_\phi / V_{200}$')
    ax1.set_ylabel(r'$\sigma_z / V_{200}$')
    ax2.set_ylabel(r'$\sigma_R / V_{200}$')
    ax3.set_ylabel(r'$\sigma_\phi / V_{200}$')

    if logy:
        ax0.set_ylabel(r'$\log \, \overline{v}_\phi / V_{200}$')
        ax1.set_ylabel(r'$\log \, \sigma_z / V_{200}$')
        ax2.set_ylabel(r'$\log \, \sigma_R / V_{200}$')
        ax3.set_ylabel(r'$\log \, \sigma_\phi / V_{200}$')

    ax3.set_xlabel('$\log \, R / r_{200} $')

    xlim = [-2.8, 0.2]
    ax0.set_xlim(xlim)
    ax1.set_xlim(xlim)
    ax2.set_xlim(xlim)
    ax3.set_xlim(xlim)

    slim = [0, 0.85]
    vlim = [0, 1.3]

    if logy:
        log_smin = -1
        # slim = [log_smin, np.log10(slim[1])]
        slim = [-0.8, 0.2]
        vlim = [np.log10(vlim[1]) - 1, np.log10(vlim[1])]

    ax0.set_ylim(vlim)
    ax1.set_ylim(slim)
    ax2.set_ylim(slim)
    ax3.set_ylim(slim)

    v200 = rcm.get_v_200()
    r200 = rcm.get_r_200()

    # bins
    n_bins = 21
    # bin_edges = np.logspace(-1, 3, 31)
    bin_edges = np.logspace(-1, 3, n_bins)

    bin_centres = 10 ** ((np.log10(bin_edges[:-1]) + np.log10(bin_edges[1:])) / 2)
    bin_edges = np.roll(np.repeat(bin_edges, 2), -1)[:-2]

    x_data = np.log10(bin_centres / r200)

    (inital_velocity_v, inital_velocity_z, inital_velocity_r, inital_velocity_p
     ) = rcm.smooth_zero_snap(0, 0, save_name_append, bin_edges)

    if show_ics:
        if not logy:
            ax0.errorbar(x_data, (inital_velocity_v / v200), c='k', ls='-.')
            ax1.errorbar(x_data, (inital_velocity_z / v200), c='k', ls='-.')
            ax2.errorbar(x_data, (inital_velocity_r / v200), c='k', ls='-.')
            ax3.errorbar(x_data, (inital_velocity_p / v200), c='k', ls='-.')
        else:
            ax0.errorbar(x_data, np.log10(inital_velocity_v / v200), c='k', ls='-.')
            ax1.errorbar(x_data, np.log10(inital_velocity_z / v200), c='k', ls='-.')
            ax2.errorbar(x_data, np.log10(inital_velocity_r / v200), c='k', ls='-.')
            ax3.errorbar(x_data, np.log10(inital_velocity_p / v200), c='k', ls='-.')

    #halo
    analytic_dispersion = rcm.get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name_list[0])
    analytic_v_circ = rcm.get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                         save_name_append=save_name_append, galaxy_name=galaxy_name_list[0])
    V200 = rcm.get_v_200(galaxy_name=galaxy_name_list[0])

    if not logy:
        if not 'dm_vc' in name:
            ax0.errorbar(x_data, (analytic_v_circ / V200), ls='-', c='k')
        if not show_dms:
            ax1.errorbar(x_data, (analytic_dispersion / V200), ls='-', c='k')
            ax2.errorbar(x_data, (analytic_dispersion / V200), ls='-', c='k')
            ax3.errorbar(x_data, (analytic_dispersion / V200), ls='-', c='k')
    else:
        if not 'dm_vc' in name:
            ax0.errorbar(x_data, np.log10(analytic_v_circ / V200), ls='-', c='k')
        if not show_dms:
            ax1.errorbar(x_data, np.log10(analytic_dispersion / V200), ls='-', c='k')
            ax2.errorbar(x_data, np.log10(analytic_dispersion / V200), ls='-', c='k')
            ax3.errorbar(x_data, np.log10(analytic_dispersion / V200), ls='-', c='k')

    #do all galaxies
    for ii, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = rcm.get_name_kwargs(galaxy_name)
        snap = int(round(t / T_TOT * snaps))

        # bins
        bin_edges = np.logspace(-1, 3, n_bins)

        bin_centres = 10 ** ((np.log10(bin_edges[:-1]) + np.log10(bin_edges[1:])) / 2)
        bin_edges = np.roll(np.repeat(bin_edges, 2), -1)[:-2]

        x_data = np.log10(bin_centres / r200)

        ten = 1
        n_star_min = 50
        if 'lgMdm_8p0' in galaxy_name:
            ten = 10
        elif 'lgMdm_7p5' in galaxy_name and 'smooth' in galaxy_name:
            ten = 4
        elif 'lgMdm_7p5' in galaxy_name:
            ten = 5

        if 'eps' in galaxy_name:
            ten = 1
            n_star_min = 50

        elif mu:
            ten = 10
            n_star_min = 2

        if ten != 1:

            Sigma = np.zeros((ten, len(x_data)))
            sigma_z = np.zeros((ten, len(x_data)))
            sigma_R = np.zeros((ten, len(x_data)))
            sigma_phi = np.zeros((ten, len(x_data)))
            mean_v_phi = np.zeros((ten, len(x_data)))
            r_current_half_seeds = np.zeros(ten)
            sigma_phi_half_seeds = np.zeros(ten)
            mean_v_phi_half_seeds = np.zeros(ten)
            sigma_r_half_seeds = np.zeros(ten)
            sigma_z_half_seeds = np.zeros(ten)

            # mask = np.zeros((ten, len(x_data)), dtype=bool)
            mask = np.zeros((ten, len(x_data)))

            for i in range(ten):
                if i > 1:
                    galaxy_name = galaxy_name[:-6]
                if i != 0:
                    galaxy_name += f'_seed{i}'

                #TODO put seeds in calc_profile_to_plot rather than do manually in plot function.
                (r_half_current_seed, sigma_p_r_half_seed, mean_v_phi_r_half_seed, sigma_r_r_half_seed, sigma_z_r_half_seed,
                 _mask, sigma_phi_seed, mean_v_phi_seed, sigma_R_seed, sigma_z_seed, Sigma_seed
                 ) = rcm.calc_profile_to_plot(galaxy_name, snap, bin_edges, n_star_min=n_star_min)

                Sigma[i, :] = Sigma_seed
                sigma_z[i, :] = sigma_z_seed
                sigma_R[i, :] = sigma_R_seed
                sigma_phi[i, :] = sigma_phi_seed
                mean_v_phi[i, :] = mean_v_phi_seed

                mask[i, :] = _mask

                r_current_half_seeds[i] = r_half_current_seed
                sigma_phi_half_seeds[i] = sigma_p_r_half_seed
                mean_v_phi_half_seeds[i] = mean_v_phi_r_half_seed
                sigma_z_half_seeds[i] = sigma_z_r_half_seed
                sigma_r_half_seeds[i] = sigma_r_r_half_seed

            Sigma = np.median(Sigma, axis=0)
            sigma_z = np.median(sigma_z, axis=0)
            sigma_R = np.median(sigma_R, axis=0)
            sigma_phi = np.median(sigma_phi, axis=0)
            mean_v_phi = np.median(mean_v_phi, axis=0)

            n_star = np.sum(mask, axis=0)

            mask = np.median(mask, axis=0)
            mask = mask > n_star_min
            # mask = mask.astype(bool)

            r_half_current = np.median(r_current_half_seeds)
            sigma_p_r_half = np.median(sigma_phi_half_seeds)
            mean_v_phi_r_half = np.median(mean_v_phi_half_seeds)
            sigma_r_r_half = np.median(sigma_r_half_seeds)
            sigma_z_r_half = np.median(sigma_z_half_seeds)

        else:
            (r_half_current, sigma_p_r_half, mean_v_phi_r_half, sigma_r_r_half, sigma_z_r_half,
             mask, sigma_phi, mean_v_phi, sigma_R, sigma_z, Sigma
             ) = rcm.calc_profile_to_plot(galaxy_name, snap, bin_edges, n_star_min=n_star_min)

            n_star = mask
            mask = mask > n_star_min

        # plot data
        lw = 3

        if eps:
            # _c = ((ii%(len(galaxy_name_list) //2)) + 0.01) / (len(galaxy_name_list) // 2 - 0.98)
            _c = ((ii%(len(galaxy_name_list) //2)) + 0.01 + 1) / (len(galaxy_name_list) // 2 - 0.98 + 2)
            c = matplotlib.cm.get_cmap(cmap)(_c)
            if 2*ii < len(galaxy_name_list):
                ls = '--'
            else:
                ls = '-'

        elif mu:
            c = 'k'
            if 'mu_0p2'  in galaxy_name: c = matplotlib.cm.get_cmap('plasma')(0.8)
            elif 'mu_0p5'in galaxy_name: c = matplotlib.cm.get_cmap('plasma')(0.5)
            elif 'mu_0p77'in galaxy_name: c = matplotlib.cm.get_cmap('plasma')(0.2)
            elif 'mu_1'  in galaxy_name: c = matplotlib.cm.get_cmap('viridis')(0.10)
            elif 'mu_2/' in galaxy_name: c = matplotlib.cm.get_cmap('viridis')(0.27)
            elif 'mu_5'  in galaxy_name: c = matplotlib.cm.get_cmap('viridis')(0.5)
            elif 'mu_25' in galaxy_name: c = matplotlib.cm.get_cmap('viridis')(0.9)
            ls = '-'

        else:
            c = kwargs['c']
            ls = '-'

        # ax0.errorbar(x_data[mask], (mean_v_phi[mask] / v200),  # / analytic_dispersion[:-1][mask],
        #              ls=ls, c=c, lw=lw)
        # ax1.errorbar(x_data[mask], (sigma_z[mask] / v200),  # / analytic_v_circ[:-1][mask],
        #              ls=ls, c=c, lw=lw)
        # ax2.errorbar(x_data[mask], (sigma_R[mask] / v200),  # / analytic_v_circ[:-1][mask],
        #               ls=ls, c=c, lw=lw)
        # ax3.errorbar(x_data[mask], (sigma_phi[mask] / v200),  # / analytic_v_circ[:-1][mask],
        #              ls=ls, c=c, lw=lw)

        if eps:
            ax0.errorbar(x_data[mask], (mean_v_phi / v200)[mask], ((mean_v_phi / v200)/np.sqrt(2*n_star))[mask],
                         ls=ls, c=c, lw=lw, capsize=4, capthick=lw)
            ax1.errorbar(x_data[mask], (sigma_z / v200)[mask],   ((sigma_z / v200)/np.sqrt(2*n_star))[mask],
                         ls=ls, c=c, lw=lw, capsize=4, capthick=lw)
            ax2.errorbar(x_data[mask], (sigma_R / v200)[mask],   ((sigma_R / v200)/np.sqrt(2*n_star))[mask],
                          ls=ls, c=c, lw=lw, capsize=4, capthick=lw)
            ax3.errorbar(x_data[mask], (sigma_phi / v200)[mask], ((sigma_phi / v200)/np.sqrt(2*n_star))[mask],
                         ls=ls, c=c, lw=lw, capsize=4, capthick=lw)
        else:
            if not logy:
                ax0.errorbar(x_data[mask], (mean_v_phi[mask] / v200),  # / analytic_dispersion[:-1][mask],
                             ls=ls, c=c, lw=lw)
                ax1.errorbar(x_data[mask], (sigma_z[mask] / v200),  # / analytic_v_circ[:-1][mask],
                             ls=ls, c=c, lw=lw)
                ax2.errorbar(x_data[mask], (sigma_R[mask] / v200),  # / analytic_v_circ[:-1][mask],
                              ls=ls, c=c, lw=lw)
                ax3.errorbar(x_data[mask], (sigma_phi[mask] / v200),  # / analytic_v_circ[:-1][mask],
                             ls=ls, c=c, lw=lw)
            else:
                ax0.errorbar(x_data[mask], np.log10(mean_v_phi[mask] / v200),  # / analytic_dispersion[:-1][mask],
                             ls=ls, c=c, lw=lw)
                ax1.errorbar(x_data[mask], np.log10(sigma_z[mask] / v200),  # / analytic_v_circ[:-1][mask],
                             ls=ls, c=c, lw=lw)
                ax2.errorbar(x_data[mask], np.log10(sigma_R[mask] / v200),  # / analytic_v_circ[:-1][mask],
                              ls=ls, c=c, lw=lw)
                ax3.errorbar(x_data[mask], np.log10(sigma_phi[mask] / v200),  # / analytic_v_circ[:-1][mask],
                             ls=ls, c=c, lw=lw)

        if False:
            lw=1
            ax0.errorbar(x_data, (mean_v_phi / v200),  # / analytic_dispersion[:-1][mask],
                         ls=':', c=c, lw=lw)
            ax1.errorbar(x_data, (sigma_z / v200),  # / analytic_v_circ[:-1][mask],
                         ls=':', c=c, lw=lw)
            ax2.errorbar(x_data, (sigma_R / v200),  # / analytic_v_circ[:-1][mask],
                          ls=':', c=c, lw=lw)
            ax3.errorbar(x_data, (sigma_phi / v200),  # / analytic_v_circ[:-1][mask],
                         ls=':', c=c, lw=lw)

        if not eps:
            s = 10 ** 2
            if not logy:
                ax0.scatter(np.log10(r_half_current / r200), (mean_v_phi_r_half / v200),  # / analytic_dispersion[:-1][mask],
                            c=np.array([c]), s=s, ec='k', linewidths=1.6, zorder=10)
                ax1.scatter(np.log10(r_half_current / r200), (sigma_z_r_half / v200),  # / analytic_v_circ[:-1][mask],
                            c=np.array([c]), s=s, ec='k', linewidths=1.6, zorder=10)
                ax2.scatter(np.log10(r_half_current / r200), (sigma_r_r_half / v200),  # / analytic_v_circ[:-1][mask],
                            c=np.array([c]), s=s, ec='k', linewidths=1.6, zorder=10)
                ax3.scatter(np.log10(r_half_current / r200), (sigma_p_r_half / v200),  # / analytic_v_circ[:-1][mask],
                            c=np.array([c]), s=s, ec='k', linewidths=1.6, zorder=10)
            else:
                ax0.scatter(np.log10(r_half_current / r200), np.log10(mean_v_phi_r_half / v200),  # / analytic_dispersion[:-1][mask],
                            c=np.array([c]), s=s, ec='k', linewidths=1.6, zorder=10)
                ax1.scatter(np.log10(r_half_current / r200), np.log10(sigma_z_r_half / v200),  # / analytic_v_circ[:-1][mask],
                            c=np.array([c]), s=s, ec='k', linewidths=1.6, zorder=10)
                ax2.scatter(np.log10(r_half_current / r200), np.log10(sigma_r_r_half / v200),  # / analytic_v_circ[:-1][mask],
                            c=np.array([c]), s=s, ec='k', linewidths=1.6, zorder=10)
                ax3.scatter(np.log10(r_half_current / r200), np.log10(sigma_p_r_half / v200),  # / analytic_v_circ[:-1][mask],
                            c=np.array([c]), s=s, ec='k', linewidths=1.6, zorder=10)

        if model:
            # theory
            x_data_range = [np.amin(x_data[mask]), np.amax(x_data[mask])]

            # can use higher resolution bins
            bin_edges = np.logspace(-1, 3, 101)

            # bins at a better resolution
            bin_centres = 10 ** ((np.log10(bin_edges[:-1]) + np.log10(bin_edges[1:])) / 2)
            bin_edges = np.roll(np.repeat(bin_edges, 2), -1)[:-2]

            # halo properties
            V200 = rcm.get_v_200(galaxy_name=galaxy_name)
            r200 = rcm.get_r_200(galaxy_name=galaxy_name)

            analytic_dispersion = rcm.get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                                     save_name_append=save_name_append, galaxy_name=galaxy_name)

            analytic_v_circ = rcm.get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                                 save_name_append=save_name_append, galaxy_name=galaxy_name)

            a, vmax = rcm.get_hernquist_params(galaxy_name=galaxy_name)
            m_tot = 4 * a * vmax ** 2 / GRAV_CONST
            rd = 4.15
            fbary = 0.01
            md_tot = m_tot * (1 - fbary)
            ms_tot = m_tot * fbary

            rho = lambda r: rcm.get_analytic_hernquist_density(np.array([r] * 2), md_tot, a)
            # acc = lambda r: get_analytic_hernquist_potential_derivative(r, md_tot, ad)
            acc = lambda r: (rcm.get_analytic_hernquist_potential_derivative(r, md_tot, a) +
                             rcm.get_analytic_spherical_exponential_potential_derivative(r, ms_tot, rd))
            integrad = lambda r: rho(r) * acc(r)
            sigma_rd = np.sqrt([quad(integrad, r_, 20 * a)[0] / rho(r_) for r_ in bin_centres])

            x_data = np.log10(bin_centres / r200)
            # x_data = bin_centres / r200
            # plot analytic dm
            if ii == 0:
                if 'dm_vc' in name:
                    a_h, vmax = rcm.get_hernquist_params(galaxy_name)
                    analytic_v_circ = rcm.get_analytic_hernquist_circular_velocity(bin_edges, vmax, a_h, save_name_append)
                    ax0.errorbar(x_data, (analytic_v_circ / V200), ls='-', c='k')

                if show_dms:
                    ax1.errorbar(x_data, (sigma_rd / V200), ls='-', c='k')
                    ax2.errorbar(x_data, (sigma_rd / V200), ls='-', c='k')
                    ax3.errorbar(x_data, (sigma_rd / V200), ls='-', c='k')

            # update data range
            mask_theory = np.logical_and(x_data_range[0] < x_data, x_data < x_data_range[1])

            # theory
            # work out constatns
            (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
             ) = rcm.get_constants(bin_edges, save_name_append, kwargs['mdm'], galaxy_name=galaxy_name)

            # burn ins
            (inital_velocity_v, inital_velocity2_z, inital_velocity2_r, inital_velocity2_p
             ) = rcm.smooth_zero_snap(kwargs['mdm'], snaps, save_name_append, bin_edges)

            inital_velocity_v = np.amin((inital_velocity_v, analytic_v_circ), axis=0)

            inital_velocity2_z **= 2
            inital_velocity2_r **= 2
            inital_velocity2_p **= 2

            # theory_best_v = np.zeros(len(bin_centres))
            theory_best_z2 = np.zeros(len(bin_centres))
            theory_best_r2 = np.zeros(len(bin_centres))
            theory_best_p2 = np.zeros(len(bin_centres))

            # experimenting
            experiment_v2 = np.zeros(len(bin_centres))

            # tau_heat_z2 = np.zeros(len(bin_centres))
            # tau_heat_r2 = np.zeros(len(bin_centres))
            tau_heat_p2 = np.zeros(len(bin_centres))

            tau_heat_experiment2 = np.zeros(len(bin_centres))

            for i in range(len(bin_centres)):
                # theory_best_v[i] = rcm.get_theory_velocity2_array(v200, analytic_v_circ[i] - inital_velocity_v[i],
                #                                               np.array([t]), t_c_dm[i], delta_dm[i],
                #                                               np.sqrt(upsilon_circ[i]),
                #                                               ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
                theory_best_z2[i] = rcm.get_theory_velocity2_array(v200 ** 2, inital_velocity2_z[i],
                                                               np.array([t]), t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                               ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
                theory_best_r2[i] = rcm.get_theory_velocity2_array(v200 ** 2, inital_velocity2_r[i],
                                                               np.array([t]), t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                               ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
                theory_best_p2[i] = rcm.get_theory_velocity2_array(v200 ** 2, inital_velocity2_p[i],
                                                               np.array([t]), t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                               ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

                # tau_heat_z2[i] = rcm.get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                #                                     ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
                # tau_heat_r2[i] = rcm.get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                #                                     ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
                tau_heat_p2[i] = rcm.get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                                    ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

                tau_heat_experiment2[i] = (t_c_dm[i] * (tau_heat_p2[i] * 0.74987599))

                experiment_v2[i] = analytic_v_circ[i] * np.exp(
                    -np.array([t]) / tau_heat_experiment2[i])

            alpha = 1
            ls = '--'
            thirty = int(len(bin_centres) - 1)
            lw = 3

            if model:
                ax0.errorbar(x_data[:thirty], (experiment_v2[:-1][:thirty] / V200),
                             ls=ls, c=c, lw=lw, alpha=alpha)

                ax1.errorbar(x_data[:thirty], (np.sqrt(theory_best_z2[:-1])[:thirty] / V200),
                             ls=ls, c=c, lw=lw, alpha=alpha)
                ax2.errorbar(x_data[:thirty], (np.sqrt(theory_best_r2[:-1])[:thirty] / V200),
                             ls=ls, c=c, lw=lw, alpha=alpha)
                ax3.errorbar(x_data[:thirty], (np.sqrt(theory_best_p2[:-1])[:thirty] / V200),
                             ls=ls, c=c, lw=lw, alpha=alpha)

                if assym:
                    # basic for paper
                    # asymmetri drift
                    d_sigma_r2_d_R = np.diff(theory_best_r2) / np.diff(bin_centres)

                    R_disk = 4.15

                    v_phi2_assymetry_on_v_c2sq = analytic_v_circ[:-1] * np.sqrt(
                        1 - theory_best_p2[:-1] / (analytic_v_circ[:-1] ** 2) +
                        theory_best_r2[:-1] / (analytic_v_circ[:-1] ** 2) * (1 - bin_centres[:-1] / R_disk) +
                        bin_centres[:-1] / (analytic_v_circ[:-1] ** 2) * d_sigma_r2_d_R)  # -
                    # (theory_best_r2[:-1,0] - theory_best_z2[:-1,0]) / analytic_v_circ[:-1]**2)

                    ax1.errorbar(x_data[mask_theory], (v_phi2_assymetry_on_v_c2sq / V200)[mask_theory],
                                 # / analytic_v_circ[:-1][:thirty],
                                 ls=(1, (0.8, 0.8)), c=c, lw=4, alpha=alpha)

        if lab:

            if eps:
                if ii < 5:
                    dx = 0.53
                    dy = 1.23 - 0.10*int(4-ii) - 0.05*(4-ii > 1)

                    if ii == 0:
                        dy = 1.23
                        ax1.text(xlim[0] + dx * (xlim[1] - xlim[0]), slim[0] + dy * (slim[1] - slim[0]),
                                 '\n'*5 + r'$\epsilon / z_d = $' + str(eps_list[ii]) + r'   $\,$', ha='left', va='top', color=c,
                                 bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8), linespacing=1.02)
                    else:
                        ax1.text(xlim[0] + dx * (xlim[1] - xlim[0]), slim[0] + dy * (slim[1] - slim[0]),
                                 r'$\epsilon / z_d = $' + str(eps_list[ii]), ha='left', va='top', color=c)

            else:
                # legends
                # if c in ['C0', 'C1', 'C2']: dx = 0.03
                # else: dx = 0.42
                dx = 0.97

                # if kwargs['c'] in ['C0', 'C3']: dy = 0.96
                # elif kwargs['c'] in ['C1', 'C4']: dy = 0.88
                # else dy = 0.80
                dy = 0.93 - 0.10*int(kwargs['c'][1]) + 0.3 - 0.05*(kwargs['c'] in ['C2', 'C3', 'C4'])

                if kwargs['c'] == 'C0':
                    ax1.text(xlim[0] + dx * (xlim[1] - xlim[0]), slim[0] + dy * (slim[1] - slim[0]),
                             r'$\mathrm{N}_{\mathrm{DM}} = $' + kwargs['labn'] + '\n'*5, ha='right', va='top', color=c,
                             bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8), linespacing=1.02)
                else:
                    ax1.text(xlim[0] + dx * (xlim[1] - xlim[0]), slim[0] + dy * (slim[1] - slim[0]),
                             r'$\mathrm{N}_{\mathrm{DM}} = $' + kwargs['labn'], ha='right', va='top', color=c)

    r_half = rcm.get_bin_edges('cum')[3]
    ax0.axvline(np.log10(r_half / r200), 0, 1, c='grey', ls=':')
    ax1.axvline(np.log10(r_half / r200), 0, 1, c='grey', ls=':')
    ax2.axvline(np.log10(r_half / r200), 0, 1, c='grey', ls=':')
    ax3.axvline(np.log10(r_half / r200), 0, 1, c='grey', ls=':')

    if lab:
        if eps:
            ax2.legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='-', linewidth=3)),
                        (lines.Line2D([0, 1], [0, 1], color='grey', ls='--', linewidth=3)),],
                       [r'$\mathrm{N}_{\mathrm{DM}} = 1.8 \times 10^5}$', r'$\mathrm{N}_{\mathrm{DM}}  = 1.8 \times 10^4}$'],
                       loc='upper right', handlelength=1.5, frameon=True)
        elif model:
            ax2.legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='-', linewidth=3)),
                        (lines.Line2D([0, 1], [0, 1], color='grey', ls='--', linewidth=3)),],
                       ['Data', 'Model'],
                       loc='upper right', handlelength=1, frameon=True)
    #handletextpad=0.2, frameon=True #, framealpha=0.95,
               #labelspacing=0.4, borderpad=0.2) #, bbox_to_anchor=(1, 0.65))

    ax0.text(xlim[0] + 0.03 * (xlim[1] - xlim[0]), vlim[0] + 0.95 * (vlim[1] - vlim[0]),
             r'$t =$' + str(t) + r' Gyr',  # , $V_{200} = 200$ km/s',
             ha='left', va='top')

    if lab:
        ax3.text(xlim[0] + 0.45 * (xlim[1] - xlim[0]), vlim[0] + 0.70 * (slim[1] - slim[0]),
                 r'$R_{1/2, 0}$', ha='left', va='top')
        # ax0.text(xlim[0] + 0.45 * (xlim[1] - xlim[0]), vlim[0] + 0.05 * (vlim[1] - vlim[0]),
        #          r'$R_{1/2}$', ha='left', va='bottom')

        ax1.text(xlim[0]+0.03*(xlim[1]-xlim[0]), slim[0]+0.02*(slim[1]-slim[0]),
                 r'ICs', ha='left', va='bottom')

        ax0.text(xlim[0] + 0.95 * (xlim[1] - xlim[0]), vlim[0] + 0.86 * (vlim[1] - vlim[0]),
                 r'$V_c$', ha='right', va='top')

        ax1.text(xlim[0] + 0.95 * (xlim[1] - xlim[0]), slim[0] + 0.45 * (slim[1] - slim[0]),
                 r'$\sigma_{\mathrm{DM}}$', ha='right', va='top')

    if not lab:
        ax0.set_yticklabels([])
        ax1.set_yticklabels([])
        ax2.set_yticklabels([])
        ax3.set_yticklabels([])

        ax0.set_ylabel('')
        ax1.set_ylabel('')
        ax2.set_ylabel('')
        ax3.set_ylabel('')

    if save:
        plt.savefig(name, bbox_inches='tight', pad_inches=0)
        plt.close()

    return


def plot_model_evolution_extras(sig=True, model=True, points=False, name='', save=False):
    # if model:
    #     raise NotImplementedError

    galaxy_name_list0 = [
        'mu_5/fdisk_0p01_lgMdm_9p0_V200-400kmps',
        'mu_5/fdisk_0p01_lgMdm_8p5_V200-400kmps',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-400kmps',
        'mu_5/fdisk_0p01_lgMdm_7p5_V200-400kmps',
        'mu_5/fdisk_0p01_lgMdm_7p0_V200-400kmps',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
        'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
        'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
        'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
        'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps',
        'mu_5/fdisk_0p01_lgMdm_7p0_V200-100kmps',
        'mu_5/fdisk_0p01_lgMdm_6p5_V200-100kmps',
        'mu_5/fdisk_0p01_lgMdm_6p0_V200-100kmps',
        'mu_5/fdisk_0p01_lgMdm_5p5_V200-100kmps',
        'mu_5/fdisk_0p01_lgMdm_5p0_V200-100kmps',
        'mu_5/fdisk_0p01_lgMdm_6p0_V200-50kmps',
        'mu_5/fdisk_0p01_lgMdm_5p5_V200-50kmps',
        'mu_5/fdisk_0p01_lgMdm_5p0_V200-50kmps',
        'mu_5/fdisk_0p01_lgMdm_4p5_V200-50kmps',
        'mu_5/fdisk_0p01_lgMdm_4p0_V200-50kmps',
    ]
    snaps_list0 = [400, 400, 101, 101, 101, 400, 400, 101, 101, 101, 400, 400, 101, 101, 101, 400, 400, 101, 101, 101]

    galaxy_name_list1 = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_2z0_fixed_eps',
                        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_4z0_fixed_eps',
                        'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps_2z0_fixed_eps',
                        'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps_4z0_fixed_eps',
                        ]
    snaps_list1 = [400, 400, 400, 101, 400, 400]

    galaxy_name_list2 = [
                        'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_1/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                        'mu_1/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_1/fdisk_0p01_lgMdm_6p5_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps',
                        'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_25/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                        'mu_25/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_25/fdisk_0p01_lgMdm_6p5_V200-200kmps',
                        'mu_25/fdisk_0p01_lgMdm_6p0_V200-200kmps',]
    snaps_list2 = [400, 400, 101, 101,
                  400, 400, 101, 101, 101,
                  400, 400, 101, 101, 101,]

    galaxy_name_list_list = [galaxy_name_list0, galaxy_name_list1, galaxy_name_list2]
    snaps_list_list = [snaps_list0, snaps_list1, snaps_list2]

    v200_map = 'turbo'
    c50  = matplotlib.cm.get_cmap(v200_map)(0.2)
    c100 = matplotlib.cm.get_cmap(v200_map)(0.4)
    c200 = matplotlib.cm.get_cmap(v200_map)(0.6)
    c400 = matplotlib.cm.get_cmap(v200_map)(0.8)

    z0_map = 'magma'
    z1 = matplotlib.cm.get_cmap(z0_map)(0.2)
    z2 = matplotlib.cm.get_cmap(z0_map)(0.5)
    z4 = matplotlib.cm.get_cmap(z0_map)(0.8)

    mu_map = 'viridis'
    mu1 = matplotlib.cm.get_cmap(mu_map)(0.1)
    mu5 = matplotlib.cm.get_cmap(mu_map)(0.5)
    mu25 = matplotlib.cm.get_cmap(mu_map)(0.9)

    def pick_color(j, galaxy_name):
        if j == 0:
            if '-50' in galaxy_name:
                return c50
            elif '-100' in galaxy_name:
                return c100
            elif '-200' in galaxy_name:
                return c200
            elif '-400' in galaxy_name:
                return c400

        if j == 1:
            if '4z0' in galaxy_name:
                return z4
            elif '2z0' in galaxy_name:
                return z2
            else:
                return z1

        if j == 2:
            if 'mu_25' in galaxy_name:
                return mu25
            elif 'mu_5' in galaxy_name:
                return mu5
            elif 'mu_1' in galaxy_name:
                return mu1

        print('Warning: Something went wrong trying to pick a color.')
        return 'k'

    def pick_label(j, i):
        if j == 0:
            if i == 0:
                return r'$V_{200} = 400$ km/s', pick_color(j, '-400')
            elif i == 1:
                return r'$V_{200} = 200$ km/s', pick_color(j, '-200')
            elif i == 2:
                return r'$V_{200} = 100$ km/s', pick_color(j, '-100')
            elif i == 3:
                return r'$V_{200} = 50$ km/s', pick_color(j, '-50')

        if j == 1:
            if i == 0:
                return r'$z_d = 0.2 \times R_d$', pick_color(j, '4z0')
            elif i == 1:
                return r'$z_d = 0.1 \times R_d$', pick_color(j, '2z0')
            elif i == 2:
                return r'$z_d = 0.05 \times R_d$', pick_color(j, '')

        if j == 2:
            if i == 0:
                return r'$\mu = 25$', pick_color(j, 'mu_25')
            elif i == 1:
                return r'$\mu = 5$', pick_color(j, 'mu_5')
            elif i == 2:
                return r'$\mu = 1$', pick_color(j, 'mu_1')

        print('Warning: Something went wrong trying to pick a color.')
        return '', 'k'

    _index = 1

    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(12, 14, forward=True)
    widths = [1, 1, 1]
    heights = [1, 1, 1, 1]
    spec = fig.add_gridspec(ncols=3, nrows=4, width_ratios=widths, height_ratios=heights)

    pth_w = [path_effects.Stroke(linewidth=4, foreground='white'), path_effects.Normal()]

    # xlims = [-3.8, -0.4]
    # xlims = [0, 0.05]
    # ylims = [-2.7, 0.3]

    eps = 1e-3
    if sig:
        xlims = [np.log10(6e-3), np.log10(6)]
        ylims = [-2.2, 0.4]
    else:
        xlims = [np.log10(3e-4), np.log10(0.06)]
        ylims = [-2.2, 0.4]

    axs = []
    for row in range(len(heights)):
        axs.append([])
        for col in range(len(widths)):
            axs[row].append(fig.add_subplot(spec[row, col]))
            axs[row][col].set_xlim(xlims)
            axs[row][col].set_ylim(ylims)
            # axs[row][col].set_aspect('equal')
            if col != 0: axs[row][col].set_yticklabels([])
            if row != len(heights) - 1:
                axs[row][col].set_xticklabels([])
            # axs[row][col].set_yticks([-2, -1, 0])

            axs[row][col].set_aspect('equal')

    # for row in range(len(heights)):
    #     for col in range(len(widths)):
    #         axs[row][col] = axs[row][0]

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    run = []
    run.append(rcm.find_least_log_squares_best_fit(save_name_append, mcmc=False, fixed_b=0, fixed_g=0))
    # run.append(rcm.find_least_log_squares_best_fit(save_name_append, mcmc=False, fixed_b=0, fixed_g=0))

    run_ls = ['--']#, '-.']
    run_c = ['k']#, 'k']
    run_funcs = [rcm.get_theory_velocity2_array]#, rcm.get_powerlaw_velocity2_array]
    if sig:
        run_labels0 = [r'$\sigma_\phi^2 \! \propto \! 1$-$\,\exp \, \frac{-t}{t_{\sigma_i}}$', r'$\sigma_\phi^2 \! \propto \! t$']
        run_labels1 = [r'$\overline{v}_\phi \propto \exp \, \frac{-t}{t_{v_\phi}}$', r'$\overline{v}_\phi \propto t$']
    else:
        run_labels0 = [r'$\sigma_\phi^2 \propto 1 - \exp \, \frac{-t}{t_c}$', r'$\sigma_\phi^2 \propto t$']
        run_labels1 = [r'$\overline{v}_\phi \propto \exp \, \frac{-t}{t_c}$', r'$\overline{v}_\phi \propto t$']
    run_zorder = [-5, -4]

    theory_best_v_list = []
    theory_best_z2_list = []
    theory_best_r2_list = []
    theory_best_p2_list = []

    # (inital_velocity2_array_v, inital_velocity2_array_z, inital_velocity2_array_r, inital_velocity2_array_p
    #  ) = smooth_burn_ins(1e-2, 400, save_name_append, burn_in, bin_edges)

    galaxy_name = 'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps'
    kwargs = rcm.get_name_kwargs(galaxy_name)
    snaps = 400

    bin_edges = rcm.get_bin_edges(save_name_append, galaxy_name=galaxy_name)
    analytic_dispersion = rcm.get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)
    analytic_v_circ = rcm.get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                         save_name_append=save_name_append, galaxy_name=galaxy_name)
    # (a_h, vmax) = get_hernquist_params(galaxy_name)
    # analytic_v_circ = get_analytic_hernquist_circular_velocity(bin_edges, vmax, a_h, save_name_append)

    v200 = rcm.get_v_200(galaxy_name=galaxy_name)

    (t_c_dm, _, delta_dm, upsilon_dm, upsilon_circ
     ) = rcm.get_constants(bin_edges, save_name_append, 1e-2, galaxy_name=galaxy_name)

    theory_times = np.logspace(-5, 3, 1000)
    log_theory_times = np.log10(theory_times)
    fifty = len(theory_times)
    o = np.ones(fifty)
    tau_t = rcm.get_tau_array(theory_times, t_c_dm[:, np.newaxis])
    # tau_t = [get_tau_array(theory_times, t_c_dm[i]) for i in range(len(bin_edges)//2)]

    (inital_velocity_v, inital_velocity_z, inital_velocity_r, inital_velocity_p
     ) = rcm.smooth_burn_ins(kwargs['mdm'], snaps, save_name_append, BURN_IN, bin_edges, galaxy_name=galaxy_name)

    experiment_v = []

    for j, (ln_Lambda_ks, alphas, betas, gammas) in enumerate(run):

        # yaxis
        # theory_best_v_list.append([run_funcs[j](v200 * o, 0 * o,
        #                                         theory_times, t_c_dm[i] * o, delta_dm[i] * o,
        #                                         np.sqrt(upsilon_circ[i]) * o,
        #                                         ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
        #                            for i in range(len(bin_edges) // 2)])

        theory_best_z2_list.append([run_funcs[j](v200 ** 2 * o, 0 * o,
                                                 theory_times, t_c_dm[i]*o, delta_dm[i] *o, upsilon_dm[i] *o,
                                                 ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
                                    for i in range(len(bin_edges)//2)])

        theory_best_r2_list.append([run_funcs[j](v200 ** 2 * o, 0 * o,
                                                 theory_times, t_c_dm[i] * o, delta_dm[i] * o, upsilon_dm[i] * o,
                                                 ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
                                    for i in range(len(bin_edges) // 2)])

        theory_best_p2_list.append([run_funcs[j](v200 ** 2 * o, 0 * o,
                                                 theory_times, t_c_dm[i] * o, delta_dm[i] * o, upsilon_dm[i] * o,
                                                 ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])
                                    for i in range(len(bin_edges) // 2)])

        tau_heat_p2 = np.array([rcm.get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                                   ln_Lambda_ks[3], alphas[3], betas[3], gammas[3]) for i in
                                range(len(bin_edges) // 2)])

        tau_heat_experiment = tau_heat_p2 * 0.74987599

        tau_0_on_tau_heat = np.array([rcm.get_tau_zero_array(v200 * o, 0 * o,
                                                         np.sqrt(upsilon_circ[i])) for i in
                                      range(len(bin_edges) // 2)])

        if run_funcs[j] == rcm.get_theory_velocity2_array:
            experiment_v.append(np.array([v200 * np.sqrt(upsilon_circ[i]) ** 2 * (1 - np.exp(
                - np.power(((theory_times / t_c_dm[i]) / tau_heat_experiment[i]) + tau_0_on_tau_heat[i],
                           1 + gammas[0])))
                                          for i in range(len(bin_edges) // 2)]))

        else:  # run_funcs == get_powerlaw_velocity2_array
            experiment_v.append(np.array([v200 * np.sqrt(upsilon_circ[i]) ** 2 * (
                np.power(((theory_times / t_c_dm[i]) / tau_heat_experiment[i]),
                         1 + gammas[0])) for i in range(len(bin_edges) // 2)]))

        # experiment_v = analytic_v_circ[:, np.newaxis] - experiment_v

        # plot
        ls = run_ls[j]

        for i in range(len(bin_edges) // 2):
            if sig:
                # tau_heat_v = np.array([rcm.get_tau_heat_array(delta_dm[i], np.sqrt(upsilon_circ[i]),
                #                                               ln_Lambda_ks[0], alphas[0], betas[0], gammas[0]) for i in
                #                        range(len(bin_edges) // 2)])
                tau_heat_v = 0.75 * tau_heat_p2
                tau_heat_z2 = np.array([rcm.get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                                               ln_Lambda_ks[1], alphas[1], betas[1], gammas[1]) for i in
                                        range(len(bin_edges) // 2)])
                tau_heat_r2 = np.array([rcm.get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                                               ln_Lambda_ks[2], alphas[2], betas[2], gammas[2]) for i in
                                        range(len(bin_edges) // 2)])

                # axs[1][i].errorbar(tau_t[i], np.log10(theory_best_v_list[j][i]) - np.log10(v200),
                #                    ls=ls, lw=3, c=run_c[j], label=run_labels1[j], path_effects=pth_w, zorder=run_zorder[j])
                axs[0][i].errorbar(np.log10(tau_t[_index]) - np.log10(tau_heat_v[_index]),
                                   np.log10(experiment_v[j][_index]) - np.log10(analytic_v_circ[_index]),
                                   ls=ls, lw=3, c=run_c[j], label=run_labels1[j], path_effects=pth_w,
                                   zorder=run_zorder[j])
                axs[1][i].errorbar(np.log10(tau_t[_index]) - np.log10(tau_heat_z2[_index]),
                                   np.log10(theory_best_z2_list[j][_index]) - 2 * np.log10(analytic_dispersion[_index]),
                                   ls=ls, lw=3, c=run_c[j], label=run_labels0[j], path_effects=pth_w,
                                   zorder=run_zorder[j])
                axs[2][i].errorbar(np.log10(tau_t[_index]) - np.log10(tau_heat_r2[_index]),
                                   np.log10(theory_best_r2_list[j][_index]) - 2 * np.log10(analytic_dispersion[_index]),
                                   ls=ls, lw=3, c=run_c[j], label=run_labels0[j], path_effects=pth_w,
                                   zorder=run_zorder[j])
                axs[3][i].errorbar(np.log10(tau_t[_index]) - np.log10(tau_heat_p2[_index]),
                                   np.log10(theory_best_p2_list[j][_index]) - 2 * np.log10(analytic_dispersion[_index]),
                                   ls=ls, lw=3, c=run_c[j], label=run_labels0[j], path_effects=pth_w,
                                   zorder=run_zorder[j])

            else:
                axs[0][i].errorbar(np.log10(tau_t[_index]), np.log10(experiment_v[j][_index]) - np.log10(v200),
                                   ls=ls, lw=3, c=run_c[j], label=run_labels1[j], path_effects=pth_w,
                                   zorder=run_zorder[j])
                axs[1][i].errorbar(np.log10(tau_t[_index]), np.log10(theory_best_z2_list[j][_index]) - 2 * np.log10(v200),
                                   ls=ls, lw=3, c=run_c[j], label=run_labels0[j], path_effects=pth_w,
                                   zorder=run_zorder[j])
                axs[2][i].errorbar(np.log10(tau_t[_index]), np.log10(theory_best_r2_list[j][_index]) - 2 * np.log10(v200),
                                   ls=ls, lw=3, c=run_c[j], label=run_labels0[j], path_effects=pth_w,
                                   zorder=run_zorder[j])
                axs[3][i].errorbar(np.log10(tau_t[_index]), np.log10(theory_best_p2_list[j][_index]) - 2 * np.log10(v200),
                                   ls=ls, lw=3, c=run_c[j], label=run_labels0[j], path_effects=pth_w,
                                   zorder=run_zorder[j])

    # maximum cut off
    ls = '--'
    c = 'grey'
    alpha = 0.5
    linewidth = 5
    for i in range(len(bin_edges) // 2):
        for j in range(4):
            if i == 0 and j == 0:
                if sig:
                    axs[j][i].errorbar([xlims[0] + 0.65 * (xlims[1] - xlims[0]), xlims[1]],
                                       [0] * 2,
                                       ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
                else:
                    axs[j][i].errorbar([xlims[0] + 0.65 * (xlims[1] - xlims[0]), xlims[1]],
                                       [np.log10(analytic_v_circ[i]) - np.log10(v200)] * 2,
                                       ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
            elif i == 1 and j == 1:
                if sig:
                    axs[j][i].errorbar([xlims[0] + 0.8 * (xlims[1] - xlims[0]), xlims[1]],
                                       [0] * 2,
                                       ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
                else:
                    axs[j][i].errorbar([xlims[0] + 0.8 * (xlims[1] - xlims[0]), xlims[1]],
                                       [2 * np.log10(analytic_dispersion[i]) - 2 * np.log10(v200)] * 2,
                                       ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)

            else:
                if sig:
                    axs[j][i].errorbar(np.log10(theory_times), 0 * o,
                                       ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
                else:
                    axs[j][i].errorbar(np.log10(theory_times), 0 * o + np.log10(analytic_v_circ[i]) - np.log10(v200),
                                       ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)

    # col_list = ['C0', 'C1', 'C2', 'C3', 'C4']

    # put data on plots
    for i, (galaxy_name_list, snaps_list) in enumerate(zip(galaxy_name_list_list, snaps_list_list)):
        for l, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
            kwargs = rcm.get_name_kwargs(galaxy_name)

            v200 = rcm.get_v_200(galaxy_name)

            bin_edges = rcm.get_bin_edges(galaxy_name=galaxy_name)

            (mean_v_R, mean_v_phi,
             sigma_z, sigma_R, sigma_phi
             ) = rcm.get_dispersions(galaxy_name, save_name_append, snaps, bin_edges)

            # work out constatns
            (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
             ) = rcm.get_constants(bin_edges, save_name_append, kwargs['mdm'], galaxy_name=galaxy_name)

            analytic_dispersion = rcm.get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                                     save_name_append=save_name_append, galaxy_name=galaxy_name)
            analytic_v_circ = rcm.get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                                 save_name_append=save_name_append, galaxy_name=galaxy_name)

            mean_v_phi = mean_v_phi.T
            sigma_z2 = sigma_z.T ** 2
            sigma_r2 = sigma_R.T ** 2
            sigma_p2 = sigma_phi.T ** 2

            (inital_velocity_v, inital_velocity_z, inital_velocity_r, inital_velocity_p
             ) = rcm.smooth_burn_ins(kwargs['mdm'], snaps, save_name_append, BURN_IN, bin_edges, galaxy_name=galaxy_name)
            inital_velocity2_z = inital_velocity_z ** 2
            inital_velocity2_r = inital_velocity_r ** 2
            inital_velocity2_p = inital_velocity_p ** 2

            window = snaps // window_n + 1
            if window % 2 == 0: window += 1

            time = np.linspace(0, T_TOT, snaps + 1)
            time = time - time[BURN_IN]

            tau = rcm.get_tau_array(time, t_c_dm[_index])

            # # TODO this seems like the wrong / old alpha, beta, ln lambda fit
            # tau_vir = rcm.get_tau_heat_array(delta_dm[_index], np.sqrt(upsilon_circ[_index]), run[1][0][0], run[1][1][0],
            #                                  run[1][2][0], run[1][3][0])

            tau_vir = 0.75 * rcm.get_tau_heat_array(delta_dm[_index], upsilon_dm[_index], run[0][0][3], run[0][1][3],
                                                    run[0][2][3], run[0][3][3])
            tau_0 = tau_vir * np.log(analytic_v_circ[_index] / inital_velocity_v[_index])

            if sig:
                x = np.log10((tau + tau_0)/tau_vir)
                y = np.log10((analytic_v_circ[_index] - mean_v_phi[_index]) / analytic_v_circ[_index])
            else:
                x = np.log10(tau + tau_0)
                y = np.log10((analytic_v_circ[_index] - mean_v_phi[_index]) / v200)

            if points:
                axs[0][i].errorbar(x, y,
                                   c=pick_color(i, galaxy_name), markeredgewidth=0, marker='.', ls='',
                                   lw=lwt, alpha=alpha, zorder=-10)
            #needed to make savgol work
            if np.sum(np.isnan(y)) > 0:
                # y = np.log10(np.abs((analytic_v_circ[_index] - mean_v_phi[_index])) / v200)
                y = np.log10(np.abs((analytic_v_circ[_index] - mean_v_phi[_index]) / analytic_v_circ[_index]))
            axs[0][i].errorbar(x, savgol_filter(y,  window, poly_n, mode='interp'),
                               c=pick_color(i, galaxy_name), marker='', ls='-', lw=lw)

            tau_vir = rcm.get_tau_heat_array(delta_dm[_index], upsilon_dm[_index], run[0][0][1], run[0][1][1],
                                             run[0][2][1], run[0][3][1])
            tau_0 = tau_vir * np.log(analytic_dispersion[_index] ** 2 / (analytic_dispersion[_index] ** 2 - inital_velocity2_z[_index]))

            if sig:
                x = np.log10((tau + tau_0)/tau_vir)
                y = np.log10(sigma_z2[_index] / analytic_dispersion[_index] ** 2)
            else:
                x = np.log10(tau + tau_0)
                y = np.log10(sigma_z2[_index] / v200 ** 2)

            if points:
                axs[1][i].errorbar(x, y,
                                   c=pick_color(i, galaxy_name), markeredgewidth=0, marker='.', ls='',
                                   lw=lwt, alpha=alpha, zorder=-10)
            axs[1][i].errorbar(x, savgol_filter(y,  window, poly_n, mode='interp'),
                               c=pick_color(i, galaxy_name), marker='', ls='-', lw=lw)

            tau_vir = rcm.get_tau_heat_array(delta_dm[_index], upsilon_dm[_index], run[0][0][2], run[0][1][2],
                                             run[0][2][2], run[0][3][2])
            tau_0 = tau_vir * np.log(analytic_dispersion[_index] ** 2 / (analytic_dispersion[_index] ** 2 - inital_velocity2_r[_index]))

            if sig:
                x = np.log10((tau + tau_0)/tau_vir)
                y = np.log10(sigma_r2[_index] / analytic_dispersion[_index] ** 2)
            else:
                x = np.log10(tau + tau_0)
                y = np.log10(sigma_r2[_index] / v200 ** 2)

            if points:
                axs[2][i].errorbar(x, y,
                                   c=pick_color(i, galaxy_name), markeredgewidth=0, marker='.', ls='',
                                   lw=lwt, alpha=alpha, zorder=-10)
            axs[2][i].errorbar(x, savgol_filter(y,  window, poly_n, mode='interp'),
                               c=pick_color(i, galaxy_name), marker='', ls='-', lw=lw)

            tau_vir = rcm.get_tau_heat_array(delta_dm[_index], upsilon_dm[_index], run[0][0][3], run[0][1][3],
                                             run[0][2][3], run[0][3][3])
            tau_0 = tau_vir * np.log(analytic_dispersion[_index] ** 2 / (analytic_dispersion[_index] ** 2 - inital_velocity2_p[_index]))

            if sig:
                x = np.log10((tau + tau_0)/tau_vir)
                y = np.log10(sigma_p2[_index] / analytic_dispersion[_index] ** 2)
            else:
                x = np.log10(tau + tau_0)
                y = np.log10(sigma_p2[_index] / v200 ** 2)

            if points:
                axs[3][i].errorbar(x, y,
                                   c=pick_color(i, galaxy_name), markeredgewidth=0, marker='.', ls='',
                                   lw=lwt, alpha=alpha, zorder=-10)
            axs[3][i].errorbar(x, savgol_filter(y,  window, poly_n, mode='interp'),
                               c=pick_color(i, galaxy_name), marker='', ls='-', lw=lw)

            ####
            #test
            ####
            #print scatter
            max_seed = 1
            if os.path.exists('../galaxies/' + galaxy_name + '_seed3'):
                max_seed = 4
                if os.path.exists('../galaxies/' + galaxy_name + '_seed4'): max_seed = 5
                if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

            if max_seed != 1:

                # V200 = (M200 * 10 * GRAV_CONST * GALIC_HUBBLE_CONST) ** (1 / 3)
                M200 = v200**3 / (10 * GRAV_CONST * GALIC_HUBBLE_CONST)
                _ind = 1

                v_R = np.zeros((max_seed, snaps+1))
                v_phi = np.zeros((max_seed, snaps+1))
                s_z = np.zeros((max_seed, snaps+1))
                s_R = np.zeros((max_seed, snaps+1))
                s_phi = np.zeros((max_seed, snaps+1))


                for _i in range(max_seed):
                    gni = galaxy_name + f'_seed{_i}'

                    (mean_v_R, mean_v_phi,
                     sigma_z, sigma_R, sigma_phi
                     ) = rcm.get_dispersions(gni, save_name_append, snaps, bin_edges)

                    v_R[_i] = mean_v_R[:, _ind]
                    v_phi[_i] = mean_v_phi[:, _ind]
                    s_z[_i] = sigma_z[:, _ind]
                    s_R[_i] = sigma_R[:, _ind]
                    s_phi[_i] = sigma_phi[:, _ind]

                print(round(np.log10(M200) + 10, 3), gni)

                s_tot = np.sqrt(s_z**2 + s_R**2 + s_phi**2)

                for _j in range(3):
                    data = s_tot[:, -_j-1]
                    # print('mean', round(np.mean(data),3), 'median     ', round(np.median(data),3))
                    print('std ', round(np.std(data),3), 'percentiles',
                          round((np.quantile(data, 0.84) - np.quantile(data, 0.16))/2,3))

            ####
            #test
            ####

        for j in range(3 + (i==0)):
            # dx = 0.05
            # dy = 0.70 - 0.10 * j
            # label, color = pick_label(i, j)
            # axs[1][i].text(xlims[0] + dx * (xlims[1] - xlims[0]), ylims[0] + dy * (ylims[1] - ylims[0]),
            #                label, color=color, ha='left', va='bottom',)
            #                #bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

            dx = [0.34, 0.42, 0.72][i] #0.97
            dy = 0.23 - 0.10 * j + 1*(i==0) - 0.05*(j==3)
            label, color = pick_label(i, j)
            axs[0 + 1*(i==0)][i].text(xlims[0] + dx * (xlims[1] - xlims[0]), ylims[0] + dy * (ylims[1] - ylims[0]),
                                    label, color=color, ha='left', va='bottom',)
                                    #bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    l0 = axs[0][0].legend(handlelength=1, frameon=False, loc='upper left',
                          labelspacing=0.2, handletextpad=0.4, borderaxespad=0.0)
    l1 = axs[1][1].legend(handlelength=1, frameon=False, loc='upper left',
                          labelspacing=0.2, handletextpad=0.4, borderaxespad=0.0)
    l0.set_zorder(20)
    l1.set_zorder(20)

    # x0 = xlims[0] + 0.001
    # dx = 0.1 * (xlims[1] - xlims[0])
    # y0 = ylims[0] + 0.05 * (ylims[1] - ylims[0])
    # axs[1][1].text(x0 + 1 * dx, y0, r'$\mathrm{N}_{\mathrm{DM}}$', va='bottom', ha='center', c='C4')
    # axs[1][1].text(x0 + 3 * dx, y0, r'$\mathrm{N}_{\mathrm{DM}}$', va='bottom', ha='center', c='C3')
    # axs[1][1].text(x0 + 5 * dx, y0, r'$\mathrm{N}_{\mathrm{DM}}$', va='bottom', ha='center', c='C2')
    # axs[1][1].text(x0 + 7 * dx, y0, r'$\mathrm{N}_{\mathrm{DM}}$', va='bottom', ha='center', c='C1')
    # axs[1][1].text(x0 + 9 * dx, y0, r'$\mathrm{N}_{\mathrm{DM}}$', va='bottom', ha='center', c='C0')
    # axs[1][1].text(x0 + 2 * dx, y0, r'$>$', va='bottom', ha='center')
    # axs[1][1].text(x0 + 4 * dx, y0, r'$>$', va='bottom', ha='center')
    # axs[1][1].text(x0 + 6 * dx, y0, r'$>$', va='bottom', ha='center')
    # axs[1][1].text(x0 + 8 * dx, y0, r'$>$', va='bottom', ha='center')

    p1 = 0.05
    xx = xlims[0] + (1 - p1) * (xlims[1] - xlims[0])
    yy = ylims[0] + p1 * (ylims[1] - ylims[0])
    yyy = ylims[0] + 0.35 * (ylims[1] - ylims[0])
    axs[0][0].text(xx, yyy, r'$R = R_{1/2, 0}$', va='bottom', ha='right')
    axs[0][1].text(xx, yyy, r'$R = R_{1/2, 0}$', va='bottom', ha='right')
    axs[0][2].text(xx, yyy, r'$R = R_{1/2, 0}$', va='bottom', ha='right')
    # axs[1][0].text(xx, yy, r'$R = R_{1/4}$', va='top')
    # axs[1][1].text(xx, yy, r'$R = R_{1/2}$', va='top')
    # axs[1][2].text(xx, yy, r'$R = R_{3/4}$', va='top')

    xx = xlims[0] + p1 * (xlims[1] - xlims[0])
    if sig:
        axs[0][1].text(xx, 0.05, r'$V_c$', va='bottom', ha='left')
        axs[1][0].text(xx, 0.05, r'$\sigma_{\mathrm{DM}}$', va='bottom', ha='left')
    else:
        axs[0][1].text(xx, -0.06, r'$V_c$', va='top', ha='left')
        axs[1][0].text(xx, -0.25, r'$\sigma_{\mathrm{DM}}$', va='top', ha='left')

    if sig:
        axs[0][0].set_ylabel(r'$\log ([V_c - \overline{v}_\phi] / V_c) $')
        axs[1][0].set_ylabel(r'$\log (\sigma_z^2 / \sigma_{\mathrm{DM}}^2) $')
        axs[2][0].set_ylabel(r'$\log (\sigma_R^2 / \sigma_{\mathrm{DM}}^2) $')
        axs[3][0].set_ylabel(r'$\log (\sigma_\phi^2 / \sigma_{\mathrm{DM}}^2) $')
    else:
        axs[0][0].set_ylabel(r'$\log ([V_c - \overline{v}_\phi] / V_{200}) $')
        axs[1][0].set_ylabel(r'$\log (\sigma_z^2 / V_{200}^2) $')
        axs[2][0].set_ylabel(r'$\log (\sigma_R^2 / V_{200}^2) $')
        axs[3][0].set_ylabel(r'$\log (\sigma_\phi^2 / V_{200}^2) $')

    if sig:
        axs[3][0].set_xlabel(r'$\log \, t / t_{\sigma_i} $')
        axs[3][1].set_xlabel(r'$\log \, t / t_{\sigma_i} $')
        axs[3][2].set_xlabel(r'$\log \, t / t_{\sigma_i} $')
    else:
        axs[3][0].set_xlabel(r'$\log \, t / t_c $')
        axs[3][1].set_xlabel(r'$\log \, t / t_c $')
        axs[3][2].set_xlabel(r'$\log \, t / t_c $')
    # axs[3][0].set_xlabel(r'$\log (\Delta \tau) $')
    # axs[3][1].set_xlabel(r'$\log (\Delta \tau) $')
    # axs[3][2].set_xlabel(r'$\log (\Delta \tau) $')

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    # np.seterr(all='warn')

    return


def plot_model_evolution(sig=False, concs=False, model=True, points=False, name='', save=False):
    # if model:
    #     raise NotImplementedError

    galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps']
    snaps_list = [400,400,101,101,101]

    if concs:
        galaxy_name_list = ['mu_5_c_7/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                            'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                            'mu_5_c_15/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                            'mu_5_c_7/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                            'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                            'mu_5_c_15/fdisk_0p01_lgMdm_7p0_V200-200kmps',]
        snaps_list = [400,400,400,101,101,101]

    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(12, 14, forward=True)
    widths = [1, 1, 1]
    heights = [1, 1, 1, 1]
    spec = fig.add_gridspec(ncols=3, nrows=4, width_ratios=widths, height_ratios=heights)

    pth_w = [path_effects.Stroke(linewidth=4, foreground='white'), path_effects.Normal()]

    # xlims = [-3.8, -0.4]
    # xlims = [0, 0.05]
    # ylims = [-2.7, 0.3]

    eps = 1e-3
    if sig:
        xlims = [np.log10(6e-3), np.log10(6)]
        ylims = [-2.2, 0.4] #[-2.2, 0.2]
    else:
        xlims = [np.log10(3e-4), np.log10(0.06)]
        ylims = [-2.2, 0.4] #[-2.2, 0.2]

    axs = []
    for row in range(len(heights)):
        axs.append([])
        for col in range(len(widths)):
            axs[row].append(fig.add_subplot(spec[row, col]))
            axs[row][col].set_xlim(xlims)
            axs[row][col].set_ylim(ylims)
            # axs[row][col].set_aspect('equal')
            if col != 0: axs[row][col].set_yticklabels([])
            if row != len(heights) - 1:
                axs[row][col].set_xticklabels([])
            # axs[row][col].set_yticks([-2, -1, 0])

            # axs[row][col].semilogx()

            axs[row][col].set_aspect('equal')

    # for row in range(len(heights)):
    #     for col in range(len(widths)):
    #         axs[row][col] = axs[row][0]

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    run = []
    run.append(rcm.find_least_log_squares_best_fit(save_name_append, mcmc=False, fixed_b=0, fixed_g=0))
    run.append(rcm.find_least_log_squares_best_fit(save_name_append, mcmc=False, fixed_b=0, fixed_g=0))

    run_ls = ['--', '-.']
    run_c = ['k', 'k']
    run_funcs = [rcm.get_theory_velocity2_array, rcm.get_powerlaw_velocity2_array]
    # run_labels0 = [r'$\sigma_\phi^2 \propto t$', r'equation (6)']
    # run_labels1 = [r'$\overline{v}_\phi \propto t$', r'equation (11)']
    # run_labels0 = [r'$\sigma_\phi^2 \propto t$', r'exponential model']
    # run_labels1 = [r'$\overline{v}_\phi \propto t$', r'exponential model']
    if sig:
        run_labels0 = [r'$\sigma_\phi^2 \! \propto \! 1$-$\,\exp \, \frac{-t}{t_{\sigma_i}}$', r'$\sigma_\phi^2 \! \propto \! t$']
        run_labels1 = [r'$\overline{v}_\phi \propto \exp \, \frac{-t}{t_{v_\phi}}$', r'$\overline{v}_\phi \propto t$']
    else:
        run_labels0 = [r'$\sigma_\phi^2 \propto 1 - \exp \, \frac{-t}{t_c}$', r'$\sigma_\phi^2 \propto t$']
        run_labels1 = [r'$\overline{v}_\phi \propto \exp \, \frac{-t}{t_c}$', r'$\overline{v}_\phi \propto t$']
    run_zorder = [-5, -4]

    theory_best_v_list = []
    theory_best_z2_list = []
    theory_best_r2_list = []
    theory_best_p2_list = []

    # (inital_velocity2_array_v, inital_velocity2_array_z, inital_velocity2_array_r, inital_velocity2_array_p
    #  ) = smooth_burn_ins(1e-2, 400, save_name_append, burn_in, bin_edges)

    galaxy_name = 'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps'
    kwargs = rcm.get_name_kwargs(galaxy_name)
    snaps = 400

    bin_edges = rcm.get_bin_edges(save_name_append, galaxy_name=galaxy_name)
    analytic_dispersion = rcm.get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)
    analytic_v_circ = rcm.get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                         save_name_append=save_name_append, galaxy_name=galaxy_name)
    # (a_h, vmax) = get_hernquist_params(galaxy_name)
    # analytic_v_circ = get_analytic_hernquist_circular_velocity(bin_edges, vmax, a_h, save_name_append)

    v200 = rcm.get_v_200(galaxy_name=galaxy_name)

    (t_c_dm, _, delta_dm, upsilon_dm, upsilon_circ
     ) = rcm.get_constants(bin_edges, save_name_append, 1e-2, galaxy_name=galaxy_name)

    theory_times = np.logspace(-5, 3, 1000)
    log_theory_times = np.log10(theory_times)
    fifty = len(theory_times)
    o = np.ones(fifty)
    tau_t = rcm.get_tau_array(theory_times, t_c_dm[:, np.newaxis])
    # tau_t = [get_tau_array(theory_times, t_c_dm[i]) for i in range(len(bin_edges)//2)]

    (inital_velocity_v, inital_velocity_z, inital_velocity_r, inital_velocity_p
     ) = rcm.smooth_burn_ins(kwargs['mdm'], snaps, save_name_append, BURN_IN, bin_edges, galaxy_name=galaxy_name)

    experiment_v = []

    for j, (ln_Lambda_ks, alphas, betas, gammas) in enumerate(run):

        # yaxis
        # theory_best_v_list.append([run_funcs[j](v200 * o, 0 * o,
        #                                         theory_times, t_c_dm[i] * o, delta_dm[i] * o,
        #                                         np.sqrt(upsilon_circ[i]) * o,
        #                                         ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
        #                            for i in range(len(bin_edges) // 2)])

        theory_best_z2_list.append([run_funcs[j](v200 ** 2 * o, 0 * o,
                                                 theory_times, t_c_dm[i]*o, delta_dm[i] *o, upsilon_dm[i] *o,
                                                 ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
                                    for i in range(len(bin_edges)//2)])

        theory_best_r2_list.append([run_funcs[j](v200 ** 2 * o, 0 * o,
                                                 theory_times, t_c_dm[i] * o, delta_dm[i] * o, upsilon_dm[i] * o,
                                                 ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
                                    for i in range(len(bin_edges) // 2)])

        theory_best_p2_list.append([run_funcs[j](v200 ** 2 * o, 0 * o,
                                                 theory_times, t_c_dm[i] * o, delta_dm[i] * o, upsilon_dm[i] * o,
                                                 ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])
                                    for i in range(len(bin_edges) // 2)])

        tau_heat_p2 = np.array([rcm.get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                                   ln_Lambda_ks[3], alphas[3], betas[3], gammas[3]) for i in
                                range(len(bin_edges) // 2)])

        tau_heat_experiment = tau_heat_p2 * 0.74987599

        tau_0_on_tau_heat = np.array([rcm.get_tau_zero_array(v200 * o, 0 * o,
                                                         np.sqrt(upsilon_circ[i])) for i in
                                      range(len(bin_edges) // 2)])

        if run_funcs[j] == rcm.get_theory_velocity2_array:
            experiment_v.append(np.array([v200 * np.sqrt(upsilon_circ[i]) ** 2 * (1 - np.exp(
                - np.power(((theory_times / t_c_dm[i]) / tau_heat_experiment[i]) + tau_0_on_tau_heat[i],
                           1 + gammas[0])))
                                          for i in range(len(bin_edges) // 2)]))

        else:  # run_funcs == get_powerlaw_velocity2_array
            experiment_v.append(np.array([v200 * np.sqrt(upsilon_circ[i]) ** 2 * (
                np.power(((theory_times / t_c_dm[i]) / tau_heat_experiment[i]),
                         1 + gammas[0])) for i in range(len(bin_edges) // 2)]))

        # experiment_v = analytic_v_circ[:, np.newaxis] - experiment_v

        # plot
        ls = run_ls[j]

        for i in range(len(bin_edges) // 2):
            if sig:
                # tau_heat_v = np.array([rcm.get_tau_heat_array(delta_dm[i], np.sqrt(upsilon_circ[i]),
                #                                               ln_Lambda_ks[0], alphas[0], betas[0], gammas[0]) for i in
                #                        range(len(bin_edges) // 2)])
                tau_heat_v = 0.75 * tau_heat_p2
                tau_heat_z2 = np.array([rcm.get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                                               ln_Lambda_ks[1], alphas[1], betas[1], gammas[1]) for i in
                                        range(len(bin_edges) // 2)])
                tau_heat_r2 = np.array([rcm.get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                                               ln_Lambda_ks[2], alphas[2], betas[2], gammas[2]) for i in
                                        range(len(bin_edges) // 2)])

                # axs[1][i].errorbar(tau_t[i], np.log10(theory_best_v_list[j][i]) - np.log10(v200),
                #                    ls=ls, lw=3, c=run_c[j], label=run_labels1[j], path_effects=pth_w, zorder=run_zorder[j])
                axs[0][i].errorbar(np.log10(tau_t[i]) - np.log10(tau_heat_v[i]),
                                   np.log10(experiment_v[j][i]) - np.log10(analytic_v_circ[i]),
                                   ls=ls, lw=3, c=run_c[j], label=run_labels1[j], path_effects=pth_w,
                                   zorder=run_zorder[j])
                axs[1][i].errorbar(np.log10(tau_t[i]) - np.log10(tau_heat_z2[i]),
                                   np.log10(theory_best_z2_list[j][i]) - 2 * np.log10(analytic_dispersion[i]),
                                   ls=ls, lw=3, c=run_c[j], label=run_labels0[j], path_effects=pth_w,
                                   zorder=run_zorder[j])
                axs[2][i].errorbar(np.log10(tau_t[i]) - np.log10(tau_heat_r2[i]),
                                   np.log10(theory_best_r2_list[j][i]) - 2 * np.log10(analytic_dispersion[i]),
                                   ls=ls, lw=3, c=run_c[j], label=run_labels0[j], path_effects=pth_w,
                                   zorder=run_zorder[j])
                axs[3][i].errorbar(np.log10(tau_t[i]) - np.log10(tau_heat_p2[i]),
                                   np.log10(theory_best_p2_list[j][i]) - 2 * np.log10(analytic_dispersion[i]),
                                   ls=ls, lw=3, c=run_c[j], label=run_labels0[j], path_effects=pth_w,
                                   zorder=run_zorder[j])

            else:
                axs[0][i].errorbar(np.log10(tau_t[i]), np.log10(experiment_v[j][i]) - np.log10(v200),
                                   ls=ls, lw=3, c=run_c[j], label=run_labels1[j], path_effects=pth_w,
                                   zorder=run_zorder[j])
                axs[1][i].errorbar(np.log10(tau_t[i]), np.log10(theory_best_z2_list[j][i]) - 2 * np.log10(v200),
                                   ls=ls, lw=3, c=run_c[j], label=run_labels0[j], path_effects=pth_w,
                                   zorder=run_zorder[j])
                axs[2][i].errorbar(np.log10(tau_t[i]), np.log10(theory_best_r2_list[j][i]) - 2 * np.log10(v200),
                                   ls=ls, lw=3, c=run_c[j], label=run_labels0[j], path_effects=pth_w,
                                   zorder=run_zorder[j])
                axs[3][i].errorbar(np.log10(tau_t[i]), np.log10(theory_best_p2_list[j][i]) - 2 * np.log10(v200),
                                   ls=ls, lw=3, c=run_c[j], label=run_labels0[j], path_effects=pth_w,
                                   zorder=run_zorder[j])

    # maximum cut off
    ls = '--'
    c = 'grey'
    alpha = 0.5
    linewidth = 5
    for i in range(len(bin_edges) // 2):
        for j in range(4):
            if i == 0 and j == 0:
                if sig:
                    axs[j][i].errorbar([xlims[0] + 0.65 * (xlims[1] - xlims[0]), xlims[1]],
                                       [0] * 2,
                                       ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
                else:
                    axs[j][i].errorbar([xlims[0] + 0.65 * (xlims[1] - xlims[0]), xlims[1]],
                                       [np.log10(analytic_v_circ[i]) - np.log10(v200)] * 2,
                                       ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
            elif i == 0 and j == 1:
                if sig:
                    axs[j][i].errorbar([xlims[0] + 0.8 * (xlims[1] - xlims[0]), xlims[1]],
                                       [0] * 2,
                                       ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
                else:
                    axs[j][i].errorbar([xlims[0] + 0.8 * (xlims[1] - xlims[0]), xlims[1]],
                                       [2 * np.log10(analytic_dispersion[i]) - 2 * np.log10(v200)] * 2,
                                       ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)

            else:
                if sig:
                    axs[j][i].errorbar(np.log10(theory_times), 0 * o,
                                       ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
                else:
                    axs[j][i].errorbar(np.log10(theory_times), 0 * o + np.log10(analytic_v_circ[i]) - np.log10(v200),
                                       ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)

    # col_list = ['C0', 'C1', 'C2', 'C3', 'C4']

    # put data on plots
    for l, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = rcm.get_name_kwargs(galaxy_name)

        v200 = rcm.get_v_200(galaxy_name)

        bin_edges = rcm.get_bin_edges(galaxy_name=galaxy_name)

        (mean_v_R, mean_v_phi,
         sigma_z, sigma_R, sigma_phi
         ) = rcm.get_dispersions(galaxy_name, save_name_append, snaps, bin_edges)

        # work out constatns
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = rcm.get_constants(bin_edges, save_name_append, kwargs['mdm'], galaxy_name=galaxy_name)

        analytic_dispersion = rcm.get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                                 save_name_append=save_name_append, galaxy_name=galaxy_name)
        analytic_v_circ = rcm.get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)

        mean_v_phi = mean_v_phi.T
        sigma_z2 = sigma_z.T ** 2
        sigma_r2 = sigma_R.T ** 2
        sigma_p2 = sigma_phi.T ** 2

        (inital_velocity_v, inital_velocity_z, inital_velocity_r, inital_velocity_p
         ) = rcm.smooth_burn_ins(kwargs['mdm'], snaps, save_name_append, BURN_IN, bin_edges, galaxy_name=galaxy_name)
        inital_velocity2_z = inital_velocity_z ** 2
        inital_velocity2_r = inital_velocity_r ** 2
        inital_velocity2_p = inital_velocity_p ** 2

        window = snaps // window_n + 1
        if window % 2 == 0: window += 1

        for i in range(len(bin_edges) // 2):
            time = np.linspace(0, T_TOT, snaps + 1)
            time = time - time[BURN_IN]

            tau = rcm.get_tau_array(time, t_c_dm[i])

            # # TODO this seems like the wrong / old alpha, beta, ln lambda fit
            # tau_vir = rcm.get_tau_heat_array(delta_dm[i], np.sqrt(upsilon_circ[i]), run[1][0][0], run[1][1][0],
            #                                  run[1][2][0], run[1][3][0])

            tau_vir = 0.75 * rcm.get_tau_heat_array(delta_dm[i], upsilon_dm[i], run[1][0][3], run[1][1][3],
                                                    run[1][2][3], run[1][3][3])
            tau_0 = tau_vir * np.log(analytic_v_circ[i] / inital_velocity_v[i])

            if sig:
                x = np.log10((tau + tau_0)/tau_vir)
                y = np.log10((analytic_v_circ[i] - mean_v_phi[i]) / analytic_v_circ[i])
            else:
                x = np.log10(tau + tau_0)
                y = np.log10((analytic_v_circ[i] - mean_v_phi[i]) / v200)

            if points:
                axs[0][i].errorbar(x, y,
                                   c=kwargs['c'], markeredgewidth=0, marker='.', ls='', lw=lwt, alpha=alpha)
            #needed to make savgol work
            if np.sum(np.isnan(y)) > 0:
                # y = np.log10(np.abs((analytic_v_circ[i] - mean_v_phi[i])) / v200)
                y = np.log10(np.abs((analytic_v_circ[i] - mean_v_phi[i]) / analytic_v_circ[i]))
            axs[0][i].errorbar(x, savgol_filter(y,  window, poly_n, mode='interp'),
                               c=kwargs['c'], marker='', ls=kwargs['ls'], lw=lw)

            tau_vir = rcm.get_tau_heat_array(delta_dm[i], upsilon_dm[i], run[1][0][1], run[1][1][1],
                                             run[1][2][1], run[1][3][1])
            tau_0 = tau_vir * np.log(analytic_dispersion[i] ** 2 / (analytic_dispersion[i] ** 2 - inital_velocity2_z[i]))

            if sig:
                x = np.log10((tau + tau_0)/tau_vir)
                y = np.log10(sigma_z2[i] / analytic_dispersion[i] ** 2)
            else:
                x = np.log10(tau + tau_0)
                y = np.log10(sigma_z2[i] / v200 ** 2)

            if points:
                axs[1][i].errorbar(x, y,
                                   c=kwargs['c'], markeredgewidth=0, marker='.', ls='', lw=lwt, alpha=alpha)
            axs[1][i].errorbar(x, savgol_filter(y,  window, poly_n, mode='interp'),
                               c=kwargs['c'], marker='', ls=kwargs['ls'], lw=lw)

            tau_vir = rcm.get_tau_heat_array(delta_dm[i], upsilon_dm[i], run[1][0][2], run[1][1][2],
                                             run[1][2][2], run[1][3][2])
            tau_0 = tau_vir * np.log(analytic_dispersion[i] ** 2 / (analytic_dispersion[i] ** 2 - inital_velocity2_r[i]))

            if sig:
                x = np.log10((tau + tau_0)/tau_vir)
                y = np.log10(sigma_r2[i] / analytic_dispersion[i] ** 2)
            else:
                x = np.log10(tau + tau_0)
                y = np.log10(sigma_r2[i] / v200 ** 2)

            if points:
                axs[2][i].errorbar(x, y,
                                   c=kwargs['c'], markeredgewidth=0, marker='.', ls='', lw=lwt, alpha=alpha)
            axs[2][i].errorbar(x, savgol_filter(y,  window, poly_n, mode='interp'),
                               c=kwargs['c'], marker='', ls=kwargs['ls'], lw=lw)

            tau_vir = rcm.get_tau_heat_array(delta_dm[i], upsilon_dm[i], run[1][0][3], run[1][1][3],
                                             run[1][2][3], run[1][3][3])
            tau_0 = tau_vir * np.log(analytic_dispersion[i] ** 2 / (analytic_dispersion[i] ** 2 - inital_velocity2_p[i]))

            if sig:
                x = np.log10((tau + tau_0)/tau_vir)
                y = np.log10(sigma_p2[i] / analytic_dispersion[i] ** 2)
            else:
                x = np.log10(tau + tau_0)
                y = np.log10(sigma_p2[i] / v200 ** 2)

            if points:
                axs[3][i].errorbar(x, y,
                                   c=kwargs['c'], markeredgewidth=0, marker='.', ls='', lw=lwt, alpha=alpha)
            axs[3][i].errorbar(x, savgol_filter(y,  window, poly_n, mode='interp'),
                               c=kwargs['c'], marker='', ls=kwargs['ls'], lw=lw)

        #lables
        if concs:
            dx = 0.47 - 0.13 * 2 * (l//3)
            dy = 0.75 - 0.14 * 2 * (l//3)  #+ 0.2  #+ 0.3 - 0.05 * (l > 2)
            x = xlims[0] + dx * (xlims[1] - xlims[0])
            y = ylims[0] + dy * (ylims[1] - ylims[0])

            xc = xlims[0] + (dx + 0.2) * (xlims[1] - xlims[0])
            yc = ylims[0] + (dy + 0.1) * (ylims[1] - ylims[0])
            dxc = -5
            if l%3 == 2: v=-0.112 #top
            elif l%3 == 1: v=-0.135 #middle
            else: v=-0.3
            dyc = v * (xlims[1] - xlims[0])

            if l%3 == 0:
                t = axs[1][1].text(x, y,
                                   r'$\mathrm{N}_{\mathrm{DM}} = $' + kwargs['labn'], color=kwargs['c'],
                                   ha='right', va='top', zorder = l + 10,
                                   bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

            else:
                t = axs[1][1].text(x, y,
                                   r'$\mathrm{N}_{\mathrm{DM}} = $' + kwargs['labn'], color=kwargs['c'],
                                   ha='right', va='top', zorder = l + 10)

            patch = patches.Rectangle((xc, yc), dxc, dyc, transform=axs[1][1].transData)

            # axs[1][1].add_artist(patch)
            t.set_clip_on(True)
            t.set_clip_path(patch)

            dx = 0.47 - 0.13 * 1.4 * (l%3)
            dy = 0.75 - 0.14 * 1.4 * (l%3)
            x = xlims[0] + dx * (xlims[1] - xlims[0])
            y = ylims[0] + dy * (ylims[1] - ylims[0])

            xc = xlims[0] + (dx + 0.2) * (xlims[1] - xlims[0])
            yc = ylims[0] + (dy + 0.1) * (ylims[1] - ylims[0])
            dxc = -5
            if l // 3 == 1:
                v = -0.115
            else:
                v = -0.3
            dyc = v * (xlims[1] - xlims[0])

            if l // 3 == 0:
                t = axs[1][2].text(xlims[0] + dx * (xlims[1] - xlims[0]), ylims[0] + dy * (ylims[1] - ylims[0]),
                                   r'$c = $' + str(kwargs['conc']), color=kwargs['c'],
                                   ha='right', va='top', zorder = l + 10,
                                   bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
            else:
                t = axs[1][2].text(xlims[0] + dx * (xlims[1] - xlims[0]), ylims[0] + dy * (ylims[1] - ylims[0]),
                                   r'$c = $' + str(kwargs['conc']), color=kwargs['c'],
                                   ha='right', va='top', zorder = l + 10)

            patch = patches.Rectangle((xc, yc), dxc, dyc, transform=axs[1][2].transData)

            t.set_clip_on(True)
            t.set_clip_path(patch)

        else:
            dx = 0.47 - 0.13 * l
            dy = 0.75 - 0.14 * l

            axs[1][1].text(xlims[0] + dx * (xlims[1] - xlims[0]), ylims[0] + dy * (ylims[1] - ylims[0]),
                           r'$\mathrm{N}_{\mathrm{DM}} = $' + kwargs['labn'], color=kwargs['c'],
                           ha='right', va='top',
                           bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    l0 = axs[0][0].legend(handlelength=1, frameon=False, loc='upper left',
                          labelspacing=0.2, handletextpad=0.4, borderaxespad=0.0)
    l1 = axs[1][0].legend(handlelength=1, frameon=False, loc='upper left',
                          labelspacing=0.2, handletextpad=0.4, borderaxespad=0.0)
    l0.set_zorder(20)
    l1.set_zorder(20)

    # x0 = xlims[0] + 0.001
    # dx = 0.1 * (xlims[1] - xlims[0])
    # y0 = ylims[0] + 0.05 * (ylims[1] - ylims[0])
    # axs[1][1].text(x0 + 1 * dx, y0, r'$\mathrm{N}_{\mathrm{DM}}$', va='bottom', ha='center', c='C4')
    # axs[1][1].text(x0 + 3 * dx, y0, r'$\mathrm{N}_{\mathrm{DM}}$', va='bottom', ha='center', c='C3')
    # axs[1][1].text(x0 + 5 * dx, y0, r'$\mathrm{N}_{\mathrm{DM}}$', va='bottom', ha='center', c='C2')
    # axs[1][1].text(x0 + 7 * dx, y0, r'$\mathrm{N}_{\mathrm{DM}}$', va='bottom', ha='center', c='C1')
    # axs[1][1].text(x0 + 9 * dx, y0, r'$\mathrm{N}_{\mathrm{DM}}$', va='bottom', ha='center', c='C0')
    # axs[1][1].text(x0 + 2 * dx, y0, r'$>$', va='bottom', ha='center')
    # axs[1][1].text(x0 + 4 * dx, y0, r'$>$', va='bottom', ha='center')
    # axs[1][1].text(x0 + 6 * dx, y0, r'$>$', va='bottom', ha='center')
    # axs[1][1].text(x0 + 8 * dx, y0, r'$>$', va='bottom', ha='center')

    p1 = 0.05
    xx = xlims[0] + (1 - p1) * (xlims[1] - xlims[0])
    yy = ylims[0] + p1 * (ylims[1] - ylims[0])
    axs[0][0].text(xx, yy, r'$R = R_{1/4, 0}$', va='bottom', ha='right')
    axs[0][1].text(xx, yy, r'$R = R_{1/2, 0}$', va='bottom', ha='right')
    axs[0][2].text(xx, yy, r'$R = R_{3/4, 0}$', va='bottom', ha='right')
    # axs[1][0].text(xx, yy, r'$R = R_{1/4}$', va='top')
    # axs[1][1].text(xx, yy, r'$R = R_{1/2}$', va='top')
    # axs[1][2].text(xx, yy, r'$R = R_{3/4}$', va='top')

    xx = xlims[0] + p1 * (xlims[1] - xlims[0])
    if sig:
        axs[0][1].text(xx, 0.05, r'$V_c$', va='bottom', ha='left')
        axs[1][1].text(xx, 0.05, r'$\sigma_{\mathrm{DM}}$', va='bottom', ha='left')
    else:
        axs[0][1].text(xx, -0.06, r'$V_c$', va='top', ha='left')
        axs[1][1].text(xx, -0.25, r'$\sigma_{\mathrm{DM}}$', va='top', ha='left')

    if sig:
        axs[0][0].set_ylabel(r'$\log ([V_c - \overline{v}_\phi] / V_c) $')
        axs[1][0].set_ylabel(r'$\log (\sigma_z^2 / \sigma_{\mathrm{DM}}^2) $')
        axs[2][0].set_ylabel(r'$\log (\sigma_R^2 / \sigma_{\mathrm{DM}}^2) $')
        axs[3][0].set_ylabel(r'$\log (\sigma_\phi^2 / \sigma_{\mathrm{DM}}^2) $')
    else:
        axs[0][0].set_ylabel(r'$\log ([V_c - \overline{v}_\phi] / V_{200}) $')
        axs[1][0].set_ylabel(r'$\log (\sigma_z^2 / V_{200}^2) $')
        axs[2][0].set_ylabel(r'$\log (\sigma_R^2 / V_{200}^2) $')
        axs[3][0].set_ylabel(r'$\log (\sigma_\phi^2 / V_{200}^2) $')

    if sig:
        axs[3][0].set_xlabel(r'$\log \, t / t_{\sigma_i} $')
        axs[3][1].set_xlabel(r'$\log \, t / t_{\sigma_i} $')
        axs[3][2].set_xlabel(r'$\log \, t / t_{\sigma_i} $')
    else:
        axs[3][0].set_xlabel(r'$\log \, t / t_c $')
        axs[3][1].set_xlabel(r'$\log \, t / t_c $')
        axs[3][2].set_xlabel(r'$\log \, t / t_c $')
    # axs[3][0].set_xlabel(r'$\log (\Delta \tau) $')
    # axs[3][1].set_xlabel(r'$\log (\Delta \tau) $')
    # axs[3][2].set_xlabel(r'$\log (\Delta \tau) $')

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    # np.seterr(all='warn')

    return


def plot_v_phi_profile(name='', save=False, model=True, pdf=True, cdf=True, mod_diff=False, fit_diff=False):
    np.seterr(all='ignore')

    _index = 1
    dex = 0.2

    n_x = pdf + cdf + mod_diff + fit_diff

    log_taus_to_plot = [-0.9, -0.4, 0.1]
    n_y = len(log_taus_to_plot)
    # xlims = [[40, 280], [-80, 300], [-250, 350]]
    xlims = [[36, 312], [-101, 367], [-303, 416]]

    n_bins = 21
    # n_bins_from_n_points = lambda n_points: int(np.ceil(np.sqrt(n_points)))
    # phi_bins_j = [lambda n_points: np.linspace(*xlims[i], n_bins_from_n_points(n_points)) for i in xlims]
    phi_bins_j = [np.linspace(*xl, n_bins) for xl in xlims]

    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(6 * n_x, 4.8 * n_y, forward=True)
    # fig.set_size_inches(12, 14, forward=True)
    spec = fig.add_gridspec(ncols=n_x, nrows=n_y)

    axs = []
    for row in range(n_y):
        axs.append([])
        for col in range(n_x):
            axs[row].append(fig.add_subplot(spec[row, col]))

            axs[row][col].set_xlim(xlims[row])

            if col == 0: axs[row][col].set_yticklabels([])

    fig.subplots_adjust(wspace=0.22, hspace=0.12)

    if pdf:
        axs[(n_y - 1) // 2][0].set_ylabel(r'Probability Density')
    if cdf:
        axs[(n_y - 1) // 2][pdf].set_ylabel(r'Cumulative Distribution')
    if mod_diff:
        axs[(n_y - 1) // 2][pdf + cdf].set_ylabel(r'Cumulative Data - Cumulative Maxwellian (Heating Model)')
    if fit_diff:
        axs[(n_y - 1) // 2][pdf + cdf + mod_diff].set_ylabel(r'Cumulative Data - Cumulative Maxwellian (Best Fit to Current Snapshot)')

    for i in range(n_x):
        axs[n_y - 1][i].set_xlabel(r'$v_\phi$ [km/s]')

    bin_edges = rcm.get_bin_edges()
    bin_centre = rcm.get_bin_edges('cum')[2 * _index + 1]

    galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps']
    snap_list = [400,400,101,101,101]

    (ln_Lambda_ks, alphas, betas, gammas
     ) = rcm.find_least_log_squares_best_fit(mcmc=False, fixed_b=0, fixed_g=0)

    for k, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snap_list)):
        kwargs = rcm.get_name_kwargs(galaxy_name)
        # print(galaxy_name)

        # (mean_v_R, mean_v_phi,
        #  sigma_z, sigma_R, sigma_phi
        #  ) = rcm.get_dispersions(galaxy_name, save_name_append, snaps, bin_edges)

        bin_edges = rcm.get_bin_edges(galaxy_name=galaxy_name)

        analytic_dispersion = rcm.get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                                     galaxy_name=galaxy_name)
        analytic_v_circ = rcm.get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                                 galaxy_name=galaxy_name)

        v200 = rcm.get_v_200(galaxy_name=galaxy_name)

        (t_c_dm, _, delta_dm, upsilon_dm, upsilon_circ
         ) = rcm.get_constants(bin_edges, save_name_append, kwargs['mdm'], galaxy_name=galaxy_name)

        theory_times = np.logspace(-5, 3, 1000)
        o = np.ones(len(theory_times))
        tau_t = rcm.get_tau_array(theory_times, t_c_dm[:, np.newaxis])

        (inital_velocity_v, inital_velocity_z, inital_velocity_r, inital_velocity_p
         ) = rcm.smooth_burn_ins(kwargs['mdm'], snaps, save_name_append, BURN_IN, bin_edges,
                                 galaxy_name=galaxy_name)
        inital_velocity2_p = inital_velocity_p**2

        theory_best_p2 = rcm.get_theory_velocity2_array(v200 ** 2 * o, 0 * o,
                                                        theory_times, t_c_dm[_index] * o, delta_dm[_index] * o,
                                                        upsilon_dm[_index] * o,
                                                        ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

        tau_heat_p2 = rcm.get_tau_heat_array(delta_dm[_index], upsilon_dm[_index],
                                             ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

        tau_heat_experiment = tau_heat_p2 * 0.74987599

        tau_0_on_tau_heat = rcm.get_tau_zero_array(v200 * o, 0 * o, np.sqrt(upsilon_circ[_index]))

        theory_best_v = analytic_v_circ[_index] - v200 * np.sqrt(upsilon_circ[_index]) ** 2 * (1 - np.exp(
            - np.power(((theory_times / t_c_dm[_index]) / tau_heat_experiment) + tau_0_on_tau_heat,
                       1 + gammas[0])))

        tau_vir_sigma_phi = rcm.get_tau_heat_array(delta_dm[_index], upsilon_dm[_index],
                                         ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])
        tau_0_sigma_phi = tau_vir_sigma_phi * np.log(analytic_dispersion[_index] ** 2 /
                                                     (analytic_dispersion[_index] ** 2 - inital_velocity2_p[_index]))

        tau_vir_mean_v_phi = 0.75 * tau_vir_sigma_phi
        tau_0_mean_v_phi = tau_vir_mean_v_phi * np.log(analytic_v_circ[_index] / inital_velocity_v[_index])

        theory_tau = rcm.get_tau_array(theory_times, t_c_dm[_index])
        theory_log_taus_sigma_phi = np.log10((theory_tau + 0)/tau_vir_sigma_phi)
        theory_log_taus_mean_v_phi = np.log10((theory_tau + 0)/tau_vir_mean_v_phi)

        #sim times
        time = np.linspace(0, T_TOT, snaps + 1)
        time = time - time[BURN_IN]
        tau = rcm.get_tau_array(time, t_c_dm[_index])
        log_taus_sigma_phi = np.log10((tau + tau_0_sigma_phi)/tau_vir_sigma_phi)
        log_taus_mean_v_phi = np.log10((tau + tau_0_mean_v_phi)/tau_vir_mean_v_phi)

        for j, log_tau_to_plot in enumerate(log_taus_to_plot):
            # snap = np.nanargmin(np.abs(log_taus_sigma_phi - log_tau_to_plot))
            snap = np.nanargmin(np.abs(log_taus_mean_v_phi - log_tau_to_plot))

            if snap < snaps:

                # theory_snap_sigma_phi = np.nanargmin(np.abs(theory_log_taus_sigma_phi - log_tau_to_plot))
                # theory_snap_mean_v_phi = np.nanargmin(np.abs(theory_log_taus_sigma_phi - log_tau_to_plot))
                theory_snap_sigma_phi = np.nanargmin(np.abs(theory_log_taus_mean_v_phi - log_tau_to_plot))
                theory_snap_mean_v_phi = np.nanargmin(np.abs(theory_log_taus_mean_v_phi - log_tau_to_plot))

                if os.path.exists('../galaxies/' + galaxy_name + '_seed4'): max_seed = 5
                if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

                n_points_list = []
                binned_phi_list = []

                for i in range(max_seed):
                    galaxy_name_i = galaxy_name + '_seed' + str(i)
                    if i == 0:
                        galaxy_name_i = galaxy_name

                    (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
                     MassStar, PosStars, VelStars, IDStars, PotStars
                     ) = load_nbody.load_and_align(galaxy_name, snap='{0:03d}'.format(int(snap)))

                    (R, phi, z, v_R, v_phi, v_z) = rcm.get_cylindrical(PosStars, VelStars)

                    experiment_bin_edges = rcm.get_dex_bins([bin_centre], dex)
                    mask = np.logical_and(R > experiment_bin_edges[0], R < experiment_bin_edges[1])

                    n_points = np.sum(mask)
                    binned_phi = v_phi[mask]

                    n_points_list.append(n_points)
                    binned_phi_list.append(binned_phi)

                n_points = np.sum(n_points_list)
                binned_phi = np.hstack(binned_phi_list)

                lin = np.linspace(-500, 500, 2001)
                #decorations
                if model:
                    if pdf:
                        axs[j][0].errorbar(lin, scipy.stats.norm.pdf(
                            lin, np.mean(binned_phi), np.std(binned_phi)),
                                             ls='-.', color=kwargs['c'], lw=3, zorder=-10)

                        if j == 1:
                            axs[j][0].axvline(np.median(binned_phi), 0, 0.75,
                                              ls=':', color=kwargs['c'], lw=1)
                            axs[j][0].axvline(np.quantile(binned_phi, 0.16), 0, 0.75,
                                              ls=':', color=kwargs['c'], lw=1)
                        else:
                            axs[j][0].axvline(np.median(binned_phi), 0, 1,
                                              ls=':', color=kwargs['c'], lw=1)
                            axs[j][0].axvline(np.quantile(binned_phi, 0.16), 0, 1,
                                              ls=':', color=kwargs['c'], lw=1)
                        axs[j][0].axvline(np.quantile(binned_phi, 0.84), 0, 1,
                                          ls=':', color=kwargs['c'], lw=1)

                    if cdf:
                        axs[j][pdf].errorbar(lin, scipy.stats.norm.cdf(
                            lin, np.mean(binned_phi), np.std(binned_phi)),
                                             ls='-.', color=kwargs['c'], lw=3, zorder=-10)

                        # if j != 2:
                        axs[j][pdf].axvline(np.median(binned_phi), 0, 1,
                                          ls=':', color=kwargs['c'], lw=1)
                        axs[j][pdf].axvline(np.quantile(binned_phi, 0.16), 0, 0.75,
                                          ls=':', color=kwargs['c'], lw=1)
                        axs[j][pdf].axvline(np.quantile(binned_phi, 0.84), 0, 1,
                                          ls=':', color=kwargs['c'], lw=1)
                        # else:
                        #     axs[j][pdf].axvline(np.median(binned_phi), 0, 1,
                        #                       ls=':', color=kwargs['c'], lw=1)
                        #     axs[j][pdf].axvline(np.quantile(binned_phi, 0.16), 0, 1,
                        #                       ls=':', color=kwargs['c'], lw=1)
                        #     axs[j][pdf].axvline(np.quantile(binned_phi, 0.84), 0.25, 1,
                        #                       ls=':', color=kwargs['c'], lw=1)

                #raw data
                if pdf:
                    axs[j][0].hist(binned_phi, phi_bins_j[j], density=True, histtype='step',
                                   ls=kwargs['ls'], color=kwargs['c'], lw=2, path_effects=pth_4)

                binned_phi = np.sort(binned_phi)
                if cdf:
                    axs[j][pdf].errorbar(binned_phi, np.linspace(0, 1, len(binned_phi)),
                                       ls=kwargs['ls'], c=kwargs['c'], lw=2, path_effects=pth_4)

                if mod_diff:
                    axs[j][pdf + cdf].errorbar(binned_phi, np.linspace(0, 1, len(binned_phi)) - scipy.stats.norm.cdf(
                        binned_phi, theory_best_v[theory_snap_mean_v_phi], np.sqrt(theory_best_p2[theory_snap_sigma_phi])),
                                       ls=kwargs['ls'], c=kwargs['c'], lw=2, path_effects=pth_4)

                if fit_diff:
                    axs[j][pdf + cdf + mod_diff].errorbar(binned_phi,
                        np.linspace(0, 1, len(binned_phi)) - scipy.stats.norm.cdf(
                        binned_phi, np.mean(binned_phi), np.std(binned_phi)),
                                       ls=kwargs['ls'], c=kwargs['c'], lw=2, path_effects=pth_4)

                #TODO make sure this doesn't plot 1000000 times
                if model and k == 0:
                    if pdf:
                        axs[j][0].errorbar(lin, scipy.stats.norm.pdf(
                            lin, theory_best_v[theory_snap_mean_v_phi],
                            np.sqrt(theory_best_p2[theory_snap_sigma_phi])),
                                       ls='--', color='k', lw=3, zorder=10)

                        if j == 1:
                            axs[j][0].axvline(theory_best_v[theory_snap_mean_v_phi], 0, 0.75,
                                              ls='--', color='k', lw=1)
                            axs[j][0].axvline(theory_best_v[theory_snap_mean_v_phi] -
                                              np.sqrt(theory_best_p2[theory_snap_sigma_phi]), 0, 0.75,
                                              ls='--', color='k', lw=1)
                        else:
                            axs[j][0].axvline(theory_best_v[theory_snap_mean_v_phi], 0, 1,
                                              ls='--', color='k', lw=1)
                            axs[j][0].axvline(theory_best_v[theory_snap_mean_v_phi] -
                                              np.sqrt(theory_best_p2[theory_snap_sigma_phi]), 0, 1,
                                              ls='--', color='k', lw=1)
                        axs[j][0].axvline(theory_best_v[theory_snap_mean_v_phi] +
                                          np.sqrt(theory_best_p2[theory_snap_sigma_phi]), 0, 1,
                                          ls='--', color='k', lw=1)

                    if cdf:
                        axs[j][pdf].errorbar(lin, scipy.stats.norm.cdf(
                            lin, theory_best_v[theory_snap_mean_v_phi],
                            np.sqrt(theory_best_p2[theory_snap_sigma_phi])),
                                       ls='--', color='k', lw=3, zorder=10)

                        # if j != 2:
                        axs[j][pdf].axvline(theory_best_v[theory_snap_mean_v_phi], 0, 1,
                                          ls='--', color='k', lw=1)
                        axs[j][pdf].axvline(theory_best_v[theory_snap_mean_v_phi] -
                                          np.sqrt(theory_best_p2[theory_snap_sigma_phi]), 0, 0.75,
                                          ls='--', color='k', lw=1)
                        axs[j][pdf].axvline(theory_best_v[theory_snap_mean_v_phi] +
                                          np.sqrt(theory_best_p2[theory_snap_sigma_phi]), 0, 1,
                                          ls='--', color='k', lw=1)
                        # else:
                        #     axs[j][pdf].axvline(theory_best_v[theory_snap_mean_v_phi], 0, 1,
                        #                       ls='--', color='k', lw=1)
                        #     axs[j][pdf].axvline(theory_best_v[theory_snap_mean_v_phi] -
                        #                       np.sqrt(theory_best_p2[theory_snap_sigma_phi]), 0, 1,
                        #                       ls='--', color='k', lw=1)
                        #     axs[j][pdf].axvline(theory_best_v[theory_snap_mean_v_phi] +
                        #                       np.sqrt(theory_best_p2[theory_snap_sigma_phi]), 0.25, 1,
                        #                       ls='--', color='k', lw=1)

                    if mod_diff:
                        axs[j][pdf + cdf].errorbar([lin[0], lin[-1]], [0, 0],
                                           ls='--', color='k', lw=3, zorder=10)

                    if fit_diff:
                        axs[j][pdf + cdf + mod_diff].errorbar([lin[0], lin[-1]], [0, 0],
                                           ls='--', color='k', lw=3, zorder=10)

                if j == 1:
                    top_y = scipy.stats.norm.pdf(0, 0, np.sqrt(theory_best_p2[theory_snap_sigma_phi]))

            if k == 0:
                # if j != 2:
                axs[j][1].text(xlims[j][0] + 0.05 * (xlims[j][1] - xlims[j][0]), 0.95,
                               # r'$\log \, t/t_{v_\phi}} $= ' + str(round(log_tau_to_plot, 1)),
                               r'$t/t_{v_\phi}} $= ' + str(round(10**log_tau_to_plot, 1)),
                               va='top')
                # else:
                #     axs[j][1].text(xlims[j][0] + 0.95 * (xlims[j][1] - xlims[j][0]), 0.05,
                #                    # r'$\log \, t/t_{v_\phi}} $= ' + str(round(log_tau_to_plot, 1)),
                #                    r'$t/t_{v_\phi}} $= ' + str(round(10 ** log_tau_to_plot, 1)),
                #                    va='bottom', ha='right')


    axs[2][1].legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='-', lw=2, path_effects=pth_4)),
                      (lines.Line2D([0, 1], [0, 1], color='grey', ls='-.', lw=3)),
                      (lines.Line2D([0, 1], [0, 1], color='k', ls='--', lw=3))],
                     ['Data', 'Fitted\nMaxwellian', 'Modelled\nMaxwellian'],
                     frameon=True, loc='upper right', bbox_to_anchor=(0.26,0.805), framealpha=0.9)
                     # framealpha=1, loc='upper left')

    axs[1][0].text(xlims[1][0] + 0.05 * (xlims[1][1] - xlims[1][0]), 1.12 * top_y,
                   r'$V_{200} = 200$ km/s',
                   ha='left', va='top')
    axs[1][0].text(xlims[1][0] + 0.05 * (xlims[1][1] - xlims[1][0]), 0.97 * top_y,
                   r'$R = R_{1/2, 0}$',
                   ha='left', va='top')

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    np.seterr(all='ignore')

    return


def plot_size_evolution(name='', save=False, linear_fit=False):
    np.seterr(all='ignore')

    galaxy_name_list = [
        'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
        'mu_2/fdisk_0p01_lgMdm_8_V200_200kmps',
        'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps',
        'mu_0p77/fdisk_0p01_lgMdm_8_V200_200kmps',
        'mu_0p5/fdisk_0p01_lgMdm_8_V200_200kmps',
        'mu_0p2/fdisk_0p01_lgMdm_8_V200_200kmps',
    ]
    # galaxy_name_list = [
        # 'mu_0p77/fdisk_0p01_lgMdm_8_V200_200kmps',
        # 'mu_0p77/fdisk_0p01_lgMdm_8_V200_200kmps_seed0',
        # 'mu_0p77/fdisk_0p01_lgMdm_8_V200_200kmps_seed1',
        # 'mu_0p77/fdisk_0p01_lgMdm_8_V200_200kmps_seed2',
        # 'mu_0p77/fdisk_0p01_lgMdm_8_V200_200kmps_seed3',
        # 'mu_0p77/fdisk_0p01_lgMdm_8_V200_200kmps_seed4',
        # 'mu_0p77/fdisk_0p01_lgMdm_8_V200_200kmps_seed5',
        # 'mu_0p77/fdisk_0p01_lgMdm_8_V200_200kmps_seed6',
        # 'mu_0p77/fdisk_0p01_lgMdm_8_V200_200kmps_seed7',
        # 'mu_0p77/fdisk_0p01_lgMdm_8_V200_200kmps_seed8',
        # 'mu_0p77/fdisk_0p01_lgMdm_8_V200_200kmps_seed9',
    # ]
    snaps_list = [400] * len(galaxy_name_list)

    # galaxy_name_list = [
    #     'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #     'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #     # 'mu_25/fdisk_0p01_lgMdm_7p5_V200-200kmps',
    #     # 'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
    #     # 'mu_1/fdisk_0p01_lgMdm_7p5_V200-200kmps',
    # ]

    # galaxy_name_list = [
    #     'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #     # 'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed0',
    #     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed1',
    #     'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed2',
    #     'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed3',
    #     'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed4',
    #     'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed5',
    #     'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed6',
    #     'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed7',
    #     'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed8',
    #     'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed9',
    #     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #     # 'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed0',
    #     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed1',
    #     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed2',
    #     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed3',
    #     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed4',
    #     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed5',
    #     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed6',
    #     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed7',
    #     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed8',
    #     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed9',
    #     'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #     # 'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed0',
    #     'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed1',
    #     'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed2',
    #     'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed3',
    #     'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed4',
    #     'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed5',
    #     'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed6',
    #     'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed7',
    #     'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed8',
    #     'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed9',
    # ]
    # snaps_list = [400] * len(galaxy_name_list)

    _index = 1
    R_d_ICs = 4.16

    half = 0.5
    R_d_norm = (-lambertw((half - 1) / np.exp(1), -1) - 1).real

    R_half_ICs = 4.16 * R_d_norm

    fig = plt.figure(figsize=(6, 6))
    ax0 = plt.subplot(2, 2, 1)
    ax1 = plt.subplot(2, 2, 3)
    ax2 = plt.subplot(2, 2, 4)

    mus = []
    values = []
    slopes = []

    Rdt = []

    if np.any(['mu_0p5' in gn for gn in galaxy_name_list]):
        # R_d_lims = np.array([2.0, 7.0])
        R_d_lims = np.array([0.6, 6.8])
    else:
        # R_d_lims = np.array([3.9, 6.9])
        # R_d_lims = np.array([0.6, 6.8])
        R_d_lims = np.array([3, 5])

    for ii, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = rcm.get_name_kwargs(galaxy_name)

        R_ds = rcm.get_stacked_exponential_fit(galaxy_name, snaps, overwrite=False)
        R_half = rcm.get_R_half(galaxy_name, snaps, overwrite=False)

        # #TODO fix this
        # if '_8_' in galaxy_name:
        #     R_ds *= 1.02
        #     R_half *= 1.02

        time = np.linspace(0, T_TOT, snaps + 1)
        time -= time[BURN_IN]

        if '7p5' in galaxy_name:
            time *= 10**-0.5

        Rdt.append([(R_ds[snaps//10] - R_ds[BURN_IN]) / R_ds[BURN_IN] / (time[snaps//10]-time[BURN_IN]),
                    (R_ds[snaps//5] - R_ds[BURN_IN]) / R_ds[BURN_IN] / (time[snaps//5]-time[BURN_IN]),
                    (R_ds[snaps//2] - R_ds[BURN_IN]) / R_ds[BURN_IN] / (time[snaps//2]-time[BURN_IN]),
                    (R_ds[-1] - R_ds[BURN_IN]) / R_ds[BURN_IN] / (time[-1]-time[BURN_IN])])

        lin_fit_x = time
        lin_fit_y = R_ds

        # color = matplotlib.cm.get_cmap('turbo')((np.log10(0.2 / 1.01) - np.log10(kwargs['mun'])) /
        #                                         (np.log10(0.2 / 1.01) - np.log10(25 * 1.01)))

        color = 'k'
        if np.any(['mu_0p5' in gn for gn in galaxy_name_list]):
            # mu_map = 'cividis'
            # if 'mu_0p2'  in galaxy_name: color = matplotlib.cm.get_cmap(mu_map)(0.001)
            # elif 'mu_0p5'in galaxy_name: color = matplotlib.cm.get_cmap(mu_map)(0.2)
            # elif 'mu_1'  in galaxy_name: color = matplotlib.cm.get_cmap(mu_map)(0.4)
            # elif 'mu_2/' in galaxy_name: color = matplotlib.cm.get_cmap(mu_map)(0.6)
            # elif 'mu_5'  in galaxy_name: color = matplotlib.cm.get_cmap(mu_map)(0.8)
            # elif 'mu_25' in galaxy_name: color = matplotlib.cm.get_cmap(mu_map)(0.999)

            mu_map = 'plasma'
            if 'mu_0p2'  in galaxy_name: color = matplotlib.cm.get_cmap(mu_map)(0.4)
            elif 'mu_0p5'in galaxy_name: color = matplotlib.cm.get_cmap(mu_map)(0.17)
            elif 'mu_0p77'in galaxy_name: color = matplotlib.cm.get_cmap(mu_map)(0.07)
            elif 'mu_1'  in galaxy_name: color = matplotlib.cm.get_cmap('viridis')(0.10)
            elif 'mu_2/' in galaxy_name: color = matplotlib.cm.get_cmap('viridis')(0.27) # (0.8 / np.log10(25)) * np.log10(2) + 0.1
            elif 'mu_5'  in galaxy_name: color = matplotlib.cm.get_cmap('viridis')(0.5)
            elif 'mu_25' in galaxy_name: color = matplotlib.cm.get_cmap('viridis')(0.9)

        else:
            mu_map = 'viridis'
            if 'mu_1' in galaxy_name: color = matplotlib.cm.get_cmap(mu_map)(0.1)
            elif 'mu_5' in galaxy_name: color = matplotlib.cm.get_cmap(mu_map)(0.5)
            elif 'mu_25' in galaxy_name: color = matplotlib.cm.get_cmap(mu_map)(0.9)

        window = snaps // 5 + 1
        if window % 2 == 0: window += 1

        ax0.errorbar(time, R_ds,
                     c=color, marker='', ls='-', lw=lwt, alpha=alpha)
        try:
            ax0.errorbar(time, savgol_filter(R_ds, window, poly_n),
                         c=color, marker='', ls='-', lw=lw)
        except np.linalg.LinAlgError:
            ax1.errorbar(time, R_ds,
                         c=color, marker='', ls='-', lw=lw)

        if linear_fit:
            chi2 = lambda slope: np.sum((lin_fit_y - R_d_ICs - slope * lin_fit_x) ** 2)
            out = minimize(chi2, 0.1)
            slope = out.x[0]
            # print(out.x)
            mus.append(kwargs['mun'])
            values.append(R_ds)
            slopes.append(slope / R_ds[BURN_IN])

            _x = np.linspace(0, T_TOT)
            ax0.errorbar(_x, R_d_ICs + slope * _x, c=color, ls='--', lw=2)

        #r half
        lin_fit_y = R_half

        ax1.errorbar(time, R_half,# / R_d_norm,
                     c=color, marker='', ls='-', lw=lwt, alpha=alpha)
        # ax0.errorbar(time, savgol_filter(R_half / R_d_norm, window, poly_n),
        try:
            ax1.errorbar(time, savgol_filter(R_half, window, poly_n),
                         c=color, marker='', ls='-', lw=lw)
        except np.linalg.LinAlgError:
            print(R_half)
            ax1.errorbar(time, R_half,
                         c=color, marker='', ls='-', lw=lw)

        if linear_fit:
            chi2 = lambda slope: np.sum((lin_fit_y - R_half_ICs - slope * lin_fit_x) ** 2)
            out = minimize(chi2, 0.1)
            slope = out.x[0]
            # print(out.x)

            _x = np.linspace(0, T_TOT)
            ax1.errorbar(_x, R_half_ICs + slope * _x, c=color, ls='--', lw=2)

        #
        ax2.errorbar(R_ds, R_half,# / R_d_norm,
                     c=color, marker='', ls='-', lw=lwt, alpha=alpha)
        # ax0.errorbar(time, savgol_filter(R_half / R_d_norm, window, poly_n),
        try:
            ax2.errorbar(savgol_filter(R_ds, window, poly_n), savgol_filter(R_half, window, poly_n),
                         c=color, marker='', ls='-', lw=lw)
        except:
            ax2.errorbar(R_ds, R_half,# / R_d_norm,
                         c=color, marker='', ls='-', lw=lw)

        #label
        if ii < 4:
            dx = 0.05 # + 0.20 * (len(galaxy_name_list) - 1 - ii)
            dy = 1 - 0.05 - 0.12 * ii
            # ax2.text(0 + dx * (T_TOT), R_d_lims[0] + dy * (R_d_lims[1] - R_d_lims[0]),
            ax2.text((R_d_lims[1] - R_d_lims[0]) * dx + R_d_lims[0],
                     (R_d_lims[0] + dy * (R_d_lims[1] - R_d_lims[0])) * R_d_norm,
                     kwargs['mu'], color=color, ha='left', va='top')
        else:
            dx = 0.05  # + 0.20 * (len(galaxy_name_list) - 1 - ii)
            dy = 0.12 * (len(galaxy_name_list) - 5) + 0.05 + 0.12 * (4 - ii)
            # ax2.text(0 + dx * (T_TOT), R_d_lims[0] + dy * (R_d_lims[1] - R_d_lims[0]),
            ax2.text((R_d_lims[1] - R_d_lims[0]) * (1 - dx) + R_d_lims[0],
                     (R_d_lims[0] + dy * (R_d_lims[1] - R_d_lims[0])) * R_d_norm,
                     kwargs['mu'], color=color, ha='right', va='bottom')

    ax2.errorbar([0, 10], [0*R_d_norm, 10*R_d_norm], c='k', ls='--')

    # ax0.set_xlabel(r'$t$ [Gyr]')
    ax0.set_ylabel(r'$R_d$ [kpc]')

    ax1.set_xlabel(r'$t$ [Gyr]')
    if np.any(['7p5' in galaxy_name for galaxy_name in galaxy_name_list]):
        ax1.set_xlabel(r'$t \times t_{\sigma_i} / t_{\sigma_i, \mathrm{lowest-resolution}}$ [Gyr]')
    else:
        ax1.set_ylabel(r'$R_{1/2}$ [kpc]')

    ax2.set_xlabel(r'$R_d$ [kpc]')

    ax0.set_ylim(R_d_lims)
    ax1.set_ylim(R_d_lims*R_d_norm)
    ax2.set_ylim(R_d_lims*R_d_norm)
    ax2.set_xlim(R_d_lims)

    ax0.set_xlim(0, T_TOT)
    ax1.set_xlim(0, T_TOT)

    ax2.set_aspect(1/R_d_norm) #'equal')

    ax0.set_xticklabels([])
    ax2.set_yticklabels([])

    fig.subplots_adjust(hspace=0.0, wspace=0.0)

    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    # plt.figure()
    #
    # plt.scatter(np.log10(mus), slopes, marker='o',
    #             c=matplotlib.cm.get_cmap('turbo')((np.log10(0.2 / 1.01) - np.log10(mus)) /
    #                                               (np.log10(0.2 / 1.01) - np.log10(25 * 1.01))))
    # plt.xlabel(r'$\log \, \mu$')
    # plt.ylabel(r'$\mathrm{d} R_d / \mathrm{d} t$ [kpc / Gyr]')

    # ax0.set_xlim([0, 0.101])
    # ax0.set_ylim([3.5, 7])

    # ax0.legend([(lines.Line2D([0, 1], [0, 1], color='C9', ls='-', lw=3, path_effects=pth)),
    #             (lines.Line2D([0, 1], [0, 1], color='C6', ls='--', lw=3, path_effects=pth)),
    #             (lines.Line2D([0, 1], [0, 1], color='C5', ls=':', lw=3, path_effects=pth)),
    #             (lines.Line2D([0, 1], [0, 1], color='k', ls='-.', lw=2))],
    #            [r'$\mu = 25$', r'$\mu = 5$', r'$\mu = 1$', 'Linear Fit'],
    #            handler_map={tuple: HandlerTupleVertical(ndivide=5)},
    #            loc='upper left', numpoints=1, handlelength=1, handletextpad=0.2,
    #            labelspacing=0, borderpad=0.2)  # , bbox_to_anchor=(-0.025, -0.053))

    #slopes?
    if linear_fit:
        plt.figure()
        plt.errorbar(np.log10(mus), np.array(Rdt)[:,0], marker='o')
        plt.errorbar(np.log10(mus), np.array(Rdt)[:,1], marker='o')
        plt.errorbar(np.log10(mus), np.array(Rdt)[:,2], marker='o')
        plt.errorbar(np.log10(mus), np.array(Rdt)[:,3], marker='o')
        plt.errorbar(np.log10(mus), np.array(np.zeros(len(mus))), c='k', ls='--')

        plt.errorbar(np.log10(mus), slopes, marker='o')

        plt.xlabel(r'$\log \, \mu$')
        plt.ylabel(r'$( \Delta R_d / R_{d, 0} ) / \Delta t $ [Gyr$^{-1}$]')

        if save:
            plt.savefig(name[:-4] + '_slopes', bbox_inches='tight')
            plt.close()

    return

def plot_ideal_j_evolution(name='', save=False, do_DM=False):

    # galaxy_name_list = [
    #     'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #     'mu_2/fdisk_0p01_lgMdm_8_V200_200kmps',
    #     'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #     'mu_0p77/fdisk_0p01_lgMdm_8_V200_200kmps',
    #     'mu_0p5/fdisk_0p01_lgMdm_8_V200_200kmps',
    #     # 'mu_0p2/fdisk_0p01_lgMdm_8_V200_200kmps',
    # ]
    # snaps_list = [400] * len(galaxy_name_list)

    # galaxy_name_list = [
    #     'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #     'mu_2/fdisk_0p01_lgMdm_8_V200_200kmps',
    #     'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #     'mu_0p5/fdisk_0p01_lgMdm_8_V200_200kmps',
    #     'mu_0p2/fdisk_0p01_lgMdm_8_V200_200kmps',
    #     'mu_25/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #     'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #     # 'mu_2/fdisk_0p01_lgMdm_7_V200_200kmps',
    #     'mu_1/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #     'mu_0p5/fdisk_0p01_lgMdm_7_V200_200kmps',
    #     'mu_0p2/fdisk_0p01_lgMdm_7_V200_200kmps',
    # ]
    # snaps_list = [400] * 6 + [101, 101, #100,
    #                           101, 100, 100]

    # galaxy_name_list = [
    #                     'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_1_k_1/fdisk_0p01_lgMdm_8_V200_200kmps',
    #                     ]
    # snaps_list = [400, 400]

    galaxy_name_list = [
                        # 'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        # 'mu_1_k_1/fdisk_0p01_lgMdm_8_V200_200kmps',
                        'mu_1_k_-0p03/fdisk_0p01_lgMdm_8_V200_200kmps',
                        'mu_1_k_-0p1/fdisk_0p01_lgMdm_8_V200_200kmps',
                        'mu_1_k_-0p3/fdisk_0p01_lgMdm_8_V200_200kmps',
                        'mu_1_k_-0p6/fdisk_0p01_lgMdm_8_V200_200kmps',
                        ]
    snaps_list = [400, 400, 400, 400, 400, 400]

    # galaxy_name_list = [
    #                     'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_25/fdisk_0p01_lgMdm_7p5_V200-200kmps',
    #                     'mu_25/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     # 'mu_25/fdisk_0p01_lgMdm_6p5_V200-200kmps',
    #                     # 'mu_25/fdisk_0p01_lgMdm_6p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     # 'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
    #                     # 'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps',
    #                     'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_1/fdisk_0p01_lgMdm_7p5_V200-200kmps',
    #                     'mu_1/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     # 'mu_1/fdisk_0p01_lgMdm_6p5_V200-200kmps',
    #                     # 'mu_1/fdisk_0p01_lgMdm_6p0_V200-200kmps',
    #                     ]
    # # # snaps_list = [400, 400, 101, 101, 101,
    # # #               400, 400, 101, 101, 101,
    # # #               400, 400, 101, 101,] #101]
    # snaps_list = [400, 400, 101, #101, 101,
    #               400, 400, 101, #101, 101,
    #               400, 400, 101,] #101,] #101]

    bin_edges = rcm.get_bin_edges(save_name_append)

    _index = 1
    dex = 0.2

    n_skip = 2
    if do_DM:
        n_skip = 0

    fig = plt.figure()  # constrained_layout=True)
    # fig.set_size_inches(12, 9, forward=True)
    fig.set_size_inches(20, 9, forward=True)
    ncols = 5 - n_skip

    spec = fig.add_gridspec(ncols=ncols, nrows=3,  # width_ratios=[1]*(ncols*four-1),
                            height_ratios=[1, 0.27, 1])

    # ax2 = fig.add_subplot(spec[2, :-1])
    # cax2 = fig.add_subplot(spec[2, four*ncols-2])
    ax2 = fig.add_subplot(spec[2, :])
    ax_ins0 = [fig.add_subplot(spec[ncols-i-1]) for i in range(ncols)]

    # fig.subplots_adjust(hspace=0.25, wspace=0.2)
    fig.subplots_adjust(hspace=0.0, wspace=0.0)

    (ln_Lambda_ks, alphas, betas, gammas
     ) = rcm.find_least_log_squares_best_fit(mcmc=False, fixed_b=0, fixed_g=0)

    # work out taus to plot
    log_taus_to_plot = []
    min_scale_time = 1e100

    v200 = rcm.get_v_200()
    r200 = rcm.get_r_200()

    analytic_v_circ = rcm.get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True)
    analytic_dispersion = rcm.get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True)

    a_h, vmax = rcm.get_hernquist_params()
    analytic_v_esc = rcm.get_analytic_hernquist_escape_velocity(bin_edges, vmax, a_h, save_name_append='diff')

    M_disk, R_d = rcm.get_exponential_disk_params()

    bin_edges = rcm.get_bin_edges()
    bin_centre = rcm.get_bin_edges('cum')[2 * _index + 1]

    # xlims = [[0.6,  1.3],
    #          [0.35, 1.45],
    #          [-0.1, 1.6],
    #          [-0.8, 1.8],
    #          [-1.6, 2.0]]
    xlims = [-2.8, 0.2] #[-1,3]
    pdflims = [0, 4.5] #[-1.5, 1.2]

    # xlim = [-1, np.log10(T_TOT * (400 - BURN_IN) / 400)] #[-2.1, 0.22]  # [-3.84, -1.34]
    xlim = [np.log10(0.046) - 2, np.log10(0.046)]
    ylim = [2.95, 3.3]
    # ylim = [2, 4]

    if np.any(['mu_0p5' in gn for gn in galaxy_name_list]):
        n_threshold = 50
    else:
        n_threshold = 10
    n_threshold = 1

    _radial_bins = np.logspace(-1, 3, 21)
    _analytic_v_circ = rcm.get_velocity_scale(np.repeat(_radial_bins,2), 0, 0, 0, 0, 0, 0, analytic_v_c=True)
    # radial_bins = np.logspace(-1, 3, 21)
    radial_bins = np.logspace(-1, 3, 21)

    for i in range(ncols):

        ax_ins0[i].set_ylim(pdflims)
        ax_ins0[i].set_xlim(xlims)
        # ax_ins0[i].set_xticks([-1,0,1,2])

        if i != ncols - 1:
            ax_ins0[i].set_yticklabels([])

        ax_ins0[i].errorbar(np.log10(_radial_bins / r200), np.log10(_radial_bins * _analytic_v_circ), c='k', ls='--')
        if do_DM:
            ax_ins0[i].errorbar(np.log10(_radial_bins / r200), np.log10(_radial_bins * _analytic_v_circ * 0.03),
                                c='k', ls='-.')
            ax_ins0[i].errorbar(np.log10(_radial_bins / r200), np.log10(_radial_bins * _analytic_v_circ * 0.1),
                                c='k', ls='-.')
            ax_ins0[i].errorbar(np.log10(_radial_bins / r200), np.log10(_radial_bins * _analytic_v_circ * 0.3),
                                c='k', ls='-.')
            ax_ins0[i].errorbar(np.log10(_radial_bins / r200), np.log10(_radial_bins * _analytic_v_circ * 0.6),
                                c='k', ls='-.')

        ax_ins0[i].axvline(np.log10(R_d / r200), 0.15, 1, c='grey', ls=':')

    ax_ins0[ncols-1].text(np.log10(R_d / r200), pdflims[1] * 0.95 + pdflims[0], r'$R_{1/2,0}$', va='top')

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list[:ncols], snaps_list[:ncols])):
        kwargs = rcm.get_name_kwargs(galaxy_name)

        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = rcm.get_constants(bin_edges, save_name_append, kwargs['mdm'])

        (inital_velocity_v, inital_velocity_z, inital_velocity_r, inital_velocity_p
         ) = rcm.smooth_burn_ins(kwargs['mdm'], snaps, save_name_append, BURN_IN, bin_edges,
                                 galaxy_name=galaxy_name)
        inital_velocity2_p = inital_velocity_p**2

        #sigma phi
        tau_vir_sigma_phi = rcm.get_tau_heat_array(delta_dm[_index], upsilon_dm[_index],
                                         ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])
        tau_0_sigma_phi = tau_vir_sigma_phi * np.log(analytic_dispersion[_index] ** 2 /
                                                     (analytic_dispersion[_index] ** 2 - inital_velocity2_p[_index]))

        #mean v phi
        tau_vir_mean_v_phi = 0.75 * tau_vir_sigma_phi
        tau_0_mean_v_phi = tau_vir_mean_v_phi * np.log(analytic_v_circ[_index] / inital_velocity_v[_index])

        #sim times
        time = np.linspace(0, T_TOT, snaps + 1)
        time = time - time[BURN_IN]
        tau = rcm.get_tau_array(time, t_c_dm[_index]) #time / t_c_dm[_index]
        # log_taus_sigma_phi = np.log10((tau + tau_0_sigma_phi)/tau_vir_sigma_phi)
        # log_taus_mean_v_phi = np.log10((tau + tau_0_mean_v_phi)/tau_vir_mean_v_phi)

        tau_edges = (rcm.get_tau_array(np.linspace(0, T_TOT, snaps + 2), t_c_dm[_index])
                     + tau_0_sigma_phi) / tau_vir_sigma_phi
        tau_centres = (rcm.get_tau_array(np.linspace(0, T_TOT, snaps + 1), t_c_dm[_index])
                     + tau_0_sigma_phi) / tau_vir_sigma_phi

        log_tau_edges = np.log10(tau_edges)
        log_tau_edges[0] = log_tau_edges[1] - 1e-8
        log_tau_centres = np.log10(tau_centres)

        snap_ins = snaps - 2
        log_taus_to_plot.append(log_tau_centres[snap_ins])

        if tau_vir_sigma_phi * t_c_dm[_index]  < min_scale_time:
            min_scale_time = tau_vir_sigma_phi * t_c_dm[_index]

    # print(log_taus_to_plot)

    # if np.any(['mu_0p5' in gn for gn in galaxy_name_list]):
    log_taus_to_plot = [0.2222638296062362, -0.2742138932482902,
                        -0.7629799805627485, -1.2341813962006694, -1.6583670618295658][:5-n_skip]

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = rcm.get_name_kwargs(galaxy_name)
        kwargs['ls'] = '-'

        if np.any(['mu_0p5' in gn for gn in galaxy_name_list]):
            mu_map = 'cividis'
            # if 'mu_0p2'  in galaxy_name: kwargs['c'] = matplotlib.cm.get_cmap(mu_map)(0.001)
            # elif 'mu_0p5'in galaxy_name: kwargs['c'] = matplotlib.cm.get_cmap(mu_map)(0.2)
            # elif 'mu_1'  in galaxy_name: kwargs['c'] = matplotlib.cm.get_cmap(mu_map)(0.4)
            # elif 'mu_2/' in galaxy_name: kwargs['c'] = matplotlib.cm.get_cmap(mu_map)(0.6)
            # elif 'mu_5'  in galaxy_name: kwargs['c'] = matplotlib.cm.get_cmap(mu_map)(0.8)
            # elif 'mu_25' in galaxy_name: kwargs['c'] = matplotlib.cm.get_cmap(mu_map)(0.999)

            if 'mu_0p2'  in galaxy_name: kwargs['c'] = matplotlib.cm.get_cmap('plasma')(0.4)
            elif 'mu_0p5'in galaxy_name: kwargs['c'] = matplotlib.cm.get_cmap('plasma')(0.17)
            elif 'mu_0p77'in galaxy_name: kwargs['c'] = matplotlib.cm.get_cmap('plasma')(0.07)
            elif 'mu_1'  in galaxy_name: kwargs['c'] = matplotlib.cm.get_cmap('viridis')(0.1)
            elif 'mu_2/' in galaxy_name: kwargs['c'] = matplotlib.cm.get_cmap('viridis')(0.27) # (0.8 / np.log10(25)) * np.log10(2) + 0.1
            elif 'mu_5'  in galaxy_name: kwargs['c'] = matplotlib.cm.get_cmap('viridis')(0.5)
            elif 'mu_25' in galaxy_name: kwargs['c'] = matplotlib.cm.get_cmap('viridis')(0.9)

        elif np.any(['mu_1_k' in gn for gn in galaxy_name_list]):
            kwargs['c'] = matplotlib.cm.get_cmap('turbo')(i / (len(galaxy_name_list) - 1 + 1e-3))

        else:
            mu_map = 'viridis'
            if 'mu_1' in galaxy_name:
                kwargs['c'] = matplotlib.cm.get_cmap(mu_map)(0.1)
            elif 'mu_5' in galaxy_name:
                kwargs['c'] = matplotlib.cm.get_cmap(mu_map)(0.5)
            elif 'mu_25' in galaxy_name:
                kwargs['c'] = matplotlib.cm.get_cmap(mu_map)(0.9)

        lwt = 5
        lw = 3
        pth = wpth_0

        if '8p0' in galaxy_name or '_8_' in galaxy_name:
            kwargs['ls'] = '-'
            lw=3
            lwt=5
        elif '7p5' in galaxy_name:
            kwargs['ls'] = (0, (6, *([3]*(3))))
            lw=3
            lwt=5
            pth=wpth_5
        elif '7p0' in galaxy_name or '_7_' in galaxy_name:
            kwargs['ls'] = (0, (2, *([1]*(3))))
            pth=wpth_5
        elif '6p5' in galaxy_name:
            kwargs['ls'] = (0, (4, *([2]*(3))))
        elif '6p0' in galaxy_name:
            kwargs['ls'] = (0, (1, *([0.5]*(3))))

        # different resolutions

        alpha = 0.2
        window = snaps // 5 + 1
        poly_n = 5

        # work out constatns
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = rcm.get_constants(bin_edges, save_name_append, kwargs['mdm'])

        (inital_velocity_v, inital_velocity_z, inital_velocity_r, inital_velocity_p
         ) = rcm.smooth_burn_ins(kwargs['mdm'], snaps, save_name_append, BURN_IN, bin_edges,
                                 galaxy_name=galaxy_name)
        inital_velocity2_p = inital_velocity_p**2

        #sigma phi
        tau_vir_sigma_phi = rcm.get_tau_heat_array(delta_dm[_index], upsilon_dm[_index],
                                         ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])
        tau_0_sigma_phi = tau_vir_sigma_phi * np.log(analytic_dispersion[_index] ** 2 /
                                                     (analytic_dispersion[_index] ** 2 - inital_velocity2_p[_index]))

        #mean v phi
        tau_vir_mean_v_phi = 0.75 * tau_vir_sigma_phi
        tau_0_mean_v_phi = tau_vir_mean_v_phi * np.log(analytic_v_circ[_index] / inital_velocity_v[_index])

        #sim times
        time = np.linspace(0, T_TOT, snaps + 1)
        time = time - time[BURN_IN]
        # log_time = np.log10(time * min_scale_time / tau_vir_sigma_phi)
        tau = rcm.get_tau_array(time, t_c_dm[_index])
        # log_taus_sigma_phi = np.log10((tau + tau_0_sigma_phi)/tau_vir_sigma_phi)
        # log_taus_mean_v_phi = np.log10((tau + tau_0_mean_v_phi)/tau_vir_mean_v_phi)

        tau_edges = (rcm.get_tau_array(np.linspace(0, T_TOT, snaps + 2), t_c_dm[_index])
                     + tau_0_sigma_phi) / tau_vir_sigma_phi
        tau_centres = (rcm.get_tau_array(np.linspace(0, T_TOT, snaps + 1), t_c_dm[_index])
                     + tau_0_sigma_phi) / tau_vir_sigma_phi

        log_time = np.log10(tau_centres * min_scale_time)

        log_tau_edges = np.log10(tau_edges)
        log_tau_edges[0] = log_tau_edges[1] - 1e-8
        log_tau_centres = np.log10(tau_centres)

        # if not do_DM:
        # load data
        j_z = rcm.get_total_angular_momentum(galaxy_name, 'diff', snaps, all=True)

        # print(galaxy_name)
        # print(j_z[:10])
        # print(np.mean(j_z[:10]))
        # print(np.log10(j_z[:10]))
        # print(np.mean(np.log10(j_z[:10])))

        y = np.log10(j_z)

        #fudge because ICs are *slightly* wrong
        # if 'mu_2/' in galaxy_name:
        #     y += 0.010
        # elif 'mu_0p77' in galaxy_name:
        #     y += 0.022
        # elif 'mu_0p5' in galaxy_name:
        #     y += 0.030
        y = y + (3.2445 - y[0]) #value for ics of mu=25

        ax2.errorbar(np.log10(tau_centres * tau_vir_sigma_phi), y,
                     c=kwargs['c'], marker='', ls='-', lw=lwt, alpha=alpha, zorder=9)
        ax2.errorbar(np.log10(tau_centres * tau_vir_sigma_phi), savgol_filter(y, window, poly_n),
                     c=kwargs['c'], marker='', ls=kwargs['ls'], lw=lw, zorder=10, path_effects=pth)

        if do_DM:
            j_z_DM = rcm.get_dm_total_angular_momentum(galaxy_name, 'diff', snaps, all=True)

            print(j_z[0], j_z_DM[0])

            y_DM = np.log10(j_z_DM)
        
            #to put on same axes
            # y_DM = y_DM + (3.2445 - y_DM[0])  # value for ics of mu=25

            ax2.errorbar(np.log10(tau_centres * tau_vir_sigma_phi), y_DM,
                         c=kwargs['c'], marker='', ls='-', lw=lwt-1, alpha=alpha, zorder=9)
            ax2.errorbar(np.log10(tau_centres * tau_vir_sigma_phi), savgol_filter(y_DM, window, poly_n),
                         c=kwargs['c'], marker='', ls=kwargs['ls'], lw=lw-1, zorder=10, path_effects=pth)

        for k, log_tau_to_plot in enumerate(log_taus_to_plot):

            # #time labels
            if i == 0 and n_skip > 1:
                ax_ins0[k].text(xlims[0] + 0.98 * (xlims[1] - xlims[0]), pdflims[0] + 0.05 * (pdflims[1] - pdflims[0]),
                                r'$t/t_c(R_{1/2, 0})=$' + f'{round(10**log_tau_to_plot * tau_vir_sigma_phi,4)}',
                                c='k', ha='right')

            # print(galaxy_name, log_tau_edges[-1], log_tau_to_plot)
            if log_tau_edges[-1] > log_tau_to_plot:
                snap = np.argmin(np.abs(log_tau_centres - log_tau_to_plot)) - 1
                # print(snap, log_tau_centres[snap], log_tau_to_plot)
                # print(galaxy_name, snap / snaps * 9.8 / t_c_dm)
                # print(k, (snap / snaps * T_TOT / t_c_dm * PC_ON_M / GYR_ON_S)[1])
                # print(k, 10**log_tau_to_plot * tau_vir_sigma_phi)

                #load data
                max_seed = 1
                if os.path.exists('../galaxies/' + galaxy_name + '_seed4'): max_seed = 5
                if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

                jz_of_r_list = []
                if do_DM:
                    jx_of_r_list_DM = []
                    jy_of_r_list_DM = []
                    jz_of_r_list_DM = []
                n_list = []

                #stack low resolution galaxies
                for ii in range(max_seed):
                # for ii in range(2, 9):
                    galaxy_name_i = galaxy_name + '_seed' + str(ii)
                    if ii == 0:
                        galaxy_name_i = galaxy_name

                    (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
                     MassStar, PosStars, VelStars, IDStars, PotStars
                     ) = load_nbody.load_and_align(galaxy_name_i, snap='{0:03d}'.format(int(snap)))

                    r = np.linalg.norm(PosStars, axis=1)
                    (R, phi, z, v_R, v_phi, v_z) = rcm.get_cylindrical(PosStars, VelStars)
                    jzs = R * v_phi

                    # j_tots = np.linalg.norm(np.cross(PosStars, VelStars), axis=1)
                    # jzs = j_tots

                    # jz_binned, _, _ = binned_statistic(R, jzs, bins=radial_bins)
                    jz_binned, _,_ = binned_statistic(r, jzs, bins=radial_bins)
                    n_binned, _,_ = binned_statistic(R, np.ones(len(R)), bins=radial_bins, statistic='sum')

                    jz_of_r_list.append(jz_binned)
                    n_list.append(n_binned)

                    if do_DM:

                        # (R_DM, phi_DM, z_DM, v_R_DM, v_phi_DM, v_z_DM) = rcm.get_cylindrical(PosDMs, VelDMs)
                        # jzs_DM = R_DM * v_phi_DM
                        # jzs_DM = np.linalg.norm(np.cross(PosDMs, VelDMs), axis=1)
                        jzs_DM = np.cross(PosDMs, VelDMs)#[:, 2]

                        # R_DM = np.sqrt(PosDMs[:, 0]**2 + PosDMs[:, 1]**2)
                        r_DM = np.linalg.norm(PosDMs, axis=1)

                        # jx_binned_DM, _,_ = binned_statistic(R_DM, jzs_DM[:, 0], bins=radial_bins)
                        # jy_binned_DM, _,_ = binned_statistic(R_DM, jzs_DM[:, 1], bins=radial_bins)
                        # jz_binned_DM, _,_ = binned_statistic(R_DM, jzs_DM[:, 2], bins=radial_bins)

                        # jx_binned_DM, _,_ = binned_statistic(r_DM, jzs_DM[:, 0], bins=radial_bins)
                        # jy_binned_DM, _,_ = binned_statistic(r_DM, jzs_DM[:, 1], bins=radial_bins)
                        jz_binned_DM, _,_ = binned_statistic(r_DM, jzs_DM[:, 2], bins=radial_bins)

                        # jx_of_r_list_DM.append(jx_binned_DM)
                        # jy_of_r_list_DM.append(jy_binned_DM)
                        jz_of_r_list_DM.append(jz_binned_DM)

                jz_of_r = np.mean(jz_of_r_list, axis=0)
                n_res = np.sum(n_list, axis=0)

                # print(jz_of_r)
                ax_ins0[k].plot(np.log10(0.5 * (radial_bins[1:] + radial_bins[:-1]) / r200)[n_res > n_threshold],
                                np.log10(jz_of_r)[n_res > n_threshold],
                                ls=kwargs['ls'], color=kwargs['c'], lw=lw, path_effects=pth)

                if do_DM:
                    jx_of_r_DM = np.mean(jx_of_r_list_DM, axis=0)
                    jy_of_r_DM = np.mean(jy_of_r_list_DM, axis=0)
                    jz_of_r_DM = np.mean(jz_of_r_list_DM, axis=0)
                    # print(galaxy_name)
                    # print(jx_of_r_DM) #/ 0.5 / (radial_bins[1:] + radial_bins[:-1]))
                    # print(jy_of_r_DM) #/ 0.5 / (radial_bins[1:] + radial_bins[:-1]))
                    # print(jz_of_r_DM) #/ 0.5 / (radial_bins[1:] + radial_bins[:-1]))

                    # ax_ins0[k].plot(np.log10(0.5 * (radial_bins[1:] + radial_bins[:-1]) / r200),
                    #                 np.log10(jx_of_r_DM),
                    #                 ls=(0,(1,1)), color=kwargs['c'], lw=2)
                    # ax_ins0[k].plot(np.log10(0.5 * (radial_bins[1:] + radial_bins[:-1]) / r200),
                    #                 np.log10(jy_of_r_DM),
                    #                 ls=(0,(1,1)), color=kwargs['c'], lw=2)
                    ax_ins0[k].plot(np.log10(0.5 * (radial_bins[1:] + radial_bins[:-1]) / r200),
                                    np.log10(jz_of_r_DM),
                                    ls='-', color=kwargs['c'], lw=2)

        #label
        #artifical legend
        if np.any(['mu_0p5' in gn for gn in galaxy_name_list]):
            if i < 6:
                dx = 0.02 + 0.10 * (i // 3)
                dy = 0.65 - 0.10 * i + 0.30 * (i // 3)
                ax2.text(xlim[0] + dx * (xlim[1] - xlim[0]), ylim[0] + dy * (ylim[1] - ylim[0]),
                         kwargs['mu'], color=kwargs['c'], ha='left', va='center')

        else:
            if i % ((len(galaxy_name_list) + 1)//3) == 0:
                dx = 0.02
                dy = 0.65 - 0.10 * (i // ((len(galaxy_name_list) + 1)//3))
                ax2.text(xlim[0] + dx * (xlim[1] - xlim[0]), ylim[0] + dy * (ylim[1] - ylim[0]),
                         kwargs['mu'], color=kwargs['c'], ha='left', va='center')
                #, bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    #artifical legend
    xs = np.array([0.02, 0.07])
    xs_ = xs[1] + 0.02
    yss = [np.array([0.3] * 2), np.array([0.2] * 2), np.array([0.1] * 2)]
    ax2.errorbar(xlim[0] + xs * (xlim[1] - xlim[0]),
                 ylim[0] + yss[0] * (ylim[1] - ylim[0]),
                 c='grey', marker='', ls='-', lw=lw)
    ax2.errorbar(xlim[0] + xs * (xlim[1] - xlim[0]),
                 ylim[0] + yss[1] * (ylim[1] - ylim[0]),
                 c='grey', marker='', ls=(0, (6, *([3]*(3)))), lw=lw)
    ax2.errorbar(xlim[0] + xs * (xlim[1] - xlim[0]),
                 ylim[0] + yss[2] * (ylim[1] - ylim[0]),
                 c='grey', marker='', ls=(0, (2, *([1]*(3)))), lw=lw)

    ax2.text(xlim[0] + xs_ * (xlim[1] - xlim[0]), ylim[0] + yss[0][0] * (ylim[1] - ylim[0]),
             r'$m_{\rm DM} = 10^{8.0} \, \mathrm{M}_\odot$', ha='left', va='center')
    ax2.text(xlim[0] + xs_ * (xlim[1] - xlim[0]), ylim[0] + yss[1][0] * (ylim[1] - ylim[0]),
             r'$m_{\rm DM} = 10^{7.5} \, \mathrm{M}_\odot$', ha='left', va='center')
    ax2.text(xlim[0] + xs_ * (xlim[1] - xlim[0]), ylim[0] + yss[2][0] * (ylim[1] - ylim[0]),
             r'$m_{\rm DM} = 10^{7.0} \, \mathrm{M}_\odot$', ha='left', va='center')

    # ax_ins2[0].set_ylabel(r'$\log( N^{-1} d N / d (v_\phi / V_{200} ) )$')
    ax_ins0[-1].set_ylabel(r'$\log \, j_z / $kpc km s$^{-1}$')
    # ax_ins0[-1].set_ylabel(r'$\log \, \overline{j}_z (R) / $kpc km s$^{-1}$')
    # ax_ins0[-1].set_ylabel(r'$\log \, \langle j_z \rangle / $kpc km s$^{-1}$')
    # ax_ins0[ncols // 2].set_xlabel(r'$\log \, R / $kpc')
    ax_ins0[ncols // 2].set_xlabel(r'$\log \, R / r_{200}$')

    ax2.set_ylabel(r'$\log \, j_{z, \rm tot} / $kpc km s$^{-1}$')
    # ax2.set_xlabel(r'$\log \, t / t_{\sigma_\phi}$')
    # ax2.set_xlabel(r'$\log \, t / $Gyr$ \, \times N_{\rm DM, lowest \, resolution} / N_{\rm DM}$')
    ax2.set_xlabel(r'$\log \, t / t_c(R_{1/2, 0})$')

    # guide lines
    # ax2.plot([-5,0], [analytic_v_circ[_index]/v200]*2, c='k', ls=':',  zorder=5)
    # ax2.plot([-5,0], [-analytic_v_circ[_index]/v200]*2, c='k', ls=':',  zorder=5)
    # ax2.plot([-5,0], [0]*2, c='k', ls='--',  zorder=5)

    # ax2.set_xlim((-3.5, -0.996))
    ax2.set_xlim(xlim)
    ax2.set_ylim(ylim)

    # cbar = fig.colorbar(image2, cax=cax2)
    # cbar.set_label(r'$\log (M / 10^{10} $M$_\odot)$')

    if save:
        plt.savefig(name, bbox_inches="tight")  # , dpi=200)
        plt.close()

    return


def spline_potential_softening(u):
    if u < 0.5:
        return 16 / 3 * u ** 2 - 48 / 5 * u ** 4 + 32 / 5 * u ** 5 - 14 / 5
    if u < 1:
        return 1 / (15 * u) + 32 / 3 * u ** 2 - 16 * u ** 3 + 48 / 5 * u ** 4 - 32 / 15 * u ** 5 - 16 / 5
    return -1 / u


def vector_spline_potential_softening(u):
    small = 16 / 3 * np.square(u) - 48 / 5 * np.power(u, 4) + 32 / 5 * np.power(u, 5) - 14 / 5
    med   = 1 / (15 * u) + 32 / 3 * np.square(u) - 16 * np.power(u, 3) + 48 / 5 * np.power(u, 4) - 32 / 15 * np.power(u, 5) - 16 / 5
    large = -1 / u

    out = np.zeros(np.shape(u))

    use_small = u < 0.5
    use_large = u > 1
    use_med = np.logical_not(np.logical_or(use_small, use_large))

    out[use_small] = small[use_small]
    out[use_med]   = med[use_med]
    out[use_large] = large[use_large]

    return out


#TODO probably faster to make a grid of params and interpolate over them
def particle_integrated_project_potential(Gm, eps, R2, extent):
    if R2 > eps * 2.8:
        return Gm * (np.arcsinh(extent[0] / np.sqrt(R2)) - np.arcsinh(extent[1] / np.sqrt(R2)))

    phi = lambda y: spline_potential_softening( np.sqrt(R2 + y**2) / eps * 0.35714285714285715)

    return Gm / eps * 0.35714285714285715 * quad(phi, extent[0], extent[1])[0]


def hernquist_integrated_project_potential(GM, a, R2, extent):
    phi = lambda y: 1 / (np.sqrt(y**2 + R2) + a)

    return -GM * quad(phi, extent[0], extent[1])[0]


def hernquist_potential(GM, a, R2, y=0):
    return -GM / (np.sqrt(y**2 + R2) + a)


def iterable_calculation(i):
    grid_x, grid_z = np.meshgrid(np.linspace(*xlims, pixels[0]), np.linspace(*zlims, pixels[1]))

    j = i % 16
    if j == 0: interp = r_on_eps_phi_interp
    elif j == 1: interp = r_on_eps_phi_interp_0
    elif j == 2: interp = r_on_eps_phi_interp_1
    elif j == 3: interp = r_on_eps_phi_interp_2
    elif j == 4: interp = r_on_eps_phi_interp_3
    elif j == 5: interp = r_on_eps_phi_interp_4
    elif j == 6: interp = r_on_eps_phi_interp_5
    elif j == 7: interp = r_on_eps_phi_interp_6
    elif j == 8: interp = r_on_eps_phi_interp_7
    elif j == 9: interp = r_on_eps_phi_interp_8
    elif j == 10: interp = r_on_eps_phi_interp_9
    elif j == 11: interp = r_on_eps_phi_interp_10
    elif j == 12: interp = r_on_eps_phi_interp_11
    elif j == 13: interp = r_on_eps_phi_interp_12
    elif j == 14: interp = r_on_eps_phi_interp_13
    elif j == 15: interp = r_on_eps_phi_interp_14

    x, y, z = PosDMs[i * 16, :]
    r = np.sqrt((x - grid_x) ** 2 + (y - y_int_extent) ** 2 + (z - grid_z) ** 2)
    out = interp(r / eps * 0.35714285714285715)
    for ii in range(i * 16 + 1, np.amin(((i + 1) * 16 , len(PosDMs)))):
        x, y, z = PosDMs[ii, :]
        r = np.sqrt((x - grid_x) ** 2 + (y - y_int_extent) ** 2 + (z - grid_z) ** 2)
        out += interp(r / eps * 0.35714285714285715)
    return out

    # if j == 0: return r_on_eps_phi_interp(r / eps * 0.35714285714285715)
    # elif j == 1: return r_on_eps_phi_interp_0(r / eps * 0.35714285714285715)
    # elif j == 2: return r_on_eps_phi_interp_1(r / eps * 0.35714285714285715)
    # elif j == 3: return r_on_eps_phi_interp_2(r / eps * 0.35714285714285715)
    # elif j == 4: return r_on_eps_phi_interp_3(r / eps * 0.35714285714285715)
    # elif j == 5: return r_on_eps_phi_interp_4(r / eps * 0.35714285714285715)
    # elif j == 6: return r_on_eps_phi_interp_5(r / eps * 0.35714285714285715)
    # elif j == 7: return r_on_eps_phi_interp_6(r / eps * 0.35714285714285715)
    # elif j == 8: return r_on_eps_phi_interp_7(r / eps * 0.35714285714285715)
    # elif j == 9: return r_on_eps_phi_interp_8(r / eps * 0.35714285714285715)
    # elif j == 10: return r_on_eps_phi_interp_9(r / eps * 0.35714285714285715)
    # elif j == 11: return r_on_eps_phi_interp_10(r / eps * 0.35714285714285715)
    # elif j == 12: return r_on_eps_phi_interp_11(r / eps * 0.35714285714285715)
    # elif j == 13: return r_on_eps_phi_interp_12(r / eps * 0.35714285714285715)
    # elif j == 14: return r_on_eps_phi_interp_13(r / eps * 0.35714285714285715)
    # elif j == 15: return r_on_eps_phi_interp_14(r / eps * 0.35714285714285715)

    # if j == 0: return np.interp(r / eps * 0.35714285714285715, r_on_eps_interp, proj_phi_interp)
    # elif j == 1: return np.interp(r / eps * 0.35714285714285715, r_on_eps_interp_0, proj_phi_interp_0)
    # elif j == 2: return np.interp(r / eps * 0.35714285714285715, r_on_eps_interp_1, proj_phi_interp_1)
    # elif j == 3: return np.interp(r / eps * 0.35714285714285715, r_on_eps_interp_2, proj_phi_interp_2)
    # elif j == 4: return np.interp(r / eps * 0.35714285714285715, r_on_eps_interp_3, proj_phi_interp_3)
    # elif j == 5: return np.interp(r / eps * 0.35714285714285715, r_on_eps_interp_4, proj_phi_interp_4)
    # elif j == 6: return np.interp(r / eps * 0.35714285714285715, r_on_eps_interp_5, proj_phi_interp_5)
    # elif j == 7: return np.interp(r / eps * 0.35714285714285715, r_on_eps_interp_6, proj_phi_interp_6)
    # elif j == 8: return np.interp(r / eps * 0.35714285714285715, r_on_eps_interp_7, proj_phi_interp_7)
    # elif j == 9: return np.interp(r / eps * 0.35714285714285715, r_on_eps_interp_8, proj_phi_interp_8)
    # elif j == 10: return np.interp(r / eps * 0.35714285714285715, r_on_eps_interp_9, proj_phi_interp_9)
    # elif j == 11: return np.interp(r / eps * 0.35714285714285715, r_on_eps_interp_10, proj_phi_interp_10)
    # elif j == 12: return np.interp(r / eps * 0.35714285714285715, r_on_eps_interp_11, proj_phi_interp_11)
    # elif j == 13: return np.interp(r / eps * 0.35714285714285715, r_on_eps_interp_12, proj_phi_interp_12)
    # elif j == 14: return np.interp(r / eps * 0.35714285714285715, r_on_eps_interp_13, proj_phi_interp_13)
    # elif j == 15: return np.interp(r / eps * 0.35714285714285715, r_on_eps_interp_14, proj_phi_interp_14)

    # proj_phis += np.interp(r / eps * 0.35714285714285715, r_on_eps_interp, proj_phi_interp)
    # return


def projected_potential_viz(name='', save=False):

    galaxy_name_list = [#'mu_5_smooth/fdisk_0p01_lgMdm_6p0_V200-200kmps',
                        #'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps',
                        'mu_5_smooth/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_5_smooth/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        # 'mu_5_smooth/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        # 'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        # 'mu_5_smooth/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        # 'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        ]
    # snaps_list = [101, 400, 101, 401, 101, 400]
    snaps_list = [101, 101, 101, 400]
    # snaps_list = [101, 101, 101, 101, 101, 400]

    # t_list = [0, 1, 1.7, 3, 5, 9] #[0,1,3,9] #Gyr
    t_list = [0,1,3,9] #Gyr

    xlims = [-25, 25]
    # zlims = [-12.5, 12.5]
    zlims = [-25, 25]
    extent = [*xlims, *zlims] #-25, 25] #kpc

    # pixels = [11, 11]
    # pixels = [51, 51]
    # pixels = [101, 101]
    pixels = [201, 201]
    # pixels = [401, 401]

    # diff_phi_lims = [-10**3.5, 10**3.5] #km^2 / s^2
    # diff_vmin, diff_vmax = diff_phi_lims
    # phi_lims = [10**4.95, 10**5.37] #km^2 / s^2
    # phi_vmin, phi_vmax = phi_lims

    diff_phi_lims = [-0.014, 0.014] #E_esc
    diff_vmin, diff_vmax = diff_phi_lims
    phi_lims = [0.4, 1] #E_esc
    phi_vmin, phi_vmax = phi_lims

    # E_esc = 10**5.661791367233461 # = 458977.4695242651 km^2 / s^2

    cmap_phi = 'inferno'
    cmap_diff = 'twilight_shifted'

    grid_x, grid_z = np.meshgrid(np.linspace(*xlims, pixels[0]), np.linspace(*zlims, pixels[1]))
    grid_x = grid_x.T
    grid_z = grid_z.T

    n_x = len(galaxy_name_list)
    n_y = len(t_list)

    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(3 * n_x, 3 * n_y + 0.05, forward=True)
    spec = fig.add_gridspec(ncols=n_x, nrows=n_y)

    fig.subplots_adjust(wspace=0.0, hspace=0.0)

    axs = []
    for row in range(n_y):
        axs.append([])
        for col in range(n_x):
            axs[row].append(fig.add_subplot(spec[row, col]))
            axs[row][col].set_xlim(xlims)
            axs[row][col].set_ylim(zlims)

            axs[row][col].set_xticks([-20, 0, 20])
            axs[row][col].set_yticks([-20, 0, 20])

            if col != 0:
                axs[row][col].set_yticklabels([])
            if row != n_y - 1:
                axs[row][col].set_xticklabels([])

            if col == 0:
                axs[row][col].set_ylabel(r'$z$ [kpc]')
            if row == n_y - 1:
                axs[row][col].set_xlabel(r'$x$ [kpc]')

            # if col == (n_x - 1):
            #     axs[row][col].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), zlims[0] + 0.05 * (zlims[1] - zlims[0]),
            #                        r'$t =$' + str(t_list[row]) + ' [Gyr]', c='white', ha='right', path_effects=pth_2)
            if col == 0:
                axs[row][col].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), zlims[0] + 0.05 * (zlims[1] - zlims[0]),
                                   r'$t =$' + str(t_list[row]) + ' [Gyr]', c='white', path_effects=pth_2)

    for col, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        print(galaxy_name)
        kwargs = rcm.get_name_kwargs(galaxy_name)
        a_h, v_max = rcm.get_hernquist_params(galaxy_name)
        r200 = rcm.get_r_200(galaxy_name)
        gmh = 4 * a_h * v_max ** 2  # kpc km^2/s^2
        Phi_0 = gmh / a_h #km^2 / s^2

        ## escape velocity or energy
        # print(2 * gmh / a_h)
        # print(np.log10(2 * gmh / a_h))

        y_int_extent = 0 #0.5 # 25 #r200 #kpc

        eps = 0.2  # kpc plumber equiv
        if 'eps' in galaxy_name:
            # eps =
            pass

        if not 'smooth' in galaxy_name:
            #make interpolator to speed things up
            n = 1_001
            r_on_eps_interp = np.logspace(-5, 5, n)  # kpc
            r_on_eps_interp[0] = 0

            proj_phi_interp = np.array([vector_spline_potential_softening(r)
                                        for r in r_on_eps_interp])

            r_on_eps_phi_interp = interp1d(r_on_eps_interp, proj_phi_interp,
                                           kind='linear', bounds_error=False, fill_value=proj_phi_interp[-1])
            r_on_eps_phi_interp_0 = interp1d(r_on_eps_interp, proj_phi_interp,
                                             kind='linear', bounds_error=False, fill_value=proj_phi_interp[-1])
            r_on_eps_phi_interp_1 = interp1d(r_on_eps_interp, proj_phi_interp,
                                             kind='linear', bounds_error=False, fill_value=proj_phi_interp[-1])
            r_on_eps_phi_interp_2 = interp1d(r_on_eps_interp, proj_phi_interp,
                                             kind='linear', bounds_error=False, fill_value=proj_phi_interp[-1])
            r_on_eps_phi_interp_3 = interp1d(r_on_eps_interp, proj_phi_interp,
                                             kind='linear', bounds_error=False, fill_value=proj_phi_interp[-1])
            r_on_eps_phi_interp_4 = interp1d(r_on_eps_interp, proj_phi_interp,
                                             kind='linear', bounds_error=False, fill_value=proj_phi_interp[-1])
            r_on_eps_phi_interp_5 = interp1d(r_on_eps_interp, proj_phi_interp,
                                             kind='linear', bounds_error=False, fill_value=proj_phi_interp[-1])
            r_on_eps_phi_interp_6 = interp1d(r_on_eps_interp, proj_phi_interp,
                                             kind='linear', bounds_error=False, fill_value=proj_phi_interp[-1])
            r_on_eps_phi_interp_7 = interp1d(r_on_eps_interp, proj_phi_interp,
                                             kind='linear', bounds_error=False, fill_value=proj_phi_interp[-1])
            r_on_eps_phi_interp_8 = interp1d(r_on_eps_interp, proj_phi_interp,
                                             kind='linear', bounds_error=False, fill_value=proj_phi_interp[-1])
            r_on_eps_phi_interp_9 = interp1d(r_on_eps_interp, proj_phi_interp,
                                             kind='linear', bounds_error=False, fill_value=proj_phi_interp[-1])
            r_on_eps_phi_interp_10 = interp1d(r_on_eps_interp, proj_phi_interp,
                                              kind='linear', bounds_error=False, fill_value=proj_phi_interp[-1])
            r_on_eps_phi_interp_11 = interp1d(r_on_eps_interp, proj_phi_interp,
                                              kind='linear', bounds_error=False, fill_value=proj_phi_interp[-1])
            r_on_eps_phi_interp_12 = interp1d(r_on_eps_interp, proj_phi_interp,
                                              kind='linear', bounds_error=False, fill_value=proj_phi_interp[-1])
            r_on_eps_phi_interp_13 = interp1d(r_on_eps_interp, proj_phi_interp,
                                              kind='linear', bounds_error=False, fill_value=proj_phi_interp[-1])
            r_on_eps_phi_interp_14 = interp1d(r_on_eps_interp, proj_phi_interp,
                                              kind='linear', bounds_error=False, fill_value=proj_phi_interp[-1])

            globals()['r_on_eps_phi_interp'] = r_on_eps_phi_interp
            globals()['r_on_eps_phi_interp_0'] = r_on_eps_phi_interp_0
            globals()['r_on_eps_phi_interp_1'] = r_on_eps_phi_interp_1
            globals()['r_on_eps_phi_interp_2'] = r_on_eps_phi_interp_2
            globals()['r_on_eps_phi_interp_3'] = r_on_eps_phi_interp_3
            globals()['r_on_eps_phi_interp_4'] = r_on_eps_phi_interp_4
            globals()['r_on_eps_phi_interp_5'] = r_on_eps_phi_interp_5
            globals()['r_on_eps_phi_interp_6'] = r_on_eps_phi_interp_6
            globals()['r_on_eps_phi_interp_7'] = r_on_eps_phi_interp_7
            globals()['r_on_eps_phi_interp_8'] = r_on_eps_phi_interp_8
            globals()['r_on_eps_phi_interp_9'] = r_on_eps_phi_interp_9
            globals()['r_on_eps_phi_interp_10'] = r_on_eps_phi_interp_10
            globals()['r_on_eps_phi_interp_11'] = r_on_eps_phi_interp_11
            globals()['r_on_eps_phi_interp_12'] = r_on_eps_phi_interp_12
            globals()['r_on_eps_phi_interp_13'] = r_on_eps_phi_interp_13
            globals()['r_on_eps_phi_interp_14'] = r_on_eps_phi_interp_14

        for row, t in enumerate(t_list):
            print(t)

            snap = int(round(t / T_TOT * snaps))

            #load data
            (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_nbody.load_and_align(galaxy_name, snap='{0:03d}'.format(int(snap)))

            proj_phis = np.zeros(pixels)

            # if 'smooth' in galaxy_name:
            for _x in range(pixels[0]):
                for _z in range(pixels[1]):
                    R2 = grid_x[_x, _z]**2 + grid_z[_x, _z]**2
                    proj_phis[_x, _z] = hernquist_potential(gmh, a_h, R2, y_int_extent)

            if 'smooth' in galaxy_name:
                axs[row][col].imshow(-proj_phis.T, extent=extent,
                                     origin='lower', cmap=cmap_phi,
                                     vmin=phi_vmin * Phi_0, vmax=phi_vmax * Phi_0,
                                     rasterized=True)

            else:
                base = proj_phis.copy()
                proj_phis = np.zeros(pixels)

                # for _i in range(len(IDDMs)):
                #     # if PosDMs[_i, 1] > -ne*y_int_extent and PosDMs[_i, 1] < ne*y_int_extent:
                #     # for _x in range(pixels[0]):
                #     #     for _z in range(pixels[1]):
                #     #         R2 = (PosDMs[_i, 0] - grid_x[_x, _z]) ** 2 + (PosDMs[_i, 2] - grid_z[_x, _z]) ** 2
                #     #         proj_phis[_x, _z] += spline_potential_softening(np.sqrt(R2 + y_int_extent**2)
                #     #                                                         / eps * 0.35714285714285715)
                #
                #     r = np.sqrt((PosDMs[_i, 0] - grid_x) ** 2 + (PosDMs[_i, 1] - y_int_extent) ** 2 +
                #                  (PosDMs[_i, 2] - grid_z) ** 2)
                #     out = np.interp(r / eps * 0.35714285714285715, r_on_eps_interp, proj_phi_interp)
                #     proj_phis += out

                # globals()['proj_phis'] = proj_phis
                globals()['PosDMs'] = PosDMs

                # globals()['r_on_eps_interp'] = r_on_eps_interp
                # globals()['proj_phi_interp'] = proj_phi_interp
                # globals()['r_on_eps_interp_0'] = r_on_eps_interp.copy()
                # globals()['proj_phi_interp_0'] = proj_phi_interp.copy()
                # globals()['r_on_eps_interp_1'] = r_on_eps_interp.copy()
                # globals()['proj_phi_interp_1'] = proj_phi_interp.copy()
                # globals()['r_on_eps_interp_2'] = r_on_eps_interp.copy()
                # globals()['proj_phi_interp_2'] = proj_phi_interp.copy()
                # globals()['r_on_eps_interp_3'] = r_on_eps_interp.copy()
                # globals()['proj_phi_interp_3'] = proj_phi_interp.copy()
                # globals()['r_on_eps_interp_4'] = r_on_eps_interp.copy()
                # globals()['proj_phi_interp_4'] = proj_phi_interp.copy()
                # globals()['r_on_eps_interp_5'] = r_on_eps_interp.copy()
                # globals()['proj_phi_interp_5'] = proj_phi_interp.copy()
                # globals()['r_on_eps_interp_6'] = r_on_eps_interp.copy()
                # globals()['proj_phi_interp_6'] = proj_phi_interp.copy()
                # globals()['r_on_eps_interp_7'] = r_on_eps_interp.copy()
                # globals()['proj_phi_interp_7'] = proj_phi_interp.copy()
                # globals()['r_on_eps_interp_8'] = r_on_eps_interp.copy()
                # globals()['proj_phi_interp_8'] = proj_phi_interp.copy()
                # globals()['r_on_eps_interp_9'] = r_on_eps_interp.copy()
                # globals()['proj_phi_interp_9'] = proj_phi_interp.copy()
                # globals()['r_on_eps_interp_10'] = r_on_eps_interp.copy()
                # globals()['proj_phi_interp_10'] = proj_phi_interp.copy()
                # globals()['r_on_eps_interp_11'] = r_on_eps_interp.copy()
                # globals()['proj_phi_interp_11'] = proj_phi_interp.copy()
                # globals()['r_on_eps_interp_12'] = r_on_eps_interp.copy()
                # globals()['proj_phi_interp_12'] = proj_phi_interp.copy()
                # globals()['r_on_eps_interp_13'] = r_on_eps_interp.copy()
                # globals()['proj_phi_interp_13'] = proj_phi_interp.copy()
                # globals()['r_on_eps_interp_14'] = r_on_eps_interp.copy()
                # globals()['proj_phi_interp_14'] = proj_phi_interp.copy()
                # globals()['r_on_eps_interp_15'] = r_on_eps_interp.copy()
                # globals()['proj_phi_interp_15'] = proj_phi_interp.copy()
                # globals()['r_on_eps_interp_16'] = r_on_eps_interp.copy()
                # globals()['proj_phi_interp_16'] = proj_phi_interp.copy()

                globals()['y_int_extent'] = y_int_extent
                globals()['xlims'] = xlims
                globals()['zlims'] = zlims
                globals()['pixels'] = pixels
                # globals()['grid_x'] = grid_x
                # globals()['grid_z'] = grid_z
                globals()['eps'] = eps

                proj_phis_0 = np.zeros(pixels)
                proj_phis_1 = np.zeros(pixels)
                proj_phis_2 = np.zeros(pixels)
                proj_phis_3 = np.zeros(pixels)
                proj_phis_4 = np.zeros(pixels)
                proj_phis_5 = np.zeros(pixels)
                proj_phis_6 = np.zeros(pixels)
                proj_phis_7 = np.zeros(pixels)
                proj_phis_8 = np.zeros(pixels)
                proj_phis_9 = np.zeros(pixels)
                proj_phis_10 = np.zeros(pixels)
                proj_phis_11 = np.zeros(pixels)
                proj_phis_12 = np.zeros(pixels)
                proj_phis_13 = np.zeros(pixels)
                proj_phis_14 = np.zeros(pixels)

                with multiprocessing.Pool(processes=7) as pool:
                    # out = pool.map(iterable_calculation, range(len(IDDMs)))  # , chunksize=1)

                    for i, out in enumerate(pool.imap_unordered(iterable_calculation, range(len(IDDMs) // 16 + 1))):
                        j = i % 16
                        if j == 0: proj_phis += out
                        elif j == 1: proj_phis_0 += out
                        elif j == 2: proj_phis_1 += out
                        elif j == 3: proj_phis_2 += out
                        elif j == 4: proj_phis_3 += out
                        elif j == 5: proj_phis_4 += out
                        elif j == 6: proj_phis_5 += out
                        elif j == 7: proj_phis_6 += out
                        elif j == 8: proj_phis_7 += out
                        elif j == 9: proj_phis_8 += out
                        elif j == 10: proj_phis_9 += out
                        elif j == 11: proj_phis_10 += out
                        elif j == 12: proj_phis_11 += out
                        elif j == 13: proj_phis_12 += out
                        elif j == 14: proj_phis_13 += out
                        elif j == 15: proj_phis_14 += out

                proj_phis += (proj_phis_0 + proj_phis_1 + proj_phis_2 + proj_phis_3 + proj_phis_4 + proj_phis_5 +
                              proj_phis_6 + proj_phis_7 + proj_phis_8 + proj_phis_9 + proj_phis_10+ proj_phis_11 +
                              proj_phis_12+ proj_phis_13+ proj_phis_14)

                proj_phis *= GRAV_CONST * MassDM / eps * 0.35714285714285715 #kpc km^2/s^2

                axs[row][col].imshow(-proj_phis.T + base.T, extent = extent,
                                     origin = 'lower', cmap = cmap_diff,
                                     vmin=diff_vmin * Phi_0, vmax=diff_vmax * Phi_0,
                                     rasterized=True)

                print(np.amax(np.abs(-proj_phis + base)), np.amax(np.log10(np.abs(-proj_phis + base))))
                # print(np.amin(proj_phis), np.amin(base))

            print(np.amin(proj_phis), np.log10(-np.amin(proj_phis))) #kpc km^2 / s^2
            print(np.amax(proj_phis), np.log10(-np.amax(proj_phis))) #kpc km^2 / s^2

            axs[row][col].scatter(PosStars[:, 0], PosStars[:, 2],
                                  c='white', s=3**2, alpha=0.8, linewidths=0, path_effects=pth_2, rasterized=True)

            if row == 0:
                axs[row][col].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), zlims[0] + 0.95 * (zlims[1] - zlims[0]),
                                   r'$N_\star$= ' + kwargs['labnstar'], c='white', va='top', path_effects=pth_2)

            if row == 1 and not 'smooth' in galaxy_name:
                axs[row][col].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), zlims[0] + 0.95 * (zlims[1] - zlims[0]),
                                   r'$N_{\mathrm{DM}}$ = ' + kwargs['labn'], c='white', va='top', path_effects=pth_2)

    axs[0][n_x // 2].set_title(r'$V_{200} = 200$ km/s, M$_{200} = 1.86 \times 10^{12}$ M$_\odot$')

    Bbox_top = axs[0][n_x - 1].get_position()
    Bbox_bot = axs[n_y - 1][n_x - 1].get_position()

    width = 0.12
    pad = 0.7
    cax = plt.axes([Bbox_top.x1, Bbox_bot.y0, (Bbox_top.x1 - Bbox_top.x0) * (pad + width), Bbox_top.y1 - Bbox_bot.y0])

    n_col_grad = 101
    x_ = np.linspace(1e-3 + 1/3, 1 - 1e-3, n_col_grad)
    phi_lims[0] += np.diff(phi_lims)[0] / 3
    x, _ = np.meshgrid(x_, np.array([0, 1]))
    cax.imshow(x.T, cmap = cmap_phi, vmin = 0, vmax = 1,
               aspect = n_y * 2 / np.diff(phi_lims)[0] / width, origin='lower', extent=[0, 1, *phi_lims])
    cax.set_xticks([])
    cax.set_xticklabels([])

    # cax.set_yticks([0.3, 0.35, 0.4, 0.45, 0.5])
    # cax.set_yticklabels(['-0.3', '-0.35', '-0.4', '-0.45', '-0.5'])
    cax.set_yticks([0.6, 0.7, 0.8, 0.9, 1])
    # cax.set_yticklabels(['-0.6', '-0.7', '-0.8', '-0.9', '-1.0'])

    cax.set_ylabel(r'$\Phi_{\mathrm{Static}}(y=0) / \Phi_0$')

    cax2 = plt.axes([Bbox_top.x1, Bbox_bot.y0, (Bbox_top.x1 - Bbox_top.x0) * (pad + 2 * width), Bbox_top.y1 - Bbox_bot.y0])

    norm = 100
    x_ = np.linspace(1e-3, 1 - 1e-3, n_col_grad)
    x, _ = np.meshgrid(x_, np.array([0, 1]))
    cax2.imshow(x.T, cmap = cmap_diff, vmin = 0, vmax = 1,
                aspect = n_y * 2 / np.diff(diff_phi_lims)[0] / norm / width, origin='lower',
                extent=[0, 1, diff_phi_lims[0] * norm, diff_phi_lims[1] * norm])
    cax2.set_xticks([])
    cax2.set_xticklabels([])

    cax2.yaxis.set_label_position('right')
    cax2.yaxis.tick_right()

    cax2.set_ylabel(r'$100 \times [\Phi_{\mathrm{Live}}(y=0) - \Phi_{\mathrm{Static}}(y=0) ] / \Phi_0$')


    # cax.set_yticks(100 - ((binbins[1:] + binbins[:-1]) / 2 - binbins[0]) / (binbins[-1] - binbins[0]) * 100)
    # cax.set_yticklabels((binbins[1:] + binbins[:-1]) / 2)


    # cax.set_ylabel(label_dict[binkey])

    # plt.figure()
    # for i in range(n):
    #     plt.errorbar(np.sqrt(R2_interp), proj_phi_interp[i, :])
    # plt.figure()
    # for i in range(n):
    #     plt.errorbar(z_interp, proj_phi_interp[:, i])

    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return


def plot_v_phi_distributions(name='', save=False):
    galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps']
    snaps_list = [400, 400, 101, 101, 101]

    bin_edges = rcm.get_bin_edges(save_name_append)

    _index = 1
    dex = 0.2

    n_skip = 2

    # v_phi_edges = np.linspace(-500, 500, 201)
    v_phi_edges = np.linspace(-500, 500, 201)
    v_phi_centres = 0.5 * (v_phi_edges[1:] + v_phi_edges[:-1])

    v200 = rcm.get_v_200()

    v_phi_edges_v200 = v_phi_edges / v200
    v_phi_centres_v200 = v_phi_centres / v200

    fig = plt.figure()  # constrained_layout=True)
    # fig.set_size_inches(12, 17, forward=True)
    fig.set_size_inches(12, 12, forward=True)
    ncols = 5 - n_skip

    # spec = fig.add_gridspec(ncols=ncols, nrows=5,  # width_ratios=[1]*(ncols*four-1),
    #                         # height_ratios=[1, 1, 0.27, 1.6, 0.5])
    #                         height_ratios=[1, 0.0001, 0.27, 1.6, 0.5])
    #                         # height_ratios=[0.0001, 1, 0.27, 1.6, 0.5])
    spec = fig.add_gridspec(ncols=ncols, nrows=4,  # width_ratios=[1]*(ncols*four-1),
                            height_ratios=[1, 0.27, 1.6, 0.5])

    # ax2 = fig.add_subplot(spec[3, :])
    ax2 = fig.add_subplot(spec[2, :])
    ax_ins0 = [fig.add_subplot(spec[0, ncols-i-1]) for i in range(ncols)]
    # ax_ins2 = [fig.add_subplot(spec[1, ncols-i-1]) for i in range(ncols)]

    # fig.subplots_adjust(hspace=0.25, wspace=0.2)
    fig.subplots_adjust(hspace=0.0, wspace=0.0)

    clevel = lambda level: 0.1 + level * 2 * (1 - 0.1 - 0.1)  #(1.3 * level + 0.3)
    contours = [0, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5]  # np.linspace(0, 0.5, n_contours+1)
    # contours = np.linspace(0, 0.5, 8)
    n_contours = len(contours) - 1

    (ln_Lambda_ks, alphas, betas, gammas
     ) = rcm.find_least_log_squares_best_fit(mcmc=False, fixed_b=0, fixed_g=0)

    # work out taus to plot
    log_taus_to_plot = []

    analytic_v_circ = rcm.get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True)
    analytic_dispersion = rcm.get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True)

    a_h, vmax = rcm.get_hernquist_params()
    analytic_v_esc = rcm.get_analytic_hernquist_escape_velocity(bin_edges, vmax, a_h, save_name_append='diff')

    bin_edges = rcm.get_bin_edges()
    bin_centre = rcm.get_bin_edges('cum')[2 * _index + 1]

    # xlims = [[0.6,  1.3],
    #          [0.35, 1.45],
    #          [-0.1, 1.6],
    #          [-0.8, 1.8],
    #          [-1.6, 2.0]]
    xlims = [[-1.6, 2.0],
             [-0.8, 1.8],
             [-0.1, 1.6],
             [0.35, 1.45],
             [0.6,  1.3],]
    pdflims = [-1.6, 0.6]

    for i in range(ncols):

        # ax_ins2[i].set_ylim((0, 1))
        # ax_ins2[i].set_xlim(xlims[i])

        ax_ins0[i].set_ylim(pdflims)
        ax_ins0[i].set_xlim(xlims[i])

        # ax_ins0[i].set_xticklabels([])
        if i != ncols - 1:
            ax_ins0[i].set_yticklabels([])
            # ax_ins2[i].set_yticklabels([])

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list[:ncols], snaps_list[:ncols])):
        kwargs = rcm.get_name_kwargs(galaxy_name)

        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = rcm.get_constants(bin_edges, save_name_append, kwargs['mdm'])

        (inital_velocity_v, inital_velocity_z, inital_velocity_r, inital_velocity_p
         ) = rcm.smooth_burn_ins(kwargs['mdm'], snaps, save_name_append, BURN_IN, bin_edges,
                                 galaxy_name=galaxy_name)
        inital_velocity2_p = inital_velocity_p**2

        #sigma phi
        tau_vir_sigma_phi = rcm.get_tau_heat_array(delta_dm[_index], upsilon_dm[_index],
                                         ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])
        tau_0_sigma_phi = tau_vir_sigma_phi * np.log(analytic_dispersion[_index] ** 2 /
                                                     (analytic_dispersion[_index] ** 2 - inital_velocity2_p[_index]))

        #mean v phi
        tau_vir_mean_v_phi = 0.75 * tau_vir_sigma_phi
        tau_0_mean_v_phi = tau_vir_mean_v_phi * np.log(analytic_v_circ[_index] / inital_velocity_v[_index])

        #sim times
        time = np.linspace(0, T_TOT, snaps + 1)
        time = time - time[BURN_IN]
        tau = rcm.get_tau_array(time, t_c_dm[_index])
        # log_taus_sigma_phi = np.log10((tau + tau_0_sigma_phi)/tau_vir_sigma_phi)
        # log_taus_mean_v_phi = np.log10((tau + tau_0_mean_v_phi)/tau_vir_mean_v_phi)

        tau_edges = (rcm.get_tau_array(np.linspace(0, T_TOT, snaps + 2), t_c_dm[_index])
                     + tau_0_sigma_phi) / tau_vir_sigma_phi
        tau_centres = (rcm.get_tau_array(np.linspace(0, T_TOT, snaps + 1), t_c_dm[_index])
                     + tau_0_sigma_phi) / tau_vir_sigma_phi

        log_tau_edges = np.log10(tau_edges)
        log_tau_edges[0] = log_tau_edges[1] - 1e-8
        log_tau_centres = np.log10(tau_centres)

        snap_ins = snaps - 2
        log_taus_to_plot.append(log_tau_centres[snap_ins])

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = rcm.get_name_kwargs(galaxy_name)
        kwargs['ls'] = '-'

        # different resolutions
        j = i // 5
        i = i % 5

        alpha = 0.2
        lwt = 5
        lw = 4
        window = snaps // 5 + 1
        poly_n = 5

        # work out constatns
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = rcm.get_constants(bin_edges, save_name_append, kwargs['mdm'])

        (inital_velocity_v, inital_velocity_z, inital_velocity_r, inital_velocity_p
         ) = rcm.smooth_burn_ins(kwargs['mdm'], snaps, save_name_append, BURN_IN, bin_edges,
                                 galaxy_name=galaxy_name)
        inital_velocity2_p = inital_velocity_p**2

        #sigma phi
        tau_vir_sigma_phi = rcm.get_tau_heat_array(delta_dm[_index], upsilon_dm[_index],
                                         ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])
        tau_0_sigma_phi = tau_vir_sigma_phi * np.log(analytic_dispersion[_index] ** 2 /
                                                     (analytic_dispersion[_index] ** 2 - inital_velocity2_p[_index]))

        #mean v phi
        tau_vir_mean_v_phi = 0.75 * tau_vir_sigma_phi
        tau_0_mean_v_phi = tau_vir_mean_v_phi * np.log(analytic_v_circ[_index] / inital_velocity_v[_index])

        #sim times
        time = np.linspace(0, T_TOT, snaps + 1)
        time = time - time[BURN_IN]
        tau = rcm.get_tau_array(time, t_c_dm[_index])
        # log_taus_sigma_phi = np.log10((tau + tau_0_sigma_phi)/tau_vir_sigma_phi)
        # log_taus_mean_v_phi = np.log10((tau + tau_0_mean_v_phi)/tau_vir_mean_v_phi)

        tau_edges = (rcm.get_tau_array(np.linspace(0, T_TOT, snaps + 2), t_c_dm[_index])
                     + tau_0_sigma_phi) / tau_vir_sigma_phi
        tau_centres = (rcm.get_tau_array(np.linspace(0, T_TOT, snaps + 1), t_c_dm[_index])
                     + tau_0_sigma_phi) / tau_vir_sigma_phi

        log_tau_edges = np.log10(tau_edges)
        log_tau_edges[0] = log_tau_edges[1] - 1e-8
        log_tau_centres = np.log10(tau_centres)

        # load data
        v_phis = rcm.get_v_phi_evolution(galaxy_name, save_name_append, snaps, bin_edges,
                                         v_phi_edges)

        # process array
        log_v_phis = np.log10(v_phis[:, _index, :] + 1e-4)

        v_phis_cumulative = np.cumsum(v_phis[:, _index, :], axis=1)
        v_phis_cumulative /= np.amax(v_phis_cumulative, axis=1)[:, np.newaxis]

        x = 0.5
        xp = 0.84
        xm = 0.16
        v_phis_medians = np.zeros(snaps + 1)
        v_phis_plus = np.zeros(snaps + 1)
        v_phis_minus = np.zeros(snaps + 1)

        for ti in range(snaps + 1):
            g = interp1d(v_phi_centres_v200, v_phis_cumulative[ti, :], kind='linear')
            try: v_phis_medians[ti] = brentq(lambda v: (g(v) - x), 0, 1.5)
            except: ValueError
            try: v_phis_plus[ti] = brentq(lambda v: (g(v) - xp), 0, 2)
            except: ValueError
            try: v_phis_minus[ti] = brentq(lambda v: (g(v) - xm), -1, 1.5)
            except: ValueError

        y = v_phis_medians
        ax2.errorbar(log_tau_centres, y,
                     c=kwargs['c'], marker='', ls='-', lw=lwt, alpha=alpha, zorder=9)
        ax2.errorbar(log_tau_centres, savgol_filter(y, window, poly_n),
                     c=kwargs['c'], marker='', ls='-', lw=lw, zorder=10, path_effects=pth_6)  # ls=kwargs['ls'])
        y = v_phis_plus
        ax2.errorbar(log_tau_centres, y,
                     c=kwargs['c'], marker='', ls='-', lw=lwt, alpha=alpha, zorder=9)
        ax2.errorbar(log_tau_centres, savgol_filter(y, window, poly_n),
                     c=kwargs['c'], marker='', ls='--', lw=lw, zorder=10, path_effects=pth_6)  # ls=kwargs['ls'])
        y = v_phis_minus
        ax2.errorbar(log_tau_centres, y,
                     c=kwargs['c'], marker='', ls='-', lw=lwt, alpha=alpha, zorder=9)
        ax2.errorbar(log_tau_centres, savgol_filter(y, window, poly_n),
                     c=kwargs['c'], marker='', ls='--', lw=lw, zorder=10, path_effects=pth_6)  # kwargs['ls'])

        # v_contours = np.zeros((2 * n_contours - 1, snaps + 1))
        #
        # for ti in range(snaps + 1):
        #     g = interp1d(v_phi_centres_v200, v_phis_cumulative[ti, :], kind='linear')
        #
        #     for k, level in enumerate(contours[1:-1]):
        #         v_contours[k, ti] = brentq(lambda v: (g(v) - level), -2, 2)
        #         v_contours[2 * n_contours - k - 2, ti] = brentq(lambda v: (g(v) - (1 - level)), -2, 2)
        #
        # for k, level in enumerate(contours[1:-1]):
        #     ax2.fill_between(log_tau_centres, v_contours[k], v_contours[2*n_contours - k -2],
        #                         color=kwargs['cmap'](clevel(level)), zorder=-5)
        #

            # ax2.errorbar(log_tau_centres, v_contours[k],
            #              c=kwargs['cmap'](clevel(level)), zorder=-1)
            # ax2.errorbar(log_tau_centres, v_contours[2*n_contours - k -2],
            #              c=kwargs['cmap'](clevel(level)), zorder=-1)

        # # main colormesh j_z/j_c plot
        # image2 = ax2.pcolormesh(log_tau_edges, v_phi_edges_v200, log_v_phis.T,
        #                         vmin=-4, vmax=-1, edgecolors='face', cmap=kwargs['cmap'], zorder=-5,
        #                         alpha=0.2)  # 'grey_r')

        snap_ins = snaps - 1

        # main plot theory lines
        if j == 0 and i < ncols:
            print(galaxy_name)

            ax_ins0[i].text(xlims[i][0] + 0.05 * (xlims[i][1] - xlims[i][0]), pdflims[0] + 0.95 * (pdflims[1] - pdflims[0]),
                                        r'$\log \, t/t_{\sigma_\phi} =$' + str(round(log_tau_centres[snap_ins], 1)),
                                        c=kwargs['c'], va='top')

            (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
             ) = rcm.get_physical_from_model(galaxy_name, snaps, save_name_append)
            theory_best_p = np.sqrt(theory_best_p2)

            if i==0:
                hundred = 20
                ax2.errorbar(log_tau_centres[hundred:], theory_best_v[_index, hundred:] / v200,
                             c='k', ls='-', lw=2, zorder=25, path_effects=wpth_3)
                ax2.errorbar(log_tau_centres[hundred:],
                             (theory_best_v[_index, hundred:] + theory_best_p[_index, hundred:]) / v200,
                             c='k', ls='--', lw=2, zorder=25, path_effects=wpth_3)
                ax2.errorbar(log_tau_centres[hundred:],
                             (theory_best_v[_index, hundred:] - theory_best_p[_index, hundred:]) / v200,
                             c='k', ls='--', lw=2, zorder=25, path_effects=wpth_3)
            if i==2:
                ax2.errorbar(log_tau_centres, theory_best_v[_index, :] / v200,
                             c='k', ls='-', lw=2, zorder=25, path_effects=wpth_3)
                ax2.errorbar(log_tau_centres, (theory_best_v[_index, :] + theory_best_p[_index, :]) / v200,
                             c='k', ls='--', lw=2, zorder=25, path_effects=wpth_3)
                ax2.errorbar(log_tau_centres, (theory_best_v[_index, :] - theory_best_p[_index, :]) / v200,
                             c='k', ls='--', lw=2, zorder=25, path_effects=wpth_3)

            ax_ins0[i].errorbar(v_phi_edges_v200, np.log10(scipy.stats.norm.pdf(
                                                v_phi_edges_v200, theory_best_v[_index, snap_ins] / v200,
                                                                  theory_best_p[_index, snap_ins] / v200) + 1e-5),
                                            c='k', ls='--', zorder=15,
                                            path_effects=wpth_3)

            # ax_ins2[i].errorbar(v_phi_edges_v200, scipy.stats.norm.cdf(
            #                                     v_phi_edges_v200, theory_best_v[_index, snap_ins] / v200,
            #                                                       theory_best_p[_index, snap_ins] / v200),
            #                                 c='k', ls='--', zorder=5)

            hl = 0.1
            ax2.arrow(np.log10(tau_edges[snap_ins]), -1.5, 0, 2 * hl,
                      fc=kwargs['c'], ec='k', head_width=hl / 2, head_length=hl, zorder=2)

        # scale = 1e-3
        scale = 1
        combine_vphi_bins = kwargs['vphibins']
        # v_phi_combined = 0.5 * (v_phi_edges_v200[1::combine_vphi_bins] + v_phi_edges_v200[:-1:combine_vphi_bins])
        v_phi_combined = v_phi_edges_v200[::combine_vphi_bins]
        d_v_phi = (v_phi_combined[1] - v_phi_combined[0]) * scale

        for k, log_tau_to_plot in enumerate(log_taus_to_plot):
            if log_tau_edges[-1] > log_tau_to_plot:
                snap = np.argmin(np.abs(log_tau_centres - log_tau_to_plot)) - 1
                print(snap, log_tau_centres[snap], log_tau_to_plot)

                # v_phi_data = np.sum(
                #     [v_phis[snap - 1, _index, i::combine_vphi_bins] for i in range(combine_vphi_bins)],
                #     axis=0)
                # total_mass = np.sum(v_phi_data)
                # v_phi_data *= 1 / total_mass * 1 / d_v_phi
                #
                # ydobuled = np.concatenate(([0], np.repeat(v_phi_data, 2), [0]))
                # ax_ins0[ncols - k - 1].plot(np.repeat(v_phi_combined, 2), np.log10(ydobuled + 1e-5),
                #                             c=kwargs['c'], ls='-', lw=3)

                #load data
                max_seed = 1
                if os.path.exists('../galaxies/' + galaxy_name + '_seed4'): max_seed = 5
                if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

                n_points_list = []
                binned_phi_list = []

                #stack low resolution galaxies
                for ii in range(max_seed):
                    galaxy_name_i = galaxy_name + '_seed' + str(ii)
                    if ii == 0:
                        galaxy_name_i = galaxy_name

                    (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
                     MassStar, PosStars, VelStars, IDStars, PotStars
                     ) = load_nbody.load_and_align(galaxy_name, snap='{0:03d}'.format(int(snap)))

                    (R, phi, z, v_R, v_phi, v_z) = rcm.get_cylindrical(PosStars, VelStars)

                    experiment_bin_edges = rcm.get_dex_bins([bin_centre], dex)
                    mask = np.logical_and(R > experiment_bin_edges[0], R < experiment_bin_edges[1])

                    n_points = np.sum(mask)
                    binned_phi = v_phi[mask]

                    n_points_list.append(n_points)
                    binned_phi_list.append(binned_phi)

                n_points = np.sum(n_points_list)
                lin = np.linspace(-2,2, 1001)
                # vbins = np.linspace(-1.5,1.5, 51)
                # vbins = np.linspace(*xlims[k], 41)
                vbins = np.linspace(*xlims[k], 16)

                binned_phi = np.hstack(binned_phi_list)
                binned_phi = np.sort(binned_phi) / v200

                #put lines on graph
                #pdf
                # ax_ins0[ncols - k - 1].hist(binned_phi, vbins, density=True, histtype='step',
                #                ls=kwargs['ls'], color=kwargs['c'], lw=2, path_effects=pth_4)

                out, _ = np.histogram(binned_phi, bins=vbins, density=True)
                ys = out # / len(binned_phi) / np.diff(vbins)[0])

                ydobuled = np.concatenate(([0], np.repeat(ys, 2), [0]))

                ax_ins0[k].plot(np.repeat(vbins, 2), np.log10(ydobuled + 1e-5),
                                            ls=kwargs['ls'], color=kwargs['c'], lw=2, path_effects=pth_4)

                ax_ins0[k].errorbar(lin, np.log10(scipy.stats.norm.pdf(
                                                lin, np.mean(binned_phi), np.std(binned_phi))),
                                                ls='-.', color=kwargs['c'], lw=3, zorder=-10)

                # #cdf
                # ax_ins2[k].errorbar(binned_phi, np.linspace(0, 1, len(binned_phi)),
                #                                 ls=kwargs['ls'], c=kwargs['c'], lw=2, path_effects=pth_4)

                # #assume Gaussian
                # ax_ins2[k].errorbar(lin, scipy.stats.norm.cdf(
                #                                 lin, np.mean(binned_phi), np.std(binned_phi)),
                #                                 ls='-.', color=kwargs['c'], lw=3, zorder=-10)
                # # print(np.mean(binned_phi) + 3 * np.std(binned_phi), np.mean(binned_phi) - 3 * np.std(binned_phi))

                np.random.seed(3*i + 5*k)
                #TODO K-S test
                # binned_phi = np.random.normal(np.mean(binned_phi), np.std(binned_phi), len(binned_phi))

                out = scipy.stats.kstest((binned_phi - np.mean(binned_phi)) / np.std(binned_phi) , scipy.stats.norm.cdf)
                print(out)
                # out = scipy.stats.shapiro(binned_phi)
                # print(out)
                # out = scipy.stats.anderson(binned_phi)
                # print(out)
                # out = scipy.stats.kurtosistest(binned_phi)
                # print(out)
                # out = scipy.stats.jarque_bera(binned_phi)
                # print(out)
                emd = scipy.stats.wasserstein_distance(binned_phi,
                            np.random.normal(np.mean(binned_phi), np.std(binned_phi), 1_000_000))
                print("Earth mover's", emd)
                print("Normed Earth mover's", emd / np.std(binned_phi))

                # skew = scipy.stats.skew(binned_phi)
                # kurt = scipy.stats.kurtosis(binned_phi)
                # print(skew)
                # print(kurt)

                print()

                # ax_ins2[k].text(xlims[k][0] + 0.03 * (xlims[k][1] - xlims[k][0]), 0.97 - 0.14 * (k - i),
                #                 r'K-S $p$-value = ' + f'$10^{{{int(round(np.log10(out.pvalue), 0))}}}$' +
                #                 '\nEMD = ' + str(round(emd, 3)),
                #                 c=kwargs['c'], va='top', fontsize=14)

                # if j != 2:
                ax_ins0[k].axvline(np.median(binned_phi), 0, 0.82,
                                    ls=':', color=kwargs['c'], lw=1)
                ax_ins0[k].axvline(np.quantile(binned_phi, 0.16), 0, 0.82,
                                    ls=':', color=kwargs['c'], lw=1)
                ax_ins0[k].axvline(np.quantile(binned_phi, 0.84), 0, 1,
                                    ls=':', color=kwargs['c'], lw=1)
                # ax_ins2[k].axvline(np.median(binned_phi), 0, 1 - 0.16 * (k + 1),
                #                     ls=':', color=kwargs['c'], lw=1)
                # ax_ins2[k].axvline(np.quantile(binned_phi, 0.16), 0, 1 - 0.16 * (k + 1),
                #                     ls=':', color=kwargs['c'], lw=1)
                # ax_ins2[k].axvline(np.quantile(binned_phi, 0.84), 0, 1,
                #                     ls=':', color=kwargs['c'], lw=1)

        xlim = [-2.1, 0.26]
        ylim = [-1.5, 1.5]
        if kwargs['c'] in ['C0', 'C1', 'C2']:
            dx = 0.40
        else:
            dx = 0.63
        if kwargs['c'] in ['C0']:
            dy = 0.32
        elif kwargs['c'] in ['C1', 'C3']:
            dy = 0.22
        else:
            dy = 0.12

        ax2.text(xlim[0] + dx * (xlim[1] - xlim[0]), ylim[0] + dy * (ylim[1] - ylim[0]),
                 r'$\mathrm{N}_{\mathrm{DM}} = $' + kwargs['labn'], ha='left', va='bottom', color=kwargs['c'])

    ax2.errorbar(xlim[0] + np.array([0.02, 0.08]) * (xlim[1] - xlim[0]),
                 ylim[0] + np.array([0.45] * 2) * (ylim[1] - ylim[0]),
                 c='grey', marker='', ls='-', lw=lw, path_effects=pth_6)
    ax2.errorbar(xlim[0] + np.array([0.02, 0.08]) * (xlim[1] - xlim[0]),
                 ylim[0] + np.array([0.35] * 2) * (ylim[1] - ylim[0]),
                 c='grey', marker='', ls='--', lw=lw, path_effects=pth_6)
    ax2.errorbar(xlim[0] + np.array([0.02, 0.08]) * (xlim[1] - xlim[0]),
                 ylim[0] + np.array([0.25] * 2) * (ylim[1] - ylim[0]),
                 c='k', marker='', ls='-', lw=2)
    ax2.errorbar(xlim[0] + np.array([0.02, 0.08]) * (xlim[1] - xlim[0]),
                 ylim[0] + np.array([0.15] * 2) * (ylim[1] - ylim[0]),
                 c='k', marker='', ls='--', lw=2)

    ax2.text(xlim[0] + 0.1 * (xlim[1] - xlim[0]), ylim[0] + 0.45 * (ylim[1] - ylim[0]),
             'median', ha='left', va='center')
    ax2.text(xlim[0] + 0.1 * (xlim[1] - xlim[0]), ylim[0] + 0.35 * (ylim[1] - ylim[0]),
             r'16 & 84th percentiles', ha='left', va='center')
    ax2.text(xlim[0] + 0.1 * (xlim[1] - xlim[0]), ylim[0] + 0.25 * (ylim[1] - ylim[0]),
             r'model $\overline{v}_\phi$', ha='left', va='center')
    ax2.text(xlim[0] + 0.1 * (xlim[1] - xlim[0]), ylim[0] + 0.15 * (ylim[1] - ylim[0]),
             r'mean $\overline{v}_\phi \pm \sigma_\phi$ ', ha='left', va='center')

    # ax_ins2[0].set_ylabel(r'$\log( N^{-1} d N / d (v_\phi / V_{200} ) )$')
    ax_ins0[-1].set_ylabel(r'$\log({\rm PDF})$')
    # ax_ins2[-1].set_ylabel(r'CDF')
    # ax_ins2[ncols // 2].set_xlabel(r'$v_\phi / V_{200}$')
    ax_ins0[ncols // 2].set_xlabel(r'$v_\phi / V_{200}$')

    ax2.set_ylabel(r'$v_\phi / V_{200}$')
    ax2.set_xlabel(r'$\log \, t / t_{\sigma_\phi}$')

    # guide lines
    # ax2.plot([-5,0], [analytic_v_circ[_index]/v200]*2, c='k', ls=':',  zorder=5)
    # ax2.plot([-5,0], [-analytic_v_circ[_index]/v200]*2, c='k', ls=':',  zorder=5)
    # ax2.plot([-5,0], [0]*2, c='k', ls='--',  zorder=5)

    # ax2.set_xlim((-3.5, -0.996))
    ax2.set_xlim(xlim)
    ax2.set_ylim(ylim)

    if _index == 1:
        ax2.text(xlim[0] + 0.97 * (xlim[1] - xlim[0]), -1.4, r'$R = R_{1/2, 0}$', c='k', va='bottom', ha='right')

    # cbar = fig.colorbar(image2, cax=cax2)
    # cbar.set_label(r'$\log (M / 10^{10} $M$_\odot)$')

    if save:
        plt.savefig(name, bbox_inches='tight')  # , dpi=200)
        plt.close()

    return


def plot_explicit_asymm_time(name='', slog_yax=False, save=True, label=True):

    # galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',]
    galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',]
    snaps_list = [400, 101]

    # 0 mean v phi
    # 2 Vc
    # 1 sigma R d log Sigma / d log R
    # 3 R d sigma R / d R
    # 4 R d mean v R v z / d z
    # 5 mean v R v z d log nu / d z

    n_panels = 6
    fig = plt.figure()
    fig.set_size_inches(10, 5 * n_panels, forward=True)

    _index = 1

    print('\nbeta=0, gamma=0 (fiducial)')
    (ln_Lambda_ks, alphas, betas, gammas
     ) = rcm.find_least_log_squares_best_fit('diff', mcmc=False, fixed_b=0, fixed_g=0)

    axs = [plt.subplot(n_panels, 1, i + 1) for i in range(n_panels)]

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    smol = 1e-4
    xlim = [0, T_TOT]
    vlim = [0.2 + smol, 1.0 - smol] #0.8
    vlim2 = [-0.5 + smol, 0.3 - smol]
    vlim3 = [-0.7 + smol, 0.1 - smol]

    vlim_thin = [0.6 + smol/2, 1.0 - smol/2] #0.4
    vlim2_thin = [-0.25 + smol/2, 0.15 - smol/2]
    vlim3_thin = [-0.25 + smol/2, 0.15 - smol/2]
    vlim2_thin_thin = [-0.10 + smol/4, 0.10 - smol/4]

    for i, ax in enumerate(axs):

        if i != n_panels - 1:
            ax.set_xticklabels([])

        ax.set_xlim(xlim)

    axs[0].set_ylim(vlim)
    axs[1].set_ylim(vlim_thin)
    axs[2].set_ylim(vlim3)
    axs[3].set_ylim(vlim3_thin)
    axs[4].set_ylim(vlim2_thin)
    axs[5].set_ylim(vlim2_thin_thin)

    if label:
        axs[0].set_ylabel(r'$\overline{v}_\phi^2 / V_{200}^2$')
        axs[1].set_ylabel(r'$V_c^2 / V_{200}^2$')
        axs[2].set_ylabel(r'$\sigma_R^2 \, \frac{\partial \ln \nu}{\partial \ln R} / V_{200}^2$')
        axs[3].set_ylabel(r'$R \, \frac{\partial \sigma_R^2}{\partial R} / V_{200}^2$')
        axs[4].set_ylabel(r'$R \, \frac{\partial \overline{v_R v_z}}{\partial z} / V_{200}^2$')
        axs[5].set_ylabel(r'$R \, \overline{v_R v_z} \, \frac{\partial \ln \nu}{\partial z} / V_{200}^2$')

        axs[1].text(xlim[0] + 0.98 * (xlim[1] - xlim[0]), vlim_thin[0] + 0.05 * (vlim_thin[1] - vlim_thin[0]),
                    r'$R = R_{1/2, 0}$', ha='right')

    else:
        for i in n_panels:
            axs[i].set_yticklabels([])

    axs[-1].set_xlabel(r'$t$ [Gyr]')

    axs0 = axs[0].twinx()
    axs0.set_yticklabels([])
    axs0.set_yticks(axs[0].get_yticks())
    axs0.set_ylim(axs[0].get_ylim())

    axs1 = axs[1].twinx()
    axs1.set_yticklabels([])
    axs1.set_yticks(axs[1].get_yticks())
    axs1.set_ylim(axs[1].get_ylim())

    for ii, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = rcm.get_name_kwargs(galaxy_name)

        if '8p0' in galaxy_name: lw = 3
        elif '7p5' in galaxy_name: lw = 3
        elif '7p0' in galaxy_name: lw = 7
        elif '6p5' in galaxy_name: lw = 7

        time = np.linspace(0, T_TOT, snaps + 1)

        V200 = rcm.get_v_200(galaxy_name)

        bin_edges = rcm.get_bin_edges(galaxy_name=galaxy_name)
        bin_centres = rcm.get_bin_edges(save_name_append='cum', galaxy_name=galaxy_name)

        # load analytic values
        analytic_dispersion = rcm.get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                                 save_name_append=save_name_append, galaxy_name=galaxy_name)
        analytic_v_circ = rcm.get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)

        # load data
        (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
         ) = rcm.get_dispersions(galaxy_name, save_name_append, snaps, bin_edges=bin_edges)

        # theory
        (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
         ) = rcm.get_physical_from_model(galaxy_name, snaps, save_name_append)

        # rad_bin_edges = np.logspace(-1, 3, 101)
        rad_bin_edges = np.logspace(np.log10(bin_centres[2 * _index + 1]) - 0.1,
                                    np.log10(bin_centres[2 * _index + 1]) + 0.1, 3)
        rad_bin_centres = 10 ** ((np.log10(rad_bin_edges[:-1]) + np.log10(rad_bin_edges[1:])) / 2)
        rad_bin_edges = np.roll(np.repeat(rad_bin_edges, 2), -1)[:-2]

        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = rcm.get_constants(rad_bin_edges, save_name_append, kwargs['mdm'], galaxy_name=galaxy_name)

        # burn ins
        (inital_velocity_v, inital_velocity2_z, inital_velocity2_r, inital_velocity2_p
         ) = rcm.smooth_zero_snap(kwargs['mdm'], snaps, save_name_append, rad_bin_edges)

        inital_velocity2_r **= 2

        theory_rad_r2 = np.zeros((len(rad_bin_centres), snaps + 1))

        for i in range(len(rad_bin_centres)):
            for j in range(snaps + 1):
                theory_rad_r2[i, j] = rcm.get_theory_velocity2_array(V200 ** 2, inital_velocity2_r[i],
                                                                 time[j], t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                                 ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])

        theory_d_sigma_R2_d_R = np.array(
            [np.diff(theory_rad_r2[:, j]) / np.diff(rad_bin_centres) for j in range(snaps + 1)]).flatten()

        d_sigma_R2_d_R = rcm.get_d_sigma_R2_dR(galaxy_name, save_name_append, snaps, bin_edges)
        d_ln_Sigma_d_ln_R = rcm.get_d_ln_Sigma_d_ln_R(galaxy_name, save_name_append, snaps, bin_edges)

        (z_integrated_vc2, midplane_vc2, mass_vc2
         ) = rcm.get_all_vc2s(galaxy_name, save_name_append, snaps, bin_edges)

        (velocity_ellipsoid_term1, velocity_ellipsoid_term2
         ) = rcm.get_velocity_ellipsoid_terms(galaxy_name, save_name_append, snaps, bin_edges)

        R_disk = 4.15

        ####
        asym_full_theory = np.sqrt(analytic_v_circ[_index] ** 2
                                   - theory_best_p2[_index, :]
                                   + theory_best_r2[_index, :]
                                   - theory_best_r2[_index, :] * bin_centres[2 * _index + 1] / R_disk
                                   + bin_centres[2 * _index + 1] * theory_d_sigma_R2_d_R
                                   + theory_best_r2[_index, :] - theory_best_z2[_index, :]
                                   + 0)

        asym_full_measure = np.sqrt(z_integrated_vc2[:, _index]
                                    # midplane_vc2[:, _index]
                                    - sigma_phi[:, _index] ** 2
                                    + sigma_R[:, _index] ** 2
                                    + sigma_R[:, _index] ** 2 * d_ln_Sigma_d_ln_R[:, _index]
                                    + bin_centres[2 * _index + 1] * d_sigma_R2_d_R[:, _index]
                                    + velocity_ellipsoid_term1[:, _index]
                                    + velocity_ellipsoid_term2[:, _index])

        axs[0].errorbar(time, asym_full_measure**2 / V200**2,
                        color='C0', linestyle='-', linewidth=lw)
        axs[0].errorbar(time, asym_full_theory**2 / V200**2,
                        color='C1', linestyle='--', linewidth=lw)
        axs[0].errorbar(time, mean_v_phi[:, _index]**2 / V200**2,
                        color='C2', linestyle='-.', linewidth=lw)
        axs[0].errorbar(time, theory_best_v[_index, :]**2 / V200**2,
                        color='C3', linestyle=':', linewidth=lw)

        # axs[0].legend(['Measured $\overline{v}_\phi$', 'All terms modelled', 'All terms measured', 'Equation XXX'])
        axs1.legend([(lines.Line2D([0, 1], [0, 1], color='C0', linewidth=3, ls='-')),
                     (lines.Line2D([0, 1], [0, 1], color='C1', linewidth=3, ls='--')),
                     # (lines.Line2D([0, 1], [0, 1], color='C2', linewidth=3, ls='-.')),
                     # (lines.Line2D([0, 1], [0, 1], color='C3', linewidth=3, ls=':'))
                     ],
                    ['Asym. drift \neqn. (A.7) \nmeasurements',
                     'Asym. drift \neqn. (A.7) \nfrom models',
                     # 'Measured $\overline{v}_\phi$', 'Empirical \nheating model \n(eqn. 2.24)',
                     ],
                    loc='lower left', #ncol=2, #columnspacing=1,
                    frameon=False,
                    bbox_to_anchor=(-0.01, 1-0.04))
        axs0.legend([#(lines.Line2D([0, 1], [0, 1], color='C0', linewidth=3, ls='-')),
                     #(lines.Line2D([0, 1], [0, 1], color='C1', linewidth=3, ls='--')),
                     (lines.Line2D([0, 1], [0, 1], color='C2', linewidth=3, ls='-.')),
                     (lines.Line2D([0, 1], [0, 1], color='C3', linewidth=3, ls=':'))],
                    [#'Asym. drift \neqn. (2.22) \nmeasurements',
                     #'Asym. drift \neqn. (2.22) \nfrom models',
                     'Measured $\overline{v}_\phi$', 'Heating model (eqn. 2.10)',],
                     # 'Measured $\overline{v}_\phi$', 'Empirical \nheating model'],# \n(eqn. 2.24)',],
                    loc='lower left', #ncol=2, #columnspacing=1,
                    frameon=False,
                    bbox_to_anchor=(0.3, -0.04))

        ####
        axs[1].errorbar(time, z_integrated_vc2[:, _index] / V200**2,
                        color='C0', linestyle='-', linewidth=lw)
        axs[1].errorbar(time, analytic_v_circ[_index]**2 * np.ones(len(time)) / V200**2,
                        color='C1', linestyle='--', linewidth=lw)
        axs[1].errorbar(time, midplane_vc2[:, _index] / V200**2,
                        color='C2', linestyle='-.', linewidth=lw)

        axs[1].legend([r'$\overline{V}_c$ for $R \approx R_{1/2,0}$ stars', r'$V_c$ from ICs',
                       r'Midplane $V_c$'], frameon=False)  # , r'$GM/r$', r'$GM/r$ from ICs'])
        # axs[1].legend([r'$\int dz \xi V_c^2 / \int dz \xi$', r'$V_c$ from ICs',
        #                r'Midplane $V_c$'])  # , r'$GM/r$', r'$GM/r$ from ICs'])

        # axs[1].errorbar(time, np.sqrt(mass_vc2[:, _index]) / V200,
        #                 color='C3', linestyle=':', linewidth=3)
        #
        # (a_h, vmax) = get_hernquist_params(galaxy_name)
        # v_halo = get_analytic_hernquist_circular_velocity(bin_edges, vmax, a_h, save_name_append)
        # (M_disk, r_disk) = get_exponential_disk_params(galaxy_name)
        # r_h = 10**((np.log10(bin_edges[0::2]) + np.log10(bin_edges[1::2]))/2)
        # v_disk = np.sqrt(GRAV_CONST * M_disk * (1 - np.exp(-r_h / r_disk) * (1 + r_h / r_disk)) / r_h)
        # spherical_vc = np.sqrt(v_halo**2 + v_disk**2)
        #
        # axs[1].errorbar(time, spherical_vc[_index] * np.ones(len(time)) / V200,
        #                 color='C4', linestyle=(1,(0.8, 0.8)), linewidth=3)

        ####
        axs[2].errorbar(time, sigma_R[:, _index] ** 2 * d_ln_Sigma_d_ln_R[:, _index] / V200**2,
                        color='C0', linestyle='-', linewidth=lw)
        axs[2].errorbar(time, - theory_best_r2[_index, :] * bin_centres[2 * _index + 1] / R_disk / V200**2,
                        color='C1', linestyle='--', linewidth=lw)
        axs[2].errorbar(time, theory_best_r2[_index, :] * d_ln_Sigma_d_ln_R[:, _index] / V200**2,
                        color='C2', linestyle='-.', linewidth=lw)

        # axs[5].errorbar(time, -np.sqrt(sigma_R[:, _index]**2) / V200,
        #                 color='C2', linestyle='-.', linewidth=3)
        # axs[5].errorbar(time, -np.sqrt(-sigma_R[0, _index]**2 * d_ln_Sigma_d_ln_R[:, _index]) / V200,
        #                 color='C3', linestyle=':', linewidth=3)

        axs[2].legend([r'Measured $\sigma_R$ & $\nu$', r'Modelled $\sigma_R$ & $\nu$' + '\n' + '(eqn. 2.6 & 2.1)',
                       r'Model $\sigma_R$ (eqn. 2.6),' + '\n' + r'measured $\nu$'],
                      loc='lower left', frameon=False)

        ####
        axs[3].errorbar(time, bin_centres[2 * _index + 1] * d_sigma_R2_d_R[:, _index] / V200**2,
                        color='C0', linestyle='-', linewidth=lw)
        axs[3].errorbar(time, bin_centres[2 * _index + 1] * theory_d_sigma_R2_d_R / V200**2,
                        color='C1', linestyle='--', linewidth=lw)

        axs[3].legend([r'Measurements', 'Modelled; $\sigma_R$ from eqn. (2.6)'], frameon=False)

        ####
        axs[4].errorbar(time, velocity_ellipsoid_term1[:, _index] / V200**2,
                        color='C0', linestyle='-', linewidth=lw)
        axs[4].errorbar(time, (theory_best_r2[_index, :] - theory_best_z2[_index, :]) / V200**2,
                        color='C1', linestyle='--', linewidth=lw)
        axs[4].errorbar(time, np.zeros(len(time)) / V200**2,
                        color='C2', linestyle='-.', linewidth=lw)
        axs[4].errorbar(time, (sigma_R[:, _index] ** 2 - sigma_z[:, _index] ** 2) / V200**2,
                        color='C3', linestyle=':', linewidth=lw)

        axs[4].legend([r'Measurements',
                       'Spherically aligned \nvelocity ellipsoid \n' + r'($\sigma_i$ from eqn. 2.6)',
                       'Cylindrically aligned \nvelocity ellipsoid',
                       'Spherically aligned \nvelocity ellipsoid \n' + r'(measured $\sigma_i$)'],
                      ncol=2, frameon=False)

        ####
        axs[5].errorbar(time, velocity_ellipsoid_term2[:, _index] / V200**2,
                        color='C0', linestyle='-', linewidth=lw)
        axs[5].errorbar(time, np.zeros(len(time)) / V200**2,
                        color='C1', linestyle='--', linewidth=lw)

        axs[5].legend([r'Measurements', r'Symmetric about $z=0$' + '\n' + r'model $(\partial \nu / \partial z = 0)$'],
                      frameon=False)

    if slog_yax:
        for ax in axs:
            ax.semilogy()
            ax.set_ylim([0.008, vlim[1]])

    if save:
        plt.savefig(name, bbox_inches='tight', pad_inches=0)
        plt.close()

    return


def plot_explicit_asymm_rad(t=5, name='',
                            slog_yax=False, save=True, legend=False, label=True):

    # galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',]
    galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps'
                        ]
    snaps_list = [400, 101]

    # 0 mean v phi
    # 2 Vc
    # 1 sigma R d log Sigma / d log R
    # 3 R d sigma R / d R
    # 4 R d mean v R v z / d z
    # 5 mean v R v z d log nu / d z

    n_panels = 6
    fig = plt.figure()
    fig.set_size_inches(5, 5 * n_panels, forward=True)

    _index = 1

    print('\nbeta=0, gamma=0 (fiducial)')
    (ln_Lambda_ks, alphas, betas, gammas
     ) = rcm.find_least_log_squares_best_fit('diff', mcmc=False, fixed_b=0, fixed_g=0)

    axs = [plt.subplot(n_panels, 1, i + 1) for i in range(n_panels)]

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    smol = 1e-4
    xlim = [-2.8, 0.2]
    vlim = [-0.05 + smol, 1.55 - smol] #1.6

    vlim2 = [-0.5 + smol, 0.3 - smol] #0.8
    vlim3 = [-0.7 + smol, 0.1 - smol]

    vlim_thin = [0.6 + smol/2, 1.0 - smol/2] #0.4
    vlim2_thin = [-0.25 + smol/2, 0.15 - smol/2]
    vlim3_thin = [-0.25 + smol/2, 0.15 - smol/2]

    vlim2_thin_thin = [-0.10 + smol/4, 0.10 - smol/4] #0.2

    old_vlim = [0.2 + smol, 1.0 - smol]
    old_vlim_thin = [0.6 + smol/2, 1.0 - smol/2] #0.4

    for i, ax in enumerate(axs):

        if i != n_panels - 1:
            ax.set_xticklabels([])

        ax.set_xlim(xlim)

    axs[0].set_ylim(vlim)
    axs[1].set_ylim(vlim)
    axs[2].set_ylim(vlim3)
    axs[3].set_ylim(vlim3_thin)
    axs[4].set_ylim(vlim2_thin)
    axs[5].set_ylim(vlim2_thin_thin)

    if label:
        axs[0].set_ylabel(r'$\overline{v}_\phi^2 / V_{200}^2$')
        axs[1].set_ylabel(r'$V_c^2 / V_{200}^2$')
        axs[2].set_ylabel(r'$\sigma_R^2 \, \frac{\partial \ln \nu}{\partial \ln R} / V_{200}^2$')
        axs[3].set_ylabel(r'$R \, \frac{\partial \sigma_R^2}{\partial R} / V_{200}^2$')
        axs[4].set_ylabel(r'$R \, \frac{\partial \overline{v_R v_z}}{\partial z} / V_{200}^2$')
        axs[5].set_ylabel(r'$R \, \overline{v_R v_z} \, \frac{\partial \ln \nu}{\partial z} / V_{200}^2$')

    else:
        for i in range(n_panels):
            axs[i].set_yticklabels([])
            
    axs[-1].set_xlabel(r'$\log \, R / r_{200}$')

    axs[0].text(xlim[0] + 0.05 * (xlim[1] - xlim[0]), vlim[0] + 0.95 * (vlim[1] - vlim[0]),
                r'$t =$' + str(t) + ' Gyr',
                ha='left', va='top')

    axs0 = axs[0].twinx()
    axs0.set_yticklabels([])
    axs0.set_yticks(axs[0].get_yticks())
    axs0.set_ylim(axs[0].get_ylim())

    axs1 = axs[1].twinx()
    axs1.set_yticklabels([])
    axs1.set_yticks(axs[1].get_yticks())
    axs1.set_ylim(axs[1].get_ylim())

    axs[0].axhline(old_vlim[0],0,1, ls=':', lw=1, c='grey')
    axs[0].axhline(old_vlim[1],0,1, ls=':', lw=1, c='grey')
    axs[1].axhline(old_vlim_thin[0],0,1, ls=':', lw=1, c='grey')
    axs[1].axhline(old_vlim_thin[1],0,1, ls=':', lw=1, c='grey')

    r_half = rcm.get_bin_edges(save_name_append='cum')[3]
    r200 = rcm.get_r_200()
    for i in range(n_panels):
        axs[i].axvline(np.log10(r_half/r200),0,1, ls=':', lw=1, c='grey')

    for ii, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        print(galaxy_name)
        kwargs = rcm.get_name_kwargs(galaxy_name)

        snap = int(round(t / T_TOT * snaps))

        V200 = rcm.get_v_200(galaxy_name)
        r200 = rcm.get_r_200(galaxy_name)

        # bins
        bin_edges = np.logspace(-1, 3, 21)
        bin_centres = 10 ** ((np.log10(bin_edges[:-1]) + np.log10(bin_edges[1:])) / 2)
        bin_edges = np.roll(np.repeat(bin_edges, 2), -1)[:-2]
        x_data = np.log10(bin_centres / r200)

        if '8p0' in galaxy_name:
            lw = 3
            rmask = x_data > -2
        elif '7p5' in galaxy_name:
            lw = 3
            rmask = x_data > -2
        elif '7p0' in galaxy_name:
            lw = 7
            rmask = np.ones(len(x_data), dtype=bool)
        elif '6p5' in galaxy_name:
            lw = 7
            rmask = np.ones(len(x_data), dtype=bool)

        # (_,_,_,_,_, mask, sigma_phi, mean_v_phi, sigma_R, sigma_z, Sigma
        #  ) = calc_profile_to_plot(galaxy_name, snap, np.hstack((bin_edges, np.nan, np.nan)),
        #                           save_name_append=save_name_append)
        (_, _, _, _, _, mask, sigma_phi, mean_v_phi, sigma_R, sigma_z, Sigma
         ) = rcm.calc_profile_to_plot(galaxy_name, snap, np.hstack((bin_edges[:-1], np.nan, np.nan)),
                                  save_name_append=save_name_append)

        # theory
        analytic_dispersion = rcm.get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                                 save_name_append=save_name_append, galaxy_name=galaxy_name)

        analytic_v_circ = rcm.get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)

        # # update data range
        # mask_theory = np.logical_and(x_data_range[0] < x_data, x_data < x_data_range[1])

        # theory
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = rcm.get_constants(bin_edges, save_name_append, kwargs['mdm'], galaxy_name=galaxy_name)

        # burn ins
        (inital_velocity_v, inital_velocity2_z, inital_velocity2_r, inital_velocity2_p
         ) = rcm.smooth_zero_snap(kwargs['mdm'], snaps, save_name_append, bin_edges)

        inital_velocity_v = np.amin((inital_velocity_v, analytic_v_circ), axis=0)

        inital_velocity2_z **= 2
        inital_velocity2_r **= 2
        inital_velocity2_p **= 2

        theory_best_v = np.zeros(len(bin_centres))
        theory_best_z2 = np.zeros(len(bin_centres))
        theory_best_r2 = np.zeros(len(bin_centres))
        theory_best_p2 = np.zeros(len(bin_centres))

        experiment_v = np.zeros(len(bin_centres))
        experiment_v2 = np.zeros(len(bin_centres))
        experiment_v3 = np.zeros(len(bin_centres))
        experiment_v4 = np.zeros(len(bin_centres))

        # experimenting
        tau_heat_v = np.zeros(len(bin_centres))
        tau_heat_z2 = np.zeros(len(bin_centres))
        tau_heat_r2 = np.zeros(len(bin_centres))
        tau_heat_p2 = np.zeros(len(bin_centres))

        # tau_heat_experiment = np.zeros(len(bin_centres))
        tau_heat_experiment2 = np.zeros(len(bin_centres))

        for i in range(len(bin_centres)):
            theory_best_v[i] = rcm.get_theory_velocity2_array(V200, analytic_v_circ[i] - inital_velocity_v[i],
                                                          np.array([t]), t_c_dm[i], delta_dm[i],
                                                          np.sqrt(upsilon_circ[i]),
                                                          ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
            theory_best_z2[i] = rcm.get_theory_velocity2_array(V200 ** 2, inital_velocity2_z[i],
                                                           np.array([t]), t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
            theory_best_r2[i] = rcm.get_theory_velocity2_array(V200 ** 2, inital_velocity2_r[i],
                                                           np.array([t]), t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
            theory_best_p2[i] = rcm.get_theory_velocity2_array(V200 ** 2, inital_velocity2_p[i],
                                                           np.array([t]), t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

            tau_heat_v[i] = rcm.get_tau_heat_array(delta_dm[i], np.sqrt(upsilon_circ[i]),
                                               ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])

            tau_heat_z2[i] = rcm.get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                                ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
            tau_heat_r2[i] = rcm.get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                                ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
            tau_heat_p2[i] = rcm.get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                                ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

            # tau_heat_experiment[i] = (t_c_dm[i] * (tau_heat_r2[i] * 1.0612))
            tau_heat_experiment2[i] = (t_c_dm[i] * (tau_heat_p2[i] * 0.74987599))

            tau_0_on_tau_heat = rcm.get_tau_zero_array(V200, analytic_v_circ[i] - inital_velocity_v[i],
                                                   np.sqrt(upsilon_circ[i]))

            # print(tau_0_on_tau_heat)

            # experiment_v[i] = analytic_v_circ[i] * np.exp(-np.array([t]) / tau_heat_experiment[i])  # tau_0_on_tau_heat)
            experiment_v2[i] = analytic_v_circ[i] * np.exp(
                -np.array([t]) / tau_heat_experiment2[i])  # tau_0_on_tau_heat)

            tau_0_on_tau_heat2 = rcm.get_tau_zero_array(V200 ** 2, inital_velocity2_p[i],
                                                    upsilon_dm[i])

        # theory_best_v = analytic_v_circ - theory_best_v
        theory_best_v = experiment_v2

        theory_d_sigma_R2_d_R = np.diff(theory_best_r2) / np.diff(bin_centres)
        theory_d_sigma_R2_d_R = np.hstack((theory_d_sigma_R2_d_R, np.nan))

        d_sigma_R2_d_R = rcm.get_one_d_sigma_R2_dR(galaxy_name, save_name_append, snap, bin_edges)
        d_ln_Sigma_d_ln_R = rcm.get_one_d_ln_Sigma_d_ln_R(galaxy_name, save_name_append, snap, bin_edges)

        (z_integrated_vc2, midplane_vc2, mass_vc2
         ) = rcm.get_one_vc2(galaxy_name, save_name_append, snap, bin_edges)

        (velocity_ellipsoid_term1, velocity_ellipsoid_term2
         ) = rcm.get_one_velocity_ellipsoid_term(galaxy_name, save_name_append, snap, bin_edges)

        R_disk = 4.15

        ####
        # print(np.shape(z_integrated_vc2), np.shape(sigma_phi ** 2),
        #       np.shape(sigma_R ** 2), np.shape(sigma_R ** 2), np.shape(d_ln_Sigma_d_ln_R),
        #       np.shape(bin_centres), np.shape(d_sigma_R2_d_R), np.shape(velocity_ellipsoid_term1),
        #       np.shape(velocity_ellipsoid_term2))

        asym_full_theory = (analytic_v_circ ** 2
                            - theory_best_p2
                            + theory_best_r2
                            - theory_best_r2 * bin_centres / R_disk
                            + bin_centres * theory_d_sigma_R2_d_R
                            + theory_best_r2 - theory_best_p2
                            + 0)

        asym_full_measure = (z_integrated_vc2
                             - sigma_phi ** 2
                             + sigma_R ** 2
                             + sigma_R ** 2 * d_ln_Sigma_d_ln_R
                             + bin_centres * d_sigma_R2_d_R
                             + velocity_ellipsoid_term1
                             + velocity_ellipsoid_term2)

        # asym_full_theory = np.sqrt(analytic_v_circ[_index] ** 2
        #                            - theory_best_p2[_index, :]
        #                            + theory_best_r2[_index, :]
        #                            - theory_best_r2[_index, :] * bin_centres[2 * _index + 1] / R_disk
        #                            + bin_centres[2 * _index + 1] * theory_d_sigma_R2_d_R
        #                            + theory_best_r2[_index, :] - theory_best_z2[_index, :]
        #                            + 0)
        #
        # asym_full_measure = np.sqrt(z_integrated_vc2[:, _index]
        #                             # midplane_vc2[:, _index]
        #                             - sigma_phi[:, _index] ** 2
        #                             + sigma_R[:, _index] ** 2
        #                             + sigma_R[:, _index] ** 2 * d_ln_Sigma_d_ln_R[:, _index]
        #                             + bin_centres[2 * _index + 1] * d_sigma_R2_d_R[:, _index]
        #                             + velocity_ellipsoid_term1[:, _index]
        #                             + velocity_ellipsoid_term2[:, _index])

        axs[0].errorbar(x_data[rmask], asym_full_measure[rmask] / V200**2,
                        color='C0', linestyle='-', linewidth=lw)
        axs[0].errorbar(x_data, asym_full_theory / V200**2,
                        color='C1', linestyle='--', linewidth=lw)
        axs[0].errorbar(x_data[rmask], mean_v_phi[rmask]**2 / V200**2,
                        color='C2', linestyle='-.', linewidth=lw)
        axs[0].errorbar(x_data, theory_best_v**2 / V200**2,
                        color='C3', linestyle=':', linewidth=lw)

        ####
        axs[1].errorbar(x_data[rmask], z_integrated_vc2[rmask] / V200**2,
                        color='C0', linestyle='-', linewidth=lw)
        axs[1].errorbar(x_data, analytic_v_circ**2 * np.ones(len(x_data)) / V200**2,
                        color='C1', linestyle='--', linewidth=lw)
        axs[1].errorbar(x_data[rmask], midplane_vc2[rmask] / V200**2,
                        color='C2', linestyle='-.', linewidth=lw)

        ####
        axs[2].errorbar(x_data[rmask], sigma_R[rmask] ** 2 * d_ln_Sigma_d_ln_R[rmask] / V200**2,
                        color='C0', linestyle='-', linewidth=lw)
        axs[2].errorbar(x_data, - theory_best_r2 * bin_centres / R_disk / V200**2,
                        color='C1', linestyle='--', linewidth=lw)
        axs[2].errorbar(x_data[rmask], theory_best_r2[rmask] * d_ln_Sigma_d_ln_R[rmask] / V200**2,
                        color='C2', linestyle='-.', linewidth=lw)

        if legend:
            axs[1].legend([(lines.Line2D([0, 1], [0, 1], color='grey', linewidth=3, ls='-')),
                           (lines.Line2D([0, 1], [0, 1], color='grey', linewidth=7, ls='-')),],
                          [r'$N_{\rm DM} = 5.8 \times 10^4$',r'$N_{\rm DM} = 5.8 \times 10^5$'])

        ####
        axs[3].errorbar(x_data[rmask], bin_centres[rmask] * d_sigma_R2_d_R[rmask] / V200**2,
                        color='C0', linestyle='-', linewidth=lw)
        axs[3].errorbar(x_data, bin_centres * theory_d_sigma_R2_d_R / V200**2,
                        color='C1', linestyle='--', linewidth=lw)

        ####
        axs[4].errorbar(x_data[rmask], velocity_ellipsoid_term1[rmask] / V200**2,
                        color='C0', linestyle='-', linewidth=lw)
        axs[4].errorbar(x_data, np.zeros(len(x_data)) / V200**2,
                        color='C2', linestyle='-.', linewidth=lw)
        axs[4].errorbar(x_data, (theory_best_r2 - theory_best_z2) / V200**2,
                        color='C1', linestyle='--', linewidth=lw)
        axs[4].errorbar(x_data[rmask], (sigma_R ** 2 - sigma_z ** 2)[rmask] / V200**2,
                        color='C3', linestyle=':', linewidth=lw)

        ####
        axs[5].errorbar(x_data[rmask], velocity_ellipsoid_term2[rmask] / V200**2,
                        color='C0', linestyle='-', linewidth=lw)
        axs[5].errorbar(x_data, np.zeros(len(x_data)) / V200**2,
                        color='C1', linestyle='--', linewidth=lw)

    if slog_yax:
        for ax in axs:
            ax.semilogy()
            ax.set_ylim([0.008, vlim[1]])

    if save:
        plt.savefig(name, bbox_inches="tight", pad_inches=0)
        plt.close()

    return


def show_best_fit_params(file_name='', save=False,
                         fixed_k=None, fixed_a=None, fixed_b=0, fixed_g=0):

    alpha_space = np.linspace(-0.8, 0.2, 41)

    # all_c_run = [True, True, False, False]
    # fixed_b_run = [0, 1, 0, 1]

    # ls_run = ['-', '-', (0,(1,1)), (0,(1,1))]
    # lw_run = [5, 3, 5, 3]
    # c_run = ['C9', 'C6', 'C0', 'C3'] #cyan, pink, blue, red
    # label_run = [r'W+23, $c\in(7,10,15)$', r'L+21, $c\in(7,10,15)$',
    #              r'W+23, $c=10$', r'L+21, $c=10$']

    all_c_run = [True]
    fixed_b_run = [0]

    ls_run = ['-']
    lw_run = [5]
    c_run = ['C9']
    label_run = [r'']

    # fig_v = plt.figure()
    # fig_v.set_size_inches(5, 5, forward=True)
    # ax_v = plt.subplot(1,1,1)

    fig_z = plt.figure()
    fig_z.set_size_inches(5, 4.5, forward=True)
    ax_z = plt.subplot(1,1,1)

    fig_R = plt.figure()
    fig_R.set_size_inches(5, 4.5, forward=True)
    ax_R = plt.subplot(1,1,1)

    fig_p = plt.figure()
    fig_p.set_size_inches(5, 4.5, forward=True)
    ax_p = plt.subplot(1,1,1)

    ax_dim = [None, ax_z, ax_R, ax_p]

    for ii, (all_c, fixed_b, ls, c, lw, label) in enumerate(zip(all_c_run, fixed_b_run, ls_run, c_run, lw_run, label_run)):

        (time_scale_array, scale_density_ratio_array, scale_velocity_ratio_array, scale_vcirc_ratio_array,
         time_array, velocity_scale_array_v, velocity_scale2_array_dispersion,
         measued_velocity_array_v, inital_velocity_array_v,
         measued_velocity2_array_z, inital_velocity2_array_z,
         measued_velocity2_array_r, inital_velocity2_array_r,
         measued_velocity2_array_p, inital_velocity2_array_p, n_array
         ) = rcm.get_raw_data(save_name_append='diff', concs=all_c)

        measured = [measued_velocity_array_v, measued_velocity2_array_z,
                    measued_velocity2_array_r, measued_velocity2_array_p]

        inital = [inital_velocity_array_v, inital_velocity2_array_z,
                  inital_velocity2_array_r, inital_velocity2_array_p]

        v_scale = [velocity_scale_array_v, velocity_scale2_array_dispersion,
                   velocity_scale2_array_dispersion, velocity_scale2_array_dispersion]

        scale_v = [scale_vcirc_ratio_array, scale_velocity_ratio_array,
                   scale_velocity_ratio_array, scale_velocity_ratio_array]

        fixed_args = []
        start_loc = []
        labels = []

        if fixed_k == None:
            # labels.append(r'$\log(\sqrt{2} \pi k_i \ln \Lambda)$')
            labels.append(r'$k_i \ln \Lambda$')
            start_loc.append(100.0)
        fixed_args.append(fixed_k)
        if fixed_a == None:
            labels.append(r'$\alpha_i$')
            start_loc.append(-0.5)
        fixed_args.append(fixed_a)
        if fixed_b == None:
            start_loc.append(0.0)
            labels.append(r'$\beta_i$')
        fixed_args.append(fixed_b)
        if fixed_g == None:
            start_loc.append(0.0)
            labels.append(r'$\gamma_i$')
        fixed_args.append(fixed_g)

        ln_Lambda_ks = []
        alphas = []
        betas = []
        gammas = []

        name_list = [r'$\overline{v}_\phi$', r'$\sigma_z$', r'$\sigma_R$', r'$\sigma_\phi$']
        x_name_list = [r'$\alpha_{\overline{v}_\phi}$', r'$\alpha_{\sigma_z}$',
                       r'$\alpha_{\sigma_R}$', r'$\alpha_{\sigma_\phi}$']

        for (measued_velocity2_array, inital_velocity2_array, velocity_scale2_array, scale_velocity_ratio_array,
             name, ax, x_name
             ) in zip(measured, inital, v_scale, scale_v, name_list, ax_dim, x_name_list):

            data = (measued_velocity2_array, velocity_scale2_array, inital_velocity2_array,
                    time_array, time_scale_array,
                    scale_density_ratio_array, scale_velocity_ratio_array, n_array)

            metrics = []

            if name == name_list[0]:
                continue
                # start_loc[0] *= 2
                #
                # the actual thing

            for alpha in alpha_space:

                if fixed_b == None:
                    min_result = minimize(rcm._log_like, [100, 0], args=(data, [None, alpha, fixed_b, fixed_g]),
                                          method='Nelder-Mead')
                else:
                    min_result = minimize(rcm._log_like, [100], args=(data, [None, alpha, fixed_b, fixed_g]),
                                          method='Nelder-Mead')
                # method='CG')
                if not min_result.success:
                    print(name)
                    print(min_result)

                metrics.append(min_result.fun)

            metrics = np.exp(np.exp(metrics))
            
            #get the best fit
            if fixed_b == None:
                min_result = minimize(rcm._log_like, [100, -0.5, 0], args=(data, [None, None, fixed_b, fixed_g]),
                                      method='Nelder-Mead')
                print(min_result)
            else:
                min_result = minimize(rcm._log_like, [100, -0.5], args=(data, [None, None, fixed_b, fixed_g]),
                                      method='Nelder-Mead')
            # method='CG')
            if not min_result.success:
                print(name)
                print(min_result)

            params = np.zeros(len(fixed_args))
            i = 0
            for j in range(len(fixed_args)):
                if fixed_args[j] == None:
                    params[j] = min_result.x[i]
                    i += 1
                else:
                    params[j] = fixed_args[j]

            # best_metric = rcm._log_like(params, data, [None] * 4)
            # best_metric = min_result.fun
            best_metric = np.exp(np.exp(min_result.fun))
            best_alpha = min_result.x[1]

            # every loop
            ln_Lambda_ks.append(params[0])
            alphas.append(params[1])
            betas.append(params[2])
            gammas.append(params[3])

            ax.errorbar(alpha_space, metrics,
                        c=c, ls=ls, lw=lw, label=label, marker='', zorder=-ii)
            ax.scatter(best_alpha, best_metric, c=c, marker='X', edgecolors='k', s=15**2, zorder=10-ii)

            # print(best_alpha, best_metric)

            ax.set_xlim([alpha_space[0], alpha_space[-1]])
            ax.set_ylim([1.001,1.01])
            ax.set_xlabel(x_name)
            # ax.set_ylabel(r'Figure of merit')
            ax.set_ylabel(r'FoM')
            # ax.set_title(name)
            if name == name_list[1]:
                ax.legend(title=name, frameon=False)
            else:
                ax.legend([], [], title=name, frameon=False)

    if save:
        for fig, name in zip([fig_z, fig_R, fig_p], ['z', 'R', 'phi']):
            fig.savefig(file_name + name + '.pdf', bbox_inches='tight')
            # fig.close()
        plt.close()
        plt.close()
        plt.close()

    return


def show_best_fit_params_v(file_name='', save=False):
    f_space = np.linspace(0.35, 1.35, 41)

    fig = plt.figure()
    fig.set_size_inches(5, 4.5, forward=True)
    ax = plt.subplot(1,1,1)

    (ln_Lambda_ks, alphas, betas, gammas
     ) = rcm.find_least_log_squares_best_fit(
        'diff', mcmc=False, fixed_b=0, fixed_g=0)

    (time_scale_array, scale_density_ratio_array, scale_velocity_ratio_array, scale_vcirc_ratio_array,
     time_array, velocity_scale_array_v, velocity_scale2_array_dispersion,
     measued_velocity_array_v, inital_velocity_array_v,
     measued_velocity2_array_z, inital_velocity2_array_z,
     measued_velocity2_array_r, inital_velocity2_array_r,
     measued_velocity2_array_p, inital_velocity2_array_p, n_array
     ) = rcm.get_raw_data(save_name_append='diff')

    tau_heat_v = rcm.get_tau_heat_array(scale_density_ratio_array, scale_vcirc_ratio_array,
                                    ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
    tau_heat_z2 = rcm.get_tau_heat_array(scale_density_ratio_array, scale_velocity_ratio_array,
                                     ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
    tau_heat_r2 = rcm.get_tau_heat_array(scale_density_ratio_array, scale_velocity_ratio_array,
                                     ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
    tau_heat_p2 = rcm.get_tau_heat_array(scale_density_ratio_array, scale_velocity_ratio_array,
                                     ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

    tau_0_on_tau_heat = rcm.get_tau_zero_array(velocity_scale_array_v, inital_velocity_array_v,
                                               np.sqrt(scale_vcirc_ratio_array))

    def to_min(f):
        # tau_heat_experiment = (tau_heat_p2 * scale_velocity_ratio_array / scale_vcirc_ratio_array)
        # tau_heat_experiment = ((tau_heat_z2 + tau_heat_r2 + tau_heat_p2) / 3 * f)
        # tau_heat_experiment = ((tau_heat_r2 + tau_heat_p2) / 2 * f)
        tau_heat_experiment = (tau_heat_p2 * f)

        experiment_v = velocity_scale_array_v * scale_vcirc_ratio_array ** 2 * (1 - np.exp(
            - np.power(((time_array / time_scale_array) / tau_heat_experiment) + tau_0_on_tau_heat, 1 + gammas[0])))

        # out = np.sum((experiment_v - measued_velocity_array_v) ** 2)
        out = np.log(np.mean((np.log(experiment_v) - np.log(measued_velocity_array_v)) ** 2 / np.log(experiment_v)))

        return out

    out = minimize(to_min, x0=(1,), method='Powell')
    # print(out)

    metrics = []
    for f in f_space:
        metrics.append(to_min(f))
    metrics = np.exp(np.exp(metrics))

    ax.errorbar(f_space, metrics,
                c='C9', ls='-', lw=5, marker='', zorder=0, label=r'$t_{\sigma_\phi}$')
    ax.scatter(out.x, np.exp(np.exp(out.fun)), c='C9', marker='X', edgecolors='k', s=15 ** 2, zorder=10)

    def to_min(f):
        tau_heat_experiment = (tau_heat_r2 * f)

        experiment_v = velocity_scale_array_v * scale_vcirc_ratio_array ** 2 * (1 - np.exp(
            - np.power(((time_array / time_scale_array) / tau_heat_experiment) + tau_0_on_tau_heat, 1 + gammas[0])))

        # out = np.sum((experiment_v - measued_velocity_array_v) ** 2)
        out = np.log(np.mean((np.log(experiment_v) - np.log(measued_velocity_array_v)) ** 2 / np.log(experiment_v)))

        return out

    out = minimize(to_min, x0=(1,), method='Powell')
    # print(out)

    metrics = []
    for f in f_space:
        metrics.append(to_min(f))
    metrics = np.exp(np.exp(metrics))

    # ax.errorbar(f_space, metrics,
    #             c='C8', ls='--', lw=3, marker='', zorder=-1, label=r'$t_{\sigma_R}$')
    # ax.scatter(out.x, np.exp(np.exp(out.fun)), c='C8', marker='X', edgecolors='k', s=15 ** 2, zorder=9)

    def to_min(f):
        tau_heat_experiment = (tau_heat_z2 * f)

        experiment_v = velocity_scale_array_v * scale_vcirc_ratio_array ** 2 * (1 - np.exp(
            - np.power(((time_array / time_scale_array) / tau_heat_experiment) + tau_0_on_tau_heat, 1 + gammas[0])))

        # out = np.sum((experiment_v - measued_velocity_array_v) ** 2)
        out = np.log(np.mean((np.log(experiment_v) - np.log(measued_velocity_array_v)) ** 2 / np.log(experiment_v)))

        return out

    out = minimize(to_min, x0=(0.5,), method='CG') #'Powell')
    # print(out)

    metrics = []
    for f in f_space:
        metrics.append(to_min(f))
    metrics = np.exp(np.exp(metrics))

    # ax.errorbar(f_space, metrics,
    #             c='C7', ls='-.', lw=3, marker='', zorder=-2, label=r'$t_{\sigma_z}$')
    # ax.scatter(out.x, np.exp(np.exp(out.fun)), c='C7', marker='X', edgecolors='k', s=15 ** 2, zorder=8)

    ax.set_xlim([f_space[0], f_space[-1]])
    ax.set_ylim([1.05, 1.2])
    # ax.set_yticklabels(['1.050', '1.100', '1.150', '1.200'])
    ax.set_xlabel(r'$f = t_{\overline{v}_\phi} / t_{\sigma_\phi}$')
    ax.set_ylabel(r'FoM')
    # ax.set_title(name)
    # ax.legend(title=r'$\overline{v}_\phi$')
    ax.legend([], [], title=r'$\overline{v}_\phi$', frameon=False)

    if save:
        plt.savefig(file_name + 'v.pdf', bbox_inches='tight')
        plt.close()

    return



if __name__ == '__main__':
    from time import time as _t

    start_time = _t()

    save = True

    # # #Figure 2.1
    # plot_time_evolution(name='../galaxies/soft1_time_evolution_epsilon.pdf', save=save)

    # #Figure 2.2
    # plot_time_evolution(name='../galaxies/seeds_time_evolution.pdf', save=save)

    #Figure 2.4
    # plot_time_evolution(name='../galaxies/mu2_time_evolution.pdf', save=save)

    # # #Figure 2.2
    # ts = [0, 3, 9]
    # lab = [True, False, False]
    # for t, la in zip(ts, lab):
    #     plot_velocity_profiles_after_t(t=t, lab=la, eps=True, model=False,
    #                                    name=f'../galaxies/eps_velocity_profile_{t}.pdf', save=save)

    # #Figure 2.4
    # plot_smooth_time_evolution(name='../galaxies/time_evolution_smooth.pdf', save=save)

    # #Figure 2.5
    # plot_time_evolution(name='../galaxies/time_evolution_three_radii.pdf', save=save)

    # plot_time_evolution(name='../galaxies/streaming_evolution_three_radii.pdf', save=save)
    # plot_time_evolution(name='../galaxies/soft8_time_evolution_three_radii.pdf', save=save)
    # plot_time_evolution(name='../galaxies/soft7_time_evolution_three_radii.pdf', save=save)

    # # Figure 2.6
    # ts = [1, 3, 9]
    # lab = [True, False, False]
    # for t, la in zip(ts, lab):
    #     plot_velocity_profiles_after_t(t=t, lab=la,
    #                                    name=f'../galaxies/all_velocity_profile_{t}.pdf', save=save)

    # plot_velocity_profiles_after_t(t=6, lab=False, model=False, mu=True, logy=True,
    #                                name=f'../galaxies/all_velocity_profile_{6}.pdf', save=True)
    # plot_velocity_profiles_after_t(t=8, lab=False, model=False, mu=True, logy=True,
    #                                name=f'../galaxies/all_velocity_profile_{8}.pdf', save=True)

    # #Figure 2.7
    # plot_model_evolution(sig=True, concs=True, name='../galaxies/time_evolution_models_concs.pdf', save=save)

    # #Figure 2.8
    # plot_model_evolution(sig=True, name='../galaxies/time_evolution_models_sig.pdf', save=save)
    # plot_model_evolution(name='../galaxies/time_evolution_models.pdf', save=save)

    # # #Figure 2.9
    # show_best_fit_params(file_name='../galaxies/heating_eqn_best_diff', save=save)
    # show_best_fit_params_v(file_name='../galaxies/heating_eqn_best_diff', save=save)
    # ?
    # rcm.find_least_log_squares_best_fit(save_name_append='diff',
    #                                     fixed_k=None, fixed_a=None, fixed_b=None, fixed_g=0,
    #                                     mcmc=True, overwrite=True, analyse_asymmetry=False)

    # #Figure 2.10
    # plot_v_phi_distributions(name='../galaxies/v_phi_profile_diff.pdf', save=save)
    # plot_v_phi_profile(name='../galaxies/v_phi_profile.pdf', save=save)
    # rcm.plot_v_phi_distributions_experiments(name='../galaxies/v_phi_profile_evolution', save=save)

    # # Figure 2.11
    # plot_explicit_asymm_time(name='../galaxies/asym_term_time.pdf',
    #                          slog_yax=False, save=save)
    # ts = [3, 9]
    # lab = [True, False]
    # for t, la in zip(ts, lab):
    #     print(la)
    #     plot_explicit_asymm_rad(name=f'../galaxies/asym_term_rad_{t}.pdf', t=t,
    #                             slog_yax=False, save=save, label=la, legend=la)

    # #appendix ?
    # rcm.experiment_concentration_dependances(name='', save=False)

    # #Figure 2.12
    # plot_model_evolution_extras(name='../galaxies/time_evolution_extra_params.pdf', save=save)

    # #Figure 2.13
    # plot_size_evolution(name='../galaxies/ideal_size_evolution.pdf', save=save)
    # #Figure 2.14
    # plot_ideal_j_evolution(name='../galaxies/ideal_j.pdf', save=save)
    # plot_ideal_j_evolution(name='../galaxies/ideal_j_dms.pdf', save=save, do_DM=True)

    # #Figure 2.15
    # skn.plot_vel_n_dependence(name='../galaxies/vel_n_dependance.pdf', save=save)


    # randomness_in_ics(name='../galaxies/randomness_in_ics.pdf', save=save)
    # randomness_in_ics2(name='../galaxies/randomness_ics_summary.pdf', save=save)
    #
    # projected_potential_viz(name='../galaxies/smooth_lumpy_projections.pdf', save=save)


    # plot_time_evolution(name='../galaxies/streaming_evolution_three_radii.pdf', save=save)
    plot_ideal_j_evolution(name='../galaxies/ideal_j_dms.pdf', save=save, do_DM=True)

    end_time = _t()

    print('Time', end_time - start_time)

    plt.show()

    pass
