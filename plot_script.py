#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 28 11:18:24 2020

@author: matt
"""
import os
# import time

import numpy as np
# import h5py as h5

from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.interpolate import interp1d
from scipy.interpolate import interpn

from scipy.integrate   import quad

from scipy.optimize    import minimize
from scipy.optimize    import brentq

from scipy.signal      import savgol_filter

from scipy.spatial.transform import Rotation

from scipy.special     import gamma
from scipy.special     import lambertw
from scipy.special     import erf

from scipy.stats       import binned_statistic
from scipy.stats       import binned_statistic_2d

#matplotlib
import matplotlib.pyplot as plt
import splotch as splt

from matplotlib.colors import LogNorm
from matplotlib.colors import ListedColormap
from matplotlib.cm     import viridis
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

#for gifs
import imageio

#for getting argument names of funtions
import inspect

#my files
import halo_calculations as halo
import load_nbody

import recalibrate_heating_model as *

from matplotlib import rcParams
rcParams['ps.useafm'] = True
rcParams['pdf.use14corefonts'] = False
rcParams['text.usetex'] = False
rcParams['font.sans-serif'] = ['cmr10', 'Times-Roman']
rcParams['font.weight'] = 'normal'
rcParams['font.size'] = 20
rcParams['xtick.labelsize'] = 20
rcParams['ytick.labelsize'] = 20
rcParams['axes.labelsize'] = 22
rcParams['axes.linewidth'] = 1
rcParams['axes.unicode_minus'] = False
# rcParams['axes.grid'] = True
rcParams['axes.grid'] = False
rcParams['xtick.minor.width'] = 1
rcParams['ytick.minor.width'] = 1
rcParams['xtick.major.width'] = 1
rcParams['ytick.major.width'] = 1
rcParams['xtick.minor.size'] = 4
rcParams['ytick.minor.size'] = 4
rcParams['xtick.major.size'] = 5
rcParams['ytick.major.size'] = 5
rcParams['xtick.direction'] = 'in'
rcParams['ytick.direction'] = 'in'
rcParams['xtick.top'] = True
rcParams['ytick.right'] = True
rcParams['mathtext.fontset'] = 'cm'

grav_const   = 4.302e4 #kpc (km/s)^2 / (10^10 Msun) (source ??)


def histOutline(dataIn, *args, **kwargs):
  (histIn, binsIn) = np.histogram(dataIn, *args, **kwargs)

  stepSize = binsIn[1] - binsIn[0]

  bins = np.zeros(len(binsIn)*2 + 2, dtype=np.float)
  data = np.zeros(len(binsIn)*2 + 2, dtype=np.float)
  for bb in range(len(binsIn)):
    bins[2*bb + 1] = binsIn[bb]
    bins[2*bb + 2] = binsIn[bb] + stepSize
    if bb < len(histIn):
      data[2*bb + 1] = histIn[bb]
      data[2*bb + 2] = histIn[bb]

  bins[0] = bins[1]
  bins[-1] = bins[-2]
  data[0] = 0
  data[-1] = 0

  return (bins, data)

###############################################################################
#change coordinates
def get_cylindrical(PosStars, VelStars):
  '''Calculates cylindrical coordinates.
  '''
  rho      = np.sqrt(PosStars[:,0]**2 + PosStars[:,1]**2)
  varphi   = np.arctan2(PosStars[:,1], PosStars[:,0])
  z        = PosStars[:,2]

  v_rho    = VelStars[:,0] * np.cos(varphi) + VelStars[:,1] * np.sin(varphi)
  v_varphi = -VelStars[:,0]* np.sin(varphi) + VelStars[:,1] * np.cos(varphi)

  # v_rho    = (PosStars[:,0] * VelStars[:,0] + PosStars[:,1] * VelStars[:,1]
  #             ) / np.sqrt(PosStars[:,0]**2 + PosStars[:,1]**2)
  # v_varphi = (PosStars[:,0] * VelStars[:,1] - PosStars[:,1] * VelStars[:,0]
  #             ) / np.sqrt(PosStars[:,0]**2 + PosStars[:,1]**2)

  v_z      = VelStars[:,2]

  return(rho, varphi, z, v_rho, v_varphi, v_z)

def get_equal_number_bins(x, n_per_bin):
  '''
  '''
  n_per_bin = int(np.round(n_per_bin))
  x_argsort = np.argsort(x)
  #make bin edges half way between point [i * n_per_bin -1] and [i * n_per_bin]
  edges = [0.5*(x[x_argsort][i*n_per_bin-1] + x[x_argsort][i*n_per_bin])
           for i in range(int(np.ceil(len(x)/n_per_bin)))]
  edges[0] = np.amin(x)
  edges.append(np.amax(x))
  edges = np.array(edges)
  return(edges)

def my_density_plot(x, y, bins=None, ax=None,
                    v_transition=None, vmax=None, s=2, cmap=plt.cm.viridis):
  '''
  '''
  if np.all(bins == None):
    bins = int(np.ceil(min((np.sqrt(len(x)/(10)), 200))))

  data, x_e, y_e = np.histogram2d(x, y, bins=bins, density=False)
  data = np.log10(data+1)

  if vmax == None:
    vmax = np.amax(data)
  if v_transition == None:
    # v_transition = np.log10(50)
    v_transition = max(np.log10(100/len(x_e)*len(y_e)), 1.7)
    # print(v_transition)

  z = interpn((0.5*(x_e[1:] + x_e[:-1]), 0.5*(y_e[1:]+y_e[:-1])), data,
              np.vstack([x, y]).T, bounds_error=False, fill_value=None,
              method='nearest')

  #only turn into hist2d if it doesn't look too bad
  if (x_e[1] - x_e[0]) / (x_e[-1] - x_e[0]) < 0.03:

    #only show the ones on the outskirts
    low_mask = np.less_equal(z, v_transition)
    # print(np.sum(low_mask)) #on the order of 5000.
    x_low, y_low, z_low = x[low_mask], y[low_mask], z[low_mask]

    if ax != None:
      ax.scatter(x_low, y_low, c=z_low, s=s, zorder=-1, rasterized=True,
                 vmin=0,vmax=vmax, cmap=cmap)
    else:
      plt.scatter(x_low, y_low, c=z_low, s=s, zorder=-1, rasterized=True,
                  vmin=0,vmax=vmax, cmap=cmap)

    #make a colormap that is transparent under a density threshold
    viridis_alpha = cmap(np.arange(cmap.N))

    c = min(v_transition/vmax, 1)
    viridis_alpha[:,-1] = np.hstack((np.zeros(int(np.floor(c*cmap.N))),
                                    1*np.ones(int(np.ceil((1-c)*cmap.N)))))
    viridis_alpha = ListedColormap(viridis_alpha)

    if ax != None:
      ax.pcolor(x_e, y_e, data.T, #edgecolor='',
                vmin=0,vmax=vmax, cmap=viridis_alpha)
    else:
      plt.pcolor(x_e, y_e, data.T, #edgecolor='',
                 vmin=0,vmax=vmax, cmap=viridis_alpha)

  else:
    if ax != None:
      ax.scatter(x, y, c=z, s=s, zorder=-1, rasterized=True,
                 vmin=0,vmax=vmax, cmap=plt.cm.viridis)
    else:
      plt.scatter(x, y, c=z, s=s, zorder=-1, rasterized=True,
                  vmin=0,vmax=vmax, cmap=plt.cm.viridis)

  return(vmax)

def my_better_violin_plot(x, y, xbin_edges, ybin_edges,
                          quartiles=[],
                          global_scale=True, local_scale=False, fixed_scale=False, bin_scale=1,
                          alpha=0.8, color='C0', label=''):

  #the main slow part
  x_bin_index = np.digitize(x, bins=xbin_edges)

  if global_scale:
    scale = bin_scale * (xbin_edges[1] - xbin_edges[0]) * len(xbin_edges) / len(x)
  elif fixed_scale:
    scale = bin_scale

  # quartile_data = np.zeros((len(quartiles), len(xbin_edges)-1))
  mean_data = np.zeros(len(xbin_edges)-1)
  disp_data = np.zeros(len(xbin_edges)-1)

  for i in range(len(xbin_edges)-1):

    ydata = y[x_bin_index==(i+1)]
    hist_height,_ = np.histogram(ydata, bins=ybin_edges)

    if local_scale:
      #TODO should have option to normalize so the area = 1 rather than the max = 1
      scale = bin_scale * (xbin_edges[i+1] - xbin_edges[i]) / np.amax(hist_height)

    if i==0:
      plt.fill_betweenx(np.repeat(ybin_edges, 2)[1:-1],
                        np.repeat(0.5*(xbin_edges[i] + xbin_edges[i+1]) *
                                  np.ones(len(ybin_edges)-1), 2),
                        np.repeat(0.5*(xbin_edges[i] + xbin_edges[i+1]) +
                                  hist_height * scale, 2),
                        where=(np.repeat(hist_height, 2) != 0),
                        label=label,
                        alpha=alpha, color=color, zorder=10)

    else:
      plt.fill_betweenx(np.repeat(ybin_edges, 2)[1:-1],
                        np.repeat(0.5*(xbin_edges[i] + xbin_edges[i+1]) *
                                  np.ones(len(ybin_edges)-1), 2),
                        np.repeat(0.5*(xbin_edges[i] + xbin_edges[i+1]) +
                                  hist_height * scale, 2),
                        where=(np.repeat(hist_height, 2) != 0),
                        alpha=alpha, color=color, zorder=10)

    # for j in range(len(quartiles)):
    #   quartile_data[j, i] = my_quantile(ydata, quartiles[j])

    mean_data[i] = np.mean(ydata)
    disp_data[i] = np.std(ydata)

  # for j in range(len(quartiles)):
    # if j == 0 and label != '':
    #   plt.plot(0.5*(xbin_edges[1:] + xbin_edges[:-1]), quartile_data[j],
    #            color='C9', linewidth=3, label='Quantiles')
    # else:
    #   plt.plot(0.5*(xbin_edges[1:] + xbin_edges[:-1]), quartile_data[j],
    #            color='C9', linewidth=3)

  if label != '':
    plt.plot(0.5*(xbin_edges[1:] + xbin_edges[:-1]), mean_data,
             color='C9', linewidth=3, label='Measured')
  else:
    plt.plot(0.5*(xbin_edges[1:] + xbin_edges[:-1]), mean_data,
             color='C9', linewidth=3)
  plt.plot(0.5*(xbin_edges[1:] + xbin_edges[:-1]), mean_data + disp_data,
           color='C9', linewidth=3, ls='--')
  plt.plot(0.5*(xbin_edges[1:] + xbin_edges[:-1]), mean_data - disp_data,
           color='C9', linewidth=3, ls='--')

  return

'''
###############################################################################
######################## single galaxy, single snapshot #######################
###############################################################################
'''
#TODO write better
# def plot_projection(PosStars, name='', snap='', save=False):
#   '''Depreciated
#   '''
#   xlim=[-30,30]
#   ylim=xlim
#   zlim=xlim
#   bins = np.linspace(xlim[0], xlim[1], 81)

#   fig, ax = plt.subplots(nrows=2, ncols=1, sharex=True, sharey=True,
#                          figsize=(5, 8))

#   max_height = 100
#   min_height = 1

#   splt.hist2D(PosStars[:,0], PosStars[:,1],
#               clog=True, clim=[min_height,max_height], xlim=xlim, ylim=ylim,
#               bins=bins, ax=ax[0], dens=False)
#   splt.hist2D(PosStars[:,0], PosStars[:,2],
#               clog=True, clim=[min_height,max_height], xlim=xlim, ylim=zlim,
#               bins=bins, ax=ax[1], dens=False)

#   ax[0].set_ylabel('y [kpc]')
#   ax[1].set_ylabel('z [kpc]')
#   ax[1].set_xlabel('x [kpc]')

#   cbar_ax = fig.add_axes([0.92, 0.15, 0.02, 0.7])
#   plt.colorbar(cax=cbar_ax)

#   fig.canvas.draw()
#   image = np.frombuffer(fig.canvas.tostring_rgb(), dtype='uint8')
#   image  = image.reshape(fig.canvas.get_width_height()[::-1] + (3,))

#   plt.show() if save:
#     fname = '../galaxies/' + name + '/results/position_projection_' + snap
#     plt.savefig(fname, bbox_inches="tight")
#     plt.close()

#   return(image)

def plot_projection(PosStars, MassStars,
                    top_leaf_id=1234, galaxy_id=5678, last_prog_id=9101,
                    decendant_id=1121,
                    save=True):
  '''Plot
  '''
  xlim=[-30,30]
  bins = np.linspace(xlim[0], xlim[1], 121) #61


  fig = plt.figure()
  fig.set_size_inches(5, 8, forward=True)

  ax0 = plt.subplot(211)
  ax1 = plt.subplot(212)

  fig.subplots_adjust(hspace=0.03,wspace=0)

  #Calculate the correct colors
  (mass_xy, _,_,_
   ) = binned_statistic_2d(PosStars[:,1], PosStars[:,0], values=MassStars,
                           statistic='sum', bins=bins)
  (mass_xz, _,_,_
   ) = binned_statistic_2d(PosStars[:,2], PosStars[:,0], values=MassStars,
                           statistic='sum', bins=bins)

  #EAGLE mass unit is 10^10 M_sun. Convert back to M_sun
  units = 10

  min_mass = np.amin(MassStars)
  log_mass_xy = np.log10(mass_xy + min_mass) + units
  log_mass_xz = np.log10(mass_xz + min_mass) + units

  min_mass = np.log10(min_mass) + units
  max_cell = np.amax((log_mass_xy, log_mass_xz))

  _     = ax0.pcolormesh(bins, bins, log_mass_xy, vmin=min_mass, vmax=max_cell)
  image = ax1.pcolormesh(bins, bins, log_mass_xz, vmin=min_mass, vmax=max_cell)

  ax1.text(-28,25,  'TopLeafID='  +str(top_leaf_id),  c='lightgrey', fontsize=16)
  ax1.text(-28,20,  'GalaxyID='   +str(galaxy_id),    c='lightgrey', fontsize=16)
  ax1.text(-28,-23, 'LastProgID=' +str(last_prog_id), c='lightgrey', fontsize=16)
  ax1.text(-28,-28, 'DecendantID='+str(decendant_id), c='lightgrey', fontsize=16)

  ax0.set_ylabel('y [kpc]')
  ax1.set_ylabel('z [kpc]')
  ax1.set_xlabel('x [kpc]')
  ax0.set_xticklabels([])

  cbar_ax = fig.add_axes([0.84, 0.11, 0.02, 0.77])
  cbar = fig.colorbar(image, cax=cbar_ax)
  cbar.set_label(r'$\log_{10} $ M$_\odot $')

  ax0.set_aspect('equal')
  ax1.set_aspect('equal')

  plt.show() if save:
    fname = '/home/matt/Pictures/test'
    plt.savefig(fname, bbox_inches="tight")
    plt.close()

  return

def plot_jzonc(j_z_stars, j_c_stars, MassStar,
               name='', snap='', save=False, ax=None):
  '''
  '''
  if ax==None:
    fig, ax = plt.subplots()

  jzonc = j_z_stars / j_c_stars

  if ax.lines == []:
    c='C0'
  # elif time == 10:
  #   c='C1'
  else:
    c='C1'

  ax.hist(jzonc, bins=np.linspace(-1.5,1.5,61), label='_nolegend_',
          weights=MassStar*np.ones(len(j_z_stars)), alpha=0.5, color=c)

  (bins, n) = histOutline(jzonc, bins=np.linspace(-1.5,1.5,61),
                          weights=MassStar*np.ones(len(j_z_stars)))
  ax.errorbar(bins, n, c=c)

  ax.set_xlabel(r'$j_z/j_c(E)$')
  ax.set_ylabel(r'$M_*$ [$10^{10} $M$_\odot$]')

  ax.grid(b=True)
  ax.set_xlim((-1.5,1.5))
  ax.set_ylim((0,1.7))

  plt.show() if save:
    fname = '../galaxies/' + name + '/results/j_zonc_hist_' + snap
    plt.savefig(fname, bbox_inches='tight')
    plt.close()
  return

def plot_e_jz(energy_stars, j_z_stars, E_interp, j_circ_interp, MassStar=1,
              hexbins=False, square=False, kde=False,
              name='', snap='', save=False, ax=None):
  '''
  '''
  if ax==None:
    fig, ax = plt.subplots()

  x = energy_stars/10**5
  y = j_z_stars/10**3

  n_bin_edges = int(np.ceil(min((np.sqrt(len(x)/(10)), 200))))
  bins = [np.linspace(-2.5, -1, n_bin_edges), np.linspace(-2, 6, n_bin_edges)]

  my_density_plot(x, y, bins=bins, ax=ax, v_transition=None)

  ax.errorbar(E_interp/10**5, j_circ_interp/10**3, fmt='',ls='-.', c='r',
              alpha=0.5, label=r'$j_z/j_c = \pm 1$')
  ax.errorbar(E_interp/10**5,-j_circ_interp/10**3, fmt='',ls='-.', c='r',
              alpha=0.5)

  ax.set_xlabel(r'E [$10^5$ (km/s)$^2$]')
  ax.set_ylabel(r'$j_z$ [$10^3$ kpc km/s]')
  ax.legend()

  ax.set_xlim((-2.5, -1))
  ax.set_ylim((-2,    6))

  ax.set_xticks(     [-2.5,-2,-1.5,-1])
  ax.set_xticklabels([-2.5,-2,-1.5,''])
  ax.set_yticks(     [-2,0,2,4, 6])
  ax.set_yticklabels([-2,0,2,4,''])
  ax.grid()

  plt.show() if save:
    fname = '../galaxies/' + name + '/results/e_jz_' + snap
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  return

def plot_jz_r(PosStars, j_z_stars, R_interp, j_circ_interp,
              name='', snap='', save=False, ax=None):
  '''
  '''
  #include nfw approx
  r = np.linalg.norm(PosStars, axis=1)

  if ax==None:
    fig, ax = plt.subplots()

  n_bin_edges = int(np.ceil(min((np.sqrt(len(r)/(10)), 200))))
  bins = [np.linspace(0,50, n_bin_edges), np.linspace(-3,15, n_bin_edges)]

  my_density_plot(r, j_z_stars/10**3, bins=bins, ax=ax, v_transition=None)

  ax.errorbar(R_interp, j_circ_interp/10**3, fmt='',
              ls=':', c='k', label=r'$j_z/j_c = 1$')

  ax.set_xlabel(r'r [kpc]')
  ax.set_ylabel(r'$j_z$ [$10^3$ kpc km/s]')
  ax.legend()

  ax.set_xticks([0,20,40,50])
  ax.set_xticklabels([0,20,40,''])
  # ax.set_yticks([0,5,10])
  # ax.set_yticklabels([0,5,10])
  ax.grid()
  ax.set_xlim((0,50))
  ax.set_ylim((-3,15))

  plt.show() if save:
    fname = '../galaxies/' + name + '/results/r_jz_' + snap
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  return

def plot_e_r(PosStars, energy_stars, R_interp, E_interp,
             name='', snap='', save=False, ax=None):
  '''
  '''
  #include nfw approx
  r = np.linalg.norm(PosStars, axis=1)

  if ax==None:
    fig, ax = plt.subplots()

  n_bin_edges = int(np.ceil(min((np.sqrt(len(r)/(10)), 200))))
  bins = [np.linspace(0,50, n_bin_edges), np.linspace(-2.5,-0.5, n_bin_edges)]

  my_density_plot(r, energy_stars/10**5, bins=bins, ax=ax, v_transition=None)

  ax.errorbar(R_interp,  E_interp/10**5, fmt='',  ls=':', c='k',
              label=r'$j_z/j_c = 1$')

  ax.set_xlabel(r'r [kpc]')
  ax.set_ylabel(r'$e$ [$10^5$ (km/s)$^2$]')
  ax.legend()

  ax.set_xticks([0,10,20,30,40,50])
  ax.set_xticklabels([0,10,20,30,40,''])
  ax.set_yticks([-2,-1])
  ax.set_yticklabels([-2,-1])
  ax.grid()
  ax.set_xlim((0,50))
  ax.set_ylim((-2.5,-0.5))

  plt.show() if save:
    fname = '../galaxies/' + name + '/results/r_e_' + snap
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  return

def plot_jzonc_r(PosStars, j_z_stars, j_c_stars,
                 name='', snap='', save=False, ax=None):
  '''
  '''
  r = np.linalg.norm(PosStars, axis=1)

  if ax==None:
    fig, ax = plt.subplots()

  n_bin_edges = int(np.ceil(min((np.sqrt(len(r)/(10)), 200))))
  bins = [np.linspace(0,50, n_bin_edges), np.linspace(-1.5,1.5, n_bin_edges)]

  my_density_plot(r, j_z_stars/j_c_stars, bins=bins, ax=ax, v_transition=None)

  ax.errorbar([0,np.amax(r)], [1,1], fmt='',  ls=':', c='k',
              label=r'$j_z/j_c(E) = 1$')

  ax.set_xlabel(r'r [kpc]')
  ax.set_ylabel(r'$j_z/j_c(E)$')
  ax.legend()

  ax.set_xticks([0,20,40])
  ax.set_xticklabels([0,20,40])
  ax.set_yticks([-1,0,1])
  ax.set_yticklabels([-1,0,1])
  ax.grid()
  ax.set_xlim((0,50))
  ax.set_ylim((-1.5,1.5))

  plt.show() if save:
    fname = '../galaxies/' + name + '/results/r_jzonc_' + snap
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

def plot_jzonc_r_inner(PosStars, j_z_stars, j_c_stars,
                       name='', snap='', save=False, ax=None):
  '''
  '''
  #inter 5 kpc
  r = np.linalg.norm(PosStars, axis=1)

  if ax==None:
    fig, ax = plt.subplots()

  n_bin_edges = int(np.ceil(min((np.sqrt(len(r)/(10)), 200))))
  bins = [np.linspace(0,50, n_bin_edges), np.linspace(-1.5,1.5, n_bin_edges)]

  my_density_plot(r, j_z_stars/j_c_stars, bins=bins, ax=ax, v_transition=None)

  # ax.errorbar([0,np.amax(r)], [1]*2,   fmt='',  ls=':', c='k',
  #             label=r'$j_z/j_c(E) = 1$')
  # ax.errorbar([0,np.amax(r)], [0.7]*2, fmt='',  ls=':', c='k',
  #             label=r'$j_z/j_c(E) = 0.7$')
  # ax.errorbar([0,np.amax(r)], [0]*2,   fmt='',  ls=':', c='k',
  #             label=r'$j_z/j_c(E) = 0$')

  ax.set_xlabel(r'r [kpc]')
  ax.set_ylabel(r'$j_z/j_c(E)$')
  # ax.legend()

  ax.set_xticks([0,2,4])
  ax.set_xticklabels([0,2,4])
  ax.set_yticks([-1,0,1])
  ax.set_yticklabels([-1,0,1])
  ax.grid()
  ax.set_xlim((0,5))
  ax.set_ylim((-1.5,1.5))

  plt.show() if save:
    fname = '../galaxies/' + name + '/results/r_jzonc_inner5_' + snap
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  return

def plot_inclination(PosStars, VelStars, MassStar,
                     name='', snap='', save=False, ax=None):
  '''
  '''
  j_star = np.cross(PosStars, VelStars)

  cosi = j_star[:,2] / np.linalg.norm(j_star, axis=1)
  inclination = np.arccos(cosi)

  if ax==None:
    fig, ax = plt.subplots()

  ax.hist(inclination/np.pi*180, bins=np.linspace(0,180,61),
          weights=MassStar*np.ones(len(cosi)))

  ax.set_xlabel(r'$|i|$')
  ax.set_ylabel(r'$M_*$ [$10^{10} M_\odot$]')

  ax.set_xticks([0,45,90,135,180])
  ax.set_xticklabels([0,45,90,135,''])
  ax.set_yticks([0,0.5,1])
  ax.set_yticklabels([0,0.5,1])
  ax.grid()
  ax.set_xlim((0,180))
  ax.set_ylim((0,1.2))

  plt.show() if save:
    fname = '../galaxies/' + name + '/results/inclination_' + snap
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  return

def plot_density_profile(PosStars, MassStar,
                         name='', snap='', save=False, ax=None):
  '''
  '''
  R = np.linalg.norm((PosStars[:,0], PosStars[:,1]), axis=0)
  #get bins
  n_per_bin = np.sqrt(len(R))*2
  bins = get_equal_number_bins(R, n_per_bin) # * u.kpc

  # bins = np.arange(0,50,20/np.sqrt(len(R)))

  #calculate surface density
  bin_count, _ = np.histogram(R, bins=bins)
  bin_area = np.pi * (np.roll(bins, -1)**2 - bins**2)[:-1]
  #maybe should be mean of points in each bin
  bin_R = 0.5 * (bins[:-1] + bins[1:])
  surface_density = MassStar * bin_count / bin_area * 10**10 # M_sun / kpc^2

  R_sort = np.argsort(R)
  if len(R)%2 == 0:
    half_mass_radii = (R[R_sort][len(R)>>1] + R[R_sort][(len(R)>>1)-1])/2
  else:
    half_mass_radii = R[R_sort][int(len(R)/2)]

  if ax == None:
    fig, ax = plt.subplots()

  bar = ax.errorbar(bin_R, np.log10(surface_density), ls='-', fmt='', alpha=0.5,
                    yerr=np.array((np.log10(1+1/np.sqrt(bin_count+1)),
                                   np.log10(1-1/np.sqrt(bin_count+1)))))
  ax.fill_between(bin_R, np.log10(surface_density*(1+1/np.sqrt(bin_count+1))),
                         np.log10(surface_density*(1-1/np.sqrt(bin_count+1))),
                         alpha=0.5)

  ax.vlines(half_mass_radii, 5,9, ls=':', color=bar[0].get_color())

  ax.set_xlabel(r'R [kpc]')
  ax.set_ylabel(r'log $\Sigma$ [$M_\odot$/kpc$^2$]')

  ax.set_xticks([0,5,10,15,20,30,40,50])
  ax.set_xticklabels([0,5,10,15,'','','',''])
  ax.set_yticks([5,6,7,8,9])
  ax.set_yticklabels(['',6,7,8,''])
  ax.set_xlim((0,20))
  ax.set_ylim((5,9))

  # ax.text(2.5, 5.5, '$t=4.9$ Gyr')

  plt.show() if save:
    fname = '../galaxies/' + name + '/results/density_profile_' + snap
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  return

def plot_dispersions(PosStars, VelStars, j_z, j_p, j_c,
                     j_c_interp, R_bins,
                     name='', snap='', save=False, ax=None):
  '''Not kept up
  '''
  rho, varphi, z, v_rho, v_varphi, v_z = get_cylindrical(PosStars, VelStars)
  j_zonc = j_z/j_c
  # j_zonc = np.sqrt(j_z**2 + j_p**2)/j_c


  n=10
  bin_edges = np.arange(n+1)
  bin_centres = 0.5 * (bin_edges[:-1] + bin_edges[1:])
  bin_mask  = np.digitize(rho, bin_edges)

  disp_v_rho    = np.zeros(n)
  disp_v_varphi = np.zeros(n)
  disp_v_z      = np.zeros(n)

  disp_v_x = np.zeros(n)
  disp_v_y = np.zeros(n)

  bin_j_zonc    = np.zeros(n)

  interp = InterpolatedUnivariateSpline(R_bins, j_c_interp/R_bins, k=1)
  v_circ = interp(bin_centres)

  for i in range(n):

    mask = bin_mask == i

    disp_v_rho[i]    = np.std(v_rho[mask])
    disp_v_varphi[i] = np.std(v_varphi[mask])
    disp_v_z[i]      = np.std(v_z[mask])

    disp_v_x[i]      = np.std(VelStars[mask,0])
    disp_v_y[i]      = np.std(VelStars[mask,1])

    bin_j_zonc[i] = np.mean(j_zonc[mask])

  plt.figure()
  plt.errorbar(bin_centres, disp_v_rho,    fmt='*', ls='-.', c='C0',
               label=r'$\sigma_{v_\rho}$')
  plt.errorbar(bin_centres, disp_v_varphi, fmt='o', ls='--', c='C1',
               label=r'$\sigma_{v_\varphi}$')
  plt.errorbar(bin_centres, disp_v_z,      fmt='x', ls='-',  c='C2',
               label=r'$\sigma_{v_z}$')

  plt.errorbar(bin_centres, disp_v_x,      fmt='+', ls=':',  c='C3',
               label=r'$\sigma_{v_x}$')
  plt.errorbar(bin_centres, disp_v_y,      fmt='s', ls=':',  c='C4',
               label=r'$\sigma_{v_y}$')

  plt.xlabel(r'$\rho$ [kpc]')
  plt.ylabel(r'$\sigma_X$ [km/s]')
  # plt.xlim((0,1.1))
  plt.legend()

  plt.figure()
  plt.errorbar(bin_j_zonc, disp_v_rho/v_circ,    fmt='*', ls='-.', c='C0',
               label=r'$\sigma_{v_\rho}$')
  plt.errorbar(bin_j_zonc, disp_v_varphi/v_circ, fmt='o', ls='--', c='C1',
               label=r'$\sigma_{v_\varphi}$')
  plt.errorbar(bin_j_zonc, disp_v_z/v_circ,      fmt='x', ls='-',  c='C2',
               label=r'$\sigma_{v_z}$')

  plt.errorbar(bin_j_zonc, disp_v_x/v_circ,      fmt='+', ls=':',  c='C3',
                label=r'$\sigma_{v_x}$')
  plt.errorbar(bin_j_zonc, disp_v_y/v_circ,      fmt='s', ls=':',  c='C4',
                label=r'$\sigma_{v_y}$')

  e = np.linspace(0, 1)
  plt.errorbar(e, np.sqrt(1-e))

  plt.xlabel(r'$\langle j_z / j_c \rangle$')
  plt.ylabel(r'$\sigma_X/v_c$ [km/s]')
  plt.xlim((0,1.2))
  plt.ylim((0,1.2))
  plt.legend()

  return

def plot_stellar_exponential(PosStars, MassStar=1, ax=None):
  '''
  '''
  R = np.linalg.norm((PosStars[:,0], PosStars[:,1]), axis=0)
  #get bins
  n_per_bin = np.sqrt(len(R))*2
  bins = get_equal_number_bins(R, n_per_bin) # * u.kpc

  # bins = np.arange(0,50,20/np.sqrt(len(R)))

  #calculate surface density
  bin_count, _ = np.histogram(R, bins=bins)
  bin_area = np.pi * (np.roll(bins, -1)**2 - bins**2)[:-1]
  #maybe should be mean of points in each bin
  bin_R = 0.5 * (bins[:-1] + bins[1:])
  surface_density = MassStar * bin_count / bin_area * 10**10 # M_sun / kpc^2

  R_sort = np.argsort(R)
  if len(R)%2 == 0:
    half_mass_radii = (R[R_sort][len(R)>>1] + R[R_sort][(len(R)>>1)-1])/2
  else:
    half_mass_radii = R[R_sort][int(len(R)/2)]

  if ax == None:
    fig, ax = plt.subplots()

  bar = ax.errorbar(bin_R, np.log10(surface_density), ls='-', fmt='', alpha=0.5,
                    yerr=np.array((np.log10(1+1/np.sqrt(bin_count+1)),
                                   np.log10(1-1/np.sqrt(bin_count+1)))))
  ax.fill_between(bin_R, np.log10(surface_density*(1+1/np.sqrt(bin_count+1))),
                         np.log10(surface_density*(1-1/np.sqrt(bin_count+1))),
                         alpha=0.5)

  #work out best fit
  likely = lambda R_d : -np.sum( np.log( R / np.square(R_d) * np.exp( -R / R_d ) ))

  R_d = minimize(likely, 1)
  R_d = R_d.x[0]

  intercept = lambda i_0 : np.sum(np.square(i_0 * np.exp(-bin_R/R_d) -
                                            surface_density))
  i_0 = minimize(intercept, surface_density[0])
  i_0 = i_0.x[0]

  plt.errorbar(bin_R, np.log10(i_0 * np.exp(-bin_R/R_d)) )

  print(R_d)

  print(half_mass_radii)
  # median = R_d * 1.6783469900166605
  median = - R_d * (lambertw(-1/2*np.exp(-1), -1).real + 1)
  print(median)

  print(R_d * np.log(2))

  dens = lambda R : R * np.exp( - R / R_d ) / R_d**2
  # print(quad(dens, 0, median)[0])
  # print(quad(dens, median, 100)[0])

  plt.vlines(half_mass_radii, np.log10(i_0), np.amin(np.log10(surface_density)))
  plt.vlines(median, np.log10(i_0), np.amin(np.log10(surface_density)))

  return

def plot_dispersion_inclination(PosStars, VelStars, j_z_stars, j_c_stars,
                                j_circ_interp, R_interp,
                                name='', snap='', save=False, ax=None):
  '''
  '''
  j_stars = np.cross(PosStars, VelStars)
  r = np.linalg.norm(PosStars, axis=1)

  #inclination
  cosi = j_stars[:,2] / np.linalg.norm(j_stars, axis=1)
  inclination = np.arccos(cosi)

  #coords and stuff
  (rho, varphi, z, v_rho, v_varphi, v_z) = get_cylindrical(PosStars, VelStars)
  epsilon = np.linalg.norm(j_stars, axis=1) / j_c_stars

  #circular velocity
  v_c_interp = j_circ_interp / R_interp
  interp = InterpolatedUnivariateSpline(R_interp, v_c_interp, k=1, ext='const')
  # v_c_stars = interp(rho)
  v_c_stars = interp(r)
  V_c2 = np.mean(v_c_stars)**2

  incl_edges = np.linspace(0, np.pi, 41)
  incls = 90 - (incl_edges[1:] + incl_edges[:-1]) / 2 * 180 / np.pi

  #which inclination bin each particle is in
  belongs_in = np.digitize(inclination, incl_edges)

  sigma_vz2 = np.zeros(len(incls))
  sigma_vR2 = np.zeros(len(incls))
  sigma_vphi2 = np.zeros(len(incls))

  ellipticity = np.zeros(len(incls))

  for i in range(len(incls)):
    mask = (belongs_in == i+1)

    #only do points with enough points
    if sum(mask) > 10:
      V_c2 = np.mean(v_c_stars[mask])**2
    else:
      V_c2 = np.nan

    sigma_vz2[i] = np.var(v_z[mask]) / V_c2
    sigma_vR2[i] = np.var(v_rho[mask]) / V_c2
    sigma_vphi2[i] = np.var(v_varphi[mask]) / V_c2

    ellipticity[i] = np.mean(epsilon[mask])

  #   plt.figure()
  #   plt.hist(r[mask])
    # plt.figure()
    # plt.hist(v_varphi[mask])

  # plt.figure()
  # plt.hist(v_varphi)
  # plt.figure()
  # plt.hist(v_varphi - v_c_stars)

  # V_c2 = np.mean(v_varphi)**2
  # sigma_vz2   /= V_c2
  # sigma_vR2   /= V_c2
  # sigma_vphi2 /= V_c2

  V_c2 = np.mean(v_c_stars)**2

  global_sigma_vz2 = [np.var(v_z) / V_c2]
  global_sigma_vR2 = [np.var(v_rho) / V_c2]
  global_sigma_vphi2 = [np.var(v_varphi) / V_c2]

  global_ellipticity = [np.median(epsilon)]
  # global_incl = [np.mean(np.abs(np.pi/4 - inclination))*180/np.pi]
  global_incl = [90 - np.median(inclination) * 180 / np.pi]

  #plot
  fig, ax = plt.subplots(nrows=4, ncols=1, sharex=True, sharey=False,
                         figsize=(7,10))
  fig.subplots_adjust(hspace=0.03,wspace=0)
  (ax[3], ax[0], ax[1], ax[2]) = (ax[0], ax[1], ax[2], ax[3])

  #load theory values
  disp_z_calc   = np.load('../../test-galaxies/galaxies/hernquist_i_z_logra-1.npy')
  disp_r_calc   = np.load('../../test-galaxies/galaxies/hernquist_i_r_logra-1.npy')
  disp_phi_calc = np.load('../../test-galaxies/galaxies/hernquist_i_phi_logra-1.npy')
  #fix because the integral messes up at 0
  for j in range(11):
    disp_z_calc[-1,j] = 2*disp_z_calc[-2,j] - disp_z_calc[-3,j]
    disp_r_calc[-1,j] = 2*disp_r_calc[-2,j] - disp_r_calc[-3,j]
  #axes linspaces
  theory_incls = np.linspace(90,0,11)
  cols = viridis(np.linspace(1, 0, 11))
  #plot theory
  for i in range(11):
    ax[0].errorbar(theory_incls, disp_z_calc[i,:], fmt='', ls='-', c=cols[i])
    ax[1].errorbar(theory_incls, disp_r_calc[i,:], fmt='', ls='-', c=cols[i])
    ax[2].errorbar(theory_incls, disp_phi_calc[i,:], fmt='', ls='-', c=cols[i])
    ax[0].errorbar(-theory_incls, disp_z_calc[i,:], fmt='', ls='-', c=cols[i])
    ax[1].errorbar(-theory_incls, disp_r_calc[i,:], fmt='', ls='-', c=cols[i])
    ax[2].errorbar(-theory_incls, disp_phi_calc[i,:], fmt='', ls='-', c=cols[i])

  ax[0].text(90,1,'Prograde \nDisk Orbit',
             rotation='vertical', ha='right',  va='top', ma='left')
  ax[0].text(0, 1,'Polar \nOrbit',
             rotation='vertical', ha='center', va='top', ma='left')
  ax[0].text(-90,1,'Retrograde \nDisk Orbit',
             rotation='vertical', ha='left',   va='top', ma='left')

  #plot data
  im=ax[0].scatter(incls, sigma_vz2, c=ellipticity, vmin=0, vmax=1,
                   edgecolors='k', zorder=100)
  ax[1].scatter(incls, sigma_vR2,    c=ellipticity, vmin=0, vmax=1,
                   edgecolors='k', zorder=100)
  ax[2].scatter(incls, sigma_vphi2,  c=ellipticity, vmin=0, vmax=1,
                   edgecolors='k', zorder=100)

  #global
  ax[0].scatter(global_incl, global_sigma_vz2, c=global_ellipticity, linewidths=3,
                vmin=0, vmax=1, edgecolors='k', zorder=101, s=200, marker='X')
  ax[1].scatter(global_incl, global_sigma_vR2, c=global_ellipticity, linewidths=3,
                vmin=0, vmax=1, edgecolors='k', zorder=101, s=200, marker='X')
  ax[2].scatter(global_incl, global_sigma_vphi2, c=global_ellipticity, linewidths=3,
                vmin=0, vmax=1, edgecolors='k', zorder=101, s=200, marker='X')

  ax[3].hist(90 - inclination*180/np.pi, bins=incl_edges*180/np.pi-90,
             color='k', alpha=0.5)

  ax[2].set_xlabel(r'Inclination [$^\circ$]')
  ax[0].set_ylabel(r'$\sigma_{v_z}^2/v_c^2$')
  ax[1].set_ylabel(r'$\sigma_{v_R}^2/v_c^2$')
  ax[2].set_ylabel(r'$\sigma_{v_\phi}^2/v_c^2$')
  ax[3].set_ylabel(r'$N$')

  ax[0].set_xlim((-90,90))
  ax[0].set_ylim((0,1))
  ax[1].set_ylim((0,1))
  ax[2].set_ylim((0,1))
  ax[0].grid()
  ax[1].grid()
  ax[2].grid()
  ax[3].grid()

  fig.subplots_adjust(right=0.8)
  cbar_ax = fig.add_axes([0.81, 0.11, 0.04, 0.77])
  cbar = fig.colorbar(im, cax=cbar_ax)
  cbar.set_label(r'$|\vec{j}| / j_c(E)$')

  plt.show() if save:
    fname = '../galaxies/' + name + '/results/inclination_dispersion_' + snap
    plt.savefig(fname, bbox_inches='tight')
    # plt.close()

  return

'''
###############################################################################
################ make all single galaxy, single snapshot plots ################
###############################################################################
'''
#TODO update
def plot_all(time, save=True):
  '''Need to update
  '''
  muse  = ['25', '5', '1']
  Mdm   = ['8p0', '7p5', '7p0', '6p5', '6p0']
  snaps = [ 400,   400,   101,   101,   101]

  n_mu = len(muse)
  n_M  = len(Mdm)

  f_array = [[], [], []]

  for i in range(n_mu):
    for j in range(n_M):

      #skip the loner
      if not((muse[i] == '1') and (Mdm[j] == '6p0')):

        #0.9, 0.8, 0.7, 0, n
        f_array[0].append([[], [], [], [], []])
        f_array[1].append([[], [], [], [], []])
        f_array[2].append([[], [], [], [], []])

        name = 'mu_{0}/fdisk_0p01_lgMdm_{1}_V200-200kmps'.format(
          muse[i], Mdm[j])
        snap = '{0:03d}'.format(int(time * snaps[j]/10))
        print('\n' + name + '_snap_' + snap)

        (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,
                  MassStar, PosStars, VelStars, IDStars,
                  j_z_stars, j_p_stars, j_c_stars, energy_stars,
                  E_interp, j_circ_interp, R_interp
                  ) = load_nbody.load_and_calculate(name, snap)

        plot_e_jz(energy_stars, j_z_stars, E_interp, j_circ_interp,
                  name, snap, save)

        plot_jz_r(PosStars, j_z_stars, R_interp, j_circ_interp,
                  name, snap, save)

        plot_jzonc(j_z_stars, j_c_stars, MassStar,
                    name, snap, save)

        plot_projection(PosStars,
                        name, snap, save)

        plot_jzonc_r(PosStars, j_z_stars, j_c_stars,
                     name, snap, save)

  return

'''
###############################################################################
######################## all galaxies, single snapshot ########################
###############################################################################
'''
#TODO make generic function work for plots vs time / plots that load data
def plot_single_snapshot_all_galaxies_generic(plot_function, plot_name, time=0,
                                              figsize=(10,15), save=False,
                                              axs=None):
  '''
  '''
  #sims to look at
  muse  = ['1', '5', '25']
  Mdm   = ['8p0', '7p5', '7p0', '6p5', '6p0']
  Mdmstr= ['8.0', '7.5', '7.0', '6.5', '6.0']
  snaps = [ 400,   400,   101,   101,   101]
  n_mu = len(muse)
  n_M  = len(Mdm)
  # t_mult = 3.086e21 / 1e5 / (1e9 * 365.25 * 24 * 3600)

  if np.all(axs == None):
    #set up plots
    fig, axs = plt.subplots(nrows=n_M, ncols=n_mu, sharex=True, sharey=True,
                            figsize=figsize)
    fig.subplots_adjust(hspace=0.03,wspace=0.03)

  for j in range(n_mu):
    for i in range(n_M):

      #skip the loner
      if not((muse[j] == '1') and (Mdm[i] == '6p0')):
      # if (Mdm[i] != '6p0') and (Mdm[i] != '6p5'):

        #pick the right file to load
        name = 'mu_{0}/fdisk_0p01_lgMdm_{1}_V200-200kmps'.format(
          muse[j], Mdm[i])
        snap = '{0:03d}'.format(int(time/10 * snaps[i]))
        if snap == '400':
          #the potential of snap 400 is briken
          snap = '399'
        print('\n' + name + '_snap_' + snap)

        #select inputs
        loaded_strings = ['', 'MassDM',   'PosDMs',   'VelDMs',   'IDDMs',
                              'MassStar', 'PosStars', 'VelStars', 'IDStars',
                          'j_z_stars', 'j_p_stars', 'j_c_stars', 'energy_stars',
                          'E_interp', 'j_circ_interp', 'R_interp']

        inputs = inspect.getfullargspec(plot_function)[0]

        #check if inputs are needed
        need_to_load = False
        for put in inputs:
          for string in loaded_strings:
            if string == put:
              need_to_load = True

        #only load if needed
        if need_to_load:
          #load file
          loaded_values = load_nbody.load_and_calculate(name, snap)

        plot_args = []
        for put in inputs:
          for value, string in zip(loaded_values, loaded_strings):
            if string == put:
              plot_args.append(value)

        #plot function
        plot_function(*plot_args, ax=axs[i,j])

        #remove lables
        xlabel = axs[i,j].get_xlabel()
        ylabel = axs[i,j].get_ylabel()
        axs[i,j].set_xlabel('')
        axs[i,j].set_ylabel('')

        #add labels
        if i == 0:
          axs[i,j].set_title(r'$\mu = $'+muse[j])
        if j == n_mu-1:
          axs[i,j].yaxis.set_label_position('right')
          axs[i,j].set_ylabel(r'$M_{DM} = $' + Mdmstr[i])
        #remove ticks
        if i != n_M-1:
          axs[i,j].tick_params(labelbottom=False)
        if j != 0:
          axs[i,j].tick_params(labelleft=False)
        #remove legend
        if i != 0 or j != 0:
          legend = axs[i,j].get_legend()
          if legend != None:
            legend.remove()
      #remove loner
      else:
        axs[i,j].set_visible(False)

  axs[3,0].tick_params(labelbottom=True)
  axs[4,1].set_xlabel(xlabel)
  axs[2,0].set_ylabel(ylabel)
  # axs[0,0].legend()

  # if colorbar:
  #   fig.subplots_adjust(right=0.8)
  #   cbar_ax = fig.add_axes([0.81, 0.11, 0.04, 0.77])
  #   cbar = fig.colorbar(cax=cbar_ax)
  #   cbar.set_label(r'$\log ( M / 10^{10} M_\odot )$')

  plt.show() if save:
    fname = '../galaxies/' + plot_name + '_' + str(time)
    plt.savefig(fname, bbox_inches='tight')#, dpi=200)
    plt.close()

  return(axs)

'''
###############################################################################
######################## single galaxy, all snapshots #########################
###############################################################################
'''
#TODO write better
def plot_histograms_vs_time(name, save=True):
  '''
  '''
  t_tot = 9.778943899409334 #Gyr

  if (('7p5' in name) or ('8p0' in name)):
    nsnaps = 401
  else:
    nsnaps = 102

  j_zonc_heights = np.zeros((60, nsnaps))
  j_onc_heights  = np.zeros((60, nsnaps))
  j_zon_heights  = np.zeros((60, nsnaps))
  incln_heights  = np.zeros((60, nsnaps))

  j_zonc_median = np.zeros(nsnaps)
  j_onc_median  = np.zeros(nsnaps)
  j_zon_median  = np.zeros(nsnaps)
  incln_median  = np.zeros(nsnaps)

  for i in range(nsnaps):
    snap = '{0:03d}'.format(int(i))
    (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,
                MassStar, PosStars, VelStars, IDStars,
      j_z_stars, j_p_stars, j_c_stars, energy_stars,
      E_interp, j_circ_interp, R_interp
      ) = load_nbody.load_and_calculate(name, snap)

    n=len(j_z_stars)
    weights = MassStar*np.ones(n)

    j_vec = np.cross(PosStars, VelStars)
    j_abs = np.linalg.norm(j_vec, axis=1)

    j_zonc = j_z_stars / j_c_stars
    j_onc  = j_abs     / j_c_stars
    j_zon  = j_z_stars / j_abs

    out, _ = np.histogram(j_zonc, bins=np.linspace(-1.5,1.5,61),
                          weights=weights)
    j_zonc_heights[:,i] = np.flip(np.log10(out+MassStar))
    j_zonc_median[i]    = np.median(j_zonc)

    out, _ = np.histogram(j_onc,  bins=np.linspace(0,1.5,61),
                          weights=weights)
    j_onc_heights[:,i] = np.flip(np.log10(out+MassStar))
    j_onc_median[i]    = np.median(j_onc)

    out, _ = np.histogram(j_zon, bins=np.linspace(-1,1,61),
                          weights=weights)
    j_zon_heights[:,i] = np.flip(np.log10(out+MassStar))
    j_zon_median[i]    = np.median(j_zon)

    # inclination = np.arccos(np.dot(j_vec, np.array([0,0,1])) / j_abs
    #                         ) /np.pi*180
    inclination = np.arccos(j_z_stars / j_abs) /np.pi*180
    out, _ = np.histogram(inclination, bins=np.linspace(0,180,61),
                          weights=weights)

    incln_heights[:,i] = np.flip(np.log10(out+MassStar))
    incln_median[i]    = np.median(inclination)

  plt.figure()
  plt.imshow(j_zonc_heights, extent=(0, t_tot, -1.5, 1.5), aspect='auto')
  cbar = plt.colorbar()
  cbar.set_label(r'$\log ( M / 10^{10} M_\odot )$')
  plt.errorbar(np.linspace(0, t_tot, nsnaps), j_zonc_median,
               fmt='', ls='-', c='r', alpha=0.5)
  plt.xlabel(r'time [Gyr]')
  plt.ylabel(r'$j_z/j_c(E)$')
  plt.show() if save:
    fname = '../galaxies/' + name + '/results/jzonc_t'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  plt.figure()
  plt.imshow(j_onc_heights, extent=(0, t_tot, 0, 1.5), aspect='auto')
  cbar = plt.colorbar()
  cbar.set_label(r'$\log ( M / 10^{10} M_\odot )$')
  plt.errorbar(np.linspace(0, t_tot, nsnaps), j_onc_median,
               fmt='', ls='-', c='r', alpha=0.5)
  plt.xlabel(r'time [Gyr]')
  plt.ylabel(r'$|j|/j_c(E)$')
  plt.show() if save:
    fname = '../galaxies/' + name + '/results/jonc_t'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  plt.figure()
  plt.imshow(j_zon_heights, extent=(0, t_tot, -1, 1), aspect='auto')
  cbar = plt.colorbar()
  cbar.set_label(r'$\log ( M / 10^{10} M_\odot )$')
  plt.errorbar(np.linspace(0, t_tot, nsnaps), j_zon_median,
               fmt='', ls='-', c='r', alpha=0.5)
  plt.xlabel(r'time [Gyr]')
  plt.ylabel(r'$j_z/|j|$')
  plt.show() if save:
    fname = '../galaxies/' + name + '/results/jzon_t'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  plt.figure()
  plt.imshow(incln_heights, extent=(0, t_tot, 0, 180), aspect='auto')
  cbar = plt.colorbar()
  cbar.set_label(r'$\log ( M / 10^{10} M_\odot )$')
  plt.errorbar(np.linspace(0, t_tot, nsnaps), incln_median,
               fmt='', ls='-', c='r', alpha=0.5)
  plt.xlabel(r'time [Gyr]')
  plt.ylabel(r'$|i|$ [deg]')
  plt.yticks(ticks=[0,45,90,135,180])
  plt.show() if save:
    fname = '../galaxies/' + name + '/results/inclination_t'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  return

def plot_circular_velocity_histograms_vs_time(name, save=True):
  '''
  '''
  t_tot = 9.778943899409334 #Gyr

  if (('7p5' in name) or ('8p0' in name)):
    nsnaps = 401
  else:
    nsnaps = 102

  vphi_heights = np.zeros((60, nsnaps))

  vphi_mean = np.zeros(nsnaps)
  vphi_std  = np.zeros(nsnaps)
  v_c       = np.zeros(nsnaps)
  v_dm      = np.zeros(nsnaps)

  r_half = 7

  for i in range(nsnaps):
    snap = '{0:03d}'.format(int(i))
    (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,   PotDMs,
                MassStar, PosStars, VelStars, IDStars, PotStars
                ) = load_nbody.load_and_align(name, snap)

    rStars = np.linalg.norm(PosStars, axis=1)
    rDMs   = np.linalg.norm(PosDMs,   axis=1)

    #circular velocity
    #mass enclosed
    n_stars_less = np.sum(rStars < r_half)
    n_dms_less   = np.sum(rDMs   < r_half)
    mass_enclosed = MassStar * n_stars_less + MassDM * (n_dms_less + 1)

    #uses spherical mass distribution approximation
    v_circ_shell  = np.sqrt(grav_const * mass_enclosed / r_half)

    #get bin around r_half
    p = 0.5
    rmin = (10**np.log10(r_half-p))
    rmax = (10**np.log10(r_half+p))

    sm = np.logical_and((rStars > rmin), (rStars < rmax))
    dm = np.logical_and((rDMs > rmin),   (rDMs < rmax))

    #circular velocities
    vphi_stars = (PosStars[sm,0] * VelStars[sm,1] - PosStars[sm,1] * VelStars[sm,0]
                  ) / np.sqrt(PosStars[sm,0]**2 + PosStars[sm,1]**2)
    # vphi_DMs   = (PosDMs[dm,0] * VelDMs[dm,1] - PosDMs[dm,1] * VelDMs[dm,0]
    #               ) / np.sqrt(PosDMs[dm,0]**2 + PosDMs[dm,1]**2)
    vDMs = np.linalg.norm(VelDMs[dm], axis=1)

    out, _ = np.histogram(vphi_stars, bins=np.linspace(-200,400,61))
    vphi_heights[:,i] = np.flip(np.log10(out*MassStar+MassStar))
    # vphi_heights[:,i] = np.flip(out * MassStar)

    vphi_mean[i] = np.mean(vphi_stars)
    vphi_std[i]  = np.std(vphi_stars)
    v_c[i]       = v_circ_shell
    v_dm[i]      = np.mean(vDMs)

  plt.figure()
  plt.imshow(vphi_heights, extent=(0, t_tot, -200, 400), aspect='auto')
  cbar = plt.colorbar()
  cbar.set_label(r'$\log ( M / 10^{10} M_\odot )$')
  plt.errorbar(np.linspace(0, t_tot, nsnaps), vphi_mean, fmt='', ls='-', c='C1',
               label=r'$\overline{v_{\phi,*}}$')
  plt.errorbar(np.linspace(0, t_tot, nsnaps), vphi_mean+vphi_std,
               fmt='', ls='--', c='C1',
               label=r'$\overline{v_{\phi,*}} \pm \sigma_{\phi}$')
  plt.errorbar(np.linspace(0, t_tot, nsnaps), vphi_mean-vphi_std,
               fmt='', ls='--', c='C1')
  plt.errorbar(np.linspace(0, t_tot, nsnaps), v_c,       fmt='', ls='-', c='C3',
               label=r'$v_c$')
  plt.errorbar(np.linspace(0, t_tot, nsnaps), v_dm,      fmt='', ls='-', c='C6',
               label=r'$\overline{|v_{DM}|}$')
  plt.xlabel(r'time [Gyr]')
  plt.ylabel(r'$v_\phi$')
  plt.legend()
  plt.show() if save:
    fname = '../galaxies/' + name + '/results/vphi_t'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  return

def plot_individual_orbits(name, save=True):
  '''
  '''
  t_tot = 9.778943899409334 #Gyr

  fname = '../galaxies/' + name + '/results/individual_orbits/'
  try:
    os.mkdir(fname)
  except:
    print('Already made folder')

  if (('7p5' in name) or ('8p0' in name)):
    nsnaps = 401
  else:
    nsnaps = 102

  (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,
              MassStar, PosStars, VelStars, IDStars,
    j_z_stars, j_p_stars, j_c_stars, energy_stars,
    E_interp, j_circ_interp, R_interp
    ) = load_nbody.load_and_calculate(name, '000')

  #just put stars in argsort order
  n_stars  = len(IDStars)

  x_value      = np.zeros((nsnaps, n_stars))
  y_value      = np.zeros((nsnaps, n_stars))
  z_value      = np.zeros((nsnaps, n_stars))
  R_value      = np.zeros((nsnaps, n_stars))
  phi_value    = np.zeros((nsnaps, n_stars))

  v_R_value    = np.zeros((nsnaps, n_stars))
  v_phi_value  = np.zeros((nsnaps, n_stars))
  v_z_value    = np.zeros((nsnaps, n_stars))

  j_zonc_value = np.zeros((nsnaps, n_stars))

  for i in range(nsnaps):
    snap = '{0:03d}'.format(int(i))
    (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,
                MassStar, PosStars, VelStars, IDStars,
      j_z_stars, j_p_stars, j_c_stars, energy_stars,
      E_interp, j_circ_interp, R_interp
      ) = load_nbody.load_and_calculate(name, snap)

    #get order
    order_mask = np.argsort(IDStars)

    #calculate some stuff and order everything correctly
    j_z_stars = j_z_stars[order_mask]
    j_c_stars = j_c_stars[order_mask]
    PosStars  = PosStars[order_mask]
    VelStars  = VelStars[order_mask]

    #add values to arrays

    x_value[i,:] = PosStars[:,0]
    y_value[i,:] = PosStars[:,1]
    z_value[i,:] = PosStars[:,2]

    R_value[i,:]   = np.sqrt(PosStars[:,0]**2 + PosStars[:,1]**2)
    phi_value[i,:] = np.arctan2(PosStars[:,1], PosStars[:,0])


    v_R_value[i,:]   = VelStars[:,0] * np.cos(phi_value[i,:]
                      ) + VelStars[:,1] * np.sin(phi_value[i,:])
    v_phi_value[i,:] = -VelStars[:,0]* np.sin(phi_value[i,:]
                      ) + VelStars[:,1] * np.cos(phi_value[i,:])
    v_z_value[i,:]   = VelStars[:,2]

    j_zonc_value[i,:] = j_z_stars / j_c_stars

  IDStars = IDStars[order_mask]

  for j in range(n_stars):

    fig, ax = plt.subplots(nrows=2, ncols=2,# sharex=True, sharey=True,
                           figsize=(9.125, 7.2))
    fig.subplots_adjust(hspace=0,wspace=0)

    t = np.linspace(0,t_tot,nsnaps)

    ax[0,0].errorbar(x_value[:,j], y_value[:,j], c='k', zorder=-1000, alpha=0.2)
    im = ax[0,0].scatter(x_value[:,j], y_value[:,j], s=5,
                         # c=j_zonc_value[:,j], cmap='twilight', vmin=-1, vmax=1)
                          c=t)
    ax[1,0].errorbar(x_value[:,j], z_value[:,j], c='k', zorder=-1000, alpha=0.2)
    ax[1,0].scatter(x_value[:,j], z_value[:,j], s=5,
                    # c=j_zonc_value[:,j], cmap='twilight', vmin=-1, vmax=1)
                    c=t)
    ax[0,1].errorbar(z_value[:,j], y_value[:,j], c='k', zorder=-1000, alpha=0.2)
    ax[0,1].scatter(z_value[:,j], y_value[:,j], s=5,
                    # c=j_zonc_value[:,j], cmap='twilight', vmin=-1, vmax=1)
                    c=t)

    axins = inset_axes(ax[1,1], width="100%", height="100%", loc=10)

    axins.errorbar(t, x_value[:,j], label='x', alpha=0.5)
    axins.errorbar(t, y_value[:,j], label='y', alpha=0.5)
    axins.errorbar(t, z_value[:,j], label='z', alpha=0.5)
    axins.legend(loc='upper left', prop={'size': 12})

    maximum = np.max((np.abs(x_value[:,j]),np.abs(y_value[:,j]),
                      np.abs(z_value[:,j])))

    xlim=[-maximum,maximum]
    ax[0,0].set_xlim(xlim)
    ax[0,0].set_ylim(xlim)
    ax[1,0].set_xlim(xlim)
    ax[1,0].set_ylim(xlim)
    ax[0,1].set_xlim(xlim)
    ax[0,1].set_ylim(xlim)
    ax[0,0].set_aspect('equal')
    ax[1,0].set_aspect('equal')
    ax[0,1].set_aspect('equal')
    ax[0,0].grid()
    ax[1,0].grid()
    ax[0,1].grid()

    axins.set_xlim((0, t_tot))
    axins.set_ylim(xlim)
    axins.grid()

    ax[0,0].set_ylabel('y [kpc]')
    ax[1,0].set_ylabel('z [kpc]')
    ax[1,0].set_xlabel('x [kpc]')
    axins.set_xlabel('t [Gyr]')

    ax[0,0].set_xticklabels([])
    ax[0,1].set_xticklabels([])
    ax[0,1].set_yticklabels([])
    ax[1,1].set_xticklabels([])
    ax[1,1].set_yticklabels([])
    axins.set_yticklabels([])

    fig.subplots_adjust(right=0.7325)
    cbar_ax = fig.add_axes([0.76, 0.11, 0.04, 0.77])
    cbar = fig.colorbar(im, cax=cbar_ax)
    # cbar.set_label(r'$j_z / j_c (E)$')
    cbar.set_label(r't [Gyr]')

    plt.show() if save:
      plt.savefig(fname + str(IDStars[j]), bbox_inches='tight')
      plt.close()

    fig, ax = plt.subplots(nrows=2, ncols=2,# sharex=True, sharey=True,
                           figsize=(9.125, 7.2))
    fig.subplots_adjust(hspace=0,wspace=0)

    t = np.linspace(0,t_tot,nsnaps)

    ax[0,0].errorbar(v_R_value[:,j], v_phi_value[:,j],
                     c='k', zorder=-1000, alpha=0.2)
    im = ax[0,0].scatter(v_R_value[:,j], v_phi_value[:,j], s=5, c=t)
    ax[1,0].errorbar(v_R_value[:,j], v_z_value[:,j],
                     c='k', zorder=-1000, alpha=0.2)
    ax[1,0].scatter(v_R_value[:,j], v_z_value[:,j], s=5, c=t)
    ax[0,1].errorbar(v_z_value[:,j], v_phi_value[:,j],
                     c='k', zorder=-1000, alpha=0.2)
    ax[0,1].scatter(v_z_value[:,j], v_phi_value[:,j], s=5, c=t)

    axins = inset_axes(ax[1,1], width="100%", height="100%", loc=10)

    axins.errorbar(t, v_R_value[:,j],   label=r'$v_R$',    alpha=0.5)
    axins.errorbar(t, v_phi_value[:,j], label=r'$v_\phi$', alpha=0.5)
    axins.errorbar(t, v_z_value[:,j],   label=r'$v_z$',    alpha=0.5)
    axins.legend(loc='lower left', prop={'size': 12})

    maximum = np.max((np.abs(v_R_value[:,j]),np.abs(v_phi_value[:,j]),
                      np.abs(v_z_value[:,j])))

    xlim=[-maximum,maximum]
    ax[0,0].set_xlim(xlim)
    ax[0,0].set_ylim(xlim)
    ax[1,0].set_xlim(xlim)
    ax[1,0].set_ylim(xlim)
    ax[0,1].set_xlim(xlim)
    ax[0,1].set_ylim(xlim)
    ax[0,0].set_aspect('equal')
    ax[1,0].set_aspect('equal')
    ax[0,1].set_aspect('equal')
    ax[0,0].grid()
    ax[1,0].grid()
    ax[0,1].grid()

    axins.set_xlim((0, t_tot))
    axins.set_ylim(xlim)
    axins.grid()

    ax[0,0].set_ylabel(r'$v_\phi$ [km/s]')
    ax[1,0].set_ylabel(r'$v_z$ [km/s]')
    ax[1,0].set_xlabel(r'$v_R$ [km/s]')
    axins.set_xlabel('t [Gyr]')

    ax[0,0].set_xticklabels([])
    ax[0,1].set_xticklabels([])
    ax[0,1].set_yticklabels([])
    ax[1,1].set_xticklabels([])
    ax[1,1].set_yticklabels([])
    axins.set_yticklabels([])

    fig.subplots_adjust(right=0.7325)
    cbar_ax = fig.add_axes([0.76, 0.11, 0.04, 0.77])
    cbar = fig.colorbar(im, cax=cbar_ax)
    cbar.set_label(r't [Gyr]')

    plt.show() if save:
      plt.savefig(fname + str(IDStars[j]) + '_v_cyl', bbox_inches='tight')
      plt.close()

    fig, ax = plt.subplots(nrows=1, ncols=2,# sharex=True, sharey=True,
                           figsize=(7, 5.48))
    fig.subplots_adjust(hspace=0,wspace=0)

    t = np.linspace(0,t_tot,nsnaps)

    ax[0].errorbar(R_value[:,j], z_value[:,j],
                   c='k', zorder=-1000, alpha=0.2)
    im = ax[0].scatter(R_value[:,j], z_value[:,j], s=5, c=t)

    axins = inset_axes(ax[1], width="100%", height="100%", loc=10)

    axins.errorbar(t, R_value[:,j], label=r'$R$', alpha=0.5)
    axins.errorbar(t, z_value[:,j], label=r'$z$', alpha=0.5)
    axins.legend(loc='lower left', prop={'size': 12})

    maximum = np.max((np.abs(R_value[:,j]),np.abs(z_value[:,j])))

    xlim=[-maximum,maximum]
    rlim=[0, maximum]
    ax[0].set_xlim(rlim)
    ax[0].set_ylim(xlim)
    ax[0].set_aspect('equal')
    ax[0].grid()

    axins.set_xlim((0, t_tot))
    axins.set_ylim(xlim)
    axins.grid()

    ax[0].set_xlabel(r'$R$ [kpc]')
    ax[0].set_ylabel(r'$z$ [kpc]')
    axins.set_xlabel('t [Gyr]')

    ax[1].set_xticklabels([])
    ax[1].set_yticklabels([])
    axins.set_yticklabels([])

    fig.subplots_adjust(right=0.7325)
    cbar_ax = fig.add_axes([0.76, 0.11, 0.04, 0.77])
    cbar = fig.colorbar(im, cax=cbar_ax)
    cbar.set_label(r't [Gyr]')

    plt.show() if save:
      plt.savefig(fname + str(IDStars[j]) + '_cylindrical', bbox_inches='tight')
      plt.close()

  return

def cumsummedian(a,weights=None):
    """
    Compute the weighted median.
    Returns the median of the array elements.
    Parameters
    ----------
    a : array_like, shape (n, )
        Input array or object that can be converted to an array.
    weights : {array_like, shape (n, ), None}, optional
        Input array or object that can be converted to an array.
    Returns
    -------
    median : float
    """
    if weights is None:
        weights=np.ones(np.array(a).shape)
    A = np.array(a).astype('float')
    W = np.array(weights).astype('float')
    if not(np.product(np.isnan(A))):
        I = np.argsort(A)
        cumweight = np.hstack([0,np.cumsum(W[I])])
        X = np.hstack([0,(cumweight[:-1]+cumweight[1:])/(2*cumweight[-1]),1])
        Y = np.hstack([np.min(A),A[I],np.max(A)])
        P = interp1d(X,Y)(0.5)
        return float(P)
    else:
        return np.nan

def plot_disk_scale_height(name, save=True):
  '''
  '''
  t_tot = 9.778943899409334 #Gyr

  fit=False

  if (('7p5' in name) or ('8p0' in name)):
    nsnaps = 401
  else:
    nsnaps = 102

  (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,   PotDMs,
              MassStar, PosStars, VelStars, IDStars, PotStars
              ) = load_nbody.load_snapshot(name, '000')

  #cylindrical positions
  R = np.linalg.norm((PosStars[:,0], PosStars[:,1]), axis=0)

  R_sort = np.argsort(R)
  if len(R)%2 == 0:
    half_mass_radii = (R[R_sort][len(R)>>1] + R[R_sort][(len(R)>>1)-1])/2
  else:
    half_mass_radii = R[R_sort][int(len(R)/2)]

  one = 1
  year = 0
  area = np.pi * ((half_mass_radii + one)**2 - (half_mass_radii - one)**2)
  colors = plt.cm.viridis(np.linspace(0,1,10))

  z_heights = np.zeros(nsnaps)
  sigma_v_z = np.zeros(nsnaps)
  sigma_z   = np.zeros(nsnaps)
  rho_0_val = np.zeros(nsnaps)

  for i in range(nsnaps):
  # for i in [398,399,400]:
    if i != 0:
      snap = '{0:03d}'.format(int(i))
      (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,   PotDMs,
                  MassStar, PosStars, VelStars, IDStars, PotStars
                  ) = load_nbody.load_snapshot(name, snap)

      #cylindrical positions
      R = np.linalg.norm((PosStars[:,0], PosStars[:,1]), axis=0)
    #cylidrical mask
    R_mask = np.logical_and(R < (half_mass_radii + one),
                            R > (half_mass_radii - one))
    #z to use
    zs = PosStars[R_mask,2]
    zs = zs - np.mean(zs)

    if fit:
      #sech^2 pdf
      likely = lambda z_0 : -np.sum( np.log( 1 / (np.abs(z_0) * 4 *
                                                  np.cosh(zs / (2*z_0))**2 )))
      #fit and save sech^2 profile
      z_sg = minimize(likely, 0.2)
      z_sg = np.abs(z_sg.x[0])
      z_heights[i] = z_sg

    else:
      #median |z|
      z_0 = np.median(np.abs(zs))
      z_sg = z_0 / np.log(3)
      z_heights[i] = z_0

    sigma_z[i] = np.std(zs)
    sigma_v_z[i] = np.std(VelStars[R_mask,2])
    Sigma = MassStar*10**10 * np.sum(R_mask) / area
    rho_0 = Sigma / (4 * z_sg)
    rho_0_val[i] = rho_0

    # #plot
    # plt.figure()

    # zlins = np.linspace(-5,5,51)
    # dz = zlins[1] - zlins[0]
    # z_middle = (zlins[:-1] + zlins[1:]) /2

    # densities, _ = np.histogram(zs, bins=zlins, density=False,
    #         weights=MassStar*np.ones(np.sum(R_mask))*10**10 / (area * dz))
    # plt.errorbar(z_middle, densities, ls='', fmt='x', alpha=0.5)

    # #self gravitating
    # profile_sg = rho_0 / np.cosh(zlins/(2*z_sg))**2
    # plt.errorbar(zlins, profile_sg)

    # #non-self gravitating
    # # r_h = np.sqrt(zlins**2 + half_mass_radii**2) + 32
    # # profile_nsg = rho_0 * (np.exp(4.301e4 * 1.85 * 10**2 /
    # #                               (r_h * sigma_v_z[i]**2)) - 1) * 1e-10
    # # plt.errorbar(zlins, profile_nsg)
    # f_scale = Sigma / (sigma_v_z[i]**2 / (2 * 4.301e4 * 10**-10 * half_mass_radii))
    # profile_nsg = f_scale * sigma_v_z[i]**2 / (2 * np.pi * 4.301e4 * 10**-10 *
    #                                   (half_mass_radii**2 + zlins**2))
    # plt.errorbar(zlins, profile_nsg)
    # plt.semilogy()

    if i == 0:
      plt.figure()
      plt.xlabel(r'$z$ [kpc]')
      plt.ylabel(r'$\rho(z)$ [M$_\odot$/kpc$^3$]')

    if np.floor(i * t_tot / nsnaps) == year:
      zlins = np.linspace(-1.2,1.2,50)
      dz = zlins[1] - zlins[0]
      z_middle = (zlins[:-1] + zlins[1:]) /2

      # plt.hist(zs, bins=zlins, density=True, alpha=0.2)
      # profile = 1 / (z_0*4*np.cosh(zlins/(2*z_0))**2)
      # plt.plot(zlins, profile, label=year)

      densities, _ = np.histogram(zs, bins=zlins, density=False,
              weights=MassStar*np.ones(np.sum(R_mask))*10**10 / (area * dz))

      plt.errorbar(z_middle, densities, c=colors[year], ls='', fmt='x', alpha=0.5)

      # profile = (MassStar*10**10 * np.sum(R_mask) / area
      #            ) / (z_0*4*np.cosh(zlins/(2*z_0))**2)
      profile = rho_0 / np.cosh(zlins/(2*z_sg))**2
      plt.errorbar(zlins, profile, c=colors[year])

      year += 1

  plt.grid()
  plt.semilogy()
  plt.ylim([10**5, 10**8])

  plt.show() if save:
    fname = '../galaxies/' + name + '/results/vertical_profile_fit'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  t = np.linspace(0, t_tot, nsnaps)

  #286186 = G^2 * 200 * rho_crit / V200 * 1e4 * (kpc/km) / (Gyr/s)
  const = 286186 * MassDM #approx 25
  nu_t = np.sqrt(4 * np.pi * rho_0_val    * 43010.0 * 10**-10)
  nu_0 = np.sqrt(4 * np.pi * rho_0_val[0] * 43010.0 * 10**-10)
  integ_const = 6.579736267392906

  #is sigma_z^2 propto h_z^3
  plt.figure()
  plt.scatter(z_heights**2, sigma_z**2, c=t)
  plt.errorbar(z_heights**2, integ_const/2 * z_heights**2 / np.log(3)**2,
               label=r'$\sigma_z \propto h_z $', ls=':', c='k')
  plt.xlabel(r'$h_z^2$ [kpc$^2$]')
  plt.ylabel(r'$\sigma_z^2$ [kpc$^2$]')
  plt.legend()
  plt.grid()

  plt.show() if save:
    fname = '../galaxies/' + name + '/results/height_disp_z'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()


  #is sigma_v_z propto sigma_z
  plt.figure()
  plt.scatter(sigma_z, sigma_v_z, c=t)
  plt.errorbar(sigma_z, nu_0*sigma_z, c='grey', ls='--',
               label=r'$\sigma_{v_z} = \nu (t=0) \sigma_z$')
  plt.errorbar(sigma_z, nu_t*sigma_z, c='k', ls=':',
               label=r'$\sigma_{v_z} = \nu (t)   \sigma_z$')
  plt.xlabel(r'$\sigma_z$ [kpc]')
  plt.ylabel(r'$\sigma_{v_z}$ [km/s]')
  plt.grid()
  plt.legend()
  # plt.xlim((0.18,0.50))
  # plt.ylim((10,20))

  plt.show() if save:
    fname = '../galaxies/' + name + '/results/disp_z_disp_v_z'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()


  #is sigma_v_z propto h
  plt.figure()
  plt.scatter(sigma_v_z**2, z_heights**2, c=t)
  plt.errorbar(sigma_v_z**2, 2/integ_const * (sigma_v_z/nu_t)**2 * np.log(3)**2,
               c='k', ls=':', label=r'$h_z^2 = f(\sigma_{v_z}^2, \rho_0) $')
  plt.xlabel(r'$\sigma_{v_z}^2$ [km/s$^2$]')
  plt.ylabel(r'$h_z^2$ [kpc$^2$]')
  plt.grid()
  plt.legend()
  # plt.xlim((0.18,0.50))
  # plt.ylim((10,20))

  plt.show() if save:
    fname = '../galaxies/' + name + '/results/disp_v_z_h'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  #dispersion vs time
  plt.figure()
  plt.errorbar(t, sigma_v_z**2)
  plt.errorbar(t, sigma_v_z[0]**2 + t*const,
               label=r'$\Delta \sigma_{v_z}^2 \propto t$')
  plt.xlabel(r'$t$ [Gyr]')
  plt.ylabel(r'$\sigma_{v_z}^2$ [kpc]')
  plt.legend()
  plt.grid()
  plt.xlim((0,t_tot))

  plt.show() if save:
    fname = '../galaxies/' + name + '/results/disp_v_z_t'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  #main observation
  plt.figure()
  plt.errorbar(t, z_heights, label=r'$M_{dm} =$ '+name[23]+'.'+name[25], zorder=-10)

  z_calc = np.sqrt( 2 / (nu_t**2 * integ_const) * ( t * const + sigma_v_z[0]**2 ) )
  plt.errorbar(t, z_calc*np.log(3), ls=':', c='k', label='self-gravitating')

  # z_fixed = np.sqrt( 2 / (nu_0**2 * integ_const) * ( t * const + sigma_v_z[0]**2 ) )
  # plt.errorbar(t, z_fixed, ls='--', c='g', label='t=0 calc')

  plt.xlabel(r'$t$ [Gyr]')
  plt.ylabel(r'$h_z$ [kpc]')
  plt.legend()
  plt.grid()
  plt.xlim((0,t_tot))
  plt.ylim((0,np.amax(z_calc)*1.2))

  plt.show() if save:
    fname = '../galaxies/' + name + '/results/disk_height_t'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  return

def plot_disk_scale_height_mean(name, save=True):
  '''
  '''
  t_tot = 9.778943899409334 #Gyr

  fit=False

  if (('7p5' in name) or ('8p0' in name)):
    nsnaps = 401
  else:
    nsnaps = 102

  (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,   PotDMs,
              MassStar, PosStars, VelStars, IDStars, PotStars
              ) = load_nbody.load_snapshot(name, '000')

  #cylindrical positions
  R = np.linalg.norm((PosStars[:,0], PosStars[:,1]), axis=0)

  R_sort = np.argsort(R)
  if len(R)%2 == 0:
    half_mass_radii = (R[R_sort][len(R)>>1] + R[R_sort][(len(R)>>1)-1])/2
  else:
    half_mass_radii = R[R_sort][int(len(R)/2)]

  one = 1
  year = 0
  area = np.pi * ((half_mass_radii + one)**2 - (half_mass_radii - one)**2)

  mean_z    = np.zeros(nsnaps)
  median_z  = np.zeros(nsnaps)
  sech_z    = np.zeros(nsnaps)
  sigma_v_z = np.zeros(nsnaps)

  for i in range(nsnaps):
  # for i in [398,399,400]:
    if i != 0:
      snap = '{0:03d}'.format(int(i))
      (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,   PotDMs,
                  MassStar, PosStars, VelStars, IDStars, PotStars
                  ) = load_nbody.load_snapshot(name, snap)

      #cylindrical positions
      R = np.linalg.norm((PosStars[:,0], PosStars[:,1]), axis=0)
    #cylidrical mask
    R_mask = np.logical_and(R < (half_mass_radii + one),
                            R > (half_mass_radii - one))
    #z to use
    zs = PosStars[R_mask,2]
    zs = zs - np.mean(zs)

    #sech^2 pdf
    likely = lambda z_0 : -np.sum( np.log( 1 / (np.abs(z_0) * 4 *
                                                np.cosh(zs / (2*z_0))**2 )))
    #fit and save sech^2 profile
    sech_z[i] = np.abs(minimize(likely, 0.2).x[0])

    mean_z[i]    = np.mean(np.abs(zs))
    median_z[i]  = np.median(np.abs(zs))
    sigma_v_z[i] = np.std(VelStars[R_mask,2])

  t = np.linspace(0, t_tot, nsnaps)

  #main observation
  plt.figure()
  plt.errorbar(t, mean_z,   label=r'mean($|z|$)')
  plt.errorbar(t, median_z, label=r'median($|z|$)')
  plt.errorbar(t, sech_z,   label=r'$z_0$')

  print(mean_z / sech_z)
  print(2 * np.log(2))

  # z_calc = sigma_v_z
  # plt.errorbar(t, z_calc*np.log(3))

  plt.xlabel(r'$t$ [Gyr]')
  plt.ylabel(r'$h_z$ [kpc]')
  plt.xlim((0,t_tot))
  plt.ylim((0,10))
  plt.legend()

  return

def plot_points_vs_time(name, save=True):
  '''
  '''
  # t_tot = 9.778943899409334 #Gyr

  if (('7p5' in name) or ('8p0' in name)):
    nsnaps = 401
  else:
    nsnaps = 102

  (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,
              MassStar, PosStars, VelStars, IDStars,
    j_z_stars, j_p_stars, j_c_stars, energy_stars,
    E_interp, j_circ_interp, R_interp
    ) = load_nbody.load_and_calculate(name, '000')

  #just put stars in argsort order
  n_stars  = len(IDStars)

  j_value           = np.zeros((nsnaps, n_stars))
  j_z_value         = np.zeros((nsnaps, n_stars))
  e_tot_value       = np.zeros((nsnaps, n_stars))
  r_value           = np.zeros((nsnaps, n_stars))
  j_zonc_value      = np.zeros((nsnaps, n_stars))
  j_onc_value       = np.zeros((nsnaps, n_stars))
  j_zon_value       = np.zeros((nsnaps, n_stars))
  inclination_value = np.zeros((nsnaps, n_stars))

  for i in range(nsnaps):
    snap = '{0:03d}'.format(int(i))
    (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,
                MassStar, PosStars, VelStars, IDStars,
      j_z_stars, j_p_stars, j_c_stars, energy_stars,
      E_interp, j_circ_interp, R_interp
      ) = load_nbody.load_and_calculate(name, snap)

    #get order
    order_mask = np.argsort(IDStars)

    #calculate some stuff and order everything correctly
    j_z_stars = j_z_stars[order_mask]
    j_c_stars = j_c_stars[order_mask]
    energy_stars = energy_stars[order_mask]
    r_stars = np.linalg.norm(PosStars, axis=1)
    r_stars = r_stars[order_mask]

    j_stars = np.linalg.norm(np.cross(PosStars, VelStars), axis=1)
    j_stars = j_stars[order_mask]

    #add values to arrays
    j_value[i,:] = j_stars

    j_z_value[i,:] = j_z_stars

    e_tot_value[i,:] = energy_stars

    r_value[i,:] = r_stars

    j_zonc_value[i,:] = j_z_stars / j_c_stars

    j_onc_value[i,:] = j_stars / j_c_stars

    j_zon_value[i,:] = j_z_stars / j_stars

    inclination_value[i,:] = np.arccos(j_z_stars / j_stars) /np.pi*180

  print('done processing files. making plots now.')

  #units and stuff
  j_value     /= 10**3 # 10^3 kpc km/s
  j_z_value   /= 10**3 # 10^3 kpc km/s
  e_tot_value /= 10**5 # 10^5 km/s

  #bins in e
  bins = [-2,-1.75,-1.5,-1.25]
  masks = np.ones((len(bins)+1,n_stars), dtype=bool)

  #initial energy bins
  bins_initial=True
  bin_decision = np.digitize(e_tot_value[1], bins=bins)

  for i in range(len(bins)+1):
    masks[i,:] = bin_decision == i

  #make plots
  actually_plot_points_vs_time(name, bins_initial, save,
    masks, np.linspace(-2,6,81), MassStar, j_z_value,
    -2, 6, [-2,0,2,4,6],[-2,0,2,4,''], r'$j_z$ [$10^3$ kpc km/s]', 'j_z')

  actually_plot_points_vs_time(name, bins_initial, save,
    masks, np.linspace(0,6,61), MassStar, j_value,
    0, 6, [0,2,4,6],[0,2,4,''], r'$|\vec{j}|$ [$10^3$ kpc km/s]', 'j')

  actually_plot_points_vs_time(name, bins_initial, save,
    masks, np.linspace(-2.5,-1,61), MassStar, e_tot_value,
    -2.5, -1, [-2.5,-2,-1.5,-1],[-2.5,-2,-1.5,''],
    r'$E$ [$10^5$ (km/s)$^2$]', 'e')

  actually_plot_points_vs_time(name, bins_initial, save,
    masks, np.linspace(0,50,51), MassStar, r_value,
    0, 50, [0,20,40],[0,20,40,''], r'$r$ [kpc]', 'r')

  actually_plot_points_vs_time(name, bins_initial, save,
    masks, np.linspace(-1.5,1.5,61), MassStar, j_zonc_value,
    -1.5, 1.5, [-1.5,-1,-.5,0,.5,1,1.5], ['',-1,'',0,'',1,''],
    r'$j_z/j_c(E)$', 'j_zonc')

  actually_plot_points_vs_time(name, bins_initial, save,
    masks, np.linspace(0,1.5,61), MassStar, j_onc_value,
    0, 1.5, [0,.5,1,1.5],[0,.5,1,''], r'$|\vec{j}|/j_c(E)$', 'j_onc')

  actually_plot_points_vs_time(name, bins_initial, save,
    masks, np.linspace(-1,1,61), MassStar, j_zon_value,
    -1, 1, [-1,-.5,0,.5,1], ['',-.5,0,.5,1], r'$j_z/|\vec{j}|$', 'j_zon')

  actually_plot_points_vs_time(name, bins_initial, save,
    masks, np.linspace(0,180,61), MassStar, inclination_value,
    0, 180, [0,45,90,135,180],[0,45,90,135,''], r'$i$ [deg]', 'i')

  #final energy bins
  bins_initial=False
  bin_decision = np.digitize(e_tot_value[nsnaps-2], bins=bins)

  for i in range(len(bins)+1):
    masks[i,:] = bin_decision == i

  #make plots
  actually_plot_points_vs_time(name, bins_initial, save,
    masks, np.linspace(-2,6,81), MassStar, j_z_value,
    -2, 6, [-2,0,2,4,6],[-2,0,2,4,''], r'$j_z$ [$10^3$ kpc km/s]', 'j_z')

  actually_plot_points_vs_time(name, bins_initial, save,
    masks, np.linspace(0,6,61), MassStar, j_value,
    0, 6, [0,2,4,6],[0,2,4,''], r'$|\vec{j}|$ [$10^3$ kpc km/s]', 'j')

  actually_plot_points_vs_time(name, bins_initial, save,
    masks, np.linspace(-2.5,-1,61), MassStar, e_tot_value,
    -2.5, -1, [-2.5,-2,-1.5,-1],[-2.5,-2,-1.5,''],
    r'$E$ [$10^5$ (km/s)$^2$]', 'e')

  actually_plot_points_vs_time(name, bins_initial, save,
    masks, np.linspace(0,50,51), MassStar, r_value,
    0, 50, [0,20,40],[0,20,40,''], r'$r$ [kpc]', 'r')

  actually_plot_points_vs_time(name, bins_initial, save,
    masks, np.linspace(-1.5,1.5,61), MassStar, j_zonc_value,
    -1.5, 1.5, [-1.5,-1,-.5,0,.5,1,1.5], ['',-1,'',0,'',1,''],
    r'$j_z/j_c(E)$', 'j_zonc')

  actually_plot_points_vs_time(name, bins_initial, save,
    masks, np.linspace(0,1.5,61), MassStar, j_onc_value,
    0, 1.5, [0,.5,1,1.5],[0,.5,1,''], r'$|\vec{j}|/j_c(E)$', 'j_onc')

  actually_plot_points_vs_time(name, bins_initial, save,
    masks, np.linspace(-1,1,61), MassStar, j_zon_value,
    -1, 1, [-1,-.5,0,.5,1], ['',-.5,0,.5,1], r'$j_z/|\vec{j}|$', 'j_zon')

  actually_plot_points_vs_time(name, bins_initial, save,
    masks, np.linspace(0,180,61), MassStar, inclination_value,
    0, 180, [0,45,90,135,180],[0,45,90,135,''], r'$i$ [deg]', 'i')

  return

def actually_plot_points_vs_time(name, bins_initial, save,
                                 masks, bins, MassStar, value,
                              ymin, ymax, yticks, yticklabels, ylabel, yname):
  '''
  '''
  t_tot = 9.778943899409334 #Gyr

  if (('7p5' in name) or ('8p0' in name)):
    nsnaps = 401
  else:
    nsnaps = 102

  times = np.linspace(0, t_tot, nsnaps)

  #set up arrays to plot
  tot_heigts = np.zeros((len(masks), nsnaps, len(bins)-1))
  medians    = np.zeros((len(masks), nsnaps))

  #calculate values in arrays
  for k in range(len(masks)):

    weights = MassStar*np.ones(np.sum(masks[k]))

    for t in range(nsnaps):
      #histogram at one snap
      out, _ = np.histogram(value[t,masks[k]],
                            bins=bins, weights=weights)
      out = np.flip(out)
      # tot_heigts[t,:] = np.log10(out)
      tot_heigts[k,t,:] = np.log10(out+MassStar)

      #median
      medians[k,t] = np.median(value[t,masks[k]])
  vmax = np.amax(tot_heigts)

  #set up plot
  fig, ax = plt.subplots(nrows=len(masks), ncols=1, sharex=True, sharey=True,
                         figsize=(5,10))
  fig.subplots_adjust(hspace=0.03,wspace=0)
  #actually do the plotting
  for k in range(len(masks)):
    im = ax[k].imshow(tot_heigts[k,:,:].T, extent=(0, t_tot, ymin, ymax),
                      vmax=vmax, aspect='auto')
    # for j in range(len(masks[k])):
    #   if masks[k,j]:
    #     ax[k].errorbar(times, value[:,j],
    #                    fmt='.', ls='-')
    ax[k].errorbar(times, medians[k], c='r', alpha=0.5)
    ax[k].grid()
    ax[k].set_yticks(yticks)
    ax[k].set_yticklabels(yticklabels)
  ax[len(masks)-1].set_xlabel(r'time [Gyr]')
  ax[int((len(masks)-1)/2)].set_ylabel(ylabel)
  ax[len(masks)-1].set_xlim(0, t_tot)
  ax[len(masks)-1].set_ylim(ymin, ymax)

  fig.subplots_adjust(right=0.8)
  cbar_ax = fig.add_axes([0.81, 0.11, 0.04, 0.77])
  cbar = fig.colorbar(im, cax=cbar_ax)
  cbar.set_label(r'$\log ( M / 10^{10} M_\odot )$')

  plt.show() if save:
    fname = '../galaxies/' + name + '/results/' + yname + '_t'
    if bins_initial:
      fname += '_initial_e_bins'
    else:
      fname += '_final_e_bins'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  return

def plot_galaxy_totals_vs_time(name, save=True):
  '''
  '''
  t_tot = 9.778943899409334 #Gyr

  if (('7p5' in name) or ('8p0' in name)):
    nsnaps = 401
  else:
    nsnaps = 102

  centre = np.zeros((nsnaps, 3))

  j_tot_stars     = np.zeros((nsnaps, 3))
  sum_abs_j_stars = np.zeros(nsnaps)
  j_tot_dms       = np.zeros((nsnaps, 3))
  j_tot_all       = np.zeros((nsnaps, 3))

  for i in range(nsnaps):
    snap = '{0:03d}'.format(int(i))
    #load raw data
    (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,   PotDMs,
                MassStar, PosStars, VelStars, IDStars, PotStars
     ) = load_nbody.load_snapshot(name, snap)

    #correct positions
    (PosDMs, VelDMs, PosStars, VelStars,
      total_offset, weighted_central_velocity
      ) = halo.centre_maximum_potential(PosDMs,   VelDMs,   MassDM,   PotDMs,
                                        PosStars, VelStars, MassStar, PotStars,
                                        verbose=False, output_delta=True)

    #record positions
    centre[i] = total_offset
    #angular momentum
    j_stars = np.cross(PosStars, VelStars) * MassStar
    j_tot_stars[i] = np.sum(j_stars, axis=0)
    sum_abs_j_stars[i] = np.sum(np.linalg.norm(j_stars, axis=1))

    #DM angular momentum
    j_DMs = np.cross(PosDMs, VelDMs) * MassDM
    j_tot_dms[i] = np.sum(j_DMs, axis=0)

    j_tot_all[i] = j_tot_stars[i] + j_tot_dms[i]

  plt.figure()
  plt.errorbar(np.linspace(0, t_tot, nsnaps), centre[:,0],
                fmt='', ls='-', c='C0', label='x')
  plt.errorbar(np.linspace(0, t_tot, nsnaps), centre[:,1],
                fmt='', ls='-', c='C1', label='y')
  plt.errorbar(np.linspace(0, t_tot, nsnaps), centre[:,2],
                fmt='', ls='-', c='C2', label='z')
  plt.xlabel(r'time [Gyr]')
  plt.ylabel(r'$\Delta$ galaxy centre [kpc]')
  plt.xlim((0, t_tot))
  plt.legend()
  plt.grid()
  plt.show() if save:
    fname = '../galaxies/' + name + '/results/delta_galaxy_position'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  plt.figure()
  plt.errorbar(np.linspace(0, t_tot, nsnaps), j_tot_stars[:,0],
               fmt='', ls='-', c='C0', label='*x')
  plt.errorbar(np.linspace(0, t_tot, nsnaps), j_tot_stars[:,1],
               fmt='', ls='-', c='C1', label='*y')
  plt.errorbar(np.linspace(0, t_tot, nsnaps), j_tot_stars[:,2],
               fmt='', ls='-', c='C2', label='*z')

  plt.errorbar(np.linspace(0, t_tot, nsnaps), j_tot_dms[:,0],
               fmt='', ls='-', c='C3', label='DMx')
  plt.errorbar(np.linspace(0, t_tot, nsnaps), j_tot_dms[:,1],
               fmt='', ls='-', c='C4', label='DMy')
  plt.errorbar(np.linspace(0, t_tot, nsnaps), j_tot_dms[:,2],
               fmt='', ls='-', c='C5', label='DMz')
  plt.xlabel(r'time [Gyr]')
  plt.ylabel(r'$j_{X}$ [$10^{10} M_\odot$ kpc km/s]')
  plt.xlim((0, t_tot))
  plt.legend()
  plt.grid()
  plt.show() if save:
    fname = '../galaxies/' + name + '/results/galaxy_angular_momentum'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  plt.figure()
  plt.errorbar(np.linspace(0, t_tot, nsnaps), np.linalg.norm(j_tot_stars, axis=1)
               - np.linalg.norm(j_tot_stars[0]),
               fmt='', ls='-', c='C0', label=r'$|\Sigma j_*|$')
  plt.errorbar(np.linspace(0, t_tot, nsnaps), np.linalg.norm(j_tot_dms, axis=1)
               - np.linalg.norm(j_tot_dms[0]),
               fmt='', ls='-', c='C1', label=r'$|\Sigma j_{DM}|$')
  plt.errorbar(np.linspace(0, t_tot, nsnaps), np.linalg.norm(j_tot_all, axis=1)
               - np.linalg.norm(j_tot_all[0]),
               fmt='', ls='-', c='C2', label=r'$|\Sigma j_{all}|$')
  plt.xlabel(r'time [Gyr]')
  plt.ylabel(r'$\Delta |j|$ [$10^{10} M_\odot$ kpc km/s]')
  plt.xlim((0, t_tot))
  plt.legend()
  plt.grid()
  plt.show() if save:
    fname = '../galaxies/' + name + '/results/delta_galaxy_angular_momentum'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  plt.figure()
  plt.errorbar(np.linspace(0, t_tot, nsnaps), np.linalg.norm(j_tot_stars, axis=1),
               fmt='', ls='-', c='C0', label=r'$|\Sigma j_*|$')
  # plt.errorbar(np.linspace(0, t_tot, nsnaps), sum_abs_j_stars,
  #              fmt='', ls='-', c='C3', label='$\Sigma |j_*|$')
  plt.errorbar(np.linspace(0, t_tot, nsnaps), np.linalg.norm(j_tot_dms, axis=1),
               fmt='', ls='-', c='C1', label=r'$|\Sigma j_{DM}|$')
  # plt.errorbar(np.linspace(0, t_tot, nsnaps), sum_abs_j_dms,
  #              fmt='', ls='-', c='C4', label='$\Sigma |j_{DM}|$')
  plt.errorbar(np.linspace(0, t_tot, nsnaps), np.linalg.norm(j_tot_all, axis=1),
               fmt='', ls='-', c='C2', label=r'$|\Sigma j_{all}|$')
  plt.xlabel(r'time [Gyr]')
  plt.ylabel(r'$|j|$ [$10^{10} M_\odot$ kpc km/s]')
  plt.xlim((0, t_tot))
  plt.legend()
  plt.grid()
  plt.show() if save:
    fname = '../galaxies/' + name + '/results/galaxy_angular_momentum'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  plt.figure()
  plt.errorbar(np.linspace(0, t_tot, nsnaps), j_tot_stars[:,2],
               fmt='', ls='-', c='C0', label=r'$|\Sigma j_*|$')
  plt.errorbar(np.linspace(0, t_tot, nsnaps), j_tot_dms[:,2],
               fmt='', ls='-', c='C2', label=r'$|\Sigma j_{DM}|$')
  plt.errorbar(np.linspace(0, t_tot, nsnaps), j_tot_all[:,2],
               fmt='', ls='-', c='C3', label=r'$|\Sigma j_{all}|$')
  plt.xlabel(r'time [Gyr]')
  plt.ylabel(r'$j_z$ [$10^{10} M_\odot$ kpc km/s]')
  plt.xlim((0, t_tot))
  plt.legend()
  plt.grid()
  plt.show() if save:
    fname = '../galaxies/' + name + '/results/galaxy_z_angular_momentum'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  return

def plot_projection_final_for_gif(name, snapi, disk_ids, pro_ids, retro_ids,
                                  corrections=False):
  '''
  '''
  t_tot = 9.778943899409334 #Gyr

  if (('7p5' in name) or ('8p0' in name)):
    nsnaps = 401
  else:
    nsnaps = 102

  snap = '{0:03d}'.format(snapi)
  print(snap)

  #load
  (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,   PotDMs,
   MassStar, PosStars, VelStars, IDStars, PotStars
   ) = load_nbody.load_snapshot(name, snap)

  if corrections:

    # centre galaxy. needs potentials done first.
    (PosDMs, VelDMs, PosStars, VelStars
      ) = halo.centre_maximum_potential(PosDMs,   VelDMs,   MassDM,   PotDMs,
                                        PosStars, VelStars, MassStar, PotStars)
    #align orientation of disk
    j_tot = halo.total_angular_momentum(PosStars, VelStars, MassStar)
    #find rotation
    rotation = halo.find_rotaion_matrix(j_tot)
    #rotate stars
    PosStars = rotation.apply(PosStars)

  # else:
  #   file_name = '../galaxies/'+str(name)+'/data/snapshot_'+str(snap)+'.hdf5'
  #   fs = h5.File(file_name,"r")
  #   PosDMs = fs['PartType1/Coordinates'][:]
  #   fs.close()

  # plt.figure()
  fig, ax = plt.subplots(nrows=2, ncols=1, sharex=True, sharey=True,
                         figsize=(5, 8))
  fig.subplots_adjust(hspace=0,wspace=0)

  disk_mask  = np.isin(IDStars, disk_ids)
  pro_mask   = np.isin(IDStars, pro_ids)
  retro_mask = np.isin(IDStars, retro_ids)

  #disk
  ax[0].scatter(PosStars[disk_mask,0], PosStars[disk_mask,1], s=5, c='C1')
  ax[1].scatter(PosStars[disk_mask,0], PosStars[disk_mask,2], s=5, c='C1')

  #prograde
  ax[0].scatter(PosStars[pro_mask,0], PosStars[pro_mask,1], s=5, c='C0')
  ax[1].scatter(PosStars[pro_mask,0], PosStars[pro_mask,2], s=5, c='C0')

  #retrograde
  ax[0].scatter(PosStars[retro_mask,0], PosStars[retro_mask,1], s=5, c='C2')
  ax[1].scatter(PosStars[retro_mask,0], PosStars[retro_mask,2], s=5, c='C2')

  #time
  ax[1].text(-29,-29, 't='+str(np.round(t_tot * snapi/nsnaps, 2))+'Gyr')

  xlim=[-30,30]
  ax[0].set_xlim(xlim)
  ax[0].set_ylim(xlim)
  ax[1].set_xlim(xlim)
  ax[1].set_ylim(xlim)
  ax[0].set_aspect('equal')
  ax[1].set_aspect('equal')

  ax[0].set_ylabel('y [kpc]')
  ax[1].set_ylabel('z [kpc]')
  ax[1].set_xlabel('x [kpc]')

  fig.canvas.draw()
  image = np.frombuffer(fig.canvas.tostring_rgb(), dtype='uint8')
  image = image.reshape(fig.canvas.get_width_height()[::-1] + (3,))

  plt.close()

  return(image)

def make_projection_final_gif(name, corrections=False):
  '''
  '''
  if (('7p5' in name) or ('8p0' in name)):
    nsnaps     =  401
    nsnaps_str = '399'
  else:
    nsnaps     =  102
    nsnaps_str = '101'

  #load
  (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,
              MassStar, PosStars, VelStars, IDStars,
    j_z_stars, j_p_stars, j_c_stars, energy_stars,
    E_interp, j_circ_interp, R_interp
    ) = load_nbody.load_and_calculate(name, nsnaps_str)

  disk_mask  = j_z_stars/j_c_stars > 0.7
  retro_mask = j_z_stars < 0
  pro_mask   = np.logical_and(np.logical_not(disk_mask),
                              np.logical_not(retro_mask))

  disk_ids  = IDStars[disk_mask]
  pro_ids   = IDStars[pro_mask]
  retro_ids = IDStars[retro_mask]

  # fps = 30
  fps = nsnaps/60
  fname = '../galaxies/' + name + '/results/projection.gif'

  imageio.mimsave(fname, [plot_projection_final_for_gif(name, i,
                          disk_ids, pro_ids, retro_ids, corrections)
                          for i in range(nsnaps)], fps=fps)

  return

def plot_projection_instantanious_for_gif(name, snapi, overwrite=False):
  '''
  '''
  if not overwrite:
    if os.path.isfile('../galaxies/'+name+
                      '/results/projection_partilce/'+str(snapi)+'.png'):
      return

  t_tot = 9.778943899409334 #Gyr

  if (('7p5' in name) or ('8p0' in name)):
    nsnaps = 401
  else:
    nsnaps = 102

  snap = '{0:03d}'.format(snapi)
  print(snap)
  (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,   PotDMs,
   MassStar, PosStars, VelStars, IDStars, PotStars
   ) = load_nbody.load_and_align(name, snap)

  fig, ax = plt.subplots(nrows=2, ncols=1, sharex=True, sharey=True,
                         figsize=(9.125, 7.2))
  fig.subplots_adjust(hspace=0,wspace=0)

  order0 = np.argsort(PosStars[:,0])
  order1 = np.argsort(PosStars[:,1])
  order2 = np.argsort(PosStars[:,2])

  im = ax[0].scatter(PosStars[order2,0], PosStars[order2,1], s=2, c='C4')
  ax[1].scatter(PosStars[order1,0], PosStars[order1,2], s=2, c='C4')

  #time
  ax[1].text(-29,-29, 't='+str(np.round(t_tot * snapi/nsnaps, 2))+'Gyr')

  xlim=[-30,30]
  ax[0].set_xlim(xlim)
  ax[0].set_ylim(xlim)
  ax[1].set_xlim(xlim)
  ax[1].set_ylim(xlim)
  ax[0].set_aspect('equal')
  ax[1].set_aspect('equal')

  ax[0].set_ylabel('y [kpc]')
  ax[1].set_ylabel('z [kpc]')
  ax[1].set_xlabel('x [kpc]')

  ax[0].set_xticks([-20,0,20])

  # fig.canvas.draw()
  # image = np.frombuffer(fig.canvas.tostring_rgb(), dtype='uint8')
  # image = image.reshape(fig.canvas.get_width_height()[::-1] + (3,))

  plt.savefig('../galaxies/'+name+'/results/projection_particles/'+str(snapi),
              bbox_inches='tight')

  plt.close()
  return

  # return(image)

def make_projection_instantanious_gif(name):
  '''
  '''
  if (('7p5' in name) or ('8p0' in name)):
    nsnaps     =  401
    # nsnaps     =  4
  else:
    nsnaps     =  102

  # fps = 10
  fps = nsnaps/40
  fname = '../galaxies/' + name + '/results/projection_particles'

  try:
    os.mkdir(fname+'/')
  except:
    pass

  for i in range(nsnaps):
    plot_projection_instantanious_for_gif(name, i)

  w = imageio.get_writer(fname+'.mp4', format='FFMPEG', mode='I', fps=fps)#,
                         # codec='h264_vaapi',
                         # output_params=['-vaapi_device',
                         #               '/dev/dri/renderD128', '-vf',
                         #               'format=gray|nv12,hwupload'],
                         # pixelformat='vaapi_vld')

  for i in range(nsnaps):
    w.append_data(imageio.imread('../galaxies/'+name+
                                 '/results/projection_particles/'+str(i)+'.png'))
  w.close()

  # imageio.mimwrite(fname, [plot_projection_instantanious_for_gif(name, i)
  #                          for i in range(nsnaps)], fps=fps)
  return

def plot_projection_density_for_gif(name, snapi, max_density,
                                    corrections=False):
  '''
  '''
  t_tot = 9.778943899409334 #Gyr

  if (('7p5' in name) or ('8p0' in name)):
    nsnaps = 401
  else:
    nsnaps = 102

  snap = '{0:03d}'.format(snapi)
  # print(snap)

  #load
  (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,   PotDMs,
   MassStar, PosStars, VelStars, IDStars, PotStars
   ) = load_nbody.load_snapshot(name, snap)

  if corrections:

    # centre galaxy. needs potentials done first.
    (PosDMs, VelDMs, PosStars, VelStars
      ) = halo.centre_maximum_potential(PosDMs,   VelDMs,   MassDM,   PotDMs,
                                        PosStars, VelStars, MassStar, PotStars)
    #align orientation of disk
    j_tot = halo.total_angular_momentum(PosStars, VelStars, MassStar)
    #find rotation
    rotation = halo.find_rotaion_matrix(j_tot)
    #rotate stars
    PosStars = rotation.apply(PosStars)

  # else:
  #   file_name = '../galaxies/'+str(name)+'/data/snapshot_'+str(snap)+'.hdf5'
  #   fs = h5.File(file_name,"r")
  #   PosDMs = fs['PartType1/Coordinates'][:]
  #   fs.close()

  # plt.figure()
  fig, ax = plt.subplots(nrows=2, ncols=1, sharex=True, sharey=True,
                         figsize=(5, 8))
  fig.subplots_adjust(hspace=0,wspace=0)


  n=len(IDStars)
  weights = MassStar*np.ones(n)

  ax[0].hist2d(PosStars[:,0], PosStars[:,1], bins=np.linspace(-30, 30, 81),
               norm=LogNorm(), weights=weights, vmax=max_density)
  _,_,_,im = ax[1].hist2d(PosStars[:,0], PosStars[:,2],
                          bins=np.linspace(-30, 30, 81),
                          norm=LogNorm(), weights=weights, vmax=max_density)

  #time
  ax[1].text(-29,-29, 't='+str(np.round(t_tot * snapi/nsnaps, 2))+'Gyr')

  xlim=[-30,30]
  ax[0].set_xlim(xlim)
  ax[0].set_ylim(xlim)
  ax[1].set_xlim(xlim)
  ax[1].set_ylim(xlim)
  ax[0].set_aspect('equal')
  ax[1].set_aspect('equal')

  ax[0].set_ylabel('y [kpc]')
  ax[1].set_ylabel('z [kpc]')
  ax[1].set_xlabel('x [kpc]')

  # cbar = ax[1].colorbar()
  # cbar.set_label(r'$M$ [$10^{10} M_\odot$]')

  fig.subplots_adjust(right=0.80)
  cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
  cbar = fig.colorbar(im, cax=cbar_ax)
  cbar.set_label(r'$M$ [$10^{10} M_\odot$]')

  fig.canvas.draw()
  image = np.frombuffer(fig.canvas.tostring_rgb(), dtype='uint8')
  image = image.reshape(fig.canvas.get_width_height()[::-1] + (3,))

  plt.close()

  return(image)

def make_projection_density_gif(name, corrections):
  '''
  '''
  if (('7p5' in name) or ('8p0' in name)):
    nsnaps = 401
  else:
    nsnaps = 102

  #load
  (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,   PotDMs,
   MassStar, PosStars, VelStars, IDStars, PotStars
   ) = load_nbody.load_snapshot(name, '000')

  n=len(IDStars)
  weights = MassStar*np.ones(n)

  out, _,_ = np.histogram2d(PosStars[:,0], PosStars[:,2],
                            bins=np.linspace(-30, 30, 81),
                            weights=weights)

  # max_density = np.log10(np.amax(out) + MassStar)
  max_density = np.amax(out)

  # fps = 10
  fps = nsnaps/60
  fname = '../galaxies/' + name + '/results/projection_density.gif'

  imageio.mimsave(fname, [plot_projection_density_for_gif(name, i, max_density,
                                                          corrections)
                          for i in range(nsnaps)], fps=fps)

  return

def plot_e_jz_final_for_gif(name, snapi, disk_ids, pro_ids, retro_ids):
  '''
  '''
  t_tot = 9.778943899409334 #Gyr

  if (('7p5' in name) or ('8p0' in name)):
    nsnaps = 401
  else:
    nsnaps = 102

  snap = '{0:03d}'.format(snapi)
  print(snap)

  (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,
              MassStar, PosStars, VelStars, IDStars,
    j_z_stars, j_p_stars, j_c_stars, energy_stars,
    E_interp, j_circ_interp, R_interp
    ) = load_nbody.load_and_calculate(name, snap)

  disk_mask  = np.isin(IDStars, disk_ids)
  pro_mask   = np.isin(IDStars, pro_ids)
  retro_mask = np.isin(IDStars, retro_ids)

  fig = plt.figure()
  #disk
  plt.scatter(energy_stars[disk_mask]/1e5,  j_z_stars[disk_mask]/1e3,
              s=5, c='C1')
  #prograde
  plt.scatter(energy_stars[pro_mask]/1e5,   j_z_stars[pro_mask]/1e3,
              s=5, c='C0')
  #retrograde
  plt.scatter(energy_stars[retro_mask]/1e5, j_z_stars[retro_mask]/1e3,
              s=5, c='C2')

  #j_z/+jc = 1
  plt.errorbar(E_interp/10**5, j_circ_interp/10**3,
                    fmt='',ls='-.', c='r', alpha=0.5)
  plt.errorbar(E_interp/10**5,-j_circ_interp/10**3,
               fmt='',ls='-.', c='r', alpha=0.5)

  #time
  plt.text(-2.4,-1.9, 't='+str(np.round(t_tot * snapi/nsnaps, 2))+'Gyr')

  plt.xlim((-2.5, -1))
  plt.ylim((-2, 6))
  plt.xlabel(r'E [$10^5$ (km/s)$^2$]')
  plt.ylabel(r'$j_z$ [$10^3$ kpc km/s]')

  fig.canvas.draw()
  image = np.frombuffer(fig.canvas.tostring_rgb(), dtype='uint8')
  image = image.reshape(fig.canvas.get_width_height()[::-1] + (3,))

  plt.close()

  return(image)

def make_e_jz_final_gif(name):
  '''
  '''
  if (('7p5' in name) or ('8p0' in name)):
    nsnaps     =  401
    nsnaps_str = '399'
  else:
    nsnaps     =  102
    nsnaps_str = '101'

  #load
  (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,
              MassStar, PosStars, VelStars, IDStars,
    j_z_stars, j_p_stars, j_c_stars, energy_stars,
    E_interp, j_circ_interp, R_interp
    ) = load_nbody.load_and_calculate(name, nsnaps_str)

  disk_mask  = j_z_stars/j_c_stars > 0.7
  retro_mask = j_z_stars < 0
  pro_mask   = np.logical_and(np.logical_not(disk_mask),
                              np.logical_not(retro_mask))

  disk_ids  = IDStars[disk_mask]
  pro_ids   = IDStars[pro_mask]
  retro_ids = IDStars[retro_mask]

  # fps = 30
  fps = nsnaps/60
  fname = '../galaxies/' + name + '/results/e_jz.gif'

  imageio.mimsave(fname, [plot_e_jz_final_for_gif(name, i,
                          disk_ids, pro_ids, retro_ids)
                          for i in range(nsnaps)], fps=fps)

  return

def plot_dispersion_j_zonc(name='', save=False):
  '''
  '''
  t_tot = 9.778943899409334 #Gyr

  if (('7p5' in name) or ('8p0' in name)):
    nsnaps = 401
  else:
    nsnaps = 102

  disp_v_rho  = np.zeros((nsnaps, 4))
  disp_v_phi  = np.zeros((nsnaps, 4))
  disp_v_z    = np.zeros((nsnaps, 4))
  j_zonc_mean = np.zeros((nsnaps, 4))

  disp_rho = np.zeros(nsnaps)
  disp_phi = np.zeros(nsnaps)
  disp_z   = np.zeros(nsnaps)
  kappa    = np.zeros(nsnaps)
  kappa_co = np.zeros(nsnaps)

  for j in range(nsnaps):
    snap = '{0:03d}'.format(int(j))
    (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,
                MassStar, PosStars, VelStars, IDStars,
      j_z_stars, j_p_stars, j_c_stars, energy_stars,
      E_interp, j_circ_interp, R_interp
      ) = load_nbody.load_and_calculate(name, snap)

    R = np.linalg.norm((PosStars[:,0], PosStars[:,1]), axis=0)

    edges = np.quantile(R, [0.25,0.50,0.75])
    R_bin = np.digitize(R, edges)

    pos_copy = [PosStars[R_bin == i,:] for i in range(4)]
    vel_copy = [VelStars[R_bin == i,:] for i in range(4)]
    j_z_copy = [j_z_stars[R_bin == i]  for i in range(4)]
    j_c_copy = [j_c_stars[R_bin == i]  for i in range(4)]

    for i in range(4):
      (rho, phi, z, v_rho, v_phi, v_z
       ) = get_cylindrical(pos_copy[i], vel_copy[i])

      disp_v_rho[j,i] = np.std(v_rho)
      disp_v_phi[j,i] = np.std(v_phi)
      disp_v_z[j,i]   = np.std(v_z)

      j_zonc_mean[j,i] = np.mean(j_z_copy[i] / j_c_copy[i])

  #make plots
  fig, axs = plt.subplots(nrows=1, ncols=3, sharex=True, sharey=True,
                          figsize=(15,6))
  fig.subplots_adjust(hspace=0,wspace=0)

  time = np.linspace(0, t_tot, nsnaps)
  #different markers for different quantiles
  markers = ['^', 'o', 's', 'v']

  for i in range(4):

    im = axs[0].scatter(disp_v_z[:,i], j_zonc_mean[:,i], c=time, marker=markers[i],
                        cmap='viridis')
    axs[1].scatter(disp_v_rho[:,i], j_zonc_mean[:,i], c=time, marker=markers[i],
                   cmap='viridis')
    axs[2].scatter(disp_v_phi[:,i], j_zonc_mean[:,i], c=time, marker=markers[i],
                   cmap='viridis')

  axs[2].legend([r'0-25 $r$ quantile', r'25-50 $r$ quantile',
                 r'50-75 $r$ quantile', r'75-100 $r$ quantile'], framealpha=0)
  axs[0].set_xlabel(r'$\sigma_{v_z}$ [km/s]')
  axs[1].set_xlabel(r'$\sigma_{v_r}$ [km/s]')
  axs[2].set_xlabel(r'$\sigma_{v_\phi}$ [km/s]')
  axs[0].set_ylabel(r'$\langle j_z / j_c(E) \rangle$')
  axs[0].grid()
  axs[1].grid()
  axs[2].grid()
  axs[0].set_xlim((0,150))
  axs[0].set_ylim((-.2,1.2))

  fig.subplots_adjust(right=0.85)
  cbar_ax = fig.add_axes([0.86, 0.11, 0.03, 0.77])
  cbar = fig.colorbar(im, cax=cbar_ax)
  cbar.set_label(r'time [Gyr]')

  plt.show() if save:
    fname = '../galaxies/' + name + '/results/dispersion_dependance'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  return

def plot_dispersion_kappa(name='', save=False, individual_plots=False):
  '''
  '''
  t_tot = 9.778943899409334 #Gyr

  if (('7p5' in name) or ('8p0' in name)):
    nsnaps = 401
  else:
    nsnaps = 102

  disp_v_rho = np.zeros(nsnaps)
  disp_v_phi = np.zeros(nsnaps)
  disp_v_z   = np.zeros(nsnaps)
  kappa    = np.zeros(nsnaps)
  kappa_co = np.zeros(nsnaps)
  mean_v_phi   = np.zeros(nsnaps)
  median_v_phi = np.zeros(nsnaps)
  v_circ = np.zeros(nsnaps)

  for j in range(nsnaps):
    snap = '{0:03d}'.format(int(j))
    (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,
                MassStar, PosStars, VelStars, IDStars,
      j_z_stars, j_p_stars, j_c_stars, energy_stars,
      E_interp, j_circ_interp, R_interp
      ) = load_nbody.load_and_calculate(name, snap)

    R = np.linalg.norm(PosStars, axis=1)
    r_30_mask = R < 30 #kpc
    PosStars = PosStars[r_30_mask]
    VelStars = VelStars[r_30_mask]

    (rho, phi, z, v_rho, v_phi, v_z
     ) = get_cylindrical(PosStars, VelStars)

    #save cylindrical dispersions
    disp_v_rho[j] = np.std(v_rho)
    disp_v_phi[j] = np.std(v_phi)
    disp_v_z[j]   = np.std(v_z)

    mean_v_phi[j]   = np.mean(v_phi)
    median_v_phi[j] = np.median(v_phi)
    v_circ[j] = np.amax(j_circ_interp / R_interp)

    if individual_plots:
      #close but not exact
      rd   = 6 *np.log(2) #kpc
      vmax = 250 #km/s
      a    = 32  #kpc
      gm   = 4 * a * vmax**2 #kpc km/s

      #only needed for plots
      R = np.linalg.norm(PosStars[:,0:2], axis=1)
      r = np.linalg.norm(PosStars[:,0:3], axis=1)

      plt.figure()
      plt.hist(R, bins=50, density=True, alpha=0.5, label=r'$R$')
      plt.hist(r, bins=50, density=True, alpha=0.5, label=r'$r$')

      #exponential disk theory
      rs = np.linspace(0,60,61)
      f_of_r_of_r = rd**-2 * np.exp(-rs / rd) * rs
      plt.errorbar(rs, f_of_r_of_r, label=r'exp disk at $t=0$')
      plt.xlabel(r'$R$')
      plt.legend()

      #plot v_phi distribution
      plt.figure()
      plt.hist(v_phi, bins=50, density=True, alpha=0.5, label=r'$v_\phi$')

      # # circular velocities from sim assuming v_c(r)
      # v_c_interp = j_circ_interp/R_interp
      # arg_at_max_vc = np.argmax(v_c_interp)

      # interp = InterpolatedUnivariateSpline(R_interp[:arg_at_max_vc],
      #                                       v_c_interp[:arg_at_max_vc], k=1)
      # v_c_at_r = interp(r)
      # plt.hist(v_c_at_r, bins=50, density=True, alpha=0.5,
      #           label=r'$v_c(r_i)$ simulation')

      v_c_hernquist_r = np.sqrt(gm * r/(r+a)**2)
      plt.hist(v_c_hernquist_r, bins=50, density=True, alpha=0.5,
               label=r'$v_c(r_i)$ hernquist')

      #exponential disk hernquist velocity
      v_cs = np.linspace(1e-3,vmax,251) #km/s

      r_of_vc = (gm - 2*a*v_cs**2 -np.sqrt(gm**2 -4*a*gm*v_cs**2)) / (2 * v_cs**2)

      f_of_r_of_vc = rd**-2 * np.exp(-r_of_vc / rd) * r_of_vc

      dr_dv = (4*a*gm*v_cs / np.sqrt(gm**2 - 4*a*gm*v_cs**2) - 4*a*v_cs) / (
        2*v_cs**2) - (-np.sqrt(gm**2 - 4*a*gm*v_cs**2) - 2*a*v_cs**2 + gm)/v_cs**3

      f_of_vc_of_vc = f_of_r_of_vc * dr_dv

      plt.errorbar(v_cs, f_of_vc_of_vc, label=r'Exp disk w/ Hernquist $v_c$')
      plt.xlabel(r'$v_\phi$')
      plt.legend()

      #R vs v_phi
      plt.figure()
      plt.errorbar(R, v_phi, ls='', fmt='.', label='stars')
      plt.errorbar(R_interp, j_circ_interp / R_interp, label='interpolated v_c(e)')
      vc = np.sqrt(gm * rs/(rs+a)**2)
      plt.errorbar(rs, vc, label='hernquist v_c(r)')
      plt.errorbar(r_of_vc, v_cs, label='hernquist r(v_c)')
      plt.xlabel(r'$R$ [kpc]')
      plt.ylabel(r'$v_\phi$ [km/s]')
      plt.legend()

      return

    #calculate kappas
    K_tot = np.sum(np.square(np.linalg.norm(VelStars,axis=1)))

    K_rot = np.sum(np.square(v_phi))
    K_co  = np.sum(np.square(v_phi[v_phi > 0]))

    kappa[j]    = K_rot / K_tot
    kappa_co[j] = K_co  / K_tot


  #make plots
  fig, axs = plt.subplots(nrows=1, ncols=3, sharex=True, sharey=True,
                          figsize=(15,6))
  fig.subplots_adjust(hspace=0,wspace=0)

  time = np.linspace(0, t_tot, nsnaps)

  im = axs[0].scatter(disp_v_z, kappa, c=time, marker='s',
                      cmap='viridis')
  axs[1].scatter(disp_v_rho, kappa, c=time, marker='s',
                 cmap='viridis')
  axs[2].scatter(disp_v_phi, kappa, c=time, marker='s',
                 cmap='viridis')

  axs[0].scatter(disp_v_z, kappa_co, c=time, marker='o',
                  cmap='viridis')
  axs[1].scatter(disp_v_rho, kappa_co, c=time, marker='o',
                  cmap='viridis')
  axs[2].scatter(disp_v_phi, kappa_co, c=time, marker='o',
                  cmap='viridis')

  # v_circ = 200
  # v_circ = median_v_phi

  # kappa_fixed_circ = (v_circ**2 + disp_v_phi**2) / (
  #                     v_circ**2 + disp_v_phi**2 + disp_v_rho**2 + disp_v_z**2)

  # axs[0].scatter(disp_v_z,   kappa_fixed_circ, marker='+', #c=time,
  #                cmap='viridis')
  # axs[1].scatter(disp_v_rho, kappa_fixed_circ, marker='+', #c=time,
  #                cmap='viridis')
  # axs[2].scatter(disp_v_phi, kappa_fixed_circ, marker='+', #c=time,
  #                cmap='viridis')

  # kappa_varing_circ = (mean_v_phi**2 + disp_v_phi**2) / (
  #                      mean_v_phi**2 + disp_v_phi**2 + disp_v_rho**2 + disp_v_z**2)

  # axs[0].scatter(disp_v_z,   kappa_varing_circ, marker='*', #c=time,
  #                cmap='viridis')
  # axs[1].scatter(disp_v_rho, kappa_varing_circ, marker='*', #c=time,
  #                cmap='viridis')
  # axs[2].scatter(disp_v_phi, kappa_varing_circ, marker='*', #c=time,
  #                cmap='viridis')

  # axs[2].legend([r'$\kappa_{rot}$', r'$\kappa_{co}$'],
  #               framealpha=0)
  axs[2].legend([r'$\kappa_{rot}$', r'$\kappa$ (max($v_c$))',
                 r'$\kappa$ (mean($v_\phi$))'],
                framealpha=0)
  axs[0].set_xlabel(r'$\sigma_{v_z}$ [km/s]')
  axs[1].set_xlabel(r'$\sigma_{v_r}$ [km/s]')
  axs[2].set_xlabel(r'$\sigma_{v_\phi}$ [km/s]')
  axs[0].set_ylabel(r'$\kappa$')
  axs[0].grid()
  axs[1].grid()
  axs[2].grid()
  axs[0].set_xlim((0,150))
  axs[0].set_ylim((0,1))

  fig.subplots_adjust(right=0.85)
  cbar_ax = fig.add_axes([0.86, 0.11, 0.03, 0.77])
  cbar = fig.colorbar(im, cax=cbar_ax)
  cbar.set_label(r'time [Gyr]')

  plt.show() if save:
    fname = '../galaxies/' + name + '/results/dispersion_kappa'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  disp_v_tot = np.sqrt(disp_v_phi**2 + disp_v_rho**2 + disp_v_z**2)
  q = v_circ**2 / disp_v_tot**2 - 1
  Mestel_mean_v_phi = np.sqrt(2) * gamma(1/2*q + 1) / gamma(1/2*q + 1/2
                                                            ) * disp_v_tot

  #time vs v_phi
  plt.figure()
  plt.errorbar(time, v_circ,       label=r'max($v_c(r)$)')
  plt.errorbar(time, median_v_phi, label=r'median$(v_\phi)$')
  plt.errorbar(time, mean_v_phi,   label=r'mean$(v_\phi)$')
  plt.errorbar(time, disp_v_phi,   label=r'$\sigma_{v_\phi}$')
  plt.errorbar(time, v_circ-disp_v_phi, label=r'max($v_c(r)$) - $\sigma_{v_\phi}$')
  plt.errorbar(time, Mestel_mean_v_phi, label=r'Mestel')

  plt.legend()

  plt.xlabel('time')
  plt.ylabel(r'$v_\phi$ [km/s]')

  #kappa_rot vs sigma_v_phi / sigma_v_tot
  plt.figure()
  disp_v_tot_2 = disp_v_phi**2 + disp_v_rho**2 + disp_v_z**2

  plt.scatter(disp_v_phi**2 / disp_v_tot_2, kappa,    c=time, marker='s')
  plt.scatter(disp_v_phi**2 / disp_v_tot_2, kappa_co, c=time, marker='o')

  plt.xlabel(r'$\sigma_{v_\phi}^2 / \sigma_{|v|}^2$')
  plt.ylabel(r'$\kappa$')

  plt.legend([r'$\kappa_{rot}$', r'$\kappa_{co}$'], framealpha=0)

  cbar = plt.colorbar()
  cbar.set_label('time [Gyr]')

  return

def plot_stellar_exponential_evolution(name='', save=False, ax=None):
  '''
  '''
  t_tot = 9.778943899409334 #Gyr

  if (('7p5' in name) or ('8p0' in name)):
    nsnaps = 401
  else:
    nsnaps = 102

  half_mass  = np.zeros(nsnaps)
  disk_expon = np.zeros(nsnaps)
  exp_to_med = np.zeros(nsnaps)

  for j in range(nsnaps):
    snap = '{0:03d}'.format(int(j))
    (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,
                MassStar, PosStars, VelStars, IDStars,
      j_z_stars, j_p_stars, j_c_stars, energy_stars,
      E_interp, j_circ_interp, R_interp
      ) = load_nbody.load_and_calculate(name, snap)

    #cylindrical radii
    # r = np.linalg.norm(PosStars, axis=1)
    R = np.linalg.norm((PosStars[:,0], PosStars[:,1]), axis=0)

    #find half mass radii quickly
    R_sort = np.argsort(R)
    if len(R)%2 == 0:
      half_mass_radii = (R[R_sort][len(R)>>1] + R[R_sort][(len(R)>>1)-1])/2
    else:
      half_mass_radii = R[R_sort][int(len(R)/2)]

    half_mass[j] = half_mass_radii

    #work out best fit
    likely = lambda R_d : -np.sum( np.log( R / np.square(R_d) * np.exp( -R / R_d ) ))

    R_d = minimize(likely, 1)
    R_d = R_d.x[0]

    disk_expon[j] = R_d

    # median = R_d * 1.6783469900166605
    median = - R_d * (lambertw(-1/2*np.exp(-1), -1).real + 1)

    exp_to_med[j] = median

  times = np.linspace(0, t_tot, nsnaps)

  plt.figure()
  plt.errorbar(times, half_mass, label='measured half mass radii')
  plt.errorbar(times, exp_to_med, label='fitted half mass radii')
  plt.errorbar(times, disk_expon, label='exponential fit')

  plt.xlabel(r'$t$ [Gyr]')
  plt.ylabel(r'$R_d$[kpc]')
  plt.xlim((0, t_tot))
  plt.ylim((0, 1.1*np.amax(half_mass)))
  plt.legend()

  plt.show() if save:
    fname = '../galaxies/' + name + '/results/radii_evolution'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  return

'''
###############################################################################
######################### all galaxies, all snapshots #########################
###############################################################################
'''
#TODO these are older, need to update
def save_plot_points():
  '''
  '''
  file = '../galaxies/time_fraction_points'

  if os.path.isfile(file+'.npy'):
    f_array = np.load(file+'.npy', allow_pickle=True)
    return(f_array)

  radii = [2.0, 5.0, 200.0]
  muse  = ['25', '5', '1']
  Mdm   = ['8p0', '7p5', '7p0', '6p5', '6p0']
  snaps = [ 400,   400,   101,   101,   101]

  n_mu = len(muse)
  n_M  = len(Mdm)
  n_r  = len(radii)

  f_cut0 = 0.9
  f_cut1 = 0.8
  f_cut2 = 0.7
  f_cut3 = 0.0

  f_array = [[], [], []]

  for i in range(n_mu):
    for j in range(n_M):

      #skip the loner
      if not((muse[i] == '1') and (Mdm[j] == '6p0')):

        #0.9, 0.8, 0.7, 0, n
        f_array[0].append([[], [], [], [], []])
        f_array[1].append([[], [], [], [], []])
        f_array[2].append([[], [], [], [], []])

        name = 'mu_{0}/fdisk_0p01_lgMdm_{1}_V200-200kmps'.format(
          muse[i], Mdm[j])
        for t in range(snaps[j]+1):
          snap = '{0:03d}'.format(int(t))
          print('\n' + name + '_snap_' + snap)

          (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,
                    MassStar, PosStars, VelStars, IDStars,
                    j_z_stars, j_p_stars, j_c_stars, energy_stars,
                    E_interp, j_circ_interp, R_interp
                    ) = load_nbody.load_and_calculate(name, snap)

          j_zonc_stars = j_z_stars / j_c_stars

          for k in range(n_r):

            interest_mask = np.linalg.norm(PosStars, axis=1) < radii[k]

            f_array[k][-1][0].append(sum(j_zonc_stars[interest_mask] < f_cut0))
            f_array[k][-1][1].append(sum(j_zonc_stars[interest_mask] < f_cut1))
            f_array[k][-1][2].append(sum(j_zonc_stars[interest_mask] < f_cut2))
            f_array[k][-1][3].append(sum(j_zonc_stars[interest_mask] < f_cut3))
            f_array[k][-1][4].append(sum(interest_mask))

  np.save(file, f_array)

  return(f_array)

def plot_disk_heating(smooth=False, save=False):
  '''
  '''
  f_array = save_plot_points()

  radii = ['2', '5', '200']
  rad__ = [r'$r < 2$kpc', r'$r < 5$kpc', 'All stars']
  f_cut = [0.9, 0.8, 0.7, 0.0]
  # muse  = ['25', '5', '1']
  # mu__  = ['-','--', '-.']
  muse  = ['5']
  mu__  = ['-']
  # muse  = ['25']
  Mdm   = ['8.0', '7.5', '7.0', '6.5', '6.0']
  M_col = ['C0',  'C1',  'C2',  'C3',  'C4']

  snaps = [ 400,   400,   101,   101,   101]

  t_mult = 3.086e21 / 1e5 / (1e9 * 365.25 * 24 * 3600)

  n_f  = len(f_cut)
  n_mu = len(muse)
  n_M  = len(Mdm)
  n_r  = len(radii)

  # f_cut0 = 0.9
  # f_cut1 = 0.8
  # f_cut2 = 0.7
  # f_cut3 = 0.0

  # for f_i in range(n_f):

  fig, axs = plt.subplots(nrows=2, ncols=n_r, sharex=True, sharey=True,
                          figsize=(12,7.2))
  fig.subplots_adjust(hspace=0.03,wspace=0.03)
  # axs[0,0].set_ylabel(r'fraction of stars with $j_z/j_c > $'+str(f_cut[2]))
  # axs[1,0].set_ylabel(r'2 times fraction of stars with $j_z/j_c < 0$')
  axs[0,0].set_ylabel(r'$j_z/j_c > $'+str(f_cut[2])+' fraction')
  axs[1,0].set_ylabel(r'spheroid fraction')

  for r_i in range(n_r):

    for M_i in range(n_M):
      for mu_i in range(n_mu-1,-1,-1):
        if not((muse[mu_i] == '1') and (Mdm[M_i] == '6.0')):
          index = n_M * mu_i + M_i

          #thin disk
          n  = np.array(f_array[r_i][index][n_f])

          y  = 1 - f_array[r_i][index][2] / n
          if smooth:
            y  = savgol_filter(y[:-1], int(snaps[M_i]/4.01), 5)

          dy = 1/np.sqrt(n)
          if smooth:
            dy = savgol_filter(dy[:-1], int(snaps[M_i]/4.01), 5)

          axs[0,r_i].plot(np.linspace(0,10*t_mult,len(y)),  y,
                          # label='$\log(M_{DM}/M_\odot)$='+Mdm[M_i]+
                          # ', $\mu$='+muse[mu_i],
                          # label='$\log(M_{DM}/M_\odot)$='+Mdm[M_i],
                          label=Mdm[M_i],
                          ls=mu__[mu_i], color=M_col[M_i])
          axs[0,r_i].fill_between(np.linspace(0,10*t_mult,len(y)), y+dy, y-dy,
                                color=M_col[M_i], alpha=0.2)

          axs[0,r_i].set_title(rad__[r_i])
          # axs[0,r_i].set_xlim((0,10*t_mult))
          axs[0,r_i].set_ylim((0,1))

          #bulge
          y  = 2 * (f_array[r_i][index][3] / n)
          if smooth:
            y  = savgol_filter(y[:-1], int(snaps[M_i]/4.01), 5)

          dy = 1/np.sqrt(n)
          if smooth:
            dy = savgol_filter(dy[:-1], int(snaps[M_i]/4.01), 5)

          axs[1,r_i].plot(np.linspace(0,10*t_mult,len(y)),  y,
                          ls=mu__[mu_i], color=M_col[M_i])
          axs[1,r_i].fill_between(np.linspace(0,10*t_mult,len(y)), y+dy, y-dy,
                                color=M_col[M_i], alpha=0.2)

          axs[1,r_i].set_xlim((0,10*t_mult))
          axs[1,r_i].set_ylim((0,0.999))

  # axs[1,n_r-1].legend()
  axs[0,n_r-1].legend(bbox_to_anchor=(0.98, 1.05), loc='upper left',
                      title='$\log(M_{DM}/M_\odot)$')
  axs[1,int((n_r-1)/2)].set_xlabel(r'time [Gyr]')
  # fname = '../galaxies/time_frac_cut{0}'.format(f_nme[f_i])

  plt.show() if save:
    fname = '../galaxies/components_over_time'
    plt.savefig(fname ,bbox_inches='tight')
    plt.close()

  return

def plot_disk_evolution(smooth=False, save=False):
  '''
  '''
  f_array = save_plot_points()

  radii = ['2', '5', '200']
  f_cut = [0.9, 0.8, 0.7, 0.0]
  # muse  = ['25', '5', '1']
  mu__  = ['-','--', '-.']
  muse  = ['25']
  Mdm   = ['8.0', '7.5', '7.0', '6.5', '6.0']
  M_col = ['C0',  'C1',  'C2',  'C3',  'C4']

  snaps = [ 400,   400,   101,   101,   101]

  grav_const   = 4.301e4 #kpc (km/s)^2 / (10^10Msun) (source ??)
  hubble_const = 0.06766 #km/s/kpc (Planck 2018)
  rho_crit     = 3*hubble_const**2 / (8 * np.pi * grav_const) #10^10Msun/kpc^3
  pc_on_m      = 3.0857e16 #pc/m = kpc/km (wiki)
  gyr_on_s     = 3.15576e16 #gyr/s
  t_tot = 9.778943899409334 #Gyr

  n_f  = len(f_cut)
  n_mu = len(muse)
  n_M  = len(Mdm)
  n_r  = len(radii)

  # f_cut0 = 0.9
  # f_cut1 = 0.8
  # f_cut2 = 0.7
  # f_cut3 = 0.0

  # for f_i in range(n_f):

  fig, axs = plt.subplots(nrows=2, ncols=n_r, sharex=True, sharey=True,
                          figsize=(20,12))
  fig.subplots_adjust(hspace=0.03,wspace=0.03)
  # axs[0,0].set_ylabel(r'fraction of stars with $j_z/j_c > $'+str(f_cut[2]))
  # axs[1,0].set_ylabel(r'2 times fraction of stars with $j_z/j_c < 0$')
  axs[0,0].set_ylabel(r'$j_z/j_c > $'+str(f_cut[2])+' fraction')
  axs[1,0].set_ylabel(r'spheroid fraction')

  for r_i in range(n_r):

    for M_i in range(n_M):
      for mu_i in range(n_mu-1,-1,-1):
        if not((muse[mu_i] == '1') and (Mdm[M_i] == '6.0')):
          index = n_M * mu_i + M_i


          t_c = (grav_const**2 * 200 * rho_crit * 10**(float(Mdm[M_i])-10) /
                 150**3 * pc_on_m / gyr_on_s)

          time = np.linspace(0,t_tot,snaps[M_i]+1)
          scale_time = np.log10(time * t_c) + 4

          #thin disk
          n  = np.array(f_array[r_i][index][n_f])

          y  = 1 - f_array[r_i][index][2] / n
          if smooth:
            y  = savgol_filter(y[:-1], int(snaps[M_i]/4.01), 5)

          dy = 1/np.sqrt(n)
          if smooth:
            dy = savgol_filter(dy[:-1], int(snaps[M_i]/4.01), 5)

          axs[0,r_i].plot(scale_time,  y,
                          # label='$\log(M_{DM}/M_\odot)$='+Mdm[M_i]+
                          # ', $\mu$='+muse[mu_i],
                          label='$\log(M_{DM}/M_\odot)$='+Mdm[M_i],
                          ls=mu__[mu_i], color=M_col[M_i])
          axs[0,r_i].fill_between(scale_time, y+dy, y-dy,
                                color=M_col[M_i], alpha=0.2)

          axs[0,r_i].set_title(r'$r<$'+radii[r_i]+'kpc')
          axs[0,r_i].set_xlim((-4,1))
          axs[0,r_i].set_ylim((0,1))
          axs[0,r_i].grid(b=True)

          #bulge
          y  = 2 * (f_array[r_i][index][3] / n)
          if smooth:
            y  = savgol_filter(y[:-1], int(snaps[M_i]/4.01), 5)

          dy = 1/np.sqrt(n)
          if smooth:
            dy = savgol_filter(dy[:-1], int(snaps[M_i]/4.01), 5)

          axs[1,r_i].plot(scale_time,  y,
                          label='Mdm='+Mdm[M_i]+', mu='+muse[mu_i],
                          ls=mu__[mu_i], color=M_col[M_i])
          axs[1,r_i].fill_between(scale_time, y+dy, y-dy,
                                color=M_col[M_i], alpha=0.2)

          axs[1,r_i].set_xlim((-4,1))
          axs[1,r_i].set_ylim((0,1))
          axs[1,r_i].grid(b=True)

  # axs[1,n_r-1].legend()
  axs[0,n_r-1].legend(bbox_to_anchor=(0.98, 1), loc='upper left')
  axs[1,int((n_r-1)/2)].set_xlabel(r'$\log \tau$ [$\times 10^4$]')
  # fname = '../galaxies/time_frac_cut{0}'.format(f_nme[f_i])

  #manual guess
  x = np.linspace(-4,1)
  y = ((1 - np.tanh(1.5*(x+1.8))) / 2) * 0.85
  axs[0,0].errorbar(x, y, c='k', ls=':', linewidth=5, alpha=0.8)
  x = np.linspace(-4,1)
  y = ((1 - np.tanh(1.4*(x+1.2))) / 2) * 0.95
  axs[0,1].errorbar(x, y, c='k', ls=':', linewidth=5, alpha=0.8)
  x = np.linspace(-4,1)
  y = ((1 - np.tanh(1.0*(x+0.45))) / 2) * 1
  axs[0,2].errorbar(x, y, c='k', ls=':', linewidth=5, alpha=0.8)

  x = np.linspace(-4,1)
  y = ((np.tanh(1.3*(x+1.4)) +1) / 2) * 1
  axs[1,0].errorbar(x, y, c='k', ls=':', linewidth=5, alpha=0.8)
  x = np.linspace(-4,1)
  y = ((np.tanh(1.3*(x+0.8)) +1) / 2) * 1
  axs[1,1].errorbar(x, y, c='k', ls=':', linewidth=5, alpha=0.8)
  x = np.linspace(-4,1)
  y = ((np.tanh(0.9*(x-0.1)) +1) / 2) * 1
  axs[1,2].errorbar(x, y, c='k', ls=':', linewidth=5, alpha=0.8)

  plt.show() if save:
    fname = '../galaxies/components_over_scale_time'
    plt.savefig(fname ,bbox_inches='tight')
    plt.close()

  return

def plot_all_disk_scale_height(save=True):
  '''
  '''
  t_tot = 9.778943899409334 #Gyr
  integ_const = 6.579736267392906
  one = 1

  names = ['mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps',
           'mu_25/fdisk_0p01_lgMdm_7p5_V200-200kmps',
           'mu_25/fdisk_0p01_lgMdm_7p0_V200-200kmps',
           'mu_25/fdisk_0p01_lgMdm_6p5_V200-200kmps',
           'mu_25/fdisk_0p01_lgMdm_6p0_V200-200kmps']

  dm_str = ['8.0', '7.5', '7.0', '6.5', '6.0']
  c_str  = ['C0',  'C1',  'C2',  'C3',  'C4']

  plt.figure(figsize=(8,5.85))

  for j in range(len(names)):

    if (('7p5' in names[j]) or ('8p0' in names[j])):
      nsnaps = 401
    else:
      nsnaps = 102

    (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,   PotDMs,
                MassStar, PosStars, VelStars, IDStars, PotStars
                ) = load_nbody.load_snapshot(names[j], '000')

    #cylindrical positions
    R = np.linalg.norm((PosStars[:,0], PosStars[:,1]), axis=0)

    R_sort = np.argsort(R)
    if len(R)%2 == 0:
      half_mass_radii = (R[R_sort][len(R)>>1] + R[R_sort][(len(R)>>1)-1])/2
    else:
      half_mass_radii = R[R_sort][int(len(R)/2)]

    area = np.pi * ((half_mass_radii + one)**2 - (half_mass_radii - one)**2)

    z_heights = np.zeros(nsnaps)
    rho_0_val = np.zeros(nsnaps)

    for i in range(nsnaps):
      if i != 0:
        snap = '{0:03d}'.format(int(i))
        (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,   PotDMs,
                    MassStar, PosStars, VelStars, IDStars, PotStars
                    ) = load_nbody.load_snapshot(names[j], snap)

        #cylindrical positions
        R = np.linalg.norm((PosStars[:,0], PosStars[:,1]), axis=0)

      #cylidrical mask
      R_mask = np.logical_and(R < (half_mass_radii + one),
                              R > (half_mass_radii - one))
      #zs to use
      zs = PosStars[R_mask,2]
      zs = zs - np.mean(zs)

      #sech^2 pdf
      likely = lambda z_0 : -np.sum( np.log( 1 / (np.abs(z_0) * 4 *
                                                  np.cosh(zs / (2*z_0))**2 )))
      #fit and save sech^2 profile
      z_0 = minimize(likely, 0.2)
      z_0 = np.abs(z_0.x[0])
      z_heights[i] = z_0

      # #median |z|
      # z_0 = np.median(np.abs(zs))
      # z_heights[i] = z_0

      #calculate and save
      rho_0 = MassStar*10**10 * np.sum(R_mask) / (4 * area * z_0)
      rho_0_val[i] = rho_0

      #save initial velocity dispersion
      if i == 0:
        sigma_v_z_0 = np.std(VelStars[R_mask,2])

    const = 286186 * MassDM
    nu_t = np.sqrt(4 * np.pi * rho_0_val * 43010.0 * 10**-10)

    t = np.linspace(0, t_tot, nsnaps)

    #main observation
    plt.errorbar(t, z_heights, c=c_str[j], label=r'$M_{dm} =$'+dm_str[j]+' data')

    z_calc = np.sqrt( 2 / (nu_t**2 * integ_const) * ( t * const + sigma_v_z_0**2 ) )
    plt.errorbar(t, z_calc, c=c_str[j], ls='--',
                 label=r'$M_{dm} =$'+dm_str[j]+' prediction')

  plt.xlabel(r'$t$ [Gyr]')
  plt.ylabel(r'$h_z$ [kpc]')
  plt.legend(bbox_to_anchor=(0.98, 1.03), loc='upper left')
  plt.grid()
  plt.xlim((0,t_tot))
  plt.ylim((0,5))

  plt.show() if save:
    fname = '../galaxies/disk_height_t'
    plt.savefig(fname, bbox_inches='tight')
    # plt.close()

  return

def plot_dm_rotation(name, align=False, save=False):
  '''
  '''
  t_tot = 9.778943899409334 #Gyr

  if (('7p5' in name) or ('8p0' in name)):
    nsnaps = 401
  else:
    nsnaps = 102

  angular_momentum = np.zeros((nsnaps, 3))
  angular_momentum_stars = np.zeros((nsnaps, 3))

  for i in range(nsnaps):

    if align:
      (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,
                  MassStar, PosStars, VelStars, IDStars,
        j_z_stars, j_p_stars, j_c_stars, energy_stars,
        E_interp, j_circ_interp, R_interp
        ) = load_nbody.load_and_calculate(name, '{0:03d}'.format(int(i)))

    else:
      (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,   PotDMs,
        MassStar, PosStars, VelStars, IDStars, PotStars
        ) = load_nbody.load_snapshot(name, '{0:03d}'.format(int(i)))

    j_dm = np.cross(PosDMs, VelDMs)
    j_tot = MassDM * np.sum(j_dm, axis=0)

    j_dm_stars = np.cross(PosStars, VelStars)
    j_tot_stars = MassStar * np.sum(j_dm_stars, axis=0)

    angular_momentum[i] = j_tot
    angular_momentum_stars[i] = j_tot_stars

  t = np.linspace(0, t_tot, nsnaps)

  fig, ax = plt.subplots(nrows=3, ncols=1, sharex=True, sharey=True, figsize=(4,8))
  fig.subplots_adjust(hspace=0.03,wspace=0)

  ax[0].errorbar(t, angular_momentum[:, 0], fmt='.', ls='-', c='C0',
                 label='Dark Matter')
  ax[1].errorbar(t, angular_momentum[:, 1], fmt='.', ls='-', c='C0')
  ax[2].errorbar(t, angular_momentum[:, 2], fmt='.', ls='-', c='C0')

  ax[0].errorbar(t, angular_momentum_stars[:, 0], fmt='.', ls='-', c='C1',
                 label='Stars')
  ax[1].errorbar(t, angular_momentum_stars[:, 1], fmt='.', ls='-', c='C1')
  ax[2].errorbar(t, angular_momentum_stars[:, 2], fmt='.', ls='-', c='C1')

  ax[2].set_xlabel('time [Gyr]')
  ax[0].set_ylabel(r'$J_x$')
  ax[1].set_ylabel(r'$J_y$ [10$^{10}$ M$_\odot$ kpc km/s]')
  ax[2].set_ylabel(r'$J_z$')

  ax[0].legend()
  ax[0].set_xlim([0, t_tot])

  plt.show() if save:
    if align: a='aligned'
    else:     a='free'
    plt.savefig('../galaxies/'+name+'/results/dm_am_'+a,
                bbox_inches='tight')
    plt.close()

  V200 = 200 #km/s
  R200 = 200 #kpc

  tot_mass = MassDM * len(IDDMs)
  tot_mass_stars = MassStar * len(IDStars)

  fig, ax = plt.subplots(nrows=3, ncols=1, sharex=True, sharey=True, figsize=(4,8))
  fig.subplots_adjust(hspace=0.03,wspace=0)

  ax[0].errorbar(t, angular_momentum[:, 0] / (tot_mass * V200 * R200),
                 fmt='.', ls='-', c='C0', label='Dark Matter')
  ax[1].errorbar(t, angular_momentum[:, 1] / (tot_mass * V200 * R200),
                 fmt='.', ls='-', c='C0')
  ax[2].errorbar(t, angular_momentum[:, 2] / (tot_mass * V200 * R200),
                 fmt='.', ls='-', c='C0')

  ax[0].errorbar(t, angular_momentum_stars[:, 0] / (tot_mass_stars * V200 * R200),
                 fmt='.', ls='-', c='C1', label='Stars')
  ax[1].errorbar(t, angular_momentum_stars[:, 1] / (tot_mass_stars * V200 * R200),
                 fmt='.', ls='-', c='C1')
  ax[2].errorbar(t, angular_momentum_stars[:, 2] / (tot_mass_stars * V200 * R200),
                 fmt='.', ls='-', c='C1')

  ax[2].set_xlabel('time [Gyr]')
  ax[0].set_ylabel(r'$j_x / R_{200} V_{200}$')
  ax[1].set_ylabel(r'$j_y / R_{200} V_{200}$')
  ax[2].set_ylabel(r'$j_z / R_{200} V_{200}$')

  ax[0].legend()
  ax[0].set_xlim([0, t_tot])

  plt.show() if save:
    if align: a='aligned'
    else:     a='free'
    plt.savefig('../galaxies/'+name+'/results/dm_am_normed_'+a,
                bbox_inches='tight')
    plt.close()

  plt.figure()

  cos_theta = np.sum(np.multiply(angular_momentum, angular_momentum_stars), axis=1
                     )/(np.linalg.norm(angular_momentum, axis=1) *
                        np.linalg.norm(angular_momentum_stars, axis=1))

  plt.errorbar(t, cos_theta, fmt='.', ls='-', c='C0')

  plt.xlabel('time [Gyr]')
  plt.ylabel(r'$\cos(\theta)$')
  plt.xlim([0, t_tot])
  plt.ylim([0, 1])

  plt.show() if save:
    if align: a='aligned'
    else:     a='free'
    plt.savefig('../galaxies/'+name+'/results/dm_am_angle_'+a,
                bbox_inches='tight')
    plt.close()

  return

def a_to_z(a):
  '''Convert scale factor to redshift.
  '''
  z = 1/a - 1
  return(z)

#to get star particle age
def lbt(a,omm=0.307,oml=0.693,h=0.6777):
    z = 1.0/a-1
    t = np.zeros(len(z))
    for i in range(len(z)):
        t[i] = 1e+3*3.086e+16/(3.154e+7*1e+9)*(1.0/(100*h))*quad(lambda z: 1/((1+z)*np.sqrt(omm*(1+z)**3+oml)),0,z[i])[0]#in billion years
    return(t)

def plot_star_formation_history_z(star_pos, star_mass, star_formation_a,
                                  top_leaf_id, snap, sim, apature=30):
  '''Assume already centred.
  Plot star formation history of stars within apature at z=0.
  '''
  onepointone = 1.1

  if apature != False:
    star_r = np.linalg.norm(star_pos, axis=1)

    apature_mask = (star_r < apature)

    star_formation_a = star_formation_a[apature_mask]
    star_mass        = star_mass[apature_mask]

  star_formation_z = a_to_z(star_formation_a)
  # log_star_formation_z = np.log10(star_formation_z + 1)

  star_formation_t = lbt(star_formation_a)

  arg_order = np.argsort(star_formation_z)
  mass_order = star_mass[arg_order]
  cum_z    = star_formation_z[arg_order]
  cum_t    = star_formation_t[arg_order]
  cum_mass = np.flip(np.cumsum(np.flip(mass_order)))

  snap_z = np.array([0.101, 0.183, 0.271, 0.366, 0.503, 0.615, 0.736,
                     0.865, 1.004, 1.259, 1.487, 1.737, 2.012, 1000])
  snap_a = 1 / (1 + snap_z)
  snap_t = lbt(snap_a)

  fig = plt.figure()
  fig.set_size_inches(8, 5, forward=True)

  ax = plt.subplot(111)
  ax2 = ax.twiny()

  (star_mass_formed ,_,_
   ) = binned_statistic(star_formation_z, star_mass, statistic='sum',
                        bins = snap_z)

  z0_mass = sum(star_mass_formed)

  ax.vlines(snap_z, 0, onepointone*z0_mass, linestyles='--', alpha=0.4)

  ax.errorbar(cum_z, cum_mass, fmt=',', linestyle='-')

  if not apature:
    ax.set_ylabel(r'$M_\star ($subhalo, $z=0)$ [$10^{10} $M$_\odot$]')
  else:
    ax.set_ylabel(r'$M_\star (<%d$kpc, $z=0)$ [$10^{10} $M$_\odot$]'%(apature))
  ax.set_xlabel(r'$z$')

  ax2.set_xlabel(r'$t [Gyr]$')
  ax2.set_xticks(snap_z[:-1:2])
  ax2.set_xticklabels(np.round(snap_t[:-1:2],1))

  print(snap_z[:-1])
  print(snap_z[:-1:2])
  print(snap_t[:-1])
  print(snap_t[:-1:2])

  # ax0.set_xlim((0,2.012))
  ax.set_xlim((0,2.012))
  ax.set_ylim((0, z0_mass*onepointone))

  # fname = '/home/mwilkins/EAGLE/plots/' + sim + '/topleaf' + str(top_leaf_id) + 'snap' + snap
  # plt.savefig(fname, bbox_inches="tight")
  # plt.close()

  return

def plot_star_formation_history(star_pos, star_mass, star_formation_a,
                                top_leaf_id, snap, sim, apature=30):
  '''Assume already centred.
  Plot star formation history of stars within apature at z=0.
  '''
  onepointone = 1.1

  if apature != False:
    star_r = np.linalg.norm(star_pos, axis=1)

    apature_mask = (star_r < apature)

    star_formation_a = star_formation_a[apature_mask]
    star_mass        = star_mass[apature_mask]

  star_formation_z = 1/star_formation_a - 1
  # log_star_formation_z = np.log10(star_formation_z + 1)

  star_formation_t = lbt(star_formation_a)

  arg_order = np.argsort(star_formation_t)
  mass_order = star_mass[arg_order]
  cum_z    = star_formation_z[arg_order]
  cum_t    = star_formation_t[arg_order]
  cum_mass = np.flip(np.cumsum(np.flip(mass_order)))

  snap_z = np.array([0.0, 0.101, 0.183, 0.271, 0.366, 0.503, 0.615, 0.736,
                     0.865, 1.004, 1.259, 1.487, 1.737, 2.012, 1000])
  snap_a = 1 / (1 + snap_z)
  snap_t = lbt(snap_a)

  fig = plt.figure()
  fig.set_size_inches(8, 5, forward=True)

  ax = plt.subplot(111)
  ax2 = ax.twiny()

  (star_mass_formed ,_,_
   ) = binned_statistic(star_formation_z, star_mass, statistic='sum',
                        bins = snap_z)

  z0_mass = sum(star_mass_formed)

  ax.vlines(snap_t, 0, onepointone*z0_mass, ls='--', alpha=0.4)


  ax.errorbar(cum_t, cum_mass, fmt=',', ls='-')

  # ax2.set_xlim((0, snap_z[-2]))
  # ax2.errorbar(cum_z, cum_mass, fmt=',', ls='-')

  if not apature:
    ax.set_ylabel(r'$M_\star ($subhalo, $z=0)$ [$10^{10} $M$_\odot$]')
  else:
    ax.set_ylabel(r'$M_\star (<%d$kpc, $z=0)$ [$10^{10} $M$_\odot$]'%(apature))
  ax.set_xlabel(r'$t$ [Gyr]')

  ax2.set_xlabel(r'$z$')
  ax2.set_xticks(np.concatenate((snap_t[:-1:2], [snap_t[-2]])))
  ax2.set_xticklabels(np.round(snap_z[:-1:2],2))

  # ax0.set_xlim((0,2.012))
  ax.set_xlim((0,snap_t[-2]))
  ax.set_ylim((0, z0_mass*onepointone))

  # fname = '/home/mwilkins/EAGLE/plots/' + sim + '/topleaf' + str(top_leaf_id) + 'sfh'
  # plt.savefig(fname, bbox_inches="tight")
  # plt.close()

  form_z = star_formation_z
  mass   = star_mass


  arg_order  = np.argsort(form_z)
  cum_z      = form_z[arg_order]
  mass_order = mass[arg_order]
  cum_mass   = np.flip(np.cumsum(np.flip(mass_order)))

  star_mass_tot     = cum_mass[0]
  log_star_mass_tot = np.log10(star_mass_tot) + 10

  print(star_mass_tot)

  z95 = cum_z[cum_mass < 0.95 * star_mass_tot][0]
  z90 = cum_z[cum_mass < 0.90 * star_mass_tot][0]
  z85 = cum_z[cum_mass < 0.85 * star_mass_tot][0]
  z80 = cum_z[cum_mass < 0.80 * star_mass_tot][0]
  z75 = cum_z[cum_mass < 0.75 * star_mass_tot][0]
  z50 = cum_z[cum_mass < 0.50 * star_mass_tot][0]

  print(z95)
  print(z90)
  print(z85)
  print(z80)
  print(z75)
  print(z50)

  pct05 = cum_mass[cum_z > 0.503][0] / star_mass_tot
  pct10 = cum_mass[cum_z > 1.004][0] / star_mass_tot
  pct20 = cum_mass[cum_z > 2.012][0] / star_mass_tot

  print(pct05)
  print(pct10)
  print(pct20)

  return

def erf_dash(y):
  '''Derivative of the error function. It is just the pdf value of standard Gaussian.
  '''
  out = 2 / np.sqrt(np.pi) * np.exp(-y*y)
  return(out)

def slowly_varying_functions(name='', save=False):
  '''Plot "slowly varying functions."
  '''
  G = lambda y: (erf(y) - y * erf_dash(y)) / (2 * y**3)
  H = lambda y: ((2*y**2 - 1) * erf(y) + y * erf_dash(y)) / (2 * y**3)

  ys = np.linspace(0, 1.5, 201)

  gs = G(ys)
  hs = H(ys)

  fig = plt.figure(figsize=(5,4))

  plt.plot(ys, ys*gs, label=r'$\gamma G(\gamma)$')
  # plt.plot(ys, ys*2/(3*np.sqrt(np.pi)), label=r'$\gamma \frac{2}{3 \sqrt{\pi}} $',
  #           c='C0', ls='--')
  # plt.plot(ys, ys*2/(3*np.sqrt(np.pi)) - ys**3 *2/(5*np.sqrt(np.pi)),
  #           label=r'$\gamma \frac{2}{3 \sqrt{\pi}} - \gamma^3 \frac{2}{5 \sqrt{\pi}}$',
  #           c='C0', ls='--')
  plt.plot(ys, gs, label=r'$G(\gamma)$')
  # plt.plot(ys, 2/(3*np.sqrt(np.pi)) - ys**2*2/(5*np.sqrt(np.pi)),
  #           label=r'$\frac{2}{3 \sqrt{\pi}} - \gamma^2 \frac{2}{5 \sqrt{\pi}}$',
  #           c='C1', ls='--')
  plt.plot(ys, hs, label=r'$H(\gamma)$')
  # plt.plot(ys, 4/(3*np.sqrt(np.pi)) - ys**2*4/(16*np.sqrt(np.pi)),
  #           label=r'$\frac{4}{3 \sqrt{\pi}} - \gamma^2 \frac{4}{15 \sqrt{\pi}}$',
  #           c='C2', ls='--')

  # plt.plot(ys, gs - (ys*gs)**2, label=r'$G(\gamma) - [\gamma G(\gamma)]^2 $')

  plt.xlabel(r'$\gamma = \langle v_{tot} \rangle / (\sqrt{2} \sigma_{DM}) $')

  plt.xlim(0, ys[-1])
  plt.ylim(0, 1.2)

  plt.legend(loc='upper center', ncol=2, frameon=False)

  plt.show() if save:
    plt.savefig(name, bbox_inches='tight')
    plt.close()

  return

def plot_sigma_ratios(galaxy_name_list, snaps_list, save_name_append='diff',
                     save=True, name=''):
  burn_in = BURN_IN

  #load data
  (scaled_mean_v_phi2, tau_v, bin_v, sim_v, scaled_sigma_z2,   tau_z,   bin_z,   sim_z,
   scaled_sigma_R2,    tau_R, bin_R, sim_R, scaled_sigma_phi2, tau_phi, bin_phi, sim_phi,
   t_c_dm, delta_dm, upsilon_dm, t_c_circ, upsilon_circ
   ) = get_combined_data(save_name_append=save_name_append)

  #the actual fitted values
  (alpha_v, beta_v, gamma_v, std_v, log_k_v,
   alpha_z, beta_z, gamma_z, std_z, log_k_z,
   alpha_R, beta_R, gamma_R, std_R, log_k_R,
   alpha_phi, beta_phi, gamma_phi, std_phi, log_k_phi
   ) = find_correct_fit(save_name_append=save_name_append, plot=False, save=False)

  bin_edges = get_bin_edges(save_name_append)

  #plot
  fig = plt.figure()
  fig.set_size_inches(12, 12, forward=True)
  #TODO this can be done more cleanly with plt.subplots
  ax00 = plt.subplot(3,3,1)
  ax01 = plt.subplot(3,3,2,sharex=ax00,sharey=ax00)
  ax02 = plt.subplot(3,3,3,sharex=ax00,sharey=ax00)
  ax10 = plt.subplot(3,3,4,sharex=ax00,sharey=ax00)
  ax11 = plt.subplot(3,3,5,sharex=ax00,sharey=ax00)
  ax12 = plt.subplot(3,3,6,sharex=ax00,sharey=ax00)
  ax20 = plt.subplot(3,3,7,sharex=ax00,sharey=ax00)
  ax21 = plt.subplot(3,3,8,sharex=ax00,sharey=ax00)
  ax22 = plt.subplot(3,3,9,sharex=ax00,sharey=ax00)

  fig.subplots_adjust(hspace=0.03,wspace=0.03)

  log_tau_th = np.linspace(-5, 0.5)

  (_ ,_, delta_dm, upsilon_dm, upsilon_circ
   ) = get_constants(bin_edges, save_name_append, 0)
  log_delta_25 = np.log10(delta_dm)[0]
  log_delta_50 = np.log10(delta_dm)[1]
  log_delta_75 = np.log10(delta_dm)[2]
  log_upsilon_25 = np.log10(upsilon_dm)[0]
  log_upsilon_50 = np.log10(upsilon_dm)[1]
  log_upsilon_75 = np.log10(upsilon_dm)[2]

  #asymptotic time
  log_z_ath_25 = get_theory_asymptote(log_tau_th, log_delta_25, log_upsilon_25,
                                      alpha_z, beta_z, gamma_z, log_k_z)
  log_z_ath_50 = get_theory_asymptote(log_tau_th, log_delta_50, log_upsilon_50,
                                      alpha_z, beta_z, gamma_z, log_k_z)
  log_z_ath_75 = get_theory_asymptote(log_tau_th, log_delta_75, log_upsilon_75,
                                      alpha_z, beta_z, gamma_z, log_k_z)
  log_R_ath_25 = get_theory_asymptote(log_tau_th, log_delta_25, log_upsilon_25,
                                      alpha_R, beta_R, gamma_R, log_k_R)
  log_R_ath_50 = get_theory_asymptote(log_tau_th, log_delta_50, log_upsilon_50,
                                      alpha_R, beta_R, gamma_R, log_k_R)
  log_R_ath_75 = get_theory_asymptote(log_tau_th, log_delta_75, log_upsilon_75,
                                      alpha_R, beta_R, gamma_R, log_k_R)
  log_phi_ath_25 = get_theory_asymptote(log_tau_th, log_delta_25, log_upsilon_25,
                                        alpha_phi, beta_phi, gamma_phi, log_k_phi)
  log_phi_ath_50 = get_theory_asymptote(log_tau_th, log_delta_50, log_upsilon_50,
                                        alpha_phi, beta_phi, gamma_phi, log_k_phi)
  log_phi_ath_75 = get_theory_asymptote(log_tau_th, log_delta_75, log_upsilon_75,
                                        alpha_phi, beta_phi, gamma_phi, log_k_phi)

  #plot
  ax00.errorbar(log_tau_th, log_R_ath_25 - log_z_ath_25,
                ls='--', c='k', linewidth=2)
  ax01.errorbar(log_tau_th, log_R_ath_50 - log_z_ath_50,
                ls='--', c='k', linewidth=2)
  ax02.errorbar(log_tau_th, log_R_ath_75 - log_z_ath_75,
                ls='--', c='k', linewidth=2)
  ax10.errorbar(log_tau_th, log_phi_ath_25 - log_R_ath_25,
                ls='--', c='k', linewidth=2)
  ax11.errorbar(log_tau_th, log_phi_ath_50 - log_R_ath_50,
                ls='--', c='k', linewidth=2)
  ax12.errorbar(log_tau_th, log_phi_ath_75 - log_R_ath_75,
                ls='--', c='k', linewidth=2)
  ax20.errorbar(log_tau_th, log_phi_ath_25 - log_z_ath_25,
                ls='--', c='k', linewidth=2)
  ax21.errorbar(log_tau_th, log_phi_ath_50 - log_z_ath_50,
                ls='--', c='k', linewidth=2)
  ax22.errorbar(log_tau_th, log_phi_ath_75 - log_z_ath_75,
                ls='--', c='k', linewidth=2)

  #put data on plots
  for galaxy_name, snaps in zip(galaxy_name_list, snaps_list):

    (mean_v_R, mean_v_phi,
     sigma_z, sigma_R, sigma_phi
     ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges)

    (_, MassDM_res, _, _, _, _,_,_,_,_,_) = load_nbody.load_snapshot(galaxy_name, '002')

    #work out constatns
    (t_c_dm , t_c_circ, delta_dm, upsilon_dm, upsilon_circ
      ) = get_constants(bin_edges, save_name_append, MassDM_res)

    (burn_max_v, burn_min_z, burn_min_R, burn_min_phi
     ) = smooth_burn_ins(MassDM_res, snaps, save_name_append, burn_in, bin_edges)

    log_sigma_z2 = 2 * np.log10(sigma_z).T
    log_sigma_R2 = 2 * np.log10(sigma_R).T
    log_sigma_phi2 = 2 * np.log10(sigma_phi).T

    kwargs = get_name_kwargs(galaxy_name)

    #time
    tau = np.zeros((3, snaps+1))
    gyr_per_snap = T_TOT / (snaps -1)
    for i in range(len(bin_edges)//2):
      tau[i] = (np.arange(snaps+1) - burn_in) * gyr_per_snap / t_c_dm[i]
    log_tau = np.log10(tau)

    alpha = 0.1

    ax00.errorbar(log_tau[0], log_sigma_R2[0] - log_sigma_z2[0],
                  marker=kwargs['marker'], c=kwargs['c'], ls='-', alpha=alpha, zorder=-1)
    ax01.errorbar(log_tau[1], log_sigma_R2[1] - log_sigma_z2[1],
                  marker=kwargs['marker'], c=kwargs['c'], ls='-', alpha=alpha, zorder=-1)
    ax02.errorbar(log_tau[2], log_sigma_R2[2] - log_sigma_z2[2],
                  marker=kwargs['marker'], c=kwargs['c'], ls='-', alpha=alpha, zorder=-1)

    ax10.errorbar(log_tau[0], log_sigma_phi2[0] - log_sigma_R2[0],
                  marker=kwargs['marker'], c=kwargs['c'], ls='-', alpha=alpha, zorder=-1)
    ax11.errorbar(log_tau[1], log_sigma_phi2[1] - log_sigma_R2[1],
                  marker=kwargs['marker'], c=kwargs['c'], ls='-', alpha=alpha, zorder=-1)
    ax12.errorbar(log_tau[2], log_sigma_phi2[2] - log_sigma_R2[2],
                  marker=kwargs['marker'], c=kwargs['c'], ls='-', alpha=alpha, zorder=-1)

    ax20.errorbar(log_tau[0], log_sigma_phi2[0] - log_sigma_z2[0],
                  marker=kwargs['marker'], c=kwargs['c'], ls='-', alpha=alpha, zorder=-1)
    ax21.errorbar(log_tau[1], log_sigma_phi2[1] - log_sigma_z2[1],
                  marker=kwargs['marker'], c=kwargs['c'], ls='-', alpha=alpha, zorder=-1)
    ax22.errorbar(log_tau[2], log_sigma_phi2[2] - log_sigma_z2[2],
                  marker=kwargs['marker'], c=kwargs['c'], ls='-', alpha=alpha, zorder=-1)

    (v_ath, z_ath, R_ath, phi_ath
     ) = get_real_theory_velocities(galaxy_name, bin_edges, snaps, save_name_append='diff')

    ax00.errorbar(log_tau[0], 2*np.log10(R_ath[:,0]) - 2*np.log10(z_ath[:,0]),
                  ls='--', c='k', linewidth=2)
    ax01.errorbar(log_tau[1], 2*np.log10(R_ath[:,1]) - 2*np.log10(z_ath[:,1]),
                  ls='--', c='k', linewidth=2)
    ax02.errorbar(log_tau[2], 2*np.log10(R_ath[:,2]) - 2*np.log10(z_ath[:,2]),
                  ls='--', c='k', linewidth=2)
    ax10.errorbar(log_tau[0], 2*np.log10(phi_ath[:,0]) - 2*np.log10(R_ath[:,0]),
                  ls='--', c='k', linewidth=2)
    ax11.errorbar(log_tau[1], 2*np.log10(phi_ath[:,1]) - 2*np.log10(R_ath[:,1]),
                  ls='--', c='k', linewidth=2)
    ax12.errorbar(log_tau[2], 2*np.log10(phi_ath[:,2]) - 2*np.log10(R_ath[:,2]),
                  ls='--', c='k', linewidth=2)
    ax20.errorbar(log_tau[0], 2*np.log10(phi_ath[:,0]) - 2*np.log10(z_ath[:,0]),
                  ls='--', c='k', linewidth=2)
    ax21.errorbar(log_tau[1], 2*np.log10(phi_ath[:,1]) - 2*np.log10(z_ath[:,1]),
                  ls='--', c='k', linewidth=2)
    ax22.errorbar(log_tau[2], 2*np.log10(phi_ath[:,2]) - 2*np.log10(z_ath[:,2]),
                  ls='--', c='k', linewidth=2)

  n = len(log_tau_th)
  ax00.errorbar(log_tau_th, np.zeros(n),
                ls='--', c='grey', alpha=0.5, linewidth=5)
  ax01.errorbar(log_tau_th, np.zeros(n),
                ls='--', c='grey', alpha=0.5, linewidth=5)
  ax02.errorbar(log_tau_th, np.zeros(n),
                ls='--', c='grey', alpha=0.5, linewidth=5)
  ax10.errorbar(log_tau_th, np.zeros(n),
                ls='--', c='grey', alpha=0.5, linewidth=5)
  ax11.errorbar(log_tau_th, np.zeros(n),
                ls='--', c='grey', alpha=0.5, linewidth=5)
  ax12.errorbar(log_tau_th, np.zeros(n),
                ls='--', c='grey', alpha=0.5, linewidth=5)
  ax20.errorbar(log_tau_th, np.zeros(n),
                ls='--', c='grey', alpha=0.5, linewidth=5)
  ax21.errorbar(log_tau_th, np.zeros(n),
                ls='--', c='grey', alpha=0.5, linewidth=5)
  ax22.errorbar(log_tau_th, np.zeros(n),
                ls='--', c='grey', alpha=0.5, linewidth=5)

  #epicycle theory / milky way ic lines
  ax00.errorbar(log_tau_th, np.log10(2) * np.ones(n),
                ls='-', c='k')
  ax01.errorbar(log_tau_th, np.log10(2) * np.ones(n),
                ls='-', c='k')
  ax02.errorbar(log_tau_th, np.log10(2) * np.ones(n),
                ls='-', c='k')
  ax10.errorbar(log_tau_th, np.log10(0.5) * np.ones(n),
                ls='-', c='k')
  ax11.errorbar(log_tau_th, np.log10(0.5) * np.ones(n),
                ls='-', c='k')
  ax12.errorbar(log_tau_th, np.log10(0.5) * np.ones(n),
                ls='-', c='k')

  ax00.set_xlim([-5, 0.5])
  ax00.set_ylim([-0.5, 0.5])

  ax00.set_ylabel(r'$\log (\sigma_R^2 / \sigma_z^2) $')
  ax10.set_ylabel(r'$\log (\sigma_\phi^2 / \sigma_R^2) $')
  ax20.set_ylabel(r'$\log (\sigma_\phi^2 / \sigma_z^2) $')
  ax21.set_xlabel(r'$\log \tau = t / t_c$')

  plt.show() if save:
    plt.savefig(name, bbox_inches="tight")
    plt.close()

  return

def velocity_pdf_r(galaxy_name, snap, snaps, dm=False, save=False):
  '''
  '''
  # xbin_edges = np.arange(0,31,2)
  # ybin_edges = np.arange(0,310,10)

  # xbin_edges = np.arange(0,101,1)
  xbin_edges = np.arange(0,31,2, dtype=np.float64)
  ybin_edges = np.arange(-1010,1010,10, dtype=np.float64)

  label_array = [r'$|\vec{v}|$ [km/s]', r'$v_z$ [km/s]', r'$v_R$ [km/s]', r'$v_\phi$ [km/s]']
  name_array = ['tot', 'z', 'R', 'phi']

  #theory line calculations
  #disk heating lines
  # (mean_v_phi, sigma_z, sigma_R, sigma_phi
  #  ) = get_real_theory_velocities(galaxy_name, np.repeat(xbin_edges, 2)[1:-1], snaps)

  (burn_max_v, burn_min_z, burn_min_R, burn_min_phi
   ) = smooth_zero_snap(1, snaps, 'diff', np.repeat(xbin_edges, 2)[1:-1])

  #params
  a_h, vmax = get_hernquist_params()
  # vmax = vmax / 0.99

  #analytic circular velocity
  v_circ = get_analytic_hernquist_circular_velocity(np.repeat(xbin_edges,2)[1:-1],
                                                    vmax, a_h)
  #analytic escape velocity
  v_esc = get_analytic_hernquist_escape_velocity(np.repeat(xbin_edges,2)[1:-1],
                                                 vmax, a_h)

  #analytic dispersion
  sigma = get_analytic_hernquist_dispersion(np.repeat(xbin_edges,2)[1:-1], vmax, a_h)

  for k in range(snaps + 1):
    snap = '{0:03d}'.format(int(k))

    #load data
    (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,   PotDMs,
                MassStar, PosStars, VelStars, IDStars, PotStars
                ) = load_nbody.load_and_align(galaxy_name, snap)

    #stars and DM on same scale
    scale = len(xbin_edges) / len(IDStars) * (xbin_edges[1] - xbin_edges[0])

    (_,_,_, v_R,   v_phi,   v_z)    = get_cylindrical(PosStars, VelStars)
    if dm:
      (_,_,_, v_R_dm,v_phi_dm,v_z_dm) = get_cylindrical(PosDMs, VelDMs)

    data_array    = [np.linalg.norm(VelStars, axis=1), v_z, v_R, v_phi]
    if dm:
      data_array_dm = [np.linalg.norm(VelDMs, axis=1), v_z_dm, v_R_dm, v_phi_dm]
    else:
      data_array_dm = [0, 0, 0, 0]

    x    = np.linalg.norm(PosStars, axis=1)
    if dm:
      x_dm = np.linalg.norm(PosDMs, axis=1)

    #start plotting
    fig = plt.figure()
    fig.set_size_inches(9, 18, forward=True)
    fig.subplots_adjust(hspace=0.03,wspace=0.03)

    for i, (y, y_dm, lab, name) in enumerate(zip(
        data_array, data_array_dm, label_array, name_array)):
    # for y, lab, name in zip(data_array, label_array, name_array):

      ax = plt.subplot(len(name_array),1,i+1)

      #stars
      if i == 0:
        my_better_violin_plot(x, y, xbin_edges, ybin_edges,
                              global_scale=False, fixed_scale=True, bin_scale = scale,
                              quartiles=[0.16,0.50,0.84],
                              alpha=0.8, color='C0', label='')
      else:
        my_better_violin_plot(x, y, xbin_edges, ybin_edges,
                              global_scale=False, fixed_scale=True, bin_scale = scale,
                              quartiles=[0.16,0.50,0.84],
                              alpha=0.8, color='C0', label='Stars')

      if dm:
        #dm
        my_better_violin_plot(x_dm, y_dm, xbin_edges, ybin_edges,
                              global_scale=False, fixed_scale=True, bin_scale= -scale,
                              alpha=0.6, color='C1', label='DM')

      # if i == 0:
      #   plt.plot(0.5*(xbin_edges[:-1]+xbin_edges[1:]), v_circ,
      #            color='C2', label=r'$v_c$', ls=':')
      # else:
      #   plt.plot(0.5*(xbin_edges[:-1]+xbin_edges[1:]), v_circ,
      #            color='C2', ls=':')
      plt.plot(0.5*(xbin_edges[:-1]+xbin_edges[1:]), v_circ,
               color='C2', ls=':')
      plt.plot(0.5*(xbin_edges[:-1]+xbin_edges[1:]),-v_circ,
               color='C2', ls=':')
      # #escape velocity
      # if i == 0:
      #   plt.plot(0.5*(xbin_edges[:-1]+xbin_edges[1:]),  v_esc,
      #            color='C3', label=r'$v_{esc}$', ls=':')
      # else:
      #   plt.plot(0.5*(xbin_edges[:-1]+xbin_edges[1:]),  v_esc,
      #            color='C3', ls=':')
      # plt.plot(0.5*(xbin_edges[:-1]+xbin_edges[1:]), -v_esc,
      #          color='C3', ls=':')

      #1d
      if i == 0:
        plt.plot(0.5*(xbin_edges[:-1]+xbin_edges[1:]),  sigma,
                 color='C4', label=r'$\sigma_{DM, 1D}$', ls='-.')
      else:
        plt.plot(0.5*(xbin_edges[:-1]+xbin_edges[1:]),  sigma,
                 color='C4', ls='-.')
      plt.plot(0.5*(xbin_edges[:-1]+xbin_edges[1:]), -sigma,
               color='C4', ls='-.')

      # if name == 'tot':
        #from maxwell-boltzmann distribution
        #look up wikipedia parameters for this conversion
        # sigma_speed = np.sqrt((sigma_R[int(snap)]**2 + sigma_phi[int(snap)]**2 +
        #                        sigma_z[int(snap)]**2) * (3*np.pi/8 -1))

        # mean = np.sqrt(sigma_R[int(snap)]**2 + sigma_phi[int(snap)]**2 +
        #                sigma_z[int(snap)]**2 + mean_v_phi[int(snap)]**2 -
                       # sigma_speed**2)

        #TODO I don't think this is correct

        # sigma_speed = np.sqrt((mean**2) * (3*np.pi/8 -1))

      #   plt.plot(0.5*(xbin_edges[:-1]+xbin_edges[1:]), mean + sigma_speed,
      #            color='k', ls='--')#, label='heating')
      #   plt.plot(0.5*(xbin_edges[:-1]+xbin_edges[1:]), mean,
      #            color='k', ls='-')
      #   plt.plot(0.5*(xbin_edges[:-1]+xbin_edges[1:]), mean - sigma_speed,
      #            color='k', ls='--')
      # elif name == 'z':
      #   plt.plot(0.5*(xbin_edges[:-1]+xbin_edges[1:]), sigma_z[int(snap)],
      #            color='k', ls='--', label='Heating')
      #   plt.plot(0.5*(xbin_edges[:-1]+xbin_edges[1:]), np.zeros(len(xbin_edges)-1),
      #            color='k', ls='-')
      #   plt.plot(0.5*(xbin_edges[:-1]+xbin_edges[1:]),-sigma_z[int(snap)],
      #            color='k', ls='--')
      # elif name == 'R':
      #   plt.plot(0.5*(xbin_edges[:-1]+xbin_edges[1:]), sigma_R[int(snap)],
      #            color='k', ls='--', label='Heating')
      #   plt.plot(0.5*(xbin_edges[:-1]+xbin_edges[1:]), np.zeros(len(xbin_edges)-1),
      #            color='k', ls='-')
      #   plt.plot(0.5*(xbin_edges[:-1]+xbin_edges[1:]),-sigma_R[int(snap)],
      #            color='k', ls='--')
      # elif name == 'phi':
      #   plt.plot(0.5*(xbin_edges[:-1]+xbin_edges[1:]), sigma_phi[int(snap)]+mean_v_phi[int(snap)],
      #            color='k', ls='--', label='Heating')
      #   plt.plot(0.5*(xbin_edges[:-1]+xbin_edges[1:]), mean_v_phi[int(snap)],
      #            color='k', ls='-')
      #   plt.plot(0.5*(xbin_edges[:-1]+xbin_edges[1:]),-sigma_phi[int(snap)]+mean_v_phi[int(snap)],
      #            color='k', ls='--')

      plt.ylabel(lab)

      plt.xlim([0, 30])
      if name == 'tot':
        #3d
        plt.plot(0.5*(xbin_edges[:-1]+xbin_edges[1:]), np.sqrt(3)*sigma,
                 color='C5', label=r'$\sigma_{DM, 3D}$', ls='-.')
        ax.set_ylim([0, 390])
      else:
        ax.set_ylim([-390, 390])

      if i+1 != len(name_array):
        ax.set_xticklabels([])

      if i == 0:
        plt.legend(loc='lower right', ncol=2)
      if i == len(name_array)-2:
        plt.text(22,-300, 't='+str(np.round(T_TOT * int(snap)/snaps, 2))+'Gyr')
      if i+1 == len(name_array):
        plt.legend(loc='lower right')

    plt.xlabel('r [kpc]')

    plt.show() if save:
      fname = '../galaxies/' + galaxy_name + '/results/radial_pdfs/' + snap
      plt.savefig(fname, bbox_inches='tight')
      plt.close()

  return

def make_velocity_r_gif(name):
  '''
  '''
  if (('7p5' in name) or ('8p0' in name)):
    nsnaps     =  401
  else:
    nsnaps     =  102

  # fps = 10
  fps = nsnaps/40
  fname = '../galaxies/' + name + '/results/radial_pdfs'

  # try:
  #   os.mkdir(fname+'/')
  # except:
  #   pass

  w = imageio.get_writer(fname+'.mp4', format='FFMPEG', mode='I', fps=fps)#,
                         # codec='h264_vaapi',
                         # output_params=['-vaapi_device',
                         #               '/dev/dri/renderD128', '-vf',
                         #               'format=gray|nv12,hwupload'],
                         # pixelformat='vaapi_vld')

  for i in range(nsnaps):
    w.append_data(imageio.imread('../galaxies/'+name+
                                 '/results/radial_pdfs/{0:03d}.png'.format(int(i))))
  w.close()

  # imageio.mimwrite(fname, [plot_projection_instantanious_for_gif(name, i)
  #                          for i in range(nsnaps)], fps=fps)
  return

def get_hernquist_guiding_radii(PosStars, VelStars, GM, a, cylindrical=False):
  '''Finds the guiding radii using Binney and Trimane eqn. 3.72
  '''
  L = np.cross(PosStars, VelStars) #kpc km / s
  if cylindrical:
    #Lz
    L = L[:, 2]
  else:
    #|L|
    L = np.sum(L, axis=1)

  ones = np.ones(len(L))

  # ploy_coefficents = np.array([ones * a**2, ones * 2*a, ones, -GM / L**2])
  ploy_coefficents = np.array([-GM / L**2, ones, ones * 2*a, ones * a**2])

  out = np.zeros(len(L))

  for i in range(len(L)):
    # poly = np.polynomial.Polynomial(ploy_coefficents[:, i])
    # roots = poly.roots()
    roots = np.roots(ploy_coefficents[:, i])
    if roots[0].imag == 0:
      root = roots[0].real
    elif roots[1].imag == 0:
      root = roots[1].real
    elif roots[2].imag == 0:
      root = roots[2].real
    else:
      print('all complex roots')
      raise ValueError

    out[i] = root

  return(out)

def plot_giuding_centre_profile_snap(galaxy_name, snap, save_name='', save=False):
  '''
  '''
  (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,   PotDMs,
              MassStar, PosStars, VelStars, IDStars, PotStars
              ) = load_nbody.load_and_align(galaxy_name, snap=snap)

  (a, vmax) = get_hernquist_params()
  GM = 4 * a * vmax**2 #kpc km^2/s^2

  radii = get_hernquist_guiding_radii(PosStars, VelStars, GM, a, cylindrical=False)
  Radii = get_hernquist_guiding_radii(PosStars, VelStars, GM, a, cylindrical=True)

  r = np.linalg.norm(PosStars, axis=1)
  R = np.sqrt(PosStars[:, 0]**2 + PosStars[:, 1]**2)

  # bins=None
  bins = np.linspace(0, 15 *2, 15 +1)

  plt.figure()

  plt.hist(radii, bins=bins, label=r'$r_g$ from $L_z$',  alpha=1, lw=8, ec='C0', fc='None', histtype='step')
  plt.hist(Radii, bins=bins, label=r'$R_g$ from $|L|$ ', alpha=1, lw=6, ec='C1', fc='None', histtype='step')
  plt.hist(r,     bins=bins, label=r'$r$',               alpha=1, lw=4, ec='C2', fc='None', histtype='step')
  plt.hist(R,     bins=bins, label=r'$R$',               alpha=1, lw=2, ec='C3', fc='None', histtype='step')

  plt.xlabel('distance [kpc]')
  plt.ylabel('counts')

  plt.legend()
  plt.ylim((0,200))

  # plt.figure()
  # plt.errorbar(R, Radii, fmt='.')
  # plt.errorbar([0,30], [0,30])
  # plt.xlabel(r'$R$ [kpc]')
  # plt.ylabel(r'$R_g$ [kpc]')

  # plt.figure()
  # plt.errorbar(r, radii, fmt='.')
  # plt.errorbar([0,30], [0,30])
  # plt.xlabel(r'$r$ [kpc]')
  # plt.ylabel(r'$r_g$ [kpc]')

  return

def plot_giuding_centre_profile_comparison(galaxy_name, save_name='', save=False):
  '''
  '''
  (a, vmax) = get_hernquist_params()
  GM = 4 * a * vmax**2 #kpc km^2/s^2

  (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,   PotDMs,
              MassStar, PosStars, VelStars, IDStars_0, PotStars
              ) = load_nbody.load_and_align(galaxy_name, snap='000')


  radii_0 = get_hernquist_guiding_radii(PosStars, VelStars, GM, a, cylindrical=False)
  # Radii = get_hernquist_guiding_radii(PosStars, VelStars, GM, a, cylindrical=True)

  r = np.linalg.norm(PosStars, axis=1)
  # R = np.sqrt(PosStars[:, 0]**2 + PosStars[:, 1]**2)

  # bins=None
  bins = np.linspace(0, 15 *2, 15 +1)

  plt.figure()

  plt.hist(radii_0, bins=bins, label=r'$r_g$ from $|L|$, $t=0$',  alpha=1, lw=8, ec='C0', fc='None', histtype='step')
  # plt.hist(Radii, bins=bins, label=r'$R_g$ from $L_z$ ', alpha=1, lw=8, ec='C0', fc='None', histtype='step')
  plt.hist(r,     bins=bins, label=r'$r$, $t=0$',               alpha=1, lw=6, ec='C2', fc='None', histtype='step')
  # plt.hist(R,     bins=bins, label=r'$R$',               alpha=1, lw=6, ec='C2', fc='None', histtype='step')


  (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,   PotDMs,
              MassStar, PosStars, VelStars, IDStars_1, PotStars
              ) = load_nbody.load_and_align(galaxy_name, snap='400')

  radii_1 = get_hernquist_guiding_radii(PosStars, VelStars, GM, a, cylindrical=False)
  # Radii = get_hernquist_guiding_radii(PosStars, VelStars, GM, a, cylindrical=True)

  r = np.linalg.norm(PosStars, axis=1)
  # R = np.sqrt(PosStars[:, 0]**2 + PosStars[:, 1]**2)

  plt.hist(radii_1, bins=bins, label=r'$r_g$ from $|L|$, $t=9.8$',  alpha=1, lw=4, ec='C1', fc='None', histtype='step')
  # plt.hist(Radii, bins=bins, label=r'$R_g$ from $L_z$ ', alpha=1, lw=4, ec='C1', fc='None', histtype='step')
  plt.hist(r,     bins=bins, label=r'$r$, $t=9.8$',               alpha=1, lw=2, ec='C3', fc='None', histtype='step')
  # plt.hist(R,     bins=bins, label=r'$R$',               alpha=1, lw=2, ec='C3', fc='None', histtype='step')

  plt.xlabel('distance [kpc]')
  plt.ylabel('counts')

  plt.legend()
  plt.ylim((0,200))

  arg_0 = np.argsort(IDStars_0)
  arg_1 = np.argsort(IDStars_1)

  fig = plt.figure()
  ax = plt.subplot(1,1,1)

  plt.errorbar(radii_0[arg_0], radii_1[arg_1], fmt='.')
  plt.plot([0,50], [0,50])

  plt.xlabel(r'$r_g$, $t=0$')
  plt.ylabel(r'$r_g$, $t=9.8$')

  plt.xlim((0,50))
  plt.ylim((0,50))

  ax.set_aspect('equal')

  return


def plot_time_evolution(galaxy_name_list, snaps_list, save_name_append='diff',
                   save=True, name=''):

  fig = plt.figure(constrained_layout=True)
  # fig.set_size_inches(6, 12, forward=True)
  fig.set_size_inches(24, 12, forward=True)
  widths = [1,1,1,1]
  heights = [0.1,1,1] #[1,1,1,1]
  spec = fig.add_gridspec(ncols=4, nrows=3, width_ratios=widths,
                          height_ratios=heights)

  pth_w = [path_effects.Stroke(linewidth=4, foreground='white'), path_effects.Normal()]

  # s_200 = 'Purples'
  # s_mu = 'Blues'
  # s_c = 'Greens'
  # s_z0 = 'Oranges'
  # s_r = 'Reds'
  s_200 = 'viridis'
  s_mu = 'plasma'
  s_c = 'inferno'
  s_z0 = 'magma'
  s_r = 'cividis'

  eps=1e-3

  axs = []
  for row in range(len(heights)):
    axs.append([])
    for col in range(len(widths)):
      axs[row].append( fig.add_subplot(spec[row, col]) )
      # axs[row][col].set_xlim([-4, 1])
      axs[row][col].set_xlim([-0.02, 1.99])
      axs[row][col].set_ylim([-2.0+eps, 0.5-eps])
      if row == 0:
        axs[row][col].set_xlim([0, 256])
        axs[row][col].set_ylim([0, 1])
      if col != 0: axs[row][col].set_yticklabels([])
      if row != len(heights)-1: axs[row][col].set_xticklabels([])

  fig.subplots_adjust(hspace=0.03,wspace=0.10)

  # np.seterr(all='ignore')

  bin_edges = get_bin_edges(save_name_append)

  analytic_dispersion = get_velocity_scale(bin_edges, 0,0,0,0,0,0, analytic_dispersion=True,
                                           save_name_append=save_name_append)

  analytic_v_circ = get_velocity_scale(bin_edges, 0,0,0,0,0,0, analytic_v_c=True,
                                       save_name_append=save_name_append)

  (t_c_dm,  _, delta_dm, upsilon_dm, _
   ) = get_constants(bin_edges, save_name_append, 1)

  theory_times = np.logspace(-5, 1)
  log_theory_times = np.log10(theory_times)
  fifty = len(theory_times)
  o = np.ones(fifty)
  tau_t = get_tau_array(theory_times, t_c_dm[:, np.newaxis])
  # tau_t = [get_tau_array(theory_times, t_c_dm[i]) for i in range(len(bin_edges)//2)]

  run = []
  # print('\nbeta = 0')
  # run.append(find_least_log_squares_best_fit(save_name_append, mcmc=False, fixed_b=0))
  print('\nbeta = 0, gamma=0')
  run.append(find_least_log_squares_best_fit(save_name_append, mcmc=False, fixed_b=0, fixed_g=0))
  print('\nalpha=0, beta = 0, gamma=0')
  run.append(find_least_log_squares_best_fit(save_name_append, mcmc=False, fixed_a=0, fixed_b=0, fixed_g=0))

  run_ls = ['-', ':']
  run_funcs = [get_theory_velocity2_array, get_powerlaw_velocity2_array]
  run_labels = [r'This Paper', r'LO85']

  theory_best_v_list = []
  theory_best_z2_list = []
  theory_best_r2_list = []
  theory_best_p2_list = []

  # (inital_velocity2_array_v, inital_velocity2_array_z, inital_velocity2_array_r, inital_velocity2_array_p
  #  ) = smooth_burn_ins(1e-2, 400, save_name_append, burn_in, bin_edges)

  for j, (ln_Lambda_ks, alphas, betas, gammas) in enumerate(run):

    #yaxis
    theory_best_v_list.append([run_funcs[j](analytic_v_circ[i] *o, 0*o,
                                            theory_times, t_c_dm[i]*o, delta_dm[i] *o, upsilon_dm[i] *o,
                                            ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
                               for i in range(len(bin_edges)//2)])
    theory_best_z2_list.append([run_funcs[j](analytic_dispersion[i]**2 *o, 0*o,
                                             theory_times, t_c_dm[i]*o, delta_dm[i] *o, upsilon_dm[i] *o,
                                             ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
                                for i in range(len(bin_edges)//2)])
    theory_best_r2_list.append([run_funcs[j](analytic_dispersion[i]**2 *o, 0*o,
                                             theory_times, t_c_dm[i]*o, delta_dm[i] *o, upsilon_dm[i] *o,
                                             ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
                                for i in range(len(bin_edges)//2)])
    theory_best_p2_list.append([run_funcs[j](analytic_dispersion[i]**2 *o, 0*o,
                                             theory_times, t_c_dm[i]*o, delta_dm[i] *o, upsilon_dm[i] *o,
                                             ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])
                                for i in range(len(bin_edges)//2)])

    #xaxis
    tau_heat_v = [get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                     ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
                  for i in range(len(bin_edges)//2)]
    tau_heat_z = [get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                     ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
                  for i in range(len(bin_edges)//2)]
    tau_heat_r = [get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                     ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
                  for i in range(len(bin_edges)//2)]
    tau_heat_p = [get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                     ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])
                  for i in range(len(bin_edges)//2)]

    #plot
    ls=run_ls[j]
    c='k'
    #should all be on the same line now
    # for i in range(len(bin_edges)//2):
    for i in [1]:

      axs[1][0].errorbar(tau_t[i] / tau_heat_v[i], np.log10(theory_best_v_list[j][i]) -
                         np.log10(analytic_v_circ[i]), ls=ls, lw=3, c=c, label=run_labels[j], path_effects=pth_w, zorder=5)
      # asx[2][0].errorbar(np.log10(tau_t[i] / tau_heat_z[i]), np.log10(theory_best_z2_list[j][i]) -
      #                    2*np.log10(analytic_dispersion[i]), ls=ls, c=c)
      # axs[2][0].errorbar(np.log10(tau_t[i] / tau_heat_r[i]), np.log10(theory_best_r2_list[j][i]) -
      #                    2*np.log10(analytic_dispersion[i]), ls=ls, c=c)
      axs[2][0].errorbar(tau_t[i] / tau_heat_p[i], np.log10(theory_best_p2_list[j][i]) -
                         2*np.log10(analytic_dispersion[i]), ls=ls, lw=3, c=c, path_effects=pth_w, zorder=5)

      axs[1][1].errorbar(tau_t[i] / tau_heat_v[i], np.log10(theory_best_v_list[j][i]) -
                         np.log10(analytic_v_circ[i]), ls=ls, lw=3, c=c, label=run_labels[j], path_effects=pth_w, zorder=5)
      axs[2][1].errorbar(tau_t[i] / tau_heat_p[i], np.log10(theory_best_p2_list[j][i]) -
                         2*np.log10(analytic_dispersion[i]), ls=ls, lw=3, c=c, path_effects=pth_w, zorder=5)
      axs[1][2].errorbar(tau_t[i] / tau_heat_v[i], np.log10(theory_best_v_list[j][i]) -
                         np.log10(analytic_v_circ[i]), ls=ls, lw=3, c=c, label=run_labels[j], path_effects=pth_w, zorder=5)
      axs[2][2].errorbar(tau_t[i] / tau_heat_p[i], np.log10(theory_best_p2_list[j][i]) -
                         2*np.log10(analytic_dispersion[i]), ls=ls, lw=3, c=c, path_effects=pth_w, zorder=5)
      axs[1][3].errorbar(tau_t[i] / tau_heat_v[i], np.log10(theory_best_v_list[j][i]) -
                         np.log10(analytic_v_circ[i]), ls=ls, lw=3, c=c, label=run_labels[j], path_effects=pth_w, zorder=5)
      axs[2][3].errorbar(tau_t[i] / tau_heat_p[i], np.log10(theory_best_p2_list[j][i]) -
                         2*np.log10(analytic_dispersion[i]), ls=ls, lw=3, c=c, path_effects=pth_w, zorder=5)
      # axs[1][4].errorbar(np.log10(tau_t[i] / tau_heat_v[i]), np.log10(theory_best_v_list[j][i]) -
      #                    np.log10(analytic_v_circ[i]), ls=ls, lw=3, c=c, label=run_labels[j], path_effects=pth_w, zorder=5)
      # axs[2][4].errorbar(np.log10(tau_t[i] / tau_heat_p[i]), np.log10(theory_best_p2_list[j][i]) -
      #                    2*np.log10(analytic_dispersion[i]), ls=ls, lw=3, c=c, path_effects=pth_w, zorder=5)

  #maximum cut off
  ls='--'
  c='grey'
  alpha=0.5
  linewidth=5
  for i in range(len(bin_edges)//2):
    axs[1][0].errorbar(theory_times, o-1 + np.log10(analytic_v_circ[i]) -
                       np.log10(analytic_v_circ[i]),
                       ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
    # asx[2][0].errorbar(log_theory_times, o-1 + 2*np.log10(analytic_dispersion[i]) -
    #                    2*np.log10(analytic_dispersion[i]),
    #                    ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
    # axs[2][0].errorbar(log_theory_times, o-1 + 2*np.log10(analytic_dispersion[i]) -
    #                    2*np.log10(analytic_dispersion[i]),
    #                    ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
    axs[2][0].errorbar(theory_times, o-1 + 2*np.log10(analytic_dispersion[i]) -
                       2*np.log10(analytic_dispersion[i]),
                       ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)

    axs[1][1].errorbar(theory_times, o-1 + np.log10(analytic_v_circ[i]) -
                       np.log10(analytic_v_circ[i]),
                       ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
    axs[2][1].errorbar(theory_times, o-1 + 2*np.log10(analytic_dispersion[i]) -
                       2*np.log10(analytic_dispersion[i]),
                       ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
    axs[1][2].errorbar(theory_times, o-1 + np.log10(analytic_v_circ[i]) -
                       np.log10(analytic_v_circ[i]),
                       ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
    axs[2][2].errorbar(theory_times, o-1 + 2*np.log10(analytic_dispersion[i]) -
                       2*np.log10(analytic_dispersion[i]),
                       ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
    axs[1][3].errorbar(theory_times, o-1 + np.log10(analytic_v_circ[i]) -
                       np.log10(analytic_v_circ[i]),
                       ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
    axs[2][3].errorbar(theory_times, o-1 + 2*np.log10(analytic_dispersion[i]) -
                       2*np.log10(analytic_dispersion[i]),
                       ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
    # axs[1][4].errorbar(log_theory_times, o-1 + np.log10(analytic_v_circ[i]) -
    #                    np.log10(analytic_v_circ[i]),
    #                    ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
    # axs[2][4].errorbar(log_theory_times, o-1 + 2*np.log10(analytic_dispersion[i]) -
    #                    2*np.log10(analytic_dispersion[i]),
    #                    ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)

  #fits for comparison
  # print('\nbeta = 0 (fiducial)')
  (ln_Lambda_ks, alphas, betas, gammas
   ) = find_least_log_squares_best_fit(
    save_name_append, mcmc=False, fixed_g=0, fixed_b=0)
    # save_name_append, mcmc=False, fixed_g=0)

  color_r25 = matplotlib.cm.get_cmap(s_r)(0)
  color_r50 = matplotlib.cm.get_cmap(s_r)(0.5)
  color_r75 = matplotlib.cm.get_cmap(s_r)(1-eps)
  color_r = [color_r25, color_r50, color_r75]

  #put data on plots
  for galaxy_name, snaps in zip(galaxy_name_list, snaps_list):
    kwargs = get_name_kwargs(galaxy_name)

    # M200 = kwargs['V200']**3 / (10 * GRAV_CONST * GALIC_HUBBLE_CONST)
    # N200 = M200 / kwargs['mdm']
    #
    # max_log_N = 6.463340570453965
    # min_log_N = 4.172610531429795
    # f = (np.log10(N200)-min_log_N)/(max_log_N - min_log_N)
    #
    # color = matplotlib.cm.get_cmap('viridis')(f)

    zo_200 = 0
    if kwargs['V200'] == 50:
      color_200 = matplotlib.cm.get_cmap(s_200)(0)
      zo_200 = 1
    if kwargs['V200'] == 100:
      color_200 = matplotlib.cm.get_cmap(s_200)(0.33)
      zo_200 = 1
    if kwargs['V200'] == 200:
      color_200 = matplotlib.cm.get_cmap(s_200)(0.67)
    if kwargs['V200'] == 400:
      color_200 = matplotlib.cm.get_cmap(s_200)(1-eps)
      zo_200 = 1

    zo_mu = 0
    if kwargs['mun'] == 1:
      color_mu = matplotlib.cm.get_cmap(s_mu)(0)
      zo_mu=1
    if kwargs['mun'] == 5:
      color_mu = matplotlib.cm.get_cmap(s_mu)(0.5)
    if kwargs['mun'] == 25:
      color_mu = matplotlib.cm.get_cmap(s_mu)(1-eps)
      zo_mu=1

    zo_c = 0
    # if kwargs['conc'] == 7:  color_c = matplotlib.cm.get_cmap(s_c)(0)
    # if kwargs['conc'] == 10: color_c = matplotlib.cm.get_cmap(s_c)(0.5)
    # if kwargs['conc'] == 15: color_c = matplotlib.cm.get_cmap(s_c)(1-eps)
    if kwargs['conc'] == 4:
      color_c = matplotlib.cm.get_cmap(s_c)(0)
      zo_c=1
    if kwargs['conc'] == 7:
      color_c = matplotlib.cm.get_cmap(s_c)(0.25)
      zo_c=1
    if kwargs['conc'] == 10:
      color_c = matplotlib.cm.get_cmap(s_c)(0.5)
    if kwargs['conc'] == 15:
      color_c = matplotlib.cm.get_cmap(s_c)(0.75)
      zo_c=1
    if kwargs['conc'] == 25:
      color_c = matplotlib.cm.get_cmap(s_c)(1-eps)
      zo_c=1

    zo_z0 = 0
    if kwargs['scale_height'] == 1:
      color_z0 = matplotlib.cm.get_cmap(s_z0)(0)
    if kwargs['scale_height'] == 2:
      color_z0 = matplotlib.cm.get_cmap(s_z0)(0.5)
      zo_z0 = 1
    if kwargs['scale_height'] == 4:
      color_z0 = matplotlib.cm.get_cmap(s_z0)(1-eps)
      zo_z0 = 1

    (mean_v_R, mean_v_phi,
     sigma_z, sigma_R, sigma_phi
     ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges)

    bin_edges = get_bin_edges(save_name_append, galaxy_name=galaxy_name)

    #work out constatns
    (t_c_dm , t_c_circ, delta_dm, upsilon_dm, upsilon_circ
     ) = get_constants(bin_edges, save_name_append, kwargs['mdm'], galaxy_name=galaxy_name)

    analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)
    analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                         save_name_append=save_name_append, galaxy_name=galaxy_name)

    mean_v_phi = mean_v_phi.T
    sigma_z2 = sigma_z.T**2
    sigma_r2 = sigma_R.T**2
    sigma_p2 = sigma_phi.T**2

    (inital_velocity_v, inital_velocity2_z, inital_velocity2_r, inital_velocity2_p
     ) = smooth_burn_ins(kwargs['mdm'], snaps, save_name_append, BURN_IN, bin_edges, galaxy_name=galaxy_name)

    inital_velocity2_z **=2
    inital_velocity2_r **=2
    inital_velocity2_p **=2

    for i in range(len(bin_edges)//2):
      time = np.linspace(0, T_TOT, snaps+1)
      time = time - time[BURN_IN]

      tau = get_tau_array(time, t_c_dm[i])

      tau_heat_v = get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                      ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
      # tau_heat_z = get_tau_heat_array(delta_dm[i], upsilon_dm[i],
      #                            ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
      # tau_heat_r = get_tau_heat_array(delta_dm[i], upsilon_dm[i],
      #                            ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
      tau_heat_p = get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                      ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

      alpha = 0.1
      lw = 2
      window = snaps // 3 + 1
      if window % 2 == 0: window += 1
      # poly_n = 1
      poly_n = 3
      # vs time
      # box = np.ones(window) / window
      # box[0] = 0.5
      # box[-1] = 0.5

      x = tau / tau_heat_v
      # y = np.log10(inital_velocity_v[i] - mean_v_phi[i]) - np.log10(analytic_v_circ[i])
      y = (inital_velocity_v[i] - mean_v_phi[i]) / analytic_v_circ[i]
      mask = ~np.isnan(y)
      mask[:BURN_IN-1] = False
      x = x[mask]
      y = y[mask]
      sy = np.log10(savgol_filter(y, window, poly_n, mode='interp'))
      y = np.log10(y)

      # axs[1][0].errorbar(x, y,  c=color_200, marker='.', ls='', alpha=alpha)
      # axs[1][1].errorbar(x, y,  c=color_mu, marker='.', ls='', alpha=alpha)
      # axs[1][2].errorbar(x, y,  c=color_c, marker='.',ls='', alpha=alpha)
      # axs[1][3].errorbar(x, y,  c=color_z0, marker='.', ls='', alpha=alpha)
      # axs[1][4].errorbar(x, y,  c=color_r[i], marker='.', ls='', alpha=alpha)

      axs[1][0].errorbar(x, y,  c=color_200, marker='', ls='-', lw=lw, alpha=alpha, zorder=zo_200)
      axs[1][0].errorbar(x, sy, c=color_200, marker='', ls='-', zorder=zo_200)#ls=kwargs['ls'])
      axs[1][1].errorbar(x, y,  c=color_mu, marker='', ls='-', lw=lw, alpha=alpha, zorder=zo_mu)
      axs[1][1].errorbar(x, sy, c=color_mu, marker='', ls='-', zorder=zo_mu)#ls=kwargs['ls'])
      axs[1][2].errorbar(x, y,  c=color_c, marker='', ls='-', lw=lw, alpha=alpha, zorder=zo_c)
      axs[1][2].errorbar(x, sy, c=color_c, marker='', ls='-', zorder=zo_c)#ls=kwargs['ls'])
      axs[1][3].errorbar(x, y,  c=color_z0, marker='', ls='-', lw=lw, alpha=alpha, zorder=zo_z0)
      axs[1][3].errorbar(x, sy, c=color_z0, marker='', ls='-', zorder=zo_z0)#ls=kwargs['ls'])
      # axs[1][4].errorbar(x, y,  c=color_r[i], marker='', ls='-', lw=lw, alpha=alpha)
      # axs[1][4].errorbar(x, sy, c=color_r[i], marker='', ls='-')#ls=kwargs['ls'])

      x = tau / tau_heat_p
      # y = np.log10(sigma_p2[i] - inital_velocity2_p[i]) - 2*np.log10(analytic_dispersion[i])
      y = (sigma_p2[i] - inital_velocity2_p[i]) / analytic_dispersion[i]**2
      mask = ~np.isnan(y)
      x = x[mask]
      y = y[mask]
      sy = np.log10(savgol_filter(y, window, poly_n, mode='interp'))
      y = np.log10(y)

      axs[2][0].errorbar(x, y,  c=color_200, marker='', ls='-', lw=lw, alpha=alpha, zorder=zo_200)
      axs[2][0].errorbar(x, sy, c=color_200, marker='', ls='-', zorder=zo_200)#ls=kwargs['ls'])
      axs[2][1].errorbar(x, y,  c=color_mu, marker='', ls='-', lw=lw, alpha=alpha, zorder=zo_mu)
      axs[2][1].errorbar(x, sy, c=color_mu, marker='', ls='-', zorder=zo_mu)#ls=kwargs['ls'])
      axs[2][2].errorbar(x, y,  c=color_c, marker='', ls='-', lw=lw, alpha=alpha, zorder=zo_c)
      axs[2][2].errorbar(x, sy, c=color_c, marker='', ls='-', zorder=zo_c)#ls=kwargs['ls'])
      axs[2][3].errorbar(x, y,  c=color_z0, marker='', ls='-', lw=lw, alpha=alpha, zorder=zo_z0)
      axs[2][3].errorbar(x, sy, c=color_z0, marker='', ls='-', zorder=zo_z0)#ls=kwargs['ls'])
      # axs[2][4].errorbar(x, y,  c=color_r[i], marker='', ls='-', lw=lw, alpha=alpha)
      # axs[2][4].errorbar(x, sy, c=color_r[i], marker='', ls='-')#ls=kwargs['ls'])

  gradient = np.linspace(0, 1, 256)
  gradient = np.vstack((gradient, gradient))

  axs[0][0].imshow(gradient, aspect='auto', cmap=s_200)
  axs[0][1].imshow(gradient, aspect='auto', cmap=s_mu)
  axs[0][2].imshow(gradient, aspect='auto', cmap=s_c)
  axs[0][3].imshow(gradient, aspect='auto', cmap=s_z0)
  # axs[0][4].imshow(gradient, aspect='auto', cmap=s_r)

  axs[0][0].set_xticks([0, 256/3, 256*2/3, 256])
  axs[0][1].set_xticks([0, 256/2, 256])
  # axs[0][2].set_xticks([0, 256/2, 256])
  axs[0][2].set_xticks([0, 256/4, 256/2, 256*3/4, 256])
  axs[0][3].set_xticks([0, 256/2, 256])
  # axs[0][4].set_xticks([0, 256/2, 256])

  axs[0][0].set_xticklabels([50, 100, 200, 400])
  axs[0][1].set_xticklabels([1, 5, 25])
  # axs[0][2].set_xticklabels([7, 10, 15])
  axs[0][2].set_xticklabels([4, 7, 10, 15, 25])
  axs[0][3].set_xticklabels([6, 12, 24])
  # axs[0][4].set_xticklabels([0.25, 0.5, 0.75])

  axs[0][0].set_yticklabels([])
  axs[0][1].set_yticklabels([])
  axs[0][2].set_yticklabels([])
  axs[0][3].set_yticklabels([])
  # axs[0][4].set_yticklabels([])

  pad = -80
  axs[0][0].set_xlabel(r'$V_{200}$ [km/s]', labelpad=pad)
  axs[0][1].set_xlabel(r'$\mu$', labelpad=pad)
  axs[0][2].set_xlabel(r'$c$', labelpad=pad)
  axs[0][3].set_xlabel(r'$z_0 / a$ [$10^{-3}$]', labelpad=pad)
  # axs[0][4].set_xlabel(r'$R_X$', labelpad=pad)

  axs[0][0].tick_params(axis='x', bottom=True, top=True, labelbottom=False, labeltop=True)
  axs[0][1].tick_params(axis='x', bottom=True, top=True, labelbottom=False, labeltop=True)
  axs[0][2].tick_params(axis='x', bottom=True, top=True, labelbottom=False, labeltop=True)
  axs[0][3].tick_params(axis='x', bottom=True, top=True, labelbottom=False, labeltop=True)
  # axs[0][4].tick_params(axis='x', bottom=True, top=True, labelbottom=False, labeltop=True)

  axs[1][0].legend(loc='lower right', frameon=False,
                   handletextpad=0.2, borderpad=0.2)#, bbox_to_anchor=(1.08, -0.15))
  # asx[2][0].legend(loc='lower right', labelspacing=-0.2, markerscale=2, handlelength=1,
  #                  handletextpad=0.0, borderpad=0.2)
  # axs[2][0].legend(loc='lower right', labelspacing=-0.3, markerscale=2, handlelength=1,
  #                  handletextpad=0.0, borderpad=0.2, bbox_to_anchor=(1.025, -0.05))#, ncol=2, columnspacing=0.0)

  # asx[2][0].legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='-', linewidth=2)),
  #             (lines.Line2D([0, 1], [0, 1], color='grey', ls='--', linewidth=2)),
  #             (lines.Line2D([0, 1], [0, 1], color='grey', ls=':', linewidth=2))],
  #            [r'$\mu = 25$', r'$\mu = 5$', r'$\mu = 1$'],
  #            handler_map={tuple: HandlerTupleVertical(ndivide=1)},
  #            loc='lower right', numpoints=1, handlelength=1, handletextpad=0.2, frameon=False,
  #            labelspacing=-0.2, borderpad=0.2)#, bbox_to_anchor=(1.04, -0.12))

  axs[1][0].set_ylabel(r'$\log (\Delta \overline{v_\phi} / v_c) $')
  # asx[2][0].set_ylabel(r'$\log (\Delta \sigma_z^2 / \sigma_{DM}^2) $')
  # axs[2][0].set_ylabel(r'$\log (\Delta \sigma_R^2 / \sigma_{DM}^2) $')
  axs[2][0].set_ylabel(r'$\log (\Delta \sigma_\phi^2 / \sigma_{DM}^2) $')
  axs[2][0].set_xlabel(r'$\tau / \tau_{\mathrm{heat}} $')
  axs[2][1].set_xlabel(r'$\tau / \tau_{\mathrm{heat}} $')
  axs[2][2].set_xlabel(r'$\tau / \tau_{\mathrm{heat}} $')
  axs[2][3].set_xlabel(r'$\tau / \tau_{\mathrm{heat}} $')
  # axs[2][4].set_xlabel(r'$\log \tau / \tau_{\mathrm{heat}} $')

  if save:
    plt.savefig(name, bbox_inches="tight")

  # np.seterr(all='warn')

  return

def plot_time_scales(save_name='', save=False):

  MassDMs = [10**(-2), 10**(-2.5), 10**(-3), 10**(-3.5), 10**(-4)]
  N200_labels = [r'$N_{200} = 2 \cdot 10^4$', r'$N_{200} = 6 \cdot 10^4$',
                 r'$N_{200} = 2 \cdot 10^5$', r'$N_{200} = 6 \cdot 10^5$',
                 r'$N_{200} = 2 \cdot 10^6$']
  color_frac = [0.99,0.8,0.6,0.4,0.2]
  # MassDMs = [10**(-2)]
  # color_frac = [0.6]

  #bins
  bin_edges = np.logspace(-0.5, 3, 36)

  #bins at a better resolution
  # if save_name_append == 'diff':
  bin_centres = 10**((np.log10(bin_edges[:-1]) + np.log10(bin_edges[1:]))/2)
  bin_edges   = np.roll(np.repeat(bin_edges, 2), -1)[:-2]

  #halo properties
  v200 = get_v_200()
  r200 = get_r_200()
  m200 = v200**3 / (10 * GRAV_CONST * GALIC_HUBBLE_CONST)
  rho200 = get_rho_200()
  n200s = [m200 / MassDM for MassDM in MassDMs]
  print(n200s)

  x_data = bin_centres/r200

  #
  orbit_distance = 2*np.pi * bin_centres

  bin_volume = 4/3 * np.pi * bin_centres**3

  #
  diff_dispersion = get_velocity_scale(bin_edges, 0,0,0,0,0,0,
                                       v200=False, dm_dispersion=False, dm_v_circ=False,
                                       analytic_dispersion=True, analytic_v_c=False,
                                       save_name_append='diff')
  cum_dispersion = get_velocity_scale(bin_edges, 0,0,0,0,0,0,
                                      v200=False, dm_dispersion=False, dm_v_circ=False,
                                      analytic_dispersion=True, analytic_v_c=False,
                                      save_name_append='cum')

  diff_v_circ = get_velocity_scale(bin_edges, 0,0,0,0,0,0,
                                   v200=False, dm_dispersion=False, dm_v_circ=False,
                                   analytic_dispersion=False, analytic_v_c=True,
                                   save_name_append='diff')

  diff_density = get_density_scale(bin_edges, 0,0,0,0,
                                   live_dm=False, analytic_dm=True,
                                   save_name_append='diff')
  cum_density = get_density_scale(bin_edges, 0,0,0,0,
                                  live_dm=False, analytic_dm=True,
                                  save_name_append='cum')

  #
  bin_ns = [cum_density * bin_volume / MassDM for MassDM in MassDMs]

  #time scales
  # #v_phi
  ln_k = 392.1475
  alpha = -0.5211
  # sigma_z
  # ln_k  =  64.41418
  # alpha =  -0.30611
  # gamma = 0

  delta = diff_density / rho200

  heating_time_scale = [diff_dispersion**3 / (GRAV_CONST**2 * diff_density * MassDM * ln_k * delta**alpha)
                        / PC_ON_M * GYR_ON_S for MassDM in MassDMs]

  orbital_time_scale = orbit_distance / diff_v_circ / PC_ON_M * GYR_ON_S

  # relaxation_time_scale = 0.1 * n200 / np.log(n200) * bin_centres / cum_dispersion / PC_ON_M * GYR_ON_S

  relaxation_time_scale = [bin_n / np.log(bin_n) * bin_centres / diff_v_circ / PC_ON_M * GYR_ON_S for bin_n in bin_ns]

  relax_halo_time = [0.0333 * n200 / np.log(n200) * r200 / v200 / PC_ON_M * GYR_ON_S for n200 in n200s]

  crossing_time_200 = r200 / v200 / PC_ON_M * GYR_ON_S

  # print(x_data[-7])
  # print(relaxation_time_scale[-7])
  # print(relax_halo_time)
  # print()
  # print(bin_n[-7])
  # print(bin_centres[-7])
  # print(diff_v_circ[-7])
  # print(n200)
  # print(r200)
  # print(v200)

  interesting_bin_edges = get_bin_edges()
  interesting_bin_centres = 10**((np.log10(interesting_bin_edges[:-1:2]) + np.log10(interesting_bin_edges[1:2]))/2)

  ymin = 1e-2
  ymax = 1e4

  bluecmap   = matplotlib.cm.get_cmap('Blues')
  orangecmap = matplotlib.cm.get_cmap('Oranges')
  greencmap  = matplotlib.cm.get_cmap('Greens')

  fig = plt.figure()
  fig.set_size_inches(10, 7, forward=True)

  plt.errorbar(x_data, 13.8, ls='--', c='grey')#, label='Hubble Time')
  plt.text(0.4, 13.8*1.1, 'Hubble Time', c='grey')

  plt.errorbar(x_data, orbital_time_scale, ls=':', c='k', label='Orbit Time')

  for i, frac in enumerate(color_frac):

    if i == 2:
      plt.errorbar(x_data, heating_time_scale[i], c=bluecmap(frac), label=('Heating Time\n$= t_{heat, v_\phi}$'))
      # plt.errorbar(x_data[bin_ns[i] > 3], relaxation_time_scale[i][bin_ns[i] > 3], c=orangecmap(frac),
      #              label=r'$N(<r)/log(N(<r))$')
      plt.errorbar(x_data, relax_halo_time[i], c=orangecmap(frac),
                   label=('Halo Relaxation Time\n$\propto N_{200} / \log (N_{200}) t_{cross}$'))
    else:
      plt.errorbar(x_data, heating_time_scale[i], c=bluecmap(frac))
      # plt.errorbar(x_data[bin_ns[i] > 3], relaxation_time_scale[i][bin_ns[i] > 3], c=orangecmap(frac))
      plt.errorbar(x_data, relax_halo_time[i], c=orangecmap(frac))

    plt.text(0.5, relax_halo_time[i]*1.1, N200_labels[i], c=bluecmap(frac))

  [plt.vlines(r/r200, ymin, ymax, ls='-.', color='k') for r in interesting_bin_centres]

  plt.text(interesting_bin_centres[0]/r200*0.78, 0.06, r'$r_{1/4}$')
  plt.text(interesting_bin_centres[1]/r200*0.78, 0.03, r'$r_{1/2}$')
  plt.text(interesting_bin_centres[2]/r200*0.78, 0.015, r'$r_{3/4}$')

  plt.vlines(1, ymin, ymax + 1e5, ls='-.', color='g')

  plt.xlabel(r'$r / r_{200}$')
  plt.ylabel(r'$t$ [Gyr]')
  plt.legend()

  plt.loglog()
  plt.xlim([np.amin(bin_centres)/r200, np.amax(bin_centres)/r200])
  plt.ylim([ymin, ymax])

  plt.show()
  if save:
    plt.savefig(save_name, bbox_inches='tight')
    plt.close()

  return

if __name__ == '__main__':
  #TODO keep this clean

  save=False
  # time = 10

  name = 'mu_25/fdisk_0p01_lgMdm_7p0_V200-200kmps'
  snap = '000'
  (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,
              MassStar, PosStars, VelStars, IDStars,
    j_z_stars, j_p_stars, j_c_stars, energy_stars,
    E_interp, j_circ_interp, R_interp
    ) = load_nbody.load_and_calculate(name, snap)

  MassStars = MassStar * np.ones(max(np.shape(PosStars)))

  star_formation_a = np.random.rand(len(IDStars))

  # plot_star_formation_history(PosStars, MassStars, star_formation_a,
  #                             top_leaf_id='1', snap='1', sim='1', apature=False)

  # plot_dm_rotation(name, save=False)

  # plot_tests(PosDMs, VelDMs)
  # plot_tests2(PosStars, VelStars, j_z_stars, j_c_stars, energy_stars,
  #             E_interp, j_circ_interp, PosDMs, VelDMs)
  # plot_tests3(PosStars, VelStars, j_z_stars, j_c_stars,
  #             PosDMs, VelDMs)

  # plot_circular_velocity_histograms_vs_time(name, save=save)

  # plot_stellar_exponential(PosStars, MassStar)

  # (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,   PotDMs,
  #                   MassStar, PosStars, VelStars, IDStars, PotStars
  #  ) = load_nbody.load_snapshot(name, snap)

  # halo.plot_midplane_potential(PosStars, VelStars, MassStar, PotStars,
  # PosDMs, VelDMs, MassDM, PotDMs, 200)

  # plot_jzonc(j_z_stars, j_c_stars, MassStar,
  #             name=name, snap=snap, save=save, ax=None)

  # plot_e_jz(energy_stars, j_z_stars, E_interp, j_circ_interp, MassStar,
  #           hexbins=False, square=False, kde=False,
  #           name=name, snap=snap, save=save, ax=None)

  # plot_jz_r(PosStars, j_z_stars, R_interp, j_circ_interp,
  #           name=name, snap=snap, save=save, ax=None)

  # plot_e_r(PosStars, energy_stars, R_interp, E_interp,
  #          name=name, snap=snap, save=save, ax=None)

  # plot_jzonc_r(PosStars, j_z_stars, j_c_stars,
  #              name=name, snap=snap, save=save, ax=None)

  # plot_jzonc_r_inner(PosStars, j_z_stars, j_c_stars,
  #                    name=name, snap=snap, save=save, ax=None)

  # plot_inclination(PosStars, VelStars, MassStar,
  #                  name=name, snap=snap, save=save,ax=None)

  # plot_density_profile(PosStars, MassStar,
  #                       name=name, snap=snap, save=save, ax=None)

  # plot_dispersion_j_zonc(name=name, save=save)

  # plot_dispersion_kappa(name=name, save=save)

###############################################################################

  # axs = plot_single_snapshot_all_galaxies_generic(plot_jzonc, 'jzonc',
  #                                                 time=0, save=False)
  # plot_single_snapshot_all_galaxies_generic(plot_jzonc, 'jzonc',
  #                                           time=10, save=False, axs=axs)
  # # print(axs[4,1].lines)
  # axs[4,1].legend(axs[4,1].lines, [r'$t=0$',r'$t=9.8$[Gyr]'],
  #                 loc='center left', bbox_to_anchor=(-1,0.5))
  # plt.show() if save:
  #   fname = '../galaxies/' + 'jzonc' + '_' + str(10)
  #   plt.savefig(fname, bbox_inches='tight')#, dpi=200)
  #   plt.close()

  # plot_single_snapshot_all_galaxies_generic(plot_e_jz, 'e_jz',
  #                                           time=time, save=save)

  # plot_single_snapshot_all_galaxies_generic(plot_jz_r, 'jz_r',
  #                                           time=time, save=save)

  # plot_single_snapshot_all_galaxies_generic(plot_e_r, 'e_r',
  #                                           time=time, save=save)

  # plot_single_snapshot_all_galaxies_generic(plot_jzonc_r, 'jzonc_r',
  #                                           time=time, save=save)

  # plot_single_snapshot_all_galaxies_generic(plot_jzonc_r_inner, 'jzonc_r_inner',
  #                                           time=time, save=save)

  # plot_single_snapshot_all_galaxies_generic(plot_inclination, 'inclination',
  #                                           time=time, save=save)

  # plot_single_snapshot_all_galaxies_generic(plot_density_profile,
  #                                           'density_profile',
  #                                           time=time, save=save)

###############################################################################

  #upgrade to generate plots for all galaxies
  # plot_histograms_vs_time(name, save=save)

  # #different bins
  # plot_points_vs_time(name, save=save)

  # plot_galaxy_totals_vs_time(name, save)

  # make_projection_instantanious_gif(name)

  # plot_individual_orbits(name, save)

  # plot_morpholocial_indicators_time(name, save)

  # plot_disk_heating(smooth=True, save=True)

  # plot_disk_evolution(save=save)

  # plot_disk_scale_height_mean(name, save)

  # plot_all_disk_scale_height(save=True)

  # plot_dispersion_inclination(PosStars, VelStars, j_z_stars, j_c_stars,
  #                             j_circ_interp, R_interp,
  #                             name=name, snap=snap, save=save, ax=None)

  plot_stellar_exponential(PosStars, MassStar)
  # plot_stellar_exponential_evolution(name, save)

  # plot_projection_instantanious_for_gif(name, 0, overwrite=True)

  # make_projection_instantanious_gif(name)

  # slowly_varying_functions(name='../galaxies/slowly_varting_function', save=save)