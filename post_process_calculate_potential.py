#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import os

import numpy as np
import h5py as h5

import time

import twobody

GRAV_CONST = 43007.1 # kpc (km/s)^2 / (10^10Msun)
EPS = 0.206986 # kpc/h

def main(snap_name):
    with h5.File(snap_name, 'r+') as fs:

        # print('Opened')

        NumPart_ThisFile = fs['Header'].attrs['NumPart_ThisFile']

        MassDMs = fs['PartType1/Masses'][:]
        PosDMs = fs['PartType1/Coordinates'][:]

        if NumPart_ThisFile[2] > 0:
            MassStars = fs['PartType2/Masses'][:]
            PosStars = fs['PartType2/Coordinates'][:]

        elif NumPart_ThisFile[3] > 0:
            MassStars = fs['PartType3/Masses'][:]
            PosStars = fs['PartType3/Coordinates'][:]

        # print('Read')

        phis = twobody.star_potential(MassDMs, PosDMs[:, 0], PosDMs[:, 1], PosDMs[:, 2],
                                      MassStars, PosStars[:, 0], PosStars[:, 1], PosStars[:, 2],
                                      EPS)#, len(MassDMs), len(MassStars))
        phis = -GRAV_CONST * phis

        phid = twobody.dm_potential(MassDMs, PosDMs[:, 0], PosDMs[:, 1], PosDMs[:, 2],
                                    MassStars, PosStars[:, 0], PosStars[:, 1], PosStars[:, 2],
                                    EPS)#, len(MassDMs), len(MassStars))
        phid = -GRAV_CONST * phid

        # if np.isnan(phid[0]):
        #     raise ValueError("DM potential not calculated properly.")

        # print('Calculated')

        try:
            fs.create_dataset('PartType1/Potential', data=phid)
            if NumPart_ThisFile[2] > 0:
                fs.create_dataset('PartType2/Potential', data=phis)
            elif NumPart_ThisFile[3] > 0:
                fs.create_dataset('PartType3/Potential', data=phis)

        except RuntimeError:
            fs['PartType1/Potential'][:] = phid
            if NumPart_ThisFile[2] > 0:
                fs['PartType2/Potential'][:] = phis
            elif NumPart_ThisFile[3] > 0:
                fs['PartType3/Potential'][:] = phis

        # print('Wrote')

    return

if __name__ == '__main__':

    # _, snap_name = sys.argv

    galaxy_name_list = [
         # '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps',
         # '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_seed1',
         # '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_seed2',
         # '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_seed3',
         # '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_seed4',
         # '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_seed5',
         # '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_seed6',
         # '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_seed7',
         # '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_seed8',
         # '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_seed9',
         # '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps',
         # '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_seed1',
         # '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_seed2',
         # '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_seed3',
         # '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_seed4',
         # '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_seed5',
         # '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_seed6',
         # '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_seed7',
         # '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_seed8',
         # '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_seed9',
         # '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps',
         # '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed1',
         # '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed2',
         # '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed3',
         # '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed4',
         # '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed5',
         # '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed6',
         # '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed7',
         # '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed8',
         # '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_seed9',
         # '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps',
         # '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed1',
         # '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed2',
         # '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed3',
         # '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed4',
         # '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed5',
         # '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed6',
         # '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed7',
         # '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed8',
         # '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_seed9',
         # '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps',
         # '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_seed1',
         # '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_seed2',
         # '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_seed3',
         # '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_seed4',
         # '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_seed5',
         # '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_seed6',
         # '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_seed7',
         # '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_seed8',
         # '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_seed9',
         # '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps',
        #  '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed1',
        #  '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed2',
        #  '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed3',
        #  '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed4',
        #  '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed5',
        #  '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed6',
        #  '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed7',
        #  '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed8',
        #  '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_seed9',
        # '../galaxies/mu_0p2/fdisk_0p01_lgMdm_8_V200_200kmps',
        # '../galaxies/mu_0p2/fdisk_0p01_lgMdm_8_V200_200kmps_seed1',
        # '../galaxies/mu_0p2/fdisk_0p01_lgMdm_8_V200_200kmps_seed2',
        # '../galaxies/mu_0p2/fdisk_0p01_lgMdm_8_V200_200kmps_seed3',
        # '../galaxies/mu_0p2/fdisk_0p01_lgMdm_8_V200_200kmps_seed4',
        # '../galaxies/mu_0p2/fdisk_0p01_lgMdm_8_V200_200kmps_seed5',
        # '../galaxies/mu_0p2/fdisk_0p01_lgMdm_8_V200_200kmps_seed6',
        # '../galaxies/mu_0p2/fdisk_0p01_lgMdm_8_V200_200kmps_seed7',
        # '../galaxies/mu_0p2/fdisk_0p01_lgMdm_8_V200_200kmps_seed8',
        # '../galaxies/mu_0p2/fdisk_0p01_lgMdm_8_V200_200kmps_seed9',
        # '../galaxies/mu_0p5/fdisk_0p01_lgMdm_8_V200_200kmps',
        # '../galaxies/mu_0p5/fdisk_0p01_lgMdm_8_V200_200kmps_seed1',
        # '../galaxies/mu_0p5/fdisk_0p01_lgMdm_8_V200_200kmps_seed2',
        # '../galaxies/mu_0p5/fdisk_0p01_lgMdm_8_V200_200kmps_seed3',
        # '../galaxies/mu_0p5/fdisk_0p01_lgMdm_8_V200_200kmps_seed4',
        # '../galaxies/mu_0p5/fdisk_0p01_lgMdm_8_V200_200kmps_seed5',
        # '../galaxies/mu_0p5/fdisk_0p01_lgMdm_8_V200_200kmps_seed6',
        # '../galaxies/mu_0p5/fdisk_0p01_lgMdm_8_V200_200kmps_seed7',
        # '../galaxies/mu_0p5/fdisk_0p01_lgMdm_8_V200_200kmps_seed8',
        # '../galaxies/mu_0p5/fdisk_0p01_lgMdm_8_V200_200kmps_seed9',
        # '../galaxies/mu_2/fdisk_0p01_lgMdm_8_V200_200kmps',
        # '../galaxies/mu_2/fdisk_0p01_lgMdm_8_V200_200kmps_seed1',
        # '../galaxies/mu_2/fdisk_0p01_lgMdm_8_V200_200kmps_seed2',
        # '../galaxies/mu_2/fdisk_0p01_lgMdm_8_V200_200kmps_seed3',
        # '../galaxies/mu_2/fdisk_0p01_lgMdm_8_V200_200kmps_seed4',
        # '../galaxies/mu_2/fdisk_0p01_lgMdm_8_V200_200kmps_seed5',
        # '../galaxies/mu_2/fdisk_0p01_lgMdm_8_V200_200kmps_seed6',
        # '../galaxies/mu_2/fdisk_0p01_lgMdm_8_V200_200kmps_seed7',
        # '../galaxies/mu_2/fdisk_0p01_lgMdm_8_V200_200kmps_seed8',
        # '../galaxies/mu_2/fdisk_0p01_lgMdm_8_V200_200kmps_seed9',
        # '../galaxies/spherical/fbulge_0p01_lgMdm_8_V200_200kmps_timestep_0p005',
        # '../galaxies/spherical/fbulge_0p01_lgMdm_8_V200_200kmps_timestep_0p01',
        # '../galaxies/spherical/fbulge_0p01_lgMdm_8_V200_200kmps_timestep_0p04',
        # '../galaxies/mu_0p2/fbulge_0p01_lgMdm_7_V200_200kmps',
        # '../galaxies/mu_0p5/fbulge_0p01_lgMdm_7_V200_200kmps',
        # '../galaxies/mu_1/fbulge_0p01_lgMdm_7_V200_200kmps',
        # '../galaxies/mu_2/fbulge_0p01_lgMdm_7_V200_200kmps',
        # '../galaxies/mu_5/fbulge_0p01_lgMdm_7_V200_200kmps',
        # '../galaxies/mu_25/fbulge_0p01_lgMdm_7_V200_200kmps',
        # '../galaxies/mu_0p2/fdisk_0p01_lgMdm_7_V200_200kmps',
        # '../galaxies/mu_0p5/fdisk_0p01_lgMdm_7_V200_200kmps',
        # '../galaxies/mu_1/fdisk_0p01_lgMdm_7_V200_200kmps',
        # '../galaxies/mu_2/fdisk_0p01_lgMdm_7_V200_200kmps',
        # '../galaxies/mu_5/fdisk_0p01_lgMdm_7_V200_200kmps',
        # '../galaxies/mu_25/fdisk_0p01_lgMdm_7_V200_200kmps',
        '../galaxies/mu_0p2/fbulge_0p01_lgMdm_8_V200_200kmps_long',
        '../galaxies/mu_0p5/fbulge_0p01_lgMdm_8_V200_200kmps_long',
        '../galaxies/mu_1/fbulge_0p01_lgMdm_8_V200_200kmps_long',
        '../galaxies/mu_2/fbulge_0p01_lgMdm_8_V200_200kmps_long',
        '../galaxies/mu_5/fbulge_0p01_lgMdm_8_V200_200kmps_long',
        '../galaxies/mu_25/fbulge_0p01_lgMdm_8_V200_200kmps_long'
    ]

    start_time = time.time()

    l = len(galaxy_name_list)

    for i, galaxy_name in enumerate(galaxy_name_list):

        print(galaxy_name)

        if i != 0:
            current_time = time.time()
            print('Estimate time left (s):',  round((current_time - start_time) / i * (l-i)),
                  'out of (s):', round((current_time - start_time) / i * l))

        for snap in range(1001): #(101) (401)
            snap_name = galaxy_name + '/data/snapshot_{0:03d}.hdf5'.format(int(snap))
            main(snap_name)

            if snap % 40 == 0:
                print(snap)

    print('Finished successfully.')