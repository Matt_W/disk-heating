#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 16 10:38:04 2021

@author: matt
"""

import numpy as np

import matplotlib.pyplot as plt

#following description in Thob et al. 2019
#(see e.g. Dubinski & Carlberg 1991; Bett 2012; Schneider et al. 2012)

def reduced_quadrupole_moments_of_mass_tensor(r_p, m_p, e2_p):
  '''Calculates the reduced inertia tensor
  M_i,j = sum_p m_p/r_~p^2 . r_p,i r_p,j / sum_p m_p/r_p^2
  Itterative selection is done in the other function.
  '''
  norm = m_p / e2_p

  m = np.zeros((3,3))

  for i in range(3):
    for j in range(3):
      m[i,j] = np.sum(norm * r_p[:, i] * r_p[:, j])

  #this line does not effect result
  m /= np.sum(norm)

  return(m)

def process_tensor(m):
  '''
  '''
  #I think I messed np.linalg.eigh(m) up when I was first writing thins
  if np.any(np.isnan(m)):
    print('Warning: Nans in tensor. Returning identity instead.')
    return(np.ones(3, dtype=np.float64), np.identity(3, dtype=np.float64))

  (eigan_values, eigan_vectors) = np.linalg.eig(m)

  order = np.flip(np.argsort(eigan_values))

  eigan_values  = eigan_values[order]
  eigan_vectors = eigan_vectors[:, order]

  return(eigan_values, eigan_vectors)

def defined_particles(pos, mass, eigan_values, eigan_vectors):
  '''Assumes eigan values are sorted
  '''
  #projection along each axis
  projected_a = (pos[:,0] * eigan_vectors[0,0] + pos[:,1] * eigan_vectors[1,0] +
                 pos[:,2] * eigan_vectors[2,0])
  projected_b = (pos[:,0] * eigan_vectors[0,1] + pos[:,1] * eigan_vectors[1,1] +
                 pos[:,2] * eigan_vectors[2,1])
  projected_c = (pos[:,0] * eigan_vectors[0,2] + pos[:,1] * eigan_vectors[1,2] +
                 pos[:,2] * eigan_vectors[2,2])

  #ellipse distance #Thob et al. 2019 eqn 4.
  ellipse_distance = (np.square(projected_a) + np.square(projected_b) / (eigan_values[1]/eigan_values[0]) +
                      np.square(projected_c) / (eigan_values[2]/eigan_values[0]) )

  # #this seems to make almost no difference. 
  # #I'm not convinced that the method in Thob is the correct way to do this
  # #ellipse radius #Thob et al. 2019 eqn 4.
  # ellipse_radius = np.power((eigan_values[1] * eigan_values[2]) / np.square(eigan_values[0]), 1/3
  #                           ) * 900
  # #Thob et al. 2019 eqn 4.
  # inside_mask = ellipse_distance <= ellipse_radius
  # return(pos[inside_mask], mass[inside_mask], ellipse_distance[inside_mask])
  
  return(pos, mass, ellipse_distance)

def find_abc(pos, mass, converge_tol=0.01, max_iter=100):
  '''Finds the major, intermediate and minor axes.
  Follows Thob et al. 2019 using reduced quadrupole moment of mass to bias towards
  particles closer to the centre
  '''
  #start off speherical
  r2 = np.square(np.linalg.norm(pos, axis=1))

  #stop problems with r=0
  pos  = pos[r2 != 0]
  mass = mass[r2 != 0]
  r2   = r2[r2 != 0]

  #mass tensor of particles
  m = reduced_quadrupole_moments_of_mass_tensor(pos, mass, r2)

  #linear algebra stuff
  (eigan_values, eigan_vectors) = process_tensor(m)

  #to see convergeance
  cona = eigan_values[2] / eigan_values[0]
  bona = eigan_values[1] / eigan_values[0]

  done = False
  for i in range(max_iter):

    #redefine particles, calculate ellipse distance
    (pos, mass, ellipse_r2) = defined_particles(pos, mass, eigan_values, eigan_vectors)

    #mass tensor of new particles
    m = reduced_quadrupole_moments_of_mass_tensor(pos, mass, ellipse_r2)

    #linear algebra stuff
    (eigan_values, eigan_vectors) = process_tensor(m)

    if (1 - eigan_values[2] / eigan_values[0] / cona < converge_tol*converge_tol) and (
        1 - eigan_values[1] / eigan_values[0] / bona < converge_tol*converge_tol):
      #converged
      done = True
      break

    else:
      cona = eigan_values[2] / eigan_values[0]
      bona = eigan_values[1] / eigan_values[0]

  if not done:
    print('Warning: Shape did not converge.')

  if len(mass) < 100:
    print('Warning: Defining shape with <100 particles.')

  return(np.sqrt(eigan_values))

def find_abc_with_outer_bias(pos, mass, converge_tol=0.01, max_iter=100):
  '''Finds the major, intermediate and minor axes.
  Follows Thob et al. 2019 using quadrupole moment of mass to bias towards
  particles further from the centre
  '''
  #start off speherical
  r2 = np.square(np.linalg.norm(pos, axis=1))

  #stop problems with r=0
  pos  = pos[r2 != 0]
  mass = mass[r2 != 0]
  one  = np.ones(len(mass))

  #mass tensor of particles
  m = reduced_quadrupole_moments_of_mass_tensor(pos, mass, one)

  #linear algebra stuff
  (eigan_values, eigan_vectors) = process_tensor(m)

  #to see convergeance
  cona = eigan_values[2] / eigan_values[0]
  bona = eigan_values[1] / eigan_values[0]

  done = False
  for i in range(max_iter):

    #don't need to do this
    #redefine particles, calculate ellipse distance
    # (pos, mass, _) = defined_particles(pos, mass, eigan_values, eigan_vectors)

    #mass tensor of new particles
    m = reduced_quadrupole_moments_of_mass_tensor(pos, mass, np.ones(len(mass)))

    #linear algebra stuff
    (eigan_values, eigan_vectors) = process_tensor(m)

    if (1 - eigan_values[2] / eigan_values[0] / cona < converge_tol*converge_tol) and (
        1 - eigan_values[1] / eigan_values[0] / bona < converge_tol*converge_tol):
      #converged
      done = True
      break

    else:
      cona = eigan_values[2] / eigan_values[0]
      bona = eigan_values[1] / eigan_values[0]

  if not done:
    print('Warning: Shape did not converge.')

  if len(mass) < 100:
    print('Warning: Defining shape with <100 particles.')

  return(np.sqrt(eigan_values))

##############################################################################
#everything else is for testing
def nfw_m(r, rs=10):
  '''
  '''
  x = r/rs

  # m = np.log(1 + x) - x / (1 + x)
  #hernquist
  m = np.square(x) / np.square(1 + x)

  return(m)

def nfw_r(m, rs=10):
  '''
  '''
  sqm = np.sqrt(m)

  #hernquist
  x = - sqm / (sqm - 1)

  r = x * rs

  return(r)

def spherical_r(r):
  '''
  '''
  n = len(r)
  u = 2*np.random.rand(n, 3) -1
  u = u / np.linalg.norm(u, axis=1)[:, np.newaxis]

  out = u * r[:, np.newaxis]

  return(out)

def mock_nfw(n=10000, cona=1, bona=1, rs=10, rmax=1000):
  '''Is actually Hernquist. Couldn't work out nfw inverse cumulate mass profile.
  '''
  mmax = 1.01

  mrand = np.random.rand(n)

  rrand = nfw_r(mrand / mmax, rs)

  pos = spherical_r(rrand)

  pos[:, 1] *= bona
  pos[:, 2] *= cona

  return(pos)

if __name__ == '__main__':

  h = 0.6777
  z = 0
  a = 1 / (1 + z)
  pos  = mock_nfw(int(1e5), cona=0.3, bona=0.4, rs=10) #* 1e-3 * h / a
  mass = np.ones(len(pos)) #* h

  #random rotation
  from scipy.spatial.transform import Rotation
  random = Rotation.from_euler('yx', [360*np.random.rand(),360*np.random.rand()], degrees=True)
  pos = random.apply(pos)

  a,b,c = find_abc(pos, mass)

  print(a, b, c)

  # print(out)
  print(c/a, b/a)

  # # work out best fit
  # likely = lambda R_d: -np.sum(np.log(R / np.square(R_d) * np.exp(-R / R_d)))
  #
  # R_d = minimize(likely, R_d_ICs)
  # R_d = R_d.x[0]

  # from scipy.optimize import minimize
  #
  # r_i = np.linalg.norm(pos, axis=1)
  #
  # nfw_n = lambda r_s, n_0, r: r * r_s * n_0 / (1 + r/r_s)**2
  # nfw_n_integral = lambda r_s, n_0, r: n_0 * r_s**2 * ( 1/(1 + r/r_s) + np.log(1 + r/r_s) )
  # likely = lambda x, r : -(np.sum(np.log(nfw_n(x[0], x[1], r))) -
  #                          (nfw_n_integral(x[0], x[1], x[0]*1e4) - nfw_n_integral(x[0], x[1], x[0]*1e-4)))
  #
  # print(minimize(likely, (10, 1e2), args=(r_i,)))

  pass
