#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 26 Apr 2022

@author: matt
"""

import numpy as np

import matplotlib.pyplot as plt
import matplotlib.animation as animation

# Create a function to apply boundary conditions
def apply_boundary(position, grid_size=1, periodic=True):
  '''periodic or not
  '''
  if not periodic:
    return position

  #too high
#  while (len(position[position > grid_size]) > 0):
  position[position > grid_size] = position[position > grid_size] - 2*grid_size

  #too low
#  while (len(position[position < -grid_size]) > 0):
  position[position < -grid_size] = position[position < -grid_size] + 2*grid_size

  return position


def distances(position, grid_size=1, periodic=True):
    '''Make an array of distances to each point
    '''
    # find N x N x Nd matrix of particle separations
    grid_r = position[:, None, :] - position[:, :, None]

    min_r = grid_r

    if periodic:
      plus_r = grid_r + 2*grid_size
      minus_r = grid_r - 2*grid_size
      min_r = np.min((np.abs(grid_r), np.abs(plus_r), np.abs(minus_r)), axis=0)

    return np.power(np.sum(min_r * min_r, axis=0), 0.5)

def force(position, mass, grid_size=1, soft_length=1e-3, periodic=True):
    '''only looks at closest distances
    '''
    # find N x N x Nd matrix of particle separations
    grid_r = position[:,None,:] - position[:,:,None]

    #vector of minimum distances
    min_r = grid_r

    if periodic:
        plus_r = grid_r + 2*grid_size
        minus_r= grid_r - 2*grid_size
        min_r = np.min((np.abs(grid_r), np.abs(plus_r), np.abs(minus_r)), axis=0)

        grid_r[np.abs(grid_r) != min_r] = 0
        plus_r[np.abs(plus_r) != min_r] = 0
        minus_r[np.abs(minus_r) != min_r] = 0

        min_r = np.sign(grid_r + plus_r + minus_r) * min_r

    #calculate force between each pair of particles. includes softening length
    #not the usual definition of softening length
    force_i = mass[None,:] * mass[:,None] * min_r / np.power(np.sum(min_r * min_r, axis=0) + soft_length, 1.5)

    return -np.sum(force_i, axis=1)


# def scale_positions(position, old_size=1, new_size=1):
#   '''start expanding centerd at (0,0)
#   '''
#   return new_size / old_size * position
# Define procedure to update positions at each timestep


# def update(mass, position, velocity, acceleration, points, dt=1, grid_size=1, soft_length=1e-3):
def update(i, dt=1e-1, grid_size=1, soft_length=1e-3):
    global mass, position, velocity, acceleration, points, txt

    #kick-drift leap frog
    velocity += acceleration * dt / 2

    position += velocity * dt
    position = apply_boundary(position, grid_size=grid_size) # Apply boundary conditions

    acceleration = force(position, mass, grid_size=grid_size, soft_length=soft_length) / mass

    velocity += acceleration * dt / 2

    points.set_data(position[0,:], position[1,:]) # Show 2D projection of first 2 position coordinates
    # if project_3d:
    #   points.set_3d_properties(position[2,:])  ## For 3D projection

    # return mass, position, velocity, acceleration, points

    txt.set(text=str(round(i * dt, 2)))

    return points, txt


# def main():
np.random.seed(0)

Nd = 3

Np = 10 ** Nd

periodic = True

soft_length = 1e-3

mp = 1 / Np

grid_size = 1.0

v_max = np.sqrt(2 * mp / grid_size)

dt = 1e-1
Nt = 20

frame_duration = 100

position = grid_size * (1 - 2 * np.random.random((Nd, Np)))
velocity = v_max * (1 - 2 * np.random.random((Nd, Np)))

mass = mp * np.ones(Np)

acceleration = force(position, mass, grid_size=grid_size, soft_length=soft_length) / mass

###################################plotting####################################
# Set the axes on which the points will be shown
plt.ion() # Set interactive mode on
fig = plt.figure(figsize=(6,6)) # Create frame and set size

ax1 = plt.subplot(111) # For normal 2D projection
plt.xlim(-grid_size,grid_size)  # Set x-axis limits
plt.ylim(-grid_size,grid_size)  # Set y-axis limits

points, = ax1.plot([],[],'o',markersize=4)
txt = ax1.text(-grid_size, grid_size, 'o', va='top', ha='left')

# for i in range(Nt):
#   (mass, position, velocity, acceleration, points
#    ) = update(mass, position, velocity, acceleration, points, dt=dt, grid_size=grid_size, soft_length=soft_length)

# Create animation
# https://matplotlib.org/api/_as_gen/matplotlib.animation.FuncAnimation.html
ani = animation.FuncAnimation(fig, update, #fargs=(position, velocity, acceleration, points),
                              frames=Nt, interval = frame_duration)
# plt.show()
ani.save('../animation.gif')

# if __name__ == '__main__':
#   main()
#   pass