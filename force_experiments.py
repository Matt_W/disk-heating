#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 23 16:36:50 2021

@author: matt
"""
from functools import lru_cache
# from functools import cache

import os

import numpy as np

# import h5py as h5

from scipy.optimize import brentq
from scipy.optimize import minimize
from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.interpolate import interp1d
# from scipy.interpolate import UnivariateSpline
from scipy.integrate import quad
from scipy.integrate import dblquad
from scipy.signal import savgol_filter
from scipy.signal import convolve
from scipy.stats import binned_statistic
from scipy.stats import binned_statistic_2d
import scipy.stats
from scipy.special import erf
from scipy.special import erfinv
from scipy.special import gamma
from scipy.special import gammainc
from scipy.special import kn, iv
from scipy.special import lambertw
from scipy.spatial import distance_matrix
from scipy.spatial.transform import Rotation

import matplotlib

# default
# matplotlib.use('Qt5Agg')
# stop resizing plots
matplotlib.use('TkAgg')

import matplotlib.pyplot as plt
import matplotlib.cm
from matplotlib import ticker
import matplotlib.patheffects as path_effects

# from   matplotlib.colors import LogNorm
# from matplotlib.cm import viridis
# from matplotlib.cm import ScalarMappable
# from matplotlib.colors import Normalize
# from matplotlib.legend_handler import HandlerTuple
# import matplotlib.lines as lines

import seaborn as sns
from seaborn import desaturate

from sphviewer.tools import QuickView
from sphviewer.tools import Blend

import time

import emcee
import corner

import vegas

import imageio

# import splotch as splt

# import halo_calculations as halo
import load_nbody

from my_shape import find_abc
from my_shape import find_abc_with_outer_bias

import halo_calculations

from matplotlib import rcParams

import twobody

rcParams['ps.useafm'] = True
rcParams['pdf.use14corefonts'] = False
rcParams['text.usetex'] = False
rcParams['font.sans-serif'] = ['cmr10', 'Times-Roman']
rcParams['font.weight'] = 'normal'
rcParams['font.size'] = 20
rcParams['xtick.labelsize'] = 20
rcParams['ytick.labelsize'] = 20
rcParams['axes.labelsize'] = 22
rcParams['axes.linewidth'] = 1
rcParams['axes.unicode_minus'] = False
# rcParams['axes.grid'] = True
rcParams['axes.grid'] = False
rcParams['xtick.minor.width'] = 1
rcParams['ytick.minor.width'] = 1
rcParams['xtick.major.width'] = 1
rcParams['ytick.major.width'] = 1
rcParams['xtick.minor.size'] = 4
rcParams['ytick.minor.size'] = 4
rcParams['xtick.major.size'] = 5
rcParams['ytick.major.size'] = 5
rcParams['xtick.direction'] = 'in'
rcParams['ytick.direction'] = 'in'
rcParams['xtick.top'] = True
rcParams['ytick.right'] = True
rcParams['mathtext.fontset'] = 'cm'

GRAV_CONST = 4.301e4  # kpc (km/s)^2 / (10^10Msun) (source ??)


def halo_get_fresh_coord(Halo_A=30, Rmax=1000):

    r = Rmax + 1
    pos = np.zeros(3)

    while (r > Rmax):
        q = np.random.rand()

        if q > 0:
            r = Halo_A * (q + np.sqrt(q)) / (1 - q)
        else:
            r = 0

    phi = np.random.rand() * 2 * np.pi
    theta = np.arccos(np.random.rand() * 2 - 1)

    pos[0] = r * np.sin(theta) * np.cos(phi)
    pos[1] = r * np.sin(theta) * np.sin(phi)
    pos[2] = r * np.cos(theta)

    return pos


def halo_get_potential(r, Halo_A=30, Halo_Mass=100):
    return -GRAV_CONST * Halo_Mass / (r + Halo_A)


def halo_get_truncated_potential(r, Halo_A=30, Halo_Mass=100, Rmax=1000):
    Mass = Halo_Mass / (Rmax / (Rmax + Halo_A)) ** 2

    return -GRAV_CONST * Mass / (r + Halo_A) + GRAV_CONST * Mass * Halo_A / (Halo_A + Rmax)**2


def halo_get_softened_potential(r, Halo_A=30, Halo_Mass=100, eps=0.1):
    raise NotImplementedError
    r_dash = np.sqrt(r**2 + 4 * eps**2)

    return -GRAV_CONST * Halo_Mass / (r_dash + Halo_A)


def halo_get_softened_truncated_potential(r, Halo_A=30, Halo_Mass=100, eps=0.1, Rmax=1000):

    Mass = Halo_Mass / (Rmax / (Rmax + Halo_A)) ** 2

    raise NotImplementedError
    #this is wrong
    r_dash = np.sqrt(r**2 + eps**2)

    # r_dash = np.sqrt(r**2 + 4 * eps**2)
    # r_dash = r + eps

    return -GRAV_CONST * Mass / (r_dash + Halo_A) + GRAV_CONST * Mass * Halo_A / (Halo_A + Rmax)**2


def halo_get_acceleration(pos, Halo_A=30, Halo_Mass=100):
    r = np.linalg.norm(pos)
    fac = GRAV_CONST * Halo_Mass / ((r + 1e-6 * Halo_A) * (r + Halo_A)**2)

    return - fac * pos


def halo_get_truncated_acceleration(pos, Halo_A=30, Halo_Mass=100):
    Mass = Halo_Mass / (Rmax / (Rmax + Halo_A)) ** 2
    r = np.linalg.norm(pos)
    fac = GRAV_CONST * Mass / ((r + 1e-6 * Halo_A) * (r + Halo_A)**2)

    return - fac * pos

########################################################################################################################

def disk_get_fresh_coord(Disk_H=4, Disk_Z0=0.1, Rmax2=1_000_000):

    r2 = Rmax2 + 1
    pos = np.zeros(3)

    while (r2 < Rmax2):
        q = np.random.rand()

        pos[2] = Disk_Z0 / 2 * np.log(q / (1-q))

        q = np.random.rand()

        R = 1
        Rold = 0
        while (np.abs(R - Rold) / R > 1e-7):
            f = (1 + R) * np.exp(-R) + q - 1
            f_ = -R * np.exp(-R)

            Rold = R
            R = R - f / f_

        R *= Disk_H

        phi = np.random.rand() * 2 * np.pi

        pos[0] = R * np.cos(phi)
        pos[1] = R * np.sin(phi)

        r2 = pos[0]**2 + pos[1]**2 + pos[2]**2

    return pos

########################################################################################################################

def generate_halo(N200=100_000, Halo_A=30, Halo_Mass=100, Rmax=1000, seed=42):

    PosDMs = np.zeros((N200, 3))

    np.random.seed(seed)

    for i in range(N200):
        PosDMs[i] =  halo_get_fresh_coord(Halo_A, Rmax)

    return PosDMs

########################################################################################################################

def evaluate_midplane_potential(PosDMs, Halo_Mass=100, eps=0.1, Rmax=1000, nbins=20, ndirections=8):

    xd = PosDMs[:, 0]
    yd = PosDMs[:, 1]
    zd = PosDMs[:, 2]
    nd = len(xd)
    md = Halo_Mass / nd * np.ones(nd)

    # xs = PosStars[:, 0]
    # ys = PosStars[:, 1]
    # zs = PosStars[:, 2]
    # ns = len(xs)
    # ms = MassStar * np.ones(ns)

    xs = np.zeros(1)
    ys = np.zeros(1)
    zs = np.zeros(1)
    ns = 1
    ms = np.zeros(1)

    minR = eps # kpc
    maxR = Rmax # kpc
    # pot_bin_edges = np.logspace(np.log10(minR), np.log10(maxR), num=nbins + 1)
    # Rbins = 10 ** ((np.log10(pot_bin_edges[:-1]) + np.log10(pot_bin_edges[1:])) / 2)
    Rbins = np.logspace(np.log10(minR), np.log10(maxR), nbins)

    # root2 = np.sqrt(2)
    # calculate potential at each radii along x and y axis and diagonals
    # xin = np.concatenate((Rbins, -Rbins, np.zeros(len(Rbins)), np.zeros(len(Rbins)),
    #                       Rbins / root2, -Rbins / root2, -Rbins / root2, Rbins / root2))
    # yin = np.concatenate((np.zeros(len(Rbins)), np.zeros(len(Rbins)), Rbins, -Rbins,
    #                       Rbins / root2, Rbins / root2, -Rbins / root2, -Rbins / root2))

    angles = np.linspace(0, 2*np.pi, ndirections+1)[:-1]

    xin = Rbins * np.cos(angles)[:, np.newaxis]
    yin = Rbins * np.sin(angles)[:, np.newaxis]
    xin = xin.flatten()
    yin = yin.flatten()
    zin = np.zeros(len(xin))
    ni = len(xin)

    start1 = time.time()
    phi = twobody.midplane_potential(md, xd, yd, zd, ms, xs, ys, zs, xin, yin, zin, eps, nd, ns, ni)
    end1 = time.time()

    print(f'Midplane pot took {end1 - start1}s')

    # for j in range(nbins):
    #     # averages each coordinate
    #     # pot_midplane[j] = (phi[j]+phi[j+nbins]+phi[j+2*nbins]+phi[j+3*nbins])*0.25
    #     pot_midplane[j] = -GRAV_CONST * (phi[j] + phi[j + nbins] +
    #                                      phi[j + 2 * nbins] + phi[j + 3 * nbins] +
    #                                      phi[j + 4 * nbins] + phi[j + 5 * nbins] +
    #                                      phi[j + 6 * nbins] + phi[j + 7 * nbins]) * 0.125

    return -GRAV_CONST * phi


def evaluate_midplane_acceleration(PosDMs, z=0, Halo_Mass=100, eps=0.1, Rmax=1000, nbins=20, ndirections=8):

    xd = PosDMs[:, 0]
    yd = PosDMs[:, 1]
    zd = PosDMs[:, 2]
    nd = len(xd)
    md = Halo_Mass / nd * np.ones(nd)

    # xs = PosStars[:, 0]
    # ys = PosStars[:, 1]
    # zs = PosStars[:, 2]
    # ns = len(xs)
    # ms = MassStar * np.ones(ns)

    xs = np.zeros(1)
    ys = np.zeros(1)
    zs = np.zeros(1)
    ns = 1
    ms = np.zeros(1)

    minR = eps # kpc
    maxR = Rmax # kpc
    # pot_bin_edges = np.logspace(np.log10(minR), np.log10(maxR), num=nbins + 1)
    # Rbins = 10 ** ((np.log10(pot_bin_edges[:-1]) + np.log10(pot_bin_edges[1:])) / 2)
    Rbins = np.logspace(np.log10(minR), np.log10(maxR), nbins)

    # root2 = np.sqrt(2)
    # calculate potential at each radii along x and y axis and diagonals
    # xin = np.concatenate((Rbins, -Rbins, np.zeros(len(Rbins)), np.zeros(len(Rbins)),
    #                       Rbins / root2, -Rbins / root2, -Rbins / root2, Rbins / root2))
    # yin = np.concatenate((np.zeros(len(Rbins)), np.zeros(len(Rbins)), Rbins, -Rbins,
    #                       Rbins / root2, Rbins / root2, -Rbins / root2, -Rbins / root2))

    angles = np.linspace(0, 2*np.pi, ndirections+1)[:-1]

    xin = Rbins * np.cos(angles)[:, np.newaxis]
    yin = Rbins * np.sin(angles)[:, np.newaxis]
    xin = xin.flatten()
    yin = yin.flatten()
    zin = z + np.zeros(len(xin))
    ni = len(xin)

    start1 = time.time()
    acc = twobody.midplane_acceleration(md, xd, yd, zd, ms, xs, ys, zs, xin, yin, zin, eps, nd, ns, ni)
    end1 = time.time()

    print(f'Midplane acc took {end1 - start1}s')
    # print(acc)

    # for j in range(nbins):
    #     # averages each coordinate
    #     # pot_midplane[j] = (phi[j]+phi[j+nbins]+phi[j+2*nbins]+phi[j+3*nbins])*0.25
    #     pot_midplane[j] = -GRAV_CONST * (phi[j] + phi[j + nbins] +
    #                                      phi[j + 2 * nbins] + phi[j + 3 * nbins] +
    #                                      phi[j + 4 * nbins] + phi[j + 5 * nbins] +
    #                                      phi[j + 6 * nbins] + phi[j + 7 * nbins]) * 0.125

    acc = np.reshape(acc, (ni, 3), order='F')
    print(acc)

    return GRAV_CONST * acc


if __name__ == '__main__':

    N200 = 1_000_000
    Halo_A = 30
    Halo_Mass = 100
    Rmax = 1_000
    eps = 0.1
    z = 1
    PosDMs = generate_halo(N200=N200, Halo_A=Halo_A, Rmax=Rmax)

    nbins=20
    ndirections=64

    mid_phi = evaluate_midplane_potential(PosDMs, Halo_Mass=Halo_Mass,
                                          eps=eps, Rmax=Rmax, nbins=nbins, ndirections=ndirections)
    mid_force = evaluate_midplane_acceleration(PosDMs, z=z, Halo_Mass=Halo_Mass,
                                               eps=eps, Rmax=Rmax, nbins=nbins, ndirections=ndirections)

    Rbins = np.logspace(np.log10(eps), np.log10(Rmax), nbins)
    # theory_phi = halo_get_potential(Rbins, Halo_A=Halo_A, Halo_Mass=Halo_Mass / (Rmax/(Rmax + Halo_A))**2)
    # theory_phi = halo_get_softened_potential(Rbins, Halo_A=Halo_A, Halo_Mass=Halo_Mass / (Rmax/(Rmax + Halo_A))**2, eps=eps)
    theory_phi = halo_get_truncated_potential(Rbins, Halo_A=Halo_A, Halo_Mass=Halo_Mass, Rmax=Rmax)
    # theory_phi = halo_get_softened_truncated_potential(Rbins, Halo_A=Halo_A, Halo_Mass=Halo_Mass, eps=eps, Rmax=Rmax)

    # theory_acc = np.array([halo_get_acceleration(np.array([Rbin, 0, z]), Halo_A=Halo_A, Halo_Mass=Halo_Mass)
    #                        for Rbin in Rbins])
    theory_acc = np.array([halo_get_truncated_acceleration(np.array([Rbin, 0, z]), Halo_A=Halo_A, Halo_Mass=Halo_Mass)
                           for Rbin in Rbins])

    total_force = np.sqrt(mid_force[:,0]**2 + mid_force[:,1]**2 + mid_force[:,2]**2)
    R_force = np.sqrt(mid_force[:,0]**2 + mid_force[:,1]**2)

    theory_R_force = np.tile(-theory_acc[:, 0], ndirections)
    theory_z_force = np.tile(-theory_acc[:, 2], ndirections)

    # plt.figure()
    # plt.scatter(np.tile(np.log10(Rbins / Halo_A), ndirections), np.log10(R_force))
    # plt.scatter(np.tile(np.log10(Rbins / Halo_A), ndirections), np.log10(mid_force[:,2]))
    #
    # plt.errorbar(np.log10(Rbins / Halo_A), np.log10(-theory_acc[:, 0]))
    # plt.errorbar(np.log10(Rbins / Halo_A), np.log10(-theory_acc[:, 2]))

    plt.figure(figsize=(10,12))

    ax0 = plt.subplot(2, 1, 1)
    ax1 = plt.subplot(2, 1, 2)

    # plt.scatter(np.tile(np.log10(Rbins / Halo_A), ndirections), (R_force - theory_R_force) / theory_R_force)
    # plt.scatter(np.tile(np.log10(Rbins / Halo_A), ndirections), (mid_force[:,0] - theory_R_force) / theory_R_force)
    # plt.scatter(np.tile(np.log10(Rbins / Halo_A), ndirections), (mid_force[:,1] - theory_R_force) / theory_R_force)
    # plt.scatter(np.tile(np.log10(Rbins / Halo_A), ndirections), (mid_force[:,2] - theory_z_force) / theory_z_force)

    data = np.reshape((R_force - theory_R_force) / theory_R_force,
                      (ndirections, nbins))
    violin_parts = ax1.violinplot(data, np.log10(Rbins / Halo_A),
                                  widths=0.2, showmedians=True, showextrema=False)
    for pc in violin_parts['bodies']:
        pc.set_facecolor(f'C0')
        pc.set_edgecolor(f'C0')

    data = np.reshape((mid_force[:,2] - theory_z_force) / theory_z_force,
                      (ndirections, nbins))
    violin_parts = ax1.violinplot(data, np.log10(Rbins / Halo_A),
                                  widths=0.2, showmedians=True, showextrema=False)
    for pc in violin_parts['bodies']:
        pc.set_facecolor(f'C1')
        pc.set_edgecolor(f'C1')

    # plt.errorbar(Rbins, -theory_phi)

    # plt.loglog()
    # plt.semilogx()

    ax0.set_xlim([-2.5, 1.5])
    ax1.set_xlim([-2.5, 1.5])
    ax0.set_ylim([-0.005, 0.005])
    ax1.set_ylim([-2, 2])
    # ax1.set_ylim([-0.1, 0.1])

    ax0.axhline(0, 0, 1, c='k')
    # ax0.set_xlabel(r'$\log r/a$')
    ax0.set_xticklabels([])
    ax0.set_ylabel(r'$(\Phi_{N \rm body} - \Phi_{\rm theory}) / \Phi_{\rm theory}$')
    ax0.axvline(np.log10(eps / Halo_A), 0, 1, c='k', ls=':')

    ax1.axhline(0, 0, 1, c='k')
    ax1.set_xlabel(r'$\log r/a$')
    ax1.set_ylabel(r'$(F_{i, N \rm body} - F_{i, \rm theory}) / F_{i, \rm theory}$')
    ax1.axvline(np.log10(eps / Halo_A), 0, 1, c='k', ls=':')

    # plt.figure()
    # plt.scatter(np.log10(np.tile(Rbins, ndirections) / Halo_A),
    #             (mid_phi - np.tile(theory_phi, ndirections)) / np.tile(theory_phi, ndirections))

    data = np.reshape((mid_phi - np.tile(theory_phi, ndirections)) / np.tile(theory_phi, ndirections), (ndirections, nbins))
    violin_parts = ax0.violinplot(data, np.log10(Rbins / Halo_A),
                                  widths=0.2, showmedians=True, showextrema=False)


    # for i, eps in enumerate([0.01, 1]):
    #     mid_phi = evaluate_midplane_potential(PosDMs, Halo_Mass=Halo_Mass,
    #                                           eps=eps, Rmax=Rmax, nbins=nbins, ndirections=ndirections)
    #
    #     Rbins = np.logspace(np.log10(eps), np.log10(Rmax), nbins)
    #     theory_phi = halo_get_truncated_potential(Rbins, Halo_A=Halo_A, Halo_Mass=Halo_Mass, Rmax=Rmax)
    #
    #     data = np.reshape((mid_phi - np.tile(theory_phi, ndirections)) / np.tile(theory_phi, ndirections),
    #                       (ndirections, nbins))
    #     violin_parts = plt.violinplot(data, np.log10(Rbins / Halo_A),
    #                                   widths=0.2, showmedians=True, showextrema=False)
    #     for pc in violin_parts['bodies']:
    #         pc.set_facecolor(f'C{i+1}')
    #         pc.set_edgecolor(f'C{i+1}')
    #
    #     plt.axvline(np.log10(eps / Halo_A), 0, 1, c=f'C{i+1}')

    # rDM = np.linalg.norm(PosDMs, axis=1)
    # rDM = np.sort(rDM)
    # plt.figure()
    # plt.errorbar(rDM / Halo_A, np.arange(N200)/rDM / N200 * (Rmax)**2/(Rmax + Halo_A)**2)
    # plt.errorbar(rDM / Halo_A, rDM / (rDM + Halo_A)**2)
    # plt.loglog()

    plt.savefig('../galaxies/force_resolution_tests')

    plt.show()

