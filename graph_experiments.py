#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 26 11:03:09 2021

@author: matt
"""

import numpy as np

import matplotlib.pyplot as plt

from scipy.optimize    import minimize

def power_law(x, a=1, b=1):

  y = b * x**a

  return(y)

def flat(x, c=1):

  y = c * np.ones(len(x))

  return(y)

def intercept(a, b, c):

  m = (c / b) ** (1/a)

  return(m)

def function_1(x, a, b, c):

  y = 1 / (1/c + 1/b * x**-a)

  # m = intercept(a, b, c)
  # y = c / (1 + (x/m)**-a)

  return(y)

def function_2(x, a, b, c):

  m = intercept(a, b, c)

  y = c * (1 - np.exp(-(x/m)**a))

  return(y)

def function_4(x, a, b, c, d):

  # m = intercept(a, b, c)

  y = 1 / (1/c + (c / (c - d) - 2) * np.sqrt(b / c**3) * x**(-a/2)  + 1/b * x**-a)

  return(y)


def test():

  # a = np.random.rand()
  # b = np.random.rand()
  # c = np.random.rand()
  a = 1
  b = 1
  c = 1
  d = 0

  x = np.logspace(-2, 2)

  y_power = power_law(x, a, b)
  y_flat  = flat(x, c)

  x_intercept = intercept(a, b, c)

  y_function_1 = function_1(x, a, b, c)
  y_function_2 = function_2(x, a, b, c)

  plt.figure()

  plt.loglog()

  for d in np.arange(0,1,0.1):
    y = function_4(x, a, b, c, d)
    plt.errorbar(x, y)

  plt.errorbar(x, y_power, label='power law')
  plt.errorbar(x, y_flat,  label='flat')

  plt.errorbar(x_intercept, c, marker='o')

  plt.errorbar(x, y_function_1,  label='function 1')

  plt.errorbar(x, y_function_2,  label='function 2')

  plt.legend()

  pass



if __name__ == '__main__':
  plt.close()
  test()