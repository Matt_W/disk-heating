#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 12 10:33:45 2021

@author: matt
"""

import numpy as np

from scipy.interpolate       import interp1d
from scipy.optimize          import brentq
from scipy.optimize          import minimize
from scipy.spatial           import cKDTree
from scipy.spatial.transform import Rotation
from scipy.stats             import binned_statistic

import emcee
import corner

import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from mpl_toolkits import mplot3d

from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.colors as colors


def calculate_fourier_m_profile(m, PosStars, MassStars, R_bin_edges=None):

  #bin stuff
  if type(R_bin_edges) == type(None):
    #0.5 kpc bins out to 30kpc
    R_bin_edges = np.linspace(0, 30, 61)

  number_of_bins = len(R_bin_edges)-1

  #polar coordinates
  R = np.sqrt(PosStars[:,0]**2 + PosStars[:,1]**2)
  phi = np.arctan2(PosStars[:,1], PosStars[:,0])

  a_m = np.zeros(number_of_bins, dtype=np.float64)
  b_m = np.zeros(number_of_bins, dtype=np.float64)
  # A_m = np.zeros(number_of_bins, dtype=np.float64)

  (bin_mass,_,bin_number) = binned_statistic(R, MassStars, bins=R_bin_edges, statistic='sum')

  for i in range(number_of_bins):
    bin_mask = (bin_number == (i+1))

  #   A_m[i] = np.abs(np.sum(MassStars[bin_mask] * np.exp(m * (0+1j) * phi[bin_mask])))
  # A_m /= bin_mass

    a_m[i] = np.sum(MassStars[bin_mask] * np.cos(m * phi[bin_mask]))
    b_m[i] = np.sum(MassStars[bin_mask] * np.sin(m * phi[bin_mask]))

  a_m /= bin_mass
  b_m /= bin_mass

  A_m = np.sqrt(a_m**2 + b_m**2)

  return(A_m, R_bin_edges)

def bar_length_from_fourier_ratios(PosStars, MassStars, R_bin_edges=None,
                                   min_length_search=0.5, max_length_search=10,
                                   plot=True):

  #my A_m might not be mathematically equivelent to the normal ones ...
  I_0,R_bin_edges = calculate_fourier_m_profile(0, PosStars, MassStars, R_bin_edges)
  I_2,_ = calculate_fourier_m_profile(2, PosStars, MassStars, R_bin_edges)
  I_4,_ = calculate_fourier_m_profile(4, PosStars, MassStars, R_bin_edges)
  I_6,_ = calculate_fourier_m_profile(6, PosStars, MassStars, R_bin_edges)

  #Ohta et al. 1990 ??? According to Gau et al. 2019 but I don't see it
  I_b  = I_0 + I_2 + I_4 + I_6
  I_ib = I_0 - I_2 + I_4 - I_6

  #Roshan et al. 2021 (for A_m), Ohta et al. 1990 / Aguerri et al. 2000 (for I_m)
  I_fancy = I_b / I_ib

  min_arg = len(R_bin_edges) - len(R_bin_edges[R_bin_edges > min_length_search])
  arg_max = np.argmax(I_fancy[np.logical_and(R_bin_edges[:-1] < max_length_search,
                                             R_bin_edges[:-1] > min_length_search)]) + min_arg
  R_max = 0.5*(R_bin_edges[arg_max] + R_bin_edges[arg_max+1])
  I_max = I_fancy[arg_max]

  I_min = np.amin(I_fancy[np.logical_and(R_bin_edges[:-1] < max_length_search,
                                         R_bin_edges[:-1] > min_length_search)])

  #Aguerri et al. 2000
  I_cut = 0.5 * (I_max - I_min) + I_min

  #to find where the data crosses the cut. Always found largest crossing in testing
  I_of_R = interp1d(0.5*(R_bin_edges[1:] + R_bin_edges[:-1]), I_fancy)
  zero = lambda R : I_of_R(R) - I_cut

  try:
    # R = brentq(zero, min_length_search, max_length_search)
    R = brentq(zero, R_max, max_length_search)
  except ValueError:
    #I(r_min) and I(r_max) are both below I_cut. No bar. Or something went very wrong.
    R=0
    print('Warning: Failed to find a bar, setting length to 0.')

  if plot:
    plt.figure()
    plt.plot(0.5*(R_bin_edges[1:] + R_bin_edges[:-1]), I_fancy)

    plt.axhline(I_max)
    plt.axhline(I_cut)
    plt.axhline(I_min)

    plt.axvline(R)

  return(R)

def symmetric_bins(phi_bin_n, starting_n):

  out = []
  for n in range(phi_bin_n//4):
    out.append([(n +starting_n )%phi_bin_n,
                (phi_bin_n//2 -n -1 +starting_n )%phi_bin_n,
                (phi_bin_n//2 +n +starting_n )%phi_bin_n,
                (phi_bin_n -n -1 +starting_n )%phi_bin_n])

  return(out)

def least_squares_sin_fixed_period(x, y, x_period=np.pi):

  model = lambda A_phase, x, y: np.sum((y + A_phase[0] * np.sin(
    (x - A_phase[1]) * (2*np.pi) / x_period) )**2)

  out = minimize(model, [np.amax(y), 1e-6], args=(x,y))

  A = out.x[0]
  phase = out.x[1] % x_period
  if A < 0:
    A *= -1
    phase = (phase + x_period/2) % x_period

  return(A, phase)

def calculate_face_on_mass_position_anlge_profile(PosStars, MassStars,
                                                  R_bin_edges=None, phi_bin_n=360,
                                                  plot=True):
  #try to find the angle where reflected mass annuli is most identical
  #uses the "bi-symmetric" density map

  #bin stuff
  if type(R_bin_edges) == type(None):
    #0.5 kpc bins out to 30kpc
    R_bin_edges = np.linspace(0, 30, 61)

  number_of_bins = len(R_bin_edges)-1

  phi_bin_edges = np.linspace(0, 2*np.pi, phi_bin_n+1)

  #polar coordinates
  R = np.sqrt(PosStars[:,0]**2 + PosStars[:,1]**2)
  phi = np.arctan2(PosStars[:,1], PosStars[:,0])

  #get anulii
  (bin_mass,_,bin_number) = binned_statistic(R, MassStars, bins=R_bin_edges, statistic='sum')

  #set up outputs
  out_array = np.zeros((number_of_bins, phi_bin_n))
  phase_array = np.zeros(number_of_bins)

  if plot:
    plt.figure()

  for i in range(number_of_bins):
    bin_mask = (bin_number == (i+1))

    #get bins in direction
    (angle_mass,_,_) = binned_statistic(phi[bin_mask], MassStars[bin_mask], bins=phi_bin_edges, statistic='sum')

    #loop over possible best PAs
    for starting_n in range(phi_bin_n):
      bins = symmetric_bins(phi_bin_n, starting_n)

      for bin_ in bins:
        #the bi-symmetric mass map
        #should be =0 with a -ve gradient at true PA
        out_array[i, starting_n] += (angle_mass[bin_[0]] - angle_mass[bin_[1]] +
                                     angle_mass[bin_[2]] - angle_mass[bin_[3]])

    A, phase = least_squares_sin_fixed_period(phi_bin_edges[:-1][:phi_bin_n], out_array[i], x_period=np.pi)

    phase_array[i] = phase

    # plt.figure()
    if plot:
      x=phi_bin_edges[:-1][:phi_bin_n]
      plt.plot(x, out_array[i]/A)
      plt.plot(x, -np.sin((x - phase) * 2))
      plt.axvline(phase)

  #radians to degrees
  phase_array *= 180 / np.pi

  if plot:
    plt.figure()
    Rs = 0.5*(R_bin_edges[1:] + R_bin_edges[:-1])
    plt.plot(Rs, phase_array)
    plt.xlabel('R')
    plt.ylabel('PA')
    plt.ylim([0,180])

  return(phase_array, R_bin_edges)

###############################################################################

def get_points_on_ellipsoid(a, b, c, n=10):

  #stolen from the internet. there is probably a better and faster way of doing this...

  # goldenRatio = 1 + np.sqrt(5) / 4
  # angleIncrement = np.pi * 2 * goldenRatio
  angleIncrement = 9.79559267269995

  out = np.zeros((n, 3))

  distance = np.linspace(0, 1, n)
  incline = np.arccos(1 - 2 * distance)
  azimuth = angleIncrement * np.arange(n)

  out[:, 0] = a * np.sin(incline) * np.cos(azimuth)
  out[:, 1] = b * np.sin(incline) * np.sin(azimuth)
  out[:, 2] = c * np.cos(incline)

  # ax = plt.axes(projection ='3d')
  # for line in out:
  #   ax.scatter(*line)

  return(out)

def likelihood(abc, pos_stars, mass_stars, density_aim, kdtree, n=20, r=1):

  if abc[0] < 0.0 or abc[1] < 0.0 or abc[2] < 0.0:
    return(np.infty)

  if abc[2] / abc[0] < 0.001:
    return(np.infty)

  out = 0

  ellipse_points = get_points_on_ellipsoid(abc[0], abc[1], abc[2], n)

  for point in ellipse_points:
    density_at_x = get_density_at_x(pos_stars, mass_stars, point, r, kdtree)
    # print(density_at_x, density_aim)
    out += (density_at_x - density_aim)**4

  # print(out)

  out = np.log10(out)

  return(out)

def calculate_shape(pos_stars, mass_stars, density, kdtree=None, n=20, r=1,
                    start_loc=None):

  if start_loc == None:
    start_loc = (1.0, 0.9, 0.2)

  if kdtree == None:
    kdtree = cKDTree(pos_stars)

  # start_loc = [3.67777399, 2.27232909, 1.3686844 ]

  #TODO orientation

  #TODO centering

  out = minimize(likelihood, start_loc, args=(pos_stars, mass_stars, density, kdtree, n, r),
                  method='Nelder-Mead')

  return(out.x)

  # n_burn_in = int(1e2)
  # n_anal = int(1e3)

  # #emcee stuff

  # nwalkers = 10
  # ndim = len(start_loc)
  # p0 = [np.array(start_loc) + 1e-2 * np.random.randn(ndim) for i in range(nwalkers)]

  # sampler = emcee.EnsembleSampler(nwalkers, ndim, likelihood, args=(pos_stars, mass_stars, density),
  #                                 live_dangerously=True)

  # # print("Running burn-in...")
  # # p0, _, _ = sampler.run_mcmc(p0, n_burn_in, skip_initial_state_check=True)
  # # sampler.reset()

  # print("Running...")
  # pos, prob, state = sampler.run_mcmc(p0, n_burn_in + n_anal,
  #                                     skip_initial_state_check=True, progress=True)

  # #get resutls
  # samples = sampler.get_chain(thin=4, discard=n_burn_in, flat=True)

  # params = np.median(samples, axis=0)

  # #fig =
  # corner.corner(samples,
  #               show_titles=True,
  #               # labels=labels,
  #               plot_datapoints=True,
  #               quantiles=[0.16, 0.5, 0.84])

  # print(params)

def plot_shape_profile(pos_stars, mass_stars):

  print('building kdtree')
  kdtree = cKDTree(pos_stars)
  print('finished tree')

  r_eval = 1
  sixty_one = 10
  n = 20

  r_array = np.linspace(0, 20, sixty_one+1)[1:]

  z_guess = np.median(np.abs(pos_stars[:, 2]))
  R_guess = np.sqrt(np.median(pos_stars[:, 0]**2 + pos_stars[:, 1]**2))

  guess_fraction = r_array / R_guess

  out = np.zeros((sixty_one, 3))

  for i, f in enumerate(guess_fraction):

    start_guess = (f * R_guess, f * R_guess, f * z_guess)
    aim = 0

    ellipse_points = get_points_on_ellipsoid(*start_guess, n)

    for point in ellipse_points:
      aim += get_density_at_x(pos_stars, mass_stars, point, r_eval, kdtree)
    aim /= n
    print(start_guess[0], aim)

    out[i] = calculate_shape(pos_stars, mass_stars, aim, kdtree,
                             n=n, r=r_eval, start_loc=start_guess)
    print(out[i])

  #do the plot
  plt.figure()

  plt.errorbar(r_array, out[:,0], c='C0')
  plt.errorbar(r_array, out[:,1], c='C1')
  plt.errorbar(r_array, out[:,2], c='C2')

  plt.errorbar(r_array, r_array, ls=':', c='k')

  ####
  fig = plt.figure()
  ax = fig.add_subplot(111, projection='3d')

  # ax.scatter(pos_stars[:, 0], pos_stars[:, 1], pos_stars[:, 2], marker='.')

  #compute each and plot each ellipsoid iteratively
  for k in range(len(out[:,0])):

    # calculate cartesian coordinates for the ellipsoid surface
    u = np.linspace(0.0, 2.0 * np.pi, 60)
    v = np.linspace(0.0, np.pi, 60)
    x = out[k, 0] * np.outer(np.cos(u), np.sin(v))
    y = out[k, 1] * np.outer(np.sin(u), np.sin(v))
    z = out[k, 2] * np.outer(np.ones_like(u), np.cos(v))

    # for i in range(len(x)):
    #     for j in range(len(x)):
    #         [x[i,j],y[i,j],z[i,j]] = np.dot([x[i,j],y[i,j],z[i,j]], rotation)

    ax.plot_surface(x, y, z,
                    # rstride=3, cstride=3,
                    linewidth=0.1, alpha=0.1, shade=False, antialiased=False)

  #stolen from stack overflow
  def axisEqual3D(ax):
    extents = np.array([getattr(ax, 'get_{}lim'.format(dim))() for dim in 'xyz'])
    sz = extents[:,1] - extents[:,0]
    centers = np.mean(extents, axis=1)
    maxsize = max(abs(sz))
    r = maxsize/2
    for ctr, dim in zip(centers, 'xyz'):
      getattr(ax, 'set_{}lim'.format(dim))(ctr - r, ctr + r)

  axisEqual3D(ax)

  return


###############################################################################

def generate_mock_barred_galaxy(n=10000, bar_mass_fraction=0.5, bar_length=1, bar_width=0.1,
                                disk_size=1, disk_y=0.9, disk_z=0.1, plot=True):
  '''
  '''
  #Bar
  bar_n = int(round(bar_mass_fraction * n))

  # #TODO look up something more physical
  bar_xyz = np.random.normal(size=(bar_n, 3))
  bar_xyz /= np.linalg.norm(bar_xyz, axis=1)[:, np.newaxis]

  bar_r = np.random.rand(bar_n)**3
  bar_xyz *= bar_r[:, np.newaxis]

  bar_xyz[:, 0] *= bar_length
  bar_xyz[:, 1:3] *= bar_width

  #disk
  disk_n = int(round((1 - bar_mass_fraction) * n))

  disk_xyz = np.random.normal(size=(disk_n, 3))
  disk_xyz /= np.linalg.norm(disk_xyz, axis=1)[:, np.newaxis]

  disk_uni = np.random.rand(disk_n)
  #exponential inverse cdf
  disk_r = - disk_size * np.log(1 - disk_uni)

  disk_xyz *= disk_r[:, np.newaxis]

  disk_xyz[:, 1] *= disk_y
  disk_xyz[:, 2] *= disk_z

  galaxy_xyz = np.concatenate((bar_xyz, disk_xyz))

  # #random direction
  # random_direction = 180 * np.random.rand()
  # if plot:
  #   print('bar orientation is', random_direction)
  # galaxy_xyz = Rotation.from_euler('z', random_direction, degrees=True).apply(galaxy_xyz)

  if plot:
    #face on projection
    plt.figure()

    bins = np.linspace(-disk_size*5, disk_size*5, 101)
    d_bin = bins[1] - bins[0]
    weights = np.ones(len(galaxy_xyz[:,0])) / len(galaxy_xyz[:,0]) / d_bin**2
    plt.hist2d(galaxy_xyz[:,0], galaxy_xyz[:,1], bins=bins, weights=weights, norm=LogNorm())

    plt.xlabel('X')
    plt.ylabel('Y')

    #edge on projection
    plt.figure()
    bins = np.linspace(-disk_size*5, disk_size*5, 101)
    d_bin = bins[1] - bins[0]
    weights = np.ones(len(galaxy_xyz[:,0])) / len(galaxy_xyz[:,0]) / d_bin**2
    plt.hist2d(galaxy_xyz[:,0], galaxy_xyz[:,2], bins=bins, weights=weights, norm=LogNorm())

    plt.xlabel('X')
    plt.ylabel('Z')

    # #pdf
    # plt.figure()

    # bins = np.linspace(0,disk_size*5, 101)

    # bar_mask = np.concatenate((np.ones(int(round(bar_mass_fraction * n)), dtype=bool),
    #                            np.zeros(int(round((1-bar_mass_fraction) * n)), dtype=bool)))
    # bar_rs = np.sqrt(galaxy_xyz[bar_mask,0]**2 + galaxy_xyz[bar_mask,1]**2)

    # disk_mask = np.concatenate((np.zeros(int(round(bar_mass_fraction * n)), dtype=bool),
    #                             np.ones(int(round((1-bar_mass_fraction) * n)), dtype=bool)))
    # disk_rs = np.sqrt(galaxy_xyz[disk_mask,0]**2 + galaxy_xyz[disk_mask,1]**2)

    # galaxy_rs = np.sqrt(galaxy_xyz[:,0]**2 + galaxy_xyz[:,1]**2)

    # plt.hist(bar_rs, bins, histtype='step')
    # plt.hist(disk_rs, bins, histtype='step')
    # plt.hist(galaxy_rs, bins, histtype='step')

    # plt.semilogy()
    # plt.xlabel('R')
    # plt.ylabel('M')

  return(galaxy_xyz)

def get_density_at_x(pos_stars, mass_stars, x, r=1, kdtree=None):
  #TODO this function is not necessarily correct in detail
  #should use kernal thing
  #would also make sense to allow for x to be an array so don't have to for loop

  #4/3*pi
  volume = 4.1887902047863905 * r**3

  if kdtree == None:

    pos = pos_stars - x
    mass_in_volume = np.sum(mass_stars[np.sum(pos*pos, axis=1) < r**2])

    print('wrong')

  else:

    mass_in_volume = np.sum(mass_stars[kdtree.query_ball_point(x, r)])

  return(mass_in_volume / volume)

###############################################################################

if __name__ == '__main__':

  n = 100_000
  bar_mass_fraction = 0.6
  bar_length = 6
  bar_width  = 2
  disk_size  = 10
  disk_y     = 0.9

  galaxy_xyz = generate_mock_barred_galaxy(n, bar_mass_fraction, bar_length, bar_width,
                                           disk_size, disk_y, plot=True)
  galaxy_mass = np.ones(n)

  plot_shape_profile(galaxy_xyz, galaxy_mass)

  #indicators
  position_angle_profile = calculate_face_on_mass_position_anlge_profile(galaxy_xyz, np.ones(n),
                                                                          plot=True)

  R = bar_length_from_fourier_ratios(galaxy_xyz, np.ones(n),
                                      plot=True)
  print('Bar size is approx =', R)

  plt.figure()
  for m in range(7):
    A_m, bin_edges = calculate_fourier_m_profile(m, galaxy_xyz, np.ones(n))
    plt.plot(0.5*(bin_edges[:-1]+bin_edges[1:]), A_m, label=str(m))
  plt.legend()

  plt.show()

  pass