#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 10 09:46:14 2020

@author: matt

This is the test version that uses astropy units.
A big TODO to make the code run faster is remove the unit stuff.

Does more than just NFW despite the name.
"""
import numpy as np
from scipy.special     import kn, iv
from scipy.integrate   import quad
from scipy.optimize    import brentq
from scipy.optimize    import minimize
from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.spatial.transform import Rotation
from scipy.signal      import savgol_filter

import os

import astropy.units     as u
import astropy.constants as cst

import matplotlib.pyplot as plt
import splotch           as splt
import time
import twobody

grav_const   = 4.302e4 #kpc (km/s)^2 / (10^10 Msun) (source ??)
hubble_const = 0.06766 #km/s/kpc (Planck 2018)

################################################################################
#halos for plotting purposes.
#taken from my nfw_test.py

def nfw_rho_0(conc, M_vir, R_s):
    '''Calculates the initial NFW density given the concentration, virial mass,
    and scale radius.
    Result in units of M_vir / R_s^3.
    '''
    rho_0 = M_vir / (4*np.pi * R_s**3 * (np.log(1+conc) - conc/(1+conc)))

    return(rho_0)

def nfw_hard_code():
    '''Calculate V_max and R_v_max, which is needed to input to other scripts.
    '''
    conc  = 10
    M_200 = 181.124 * 10**10 *u.Msun  #pretty sure this is wrong
    V_200 = 200 * u.km/u.s

    R_200 = (cst.G * M_200) / V_200**2

    R_s   = R_200 / conc

    rho_0 = nfw_rho_0(conc, M_200, R_s)

    return(rho_0, R_s)

def nfw_mass(r, rho_0, R_s):
    '''mass at a given radii given central potential and scale radius.
    R_s and r shoud be in the same units
    Result in units of rho_0 * R_s^3
    May be numerical errors for small r
    '''
    M = 4*np.pi*rho_0*R_s**3 * (np.log((R_s+r)/R_s) - r/(R_s+r))
    return(M)

def nfw_velocity_circ(r, rho_0, R_s):
    '''potential at a given radii given central potential and scale radius.
    If not given astropy units, assumes kg/m^3 and Mpc.
    '''
    if not isinstance(rho_0, u.Quantity):
      rho_0 *= u.kg / u.m**3
    if not isinstance(R_s, u.Quantity):
      R_s   *= u.kpc
    if not isinstance(r, u.Quantity):
      r     *= u.kpc

    mass = nfw_mass(r, rho_0, R_s)
    v_circ = np.sqrt(cst.G * mass / r)
    v_circ = v_circ.to(u.km/u.s)

    return(v_circ)

def nfw_potential(r, rho_0, R_s):
    '''potential at a given radii given central density and scale radius.
    If not given astropy units, assumes kg/m^3 and Mpc.
    Might not work for r=0.
    '''
    if not isinstance(rho_0, u.Quantity):
      rho_0 *= u.kg / u.m**3
    if not isinstance(R_s, u.Quantity):
      R_s   *= u.kpc
    if not isinstance(r, u.Quantity):
      r     *= u.kpc

    phi = -4*np.pi*cst.G*rho_0*R_s**3/r * np.log(1+r/R_s)

    return(phi)

################################################################################

GalIC_G = 43018.7 / 43009.1727 * cst.G
GalIC_H = 3.2407789e-18 * 3.085678e21 / 1e5 * u.km / u.kpc / u.s

def hernquist_mass_from_GalIC(v_200, c):
    '''
    '''
    if not isinstance(v_200, u.Quantity):
      v_200 *= u.km / u.s

    M_dm = v_200**3 / (10 * GalIC_G * GalIC_H)

    return(M_dm)

def r_200_for_GalIC(v_200=200):
    '''Not actually r_200. Isothermal sphere parameter approximations used in GalIC
    '''
    if not isinstance(v_200, u.Quantity):
      v_200 *= u.km / u.s

    r_200 = v_200 / (10 * GalIC_H)

    return(r_200)

def hernquist_a_from_GalIC(v_200, c):
    '''
    '''
    r_200 = r_200_for_GalIC(v_200)

    a = r_200 / c * np.sqrt(2 * (np.log(1 + c) - c / (1 + c)))

    return(a)

def hernquist_parameters_from_GalIC(v_200, conc):
    '''
    '''
    M = hernquist_mass_from_GalIC(v_200, conc)
    a = hernquist_a_from_GalIC(v_200, conc)

    return(M, a)

def hernquist_mass(r, M_dm, a):
    '''
    '''
    M = M_dm * (r / (r + a))**2

    return(M)

def hernquist_velocity_circ(r, M_dm, a):
    '''
    '''
    if not isinstance(M_dm, u.Quantity):
      M_dm *= 10**10 * u.Msun
    if not isinstance(a, u.Quantity):
      a *= u.kpc
    if not isinstance(r, u.Quantity):
      r *= u.kpc

    mass = hernquist_mass(r, M_dm, a)
    v_circ = np.sqrt(GalIC_G * mass / r)
    v_circ = v_circ.to(u.km/u.s)

    return(v_circ)

def hernquist_potential(r, M_dm, a):
    '''
    '''
    if not isinstance(M_dm, u.Quantity):
      M_dm *= 10**10 * u.Msun
    if not isinstance(a, u.Quantity):
      a *= u.kpc
    if not isinstance(r, u.Quantity):
      r *= u.kpc

    phi = - M_dm * GalIC_G / (a * (1 + r/a))

    return(phi)

################################################################################

def get_cylindrical(PosStars, VelStars):
  '''Calculates cylindrical coordinates.
  '''
  rho      = np.sqrt(PosStars[:,0]**2 + PosStars[:,1]**2)
  varphi   = np.arctan2(PosStars[:,1], PosStars[:,0])
  z        = PosStars[:,2]

  v_rho    = VelStars[:,0] * np.cos(varphi) + VelStars[:,1] * np.sin(varphi)
  v_varphi = -VelStars[:,0]* np.sin(varphi) + VelStars[:,1] * np.cos(varphi)

  # v_rho    = (PosStars[:,0] * VelStars[:,0] + PosStars[:,1] * VelStars[:,1]
  #             ) / np.sqrt(PosStars[:,0]**2 + PosStars[:,1]**2)
  # v_varphi = (PosStars[:,0] * VelStars[:,1] - PosStars[:,1] * VelStars[:,0]
  #             ) / np.sqrt(PosStars[:,0]**2 + PosStars[:,1]**2)

  v_z      = VelStars[:,2]

  return(rho, varphi, z, v_rho, v_varphi, v_z)

################################################################################

def plot_midplane_potential(PosStars, VelStars, MassStar, PotStars,
                            PosDMs,   VelDMs,   MassDM,   PotDMs, rvir):
    '''From gsf Obreja (2018). I stripped it down. It's mostly just a wrapper
    for some fortran codes in two body.
    This function calculates j_c(E).
    '''
    start = time.time()

    #minimum softening length (I think)
    eps  = 0.1
    # minR = 0.1 * eps
    minR = np.amin(np.linalg.norm(PosStars, axis=1)) * 0.9
    maxR = rvir
    nbins = 50
    bin_edges = np.logspace(np.log10(minR), np.log10(maxR), num=nbins+1)
    Rbins = 0.5 * (bin_edges[:-1] + bin_edges[1:])

###############################################################################
    # #calculate potential at each radii along +/- x and y axis
    # xin = np.concatenate((Rbins,-Rbins,np.zeros(len(Rbins)),np.zeros(len(Rbins))))
    # yin = np.concatenate((np.zeros(len(Rbins)),np.zeros(len(Rbins)),Rbins,-Rbins))
    # xin = xin.flatten()
    # yin = yin.flatten()
    # zin = np.zeros(len(xin))
    # ni = len(xin)

    root2 = np.sqrt(2)
    #calculate potential at each radii along x and y axis and diagonals
    xin = np.concatenate((Rbins,-Rbins,np.zeros(len(Rbins)),np.zeros(len(Rbins)),
                          Rbins/root2, -Rbins/root2, -Rbins/root2, Rbins/root2))
    yin = np.concatenate((np.zeros(len(Rbins)),np.zeros(len(Rbins)),Rbins,-Rbins,
                          Rbins/root2, Rbins/root2, -Rbins/root2, -Rbins/root2))
    xin = xin.flatten()
    yin = yin.flatten()
    zin = np.zeros(len(xin))
    ni = len(xin)
###############################################################################

    xd = PosDMs[:,0]
    yd = PosDMs[:,1]
    zd = PosDMs[:,2]
    nd = len(xd)
    md = MassDM * np.ones(nd)

    xs = PosStars[:,0]
    ys = PosStars[:,1]
    zs = PosStars[:,2]
    ns = len(xs)
    ms = MassStar * np.ones(ns)

    # start1 = time.time()
    phi = twobody.midplane_potential(md,xd,yd,zd,ms,xs,ys,zs,xin,yin,zin,eps,nd,ns,ni)
    # end1 = time.time()

    pot_midplane = np.zeros(nbins)
    for j in range(nbins):
        #averages each coordinate
        # pot_midplane[j] = (phi[j]+phi[j+nbins]+phi[j+2*nbins]+phi[j+3*nbins])*0.25
        pot_midplane[j] = -grav_const*(phi[j]+phi[j+nbins]+
                                        phi[j+2*nbins]+phi[j+3*nbins]+
                                        phi[j+4*nbins]+phi[j+5*nbins]+
                                        phi[j+6*nbins]+phi[j+7*nbins])*0.125

    r_stars = np.linalg.norm(PosStars, axis=1)
    r_dms = np.linalg.norm(PosDMs, axis=1)

    star_bins = np.digitize(r_stars, bin_edges)
    dm_bins   = np.digitize(r_dms, bin_edges)

    pot_midplane = np.array([np.mean(np.hstack((
      PotStars[star_bins==(i+1)], PotDMs[dm_bins==(i+1)] )))
                             for i in range(nbins)])

    print(pot_midplane)

    # start2 = time.time()
    vcirc2 = twobody.midplane_vcirc2(md,xd,yd,zd,ms,xs,ys,zs,xin,yin,zin,eps,nd,ns,ni)
    # end2 = time.time()

    #lots of problems with this j_circ
    v_circ_full = np.zeros(nbins)
    v_circ_ogym = np.zeros(nbins)
    for j in range(nbins):
        #averages each coordinate
        # v_circ_full[j] = np.sqrt(grav_const*(vcirc2[j]+vcirc2[j+nbins]+
        #                                 vcirc2[j+2*nbins]+vcirc2[j+3*nbins])*0.25)
        v_circ_full[j] = np.sqrt(grav_const*(vcirc2[j]+vcirc2[j+nbins]+
                                            vcirc2[j+2*nbins]+vcirc2[j+3*nbins]+
                                            vcirc2[j+4*nbins]+vcirc2[j+5*nbins]+
                                            vcirc2[j+6*nbins]+vcirc2[j+7*nbins])*0.125)

        v_circ_ogym[j] = np.sqrt(grav_const*np.mean(np.sort(
          [vcirc2[j+0*nbins],vcirc2[j+1*nbins],
            vcirc2[j+2*nbins],vcirc2[j+3*nbins],
            vcirc2[j+4*nbins],vcirc2[j+5*nbins],
            vcirc2[j+6*nbins],vcirc2[j+7*nbins]])[1:-1]))

    # if plot:
    #   plt.figure()
    #   plt.plot(Rbins, grav_const*vcirc2[np.arange(0*nbins, 1*nbins)])
    #   plt.plot(Rbins, grav_const*vcirc2[np.arange(1*nbins, 2*nbins)])
    #   plt.plot(Rbins, grav_const*vcirc2[np.arange(2*nbins, 3*nbins)])
    #   plt.plot(Rbins, grav_const*vcirc2[np.arange(3*nbins, 4*nbins)])
    #   plt.plot(Rbins, grav_const*vcirc2[np.arange(4*nbins, 5*nbins)])
    #   plt.plot(Rbins, grav_const*vcirc2[np.arange(5*nbins, 6*nbins)])
    #   plt.plot(Rbins, grav_const*vcirc2[np.arange(6*nbins, 7*nbins)])
    #   plt.plot(Rbins, grav_const*vcirc2[np.arange(7*nbins, 8*nbins)])

    #   plt.plot(Rbins, v_circ_full**2, linewidth=5, ls='-.', label='v circ full^2')
    #   plt.plot(Rbins, v_circ_ogym**2, linewidth=5, ls='--', label='v circ ogym^2')
    #   plt.legend()
    #   plt.xlabel(r'$R$')
    #   plt.ylabel(r'$v_c^2$')
    #   plt.semilogx()

    v_circ_full = v_circ_ogym

    #try to fix algorithm mistakes
    for j in range(nbins):
        if np.isnan(v_circ_full[j]):
            print('Warning: problem with v_circ at index', j)
            if (j == 0) :
                print('Warning: index 0 has a problem')
                if not np.isnan(v_circ_full[1]):
                    v_circ_full[0] = v_circ_full * 0.5
                else:
                    print('Warning: index 0 has a big problem')
                    v_circ_full[0] = np.nanmin(v_circ_full) * 0.5
                print('v circ (r min) =', v_circ_full[0])
            elif (not np.isnan(v_circ_full[j-1])) and (not np.isnan(v_circ_full[j+1])):
                v_circ_full[j] = (v_circ_full[j-1] + v_circ_full[j+1]) / 2
            else:
                # raise ValueError('two or more points in a row have problems.')
                print('Warning: two or more points in a row have problems')

    #https://stackoverflow.com/questions/13728392/moving-average-or-running-mean
    # v_circ_smooth = np.convolve(v_circ_full, np.ones(5)/5, mode='same')
    v_circ_smooth = savgol_filter(v_circ_full, 5, 3)

    #classic mass enclosed
    r_dms   = np.linalg.norm(PosDMs, axis=1)
    r_stars = np.linalg.norm(PosStars, axis=1)
    mass_enclosed = np.array([MassStar * np.sum([r_stars < R_]) +
                              MassDM   * np.sum([r_dms < R_])
                              for R_ in Rbins])

    v_circ_shell = np.sqrt(grav_const * mass_enclosed / Rbins)
    v_circ_ssmoth= savgol_filter(v_circ_shell, 5, 3)

    #miget be slow
    mass_inside_r = np.array([MassStar * np.sum([r_stars < r]) +
                              MassDM   * np.sum([r_dms < r])
                              for r in r_stars])

    v_circ_stars = np.sqrt(grav_const * mass_inside_r / r_stars)


    v_circ = np.zeros(nbins)
    #hybrid v_circ
    swap_calc = 10 #kpc
    v_circ = np.zeros(nbins)
    v_circ[Rbins < swap_calc] = v_circ_smooth[Rbins < swap_calc]
    #smoothig messes up the first few points
    v_circ[0:3]               = v_circ_full[0:3]
    v_circ[Rbins > swap_calc] = v_circ_ssmoth[Rbins > swap_calc]

    # else:
    # v_circ = v_circ_ssmoth

  ###############################################################################
    #nfw v_circ
    rho_0, R_s = nfw_hard_code()
    v_circ_nfw = nfw_velocity_circ(Rbins, rho_0, R_s).to(u.km/u.s).value

    pot_nfw    = nfw_potential(Rbins, rho_0, R_s).to(u.km**2/u.s**2).value
    E_nfw      = 0.5 * v_circ_nfw**2 + pot_nfw

    #hernquist v_circ
    M_dm, a      = hernquist_parameters_from_GalIC(200*u.km/u.s, 10)
    v_circ_hernq = hernquist_velocity_circ(Rbins, M_dm, a).to(u.km/u.s).value

    pot_hernq    = hernquist_potential(Rbins, M_dm, a).to(u.km**2/u.s**2).value
    E_hernq      = 0.5 * v_circ_hernq**2 + pot_hernq

    #median velocity in anuli
    v_circ_annuli = np.zeros(len(Rbins))

    for i in range(nbins):
        mask = (np.logical_and(r_stars > Rbins[i], r_stars < Rbins[(i+1)%nbins]))
        v_xy = np.linalg.norm(VelStars[mask, 0:2], axis=1)

        v_circ_annuli[i] = np.median(v_xy)

    #plots
    # plt.figure(figsize=(9,7))

    fig, axs = plt.subplots(nrows=1, ncols=1, sharex=True, figsize=(9,9))
    fig.subplots_adjust(hspace=0)

    axs.plot(Rbins, v_circ_hernq, ls='-.', linewidth=4, c='C5',
             label=r'hernquist profile')
    axs.plot(Rbins, v_circ_nfw,   ls=':',  linewidth=4, c='C6',
             label=r'nfw profile')
    axs.plot(Rbins, v_circ_shell, ls='-',  linewidth=6, c='C0',
             label=r'$\sqrt{GM/r}$')
    axs.plot(Rbins, v_circ_ssmoth,ls='--', linewidth=5, c='C1',
             label=r'smoothed $\sqrt{GM/r}$')
    axs.plot(Rbins, v_circ_annuli,ls='-.', linewidth=5, c='C4',
             label='median velocity in \n $x-y$ plane in annuli')
    axs.plot(Rbins, v_circ_full,  ls='-',  linewidth=4, c='C2',
             label=r'$\sqrt{ \vec{a} \cdot \vec{x} }$')
    axs.plot(Rbins, v_circ_smooth,ls='--', linewidth=4, c='C3',
             label=r'smoothed $\sqrt{ \vec{a} \cdot \vec{x} }$')
    axs.plot(r_stars[np.argsort(r_stars)],v_circ_stars[np.argsort(r_stars)],
             ls='-', linewidth=4, c='C7',
             label=r'$\sqrt{G M / r}$')

    # plt.plot(Rbins, v_circ,       label=r'Hybrid')

    axs.semilogx()
    # axs.xlabel(r'$r$ [kpc]')
    axs.set_ylabel(r'$v_c$ [km/s]')
    axs.legend()


    from mpl_toolkits.axes_grid1 import make_axes_locatable
    divider = make_axes_locatable(axs)
    axdif = divider.append_axes("bottom", 1.4, pad=0.0, sharex=axs)

    # plt.figure(figsize=(9,4))
    # plt.plot(Rbins, v_circ_hernq-v_circ_hernq, ls='-.', linewidth=6,
    #          label=r'hernquist profile')
    # plt.plot(Rbins, v_circ_nfw-v_circ_hernq,   ls=':', linewidth=5,
    #          label=r'nfw profile')
    axdif.plot(Rbins, v_circ_shell-v_circ_hernq, ls='-',  linewidth=6, c='C0',
              label=r'$\sqrt{GM/r}$')
    axdif.plot(Rbins, v_circ_ssmoth-v_circ_hernq,ls='--', linewidth=5, c='C1',
              label=r'$\sqrt{GM/r}$')
    axdif.plot(Rbins, v_circ_annuli-v_circ_hernq,ls='-.', linewidth=5, c='C4',
             label='median velocity in \n $x-y$ plane in annuli')
    axdif.plot(Rbins, v_circ_full-v_circ_hernq,  ls='-',  linewidth=4, c='C2',
              label=r'$\sqrt{ \vec{a} \cdot \vec{x} }$')
    axdif.plot(Rbins, v_circ_smooth-v_circ_hernq,ls='--', linewidth=4, c='C3',
              label=r'smoothed $\sqrt{ \vec{a} \cdot \vec{x} }$')
    # plt.plot(Rbins, v_circ,       label=r'Hybrid')

    axdif.semilogx()
    axdif.set_xlabel(r'$r$ [kpc]')
    axdif.set_ylabel(r'$v_c$ - $v_c$(Hernquist) [km/s]')
    # plt.legend()
    axdif.grid()
    axdif.set_xlim(0.1,10)
###############################################################################

    j_circ   = v_circ*Rbins
    bindingE = 0.5*(v_circ**2) + pot_midplane

##############################################################################

    #   # j_circ   = v_circ_hernq * Rbins
    #   # bindingE = E_hernq
    #   # j_circ   = v_circ_nfw * Rbins
    #   # bindingE = E_nfw

    #plots
    plt.figure()
    plt.plot(Rbins, bindingE,     label='total',     c='C0', ls='-')
    plt.plot(Rbins, pot_midplane, label='potential', c='C0', ls='-.')
    plt.plot(Rbins, 0.5*v_circ**2,label='kinetic',   c='C0', ls=':')

    plt.plot(Rbins, E_nfw,            label='nfw total',     c='C1', ls='-')
    plt.plot(Rbins, pot_nfw,          label='nfw potential', c='C1', ls='-.')
    plt.plot(Rbins, 0.5*v_circ_nfw**2,label='nfw kinetic',   c='C1', ls=':')
    plt.plot(Rbins, E_hernq,            label='hernq total',     c='C2', ls='-')
    plt.plot(Rbins, pot_hernq,          label='hernq potential', c='C2', ls='-.')
    plt.plot(Rbins, 0.5*v_circ_hernq**2,label='hernq kinetic',   c='C2', ls=':')
    plt.semilogx()
    plt.xlabel('r [kpc]')
    plt.ylabel(r'[(km/s)^2]')
    plt.legend()
  ###############################################################################

    #checkes that velocities are monotonically increasing
    check_indexes    = (bindingE - np.roll(bindingE, 1))
    check_indexes[0] = 1
    indexes_fine     = check_indexes > 0

    while np.any(np.logical_not(indexes_fine)):
        print('Warning: velocities are not monotonically increasting.')
        print('Attempting to fix be reducing velocity resolution by',
              sum(np.logical_not(indexes_fine)))

        # print(indexes_fine)

        Rbins        = Rbins[indexes_fine]
        # v_circ       = v_circ[indexes_fine]
        # pot_midplane = pot_midplane[indexes_fine]
        j_circ       = j_circ[indexes_fine]
        bindingE     = bindingE[indexes_fine]

        #checkes that velocities are monotonically increasing
        check_indexes    = (bindingE - np.roll(bindingE, 1))
        check_indexes[0] = 1
        indexes_fine     = check_indexes > 0

        if len(Rbins) < nbins/2:
            raise ValueError('Had to reduce resolution too much.')
            break

    end = time.time()
    print('j_c(E) runtime', end-start)
    return(j_circ, bindingE, Rbins)

###############################################################################

def centre_maximum_potential(PosDMs,   VelDMs,   MassDM,   PotDMs,
                             PosStars, VelStars, MassStar, PotStars,
                             verbose=False, output_delta=False):
  # '''Centre on maximum potenial
  # '''
  '''Potential weighted centre
  '''

  # # if len(PotDMs) < 20000:

  #300 kpc to 1 kpc
  # #looks like result somewhat depends on number of spheres
  # shrink_radii = np.logspace(np.log10(300), np.log10(1), 51)
  # dsphere = shrink_radii[1] / shrink_radii[0]
  #
  # if verbose:
  #   print('Calculating center of mass based on stars in a shrinking radii.')
  #   print('Minimum radii is', shrink_radii[-1])
    #with equal masses

  total_offset = np.zeros(3)

  # #not sure this is 100% robust
  # for sphere_radii in shrink_radii:
  #   r_stars = np.linalg.norm(PosStars, axis=1)
  #   r_dms   = np.linalg.norm(PosDMs,   axis=1)
  #   mask_stars = (r_stars < sphere_radii)
  #   mask_dms   = (r_dms   < sphere_radii)

  #   #this could take a little while with large N
  #   com = (np.sum(MassStar * PosStars[mask_stars], axis=0) +
  #           np.sum(MassDM * PosDMs[mask_dms], axis=0)) / (
  #             MassStar * np.sum(mask_stars) + MassDM * np.sum(mask_dms))


  #   # if np.linalg.norm(com) > 0.2 * (sphere_radii - dsphere*sphere_radii) :
  #   #   print('Warning: Big change of centre position at radii', sphere_radii)

  #   PosStars     -= com
  #   PosDMs       -= com
  #   total_offset += com

  #   com = (np.sum(MassStar * PosStars[mask_stars], axis=0) +
  #           np.sum(MassDM * PosDMs[mask_dms], axis=0)) / (
  #             MassStar * np.sum(mask_stars) + MassDM * np.sum(mask_dms))

  #   PosStars     -= com
  #   PosDMs       -= com
  #   total_offset += com

  #   r_stars = np.linalg.norm(PosStars, axis=1)
  #   r_dms   = np.linalg.norm(PosDMs,   axis=1)
  #   mask_stars = (r_stars < sphere_radii)
  #   mask_dms   = (r_dms   < sphere_radii)
  #   # print(com)
  #   # print((np.sum(MassStar * PosStars[mask_stars], axis=0) + np.sum(
  #   #     MassDM * PosDMs[mask_dms], axis=0)) / (
  #   #     MassStar * np.sum(mask_stars) + MassDM * np.sum(mask_dms)))

  # else:

  # #find particle with greatest potential
  # dm_arg_max   = np.argmin(PotDMs)
  # star_arg_max = np.argmin(PotStars)

  # #find position offset
  # if PotDMs[dm_arg_max] < PotStars[star_arg_max]:
  #   total_offset = PosDMs[dm_arg_max].copy()
  # else:
  #   total_offset = PosStars[star_arg_max].copy()


  # #potential weighted offset based on 1000 most bound DM particles
  # thousand = 1000
  # arg_dm = np.argsort(PotDMs)
  # total_offset = np.sum(PotDMs[arg_dm][:thousand][:,np.newaxis] * PosDMs[arg_dm][:thousand],
  #                       axis=0) / np.sum(PotDMs[arg_dm][:thousand])

  # # this is centre of potential
  # arg_dm = np.argmin(PotDMs)
  # old_offset = PosDMs[arg_dm]

  # print(total_offset - old_offset)
  # print(total_offset)

  #this is centre of potential
  arg_dm = np.argmin(PotDMs)
  total_offset = PosDMs[arg_dm]

  # #correct positions
  PosStars -= total_offset
  PosDMs   -= total_offset

  #find velocity offset
  velocity_apature = 30
  r_stars = np.linalg.norm(PosStars, axis=1)
  r_dms   = np.linalg.norm(PosDMs, axis=1)
  mask_stars = r_stars < velocity_apature
  mask_dms   = r_dms   < velocity_apature
  # mask_stars = np.ones(len(PotStars), dtype=bool)
  # mask_dms   = np.ones(len(PotDMs),   dtype=bool)

  weighted_central_velocity = (np.sum(MassStar * VelStars[mask_stars], axis=0) +
                               np.sum(MassDM   * VelDMs[mask_dms], axis=0)) / (
        MassStar * np.sum(mask_stars) + MassDM * np.sum(mask_dms))

  #correct velocity
  VelStars -= weighted_central_velocity
  VelDMs   -= weighted_central_velocity

  if verbose:
    print('Position offset =', total_offset)
    print('Velocity offset =', weighted_central_velocity)

  if output_delta:
    return(PosDMs, VelDMs, PosStars, VelStars,
           total_offset, weighted_central_velocity)

  return(PosDMs, VelDMs, PosStars, VelStars)


def centre_shrink_spheres(PosDMs,   VelDMs,   MassDM,   PotDMs,
                          PosStars, VelStars, MassStar, PotStars,
                          verbose=False, output_delta=False):
    '''Centre on maximum potenial
    '''

    # 300 kpc to 1 kpc
    #looks like result somewhat depends on number of spheres
    shrink_radii = np.logspace(np.log10(300), np.log10(1), 31)
    dsphere = shrink_radii[1] / shrink_radii[0]

    if verbose:
        print('Calculating center of mass based on stars in a shrinking radii.')
        print('Minimum radii is', shrink_radii[-1])
        # with equal masses

    total_offset = np.zeros(3)

    #not sure this is 100% robust
    for sphere_radii in shrink_radii:
        r_stars = np.linalg.norm(PosStars, axis=1)
        r_dms   = np.linalg.norm(PosDMs,   axis=1)
        mask_stars = (r_stars < sphere_radii)
        mask_dms   = (r_dms   < sphere_radii)

        #this could take a little while with large N
        com = (np.sum(MassStar * PosStars[mask_stars], axis=0) +
                np.sum(MassDM * PosDMs[mask_dms], axis=0)) / (
                  MassStar * np.sum(mask_stars) + MassDM * np.sum(mask_dms))

        # if np.linalg.norm(com) > 0.2 * (sphere_radii - dsphere*sphere_radii) :
        #   print('Warning: Big change of centre position at radii', sphere_radii)

        PosStars     -= com
        PosDMs       -= com
        total_offset += com

    #find velocity offset
    velocity_apature = 30
    r_stars = np.linalg.norm(PosStars, axis=1)
    r_dms   = np.linalg.norm(PosDMs, axis=1)
    mask_stars = r_stars < velocity_apature
    mask_dms   = r_dms   < velocity_apature
    # mask_stars = np.ones(len(PotStars), dtype=bool)
    # mask_dms   = np.ones(len(PotDMs),   dtype=bool)

    weighted_central_velocity = (np.sum(MassStar * VelStars[mask_stars], axis=0) +
                                 np.sum(MassDM   * VelDMs[mask_dms], axis=0)) / (
            MassStar * np.sum(mask_stars) + MassDM * np.sum(mask_dms))

    #correct velocity
    VelStars -= weighted_central_velocity
    VelDMs   -= weighted_central_velocity

    if verbose:
        print('Position offset =', total_offset)
        print('Velocity offset =', weighted_central_velocity)

    if output_delta:
        return(PosDMs, VelDMs, PosStars, VelStars,
               total_offset, weighted_central_velocity)

    return(PosDMs, VelDMs, PosStars, VelStars)


def find_r200(PosDMs, MassDM, PosStars, MassStar):
    '''Assume PosDMs and PosStars are in kpc and MassDM and MassStar are in 10^10
    Msun.
    Not tested with vector mass but probably works.
    '''
    rDMs   = np.linalg.norm(PosDMs, axis=1)
    rStars = np.linalg.norm(PosStars, axis=1)

    # M / 4/3 r_200^3 = 200 * 3/(8 pi) * H^2/G
    # M = 100 * r_200^3 * H^2 / G
    const = hubble_const**2 / grav_const * 100

    #Uniform Mass
    zero = lambda r200 : r200**3 * const - (
      MassDM * np.sum(rDMs < r200) + MassStar * np.sum(rStars < r200))

    # #Vector mass
    # zero = lambda r200 : r200**3 * const - (
    #   np.sum(MassDM[rDMs < r200]) + np.sum(MassStar[rStars < r200]))

    #this could take a little while with large N
    r200 = brentq(zero, (np.amin(rDMs)+10), np.amax(rDMs))

    return(r200)

def apature_mask(pos, apature=200):
    '''Assume already centered. Returns a mask of Trues for particles within apature.
    '''
    mask = np.linalg.norm(pos, axis=1) < apature

    return(mask)

def total_angular_momentum(r_vector, v_vector, masses):
    '''Total angular momentum of given particles.
    Useful for parties.
    '''
    angular_momentum = np.sum(np.cross(r_vector, v_vector) * masses, axis=0)

    return(angular_momentum)

def find_rotaion_matrix(j_vector):
    '''Returns a scipy.spatial.transform.Rotation object.
    '''
    #rotate until x coord = 0
    fy = lambda y : Rotation.from_euler('y', y, degrees=True).apply(j_vector)[0]
    y = brentq(fy, 0, 180)

    #rotate until y coord = 0
    fx = lambda x : Rotation.from_euler('yx', [y,x], degrees=True).apply(j_vector)[1]
    x = brentq(fx, 0, 180)

    #check it isn't upsidedown
    j_tot = Rotation.from_euler('yx', [y,x], degrees=True).apply(j_vector)

    if j_tot[2] < 0:
      x += 180

    return(Rotation.from_euler('yx', [y,x], degrees=True))

def z_specific_angular_momentum(r_vector, v_vector):
    '''Assume equal mass and already alligned.
    '''
    # j_z = np.cross(r_vector, v_vector)[:,2]
    j_z = r_vector[:,0] * v_vector[:,1] - r_vector[:,1] * v_vector[:,0]

    return(j_z)

def p_specific_angular_momentum(r_vector, v_vector):
    '''Assume equal mass
    '''
    j_p = np.linalg.norm(np.cross(r_vector, v_vector)[:,:2], axis=1)

    return(j_p)

#addapted from gsf (Obreja 2018)
################################################################################

def star_potential(PosStars, VelStars, MassStar, PotStars,
                   PosDMs,   VelDMs,   MassDM,   PotDMs, file=''):
    '''From gsf Obreja (2018). I stripped it down. It's mostly just a wrapper
    for some fortran codes in two body.
    This function calculates the binding energy of each star assuming and isolated
    halo.
    '''
    if type(PotStars) == np.ndarray:
        # print('Potential already calculated.')
        return(PotStars)

    if os.path.isfile(file+'.npy'):
        phi = np.load(file+'.npy')
        return(phi)

    start = time.time()
    eps = 0.1

    xd = PosDMs[:,0]
    yd = PosDMs[:,1]
    zd = PosDMs[:,2]
    nd = len(xd)
    md = MassDM * np.ones(nd)

    xs = PosStars[:,0]
    ys = PosStars[:,1]
    zs = PosStars[:,2]
    ns = len(xs)
    ms = MassStar * np.ones(ns)

    if ns > 10000:
      print('Warning: calculating star potential will take a while.')

    phi = twobody.star_potential(md,xd,yd,zd,ms,xs,ys,zs,eps)

    phi = -phi*grav_const

    end = time.time()
    print('star_potential runtime', end-start)

    if ns > 10000:
        print('saving to ', file)
        np.save(file, phi)

    return(phi)

def plot_potential_verify(dm_pos,   dm_vel,   dm_mass,   dm_bind,
                          star_pos, star_vel, star_mass, star_bind,
                          j_circ=None, binding_e=None, r_bins=None):

    dm_r   = np.linalg.norm(dm_pos,   axis=1)
    star_r = np.linalg.norm(star_pos, axis=1)

    dm_kin   = 0.5 * np.square(np.linalg.norm(dm_vel,   axis=1))
    star_kin = 0.5 * np.square(np.linalg.norm(star_vel, axis=1))

    dm_pot   = dm_bind   - dm_kin
    star_pot = star_bind - star_kin

    #mass enclosed
    #measured
    mass_enclosed = np.zeros(len(r_bins))
    for i in range(len(r_bins)):
        # lim = bin_edges[i+1]
        lim = r_bins[i]

        dm_mass_less_than   = np.sum(dm_mass[dm_r < lim])
        star_mass_less_than = np.sum(star_mass[star_r < lim])

        mass_enclosed[i] = (dm_mass_less_than + star_mass_less_than)

    #analytic
    ten = 0
    #nfw v_circ
    rho_0 = 0.001 #10^10 M_sun / kpc^3
    R_s   = 20    #kpc

    #quick and dirty, there are better ways of doing this. Biased to larger values.
    nfw_mass_least_squares = lambda args: np.sum(
      np.square((4 * np.pi * args[0] * np.power(args[1],3) * (
        np.log(1 + r_bins[ten:] / args[1]) - r_bins[ten:]/(args[1] + r_bins[ten:]))) -
        mass_enclosed[ten:]))

    out = minimize(nfw_mass_least_squares, (rho_0, R_s))

    # print(out)

    # #quick and dirty, there are better ways of doing this.
    # nfw_mass_least_squares = lambda args: np.sum(
    #   np.square(np.log10(4 * np.pi * args[0] * np.power(args[1],3) * (
    #     np.log(1 + r_bins[ten:] / args[1]) - r_bins[ten:]/(args[1] + r_bins[ten:]))) -
    #     np.log10(mass_enclosed[ten:])))

    # #had to start very close to solution
    # out = minimize(nfw_mass_least_squares, out.x)

    # print(out)

    rho_0 = out.x[0] * (10**10 * u.M_sun / u.kpc**3)
    R_s   = out.x[1] * u.kpc

    v_nfw      = nfw_velocity_circ(r_bins, rho_0, R_s).to(u.km/u.s).value

    pot_nfw    = nfw_potential(r_bins, rho_0, R_s).to(u.km**2/u.s**2).value
    E_nfw      = 0.5 * v_nfw**2 + pot_nfw

    cum_mass_nfw = nfw_mass(r_bins*u.kpc, rho_0, R_s).to(10**10 * u.M_sun).value

    #hernquist v_circ
    M_dm = 200 #10^10 M_star
    a    = 30  #kpc

    #quick and dirty, there are better ways of doing this.
    hernquist_mass_least_squares = lambda args: np.sum(
      np.square(np.log10(args[0] * np.square(r_bins[ten:] / (r_bins[ten:] + args[1]))) -
        np.log10(mass_enclosed[ten:])))

    out = minimize(hernquist_mass_least_squares, (M_dm, a))

    M_dm = out.x[0] * (10**10 * u.M_sun)
    a    = out.x[1] * u.kpc

    v_hernq      = hernquist_velocity_circ(r_bins, M_dm, a).to(u.km/u.s).value

    pot_hernq    = hernquist_potential(r_bins, M_dm, a).to(u.km**2/u.s**2).value
    E_hernq      = 0.5 * v_hernq**2 + pot_hernq

    cum_mass_hernq = hernquist_mass(r_bins*u.kpc, M_dm, a).to(10**10 * u.M_sun).value

    #plots
    plt.figure()
    plt.errorbar(r_bins, mass_enclosed,  ls='-', label='Galaxy', fmt='.')

    plt.errorbar(r_bins, cum_mass_nfw,   ls='-', label='NFW',    fmt='.')
    plt.errorbar(r_bins, cum_mass_hernq, ls='-', label='Hernq',  fmt='.')

    plt.loglog()
    plt.xlim(1e-1,1e3)
    # plt.xlim(1e-1,200)
    plt.xlabel(r'r [kpc]')
    plt.ylabel(r'Cumulative Mass [$10^{10}$M$_\odot$]')
    plt.legend(loc='upper left')

    plt.figure()
    # plt.errorbar(dm_r,   dm_bind,   ls='', fmt='.', alpha=0.8, label='dm binding')
    plt.errorbar(star_r, star_bind, ls='', fmt='.', alpha=0.8, label='star binding')

    # plt.errorbar(dm_r,   dm_pot,   ls='', fmt='.', alpha=0.8, label='dm potential')
    plt.errorbar(star_r, star_pot, ls='', fmt='.', alpha=0.8, label='star potential')

    # plt.errorbar(dm_r,   dm_kin,   ls='', fmt='.', alpha=0.8, label='dm kinetic')
    plt.errorbar(star_r, star_kin, ls='', fmt='.', alpha=0.8, label='star kinetic')

    kinetic_e   = 0.5*np.square(j_circ / r_bins)
    potential_e = binding_e - kinetic_e

    plt.errorbar(r_bins, binding_e,   label='total',     c='k', ls='-',  fmt='s')
    plt.errorbar(r_bins, potential_e, label='potential', c='k', ls='-.', fmt='o')
    plt.errorbar(r_bins, kinetic_e,   label='kinetic',   c='k', ls=':',  fmt='^')

    plt.errorbar(r_bins, E_nfw,        label='NFW total',     c='C3', ls='-',  fmt='s')
    plt.errorbar(r_bins, pot_nfw,      label='NFW potential', c='C3', ls='-.', fmt='o')
    plt.errorbar(r_bins, 0.5*v_nfw**2, label='NFW kinetic',   c='C3', ls=':',  fmt='^')

    plt.errorbar(r_bins, E_hernq,        label='Hernq total',     c='C4', ls='-',  fmt='s')
    plt.errorbar(r_bins, pot_hernq,      label='Hernq potential', c='C4', ls='-.', fmt='o')
    plt.errorbar(r_bins, 0.5*v_hernq**2, label='Hernq kinetic',   c='C4', ls=':',  fmt='^')

    plt.semilogx()
    plt.xlim(1e-1,1e3)
    # plt.xlim(1e-1,200)
    plt.xlabel(r'r [kpc]')
    plt.ylabel(r'energy [(km/s)$^2$]')
    plt.legend(loc='upper left')

    print(np.amax(j_circ / r_bins))

    return

def midplane_potential(PosStars, VelStars, MassStar, PotStars,
                       PosDMs,   VelDMs,   MassDM,   PotDMs):
    '''From gsf Obreja (2018). I stripped it down. It's mostly just a wrapper
    for some fortran codes in two body.
    This function calculates j_c(E).
    '''
    r_stars = np.linalg.norm(PosStars, axis=1)
    r_dms   = np.linalg.norm(PosDMs,   axis=1)

    # minR = min(np.amin(r_stars[r_stars!=0]), np.amin(r_dms[r_dms!=0])) * 0.9
    maxR = np.amax(r_stars) * 1.1

    change_r = 10 #kpc
    smol_gap = 1  #kpc
    smol_dr  = np.amin((10, np.ceil(np.sqrt(np.sum(r_dms<smol_gap)))))

    bin_edge_tiny = np.arange(0.1, smol_gap, smol_gap/smol_dr)
    bin_edge_low  = np.arange(0,   change_r, smol_gap)[1:]
    #first gap is still smol_gap
    bin_edge_hi   = 10**np.arange(np.log10(change_r), np.log10(maxR),
                                 np.log10(1 + smol_gap/change_r))
    bin_edges = np.hstack((bin_edge_tiny, bin_edge_low, bin_edge_hi))
    Rbins = 0.5 * (bin_edges[:-1] + bin_edges[1:])
    nbins = len(Rbins)

    #
    # if the mass of the disk dominates, should instead use
    # two_body.midplane_potential
    #

    star_bins = np.digitize(r_stars, bin_edges)
    dm_bins   = np.digitize(r_dms, bin_edges)

    pot_midplane = np.array([np.mean(np.hstack((
      PotStars[star_bins==(i+1)], PotDMs[dm_bins==(i+1)] )))
                              for i in range(nbins)])
    # pot_midplane = np.array([np.mean(PotStars[star_bins==(i+1)])
    #                          for i in range(nbins)])

    indexes_fine = np.logical_not(np.isnan(pot_midplane))

    if np.any(np.logical_not(indexes_fine)):
        print('Warning: empty potential bins.')
        print('Attempting to fix be reducing resolution by',
              sum(np.isnan(pot_midplane)))

        Rbins        = Rbins[indexes_fine]
        bin_edges    = bin_edges[np.hstack((True, indexes_fine))]

        pot_midplane = pot_midplane[indexes_fine]

        if len(Rbins) < nbins/2:
            raise ValueError('Had to reduce resolution too much.')

    #might not be stricky correct with different bin widths
    pot_midplane = savgol_filter(pot_midplane, 5, 3)

    # #plot when needed
    # plt.figure()
    # plt.errorbar(Rbins, pot_midplane, fmt='*', ls='-', lw=5, c='k')
    # for i in range(nbins):
    #   # print(sum(star_bins==i), sum(dm_bins==i))
    #   plt.errorbar(r_dms[dm_bins==i],     PotDMs[dm_bins==i],
    #                 ls='', fmt='.')
    #   plt.errorbar(r_stars[star_bins==i], PotStars[star_bins==i],
    #                 ls='', fmt='.')
    # # plt.semilogx()

    #mass enclosed
    n_stars_less = np.cumsum(np.histogram(r_stars, bins=bin_edges)[0])
    n_dms_less   = np.cumsum(np.histogram(r_dms,   bins=bin_edges)[0])

    mass_enclosed = MassStar * n_stars_less + MassDM * (n_dms_less + 1)

    #uses spherical mass distribution approximation
    v_circ_shell  = np.sqrt(grav_const * mass_enclosed / Rbins)

    # plt.figure()
    # v_DMs = np.linalg.norm(VelDMs, axis=1)
    # # plt.errorbar(r_dms, np.linalg.norm(VelDMs, axis=1), fmt='.', ls='')
    # plt.hist2d(r_dms, v_DMs,
    #             bins=[np.linspace(0,100),np.linspace(0,600)])
    # plt.plot(Rbins, v_circ_shell)

    # means = np.array([np.mean(v_DMs[dm_bins==(i+1)]) for i in range(nbins)])
    # plt.plot(Rbins, means)
    # #maxwell-boltzmann distribtuion + isothermal sphere predicts 1d std shoule be:
    # plt.plot(Rbins, means / np.sqrt(2**3 / (np.pi)))

    # stds = np.array([np.std(VelDMs[dm_bins==(i+1),0]) for i in range(nbins)])
    # plt.plot(Rbins, stds)

    # plt.xlim((0,100))

    #smooth over this
    #might not be stricky correct with different bin widths
    v_circ_ssmoth = savgol_filter(v_circ_shell, 5, 3)

    #just cause
    v_circ = v_circ_ssmoth

    j_circ   = v_circ * Rbins
    bindingE = 0.5*(v_circ**2) + pot_midplane

    # #smooth
    # bindingE = savgol_filter(bindingE, 5, 3)

    #checkes that velocities are monotonically increasing
    check_indexes    = (bindingE - np.roll(bindingE, 1))
    check_indexes[0] = 1
    indexes_fine     = check_indexes > 0

    while np.any(np.logical_not(indexes_fine)):
        # print(check_indexes)
        print('Warning: velocities are not monotonically increasting.')
        print('Attempting to fix by reducing resolution by',
              sum(np.logical_not(indexes_fine)))

        # print(indexes_fine)
        if np.isnan(bindingE[0]):
            bindingE[0] = bindingE[np.logical_not(np.isnan(bindingE))][0]

        Rbins        = Rbins[indexes_fine]
        # v_circ       = v_circ[indexes_fine]
        # pot_midplane = pot_midplane[indexes_fine]
        j_circ       = j_circ[indexes_fine]
        bindingE     = bindingE[indexes_fine]

        #checkes that velocities are monotonically increasing
        check_indexes    = (bindingE - np.roll(bindingE, 1))
        check_indexes[0] = 1
        indexes_fine     = check_indexes > 0

        if len(Rbins) < nbins/2:
            raise ValueError('Had to reduce resolution too much.')

    # #plots
    # plt.figure()
    # plt.plot(Rbins, bindingE,     label='total',     c='C0', ls='-')
    # plt.plot(Rbins, pot_midplane, label='potential', c='C0', ls='-.')
    # plt.plot(Rbins, 0.5*v_circ**2,label='kinetic',   c='C0', ls=':')
    # plt.semilogx()

    return(j_circ, bindingE, Rbins)

#the main part
###############################################################################

def calculate_star_phase_space(PosStars, VelStars, MassStar, PotDMs,
                               PosDMs,   VelDMs,   MassDM,   PotStars,
                               align=True, r200_only=False, name='', snap=''):
    '''Does all the calculations for the phase space of stars.
    If PotDMs and PotStars are None, then this script calculates and saves the
    potentials.
    '''
    #galaxy should only need to be approximately centred for this
    r200 = find_r200(PosDMs, MassDM, PosStars, MassStar)

    if r200_only:
        #only care about bound particles
        #need to do this berfore calculating potential
        #all stars are within r200 anyway
        DM_mask   = apature_mask(PosDMs,   r200)
        star_mask = apature_mask(PosStars, r200)

        PosDMs = PosDMs[DM_mask]
        VelDMs = VelDMs[DM_mask]
        PotDMs = PotDMs[DM_mask]

        PosStars = PosStars[star_mask]
        VelStars = VelStars[star_mask]
        PotStars = PotStars[star_mask]
    else:
        #because this function returns the mask
        DM_mask   = np.ones(len(PotDMs),   dtype=bool)
        star_mask = np.ones(len(PotStars), dtype=bool)

    #get potentials
    #DM
    file = '../galaxies/' + str(name) + '/data/snapshot_' + str(snap) + 'DM'
    PotDMs = star_potential(PosDMs,  VelDMs,  MassDM,  PotDMs,
                            PosStars,VelStars,MassStar,PotStars, file)
    #stars
    file = '../galaxies/' + str(name) + '/data/snapshot_' + str(snap)
    PotStars = star_potential(PosStars,VelStars,MassStar,PotStars,
                              PosDMs,  VelDMs,  MassDM,  PotDMs, file)

    if align:
        # centre galaxy. needs potentials done first.
        (PosDMs, VelDMs, PosStars, VelStars
          ) = centre_maximum_potential(PosDMs,   VelDMs,   MassDM,   PotDMs,
                                       PosStars, VelStars, MassStar, PotStars)

        #align orientation of disk
        j_tot = total_angular_momentum(PosStars, VelStars, MassStar)
        #find rotation
        rotation = find_rotaion_matrix(j_tot)

        #rotate DM
        PosDMs = rotation.apply(PosDMs)
        VelDMs = rotation.apply(VelDMs)
        #rotate stars
        PosStars = rotation.apply(PosStars)
        VelStars = rotation.apply(VelStars)

    else:
        print('Warning: align=False. If input is not aligned, there will be problems.')

    #specific angular momentums
    j_z_stars = z_specific_angular_momentum(PosStars, VelStars)
    j_p_stars = p_specific_angular_momentum(PosStars, VelStars)

    #get total energy of stars
    KEStars = 0.5 * np.linalg.norm(VelStars, axis=1)**2
    EnergyStars = KEStars + PotStars

    #get j_c vs E to be interpolated
    #the hard part
    (j_circ_interp, E_interp, R_interp
     ) = midplane_potential(PosStars, VelStars, MassStar, PotStars,
                            PosDMs,   VelDMs,   MassDM,   PotDMs)

    #make interpolator. hopefully midplane_potential takes care of everything.
    try:
        interp = InterpolatedUnivariateSpline(E_interp, j_circ_interp,
                                              k=1, ext='const')

    except ValueError:
      print('Try reducing nbins in halo_calculations.midplane_potential.')
      print('The halo might be clumpy.')
      print('All these values should be positive.')
      print((E_interp - np.roll(E_interp, 1))[1:])
      print((E_interp))
      raise ValueError('x must be strictly increasing')

    #the main point of these calculations
    j_c_stars = interp(EnergyStars)

    return(PosStars, VelStars, star_mask, PosDMs, VelDMs, DM_mask,
           j_z_stars, j_p_stars, j_c_stars, EnergyStars,
           E_interp, j_circ_interp, R_interp)


if __name__ == '__main__':

  pass