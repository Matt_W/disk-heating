#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 23 16:36:50 2021

@author: matt
"""
from functools import lru_cache
# from functools import cache

import os

import numpy as np

# import h5py as h5

from scipy.optimize import brentq
from scipy.optimize import minimize
from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.interpolate import interp1d
# from scipy.interpolate import UnivariateSpline
from scipy.integrate import quad
from scipy.integrate import dblquad
from scipy.signal import savgol_filter
from scipy.signal import convolve
from scipy.stats import binned_statistic
from scipy.stats import binned_statistic_2d
import scipy.stats
from scipy.special import erf
from scipy.special import erfinv
from scipy.special import gamma
from scipy.special import gammainc
from scipy.special import kn, iv
from scipy.special import lambertw
from scipy.spatial import distance_matrix
from scipy.spatial.transform import Rotation

import matplotlib

# default
# matplotlib.use('Qt5Agg')
# stop resizing plots
matplotlib.use('TkAgg')

import matplotlib.pyplot as plt
import matplotlib.cm
from matplotlib import ticker
import matplotlib.patheffects as path_effects

# from   matplotlib.colors import LogNorm
# from matplotlib.cm import viridis
# from matplotlib.cm import ScalarMappable
# from matplotlib.colors import Normalize
# from matplotlib.legend_handler import HandlerTuple
# import matplotlib.lines as lines

import seaborn as sns
from seaborn import desaturate

from sphviewer.tools import QuickView
from sphviewer.tools import Blend

import emcee
import corner

import vegas

import imageio

# import splotch as splt

# import halo_calculations as halo
import load_nbody

from my_shape import find_abc
from my_shape import find_abc_with_outer_bias

import halo_calculations

from matplotlib import rcParams

import twobody

rcParams['ps.useafm'] = True
rcParams['pdf.use14corefonts'] = False
rcParams['text.usetex'] = False
rcParams['font.sans-serif'] = ['cmr10', 'Times-Roman']
rcParams['font.weight'] = 'normal'
rcParams['font.size'] = 20
rcParams['xtick.labelsize'] = 20
rcParams['ytick.labelsize'] = 20
rcParams['axes.labelsize'] = 22
rcParams['axes.linewidth'] = 1
rcParams['axes.unicode_minus'] = False
# rcParams['axes.grid'] = True
rcParams['axes.grid'] = False
rcParams['xtick.minor.width'] = 1
rcParams['ytick.minor.width'] = 1
rcParams['xtick.major.width'] = 1
rcParams['ytick.major.width'] = 1
rcParams['xtick.minor.size'] = 4
rcParams['ytick.minor.size'] = 4
rcParams['xtick.major.size'] = 5
rcParams['ytick.major.size'] = 5
rcParams['xtick.direction'] = 'in'
rcParams['ytick.direction'] = 'in'
rcParams['xtick.top'] = True
rcParams['ytick.right'] = True
rcParams['mathtext.fontset'] = 'cm'

import matplotlib.lines as lines
from matplotlib.legend_handler import HandlerTuple

GRAV_CONST = 4.301e4  # kpc (km/s)^2 / (10^10Msun) (source ??)
HUBBLE_CONST = 0.06777  # km/s/kpc (Planck 2018)
GALIC_HUBBLE_CONST = 0.1  # h km/s/kpc
RHO_CRIT = 3 * HUBBLE_CONST ** 2 / (8 * np.pi * GRAV_CONST)  # 10^10Msun/kpc^3

PC_ON_M = 3.0857e16  # pc/m = kpc/km (wiki)
GYR_ON_S = 3.15576e16  # gyr/s

T_TOT = 9.778943899409334  # Gyr  # 10 * PC_ON_M / GYR_ON_S / CM_ON_S

BURN_IN = 2

like_fail_value = np.inf


# The custom handler class
class HandlerTupleVertical(HandlerTuple):
    def __init__(self, **kwargs):
        HandlerTuple.__init__(self, **kwargs)
        # HandlerLine2D.__init__(self, **kwargs)

    def create_artists(self, legend, orig_handle, xdescent, ydescent, width, height, fontsize, trans):
        # How many lines are there.
        numlines = len(orig_handle)
        handler_map = legend.get_legend_handler_map()

        # divide the vertical space where the lines will go
        # into equal parts based on the number of lines
        height_y = (height / numlines)

        leglines = []
        for i, handle in enumerate(orig_handle):
            handler = legend.get_legend_handler(handler_map, handle)

            legline = handler.create_artists(legend, handle, xdescent, (2 * i + 1) * height_y, width, 2 * height,
                                             fontsize, trans)
            leglines.extend(legline)

        return leglines


def get_cylindrical(pos_stars, vel_stars):
    '''Calculates cylindrical coordinates.
  '''
    rho = np.sqrt(pos_stars[:, 0] ** 2 + pos_stars[:, 1] ** 2)
    phi = np.arctan2(pos_stars[:, 1], pos_stars[:, 0])
    zax = pos_stars[:, 2]

    v_rho = vel_stars[:, 0] * np.cos(phi) + vel_stars[:, 1] * np.sin(phi)
    v_phi = -vel_stars[:, 0] * np.sin(phi) + vel_stars[:, 1] * np.cos(phi)

    # v_rho = (pos_stars[:,0] * vel_stars[:,0] + pos_stars[:,1] * vel_stars[:,1]) / rho
    # v_phi = (pos_stars[:,0] * vel_stars[:,1] - pos_stars[:,1] * vel_stars[:,0]) / rho

    v_z = vel_stars[:, 2]

    return (rho, phi, zax, v_rho, v_phi, v_z)


def get_spherical_coords(pos, vel):

    r = np.linalg.norm(pos, axis=1)
    phi = np.arctan2(pos[:, 1], pos[:, 0])
    theta = np.arccos(pos[:, 2] / (r + 1e-6))

    v_r = vel[:, 0] * np.sin(theta) * np.cos(phi) + vel[:, 1] * np.sin(theta) * np.sin(phi) + vel[:, 2] * np.cos(theta)
    v_phi = -vel[:, 0] * np.sin(phi) + vel[:, 1] * np.cos(phi)
    v_theta = vel[:, 0] * np.cos(theta) * np.cos(phi) + vel[:, 1] * np.cos(theta) * np.sin(phi) - vel[:, 2] * np.sin(theta)

    return r, phi, theta, v_r, v_phi, v_theta


def get_radii_that_are_interesting(r):
    '''Find stellar quarter, half mass and 3 quarter mass assuming equal mass stars.
  '''
    rs = np.sort(r)

    quarter = rs[int(round(len(rs) / 4))]
    half = rs[int(round(len(rs) / 2))]
    three = rs[int(round(3 * len(rs) / 4))]

    return (quarter, half, three)


def get_dex_bins(bin_centres, dex=0.2):
    '''Finds bin edges from centres that span a given dex.
  '''
    edges = np.zeros(2 * len(bin_centres))

    for i, centre in enumerate(bin_centres):
        edges[2 * i] = centre * 10 ** (-dex / 2)
        edges[2 * i + 1] = centre * 10 ** (dex / 2)

    return (edges)


def get_bin_edges(save_name_append='diff', hard_code=True, galaxy_name=''):
    '''Get bin edges. Hard coded to self similar galaxies.
    If "diff" gets dex bins around r_1/4, r_1/2 and r_3/4.
    If "cum" get 0 to r_1/4, r_1/2 and r_3/4 bin edges respectively.
    '''
    if hard_code:
        if galaxy_name == '' or '200kmps' in galaxy_name:
            if 'c_7' in galaxy_name:
                if save_name_append == 'diff':
                    return (np.array([2.28317571, 3.61858964,
                                      4.10575147, 6.50717755,
                                      7.09773475, 11.24915149]))
                elif save_name_append == 'cum':
                    return (np.array([0., 2.8743479232430205,
                                      0., 5.168834857370659,
                                      0., 8.935518647841933]))
            elif 'c_15' in galaxy_name:
                if save_name_append == 'diff':
                    return (np.array([4.53514534, 7.18772097,
                                      7.04444705, 11.16469618,
                                      10.55782598, 16.73302653]))
                elif save_name_append == 'cum':
                    return (np.array([0, 5.709409709084269,
                                      0, 8.868433407362197,
                                      0, 13.291515425532179]))
            else:
                if save_name_append == 'diff':
                    return (np.array([3.25047902, 5.15166206,
                                      5.46707974, 8.66473746,
                                      8.80905631, 13.96141337]))
                elif save_name_append == 'cum':
                    return (np.array([0., 4.09211063,
                                      0., 6.88264561,
                                      0., 11.08994484]))
                elif save_name_append == 'all':
                    print("Warning: save_name_append == 'all' is not supported very well")
                    return (np.array([0., 40.]))
        elif '50kmps' in galaxy_name:
            if save_name_append == 'diff':
                return (np.array([0.79030272, 1.25254541,
                                  1.37812968, 2.18418835,
                                  2.21040947, 3.50326292]))
            elif save_name_append == 'cum':
                return (np.array([0., 0.9949321823649048,
                                  0., 1.7349624748138122,
                                  0., 2.7827406521566296]))
        elif '100kmps' in galaxy_name:
            if save_name_append == 'diff':
                return (np.array([1.58309817, 2.50904152,
                                  2.76394524, 4.38055799,
                                  4.42730402, 7.01680399]))
            elif save_name_append == 'cum':
                return (np.array([0., 1.9930025202055555,
                                  0., 3.479600894074883,
                                  0., 5.573645530295718]))
        elif '400kmps' in galaxy_name:
            if save_name_append == 'diff':
                return (np.array([6.31212233, 10.00403972,
                                  11.02328112, 17.47072321,
                                  17.69392947, 28.04298837]))
            elif save_name_append == 'cum':
                return (np.array([0., 7.946491207817563,
                                  0., 13.877488725754327,
                                  0., 22.27533744444062]))

    (_, MassDM, _, _, _, _, _, PosStars, _, _, _
     ) = load_nbody.load_snapshot(galaxy_name, '002')

    # work out bin edges
    interesting_radii = get_radii_that_are_interesting(np.linalg.norm(PosStars, axis=1))

    if save_name_append == 'diff':
        bin_edges = get_dex_bins(interesting_radii)
    elif save_name_append == 'cum':
        bin_edges = np.zeros(6)
        bin_edges[1] = interesting_radii[0]
        bin_edges[3] = interesting_radii[1]
        bin_edges[5] = interesting_radii[2]

    return (bin_edges)


def get_hernquist_params(galaxy_name=''):
    '''Get Hernquist halo properties from hard coded GalIC inputs.
    GALIC_HUBBLE_CONST = 0.1 (h) km/s/kpc
    '''
    c = 10
    if 'c_7' in galaxy_name:
        c = 7
    if 'c_15' in galaxy_name:
        c = 15

    if galaxy_name == '':
        v200 = 200
    else:
        if '50kmps' in galaxy_name:
            v200 = 50
        elif '100kmps' in galaxy_name:
            v200 = 100
        elif '200kmps' in galaxy_name:
            v200 = 200
        elif '300kmps' in galaxy_name:
            v200 = 300
        elif '400kmps' in galaxy_name:
            v200 = 400
        else:
          raise ValueError('Did not find v200')

        if 'c_4' in galaxy_name:
            c = 4
        elif 'c_5' in galaxy_name:
            c = 5
        elif 'c_7' in galaxy_name:
            c = 7
        elif 'c_15' in galaxy_name:
            c = 15
        elif 'c_25' in galaxy_name:
            c = 25

    f_bary = 0.01

    r200 = v200 / (10 * GALIC_HUBBLE_CONST)

    a_h = r200 / c * np.sqrt(2 * (np.log(1 + c) - c / (1 + c)))

    M200 = v200 ** 3 / (10 * GRAV_CONST * GALIC_HUBBLE_CONST)
    # not sure if need to do this...
    M_h = (1 - f_bary) * M200
    # M_h = M200

    # M_h *= 1.18

    vmax = np.sqrt(GRAV_CONST * M_h / (4 * a_h))

    return (a_h, vmax)


def hernquist_potential(r, galaxy_name=''):
    '''
    '''
    a_h, vmax = get_hernquist_params(galaxy_name)

    GM_h = 4 * a_h * vmax ** 2  # (km/s)^2 kpc

    phi = - GM_h / (r + a_h)

    return (phi)  # (km/s)^2


def get_exponential_disk_potential(r_h, galaxy_name=''):
    '''
  '''
    M_disk, r_disk = get_exponential_disk_params(galaxy_name)

    # #bin centres
    # r_hs = 10**((np.log10(r_h[0::2]) + np.log10(r_h[1::2]))/2)

    y = r_h / (2 * r_disk)

    phi = -GRAV_CONST * M_disk * r_h / (2 * r_disk ** 2) * (iv(0, y) * kn(1, y) - iv(1, y) * kn(0, y))

    # #TODO check and maybe change back
    # phi = - GRAV_CONST * exponential_cumulative_mass(r_h, M_disk, r_disk) / r_h

    return (phi)  # (km/s)^2


def get_exponential_disk_params(galaxy_name=''):
    # found with a measurement
    # c = 10
    # if 'c_7' in galaxy_name:
    #     c = 7
    # if 'c_15' in galaxy_name:
    #     c = 15

    if galaxy_name == '':
        v200 = 200
    else:
        if '50kmps' in galaxy_name:
            v200 = 50
        elif '100kmps' in galaxy_name:
            v200 = 100
        elif '200kmps' in galaxy_name:
            v200 = 200
        elif '300kmps' in galaxy_name:
            v200 = 300
        elif '400kmps' in galaxy_name:
            v200 = 400
        else:
            raise ValueError('Did not find v200')

    f_bary = 0.01

    M200 = v200 ** 3 / (10 * GRAV_CONST * GALIC_HUBBLE_CONST)

    M_disk = f_bary * M200  # 10^10 M_sun

    # M_disk = 1.86 #10^10 M_sun
    # R_disk = 4.15 #kpc

    # use r_200 = v_200 and all disk sizes are same relative to halo
    R_disk = 4.15 * (v200 / 200)  # kpc

    # if 'c_4' in galaxy_name:
    # elif 'c_7' in galaxy_name:
    # elif 'c_15' in galaxy_name:
    # elif 'c_25' in galaxy_name:

    return (M_disk, R_disk)


###############################################################################
# Velocity
# Velocity scale
# Measured
def get_v_200(galaxy_name=''):
    '''Hardcoded from GalIC input.
  '''
    if galaxy_name == '':
        v200 = 200
    else:
        if '50kmps' in galaxy_name:
            v200 = 50
        elif '100kmps' in galaxy_name:
            v200 = 100
        elif '200kmps' in galaxy_name:
            v200 = 200
        elif '300kmps' in galaxy_name:
            v200 = 300
        elif '400kmps' in galaxy_name:
            v200 = 400
        else:
            raise ValueError('Did not find v200')

    return (v200)


def get_r_200(galaxy_name=''):
    '''Hardcoded from GalIC input. Is astucally for isothermal sphere.
  '''
    v200 = get_v_200(galaxy_name)
    r200 = v200 / (10 * GALIC_HUBBLE_CONST)

    return (r200)


def get_rho_200(galaxy_name=''):
    '''Hardcoded from GalIC input.
    '''
    # v200 = get_v_200(galaxy_name)
    # r200 = v200 / (10 * GALIC_HUBBLE_CONST)
    # M200 = v200 ** 3 / (10 * GRAV_CONST * GALIC_HUBBLE_CONST)
    #
    # rho200 = M200 / (4 / 3 * np.pi * r200 ** 3)

    rho200 = 3 * 200 * GALIC_HUBBLE_CONST**2 / (8 * np.pi * GRAV_CONST)

    return (rho200)


def get_dm_dispersion(PosDMs, VelDMs, bin_edges, cylindrical=False):
    '''Calculates 1D DM dispersion by averaging dispersion over x,y,z within bins.
  '''
    if cylindrical:
        R = np.sqrt(PosDMs[:, 0] ** 2 + PosDMs[:, 1] ** 2)
    else:
        R = np.linalg.norm(PosDMs, axis=1)

    sigma = np.zeros(len(bin_edges) // 2)
    for i in range(len(sigma)):
        mask = np.logical_and((R > bin_edges[2 * i]), (R < bin_edges[2 * i + 1]))
        sigma[i] = np.sqrt((np.square(np.std(VelDMs[mask, 0])) +
                            np.square(np.std(VelDMs[mask, 1])) +
                            np.square(np.std(VelDMs[mask, 2]))) / 3)

    return (sigma)


def get_v_circ(PosDMs, MassDM, PosStars, MassStar, bin_edges, cylindrical=False):
    '''Measures circular velocity (sqrt(GM(<r)/r)) for each particle.
    Calculates rms of v_circ of particles within bin_edges.
    Can be unconstantped to make circular velocity profile.

    Need to be carefult that rms of v_c is what you want.
    '''
    if cylindrical:
        R_dm = np.sqrt(PosDMs[:, 0] ** 2 + PosDMs[:, 1] ** 2)
        R_star = np.sqrt(PosStars[:, 0] ** 2 + PosStars[:, 1] ** 2)
    else:
        R_dm = np.linalg.norm(PosDMs, axis=1)
        R_star = np.linalg.norm(PosStars, axis=1)

    # uniform mass
    # R_dm   = np.sort(R_dm)
    # R_star = np.sort(R_star)
    # cum_M_dm   = MassDM   * np.arange(len(R_dm))
    # cum_M_star = MassStar * np.arange(len(R_star))
    # v_circ_dm   = np.sqrt(GRAV_CONST * cum_M_dm   / R_dm)
    # v_circ_star = np.sqrt(GRAV_CONST * cum_M_star / R_star)

    # there is definately a better way to get cumulative mass. This works though.
    eps = 1e-3
    R = np.concatenate((R_dm, R_star)) + eps
    M = np.ones(len(R))
    M[:len(R_dm)] *= MassDM
    M[len(R_dm):] *= MassStar
    R_arg = np.argsort(R)
    R = R[R_arg]
    M = M[R_arg]
    cum_M = np.cumsum(M)

    v_circ = np.sqrt(GRAV_CONST * cum_M / R)
    # remove devide by zero
    # not_inf = np.logical_not(np.isinf(v_circ))
    # R       = R[not_inf]
    # v_circ  = v_circ[not_inf]
    v_circ[np.isinf(v_circ)] = 0

    # rms of v_c**2
    v_circ_binned = np.zeros(len(bin_edges) // 2)
    for i in range(len(v_circ_binned)):
        mask = np.logical_and((R > bin_edges[2 * i]), (R < bin_edges[2 * i + 1]))
        n_mask = np.sum(mask)
        v_circ_binned[i] = np.sqrt(np.sum(v_circ[mask] ** 2) / n_mask)

    return (v_circ_binned)


# Analytic
def get_analytic_hernquist_dispersion(r_h, vmax, a_h, save_name_append='diff'):
    '''What it says in the function name. r_h is actually bin_edges. If diff,
    it will average (in log space) between bins and evaluate at the centre.
    If cum, will get the (Hernquist) density weighted dispersion.
    '''
    gmh = 4 * a_h * vmax ** 2  # kpc km/s

    disp_1d = np.zeros(len(r_h) // 2)

    # differential. evaluate at a point
    if save_name_append == 'diff':
        # to fix if the centre is 0
        r_s = np.copy(r_h)
        if r_s[0] == 0:
            r_s[0] = 0.0001

        # bin centres
        r_hs = 10 ** ((np.log10(r_s[0::2]) + np.log10(r_s[1::2])) / 2)

        for i in range(len(disp_1d)):
            # #binney and tremaine eq 4.219
            # x_h = r_hs[i] / a_h
            # y_h = x_h + 1
            # disp_1d[i] = np.sqrt(gmh / a_h * (x_h * y_h**3 * np.log(y_h/x_h) - x_h/y_h * (
            #   1/4 + 1/3*y_h + 1/2*y_h**2 + y_h**3)))

            # Ludlow et al. 2021 eqn 14 etc.
            disp_1d[i] = np.sqrt(gmh / (12 * a_h) * (
                    12 * r_hs[i] * (r_hs[i] + a_h) ** 3 / a_h ** 4 * np.log1p(a_h / r_hs[i]) -
                    r_hs[i] / (r_hs[i] + a_h) * (25 + 52 * r_hs[i] / a_h +
                                                 42 * (r_hs[i] / a_h) ** 2 + 12 * (r_hs[i] / a_h) ** 3)))

            # - R^2 d^2 Phi / dR^2
            # disp_1d[i] = np.sqrt( 2 * gmh * r_hs[i]**2 / (a_h + r_hs[i])**3)

    # cumulative. evaluate in a range
    elif save_name_append == 'cum':

        g = lambda r: 0 if r == 0 else np.log1p(a_h / r)
        # sympy answer
        f = lambda r: (4 * r ** 3 / a_h ** 4 * g(r) -
                       4 * r ** 2 / a_h ** 3 + 2 * r / a_h ** 2 +
                       (a_h ** 2 + a_h * r + r ** 2) / (
                               a_h ** 3 + 3 * a_h ** 2 * r + 3 * a_h * r ** 2 + r ** 3))

        for i in range(len(disp_1d)):
            c = gmh / 6 / ((r_h[2 * i + 1] / (r_h[2 * i + 1] + a_h)) ** 2 - (r_h[2 * i] / (r_h[2 * i] + a_h)) ** 2)

            disp_1d[i] = c * (f(r_h[2 * i + 1]) - f(r_h[2 * i]))

        disp_1d = np.sqrt(disp_1d)

    return (disp_1d)


def get_analytic_hernquist_circular_velocity(r_h, vmax, a_h, save_name_append='diff'):
    '''What it says in the function name. If diff, it will average (in log space)
    between bins and evaluate at the centre. If cum, will get the (Hernquist) density
    weighted velocity.
    '''
    gmh = 4 * a_h * vmax ** 2  # kpc km/s

    v_c = np.zeros(len(r_h) // 2)

    # differential. evaluate at a point
    if save_name_append == 'diff':
        # to fix if the centre is 0
        r_s = np.copy(r_h)
        if r_s[0] == 0:
            r_s[0] = 0.1

        # bin centres
        r_hs = 10 ** ((np.log10(r_s[0::2]) + np.log10(r_s[1::2])) / 2)

        for i in range(len(v_c)):
            # G * mass enclosed
            gmh_r = gmh * (r_hs[i] / (r_hs[i] + a_h)) ** 2

            v_c[i] = np.sqrt(gmh_r / r_hs[i])

            # # - R^2 d^2 Phi / dR^2
            # v_c[i] = np.sqrt(2 * gmh * r_hs[i]**2 / (a_h + r_hs[i])**3)

    # cumulative. evaluate in a range
    elif save_name_append == 'cum':

        f = lambda r: -a_h * (a_h ** 2 + 4 * a_h * r + 6 * r ** 2) / (a_h + r) ** 4

        for i in range(len(v_c)):
            c = gmh / 6 / ((r_h[2 * i + 1] / (r_h[2 * i + 1] + a_h)) ** 2 - (r_h[2 * i] / (r_h[2 * i] + a_h)) ** 2)

            v_c[i] = c * (f(r_h[2 * i + 1]) - f(r_h[2 * i]))
            
        v_c = np.sqrt(v_c)


    return (v_c)


def get_exponential_disk_velocity(r_h, M_disk, r_disk, save_name_append='diff'):
    '''Calculates the circular velocity for an analytic thin exponential disk
    '''
    # v_c = np.zeros(len(r_h)//2)

    # differential. evaluate at a point
    if save_name_append == 'diff':
        # TODO I don't think this actually works...
        # to fix if the centre is 0
        r_s = np.copy(r_h)
        if r_s[0] == 0:
            r_s[0] = 0.1

        # #should be this but doesn't work?
        # r_s = np.copy(r_h)
        # for i in range(len(r_s)):
        #   if r_s[i] == 0:
        #     r_s[i] = 0.1

        # bin centres
        r_hs = 10 ** ((np.log10(r_s[0::2]) + np.log10(r_s[1::2])) / 2)

        y = r_hs / (2 * r_disk)

        v_c2 = GRAV_CONST * M_disk * r_hs ** 2 / (2 * r_disk ** 3) * (iv(0, y) * kn(0, y) - iv(1, y) * kn(1, y))

        # # TODO check and maybe change back
        # v_c2 = GRAV_CONST * exponential_cumulative_mass(r_hs, M_disk, r_disk) / r_hs

    # cumulative. evaluate in a range
    elif save_name_append == 'cum':

        integrand = lambda y: y * y * y * np.exp(-2 * y) * (iv(0, y) * kn(0, y) - iv(1, y) * kn(1, y))

        v_c2 = np.array([quad(integrand, r_h[2 * i] / (2 * r_disk), r_h[2 * i + 1] / (2 * r_disk))[0]
                         for i in range(len(r_h) // 2)])

        v_c2 *= 8 * GRAV_CONST * M_disk

        v_c2 /= np.array([np.exp(-r_h[2 * i] / r_disk) * (r_disk + r_h[2 * i]) -
                          np.exp(-r_h[2 * i + 1] / r_disk) * (r_disk + r_h[2 * i + 1]) for i in range(len(r_h) // 2)])

    return (np.sqrt(v_c2))


def get_exponential_henrquist_velocity(r_h, vmax, a_h, M_disk, r_disk, save_name_append='diff'):
    '''Calculates the circular velocity for an analytic thin exponential disk
  '''
    # v_c = np.zeros(len(r_h)//2)

    # differential. evaluate at a point
    if save_name_append == 'diff':
        v_c = get_analytic_hernquist_circular_velocity(r_h, vmax, a_h, save_name_append)

    # cumulative. evaluate in a range
    elif save_name_append == 'cum':
        gmh = 4 * a_h * vmax ** 2  # kpc km/s

        integrand = lambda r, a, r_d: r * r / ((r + a) * (r + a)) * np.exp(-r / r_d)

        v_c2 = np.array([quad(integrand, r_h[2 * i], r_h[2 * i + 1], args=(a_h, r_disk))[0]
                         for i in range(len(r_h) // 2)])

        v_c2 *= gmh / r_disk

        v_c2 /= np.array([np.exp(-r_h[2 * i] / r_disk) * (r_disk + r_h[2 * i]) -
                          np.exp(-r_h[2 * i + 1] / r_disk) * (r_disk + r_h[2 * i + 1]) for i in range(len(r_h) // 2)])

        v_c = np.sqrt(v_c2)

    return (v_c)


def get_analytic_hernquist_escape_velocity(r_h, vmax, a_h, save_name_append='diff'):
    '''
    '''
    gmh = 4 * a_h * vmax ** 2  # kpc km/s

    v_esc = np.zeros(len(r_h) // 2)

    # differential. evaluate at a point
    if save_name_append == 'diff':
      # to fix if the centre is 0
        r_s = np.copy(r_h)
        if r_s[0] == 0:
            r_s[0] = 0.1

        # bin centres
        r_hs = 10 ** ((np.log10(r_s[0::2]) + np.log10(r_s[1::2])) / 2)

        for i in range(len(v_esc)):
            # Hernquist 1990 eqn. (15)
            v_esc[i] = np.sqrt(2 * gmh / (r_hs[i] + a_h))

    # cumulative. evaluate in a range
    elif save_name_append == 'cum':
        # I don't think this measurement makes sense
        raise NotImplementedError

    return (v_esc)


def get_analytic_hernquist_potential_derivative(r_measure, M, a):
    dPhi_dR = GRAV_CONST * M / (r_measure + a)**2
    return dPhi_dR


def get_analytic_spherical_exponential_potential_derivative(r_measure, M_star, r_d):
    dPhi_dR = M_star * GRAV_CONST / r_measure**2 * (1 - np.exp(-r_measure / r_d) * (1 + r_measure / r_d))
    return dPhi_dR


# Combine everything
def get_velocity_scale(bin_edges,
                       PosDMs, VelDMs, MassDM, PosStars, VelStars, MassStar,
                       v200=False, dm_dispersion=False, dm_v_circ=False,
                       analytic_dispersion=False, analytic_v_c=False,
                       save_name_append='diff', galaxy_name=''):
    '''Get a given velocity. The data is not needed for the analytic velocities.
  '''

    # TODO calculate cumulative exponential density weighted hernquist dispersion

    if v200:
        out = get_v_200(galaxy_name)

    elif dm_dispersion:
        out = get_dm_dispersion(PosDMs, VelDMs, bin_edges)

    elif dm_v_circ:
        out = get_v_circ(PosDMs, MassDM, PosStars, MassStar, bin_edges)

    elif analytic_dispersion:
        (a_h, vmax) = get_hernquist_params(galaxy_name)
        out = get_analytic_hernquist_dispersion(bin_edges, vmax, a_h, save_name_append)

    elif analytic_v_c:
        (a_h, vmax) = get_hernquist_params(galaxy_name)
        v_halo = get_analytic_hernquist_circular_velocity(bin_edges, vmax, a_h, save_name_append)

        (M_disk, r_disk) = get_exponential_disk_params(galaxy_name)
        v_disk = get_exponential_disk_velocity(bin_edges, M_disk, r_disk, save_name_append)

        # r_h = 10**((np.log10(bin_edges[0::2]) + np.log10(bin_edges[1::2]))/2)
        # v_disk = np.sqrt(GRAV_CONST * M_disk * (1 - np.exp(-r_h / r_disk) * (1 + r_h / r_disk)) / r_h)

        # v_disk=0

        out = np.sqrt(v_halo ** 2 + v_disk ** 2)
        # out = v_halo
        # out = v_disk

    return (out)


# Velocity measurement
# Single snapshot
def get_dispersion_profiles(PosStars, VelStars, bin_edges, cylindrical=True):
    '''Calculates dipserions in 3 dimensions, binned by radius.
    Assumes mean(v_z) = 0 and mean(v_R) = 0.
    Only for equal mass particles
    '''
    # cylindrical
    R, _, _, v_R, v_phi, v_z = get_cylindrical(PosStars, VelStars)

    if not cylindrical: R = np.linalg.norm(PosStars, axis=1)

    number_of_bins = len(bin_edges) // 2

    # set up arrays
    mean_v_R = np.zeros(number_of_bins, dtype=np.float64)
    mean_v_phi = np.zeros(number_of_bins, dtype=np.float64)

    sigma_z2 = np.zeros(number_of_bins, dtype=np.float64)
    sigma_R2 = np.zeros(number_of_bins, dtype=np.float64)
    sigma_phi2 = np.zeros(number_of_bins, dtype=np.float64)

    # faster than calling binned statistic a bunch
    for i in range(number_of_bins):
        mask = np.logical_and((R > bin_edges[2 * i]), (R < bin_edges[2 * i + 1]))
        bin_N_i = 1 / np.sum(mask)

        # sigmas
        mean_v_R[i] = np.sum(v_R[mask]) * bin_N_i
        mean_v_phi[i] = np.sum(v_phi[mask]) * bin_N_i

        # these are the squared values
        sigma_z2[i] = np.sum(np.square(v_z[mask])) * bin_N_i
        sigma_R2[i] = np.sum(np.square(v_R[mask])) * bin_N_i
        # sigma_z2[i] = np.sum((v_z[mask] - np.mean(v_z[mask])) ** 2) * bin_N_i
        # sigma_R2[i] = np.sum((v_R[mask] - np.mean(v_R[mask])) ** 2) * bin_N_i
        sigma_phi2[i] = np.sum((v_phi[mask] - mean_v_phi[i]) ** 2) * bin_N_i
        
        # sigma_z2[i] = np.sum((v_z[mask] - np.mean(v_z[mask])) ** 2) * bin_N_i
        # sigma_R2[i] = np.sum((VelStars[mask, 1] - np.mean(VelStars[mask, 1])) ** 2) * bin_N_i
        # sigma_phi2[i] = np.sum((VelStars[mask, 0] - np.mean(VelStars[mask, 0])) ** 2) * bin_N_i

    sigma_z = np.sqrt(sigma_z2)
    sigma_R = np.sqrt(sigma_R2)
    sigma_phi = np.sqrt(sigma_phi2)

    return (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi)


# All snapshots
def get_dispersions(galaxy_name, save_name_append, snaps, bin_edges, overwrite=False, save=True):
    '''Read or load galaxies, calculate velocity dispersions and then save them.
    '''
    if os.path.exists('../galaxies/' + galaxy_name + '_seed3'): 
        max_seed = 4
        if os.path.exists('../galaxies/' + galaxy_name + '_seed4'): max_seed = 5
        if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): 
            max_seed = 10
            
        mean_v_R_list = []
        mean_v_phi_list = []
        sigma_z_list = []
        sigma_R_list = []
        sigma_phi_list = []

        for i in range(max_seed):
            galaxy_name_i = galaxy_name + '_seed' + str(i)
            (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
             ) = get_dispersions(galaxy_name_i, save_name_append, snaps, bin_edges, overwrite)

            mean_v_R_list.append(mean_v_R)
            mean_v_phi_list.append(mean_v_phi)
            sigma_z_list.append(sigma_z)
            sigma_R_list.append(sigma_R)
            sigma_phi_list.append(sigma_phi)

        mean_v_R_list = np.array(mean_v_R_list)
        mean_v_phi_list = np.array(mean_v_phi_list)
        sigma_z_list = np.array(sigma_z_list)
        sigma_R_list = np.array(sigma_R_list)
        sigma_phi_list = np.array(sigma_phi_list)

        mean_v_R = np.median(mean_v_R_list, axis=0)
        mean_v_phi = np.median(mean_v_phi_list, axis=0)
        sigma_z = np.median(sigma_z_list, axis=0)
        sigma_R = np.median(sigma_R_list, axis=0)
        sigma_phi = np.median(sigma_phi_list, axis=0)

        return (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi)

    if galaxy_name[-6:] == '_seed0':
        galaxy_name = galaxy_name[:-6]
    file_name = '../galaxies/' + galaxy_name + '/dispersion_profile_' + save_name_append
    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        (mean_v_R, mean_v_phi,
         sigma_z, sigma_R, sigma_phi
         ) = np.load(file_name + '.npy')

    else:
        print('Calculating Dispersions')
        print(galaxy_name)
        print(bin_edges)

        # set up arrays
        n_dim = len(bin_edges) // 2
        mean_v_R = np.zeros((snaps + 1, n_dim), dtype=np.float64)
        mean_v_phi = np.zeros((snaps + 1, n_dim), dtype=np.float64)
        sigma_z = np.zeros((snaps + 1, n_dim), dtype=np.float64)
        sigma_R = np.zeros((snaps + 1, n_dim), dtype=np.float64)
        sigma_phi = np.zeros((snaps + 1, n_dim), dtype=np.float64)

        # loop through all snaps
        for i in range(snaps + 1):
            snap = '{0:03d}'.format(int(i))
            # read a file
            (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_nbody.load_and_align(galaxy_name, snap)

            # calculate
            (mean_v_R[i, :], mean_v_phi[i, :],
             sigma_z[i, :], sigma_R[i, :], sigma_phi[i, :]
             ) = get_dispersion_profiles(PosStars, VelStars, bin_edges)

        # save when done with loop
        if save:
            np.save(file_name, (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi))

    return (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi)


def get_d_sigma_R2_dR(galaxy_name, save_name_append, snaps, bin_edges, overwrite=False, save=True):
    '''Read or load galaxies, calculate velocity dispersions and then save them.
    '''
    if os.path.exists('../galaxies/' + galaxy_name + '_seed3'):
        max_seed = 4
        if os.path.exists('../galaxies/' + galaxy_name + '_seed4'): max_seed = 5
        if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

        d_sigma_R2_dR_list = []

        for i in range(max_seed):
            galaxy_name_i = galaxy_name + '_seed' + str(i)
            d_sigma_R2_dR = get_d_sigma_R2_dR(galaxy_name_i, save_name_append, snaps, bin_edges, overwrite)

            d_sigma_R2_dR_list.append(d_sigma_R2_dR)

        d_sigma_R2_dR_list = np.array(d_sigma_R2_dR_list)

        d_sigma_R2_dR = np.median(d_sigma_R2_dR_list, axis=0)

        return d_sigma_R2_dR

    if galaxy_name[-6:] == '_seed0':
        galaxy_name = galaxy_name[:-6]
    file_name = '../galaxies/' + galaxy_name + '/d_sigma_R2_dR_' + save_name_append
    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        (d_sigma_R2_dR
         ) = np.load(file_name + '.npy')

    else:
        print('Calculating Dispersions')
        print(galaxy_name)
        print(bin_edges)

        # set up arrays
        n_dim = len(bin_edges) // 2
        d_sigma_R2_dR = np.zeros((snaps + 1, n_dim), dtype=np.float64)

        if save_name_append == 'cum':
            raise NotImplementedError

        dex = np.log10(bin_edges[1] / bin_edges[0])
        if not np.isclose(dex, np.log10(bin_edges[3] / bin_edges[2])):
            raise ValueError('Bin widths inconsistant.')
        
        sigma_bin_edges = get_dex_bins(bin_edges, dex)

        # loop through all snaps
        for i in range(snaps + 1):
            snap = '{0:03d}'.format(int(i))
            # read a file
            (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_nbody.load_and_align(galaxy_name, snap)

            # calculate
            (_, _, _, sigma_R, _
             ) = get_dispersion_profiles(PosStars, VelStars, sigma_bin_edges)

            d_sigma_R2_dR[i, :] = np.diff(sigma_R**2)[::2] / np.diff(bin_edges)[::2]

        # save when done with loop
        if save:
            np.save(file_name, d_sigma_R2_dR)

    return d_sigma_R2_dR


def get_d_ln_Sigma_d_ln_R(galaxy_name, save_name_append, snaps, bin_edges, overwrite=False, save=True):
    '''Read or load galaxies, calculate velocity dispersions and then save them.
    '''
    if os.path.exists('../galaxies/' + galaxy_name + '_seed3'):
        max_seed = 4
        if os.path.exists('../galaxies/' + galaxy_name + '_seed4'): max_seed = 5
        if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

        d_ln_Sigma_d_ln_R_list = []

        for i in range(max_seed):
            galaxy_name_i = galaxy_name + '_seed' + str(i)
            d_ln_Sigma_d_ln_R = get_d_ln_Sigma_d_ln_R(galaxy_name_i, save_name_append, snaps, bin_edges, overwrite)

            d_ln_Sigma_d_ln_R_list.append(d_ln_Sigma_d_ln_R)

        d_ln_Sigma_d_ln_R_list = np.array(d_ln_Sigma_d_ln_R_list)

        d_ln_Sigma_d_ln_R = np.median(d_ln_Sigma_d_ln_R_list, axis=0)

        return d_ln_Sigma_d_ln_R

    if galaxy_name[-6:] == '_seed0':
        galaxy_name = galaxy_name[:-6]
    file_name = '../galaxies/' + galaxy_name + '/d_ln_Sigma_d_ln_R_' + save_name_append
    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        (d_ln_Sigma_d_ln_R
         ) = np.load(file_name + '.npy')

    else:
        print('Calculating surface density')
        print(galaxy_name)
        print(bin_edges)

        # set up arrays
        n_dim = len(bin_edges) // 2
        d_ln_Sigma_d_ln_R = np.zeros((snaps + 1, n_dim), dtype=np.float64)

        if save_name_append == 'cum':
            raise NotImplementedError

        dex = np.log10(bin_edges[1] / bin_edges[0])
        if not np.isclose(dex, np.log10(bin_edges[3] / bin_edges[2])):
            raise ValueError('Bin widths inconsistant.')

        sigma_bin_edges = get_dex_bins(bin_edges, dex)

        # loop through all snaps
        for i in range(snaps + 1):
            snap = '{0:03d}'.format(int(i))
            # read a file
            (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_nbody.load_and_align(galaxy_name, snap)

            # calculate
            R = np.sqrt(PosStars[:, 0]**2 + PosStars[:, 1]**2)

            # N, _ = np.histogram(R, sigma_bin_edges)
            N = np.zeros(len(bin_edges))
            for j in range(len(bin_edges)):
                N[j] = np.sum(np.logical_and(sigma_bin_edges[2*j] < R, R < sigma_bin_edges[2*j+1]))

            Sigma = N * MassStar / (np.pi * (sigma_bin_edges[1::2] ** 2 - sigma_bin_edges[:-1:2] ** 2))

            d_ln_Sigma_d_ln_R[i, :] = np.diff(np.log(Sigma))[::2] / np.diff(np.log(bin_edges))[::2]
            
            # print(d_ln_Sigma_d_ln_R[i])
            # print(- (bin_edges[1::2] + bin_edges[::2])/2 / 4.15)
            # print()

        # save when done with loop
        if save:
            np.save(file_name, d_ln_Sigma_d_ln_R)

    return d_ln_Sigma_d_ln_R


def get_all_vc2s(galaxy_name, save_name_append, snaps, bin_edges, overwrite=False, save=True):
    '''Read or load galaxies, calculate velocity dispersions and then save them.
    '''
    if os.path.exists('../galaxies/' + galaxy_name + '_seed3'):
        max_seed = 4
        if os.path.exists('../galaxies/' + galaxy_name + '_seed4'): max_seed = 5
        if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

        z_integrated_vc2_list = []
        midplane_vc2_list = []
        mass_vc2_list = []

        for i in range(max_seed):
            galaxy_name_i = galaxy_name + '_seed' + str(i)
            (z_integrated_vc2, midplane_vc2, mass_vc2
             ) = get_all_vc2s(galaxy_name_i, save_name_append, snaps, bin_edges, overwrite)

            z_integrated_vc2_list.append(z_integrated_vc2)
            midplane_vc2_list.append(midplane_vc2)
            mass_vc2_list.append(mass_vc2)

        z_integrated_vc2_list = np.array(z_integrated_vc2_list)
        midplane_vc2_list = np.array(midplane_vc2_list)
        mass_vc2_list = np.array(mass_vc2_list)

        z_integrated_vc2 = np.median(z_integrated_vc2_list, axis=0)
        midplane_vc2 = np.median(midplane_vc2_list, axis=0)
        mass_vc2 = np.median(mass_vc2_list, axis=0)

        return z_integrated_vc2, midplane_vc2, mass_vc2

    if galaxy_name[-6:] == '_seed0':
        galaxy_name = galaxy_name[:-6]
    file_name = '../galaxies/' + galaxy_name + '/all_vc2s_' + save_name_append
    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        (z_integrated_vc2, midplane_vc2, mass_vc2
         ) = np.load(file_name + '.npy')

    else:
        print('Calculating circular velocities')
        print(galaxy_name)
        print(bin_edges)

        # set up arrays
        n_dim = len(bin_edges) // 2

        z_integrated_vc2 = np.zeros((snaps + 1, n_dim), dtype=np.float64)
        midplane_vc2 = np.zeros((snaps + 1, n_dim), dtype=np.float64)
        mass_vc2 = np.zeros((snaps + 1, n_dim), dtype=np.float64)

        bin_centres = 10 ** ((np.log10(bin_edges[:-1:2]) + np.log10(bin_edges[1::2])) / 2)

        # loop through all snaps
        for i in range(snaps + 1):
            snap = '{0:03d}'.format(int(i))
            # read a file
            (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_nbody.load_and_align(galaxy_name, snap)

            Rs = np.sqrt(PosStars[:, 0]**2 + PosStars[:, 1]**2)
            rs = np.linalg.norm(PosStars, axis=1)
            rd = np.linalg.norm(PosDMs, axis=1)

            # calculate
            xd = PosDMs[:, 0]
            yd = PosDMs[:, 1]
            zd = PosDMs[:, 2]
            nd = len(xd)
            md = MassDM * np.ones(nd)

            xs = PosStars[:, 0]
            ys = PosStars[:, 1]
            zs = PosStars[:, 2]
            ns = len(xs)
            ms = MassStar * np.ones(nd)

            ndirections = 8
            angles = np.linspace(0, 2 * np.pi, ndirections + 1)[:-1]

            xin = bin_centres * np.cos(angles)[:, np.newaxis]
            yin = bin_centres * np.sin(angles)[:, np.newaxis]
            xin = xin.flatten()
            yin = yin.flatten()
            zin = np.zeros(len(xin))
            ni = n_dim * ndirections

            eps = 0.2

            star_vcirc2 = twobody.star_vcirc2(md,xd,yd,zd,ms,xs,ys,zs,eps,nd,ns)
            binned_vcirc2,_,_ = binned_statistic(Rs, star_vcirc2, bins=bin_edges, statistic='mean')
            binned_vcirc2 = binned_vcirc2[::2]

            midplane_vcirc2 = twobody.midplane_vcirc2(md,xd,yd,zd,ms,xs,ys,zs,xin,yin,zin,eps,nd,ns,ni)
            midplane_vcirc2 = np.median(np.reshape(midplane_vcirc2, (ndirections, n_dim)), axis=0)

            Ns = np.array([np.sum(rs < bin_centre) for bin_centre in bin_centres])
            Nd = np.array([np.sum(rd < bin_centre) for bin_centre in bin_centres])
            mass_vcirc2 = (MassStar * Ns + MassDM * Nd) / bin_centres

            z_integrated_vc2[i, :] = binned_vcirc2
            midplane_vc2[i, :] = midplane_vcirc2
            mass_vc2[i, :] = mass_vcirc2

            # print(binned_vcirc2)
            # print(midplane_vcirc2)
            # print(mass_vcirc2)
            # print()

        z_integrated_vc2 *= GRAV_CONST
        midplane_vc2 *= GRAV_CONST
        mass_vc2 *= GRAV_CONST

        # save when done with loop
        if save:
            np.save(file_name, (z_integrated_vc2, midplane_vc2, mass_vc2))

    return z_integrated_vc2, midplane_vc2, mass_vc2


def get_velocity_ellipsoid_terms(galaxy_name, save_name_append, snaps, bin_edges, overwrite=False, save=True):
    '''Read or load galaxies, calculate velocity dispersions and then save them.
    '''
    if os.path.exists('../galaxies/' + galaxy_name + '_seed3'):
        max_seed = 4
        if os.path.exists('../galaxies/' + galaxy_name + '_seed4'): max_seed = 5
        if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

        term1_list = []
        term2_list = []

        for i in range(max_seed):
            galaxy_name_i = galaxy_name + '_seed' + str(i)
            (term1, term2
             ) = get_velocity_ellipsoid_terms(galaxy_name_i, save_name_append, snaps, bin_edges, overwrite)

            term1_list.append(term1)
            term2_list.append(term2)

        term1_list = np.array(term1_list)
        term2_list = np.array(term2_list)

        term1 = np.median(term1_list, axis=0)
        term2 = np.median(term2_list, axis=0)

        return term1, term2

    if galaxy_name[-6:] == '_seed0':
        galaxy_name = galaxy_name[:-6]
    file_name = '../galaxies/' + galaxy_name + '/velocity_ellipsoid_terms_' + save_name_append
    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        (term1, term2
         ) = np.load(file_name + '.npy')

    else:
        print('Calculating velocity ellipsoid terms')
        print(galaxy_name)
        print(bin_edges)

        # set up arrays
        n_dim = len(bin_edges) // 2

        term1 = np.zeros((snaps + 1, n_dim), dtype=np.float64)
        term2 = np.zeros((snaps + 1, n_dim), dtype=np.float64)

        bin_centres = 10 ** ((np.log10(bin_edges[:-1:2]) + np.log10(bin_edges[1::2])) / 2)

        n_z_bins = 6

        # loop through all snaps
        for i in range(snaps + 1):
            snap = '{0:03d}'.format(int(i))
            # read a file
            (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_nbody.load_and_align(galaxy_name, snap)

            Rs = np.sqrt(PosStars[:, 0]**2 + PosStars[:, 1]**2)

            #get stars in each radial bin
            for j in range(n_dim):
                R_mask = np.logical_and(bin_edges[2*j] < Rs, Rs < bin_edges[2*j+1])

                (R, phi, z, v_R, v_phi, v_z) = get_cylindrical(PosStars[R_mask], VelStars[R_mask])
                    
                #work out reasonable z bins and edges
                median_z = np.median(np.abs(z))

                #to calc vRvz
                z_bin_edges = np.linspace(-2*median_z, 2*median_z, n_z_bins + 1)
                #to calc N
                z_bin_centres = np.linspace(-30, 30, n_z_bins + 2)
                z_bin_centres[1:-1] = 0.5 * (z_bin_edges[:-1] + z_bin_edges[1:])

                z_bin_edges[0] = -30
                z_bin_edges[-1] = 30

                #to calc vRvz
                mean_vRvzs, _, _ = binned_statistic(z, v_R * v_z, bins=z_bin_edges, statistic='mean')

                #to calc N
                N_z, _ = np.histogram(z, bins=z_bin_centres)

                z_bin_edges = np.linspace(-2*median_z, 2*median_z, n_z_bins + 1)

                term1[i, j] = np.sum(N_z[1:-1] * bin_centres[j] * np.diff(mean_vRvzs) / np.diff(z_bin_centres[1:-1])
                                     ) / np.sum(N_z[1:-1])

                
                term2[i, j] = np.sum(mean_vRvzs * bin_centres[j] * np.diff(N_z) / np.diff(z_bin_edges)
                                     ) / np.sum(N_z)
        
        # save when done with loop
        if save:
            np.save(file_name, (term1, term2))

    return term1, term2


def get_one_d_sigma_R2_dR(galaxy_name, save_name_append, snap, bin_edges):

    if galaxy_name[-6:] == '_seed0':
        galaxy_name = galaxy_name[:-6]

    if save_name_append == 'cum':
        raise NotImplementedError

    dex = np.log10(bin_edges[1] / bin_edges[0])
    if not np.isclose(dex, np.log10(bin_edges[3] / bin_edges[2])):
        raise ValueError('Bin widths inconsistant.')

    sigma_bin_edges = get_dex_bins(bin_edges, dex)

    # read a file
    (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
     MassStar, PosStars, VelStars, IDStars, PotStars
     ) = load_nbody.load_and_align(galaxy_name, '{0:03d}'.format(int(snap)))

    # calculate
    (_, _, _, sigma_R, _
     ) = get_dispersion_profiles(PosStars, VelStars, sigma_bin_edges)

    d_sigma_R2_dR = np.diff(sigma_R ** 2)[::2] / np.diff(bin_edges)[::2]

    return d_sigma_R2_dR


def get_one_d_ln_Sigma_d_ln_R(galaxy_name, save_name_append, snap, bin_edges):

    if galaxy_name[-6:] == '_seed0':
        galaxy_name = galaxy_name[:-6]
    file_name = '../galaxies/' + galaxy_name + '/d_ln_Sigma_d_ln_R_' + save_name_append

    if save_name_append == 'cum':
        raise NotImplementedError

    dex = np.log10(bin_edges[1] / bin_edges[0])
    if not np.isclose(dex, np.log10(bin_edges[3] / bin_edges[2])):
        raise ValueError('Bin widths inconsistant.')

    sigma_bin_edges = get_dex_bins(bin_edges, dex)

    # read a file
    (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
     MassStar, PosStars, VelStars, IDStars, PotStars
     ) = load_nbody.load_and_align(galaxy_name, snap = '{0:03d}'.format(int(snap)))

    # calculate
    R = np.sqrt(PosStars[:, 0] ** 2 + PosStars[:, 1] ** 2)

    # N, _ = np.histogram(R, sigma_bin_edges)
    N = np.zeros(len(bin_edges))
    for j in range(len(bin_edges)):
        N[j] = np.sum(np.logical_and(sigma_bin_edges[2 * j] < R, R < sigma_bin_edges[2 * j + 1]))

    Sigma = N * MassStar / (np.pi * (sigma_bin_edges[1::2] ** 2 - sigma_bin_edges[:-1:2] ** 2))

    d_ln_Sigma_d_ln_R = np.diff(np.log(Sigma))[::2] / np.diff(np.log(bin_edges))[::2]

    # print(d_ln_Sigma_d_ln_R[i])
    # print(- (bin_edges[1::2] + bin_edges[::2])/2 / 4.15)
    # print()

    return d_ln_Sigma_d_ln_R


def get_one_vc2(galaxy_name, save_name_append, snap, bin_edges):

    bin_centres = 10 ** ((np.log10(bin_edges[:-1:2]) + np.log10(bin_edges[1::2])) / 2)

    (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
     MassStar, PosStars, VelStars, IDStars, PotStars
     ) = load_nbody.load_and_align(galaxy_name, '{0:03d}'.format(int(snap)))

    Rs = np.sqrt(PosStars[:, 0] ** 2 + PosStars[:, 1] ** 2)
    rs = np.linalg.norm(PosStars, axis=1)
    rd = np.linalg.norm(PosDMs, axis=1)

    # calculate
    xd = PosDMs[:, 0]
    yd = PosDMs[:, 1]
    zd = PosDMs[:, 2]
    nd = len(xd)
    md = MassDM * np.ones(nd)

    xs = PosStars[:, 0]
    ys = PosStars[:, 1]
    zs = PosStars[:, 2]
    ns = len(xs)
    ms = MassStar * np.ones(nd)

    ndirections = 8
    angles = np.linspace(0, 2 * np.pi, ndirections + 1)[:-1]

    xin = bin_centres * np.cos(angles)[:, np.newaxis]
    yin = bin_centres * np.sin(angles)[:, np.newaxis]
    xin = xin.flatten()
    yin = yin.flatten()
    zin = np.zeros(len(xin))
    ni = len(xin)

    eps = 0.2

    comb = np.zeros(len(bin_edges))
    comb[::2] += 1e-10

    star_vcirc2 = twobody.star_vcirc2(md, xd, yd, zd, ms, xs, ys, zs, eps, nd, ns)
    binned_vc2, _, _ = binned_statistic(Rs, star_vcirc2, bins=bin_edges + comb, statistic=np.nanmean)
    binned_vc2 = binned_vc2[::2]

    midplane_vc2 = twobody.midplane_vcirc2(md, xd, yd, zd, ms, xs, ys, zs, xin, yin, zin, eps, nd, ns, ni)
    midplane_vc2 = np.median(np.reshape(midplane_vc2, (ndirections, len(bin_centres))), axis=0)

    Ns = np.array([np.sum(rs < bin_centre) for bin_centre in bin_centres])
    Nd = np.array([np.sum(rd < bin_centre) for bin_centre in bin_centres])
    mass_vc2 = (MassStar * Ns + MassDM * Nd) / bin_centres

    # print(binned_vcirc2)
    # print(midplane_vcirc2)
    # print(mass_vcirc2)
    # print()

    binned_vc2 *= GRAV_CONST
    midplane_vc2 *= GRAV_CONST
    mass_vc2 *= GRAV_CONST

    return binned_vc2, midplane_vc2, mass_vc2


def get_one_velocity_ellipsoid_term(galaxy_name, save_name_append, snap, bin_edges):
    
    # set up arrays
    n_dim = len(bin_edges) // 2

    term1 = np.zeros(n_dim, dtype=np.float64)
    term2 = np.zeros(n_dim, dtype=np.float64)

    bin_centres = 10 ** ((np.log10(bin_edges[:-1:2]) + np.log10(bin_edges[1::2])) / 2)

    n_z_bins = 6

    # read a file
    (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
     MassStar, PosStars, VelStars, IDStars, PotStars
     ) = load_nbody.load_and_align(galaxy_name, '{0:03d}'.format(int(snap)))

    Rs = np.sqrt(PosStars[:, 0] ** 2 + PosStars[:, 1] ** 2)

    # get stars in each radial bin
    for j in range(n_dim):
        R_mask = np.logical_and(bin_edges[2 * j] < Rs, Rs < bin_edges[2 * j + 1])
        if np.sum(R_mask) > 1:
            (R, phi, z, v_R, v_phi, v_z) = get_cylindrical(PosStars[R_mask], VelStars[R_mask])

            # work out reasonable z bins and edges
            median_z = np.median(np.abs(z))

            # to calc vRvz
            z_bin_edges = np.linspace(-2 * median_z, 2 * median_z, n_z_bins + 1)
            # to calc N
            z_bin_centres = np.linspace(-30, 30, n_z_bins + 2)
            z_bin_centres[1:-1] = 0.5 * (z_bin_edges[:-1] + z_bin_edges[1:])

            z_bin_edges[0] = -30
            z_bin_edges[-1] = 30

            # to calc vRvz
            mean_vRvzs, _, _ = binned_statistic(z, v_R * v_z, bins=z_bin_edges, statistic=np.nanmean)

            # to calc N
            N_z, _ = np.histogram(z, bins=z_bin_centres)

            z_bin_edges = np.linspace(-2 * median_z, 2 * median_z, n_z_bins + 1)

            term1[j] = np.sum(N_z[1:-1] * bin_centres[j] * np.diff(mean_vRvzs) / np.diff(z_bin_centres[1:-1])
                                 ) / np.sum(N_z[1:-1])

            term2[j] = np.sum(mean_vRvzs * bin_centres[j] * np.diff(N_z) / np.diff(z_bin_edges)
                                 ) / np.sum(N_z)

    return term1, term2

def get_thin_bin_dispersions(galaxy_name, save_name_append, snaps, bin_edges, overwrite=False):
    '''Read or load galaxies, calculate velocity dispersions and then save them.
    '''
    if os.path.exists('../galaxies/' + galaxy_name + '_seed4'):
        max_seed = 5
        if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

        mean_v_R_list = []
        mean_v_phi_list = []
        sigma_z_list = []
        sigma_R_list = []
        sigma_phi_list = []

        for i in range(max_seed):
            galaxy_name_i = galaxy_name + '_seed' + str(i)
            (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
             ) = get_dispersions(galaxy_name_i, save_name_append, snaps, bin_edges, overwrite)

            mean_v_R_list.append(mean_v_R)
            mean_v_phi_list.append(mean_v_phi)
            sigma_z_list.append(sigma_z)
            sigma_R_list.append(sigma_R)
            sigma_phi_list.append(sigma_phi)

        mean_v_R_list = np.array(mean_v_R_list)
        mean_v_phi_list = np.array(mean_v_phi_list)
        sigma_z_list = np.array(sigma_z_list)
        sigma_R_list = np.array(sigma_R_list)
        sigma_phi_list = np.array(sigma_phi_list)

        mean_v_R = np.median(mean_v_R_list, axis=0)
        mean_v_phi = np.median(mean_v_phi_list, axis=0)
        sigma_z = np.median(sigma_z_list, axis=0)
        sigma_R = np.median(sigma_R_list, axis=0)
        sigma_phi = np.median(sigma_phi_list, axis=0)

        return (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi)

    if galaxy_name[-6:] == '_seed0':
        galaxy_name = galaxy_name[:-6]
    file_name = '../galaxies/' + galaxy_name + '/dispersion_profile_thin_bin' + save_name_append
    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        (mean_v_R, mean_v_phi,
         sigma_z, sigma_R, sigma_phi
         ) = np.load(file_name + '.npy')

    else:
        print('Calculating Dispersions')
        print(galaxy_name)
        print(bin_edges)

        # set up arrays
        n_dim = len(bin_edges) // 2
        mean_v_R = np.zeros((snaps + 1, n_dim), dtype=np.float64)
        mean_v_phi = np.zeros((snaps + 1, n_dim), dtype=np.float64)
        sigma_z = np.zeros((snaps + 1, n_dim), dtype=np.float64)
        sigma_R = np.zeros((snaps + 1, n_dim), dtype=np.float64)
        sigma_phi = np.zeros((snaps + 1, n_dim), dtype=np.float64)

        # loop through all snaps
        for i in range(snaps + 1):
            snap = '{0:03d}'.format(int(i))
            # read a file
            (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_nbody.load_and_align(galaxy_name, snap)

            # calculate
            (mean_v_R[i, :], mean_v_phi[i, :],
             sigma_z[i, :], sigma_R[i, :], sigma_phi[i, :]
             ) = get_dispersion_profiles(PosStars, VelStars, bin_edges)

        # save when done with loop
        np.save(file_name, (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi))

    return (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi)


def smooth_burn_ins(MassDM_res, snaps, save_name_append, burn_in, bin_edges, galaxy_name=''):
    '''Burn in to get initial velocities.
    Smooths over saved highest resolution measurements so only works if bin_edges match
    '''
    # TODO might need to mess around with these some more
    window = 41
    poly = 5

    high_snaps = 101

    rez_0 = 'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps'
    rez_1 = 'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps'
    rez_2 = 'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps'
    high_mdm = 0.0001

    if 'c_7' in galaxy_name:
        rez_0 = 'mu_5_c_7/fdisk_0p01_lgMdm_7p0_V200-200kmps'
        rez_1 = ''
        rez_2 = 'mu_5_c_7/fdisk_0p01_lgMdm_8p0_V200-200kmps'
        high_mdm = 0.001
    if 'c_15' in galaxy_name:
        rez_0 = 'mu_5_c_15/fdisk_0p01_lgMdm_7p0_V200-200kmps'
        rez_1 = ''
        rez_2 = 'mu_5_c_15/fdisk_0p01_lgMdm_8p0_V200-200kmps'
        high_mdm = 0.001
    if '50kmps' in galaxy_name:
        rez_0 = 'mu_5/fdisk_0p01_lgMdm_4p0_V200-50kmps'
        rez_1 = 'mu_5/fdisk_0p01_lgMdm_4p5_V200-50kmps'
        rez_2 = 'mu_5/fdisk_0p01_lgMdm_5p0_V200-50kmps'
        high_mdm = 0.000001
    elif '100kmps' in galaxy_name:
        rez_0 = 'mu_5/fdisk_0p01_lgMdm_5p0_V200-100kmps'
        rez_1 = 'mu_5/fdisk_0p01_lgMdm_5p5_V200-100kmps'
        rez_2 = 'mu_5/fdisk_0p01_lgMdm_6p0_V200-100kmps'
        high_mdm = 0.00001
    elif '400kmps' in galaxy_name:
        rez_0 = 'mu_5/fdisk_0p01_lgMdm_7p0_V200-400kmps'
        rez_1 = 'mu_5/fdisk_0p01_lgMdm_7p5_V200-400kmps'
        rez_2 = 'mu_5/fdisk_0p01_lgMdm_8p0_V200-400kmps'
        high_mdm = 0.001

    # if '_c_' in galaxy_name:
    #     rez_0 = galaxy_name
    #     if '8p0' in galaxy_name:
    #         high_mdm = 0.01
    #         high_snaps = 400
    #     elif '7p0' in galaxy_name:
    #         high_mdm = 0.001

    if 'fixed_eps' in galaxy_name:
        rez_0 = galaxy_name
        if '8p0' in galaxy_name:
            high_mdm = 0.01
            high_snaps = 400
        elif '7p0' in galaxy_name:
            high_mdm = 0.001

    # load high rez galaxy at an early snapshot
    snap = high_snaps * burn_in / snaps * MassDM_res / high_mdm

    if snap < high_snaps:
        # load data
        (_, mean_v_phi, sigma_z, sigma_R, sigma_phi
         ) = get_dispersions(rez_0, save_name_append,
                             high_snaps, bin_edges)
    else:
        snap = high_snaps * burn_in / snaps * MassDM_res / high_mdm / np.sqrt(10)
        if snap < high_snaps and rez_1 != '':
            (_, mean_v_phi, sigma_z, sigma_R, sigma_phi
             ) = get_dispersions(rez_1, save_name_append,
                                 high_snaps, bin_edges)

        else:
            snap = high_snaps * burn_in / snaps * MassDM_res / high_mdm / 10
            # if snap < 101:
            (_, mean_v_phi, sigma_z, sigma_R, sigma_phi
             ) = get_dispersions(rez_2, save_name_append,
                                 high_snaps, bin_edges)

            # else:
            #   snap = 400 * burn_in / snaps * MassDM_res / 0.00010003034 / 10
            #   print('too much burn in')
            #   (_, mean_v_phi, sigma_z, sigma_R, sigma_phi
            #    ) = get_dispersions('mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps', save_name_append,
            #                        400, bin_edges)
            #   window = 161
            #
            #   print('used 4th choice')

    burn_max_v = np.zeros(len(bin_edges) // 2)
    burn_min_z = np.zeros(len(bin_edges) // 2)
    burn_min_R = np.zeros(len(bin_edges) // 2)
    burn_min_phi = np.zeros(len(bin_edges) // 2)

    for i in range(len(bin_edges) // 2):
        mean_v_phi[:, i] = savgol_filter(mean_v_phi[:, i], window, poly)
        sigma_z[:, i] = savgol_filter(sigma_z[:, i], window, poly)
        sigma_R[:, i] = savgol_filter(sigma_R[:, i], window, poly)
        sigma_phi[:, i] = savgol_filter(sigma_phi[:, i], window, poly)

        spline_v = InterpolatedUnivariateSpline(np.arange(len(mean_v_phi)), mean_v_phi[:, i])
        spline_z = InterpolatedUnivariateSpline(np.arange(len(sigma_z)), sigma_z[:, i])
        spline_R = InterpolatedUnivariateSpline(np.arange(len(sigma_R)), sigma_R[:, i])
        spline_phi = InterpolatedUnivariateSpline(np.arange(len(sigma_phi)), sigma_phi[:, i])

        burn_max_v[i] = spline_v(snap)
        burn_min_z[i] = spline_z(snap)
        burn_min_R[i] = spline_R(snap)
        burn_min_phi[i] = spline_phi(snap)

    return (burn_max_v, burn_min_z, burn_min_R, burn_min_phi)


# @lru_cache(maxsize=100000)
def smooth_zero_snap(MassDM_res, snaps, save_name_append, bin_edges, galaxy_name=''):
    '''About the same as burn in but bin_edges are not the nomral ones.
    '''
    if galaxy_name == '' or ('mu_5/fdisk_0p01_lgMdm_' in galaxy_name and 'V200-200kmps' in galaxy_name):
        # read file
        (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
         MassStar, PosStars, VelStars, IDStars, PotStars
         ) = load_nbody.load_and_align('mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps', '000')
    
    else:
        (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
         MassStar, PosStars, VelStars, IDStars, PotStars
         ) = load_nbody.load_and_align(galaxy_name, '000')

    # calculate
    (_, burn_max_v, burn_min_z, burn_min_R, burn_min_phi
     ) = get_dispersion_profiles(PosStars, VelStars, bin_edges)
    
    burn_min_z[np.isnan(burn_min_z)] = 0
    burn_min_R[np.isnan(burn_min_R)] = 0
    burn_min_phi[np.isnan(burn_min_phi)] = 0

    return (burn_max_v, burn_min_z, burn_min_R, burn_min_phi)


# @lru_cache(maxsize=100000)
def smooth_z_zero_snap_interpolator():
    '''Bin edges must be a list/tuple/array. Only fist 2 edges are evaluated.
  '''
    # read file
    (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
     MassStar, PosStars, VelStars, IDStars, PotStars
     ) = load_nbody.load_and_align('mu_25/fdisk_0p01_lgMdm_6p0_V200-200kmps', '000')

    # guess and check. these numbers work ok.
    nRs = 51
    poly = 5
    R_for_interp = np.logspace(-1, 1.5, nRs)
    bin_edges = get_dex_bins(R_for_interp, 0.1)

    R = np.sqrt(PosStars[:, 0] ** 2 + PosStars[:, 1] ** 2)

    sigma_z2 = np.zeros(len(bin_edges) // 2)

    for i in range(len(bin_edges) // 2):
        mask = np.logical_and((R > bin_edges[2 * i]), (R < bin_edges[2 * i + 1]))
        bin_N_i = 1 / np.sum(mask)

        sigma_z2[i] = np.sum(np.square(VelStars[mask, 2])) * bin_N_i

    inital_velocity2_z = savgol_filter(sigma_z2, nRs // 2, poly)

    interp_inital_velocity2_z = InterpolatedUnivariateSpline(R_for_interp, inital_velocity2_z, ext=3)

    return (interp_inital_velocity2_z)


def smooth_R_zero_snap_interpolator():
    '''Bin edges must be a list/tuple/array. Only fist 2 edges are evaluated.
  '''
    # read file
    (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
     MassStar, PosStars, VelStars, IDStars, PotStars
     ) = load_nbody.load_and_align('mu_25/fdisk_0p01_lgMdm_6p0_V200-200kmps', '000')

    # guess and check. these numbers work ok.
    nRs = 51
    poly = 5
    R_for_interp = np.logspace(-1, 1.5, nRs)
    bin_edges = get_dex_bins(R_for_interp, 0.1)

    R = np.sqrt(PosStars[:, 0] ** 2 + PosStars[:, 1] ** 2)

    phi = np.arctan2(PosStars[:, 1], PosStars[:, 0])
    v_R = VelStars[:, 0] * np.cos(phi) + VelStars[:, 1] * np.sin(phi)

    sigma_R2 = np.zeros(len(bin_edges) // 2)

    for i in range(len(bin_edges) // 2):
        mask = np.logical_and((R > bin_edges[2 * i]), (R < bin_edges[2 * i + 1]))
        bin_N_i = 1 / np.sum(mask)

        sigma_R2[i] = np.sum(np.square(v_R[mask])) * bin_N_i

    inital_velocity2_R = savgol_filter(sigma_R2, nRs // 2, poly)

    interp_inital_velocity2_R = InterpolatedUnivariateSpline(R_for_interp, inital_velocity2_R, ext=3)

    return (interp_inital_velocity2_R)


###############################################################################
# Density
# Measured
def get_dm_density(PosDMs, MassDM, bin_edges, cylindrical=False):
    '''Calculates DM density within bins.
    '''
    if cylindrical:
        print('Warning: Incorrect volume for cylindrical denisty')
        R = np.sqrt(PosDMs[:, 0] ** 2 + PosDMs[:, 1] ** 2)
    else:
        R = np.linalg.norm(PosDMs, axis=1)

    # mass_binned   = binned_statistic(R, MassDM * np.ones(len(R)), bins=bin_edges,
    #                                  statistic='sum')[0] #10^10Msun
    # volume_binned = 4/3 * np.pi * (bin_edges[1:]**3 - bin_edges[:-1]**3) #kcp^3

    rho = np.zeros(len(bin_edges) // 2)

    for i in range(len(rho)):
        mask = np.logical_and((R > bin_edges[2 * i]), (R < bin_edges[2 * i + 1]))

        mass_binned = MassDM * np.sum(mask)  # 10^10Msun
        volume_binned = 4 / 3 * np.pi * (bin_edges[2 * i + 1] ** 3 - bin_edges[2 * i] ** 3)  # kcp^3

        rho[i] = mass_binned / volume_binned  # 10^10Msun/kpc^3

    return (rho)


# Analytic
def get_analytic_hernquist_density(r_h, vmax, a_h, save_name_append='diff'):
    '''
    '''
    rho = np.zeros(len(r_h) // 2)

    m_h = 4 * a_h * vmax ** 2 / GRAV_CONST

    if save_name_append == 'diff':
        # to fix if the centre is 0
        r_s = np.copy(r_h)
        if r_s[0] == 0:
            r_s[0] = 0.1

        # bin centres
        r_hs = 10 ** ((np.log10(r_s[0::2]) + np.log10(r_s[1::2])) / 2)

        for i in range(len(r_hs)):
            # binney and tremaine eq 2.64 etc.
            rho[i] = m_h / (2 * np.pi) * a_h / ((r_hs[i]) * (r_hs[i] + a_h) ** 3)

    elif save_name_append == 'cum':

        for i in range(len(rho)):
            rho[i] = (m_h * ((r_h[2 * i + 1] / (r_h[2 * i + 1] + a_h)) ** 2 - (r_h[2 * i] / (r_h[2 * i] + a_h)) ** 2)
                      / (4 / 3 * np.pi * (r_h[2 * i + 1] ** 3 - r_h[2 * i] ** 3)))

    return (rho)


# Combine everything
def get_density_scale(bin_edges, PosDMs, MassDM, PosStars, MassStar,
                      live_dm=False, analytic_dm=False,
                      save_name_append='diff', galaxy_name=''):
    '''Calculate the chosen velocity scale.
  '''
    if live_dm:
        out = get_dm_density(PosDMs, MassDM, bin_edges)

    elif analytic_dm:
        (a_h, vmax) = get_hernquist_params(galaxy_name)
        out = get_analytic_hernquist_density(bin_edges, vmax, a_h, save_name_append)

    return (out)


def get_j_c(PosStars, VelStars, PotStars, galaxy_name=''):
    # get total energy of stars
    KEStars = 0.5 * np.linalg.norm(VelStars, axis=1) ** 2

    EnergyStars = KEStars + PotStars

    # r = np.linalg.norm(PosStars, axis=1)
    # analytic_potential = hernquist_potential(r, galaxy_name=galaxy_name)
    #
    # R = np.sqrt(PosStars[:,0]**2 + PosStars[:,1]**2)
    # analytic_potential += get_exponential_disk_potential(R, galaxy_name=galaxy_name)
    #
    # EnergyStars = KEStars + analytic_potential

    # get j_c vs E to be interpolated
    r_s = np.logspace(-3, 3, 1001)

    r_centres = 10 ** ((np.log10(r_s[:-1]) + np.log10(r_s[1:])) / 2)
    r_edges = np.roll(np.repeat(r_s, 2), -1)[:-2]

    v_circ = get_velocity_scale(r_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True, save_name_append='diff',
                                galaxy_name=galaxy_name)

    pot_circ = (hernquist_potential(r_centres, galaxy_name=galaxy_name) +
                get_exponential_disk_potential(r_centres, galaxy_name=galaxy_name))
    # pot_circ = hernquist_potential(r_centres, galaxy_name=galaxy_name)

    j_circ = v_circ * r_centres
    e_circ = 0.5 * v_circ ** 2 + pot_circ

    # #make interpolator. hopefully midplane_potential takes care of everything.
    # try:
    #   interp = InterpolatedUnivariateSpline(e_circ, j_circ,
    #                                         k=1, ext='const')

    # except ValueError:
    #   print('Try reducing nbins in halo_calculations.midplane_potential.')
    #   print('The halo might be clumpy.')
    #   print('All these values should be positive.')
    #   print((E_interp - np.roll(E_interp, 1))[1:])
    #   print((E_interp))
    #   raise ValueError('x must be strictly increasing')

    interp = InterpolatedUnivariateSpline(e_circ, j_circ,
                                          k=1, ext='const')

    # the main point of these calculations
    j_c_stars = interp(EnergyStars)

    return (j_c_stars)


def get_speed_jc_interpolator(R):
    # get j_c vs E to be interpolated
    r_s = np.logspace(-3, 3, 601)

    r_centres = 10 ** ((np.log10(r_s[:-1]) + np.log10(r_s[1:])) / 2)
    r_edges = np.roll(np.repeat(r_s, 2), -1)[:-2]

    v_circ = get_velocity_scale(r_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True, save_name_append='diff')

    # (M_disk, r_disk) = get_exponential_disk_params()

    pot_circ = (hernquist_potential(r_centres) +
                get_exponential_disk_potential(r_centres))

    pot_fixed_R = (hernquist_potential(R) +
                   get_exponential_disk_potential(R))

    # pot_circ = hernquist_potential(r_centres)
    # pot_fixed_R = hernquist_potential(R)

    j_circ = v_circ * r_centres
    mod_2_e_circ = v_circ ** 2 + 2 * (pot_circ - pot_fixed_R)

    interp = InterpolatedUnivariateSpline(j_circ, mod_2_e_circ,
                                          k=1, ext='const')

    derive_interp = interp.derivative(n=1)

    return (interp, derive_interp)


def speed_from_jc(j_c, interp):
    speed_2 = interp(j_c)

    return (speed_2)


def derive_speed_from_jc(j_c, derive_interp):
    deriv = derive_interp(j_c)

    return (deriv)


###############################################################################

def get_exponential_surface_density(R, M_star, R_d, save_name_append='diff'):
    '''
    '''
    if save_name_append == 'diff':
        sigma_0 = M_star / (2 * np.pi * R_d ** 2)
        sigma_R = sigma_0 * np.exp(-R / R_d)
    else:
        raise NotImplementedError

    return (sigma_R)


def exponential_cumulative_mass(R, M_star, R_d):
    '''
    '''
    M = M_star * (1 - ((R_d + R) / R_d) * np.exp(-R / R_d))

    return (M)


def sersic_cumulative_mass(R, M_star, R_d, n):
    '''
    '''
    # # # #approx for n > 0.36 from Ciotti & Bertin
    # b_n = 2*n -1/3 +4/(405*n) +46/(25515*n**2) +131/(1148175*n**3) -2194697/(30690717750*n**4)
    # print(b_n)

    b_f = lambda b_n: gamma(2 * n) * (1 - 2 * gammainc(2 * n, b_n))
    b_n = brentq(b_f, 0, 100)

    x = b_n * (R / R_d) ** (1 / n)

    I_e = M_star * b_n ** (2 * n) / (R_d ** 2 * 2 * np.pi * n * np.exp(b_n) * gamma(2 * n))

    M = I_e * R_d ** 2 * 2 * np.pi * n * np.exp(b_n) / b_n ** (2 * n) * gammainc(2 * n, x) * gamma(2 * n)

    return (M)


def my_quantile(a, q, axis=None, interpolation='linear'):
    '''
    '''
    if len(a) == 0:
        return (np.nan)
    else:
        return (np.quantile(a, q, axis=axis, interpolation=interpolation))
    # this maybe should be np.nanquantile


###############################################################################

def get_constants(bin_edges, save_name_append, MassDM, galaxy_name=''):
    '''Get all the scaled quantities.
    '''
    rho_dm = get_density_scale(bin_edges, 0, 0, 0, 0, live_dm=False, analytic_dm=True,
                               save_name_append=save_name_append, galaxy_name=galaxy_name)

    sigma_dm = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                  save_name_append=save_name_append, galaxy_name=galaxy_name)

    v_circ = get_velocity_scale(bin_edges, 0,0,0,0,0,0, analytic_v_c=True,
                                save_name_append=save_name_append, galaxy_name=galaxy_name)

    # a_h, v_max = get_hernquist_params(galaxy_name=galaxy_name)
    # M_h = 4 * a_h * v_max ** 2 / GRAV_CONST # 10^10 M_sun
    # rho_200 = M_h / (16 * np.pi * a_h) * 1e-10

    V200 = get_v_200(galaxy_name)
    rho_200 = get_rho_200(galaxy_name)

    # t_c_dm = sigma_dm ** 3 / (GRAV_CONST ** 2 * rho_dm * MassDM) * GYR_ON_S / PC_ON_M  # Gyr
    # t_c_circ = v_circ**3   / (GRAV_CONST**2 * rho_dm * MassDM) * GYR_ON_S / PC_ON_M # Gyr

    t_c_dm = V200 ** 3 / (GRAV_CONST ** 2 * rho_dm * MassDM) * GYR_ON_S / PC_ON_M  # Gyr

    delta_dm = rho_dm / rho_200
    upsilon_dm = sigma_dm / V200
    upsilon_circ = v_circ / V200

    # return(t_c_dm , t_c_circ, delta_dm, upsilon_dm, upsilon_circ)
    return (t_c_dm, 0, delta_dm, upsilon_dm, upsilon_circ)


def get_raw_data(save_name_append='diff', burn_in=BURN_IN, concs=True):
    '''
    '''
    # galaxy_name_list = ['mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_25/fdisk_0p01_lgMdm_7p5_V200-200kmps',
    #                     'mu_25/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     'mu_25/fdisk_0p01_lgMdm_6p5_V200-200kmps',
    #                     'mu_25/fdisk_0p01_lgMdm_6p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps',
    #                     'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_1/fdisk_0p01_lgMdm_7p5_V200-200kmps',
    #                     'mu_1/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     'mu_1/fdisk_0p01_lgMdm_6p5_V200-200kmps']
    # snaps_list = [400,400,101,101,101,
    #               400,400,101,101,101,
    #               400,400,101,101]

    # galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps']
    # snaps_list = [400,400,101,101,101]

    galaxy_name_list = [
                        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps',
                        # 'mu_5_c_4/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_5_c_7/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_5_c_15/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        # 'mu_5_c_25/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        # 'mu_5_c_4/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_5_c_7/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_5_c_15/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        # 'mu_5_c_25/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        ]
    snaps_list = [400, 400, 101, 101, 101,
                  101, 101,  # 101,101,101,101,
                  400, 400,  # 101,400,400,400
                  ]

    if not concs:
        galaxy_name_list = [
            'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
            'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
            'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
            'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
            'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps',
        ]
        snaps_list = [400, 400, 101, 101, 101,
                      ]

    # galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     'mu_5_c_7/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     'mu_5_c_15/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_5_c_7/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_5_c_15/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     ]
    # snaps_list = [101, 101, 101,
    #               400, 400, 400,
    #               ]

    # galaxy_name_list = ['mu_5_c_7/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     'mu_5_c_7/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     ]
    # snaps_list = [101, 400,
    #               ]

    # things to calculate
    time_scale_array = []
    scale_density_ratio_array = []
    scale_velocity_ratio_array = []
    scale_vcirc_ratio_array = []
    n_array = []

    time_array = []

    velocity_scale_array_v = []
    velocity_scale2_array_dispersion = []

    measued_velocity_array_v = []
    inital_velocity_array_v = []
    measued_velocity2_array_z = []
    inital_velocity2_array_z = []
    measued_velocity2_array_r = []
    inital_velocity2_array_r = []
    measued_velocity2_array_p = []
    inital_velocity2_array_p = []

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = get_name_kwargs(galaxy_name)

        bin_edges = get_bin_edges(save_name_append, galaxy_name=galaxy_name)

        analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                                 save_name_append=save_name_append, galaxy_name=galaxy_name)
        analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)

        V200 = get_v_200(galaxy_name=galaxy_name)

        # calculate constants
        MassDM = kwargs['mdm']
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, MassDM, galaxy_name=galaxy_name)

        # load data
        (mean_v_R, mean_v_phi,
         sigma_z, sigma_R, sigma_phi
         ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges)

        n = get_n_for_poisson(galaxy_name, save_name_append, snaps, bin_edges)

        # burn in
        burn_in_mask = np.ones(snaps + 1, dtype=bool)
        burn_in_mask[:burn_in] = False

        time = np.linspace(0, T_TOT, snaps + 1) - np.linspace(0, T_TOT, snaps + 1)[burn_in]

        # max_log_tau_cut = -1.5
        max_log_tau_cut = -2.5
        # max_log_tau_cut = 0

        log_tau = np.log10(time[:, np.newaxis] / t_c_dm[np.newaxis, :])
        tau_mask = log_tau < max_log_tau_cut
        burn_in_mask = np.logical_and(burn_in_mask[:, np.newaxis], tau_mask)

        time = [np.linspace(0, T_TOT, snaps + 1)[burn_in_mask[:, 0]] - np.linspace(0, T_TOT, snaps + 1)[burn_in] + 1e-4,
                np.linspace(0, T_TOT, snaps + 1)[burn_in_mask[:, 1]] - np.linspace(0, T_TOT, snaps + 1)[burn_in] + 1e-4,
                np.linspace(0, T_TOT, snaps + 1)[burn_in_mask[:, 2]] - np.linspace(0, T_TOT, snaps + 1)[burn_in] + 1e-4]

        mean_v_R = mean_v_R.T[burn_in_mask.T]
        mean_v_phi = mean_v_phi.T[burn_in_mask.T]
        sigma_z = sigma_z.T[burn_in_mask.T]
        sigma_R = sigma_R.T[burn_in_mask.T]
        sigma_phi = sigma_phi.T[burn_in_mask.T]
        n = n.T[burn_in_mask.T]

        # shape = np.shape(sigma_z)
        # shape = np.array([np.ones(sum(burn_in_mask[:,0])), np.ones(sum(burn_in_mask[:,1])), np.ones(sum(burn_in_mask[:,2]))])
        data_length = np.sum(burn_in_mask)
        element_mask = np.zeros((len(bin_edges) // 2, data_length), dtype=bool)

        counter = 0
        for i in range(len(bin_edges) // 2):
            move = np.sum(tau_mask[:, i])
            element_mask[i, counter:counter + move] = True
            counter += move

        # (inital_velocity_v, inital_velocity_z, inital_velocity_r, inital_velocity_p
        #  ) = smooth_zero_snap(MassDM, snaps, save_name_append, bin_edges)
        (inital_velocity_v, inital_velocity_z, inital_velocity_r, inital_velocity_p
         ) = smooth_burn_ins(MassDM, snaps, save_name_append, burn_in, bin_edges, galaxy_name=galaxy_name)

        # #this weird that it is needed. Sometimes need analytic v_c > initial velocity
        # bad_ic_mask = inital_velocity_v > analytic_v_circ
        # analytic_v_circ[bad_ic_mask] = inital_velocity_v[bad_ic_mask] + 1e-3

        in_array = np.zeros(data_length)
        for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = time[i]
        time_array.append(in_array)

        # halo constants
        in_array = np.zeros(data_length)
        for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = t_c_dm[i]
        time_scale_array.append(in_array)
        in_array = np.zeros(data_length)
        for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = delta_dm[i]
        scale_density_ratio_array.append(in_array)
        in_array = np.zeros(data_length)
        for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = upsilon_dm[i]
        scale_velocity_ratio_array.append(in_array)
        in_array = np.zeros(data_length)
        for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = np.sqrt(upsilon_circ[i])
        # for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = upsilon_circ[i]
        scale_vcirc_ratio_array.append(in_array)

        # maximum values
        # in_array = np.zeros(data_length)
        # for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = analytic_v_circ[i]
        # # for i in range(len(bin_edges)//2): in_array[element_mask[i]] = inital_velocity_v[i]
        # velocity_scale_array_v.append(in_array)
        # in_array = np.zeros(data_length)
        # for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = analytic_dispersion[i] ** 2
        # velocity_scale2_array_dispersion.append(in_array)

        in_array = np.zeros(data_length)
        for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = V200
        # for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = V200 **2
        # for i in range(len(bin_edges)//2): in_array[element_mask[i]] = inital_velocity_v[i]
        velocity_scale_array_v.append(in_array)
        in_array = np.zeros(data_length)
        for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = V200 ** 2
        velocity_scale2_array_dispersion.append(in_array)

        # masued velocity data
        in_array = np.zeros(data_length)
        for i in range(len(bin_edges) // 2):
            in_array[element_mask[i]] = np.abs(analytic_v_circ[i, np.newaxis] - mean_v_phi[element_mask[i]])
            # in_array[element_mask[i]] = np.abs(analytic_v_circ[i, np.newaxis]**2 - mean_v_phi[element_mask[i]]**2)
            # in_array[element_mask[i]] = np.abs(analytic_v_circ[i, np.newaxis] - mean_v_phi[element_mask[i]])**2
            # in_array[element_mask[i]] = np.abs(inital_velocity_v[i, np.newaxis] - mean_v_phi[element_mask[i]])
        measued_velocity_array_v.append(in_array)
        measued_velocity2_array_z.append(sigma_z ** 2)
        measued_velocity2_array_r.append(sigma_R ** 2)
        measued_velocity2_array_p.append(sigma_phi ** 2)

        n_array.append(n)

        in_array = np.zeros(data_length)
        for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = (analytic_v_circ - inital_velocity_v)[i]
        # for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = (analytic_v_circ**2 - inital_velocity_v**2)[i]
        # for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = (analytic_v_circ - inital_velocity_v)[i]**2
        inital_velocity_array_v.append(in_array)
        # in_array = np.zeros(data_length)
        # inital_velocity_array_v.append(in_array)

        in_array = np.zeros(data_length)
        for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = inital_velocity_z[i] ** 2
        inital_velocity2_array_z.append(in_array)
        in_array = np.zeros(data_length)
        for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = inital_velocity_r[i] ** 2
        inital_velocity2_array_r.append(in_array)
        in_array = np.zeros(data_length)
        for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = inital_velocity_p[i] ** 2
        inital_velocity2_array_p.append(in_array)

    # flatten
    time_scale_array = np.ravel(np.concatenate(time_scale_array))
    scale_density_ratio_array = np.ravel(np.concatenate(scale_density_ratio_array))
    scale_velocity_ratio_array = np.ravel(np.concatenate(scale_velocity_ratio_array))
    scale_vcirc_ratio_array = np.ravel(np.concatenate(scale_vcirc_ratio_array))

    time_array = np.ravel(np.concatenate(time_array))

    velocity_scale_array_v = np.ravel(np.concatenate(velocity_scale_array_v))
    velocity_scale2_array_dispersion = np.ravel(np.concatenate(velocity_scale2_array_dispersion))

    measued_velocity_array_v = np.ravel(np.concatenate(measued_velocity_array_v))
    inital_velocity_array_v = np.ravel(np.concatenate(inital_velocity_array_v))
    measued_velocity2_array_z = np.ravel(np.concatenate(measued_velocity2_array_z))
    inital_velocity2_array_z = np.ravel(np.concatenate(inital_velocity2_array_z))
    measued_velocity2_array_r = np.ravel(np.concatenate(measued_velocity2_array_r))
    inital_velocity2_array_r = np.ravel(np.concatenate(inital_velocity2_array_r))
    measued_velocity2_array_p = np.ravel(np.concatenate(measued_velocity2_array_p))
    inital_velocity2_array_p = np.ravel(np.concatenate(inital_velocity2_array_p))

    n_array = np.ravel(np.concatenate(n_array))

    return (time_scale_array, scale_density_ratio_array, scale_velocity_ratio_array, scale_vcirc_ratio_array,
            time_array, velocity_scale_array_v, velocity_scale2_array_dispersion,
            measued_velocity_array_v, inital_velocity_array_v,
            measued_velocity2_array_z, inital_velocity2_array_z,
            measued_velocity2_array_r, inital_velocity2_array_r,
            measued_velocity2_array_p, inital_velocity2_array_p, n_array)


###############################################################################

def get_tau_array(time_array, time_scale_array):
    # tau as of 26/7/21
    tau_array = time_array / time_scale_array

    return (tau_array)


def get_tau_heat_array(scale_density_ratio_array, scale_velocity_ratio_array,
                       ln_Lambda_k_i, alpha_i, beta_i, gamma_i):
    # equation 10 in paper as of 26/7/21 # A7 as of 30/3/22
    # this can be calculated faster with logs ... this way is more straightforward for experimenting
    tau_heat_array = np.power((ln_Lambda_k_i * np.power(scale_density_ratio_array, alpha_i) *
                               np.power(scale_velocity_ratio_array, -beta_i) / scale_velocity_ratio_array**2),
                               -1.0 / (1 + gamma_i))

    return (tau_heat_array)


def get_tau_zero_array(velocity_scale2_array, inital_velocity2_array, scale_velocity_ratio_array):
    # # equation A8 in paper as of 30/3/22
    # tau_0_on_tau_heat = np.log(velocity_scale2_array / (velocity_scale2_array - inital_velocity2_array))

    # equation A8 in paper as of 4/5/22
    # sigma_dm_2 = velocity_scale2_array * scale_velocity_ratio_array**2
    # tau_0_on_tau_heat = scale_velocity_ratio_array**2 * np.log(
    #     sigma_dm_2 / (sigma_dm_2 - inital_velocity2_array))

    sigma_dm_2 = velocity_scale2_array * scale_velocity_ratio_array**2
    tau_0_on_tau_heat = np.log(
        sigma_dm_2 / (sigma_dm_2 - inital_velocity2_array))

    # if np.all(inital_velocity2_array != 0):
    #     print(inital_velocity2_array)
    #     raise NotImplimentedError('Eqation has changed and I was too lazy to impliment it.')

    return (tau_0_on_tau_heat)


def get_theory_velocity2_array(velocity_scale2_array, inital_velocity2_array,
                               time_array, time_scale_array,
                               scale_density_ratio_array, scale_velocity_ratio_array,
                               ln_Lambda_k_i, alpha_i, beta_i, gamma_i):

    tau_heat_array = get_tau_heat_array(scale_density_ratio_array, scale_velocity_ratio_array,
                                        ln_Lambda_k_i, alpha_i, beta_i, gamma_i)

    tau_0_on_tau_heat = get_tau_zero_array(velocity_scale2_array, inital_velocity2_array, scale_velocity_ratio_array)

    # #equation 11 in paper as of 29/7/21
    # theory_velocity2_array = (velocity_scale2_array - inital_velocity2_array) * (1 - np.exp(
    #   - np.power(((time_array / time_scale_array) / tau_heat_array),
    #   1 + gamma_i))) + inital_velocity2_array

    # # equation A9 in paper as of 30/3/22
    # theory_velocity2_array = velocity_scale2_array * (1 - np.exp(
    #     - np.power(((time_array / time_scale_array) / tau_heat_array) + tau_0_on_tau_heat,
    #                1 + gamma_i)))


    # equation ? in paper as of 21/4/22
    theory_velocity2_array = velocity_scale2_array * scale_velocity_ratio_array**2 * (1 - np.exp(
        - np.power(((time_array / time_scale_array) / tau_heat_array) +
                   tau_0_on_tau_heat,
                   1 + gamma_i)))

    # print(round(np.amax(theory_velocity2_array / velocity_scale2_array / scale_velocity_ratio_array**2), 6),
    #       round(np.amin(theory_velocity2_array / velocity_scale2_array / scale_velocity_ratio_array**2), 6))
    
    # # time_scale_array = sigma_DM^3 / (G^2 rho_DM m_DM)
    # # scale_velocity_ratio_array = sigma_DM / V200
    # 
    # theory_velocity2_array = (ln_Lambda_k_i / time_scale_array * scale_velocity_ratio_array**2 / 200**2 * 
    #                           np.power(scale_density_ratio_array, alpha_i) * np.power(scale_velocity_ratio_array, -beta_i)
    #                           + inital_velocity2_array) * time_array

    return theory_velocity2_array


def get_powerlaw_velocity2_array(velocity_scale2_array, inital_velocity2_array,
                                 time_array, time_scale_array,
                                 scale_density_ratio_array, scale_velocity_ratio_array,
                                 ln_Lambda_k_i, alpha_i, beta_i, gamma_i):
    tau_heat_array = get_tau_heat_array(scale_density_ratio_array, scale_velocity_ratio_array,
                                        ln_Lambda_k_i, alpha_i, beta_i, gamma_i) / scale_velocity_ratio_array**2

    # equation 8 in paper as of 30/7/21
    theory_velocity2_array = (velocity_scale2_array - inital_velocity2_array) * (
        np.power(((time_array / time_scale_array) / tau_heat_array),
                 1 + gamma_i)) + inital_velocity2_array

    return (theory_velocity2_array)


def evaluate_least_log_squares(measued_velocity2_array, velocity_scale2_array, inital_velocity2_array,
                               time_array, time_scale_array,
                               scale_density_ratio_array, scale_velocity_ratio_array, n_array, dof,
                               ln_Lambda_k_i, alpha_i, beta_i, gamma_i):
    '''Given a bunch of inputs, calculates the array of least squares.
    '''
    theory_velocity2_array = get_theory_velocity2_array(velocity_scale2_array, inital_velocity2_array,
                                                        time_array, time_scale_array,
                                                        scale_density_ratio_array, scale_velocity_ratio_array,
                                                        # 10**ln_Lambda_k_i, alpha_i, beta_i, gamma_i)
                                                        ln_Lambda_k_i, alpha_i, beta_i, gamma_i)

    # theory_velocity2_array = get_powerlaw_velocity2_array(velocity_scale2_array, inital_velocity2_array,
    #                                                       time_array, time_scale_array,
    #                                                       scale_density_ratio_array, scale_velocity_ratio_array,
    #                                                       # 10**ln_Lambda_k_i, alpha_i, beta_i, gamma_i)
    #                                                       ln_Lambda_k_i, alpha_i, beta_i, gamma_i)

    # TODO check this is not biasing stuff
    # least squares

    # least_log_squares_array = (np.log(measued_velocity2_array) - np.log(theory_velocity2_array)) ** 2
    least_log_squares_array = (np.log(measued_velocity2_array) - np.log(theory_velocity2_array)) ** 2 / np.log(theory_velocity2_array)

    # least_log_squares_array = (measued_velocity2_array - theory_velocity2_array)**2 / (
    #     theory_velocity2_array**2 / n_array)

    # metric = np.sum(least_log_squares_array) / (len(least_log_squares_array)/3 - dof)

    # metric = np.log(np.sum(least_log_squares_array))
    metric = np.log(np.mean(least_log_squares_array))

    # metric = np.mean((measued_velocity2_array - theory_velocity2_array) ** 2 / theory_velocity2_array)

    return metric


def _log_like(args, data, fixed_args):
    params = np.zeros(len(fixed_args))
    i = 0

    for j in range(len(fixed_args)):
        if fixed_args[j] == None:
            params[j] = args[i]
            i += 1
        else:
            params[j] = fixed_args[j]

    dof = i

    # stop k from being crazy
    # if params[0] > 4 or params[0] <= 0:
    if params[0] > 1e4 or params[0] <= 0:
        return like_fail_value
    if params[1] > 1.5 or params[1] < -1.5:
        return like_fail_value
    if params[2] > 5 or params[2] < -5:
        return like_fail_value
    if params[3] > 2 or params[3] < -2:
        return like_fail_value

    out = evaluate_least_log_squares(*data, dof, *params)

    if np.isnan(out):
        out = like_fail_value

    return out


@lru_cache(maxsize=None)
def find_least_log_squares_best_fit(save_name_append='diff',
                                    fixed_k=None, fixed_a=None, fixed_b=None, fixed_g=None,
                                    mcmc=False, overwrite=False, analyse_asymmetry=False, savefig=False):
    # if fixed_g != 0:
    #     raise ValueError('Illegal value of gamma!')

    if overwrite or not mcmc:
        (time_scale_array, scale_density_ratio_array, scale_velocity_ratio_array, scale_vcirc_ratio_array,
         time_array, velocity_scale_array_v, velocity_scale2_array_dispersion,
         measued_velocity_array_v, inital_velocity_array_v,
         measued_velocity2_array_z, inital_velocity2_array_z,
         measued_velocity2_array_r, inital_velocity2_array_r,
         measued_velocity2_array_p, inital_velocity2_array_p, n_array
         ) = get_raw_data(save_name_append=save_name_append)

        measured = [measued_velocity_array_v, measued_velocity2_array_z,
                    measued_velocity2_array_r, measued_velocity2_array_p]

        inital = [inital_velocity_array_v, inital_velocity2_array_z,
                  inital_velocity2_array_r, inital_velocity2_array_p]

        v_scale = [velocity_scale_array_v, velocity_scale2_array_dispersion,
                   velocity_scale2_array_dispersion, velocity_scale2_array_dispersion]

        scale_v = [scale_vcirc_ratio_array, scale_velocity_ratio_array,
                   scale_velocity_ratio_array, scale_velocity_ratio_array]

        fixed_args = []
        start_loc = []
        labels = []

        if fixed_k == None:
            # labels.append(r'$\log(\sqrt{2} \pi k_i \ln \Lambda)$')
            labels.append(r'$k_i \ln \Lambda$')
            start_loc.append(100.0)
        fixed_args.append(fixed_k)
        if fixed_a == None:
            labels.append(r'$\alpha_i$')
            start_loc.append(-0.5)
        fixed_args.append(fixed_a)
        if fixed_b == None:
            start_loc.append(0.0)
            labels.append(r'$\beta_i$')
        fixed_args.append(fixed_b)
        if fixed_g == None:
            start_loc.append(0.0)
            labels.append(r'$\gamma_i$')
        fixed_args.append(fixed_g)

        if mcmc:
            np.seterr(all='ignore')
            # start_loc[0] = np.log10(start_loc[0])

    else:
        measured = [None, None, None, None]
        inital = [None, None, None, None]
        v_scale = [None, None, None, None]

    name_list = ['v_phi', 'sigma_z', 'sigma_R', 'sigma_phi']

    ln_Lambda_ks = []
    alphas = []
    betas = []
    gammas = []

    file_name = '../galaxies/mcmc_' + save_name_append + '_fixed'
    if fixed_k != None: file_name += 'k'
    if fixed_a != None: file_name += 'a'
    if fixed_b != None: file_name += 'b'
    if fixed_g != None: file_name += 'g'

    for (measued_velocity2_array, inital_velocity2_array, velocity_scale2_array, scale_velocity_ratio_array, name
         ) in zip(measured, inital, v_scale, scale_v, name_list):

        if overwrite or not mcmc:
            data = (measued_velocity2_array, velocity_scale2_array, inital_velocity2_array,
                    time_array, time_scale_array,
                    scale_density_ratio_array, scale_velocity_ratio_array, n_array)
            
        if name == name_list[0]:
            min_result = minimize(_log_like, start_loc, args=(data, fixed_args),
                                  method='Nelder-Mead')
            params = np.zeros(len(fixed_args))
            i = 0
            for j in range(len(fixed_args)):
                if fixed_args[j] == None:
                    params[j] = min_result.x[i]
                    i += 1
                else:
                    params[j] = fixed_args[j]

            ln_Lambda_ks.append(params[0])
            alphas.append(params[1])
            betas.append(params[2])
            gammas.append(params[3])

            continue

        if mcmc:
            if overwrite:

                n_burn_in = int(0)
                n_anal = int(1000) #int(10_000)

                # n_burn_in = int(1_000)
                # n_anal = int(10_000) #int(10_000)

                #use best fit for starting location
                min_result = minimize(_log_like, start_loc, args=(data, fixed_args),
                                      method='Nelder-Mead')
                # method='CG')

                if not min_result.success:
                    print(name)
                    print(min_result)
                    
                start_loc = min_result.x 
                # start_loc[0] = np.log10(start_loc[0])
                if fixed_args[0] == None:
                    # start_loc[0] = np.log10(start_loc[0] / 4.442882938158366) #4.442882938158366
                    start_loc[0] = start_loc[0] / 4.442882938158366

                # emcee stuff
                # lnprior = lambda args : 0
                def lnprior(args, data, fixed_args, likelihood):
                    if np.isnan(likelihood):
                        return np.inf
                    if np.any(np.isnan(args)) or np.any(np.isinf(args)):
                        return np.inf
                    return 0

                # lnprob = lambda args : lnprior(args) + lnlike(args)
                def lnprob(args, data, fixed_args):
                    
                    #this is probably slow...
                    _args = []
                    for i, a in enumerate(args):
                        if fixed_args[0] == None and i == 0:
                            # _args.append(np.power(10, (4.442882938158366 * args[i])))
                            _args.append(4.442882938158366 * args[i])
                        else:
                            _args.append(args[i])
                    
                    # _data = add_random_error_to_data(data, randomness=0.01)
                    _data = data

                    # likelihood = _log_like(_args, data, fixed_args)
                    
                    likelihood = _log_like(_args, _data, fixed_args)
                    prior = lnprior(_args, data, fixed_args, likelihood)
                    
                    # if np.any(np.isinf(args)):
                    # print(_args, likelihood)

                    if not np.isfinite(prior) or np.isnan(prior):
                        return (-np.inf)
                    return 10**(prior - likelihood)
                    # return prior - likelihood
                
                nwalkers = 10
                ndim = len(start_loc)
                np.random.seed(1)

                p0 = [np.array(start_loc) + 1e-10 * np.random.randn(ndim) for i in range(nwalkers)]

                sampler = emcee.EnsembleSampler(nwalkers, ndim, lnprob, args=(data, fixed_args),
                                                live_dangerously=True)

                # print("Running burn-in...")
                # p0, _, _ = sampler.run_mcmc(p0, n_burn_in, skip_initial_state_check=True)
                # sampler.reset()

                print("Running...")
                pos, prob, state = sampler.run_mcmc(p0, n_burn_in + n_anal, skip_initial_state_check=True,
                                                    progress=True)

                # get resutls
                samples = sampler.get_chain(thin=4, discard=n_burn_in, flat=True)

                params = np.zeros(len(fixed_args))
                i = 0
                for j in range(len(fixed_args)):
                    if fixed_args[j] == None:
                        params[j] = np.median(samples, axis=0)[i]
                        i += 1
                    else:
                        params[j] = fixed_args[j]

                # fig =
                figure = corner.corner(samples,
                                       show_titles=True,
                                       labels=labels,
                                       plot_datapoints=True,
                                       quantiles=[0.16, 0.5, 0.84],
                                       title_fmt='.3f')

                # value1 = start_loc
                value2 = np.median(samples, axis=0)

                # Extract the axes
                axes = np.array(figure.axes).reshape((ndim, ndim))

                # Loop over the diagonal
                for i in range(ndim):
                    ax = axes[i, i]
                    # ax.axvline(value1[i], color='C1')
                    ax.axvline(value2[i], color='C0')

                # Loop over the histograms
                for yi in range(ndim):
                    for xi in range(yi):
                        ax = axes[yi, xi]
                        # ax.axvline(value1[xi], color='C1')
                        ax.axvline(value2[xi], color='C0')
                        # ax.axhline(value1[yi], color='C1')
                        ax.axhline(value2[yi], color='C0')
                        # ax.scatter(value1[xi], value1[yi], c='C1', s=10**2, ec='k')
                        # ax.scatter(value2[xi], value2[yi], c='C0', s=10**2, ec='k')
                
                if savefig:
                    plt.savefig(file_name + '_' + name + '.pdf', bbox_inches='tight')
                    plt.close()

                with open(file_name + '_' + name + '.txt', 'w') as file:
                    file.write(str(params[0]) + '\n')
                    file.write(str(params[1]) + '\n')
                    file.write(str(params[2]) + '\n')
                    file.write(str(params[3]) + '\n')

            else:
                with open(file_name + '_' + name + '.txt', 'r') as file:
                    data = file.read()
                    params = [float(s) for s in data.split('\n')[:-1]]

            params[0] = 4.442882938158366 * params[0]

            np.seterr(all='warn')
            
            return

        # just do minimize
        else:

            if name == 'v_phi' and fixed_k == False:
                start_loc[0] *= 2
                
            # the actual thing
            min_result = minimize(_log_like, start_loc, args=(data, fixed_args),
                                  method='Nelder-Mead')
            # method='CG')

            if not min_result.success:
                print(name)
                print(min_result)

            params = np.zeros(len(fixed_args))
            i = 0
            for j in range(len(fixed_args)):
                if fixed_args[j] == None:
                    params[j] = min_result.x[i]
                    i += 1
                else:
                    params[j] = fixed_args[j]

        # every loop
        ln_Lambda_ks.append(params[0])
        alphas.append(params[1])
        betas.append(params[2])
        gammas.append(params[3])
        
        # metric = min_result.fun
        metric = _log_like(params, data, [None]*4)

        n = np.sqrt(2) * np.pi # 2 * np.sqrt(2) * np.pi
        if name == 'v_phi':
            # n /= 2
            n = np.pi * np.sqrt(2)

        print(name)
        print('k ln L  = ', round(params[0] / n, 5))
        print('alpha   = ', round(params[1], 5))
        print('beta    = ', round(params[2], 5))
        # print('gamma  = ', round(params[3], 5))
        print('Squares = ', round(metric, 5))
        print()

    if analyse_asymmetry:

        galaxy_name_list = [
            'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
            'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
            'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
            'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
            'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps',
            # 'mu_5_c_4/fdisk_0p01_lgMdm_7p0_V200-200kmps',
            'mu_5_c_7/fdisk_0p01_lgMdm_7p0_V200-200kmps',
            'mu_5_c_15/fdisk_0p01_lgMdm_7p0_V200-200kmps',
            # 'mu_5_c_25/fdisk_0p01_lgMdm_7p0_V200-200kmps',
            # 'mu_5_c_4/fdisk_0p01_lgMdm_8p0_V200-200kmps',
            'mu_5_c_7/fdisk_0p01_lgMdm_8p0_V200-200kmps',
            'mu_5_c_15/fdisk_0p01_lgMdm_8p0_V200-200kmps',
            # 'mu_5_c_25/fdisk_0p01_lgMdm_8p0_V200-200kmps',
        ]
        snaps_list = [400, 400, 101, 101, 101,
                      101, 101,  # 101,101,101,101,
                      400, 400,  # 101,400,400,400
                      ]

        mean_v_phi = []
        theory_best_v_phi = []
        asymmetry_v_phi = []
        
        for galaxy_name, snaps in zip(galaxy_name_list, snaps_list):
            kwargs = get_name_kwargs(galaxy_name=galaxy_name)

            if kwargs['conc'] == 10:
            
                time = np.linspace(0, T_TOT, snaps+1)

                # can use higher resolution bins
                bin_edges = np.logspace(-1, 3, 101)

                # bins at a better resolution
                if save_name_append == 'diff':
                    bin_centres = 10 ** ((np.log10(bin_edges[:-1]) + np.log10(bin_edges[1:])) / 2)
                    bin_edges = np.roll(np.repeat(bin_edges, 2), -1)[:-2]

                # halo properties
                V200 = get_v_200(galaxy_name=galaxy_name)
                r200 = get_r_200(galaxy_name=galaxy_name)

                analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                                         save_name_append=save_name_append, galaxy_name=galaxy_name)

                analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                                     save_name_append=save_name_append, galaxy_name=galaxy_name)

                # theory
                # work out constatns
                (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
                 ) = get_constants(bin_edges, save_name_append, kwargs['mdm'], galaxy_name=galaxy_name)

                # burn ins
                (inital_velocity_v, inital_velocity2_z, inital_velocity2_r, inital_velocity2_p
                 ) = smooth_zero_snap(kwargs['mdm'], snaps, save_name_append, bin_edges, galaxy_name=galaxy_name)

                inital_velocity_v = np.amin((inital_velocity_v, analytic_v_circ), axis=0)

                inital_velocity2_z **=2
                inital_velocity2_r **=2
                inital_velocity2_p **= 2

                theory_best_v = np.zeros((len(bin_centres), snaps+1))
                theory_best_z2 = np.zeros((len(bin_centres), snaps+1))
                theory_best_r2 = np.zeros((len(bin_centres), snaps+1))
                theory_best_p2 = np.zeros((len(bin_centres), snaps+1))

                for i in range(len(bin_centres)):
                    theory_best_v[i] = get_theory_velocity2_array(V200, analytic_v_circ[i] - inital_velocity_v[i],
                                                                  time, t_c_dm[i], delta_dm[i], np.sqrt(upsilon_circ[i]),
                                                                  ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
                    theory_best_z2[i] = get_theory_velocity2_array(V200 ** 2, inital_velocity2_z[i],
                                                                   time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                                   ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
                    theory_best_r2[i] = get_theory_velocity2_array(V200 ** 2, inital_velocity2_r[i],
                                                                   time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                                   ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
                    theory_best_p2[i] = get_theory_velocity2_array(V200 ** 2, inital_velocity2_p[i],
                                                                   time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                                   ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

                theory_best_v = analytic_v_circ[:, np.newaxis] - theory_best_v

                # asymmetri dfirt
                d_sigma_r2_d_R = np.diff(theory_best_r2, axis=0)[:,:] / np.diff(bin_centres)[:, np.newaxis]

                R_disk = 4.15

                this = (1 - theory_best_p2[:-1] / (analytic_v_circ[:-1, np.newaxis] ** 2) +
                    theory_best_r2[:-1] / (analytic_v_circ[:-1, np.newaxis] ** 2) * (1 - bin_centres[:-1, np.newaxis] / R_disk) +
                    bin_centres[:-1, np.newaxis] / (analytic_v_circ[:-1, np.newaxis] ** 2) * d_sigma_r2_d_R)

                # v_phi_assymmetry = analytic_v_circ[:-1, np.newaxis] * np.sign(this) * np.sqrt(np.sign(this) * this)
                v_phi_assymmetry = analytic_v_circ[:-1, np.newaxis] * np.sqrt(this)

                    #(theory_best_r2[:-1,0] - theory_best_z2[:-1,0]) / analytic_v_circ[:-1]**2)

                # print(np.shape(theory_best_v[:-1, :]))
                # print(np.shape(v_phi_assymmetry))

                bin_edges = get_bin_edges('cum', galaxy_name = galaxy_name)

                # theroy_v = []
                # asym_v = []

                for snap in range(snaps+1):
                    theory_best_v_interp = interp1d(bin_centres[:-1], theory_best_v[:-1, snap])
                    asymmetry_v_interp = interp1d(bin_centres[:-1], v_phi_assymmetry[:, snap])

                    # theory_best_v_phi.append(theory_best_v_interp(bin_edges[1]))
                    theory_best_v_phi.append(theory_best_v_interp(bin_edges[3]))
                    # theory_best_v_phi.append(theory_best_v_interp(bin_edges[5]))
                    # asymmetry_v_phi.append(asymmetry_v_interp(bin_edges[1]))
                    asymmetry_v_phi.append(asymmetry_v_interp(bin_edges[3]))
                    # asymmetry_v_phi.append(asymmetry_v_interp(bin_edges[5]))

                (_, m_v_p,
                 _, _, _
                 ) = get_dispersions(galaxy_name, 'diff', snaps, bin_edges)

                for i in range(np.shape(m_v_p)[0]):
                    for j in range(np.shape(m_v_p)[1]):
                        if j == 1:
                            mean_v_phi.append(m_v_p[i,j])

        theory_best_v_phi = np.array(theory_best_v_phi)
        asymmetry_v_phi = np.array(asymmetry_v_phi)
        mean_v_phi = np.array(mean_v_phi)
        
        print(np.sum(np.isnan(theory_best_v_phi)))
        print(np.sum(np.isnan(asymmetry_v_phi)))
        print(np.sum(np.isnan(mean_v_phi)))
        print(np.sum(theory_best_v_phi < 0))
        print(np.sum(asymmetry_v_phi < 0))
        print(np.sum(mean_v_phi < 0))

        print(np.sum(theory_best_v_phi < 1))
        print(np.sum(asymmetry_v_phi < 1))
        print(np.sum(mean_v_phi < 1))

        print(theory_best_v_phi[:20])
        print(asymmetry_v_phi[:20])
        print(mean_v_phi[:20])
        
        print(theory_best_v_phi[-20:])
        print(asymmetry_v_phi[-20:])
        print(mean_v_phi[-20:])
        
        # for v in theory_best_v_phi:
        #     print(v)

        thresh = 40

        # least_log_squares_array = ((np.log(mean_v_phi) -
        #                            np.log(theory_best_v_phi)) ** 2 / np.log(theory_best_v_phi))[theory_best_v_phi > thresh]
        least_log_squares_array = ((mean_v_phi -
                                   theory_best_v_phi) ** 2)[theory_best_v_phi > thresh]
        metric = np.sqrt(np.nansum(least_log_squares_array)  / (np.sum(theory_best_v_phi > thresh)))
        
        print('Fit metric:', metric / 200)

        # least_log_squares_array = ((np.log(mean_v_phi) -
        #                            np.log(asymmetry_v_phi)) ** 2 / np.log(asymmetry_v_phi))[asymmetry_v_phi > thresh]
        least_log_squares_array = ((mean_v_phi -
                                   asymmetry_v_phi) ** 2)[asymmetry_v_phi > thresh]
        metric = np.sqrt(np.nansum(least_log_squares_array) / (np.sum(asymmetry_v_phi > thresh)))
        
        print('Asymmetry metric:', metric / 200)

    if mcmc:
        (time_scale_array, scale_density_ratio_array, scale_velocity_ratio_array, scale_vcirc_ratio_array,
         time_array, velocity_scale_array_v, velocity_scale2_array_dispersion,
         measued_velocity_array_v, inital_velocity_array_v,
         measued_velocity2_array_z, inital_velocity2_array_z,
         measued_velocity2_array_r, inital_velocity2_array_r,
         measued_velocity2_array_p, inital_velocity2_array_p, n_array
         ) = get_raw_data(save_name_append=save_name_append)

        tau_heat_v = get_tau_heat_array(scale_density_ratio_array, scale_vcirc_ratio_array,
                                         ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
        tau_heat_z2 = get_tau_heat_array(scale_density_ratio_array, scale_velocity_ratio_array,
                                         ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
        tau_heat_r2 = get_tau_heat_array(scale_density_ratio_array, scale_velocity_ratio_array,
                                         ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
        tau_heat_p2 = get_tau_heat_array(scale_density_ratio_array, scale_velocity_ratio_array,
                                         ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

        # print('t_sigma_z/t_sigma_R:', np.unique(tau_heat_z2 / tau_heat_r2))
        # print('t_sigma_p/t_sigma_R:', np.unique(tau_heat_p2 / tau_heat_r2))
        # print('t_sigma_p/t_sigma_z:', np.unique(tau_heat_p2 / tau_heat_z2))
        # print('t_v_phi/t_sigma_z:', np.unique(tau_heat_v / tau_heat_z2))
        # print('t_v_phi/t_sigma_R:', np.unique(tau_heat_v / tau_heat_r2))
        # print('t_v_phi/t_sigma_p:', np.unique(tau_heat_v / tau_heat_p2))
        # print('2t_v_phi/(t_sigmar + t_sigma_p):', np.unique(tau_heat_v / (tau_heat_r2 + tau_heat_p2)*2))

        gmh = 0.99 #G M200
        gmd = 0.01 #G M200
        a_h = 34 #kpc
        rd = 4.15 #kpc

        r_hs = np.array([4.09211063, 6.88264561, 11.08994484])

        disp_1d2 = (gmh / (12 * a_h) * (
                12 * r_hs * (r_hs + a_h) ** 3 / a_h ** 4 * np.log1p(a_h / r_hs) -
                r_hs / (r_hs + a_h) * (25 + 52 * r_hs / a_h +
                                             42 * (r_hs / a_h) ** 2 + 12 * (r_hs / a_h) ** 3)))

        gmh_r = gmh * (r_hs / (r_hs + a_h)) ** 2
        v_c2 = gmh_r / r_hs

        y = r_hs/(2*rd)
        v_c2_disk = gmd * r_hs ** 2 / (2 * rd ** 3) * (iv(0, y) * kn(0, y) - iv(1, y) * kn(1, y))

        # - R^2 d^2 Phi / dR^2

        # phi = - gmd / rd * y * (iv(0, y) * kn(1, y) - iv(1, y) * kn(0, y))
        # phi2 = - r_hs ** 2 * gmd / rd * (iv(0, y) * (4 * y * kn(1, y) - 2 * kn(0, y)) -
        #                                  2 * iv(1, y) * (2 * y * kn(0, y) + kn(1, y)))
        phi2_disk = r_hs ** 2 * gmd / (4 * rd**3) * (4 * y * iv(1, y) * kn(0, y) + 2 * iv(1, y) * kn(1, y) -
                                                     4 * y * iv(0, y) * kn(1, y) + 2 * iv(0, y) * kn(0, y))

        phi2 = - r_hs ** 2 * 2 * gmh / (a_h + r_hs) ** 3

        # print('1/gamma disk', 1/np.sqrt(3/4 + (phi2 + phi2_disk)/(v_c2 + v_c2_disk)/4))
        print('gamma^-2 disk', (3/4 + (phi2 + phi2_disk)/(v_c2 + v_c2_disk) / 4))
        print('gamma^2 disk', 1/(3/4 + (phi2 + phi2_disk)/(v_c2 + v_c2_disk) / 4))
        # print('1/gamma', 1/np.sqrt(3/4 + phi2/v_c2/4))
        print('gamma^-2', (3/4 + phi2/v_c2 / 4))
        print('gamma^2', 1/(3/4 + phi2/v_c2 / 4))

        print('v_c^2/sigma_DM^2', v_c2 / disp_1d2)
        print('v_c/sigma_DM', np.sqrt(v_c2 / disp_1d2))

        tau_0_on_tau_heat = get_tau_zero_array(velocity_scale_array_v, inital_velocity_array_v,
                                               np.sqrt(scale_vcirc_ratio_array))

        def to_min(f):
            # tau_heat_experiment = (tau_heat_p2 * scale_velocity_ratio_array / scale_vcirc_ratio_array)
            # tau_heat_experiment = ((tau_heat_z2 + tau_heat_r2 + tau_heat_p2) / 3 * f)
            # tau_heat_experiment = ((tau_heat_r2 + tau_heat_p2) / 2 * f)
            tau_heat_experiment = (tau_heat_p2 * f)

            experiment_v = velocity_scale_array_v * scale_vcirc_ratio_array ** 2 * (1 - np.exp(
                - np.power(((time_array / time_scale_array) / tau_heat_experiment) +  tau_0_on_tau_heat, 1 + gammas[0])))

            out = np.sum( ( experiment_v - measued_velocity_array_v )**2 )

            return out

        out = minimize(to_min, x0=(1,), method='Powell')

        print(out)

        #
        tau_0_on_tau_heat = get_tau_zero_array(velocity_scale2_array_dispersion, inital_velocity2_array_p,
                                               np.sqrt(scale_velocity_ratio_array))

        print('new')

        def to_min(f):
            tau_heat_experiment = (tau_heat_r2 * f)

            experiment_v = velocity_scale2_array_dispersion * scale_velocity_ratio_array ** 2 * (1 - np.exp(
                - np.power(((time_array / time_scale_array) / tau_heat_experiment) + tau_0_on_tau_heat, 1 + gammas[0])))

            out = np.sum((experiment_v - measued_velocity2_array_p) ** 2)

            return out

        out = minimize(to_min, x0=(1,), method='Powell')

        print(out)

        # experiment_v = velocity_scale_array_v * scale_vcirc_ratio_array ** 2 - experiment_v
        
        # print(experiment_v, measued_velocity_array_v)


        # raise ValueError

    if False:
        plt.figure()

        theory_velocity2_array = get_powerlaw_velocity2_array(velocity_scale2_array, inital_velocity2_array,
                                                              time_array, time_scale_array,
                                                              scale_density_ratio_array, scale_velocity_ratio_array,
                                                              params[0], params[1], params[2], params[3])

        metric_i = (np.log(measued_velocity2_array) - np.log(theory_velocity2_array))

        # metric_i = (measued_velocity2_array - theory_velocity2_array) / np.sqrt(
        #     theory_velocity2_array**2 / n_array)

        mean = np.mean(metric_i)
        std  = np.std(metric_i)

        n, bins, _ = plt.hist(metric_i, bins=50, density=True)

        for time_scale in  np.unique(time_scale_array):
            mask = time_scale == time_scale_array
            plt.hist(metric_i[mask], bins=bins, density=True, histtype='step')

        plt.errorbar(bins, scipy.stats.norm.pdf(bins, loc=mean, scale=std))

        plt.title(name)

    return (ln_Lambda_ks, alphas, betas, gammas)


####
def get_physical_from_model(galaxy_name, snaps, save_name_append, exp_model=True):
    # #the fit
    # print('\nbeta=0, gamma=0 (fiducial)')
    (ln_Lambda_ks, alphas, betas, gammas
     ) = find_least_log_squares_best_fit(
        save_name_append, mcmc=False, fixed_b=0, fixed_g=0)

    # where to measure
    bin_edges = get_bin_edges(save_name_append, galaxy_name=galaxy_name)

    # all galaxies
    analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)
    analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                         save_name_append=save_name_append, galaxy_name=galaxy_name)
    v200 = get_v_200(galaxy_name=galaxy_name)

    # load galaxy stuff
    kwargs = get_name_kwargs(galaxy_name)

    # time is complex to go backwards in time
    time = np.linspace(0, T_TOT, snaps + 1)  # , dtype=np.complex128)
    # time = np.linspace(0, T_TOT, snaps+1, dtype=np.complex128)
    time = time - time[BURN_IN]

    # work out constatns
    (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
     ) = get_constants(bin_edges, save_name_append, kwargs['mdm'], galaxy_name=galaxy_name)

    # burn ins
    (inital_velocity_v, inital_velocity2_z, inital_velocity2_r, inital_velocity2_p
     ) = smooth_burn_ins(kwargs['mdm'], snaps, save_name_append, BURN_IN, bin_edges, galaxy_name=galaxy_name)

    # (inital_velocity_v, inital_velocity2_z, inital_velocity2_r, inital_velocity2_p
    #  ) = smooth_zero_snap(kwargs['mdm'], snaps, save_name_append, bin_edges)
    
    inital_velocity2_z **= 2
    inital_velocity2_r **= 2
    inital_velocity2_p **= 2

    theory_best_v = np.zeros((len(bin_edges) // 2, snaps + 1))
    theory_best_z2 = np.zeros((len(bin_edges) // 2, snaps + 1))
    theory_best_r2 = np.zeros((len(bin_edges) // 2, snaps + 1))
    theory_best_p2 = np.zeros((len(bin_edges) // 2, snaps + 1))

    experiment_v = np.zeros((len(bin_edges) // 2, snaps + 1))

    tau_heat_v = np.zeros(len(bin_edges) // 2)
    tau_heat_z2 = np.zeros(len(bin_edges) // 2)
    tau_heat_r2 = np.zeros(len(bin_edges) // 2)
    tau_heat_p2 = np.zeros(len(bin_edges) // 2)

    tau_heat_experiment = np.zeros(len(bin_edges) // 2)

    if exp_model:
        func = get_theory_velocity2_array
    else: #linear model
        func = get_powerlaw_velocity2_array

    # the actual thing
    for i in range(len(bin_edges) // 2):
        # get_theory_velocity2_array
        theory_best_v[i] = func(v200, analytic_v_circ[i] - inital_velocity_v[i],
                                # theory_best_v[i] = get_theory_velocity2_array(analytic_c_circ[i], 0,
                                # theory_best_v[i] = get_theory_velocity2_array(inital_velocity_v[i], 0,
                                time, t_c_dm[i], delta_dm[i], np.sqrt(upsilon_circ[i]),
                                ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
        theory_best_z2[i] = func(v200 ** 2, inital_velocity2_z[i],
                                 time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                 ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
        theory_best_r2[i] = func(v200 ** 2, inital_velocity2_r[i],
                                 time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                 ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
        theory_best_p2[i] = func(v200 ** 2, inital_velocity2_p[i],
                                 time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                 ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

        tau_heat_z2[i] = get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                            ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
        tau_heat_r2[i] = get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                            ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
        tau_heat_p2[i] = get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                            ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

        # tau_heat_experiment[i] = ((tau_heat_z2[i] + tau_heat_r2[i] + tau_heat_p2[i]
        #                     ) * np.sqrt(upsilon_dm[i]) / np.sqrt(upsilon_circ[i]) / 3) #
        # tau_heat_experiment[i] = (tau_heat_r2[i] * 1.06126839)
        tau_heat_experiment[i] = (tau_heat_p2[i] * 0.74987599)
    
        # print(galaxy_name)
        # print('tau_p')
        # print(t_c_dm[1] * 0.003) #/ tau_heat_p2[1])
        # print(np.log10(t_c_dm[1] * 0.003))# / tau_heat_p2[1]))
        # print(0.003 / tau_heat_p2[1])
        # print(np.log10(0.003 / tau_heat_p2[1]))

        tau_0_on_tau_heat = get_tau_zero_array(v200, analytic_v_circ[i] - inital_velocity_v[i],
                                               np.sqrt(upsilon_circ[i]))

        if exp_model:
            experiment_v[i] = v200 * np.sqrt(upsilon_circ[i]) ** 2 * (1 - np.exp(
                - np.power(((time / t_c_dm[i]) / tau_heat_experiment[i]) +  tau_0_on_tau_heat, 1 + gammas[0])))
            # experiment_v[i] = v200 * np.sqrt(upsilon_circ[i]) ** 2 * (0.85 - 1 * np.exp(
            #     - np.power(((time / t_c_dm[i]) / tau_heat_experiment[i]) +  tau_0_on_tau_heat, 1 + gammas[0])))

        else:
            experiment_v[i] = (v200 - (analytic_v_circ[i] - inital_velocity_v[i])) * (
                np.power(((time / t_c_dm[i]) / tau_heat_experiment[i]),
                         1 + gammas[0])) + analytic_v_circ[i] - inital_velocity_v[i]

    experiment_v = analytic_v_circ[:, np.newaxis] - experiment_v

    theory_best_v = analytic_v_circ[:, np.newaxis] - theory_best_v
    # theory_best_v = inital_velocity_v[:, np.newaxis] - theory_best_v

    # print(inital_velocity_v, inital_velocity2_z, inital_velocity2_r, inital_velocity2_p)
    # print(theory_best_v[:,0], theory_best_z2[:,0], theory_best_r2[:,0], theory_best_p2[:,0])
    # print()

    # return (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2)
    return (experiment_v, theory_best_z2, theory_best_r2, theory_best_p2)


###############################################################################

def get_kappa_rot(galaxy_name, save_name_append, snaps, bin_edges, overwrite=False):
    '''
  '''
    if os.path.exists('../galaxies/' + galaxy_name + '_seed4'):
        max_seed = 5
        if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

        x_list = []
        for i in range(max_seed):
            galaxy_name_i = galaxy_name + '_seed' + str(i)
            x = get_kappa_rot(galaxy_name_i, save_name_append, snaps, bin_edges, overwrite)

            x_list.append(x)

        x_list = np.array(x_list)

        x = np.median(x_list, axis=0)

        return (x)

    if galaxy_name[-6:] == '_seed0':
        galaxy_name = galaxy_name[:-6]
    file_name = '../galaxies/' + galaxy_name + '/kappa_' + save_name_append
    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        kappa = np.load(file_name + '.npy')

    else:
        print('Calculating Kappas')
        print(galaxy_name)
        print(bin_edges)

        # set up arrays
        n_dim = len(bin_edges) // 2

        kappa = np.zeros((snaps + 1, n_dim), dtype=np.float64)

        # loop through all snaps
        for i in range(snaps + 1):
            snap = '{0:03d}'.format(int(i))
            # read a file
            (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_nbody.load_and_align(galaxy_name, snap)

            # calculate
            (R, phi, z, v_R, v_phi, v_z
             ) = get_cylindrical(PosStars, VelStars)

            # r = np.linalg.norm(PosStars, axis=1)
            v = np.linalg.norm(VelStars, axis=1)

            for j in range(n_dim):
                mask = np.logical_and(R > bin_edges[2 * j], R < bin_edges[2 * j + 1])

                energy_rotation = np.sum(MassStar * v_phi[mask] ** 2)
                energy_kinetic = np.sum(MassStar * v[mask] ** 2)

                kappa[i, j] = energy_rotation / energy_kinetic

        # save when done with loop
        np.save(file_name, kappa)

    return (kappa)


def get_kappa_co(galaxy_name, save_name_append, snaps, bin_edges, overwrite=False):
    '''
    '''
    if os.path.exists('../galaxies/' + galaxy_name + '_seed4'):
        max_seed = 5
        if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

        x_list = []
        for i in range(max_seed):
            galaxy_name_i = galaxy_name + '_seed' + str(i)
            x = get_kappa_co(galaxy_name_i, save_name_append, snaps, bin_edges, overwrite)

            x_list.append(x)

        x_list = np.array(x_list)

        x = np.median(x_list, axis=0)

        return (x)

    if galaxy_name[-6:] == '_seed0':
        galaxy_name = galaxy_name[:-6]
    file_name = '../galaxies/' + galaxy_name + '/kappa_co' + save_name_append
    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        kappa = np.load(file_name + '.npy')

    else:
        print('Calculating Kappas')
        print(galaxy_name)
        print(bin_edges)

        # set up arrays
        n_dim = len(bin_edges) // 2

        kappa = np.zeros((snaps + 1, n_dim), dtype=np.float64)

        # loop through all snaps
        for i in range(snaps + 1):
            snap = '{0:03d}'.format(int(i))
            # read a file
            (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_nbody.load_and_align(galaxy_name, snap)

            # calculate
            (R, phi, z, v_R, v_phi, v_z
             ) = get_cylindrical(PosStars, VelStars)

            # r = np.linalg.norm(PosStars, axis=1)
            v = np.linalg.norm(VelStars, axis=1)

            for j in range(n_dim):
                mask = np.logical_and(R > bin_edges[2 * j], R < bin_edges[2 * j + 1])

                energy_rotation = np.sum(MassStar * v_phi[np.logical_and(mask, v_phi > 0)] ** 2)
                energy_kinetic = np.sum(MassStar * v[mask] ** 2)

                kappa[i, j] = energy_rotation / energy_kinetic

        # save when done with loop
        np.save(file_name, kappa)

    return (kappa)


def get_lambda_R(galaxy_name, save_name_append, snaps, bin_edges,
                 line_of_sight_bins=None, overwrite=False):
    '''
    '''
    if os.path.exists('../galaxies/' + galaxy_name + '_seed4'):
        max_seed = 5
        if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

        x_list = []
        for i in range(max_seed):
            galaxy_name_i = galaxy_name + '_seed' + str(i)
            x = get_lambda_R(galaxy_name_i, save_name_append, snaps, bin_edges, line_of_sight_bins, overwrite)

            x_list.append(x)

        x_list = np.array(x_list)

        x = np.median(x_list, axis=0)

        return (x)

    if galaxy_name[-6:] == '_seed0':
        galaxy_name = galaxy_name[:-6]
    file_name = '../galaxies/' + galaxy_name + '/lambda_re_' + save_name_append
    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        lambda_R = np.load(file_name + '.npy')

    else:
        print('Calculating Lambda_R s')
        print(galaxy_name)
        print(bin_edges)

        if type(line_of_sight_bins) == type(None):
            max_rho = 30
            # n=2*max_rho + 1
            n = max_rho + 1

            line_of_sight_bin_edges = [np.linspace(-max_rho, max_rho, n), np.linspace(-max_rho, max_rho, n)]

        # set up arrays
        n_dim = len(bin_edges) // 2

        lambda_R = np.zeros((snaps + 1, n_dim), dtype=np.float64)

        # loop through all snaps
        for i in range(snaps + 1):
            snap = '{0:03d}'.format(int(i))
            # read a file
            (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_nbody.load_and_align(galaxy_name, snap)

            # calculate
            R = np.sqrt(PosStars[:, 1] ** 2 + PosStars[:, 0] ** 2)

            for j in range(n_dim):
                mask = np.logical_and(R > bin_edges[2 * j], R < bin_edges[2 * j + 1])

                x = PosStars[mask, 0]
                z = PosStars[mask, 2]
                v_y = VelStars[mask, 1]

                m_pix, _, _, _ = binned_statistic_2d(x, z, MassStar * np.ones(np.sum(mask)),
                                                     statistic='sum', bins=line_of_sight_bin_edges)
                v_pix, _, _, _ = binned_statistic_2d(x, z, v_y,
                                                     statistic='mean', bins=line_of_sight_bin_edges)
                s_pix, _, _, _ = binned_statistic_2d(x, z, v_y,
                                                     statistic='std', bins=line_of_sight_bin_edges)

                r_pix = np.sqrt(
                  (((line_of_sight_bin_edges[0][1:] + line_of_sight_bin_edges[0][:-1]) / 2) ** 2)[:, np.newaxis] +
                  (((line_of_sight_bin_edges[1][1:] + line_of_sight_bin_edges[1][:-1]) / 2) ** 2)[np.newaxis, :])

                # lambda_r[i, j] = np.nansum(m_pix * r_pix * np.abs(v_pix)) / np.nansum(
                #   m_pix * r_pix * np.sqrt(v_pix**2 + s_pix**2))

                ordered = 0
                total = 0
                for n in range(len(m_pix)):
                    for o in range(len(m_pix[n])):
                        if m_pix[n, o] > 10 * MassStar:
                            ordered += m_pix[n, o] * r_pix[n, o] * np.abs(v_pix[n, o])
                            total += m_pix[n, o] * r_pix[n, o] * np.sqrt(v_pix[n, o] ** 2 + s_pix[n, o] ** 2)

                lambda_R[i, j] = ordered / total

        # save when done with loop
        np.save(file_name, lambda_R)

    return (lambda_R)


def get_lambda_dm(galaxy_name, save_name_append, snaps, bin_edges, overwrite=False):
    '''
    '''
    if os.path.exists('../galaxies/' + galaxy_name + '_seed4'):
        max_seed = 5
        if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

        x_list = []
        for i in range(max_seed):
            galaxy_name_i = galaxy_name + '_seed' + str(i)
            x = get_lambda_dm(galaxy_name_i, save_name_append, snaps, bin_edges, overwrite)

            x_list.append(x)

        x_list = np.array(x_list)

        x = np.median(x_list, axis=0)

        return (x)

    if galaxy_name[-6:] == '_seed0':
        galaxy_name = galaxy_name[:-6]
    file_name = '../galaxies/' + galaxy_name + '/lambda_dm_' + save_name_append
    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        lambda_dm = np.load(file_name + '.npy')

    else:
        print('Calculating Lambda_DM s')
        print(galaxy_name)
        print(bin_edges)

        # set up arrays
        n_dim = len(bin_edges) // 2

        lambda_dm = np.zeros((snaps + 1, n_dim), dtype=np.float64)

        # loop through all snaps
        for i in range(snaps + 1):
            snap = '{0:03d}'.format(int(i))
            # read a file
            (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_nbody.load_and_align(galaxy_name, snap)

            # calculate
            (R, phi, z, v_R, v_phi, v_z
             ) = get_cylindrical(PosStars, VelStars)
            ke = 0.5 * (v_R ** 2 + v_phi ** 2 + v_z ** 2)

            # r = np.linalg.norm(PosStars, axis=1)
            # (M_disk, r_disk) = get_exponential_disk_params()
            # pot_circ = (hernquist_potential(r) +
            #             get_exponential_disk_potential([r]*2, M_disk, r_disk))[0]

            R_dm = PosDMs[:, 0] ** 2 + PosDMs[:, 1] ** 2

            # (R_dm, phi_dm, z_dm, v_R_dm, v_phi_dm, v_z_dm
            #  ) = get_cylindrical(PosDMs, VelDMs)
            # ke_dm = 0.5 * (v_R_dm**2 + v_phi_dm**2 + v_z_dm**2)

            for j in range(n_dim):
                mask = np.logical_and(R > bin_edges[2 * j], R < bin_edges[2 * j + 1])
                mask_dm = np.logical_and(R_dm > bin_edges[2 * j], R_dm < bin_edges[2 * j + 1])

                stellar_M = MassStar * np.sum(mask)  # 10^10 M_sun

                stellar_j = np.sum(MassStar * R[mask] * v_phi[mask]) / stellar_M  # kpc km/s

                stellar_e = np.sum(MassStar * (ke[mask] + PotStars[mask])) / stellar_M  # km^2 / s^2

                total_M = stellar_M + MassDM * np.sum(mask_dm)

                lambda_dm[i, j] = stellar_j * np.sqrt(np.abs(stellar_e)) / (GRAV_CONST * total_M)

                # stellar_J = np.sum(MassStar * R[mask] * v_phi[mask]) #kpc km/s
                #
                # stellar_E = np.sum(MassStar * (ke[mask] + PotStars[mask])) #km^2 / s^2
                #
                # dm_J = np.sum(MassDM * R_dm[mask_dm] * v_phi_dm[mask_dm]) #kpc km/s
                #
                # dm_E = np.sum(MassDM * (ke_dm[mask_dm] + PotDMs[mask_dm])) #km^2 / s^2
                #
                # total_M = MassStar * np.sum(mask) + MassDM * np.sum(mask_dm)
                #
                # # lambda_dm[i, j] = stellar_J * np.sqrt(np.abs(stellar_E)) / (GRAV_CONST * total_M**2.5)
                # lambda_dm[i, j] = (stellar_J + dm_J) * np.sqrt(np.abs(stellar_E + dm_E)) / (GRAV_CONST * total_M**2.5)

        # save when done with loop
        np.save(file_name, lambda_dm)

    return (lambda_dm)


def get_bulge_to_total(galaxy_name, save_name_append, snaps, bin_edges, overwrite=False):
    '''
    '''
    if os.path.exists('../galaxies/' + galaxy_name + '_seed4'):
        max_seed = 5
        if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

        x_list = []
        for i in range(max_seed):
            galaxy_name_i = galaxy_name + '_seed' + str(i)
            x = get_bulge_to_total(galaxy_name_i, save_name_append, snaps, bin_edges, overwrite)

            x_list.append(x)

        x_list = np.array(x_list)

        x = np.median(x_list, axis=0)

        return (x)

    if galaxy_name[-6:] == '_seed0':
      galaxy_name = galaxy_name[:-6]
    file_name = '../galaxies/' + galaxy_name + '/bulge_to_total' + save_name_append
    if not overwrite and os.path.isfile(file_name + '.npy'):
      # load
      bulge_to_total = np.load(file_name + '.npy')

    else:
        print('Calculating B/Ts')
        print(galaxy_name)
        print(bin_edges)

        # set up arrays
        n_dim = len(bin_edges) // 2

        bulge_to_total = np.zeros((snaps + 1, n_dim), dtype=np.float64)

        # loop through all snaps
        for i in range(snaps + 1):
            snap = '{0:03d}'.format(int(i))
            # read a file
            (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_nbody.load_and_align(galaxy_name, snap)

            # calculate
            (R, phi, z, v_R, v_phi, v_z
             ) = get_cylindrical(PosStars, VelStars)

            for j in range(n_dim):
                mask = np.logical_and(R > bin_edges[2 * j], R < bin_edges[2 * j + 1])

                m_tot = MassStar * np.sum(mask)
                m_cr = MassStar * np.sum(v_phi[mask] < 0)

                bulge_to_total[i, j] = 2 * m_cr / m_tot

        # save when done with loop
        np.save(file_name, bulge_to_total)

    return (bulge_to_total)


def load_galaxy_abc(galaxy_name, save_name_append, snaps, bin_edges, overwrite=False):
    if os.path.exists('../galaxies/' + galaxy_name + '_seed4'):
        max_seed = 5
        if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

        a_list = []
        b_list = []
        c_list = []

        for i in range(max_seed):
            galaxy_name_i = galaxy_name + '_seed' + str(i)
            (a, b, c) = load_galaxy_abc(galaxy_name_i, save_name_append, snaps, bin_edges, overwrite)

            if save_name_append == '' and len(np.shape(a)) > 1:
                a_list.append(a.T[0])
                b_list.append(b.T[0])
                c_list.append(c.T[0])
            else:
                a_list.append(a)
                b_list.append(b)
                c_list.append(c)

        # TODO not sure this the correct way to median them...
        a_list = np.array(a_list)
        b_list = np.array(b_list)
        c_list = np.array(c_list)

        a = np.median(a_list, axis=0)
        b = np.median(b_list, axis=0)
        c = np.median(c_list, axis=0)

        return (a, b, c)

    if galaxy_name[-6:] == '_seed0':
        galaxy_name = galaxy_name[:-6]
    file_name = '../galaxies/' + galaxy_name + '/shape' + save_name_append
    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        (a, b, c) = np.load(file_name + '.npy')

    else:
        print('Calculating Shapes')
        print(galaxy_name)

        # set up arrays
        n_dim = len(bin_edges) // 2

        a = np.zeros((snaps + 1, n_dim))
        b = np.zeros((snaps + 1, n_dim))
        c = np.zeros((snaps + 1, n_dim))

        for i in range(snaps + 1):
            # print(i)
            # load a snap
            (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_nbody.load_and_align(galaxy_name, snap='{0:03d}'.format(int(i)))

            # calculate
            (R, phi, z, v_R, v_phi, v_z
             ) = get_cylindrical(PosStars, VelStars)

            for j in range(n_dim):
                mask = np.logical_and(R > bin_edges[2 * j], R < bin_edges[2 * j + 1])

                a_, b_, c_ = find_abc(PosStars[mask], np.ones(np.sum(mask)))

                a[i, j] = a_
                b[i, j] = b_
                c[i, j] = c_

        # save when done with loop
        np.save(file_name, (a, b, c))

    return (a, b, c)

def get_median_z(galaxy_name, save_name_append, snaps, bin_edges, overwrite=False):
    if os.path.exists('../galaxies/' + galaxy_name + '_seed4'):
        max_seed = 5
        if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

        x_list = []
        for i in range(max_seed):
            galaxy_name_i = galaxy_name + '_seed' + str(i)
            x = get_median_z(galaxy_name_i, save_name_append, snaps, bin_edges, overwrite)

            x_list.append(x)

        x_list = np.array(x_list)

        x = np.median(x_list, axis=0)

        return (x)

    if galaxy_name[-6:] == '_seed0':
        galaxy_name = galaxy_name[:-6]
    file_name = '../galaxies/' + galaxy_name + '/median_z_' + save_name_append
    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        z_half = np.load(file_name + '.npy')

    else:
        print('Calculating z half')
        print(galaxy_name)

        # set up arrays
        n_dim = len(bin_edges) // 2

        z_half = np.zeros((snaps + 1, n_dim))

        for i in range(snaps + 1):
            # print(i)
            # load a snap
            (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_nbody.load_and_align(galaxy_name, snap='{0:03d}'.format(int(i)))

            R = np.sqrt(PosStars[:, 0] ** 2 + PosStars[:, 1] ** 2)

            for j in range(n_dim):
                mask = np.logical_and(R > bin_edges[2 * j], R < bin_edges[2 * j + 1])

                z_half[i, j] = np.median(np.abs(PosStars[mask, 2]))

        # save when done with loop
        np.save(file_name, z_half)

    return (z_half)


def get_R_half(galaxy_name, snaps, overwrite=False):
    if os.path.exists('../galaxies/' + galaxy_name + '_seed4'):
        max_seed = 5
        if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

        x_list = []
        for i in range(max_seed):
            galaxy_name_i = galaxy_name + '_seed' + str(i)
            x = get_R_half(galaxy_name_i, snaps, overwrite)

            x_list.append(x)

        x_list = np.array(x_list)

        x = np.median(x_list, axis=0)

        return (x)

    if galaxy_name[-6:] == '_seed0':
        galaxy_name = galaxy_name[:-6]
    file_name = '../galaxies/' + galaxy_name + '/R_halfdiff'
    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        R_half = np.load(file_name + '.npy')

    else:
        print('Calculating R half')
        print(galaxy_name)

        # set up arrays
        R_half = np.zeros(snaps + 1)

        for i in range(snaps + 1):
            # print(i)
            # load a snap
            (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_nbody.load_and_align(galaxy_name, snap='{0:03d}'.format(int(i)))

            R = np.sqrt(PosStars[:, 0] ** 2 + PosStars[:, 1] ** 2)

            R_half[i] = np.median(R)

        # save when done with loop
        np.save(file_name, R_half)

    return (R_half)


def get_angular_deviation(galaxy_name, save_name_append, snaps, bin_edges=[], overwrite=False):
    if os.path.exists('../galaxies/' + galaxy_name + '_seed4'):
        max_seed = 5
        if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

        x_list = []
        for i in range(max_seed):
            galaxy_name_i = galaxy_name + '_seed' + str(i)
            x = get_angular_deviation(galaxy_name_i, save_name_append, snaps, bin_edges, overwrite)

            x_list.append(x)

        x_list = np.array(x_list)

        x = np.median(x_list, axis=0)

        return x

    if galaxy_name[-6:] == '_seed0':
        galaxy_name = galaxy_name[:-6]
    file_name = '../galaxies/' + galaxy_name + '/angular_deviation' + save_name_append
    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        (rms_theta, sigma_theta) = np.load(file_name + '.npy')

    else:
        print('Calculating angular deviation')
        print(galaxy_name)

        # set up arrays
        n_dim = len(bin_edges) // 2

        rms_theta = np.zeros((snaps + 1, n_dim))
        sigma_theta = np.zeros((snaps + 1, n_dim))

        for i in range(snaps + 1):
            # print(i)
            # load a snap
            (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_nbody.load_and_align(galaxy_name, snap='{0:03d}'.format(int(i)))

            r = np.linalg.norm(PosStars, axis=1)
            theta = np.arccos(PosStars[:, 2] / (r + 1e-6))

            for j in range(n_dim):
                mask = np.logical_and(r > bin_edges[2 * j], r < bin_edges[2 * j + 1])

                rms_theta[i, j] = np.sqrt( np.mean( (theta[mask] - np.pi/2 ) ** 2) )
                sigma_theta[i, j] = np.std(theta[mask])

        # save when done with loop
        np.save(file_name, (rms_theta, sigma_theta))

    return (rms_theta, sigma_theta)


def get_cross_terms(galaxy_name, save_name_append, snaps, bin_edges, overwrite=False):
    '''
    '''
    file_name = '../galaxies/' + galaxy_name + '/cross_terms_' + save_name_append
    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        (vRvz, vRvphi, vzvphi) = np.load(file_name + '.npy')

    else:
        print('Calculating Cross Terms')
        print(galaxy_name)
        print(bin_edges)

        # set up arrays
        n_dim = len(bin_edges) // 2

        # intialize
        vRvz = np.zeros((snaps + 1, n_dim), dtype=np.float64)
        vRvphi = np.zeros((snaps + 1, n_dim), dtype=np.float64)
        vzvphi = np.zeros((snaps + 1, n_dim), dtype=np.float64)

        # loop through all snaps
        for i in range(snaps + 1):
            snap = '{0:03d}'.format(int(i))
            # read a file
            (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_nbody.load_and_align(galaxy_name, snap)

            # cylindrical
            R, _, _, v_R, v_phi, v_z = get_cylindrical(PosStars, VelStars)

            # calculate
            for j in range(len(bin_edges) // 2):
                mask = np.logical_and((R > bin_edges[2 * j]), (R < bin_edges[2 * j + 1]))
                bin_N_i = 1 / np.sum(mask)

                # need to do this. mean of v_z and v_R = 0
                mean_v_phi = np.mean(v_phi[mask])
                d_v_phi = v_phi[mask] - mean_v_phi

                # these are the squared values
                vRvz[i, j] = np.sum(v_R[mask] * v_z[mask]) * bin_N_i
                vRvphi[i, j] = np.sum(v_R[mask] * d_v_phi) * bin_N_i
                vzvphi[i, j] = np.sum(v_z[mask] * d_v_phi) * bin_N_i

        # save when done with loop
        np.save(file_name, (vRvz, vRvphi, vzvphi))

    return (vRvz, vRvphi, vzvphi)


def get_n_for_poisson(galaxy_name, save_name_append, snaps, bin_edges, overwrite=False):
    '''
    '''
    if os.path.exists('../galaxies/' + galaxy_name + '_seed4'):
        max_seed = 5
        if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

        x_list = []
        for l in range(max_seed):
            galaxy_name_i = galaxy_name + '_seed' + str(l)
            x = get_n_for_poisson(galaxy_name_i, save_name_append, snaps, bin_edges, overwrite)

            x_list.append(x)

        x_list = np.array(x_list)

        # x = np.median(x_list, axis=0)
        x = np.mean(x_list, axis=0)

        return (x)

    if galaxy_name[-6:] == '_seed0':
        galaxy_name = galaxy_name[:-6]

    file_name = '../galaxies/' + galaxy_name + '/n_for_poisson_' + save_name_append
    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        n = np.load(file_name + '.npy')

    else:
        print('Calculating n')
        print(galaxy_name)
        print(bin_edges)

        # set up arrays
        n_dim = len(bin_edges) // 2

        # intialize
        n = np.zeros((snaps + 1, n_dim), dtype=np.int64)

        # loop through all snaps
        for i in range(snaps + 1):
            snap = '{0:03d}'.format(int(i))
            # read a file
            (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_nbody.load_and_align(galaxy_name, snap)

            # cylindrical
            R_2 = PosStars[:, 0] ** 2 + PosStars[:, 1] ** 2

            # calculate
            for j in range(len(bin_edges) // 2):
                mask = np.logical_and((R_2 > bin_edges[2 * j] ** 2), (R_2 < bin_edges[2 * j + 1] ** 2))
                n[i, j] = np.sum(mask)

        # save when done with loop
        np.save(file_name, n)

    return (n)


def get_j_zonc_evolution(galaxy_name, save_name_append, snaps, bin_edges,
                         j_zonc_edges=np.linspace(-1.2, 1.2, 4 * 24 + 1), overwrite=False):
    '''
    '''
    subsample_N = 100_000

    if os.path.exists('../galaxies/' + galaxy_name + '_seed4'):
        max_seed = 5
        if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

        x_list = []
        for l in range(max_seed):
            galaxy_name_i = galaxy_name + '_seed' + str(l)
            x = get_j_zonc_evolution(galaxy_name_i, save_name_append, snaps, bin_edges, j_zonc_edges, overwrite)

            x_list.append(x)

        x_list = np.array(x_list)

        # x = np.median(x_list, axis=0)
        x = np.mean(x_list, axis=0)

        return (x)

    if galaxy_name[-6:] == '_seed0':
        galaxy_name = galaxy_name[:-6]

    file_name = '../galaxies/' + galaxy_name + '/j_zonc_evolution_' + save_name_append
    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        j_zoncs = np.load(file_name + '.npy')

    else:
        print('Calculating j_z/j_c(E)')
        print(galaxy_name)
        print(bin_edges)

        # set up arrays
        n_dim = len(bin_edges) // 2

        # initialize
        j_zoncs = np.zeros((snaps + 1, n_dim, len(j_zonc_edges) - 1))
    
        #for two body
        eps = 0.2  # kpc #softening length
        minR = np.amax((eps, 0.2 * np.amin(bin_edges)))  # kpc
        maxR = np.amax(bin_edges) * 5  # kpc
        nbins = 50
        pot_bin_edges = np.logspace(np.log10(minR), np.log10(maxR), num=nbins + 1)
        Rbins = 10 ** ((np.log10(pot_bin_edges[:-1]) + np.log10(pot_bin_edges[1:])) / 2)

        root2 = np.sqrt(2)
        # calculate potential at each radii along x and y axis and diagonals
        xin = np.concatenate((Rbins, -Rbins, np.zeros(len(Rbins)), np.zeros(len(Rbins)),
                              Rbins / root2, -Rbins / root2, -Rbins / root2, Rbins / root2))
        yin = np.concatenate((np.zeros(len(Rbins)), np.zeros(len(Rbins)), Rbins, -Rbins,
                              Rbins / root2, Rbins / root2, -Rbins / root2, -Rbins / root2))
        xin = xin.flatten()
        yin = yin.flatten()
        zin = np.zeros(len(xin))
        ni = len(xin)

        for i in range(snaps + 1):

            snap = '{0:03d}'.format(int(i))
            # read snap
            (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_nbody.load_and_align(galaxy_name, snap)

            # calculate
            (R, phi, z, v_R, v_phi, v_z
             ) = get_cylindrical(PosStars, VelStars)

            # r_stars = np.linalg.norm(PosStars, axis=1)
            # r_dms = np.linalg.norm(PosDMs, axis=1)

            xd = PosDMs[:, 0]
            yd = PosDMs[:, 1]
            zd = PosDMs[:, 2]
            nd = len(xd)
            md = MassDM * np.ones(nd)

            if len(PosDMs) > subsample_N:
                factor = len(PosDMs) / subsample_N

                if i == 0: print('subsampling dm with factor: ', factor)

                subsample = np.random.choice(len(PosDMs), subsample_N, replace=False)

                xd = PosDMs[subsample, 0]
                yd = PosDMs[subsample, 1]
                zd = PosDMs[subsample, 2]
                nd = subsample_N
                md = factor * MassDM * np.ones(nd)

            xs = PosStars[:, 0]
            ys = PosStars[:, 1]
            zs = PosStars[:, 2]
            ns = len(xs)
            ms = MassStar * np.ones(ns)

            # start1 = time.time()
            phi = twobody.midplane_potential(md, xd, yd, zd, ms, xs, ys, zs, xin, yin, zin, eps, nd, ns, ni)
            # end1 = time.time()

            pot_midplane = np.zeros(nbins)
            for j in range(nbins):
                # averages each coordinate
                # pot_midplane[j] = (phi[j]+phi[j+nbins]+phi[j+2*nbins]+phi[j+3*nbins])*0.25
                pot_midplane[j] = -GRAV_CONST * (phi[j] + phi[j + nbins] +
                                                 phi[j + 2 * nbins] + phi[j + 3 * nbins] +
                                                 phi[j + 4 * nbins] + phi[j + 5 * nbins] +
                                                 phi[j + 6 * nbins] + phi[j + 7 * nbins]) * 0.125

            # star_bins = np.digitize(r_stars, bin_edges)
            # dm_bins = np.digitize(r_dms, bin_edges)
            # pot_midplane = np.array([np.mean(np.hstack((
            #     PotStars[star_bins == (i + 1)], PotDMs[dm_bins == (i + 1)])))
            #     for i in range(nbins)])

            # start2 = time.time()
            vcirc2 = twobody.midplane_vcirc2(md, xd, yd, zd, ms, xs, ys, zs, xin, yin, zin, eps, nd, ns, ni)
            # end2 = time.time()

            v_circ_full = np.zeros(nbins)
            for j in range(nbins):
                # averages each coordinate
                # v_circ_full[j] = np.sqrt(grav_const*(vcirc2[j]+vcirc2[j+nbins]+
                #                                 vcirc2[j+2*nbins]+vcirc2[j+3*nbins])*0.25)
                v_circ_full[j] = np.sqrt(GRAV_CONST * (vcirc2[j] + vcirc2[j + nbins] +
                                                       vcirc2[j + 2 * nbins] + vcirc2[j + 3 * nbins] +
                                                       vcirc2[j + 4 * nbins] + vcirc2[j + 5 * nbins] +
                                                       vcirc2[j + 6 * nbins] + vcirc2[j + 7 * nbins]) * 0.125)

            j_circ = v_circ_full * Rbins
            # j_circ = savgol_filter(j_circ, nbins // 2, 3)

            # for i in range(len(j_circ)-1):
            #     if j_circ[i + 1] < j_circ[i]:
            #         j_circ[i + 1] = j_circ[i] + 1e-10

            e_circ = 0.5 * v_circ_full ** 2 + pot_midplane
            # e_circ = savgol_filter(e_circ, nbins // 2, 3)
            
            #force energies to be in order. This is sometimes necissary for the centre.
            for k in range(len(e_circ)-1):
                if e_circ[k + 1] < e_circ[k]:
                    print('Warning: Adjusting energeis in the centre of the halo.')
                    e_circ[k + 1] = e_circ[k] + 1e-10

            if False:
                plt.figure(2)
                plt.plot(j_circ, e_circ)

                plt.figure(3)
                plt.plot(Rbins, j_circ)

                plt.figure(4)
                plt.plot(Rbins, e_circ)

                plt.show()

            print(i)

            try:
                interp = InterpolatedUnivariateSpline(e_circ, j_circ, k=1, ext='const')#'raise')
            except ValueError: #ValueError: x must be increasing if s > 0 #I think other value errors are possbile ...
                print('Warning: There is a crazy negative acceleration.')

                def is_good(vc2, possible_vcirc2s):
                    five = 5
                    sort_possible_vcirc2s = np.sort(possible_vcirc2s)
                    #check without one crazy value
                    sig = np.amin((np.std(sort_possible_vcirc2s[1:]), np.std(sort_possible_vcirc2s[:-1])))
                    med = np.median(sort_possible_vcirc2s)

                    out = (vc2 > med - five*sig) and (vc2 < med + five*sig)
                    return out

                for j in range(nbins):
                    good_vcirc2s = []

                    possible_vcirc2s = [vcirc2[j], vcirc2[j + nbins],
                                        vcirc2[j + 2 * nbins], vcirc2[j + 3 * nbins],
                                        vcirc2[j + 4 * nbins], vcirc2[j + 5 * nbins],
                                        vcirc2[j + 6 * nbins], vcirc2[j + 7 * nbins]] 

                    for vc2 in possible_vcirc2s:
                        if is_good(vc2, possible_vcirc2s):
                            good_vcirc2s.append(vc2)
                    
                    if len(possible_vcirc2s) != len(good_vcirc2s):
                        print(possible_vcirc2s)
                        print('vc^2 to vc^2')
                        print(good_vcirc2s)

                        # averages each coordinate
                        v_circ_full[j] = np.sqrt(GRAV_CONST * np.sum(good_vcirc2s) / len(good_vcirc2s))

                j_circ = v_circ_full * Rbins
                # j_circ = savgol_filter(j_circ, nbins // 2, 3)

                # for i in range(len(j_circ)-1):
                #     if j_circ[i + 1] < j_circ[i]:
                #         j_circ[i + 1] = j_circ[i] + 1e-10

                e_circ = 0.5 * v_circ_full ** 2 + pot_midplane
                # e_circ = savgol_filter(e_circ, nbins // 2, 3)

                for k in range(len(e_circ) - 1):
                    if e_circ[k + 1] < e_circ[k]:
                        e_circ[k + 1] = e_circ[k] + 1e-10

                interp = InterpolatedUnivariateSpline(e_circ, j_circ, k=1, ext='const')#'raise')

            # raise NotImplementedError

            phi = twobody.star_potential(md, xd, yd, zd, ms, xs, ys, zs, eps)
            phi = -GRAV_CONST * phi

            EnergyStars = 0.5 * np.linalg.norm(VelStars, axis=1)**2 + phi

            # the main point of these calculations
            j_c_stars = interp(EnergyStars)

            for j in range(n_dim):
                mask = np.logical_and(R > bin_edges[2 * j], R < bin_edges[2 * j + 1])

                j_z = R[mask] * v_phi[mask]
                j_c = j_c_stars[mask]

                binned_j_zonc, _ = np.histogram(j_z / j_c, bins=j_zonc_edges)

                j_zoncs[i, j] = binned_j_zonc * MassStar

            # for j in range(n_dim):
            #     mask = np.logical_and(R > bin_edges[2 * j], R < bin_edges[2 * j + 1])
            # 
            #     j_z = R[mask] * v_phi[mask]
            #     j_c = get_j_c(PosStars[mask], VelStars[mask], PotStars[mask], galaxy_name=galaxy_name)
            # 
            #     binned_j_zonc, _ = np.histogram(j_z / j_c, bins=j_zonc_edges)
            # 
            #     j_zoncs[i, j] = binned_j_zonc * MassStar

        # save when done with loop
        np.save(file_name, j_zoncs)

    return (j_zoncs)


def get_v_phi_evolution(galaxy_name, save_name_append, snaps, bin_edges,
                        v_phi_edges=np.linspace(-500, 500, 201), overwrite=False):
    '''
    '''

    if os.path.exists('../galaxies/' + galaxy_name + '_seed4'):
        max_seed = 5
        if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

        x_list = []
        for i in range(max_seed):
            galaxy_name_i = galaxy_name + '_seed' + str(i)
            x = get_v_phi_evolution(galaxy_name_i, save_name_append, snaps, bin_edges, v_phi_edges, overwrite)

            x_list.append(x)

        x_list = np.array(x_list)

        # x = np.median(x_list, axis=0)
        x = np.mean(x_list, axis=0)

        return (x)

    if galaxy_name[-6:] == '_seed0':
        galaxy_name = galaxy_name[:-6]
    file_name = '../galaxies/' + galaxy_name + '/v_phi_evolution_' + save_name_append
    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        v_phis = np.load(file_name + '.npy')

    else:
        print('Calculating v_phi')
        print(galaxy_name)
        print(bin_edges)

        # set up arrays
        n_dim = len(bin_edges) // 2

        # initialize
        v_phis = np.zeros((snaps + 1, n_dim, len(v_phi_edges) - 1))

        for i in range(snaps + 1):

            snap = '{0:03d}'.format(int(i))
            # read snap
            (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_nbody.load_and_align(galaxy_name, snap)

            # calculate
            (R, phi, z, v_R, v_phi, v_z
             ) = get_cylindrical(PosStars, VelStars)

            for j in range(n_dim):
                mask = np.logical_and(R > bin_edges[2 * j], R < bin_edges[2 * j + 1])

                binned_v_phi, _ = np.histogram(v_phi[mask], bins=v_phi_edges)

                v_phis[i, j] = binned_v_phi * MassStar

        # save when done with loop
        np.save(file_name, v_phis)

    return (v_phis)


def get_v_z_evolution(galaxy_name, save_name_append, snaps, bin_edges,
                      v_z_edges=np.linspace(-500, 500, 201), overwrite=False):
    '''
    '''
    file_name = '../galaxies/' + galaxy_name + '/v_z_evolution_' + save_name_append
    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        v_zs = np.load(file_name + '.npy')

    else:
        print('Calculating v_z')
        print(galaxy_name)
        print(bin_edges)

        # set up arrays
        n_dim = len(bin_edges) // 2

        # initialize
        v_zs = np.zeros((snaps + 1, n_dim, len(v_z_edges) - 1))

        for i in range(snaps + 1):

            snap = '{0:03d}'.format(int(i))
            # read snap
            (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_nbody.load_and_align(galaxy_name, snap)

            # calculate
            (R, phi, z, v_R, v_phi, v_z
             ) = get_cylindrical(PosStars, VelStars)

            for j in range(n_dim):
                mask = np.logical_and(R > bin_edges[2 * j], R < bin_edges[2 * j + 1])

                binned_v_z, _ = np.histogram(v_z[mask], bins=v_z_edges)

                v_zs[i, j] = binned_v_z * MassStar

        # save when done with loop
        np.save(file_name, v_zs)

    return (v_zs)


def get_v_R_evolution(galaxy_name, save_name_append, snaps, bin_edges,
                      v_R_edges=np.linspace(-500, 500, 201), overwrite=False):
    '''
    '''
    file_name = '../galaxies/' + galaxy_name + '/v_R_evolution_' + save_name_append
    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        v_Rs = np.load(file_name + '.npy')

    else:
        print('Calculating v_R')
        print(galaxy_name)
        print(bin_edges)

        # set up arrays
        n_dim = len(bin_edges) // 2

        # initialize
        v_Rs = np.zeros((snaps + 1, n_dim, len(v_R_edges) - 1))

        for i in range(snaps + 1):

            snap = '{0:03d}'.format(int(i))
            # read snap
            (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_nbody.load_and_align(galaxy_name, snap)

            # calculate
            (R, phi, z, v_R, v_phi, v_z
             ) = get_cylindrical(PosStars, VelStars)

            for j in range(n_dim):
                mask = np.logical_and(R > bin_edges[2 * j], R < bin_edges[2 * j + 1])

                binned_v_R, _ = np.histogram(v_R[mask], bins=v_R_edges)

                v_Rs[i, j] = binned_v_R * MassStar

        # save when done with loop
        np.save(file_name, v_Rs)

    return (v_Rs)


def get_exponential_fit(galaxy_name, snaps, overwrite=False):
    '''
    '''
    if os.path.exists('../galaxies/' + galaxy_name + '_seed4'):
        max_seed = 5
        if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

        x_list = []
        for l in range(max_seed):
            galaxy_name_i = galaxy_name + '_seed' + str(l)
            x = get_exponential_fit(galaxy_name_i, snaps, overwrite)

            x_list.append(x)

        x_list = np.array(x_list)

        # x = np.median(x_list, axis=0)
        x = np.mean(x_list, axis=0)

        return x
    
    file_name = '../galaxies/' + galaxy_name + '/exponential_fit'
    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        R_ds = np.load(file_name + '.npy')

    else:
        print('Calculating exponential fit')
        print(galaxy_name)

        # initialize
        R_ds = np.zeros(snaps + 1)

        for i in range(snaps + 1):
            snap = '{0:03d}'.format(int(i))
            # read snap
            (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_nbody.load_and_align(galaxy_name, snap)

            R = np.sqrt(PosStars[:, 0] ** 2 + PosStars[:, 1] ** 2)

            # work out best fit
            likely = lambda R_d: -np.sum(np.log(R / np.square(R_d) * np.exp(-R / R_d)))

            R_d = minimize(likely, 4.15)

            R_ds[i] = R_d.x[0]

        # save when done with loop
        np.save(file_name, R_ds)

    return R_ds


def get_stacked_exponential_fit(galaxy_name, snaps, overwrite=False):
    '''
    '''
    file_name = '../galaxies/' + galaxy_name + '/stacked_exponential_fit'
    if galaxy_name[-6:] == '_seed0':
        file_name = '../galaxies/' + galaxy_name[:-6] + '/stacked_exponential_fit' + '_seed0'
    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        R_ds = np.load(file_name + '.npy')

    else:
        print('Calculating stacked exponential fit')
        print(galaxy_name)

        # initialize
        R_ds = np.zeros(snaps + 1)

        max_seed = 1
        if os.path.exists('../galaxies/' + galaxy_name + '_seed4'): max_seed = 5
        if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

        for i in range(snaps + 1):
            snap = '{0:03d}'.format(int(i))

            # read snap
            pos_list = []
            for l in range(max_seed):
                galaxy_name_i = galaxy_name

                if galaxy_name_i[-6:] == '_seed0':
                    galaxy_name_i = galaxy_name[:-6]
                
                if l != 0:
                    galaxy_name_i += '_seed' + str(l)

                (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
                 MassStar, PosStars, VelStars, IDStars, PotStars
                 ) = load_nbody.load_and_align(galaxy_name_i, snap)

                pos_list.append(PosStars)
            
            #stack
            PosStars = np.vstack(pos_list)

            R = np.sqrt(PosStars[:, 0] ** 2 + PosStars[:, 1] ** 2)

            # work out best fit
            likely = lambda R_d: -np.sum(np.log(R / np.square(R_d) * np.exp(-R / R_d)))

            R_d = minimize(likely, 4.15)

            R_ds[i] = R_d.x[0]

        # save when done with loop
        np.save(file_name, R_ds)

    return R_ds


def get_total_angular_momentum(galaxy_name, save_name_append, snaps,
                               quarter=False, five=False, all=True, overwrite=False):
    '''Specific Angular Momentum
    '''
    if os.path.exists('../galaxies/' + galaxy_name + '_seed4'):
        max_seed = 5
        if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

        j_z_list = []
        for i in range(max_seed):
            j_z = get_total_angular_momentum(galaxy_name + '_seed' + str(i), save_name_append=save_name_append,
                                             snaps=snaps,
                                             quarter=quarter, five=five, all=all, overwrite=overwrite)
            j_z_list.append(j_z)

        j_z_list = np.array(j_z_list)

        # print(j_z_list[:,0] /  np.mean(j_z_list[:,0]))
        # print(j_z_list[:,400] /  np.mean(j_z_list[:,400]))

        j_z = np.median(j_z_list, axis=0)

        return (j_z)

    if galaxy_name[-6:] == '_seed0':
        galaxy_name = galaxy_name[:-6]
    # no extra seeds
    file_name = '../galaxies/' + galaxy_name + '/angular_momentum_'
    if quarter:
        file_name += 'q_'
    if five:
        file_name += '5_'
    if all:
        file_name += 'all'
    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        j_z = np.load(file_name + '.npy')

    else:
        if quarter or five:
            raise NotImplementedError
        
        print('Calculating Angular Momentum')
        print(galaxy_name)
        print(file_name)

        # initialize
        j_z = np.zeros(snaps + 1)

        for i in range(snaps + 1):

            snap = '{0:03d}'.format(int(i))
            # read snap
            (_, _, _, _, _, _,
             MassStar, PosStars, VelStars, _, _
             ) = load_nbody.load_and_align(galaxy_name, snap)

            # #calculate
            # (R, phi, z, v_R, v_phi, v_z
            #  ) = get_cylindrical(PosStars, VelStars)

            # r = np.linalg.norm(PosStars, axis=1)
            # 
            # # r_half = np.median(r)
            # # r_qaurter = np.quantile(r, 0.25)
            # r_half = get_bin_edges('cum')[3]
            # r_qaurter = get_bin_edges('cum')[1]
            # 
            # if not quarter and not five:
            #     mask = (r < r_half)
            # if quarter and not five:
            #     mask = np.logical_and(r > r_qaurter, r < r_half)
            # if not quarter and five:
            #     mask = (r < 5 * r_half)
            # if quarter and five:
            #     mask = np.logical_and(r > r_qaurter, r < 5 * r_half)
            # if all:
            #     mask = np.ones(len(r), dtype=bool)
            # mask = np.ones(len(IDStars), dtype=bool)

            # j_z[i] = (np.sum(
            #     MassStar * np.cross(PosStars[mask, :], VelStars[mask, :])[:, 2]) /
            #           (MassStar * np.sum(mask)))
            # j_z[i] = np.sum(np.cross(PosStars[mask, :], VelStars[mask, :])[:, 2]) / len(mask)
            j_z[i] = np.sum(np.cross(PosStars, VelStars)[:, 2]) / len(IDStars)

            # j_z[i] = np.sum(MassStar * R[mask] * v_phi[mask]) / (MassStar * np.sum(mask))

        # save when done with loop
        np.save(file_name, j_z)

    return (j_z)


def get_dm_total_angular_momentum(galaxy_name, save_name_append, snaps,
                               quarter=False, five=False, all=True, overwrite=False):
    '''Specific Angular Momentum
    '''
    if os.path.exists('../galaxies/' + galaxy_name + '_seed4'):
        max_seed = 5
        if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

        j_z_list = []
        for i in range(max_seed):
            j_z = get_dm_total_angular_momentum(galaxy_name + '_seed' + str(i), save_name_append=save_name_append,
                                             snaps=snaps,
                                             quarter=quarter, five=five, all=all, overwrite=overwrite)
            j_z_list.append(j_z)

        j_z_list = np.array(j_z_list)

        # print(j_z_list[:,0] /  np.mean(j_z_list[:,0]))
        # print(j_z_list[:,400] /  np.mean(j_z_list[:,400]))

        j_z = np.median(j_z_list, axis=0)

        return (j_z)

    if galaxy_name[-6:] == '_seed0':
        galaxy_name = galaxy_name[:-6]
    # no extra seeds
    file_name = '../galaxies/' + galaxy_name + '/angular_momentum_DM_'
    if quarter:
        file_name += 'q_'
    if five:
        file_name += '5_'
    if all:
        file_name += 'all'
    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        j_z = np.load(file_name + '.npy')

    else:
        print('Calculating DM Angular Momentum')
        print(galaxy_name)
        print(file_name)

        # initialize
        j_z = np.zeros(snaps + 1)

        for i in range(snaps + 1):

            snap = '{0:03d}'.format(int(i))
            # read snap

            (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_nbody.load_and_align(galaxy_name, snap)

            # #calculate
            # (R, phi, z, v_R, v_phi, v_z
            #  ) = get_cylindrical(PosStars, VelStars)

            # r = np.linalg.norm(PosDMs, axis=1)

            # # r_half = np.median(r)
            # # r_qaurter = np.quantile(r, 0.25)
            # r_half = get_bin_edges('cum')[3]
            # r_qaurter = get_bin_edges('cum')[1]
            # 
            # if not quarter and not five:
            #     mask = (r < r_half)
            # if quarter and not five:
            #     mask = np.logical_and(r > r_qaurter, r < r_half)
            # if not quarter and five:
            #     mask = (r < 5 * r_half)
            # if quarter and five:
            #     mask = np.logical_and(r > r_qaurter, r < 5 * r_half)
            # if all:
            #     mask = np.ones(len(r), dtype=bool)
            # mask = np.ones(len(IDDMs), dtype=bool)

            # j_z[i] = (np.sum(
            #     MassDM * np.cross(PosDMs[mask, :], VelDMs[mask, :])[:, 2]) /
            #           (MassDM * np.sum(mask)))
            j_z[i] = np.sum(np.cross(PosDMs, VelDMs)[:, 2]) / len(IDDMs)

            # j_z[i] = np.sum(MassStar * R[mask] * v_phi[mask]) / (MassStar * np.sum(mask))

        # save when done with loop
        np.save(file_name, j_z)

    return (j_z)


def get_vc_from_M_at_R_half(galaxy_name, save_name_append, snaps, overwrite=False):
    if os.path.exists('../galaxies/' + galaxy_name + '_seed4'):
        max_seed = 5
        if os.path.exists('../galaxies/' + galaxy_name + '_seed9'): max_seed = 10

        x_list = []
        for i in range(max_seed):
            galaxy_name_i = galaxy_name + '_seed' + str(i)
            x = get_vc_from_M_at_R_half(galaxy_name_i, save_name_append, snaps, overwrite)

            x_list.append(x)

        x_list = np.array(x_list)

        x = np.median(x_list, axis=0)

        return (x)

    if galaxy_name[-6:] == '_seed0':
        galaxy_name = galaxy_name[:-6]
    file_name = '../galaxies/' + galaxy_name + '/vc_from_profile' + save_name_append
    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        v_circ = np.load(file_name + '.npy')

    else:
        print('Calculating v circ half')
        print(galaxy_name)

        # set up arrays
        v_circ = np.zeros(snaps + 1)

        for i in range(snaps + 1):
            # print(i)
            # load a snap
            (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_nbody.load_and_align(galaxy_name, snap='{0:03d}'.format(int(i)))

            R = np.sqrt(PosStars[:, 0] ** 2 + PosStars[:, 1] ** 2)

            R_half = np.median(R)

            rDMs = np.linalg.norm(PosDMs, axis=1)
            rStars = np.linalg.norm(PosStars, axis=1)

            f = lambda r: - (MassDM * np.sum(rDMs < r) + MassStar * np.sum(rStars < r)) / r
            v_c_r_half = np.sqrt(-GRAV_CONST * f(R_half))

            v_circ[i] = v_c_r_half

        # save when done with loop
        np.save(file_name, v_circ)

    return (v_circ)


###############################################################################
def get_normalized_image(image, vmin=None, vmax=None):
    if (vmin == None):
        vmin = np.min(image)
    if (vmax == None):
        vmax = np.max(image)

    image = np.clip(image, vmin, vmax)
    image = (image - vmin) / (vmax - vmin)

    return image

def plot_projected_evolutions(save=False, name=''):
    # galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps']
    # galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps']
    # snaps_list = [101,101,400]

    # galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps']

    # galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps']

    # galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_5p0_V200-100kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_5p5_V200-100kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_6p0_V200-100kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_6p5_V200-100kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p0_V200-100kmps']
    # name += '_low'

    galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_7p0_V200-400kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p5_V200-400kmps',
                        'mu_5/fdisk_0p01_lgMdm_8p0_V200-400kmps',
                        'mu_5/fdisk_0p01_lgMdm_8p5_V200-400kmps',
                        'mu_5/fdisk_0p01_lgMdm_9p0_V200-400kmps']
    name += '_hi'

    snaps_list = [101, 101, 101, 400, 400]
    # snaps_list = [101, 400]

    # times_list = [0, 1, 3, 9]
    times_list = [0, 1, 2, 4, 8]
    # times_list = [0, 9.8]

    n_gals = len(galaxy_name_list)
    n_times = len(times_list)

    fig = plt.figure(constrained_layout=True)
    # fig.set_size_inches(4*n_gals+0.67, 4*n_times+06, forward=True)
    fig.set_size_inches(3 * n_gals + 0.4, 3 * n_times + 0.4, forward=True)
    widths = [1] * n_gals
    heights = [1] * n_times
    spec = fig.add_gridspec(ncols=n_gals, nrows=n_times, width_ratios=widths, height_ratios=heights,
                            wspace=0.01, hspace=0.005)

    # extent = [-0.0125,0.0125,-0.0125,0.0125]
    extent = [-25, 25, -25, 25]
    if '_low' in name:
        extent = [-12.5, 12.5, -12.5, 12.5]  # extent * 100/200
    elif '_hi' in name:
        extent = [-50, 50, -50, 50]  # extent * 400/200

    axs = []
    for row in range(len(heights)):
        axs.append([])
        for col in range(len(widths)):
            axs[row].append(fig.add_subplot(spec[row, col], frameon=False))
            axs[row][col].set_xlim(extent[:2])
            axs[row][col].set_ylim(extent[2:])
            if '_low' in name:
                # axs[row][col].set_yticks([-10, 0, 10])
                axs[row][col].set_yticks([-10, -5, 0, 5, 10])
            elif '_hi' in name:
                axs[row][col].set_yticks([-40, -20, 0, 20, 40])
                # axs[row][col].set_yticks([-40, -30, -20, -10, 0, 10, 20, 30, 40])
                # axs[row][col].set_yticklabels([-40, None, -20, None, 0, None, 20, None, 40])
                # axs[row][col].set_yticks([-40, -35, -30, -25, -20, -15, -10, -5, 0, 5, 10, 15, 20, 25, 30, 35, 40])
                # axs[row][col].set_yticklabels([-40, None, None, None, -20, None, None, None, 0,
                #                                None, None, None, 20, None, None, None, 40])
            else:
                # axs[row][col].set_yticks([-20, 0, 20])
                axs[row][col].set_yticks([-20, -10, 0, 10, 20])
            if '_low' in name:
                axs[row][col].set_xticks([-10, -5, 0, 5, 10])
            elif '_hi' in name:
                axs[row][col].set_xticks([-40, -20, 0, 20, 40])
            else:
                axs[row][col].set_xticks([-20, -10, 0, 10, 20])

            if col != 0: axs[row][col].set_yticklabels([])
            if row != len(heights) - 1: axs[row][col].set_xticklabels([])

            axs[row][col].tick_params(color='white')
            # axs[row][col].figure.set_edgecolor('white')

    plt.subplots_adjust(wspace=0.00, hspace=0.00)

    axs[0][n_gals // 2].set_title(r'$V_{200} = 200$ km/s, M$_{200} = 1.86 \times 10^{12}$ M$_\odot$')
    if '_low' in name:
        axs[0][n_gals // 2].set_title(r'$V_{200} = 100$ km/s, M$_{200} = 2.32 \times 10^{11}$ M$_\odot$')
    elif '_hi' in name:
        axs[0][n_gals // 2].set_title(r'$V_{200} = 400$ km/s, M$_{200} = 1.49 \times 10^{13}$ M$_\odot$')

    V200 = 200
    if '_low' in name:
        V200 = 100
    elif '_hi' in name:
        V200 = 400

    min_hsml = 3e-1 * V200 / 200
    max_hsml = 3e-1 * V200 / 200
    dm_min_hsml = 6e-1 * V200 / 200
    dm_max_hsml = 6e-1 * V200 / 200
    # # dm_c_max = 1.8
    dm_c_max = 2.5

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        galaxy_times = np.linspace(0, T_TOT, snaps)
        kwargs = get_name_kwargs(galaxy_name)

        for j, t in enumerate(times_list):

            snap = np.argmin(np.abs(galaxy_times - t))
            # print(snap)
            snap = '{0:03d}'.format(snap)

            # read a file
            (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_nbody.load_and_align(galaxy_name, snap)
            N200 = len(PotDMs)  # + len(PotStars)
            # print(MassDM * len(PotDMs) + MassStar * len(PotStars))

            cmap_star = matplotlib.cm.Greys_r
            cmap_dm = matplotlib.cm.Blues_r
            
            xsize=300 #500 #is default
            qv_star = QuickView(PosStars, mass=V200 / 200 * np.ones(len(PosStars)), r='infinity', plot=False,
                                logscale=True, min_hsml=min_hsml, max_hsml=max_hsml,
                                x=0, y=0, z=0, extent=extent, t=90, origin='lower', xsize=xsize, ysize=xsize)
            qv_dm = QuickView(PosDMs, mass=V200 / 200 * np.ones(len(PosDMs)), r='infinity', plot=False,
                              logscale=True, min_hsml=dm_min_hsml, max_hsml=dm_max_hsml,
                              x=0, y=0, z=0, extent=extent, t=90, origin='lower', xsize=xsize, ysize=xsize)

            img_star = qv_star.get_image()
            img_dm = qv_dm.get_image()

            rgb_star = cmap_star(get_normalized_image(img_star))
            rgb_dm = cmap_dm(get_normalized_image(img_dm, vmax=dm_c_max * np.max(img_dm)))

            blend = Blend.Blend(rgb_star, rgb_dm)
            rgb_output = blend.Screen()

            # plot
            axs[j][i].imshow(rgb_output, extent=qv_star.get_extent())

            if j == 0:
                axs[j][i].text(0.90 * extent[0], 0.90 * extent[1], r'$m_{\mathrm{DM}} = $' + kwargs['lab'],
                               ha='left', va='top', c='white')
            if j == 1:
                axs[j][i].text(0.90 * extent[0], 0.90 * extent[1],
                               r'$\mathrm{N}_{\mathrm{DM}} = $' + format(N200, '.1E')[:3] + r'$\times$'
                               + f"$10^{format(N200, '.1E')[6:]}$",
                               ha='left', va='top', c='white')
            if i == 4:
                axs[j][i].text(0.90 * extent[1], 0.90 * extent[0], r'$t =$' + str(t) + ' Gyr',
                               ha='right', va='bottom', c='white')

            print(t, galaxy_name)
            z_half = np.median(np.abs(PosStars[:, 2]))
            R_half = np.median(np.sqrt(PosStars[:, 0]**2 + PosStars[:, 1]**2))
            print('z half', z_half)
            print('R half', R_half)
            print('z/R', z_half/R_half)
            print()

    axs[n_times - 1][n_gals // 2].set_xlabel(r'$x$ [kpc]')
    axs[n_times // 2][0].set_ylabel(r'$z$ [kpc]')

    if save:
        plt.savefig(name + '.pdf', bbox_inches='tight', dpi=80)
        plt.close()

    return


def plot_poster_fg_projected_evolutions(save=False, name=''):

    galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps',
                        #'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps']

    snaps_list = [101, 101, 400]

    times_list = [0, 5]

    aspect_ratio = 3/4

    n_gals = len(galaxy_name_list)
    n_times = len(times_list)

    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(4 / aspect_ratio * n_gals + 0.0, 4 * n_times + 0.07, forward=True)
    widths = [1] * n_gals
    heights = [aspect_ratio] * n_times
    spec = fig.add_gridspec(ncols=n_gals, nrows=n_times, width_ratios=widths, height_ratios=heights,
                            wspace=0.00, hspace=0.00)

    # extent = [-0.0125,0.0125,-0.0125,0.0125]
    extent = [-25, 25, -25 * aspect_ratio, 25 * aspect_ratio]

    axs = []
    for row in range(len(heights)):
        axs.append([])
        for col in range(len(widths)):
            axs[row].append(fig.add_subplot(spec[row, col]))
            axs[row][col].set_xlim(extent[:2])
            axs[row][col].set_ylim(extent[2:])
            axs[row][col].set_xticklabels([])
            axs[row][col].set_yticklabels([])
            axs[row][col].set_xticks([-20, -10, 0, 10, 20])
            axs[row][col].set_yticks([-20, -10, 0, 10, 20])

    plt.subplots_adjust(wspace=0.00, hspace=0.00)

    V200 = 200
    if '_low' in name:
        V200 = 100
    elif '_hi' in name:
        V200 = 400

    min_hsml = 3e-1 * V200 / 200
    max_hsml = 3e-1 * V200 / 200
    dm_min_hsml = 1e0 * V200 / 200
    dm_max_hsml = 1e0 * V200 / 200
    # # dm_c_max = 1.8
    dm_c_max = 2.5

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        galaxy_times = np.linspace(0, T_TOT, snaps)
        kwargs = get_name_kwargs(galaxy_name)

        for j, t in enumerate(times_list):

            snap = np.argmin(np.abs(galaxy_times - t))
            # print(snap)
            snap = '{0:03d}'.format(snap)

            # read a file
            (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_nbody.load_and_align(galaxy_name, snap)
            N200 = len(PotDMs)  # + len(PotStars)
            # print(MassDM * len(PotDMs) + MassStar * len(PotStars))

            cmap_star = matplotlib.cm.Greys_r
            # cmap_dm = matplotlib.cm.Blues_r
            cmap_dm = matplotlib.cm.YlOrBr_r
            # cmap_dm = matplotlib.cm.inferno
            # cmap_dm = matplotlib.cm.Reds_r

            xsize = 500

            qv_star = QuickView(PosStars, mass=V200 / 200 * np.ones(len(PosStars)), r='infinity', plot=False,
                                logscale=True, min_hsml=min_hsml, max_hsml=max_hsml,
                                x=0, y=0, z=0, extent=extent, xsize=xsize, ysize=int(xsize * aspect_ratio),
                                t=90, origin='lower')
            qv_dm = QuickView(PosDMs, mass=V200 / 200 * np.ones(len(PosDMs)), r='infinity', plot=False,
                              logscale=True, min_hsml=dm_min_hsml, max_hsml=dm_max_hsml,
                              x=0, y=0, z=0, extent=extent, xsize=xsize, ysize=int(xsize * aspect_ratio),
                              t=90, origin='lower')

            img_star = qv_star.get_image()
            img_dm = qv_dm.get_image()

            rgb_star = cmap_star(get_normalized_image(img_star))
            rgb_dm = cmap_dm(get_normalized_image(img_dm, vmax=dm_c_max * np.max(img_dm)))

            blend = Blend.Blend(rgb_star, rgb_dm)
            rgb_output = blend.Screen()

            # plot
            axs[j][i].imshow(rgb_output, extent=qv_star.get_extent())

            if j == 0:
                axs[j][i].text(0.95 * extent[0], 0.95 * extent[3],
                               r'$\mathrm{N}_{\mathrm{DM}} = $' + format(N200, '.1E')[:3] + r'$\times$'
                               + f"$10^{format(N200, '.1E')[6:]}$",
                               ha='left', va='top', c='white', fontsize=28)
                axs[j][i].text(0.95 * extent[0], 0.65 * extent[3], r'$m_{\mathrm{DM}} = $' + kwargs['lab'],
                               ha='left', va='top', c='white', fontsize=28)
            if True: #j == n_gals -1:
                axs[j][i].text(0.95 * extent[0], 0.95 * extent[2], r'$t =$' + str(t) + ' Gyr',
                               ha='left', va='bottom', c='white', fontsize=28)

    # axs[n_times - 1][n_gals // 2].set_xlabel(r'$x$ [kpc]')
    # axs[n_times // 2][0].set_ylabel(r'$z$ [kpc]')

    for row in range(len(heights)):
        for col in range(len(widths)):
            axs[row][col].set_xlim(extent[:2])
            axs[row][col].set_ylim(extent[2:])
    if save:
        plt.savefig(name, bbox_inches='tight', dpi=200)
        plt.close()

    return


def plot_poster_bg_projected_evolution(save=False, name=''):

    aspect = 16 / 9
    
    galaxy_name = 'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps'
    snap = 399
    snaps = 400

    fig = plt.figure(constrained_layout=True)

    # A1 size = 594 x 841 mm
    #         = 23.4 x 33.1 inches

    ax = plt.subplot(111)

    fig.set_size_inches(28, 28/aspect, forward=True)

    extent = [-25, 25, -25 / aspect, 25 / aspect]
    # extent = [-25, 25, -25, 25]

    ax.set_xlim(extent[:2])
    ax.set_ylim(extent[2:])
    ax.set_yticklabels([])
    ax.set_xticklabels([])

    V200 = 200

    min_hsml = 2e-1 * V200 / 200
    max_hsml = 2e-1 * V200 / 200
    dm_min_hsml = 0.8e0 * V200 / 200
    dm_max_hsml = 1e0 * V200 / 200

    star_c_max = 0.5 #1.4
    dm_c_max = 6 #4.2 #2.8

    galaxy_times = np.linspace(0, T_TOT, 400)
    kwargs = get_name_kwargs(galaxy_name)

    snap = '{0:03d}'.format(snap)

    # read a file
    (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
     MassStar, PosStars, VelStars, IDStars, PotStars
     ) = load_nbody.load_and_align(galaxy_name, snap)

    N200 = len(PotDMs)  # + len(PotStars)
    # print(MassDM * len(PotDMs) + MassStar * len(PotStars))

    cmap_star = matplotlib.cm.Greys
    cmap_dm = matplotlib.cm.Oranges_r
    # cmap_star = matplotlib.cm.Greys_r
    # cmap_dm = matplotlib.cm.Blues_r
    # cmap_dm = matplotlib.cm.YlOrBr_r
    # cmap_dm = matplotlib.cm.get_cmap('GnBu_r')

    cmap = np.zeros((256, 3))
    cmap_s = np.zeros((256, 4))
    for i in range(256):
        cn2 = cmap_dm(i/256)[0]**2 + cmap_dm(i/256)[1]**2 + cmap_dm(i/256)[2]**2
        cmap[i] = [(cmap_dm(i/256)[0]**2*3/4 + cn2/4)**0.5,
                   (cmap_dm(i/256)[1]**2*3/4 + cn2/4)**0.5,
                   (cmap_dm(i/256)[2]**2*3/4 + cn2/4)**0.5]
                   #desaturate(cmap_dm(i/256), 1) #0.5)
        cmap_s[i] = cmap_star(i/256)
    cmap_dm = matplotlib.colors.ListedColormap(1 - cmap * 0.7)# + 0.3)
    cmap_star = matplotlib.colors.ListedColormap(0.9 - cmap_s * 0.5)# + 0.7)
    
    # cmap_dm = sns.color_palette('flare_r', as_cmap=True)

    npixx = 1000 #2000

    qv_star = QuickView(PosStars, mass=V200 / 200 * np.ones(len(PosStars)), r='infinity', plot=False,
                        logscale=True, min_hsml=min_hsml, max_hsml=max_hsml,
                        x=0, y=0, z=0, extent=extent, t=90, origin='lower',
                        xsize=npixx, ysize=int(npixx / aspect))
    qv_dm = QuickView(PosDMs, mass=V200 / 200 * np.ones(len(PosDMs)), r='infinity', plot=False,
                      logscale=True, min_hsml=dm_min_hsml, max_hsml=dm_max_hsml,
                      x=0, y=0, z=0, extent=extent, t=90, origin='lower',
                      xsize=npixx, ysize=int(npixx / aspect))

    img_star = qv_star.get_image()
    img_dm = qv_dm.get_image()

    rgb_star = cmap_star(get_normalized_image(img_star, vmax=star_c_max * np.max(img_star), vmin=1 * np.min(img_star)))
    rgb_dm = cmap_dm(get_normalized_image(img_dm, vmax=dm_c_max * np.max(img_dm), vmin=1 * np.min(img_dm)))

    # blend = Blend.Blend(rgb_star, rgb_dm)
    blend = Blend.Blend(rgb_dm, rgb_star)
    rgb_output = blend.Overlay()

    # rgb_output = blend.Screen()

    # plot
    ax.imshow(rgb_output, extent=qv_star.get_extent())

    if save:
        print(name)
        plt.savefig(name, dpi=100)#, bbox_inches='tight')
        plt.close()

    return


def hernquist_projected_intenisty(R, Mass, a_h):
    '''Proportional to this
  '''
    s = R / a_h

    I = Mass / (a_h ** 2 * (1 - s ** 2) ** 2) * ((2 + s ** 2) * hernquist_X(s) - 3)

    return (I)


def hernquist_X(s):
    # if s <= 1:
    #   X = 1 / np.sqrt(1 - s**2) * np.arccosh(1/s)
    # else:
    #   X = 1 / np.sqrt(s**2 - 1) * np.arccos(1/s)

    X_l = 1 / np.sqrt(1 - s ** 2) * np.arccosh(1 / s)
    X_h = 1 / np.sqrt(s ** 2 - 1) * np.arccos(1 / s)

    X = np.zeros(np.shape(s))
    n_l = ~np.isnan(X_l)
    n_h = ~np.isnan(X_h)
    X[n_l] = X_l[n_l]
    X[n_h] = X_h[n_h]

    return (X)


def get_name_kwargs(galaxy_name):
    '''
  '''
    if '50kmps' in galaxy_name:  V200 = 50
    if '100kmps' in galaxy_name: V200 = 100
    if '200kmps' in galaxy_name: V200 = 200
    if '300kmps' in galaxy_name: V200 = 300
    if '400kmps' in galaxy_name: V200 = 400
    
    vmarker = 'o'
    if '50kmps' in galaxy_name:  vmarker = 'o'
    if '100kmps' in galaxy_name: vmarker = 'D'
    if '200kmps' in galaxy_name: vmarker = 's'
    if '300kmps' in galaxy_name: vmarker = '^'
    if '400kmps' in galaxy_name: vmarker = 'P'

    mun = 5
    if 'mu_0p2' in galaxy_name:  mun = 0.2
    if 'mu_0p5' in galaxy_name:  mun = 0.5
    if 'mu_0p77' in galaxy_name:  mun = 0.77
    if 'mu_1' in galaxy_name:  mun = 1
    if 'mu_2' in galaxy_name:  mun = 2
    if 'mu_5' in galaxy_name:  mun = 5
    if 'mu_25' in galaxy_name: mun = 25
    
    mu = r'$\mu=5$'
    if 'mu_0p2' in galaxy_name:  mu = r'$\mu=0.2$'
    if 'mu_0p5' in galaxy_name:  mu = r'$\mu=0.5$'
    if 'mu_0p77' in galaxy_name:  mu = r'$\mu=0.77$'
    if 'mu_1' in galaxy_name:  mu = r'$\mu=1$'
    if 'mu_2' in galaxy_name:  mu = r'$\mu=2$'
    if 'mu_5' in galaxy_name:  mu = r'$\mu=5$'
    if 'mu_25' in galaxy_name: mu = r'$\mu=25$'

    # in 10^10 Msun
    if 'Mdm_4p0' in galaxy_name: mdm = 1e-6
    if 'Mdm_4p5' in galaxy_name: mdm = 10 ** (-5.5)
    if 'Mdm_5p0' in galaxy_name: mdm = 1e-5
    if 'Mdm_5p5' in galaxy_name: mdm = 10 ** (-4.5)
    if 'Mdm_6p0' in galaxy_name: mdm = 1e-4
    if 'Mdm_6_' in galaxy_name: mdm = 1e-4
    if 'Mdm_6p5' in galaxy_name: mdm = 10 ** (-3.5)
    if 'Mdm_7p0' in galaxy_name: mdm = 1e-3
    if 'Mdm_7_' in galaxy_name: mdm = 1e-3
    if 'Mdm_7p5' in galaxy_name: mdm = 10 ** (-2.5)
    if 'Mdm_8p0' in galaxy_name: mdm = 1e-2
    if 'Mdm_8_' in galaxy_name: mdm = 1e-2
    if 'Mdm_8p5' in galaxy_name: mdm = 10 ** (-1.5)
    if 'Mdm_9p0' in galaxy_name: mdm = 1e-1

    if 'Mdm_4p0' in galaxy_name: lab = r'$10^{4.0} $ M$_\odot$'
    if 'Mdm_4p5' in galaxy_name: lab = r'$10^{4.5} $ M$_\odot$'
    if 'Mdm_5p0' in galaxy_name: lab = r'$10^{5.0} $ M$_\odot$'
    if 'Mdm_5p5' in galaxy_name: lab = r'$10^{5.5} $ M$_\odot$'
    if 'Mdm_6p0' in galaxy_name: lab = r'$10^{6.0} $ M$_\odot$'
    if 'Mdm_6_' in galaxy_name: lab = r'$10^{6.0} $ M$_\odot$'
    if 'Mdm_6p5' in galaxy_name: lab = r'$10^{6.5} $ M$_\odot$'
    if 'Mdm_7p0' in galaxy_name: lab = r'$10^{7.0} $ M$_\odot$'
    if 'Mdm_7' in galaxy_name: lab = r'$10^{7.0} $ M$_\odot$'
    if 'Mdm_7p5' in galaxy_name: lab = r'$10^{7.5} $ M$_\odot$'
    if 'Mdm_8p0' in galaxy_name: lab = r'$10^{8.0} $ M$_\odot$'
    if 'Mdm_8_' in galaxy_name: lab = r'$10^{8.0} $ M$_\odot$'
    if 'Mdm_8p5' in galaxy_name: lab = r'$10^{8.5} $ M$_\odot$'
    if 'Mdm_9p0' in galaxy_name: lab = r'$10^{9.0} $ M$_\odot$'

    labstar = ''
    if '200kmps' in galaxy_name:
        if 'Mdm_6p0' in galaxy_name: labstar = r'$2 \times 10^{5.0} $ M$_\odot$'
        if 'Mdm_6p5' in galaxy_name: labstar = r'$6 \times 10^{5.0} $ M$_\odot$'
        if 'Mdm_7p0' in galaxy_name: labstar = r'$2 \times 10^{6.0} $ M$_\odot$'
        if 'Mdm_7p5' in galaxy_name: labstar = r'$6 \times 10^{6.0} $ M$_\odot$'
        if 'Mdm_8p0' in galaxy_name: labstar = r'$2 \times 10^{7.0} $ M$_\odot$'

    labn = ''
    if '50kmps' in galaxy_name:
        if 'Mdm_4p0' in galaxy_name: labn = r'$2.9 \times 10^{6}$'
        if 'Mdm_4p5' in galaxy_name: labn = r'$9.1 \times 10^{5}$'
        if 'Mdm_5p0' in galaxy_name: labn = r'$2.9 \times 10^{5}$'
        if 'Mdm_5p5' in galaxy_name: labn = r'$9.1 \times 10^{4}$'
        if 'Mdm_6p0' in galaxy_name: labn = r'$2.9 \times 10^{4}$'
    if '100kmps' in galaxy_name:
        if 'Mdm_5p0' in galaxy_name: labn = r'$2.3 \times 10^{6}$'
        if 'Mdm_5p5' in galaxy_name: labn = r'$7.3 \times 10^{5}$'
        if 'Mdm_6p0' in galaxy_name: labn = r'$2.3 \times 10^{5}$'
        if 'Mdm_6p5' in galaxy_name: labn = r'$7.3 \times 10^{4}$'
        if 'Mdm_7p0' in galaxy_name: labn = r'$2.3 \times 10^{4}$'
    if '200kmps' in galaxy_name:
        if 'Mdm_6p0' in galaxy_name: labn = r'$1.8 \times 10^{6}$'
        if 'Mdm_6p5' in galaxy_name: labn = r'$5.8 \times 10^{5}$'
        if 'Mdm_7p0' in galaxy_name: labn = r'$1.8 \times 10^{5}$'
        if 'Mdm_7p5' in galaxy_name: labn = r'$5.8 \times 10^{4}$'
        if 'Mdm_8p0' in galaxy_name: labn = r'$1.8 \times 10^{4}$'
    if '400kmps' in galaxy_name:
        if 'Mdm_7p0' in galaxy_name: labn = r'$1.5 \times 10^{6}$'
        if 'Mdm_7p5' in galaxy_name: labn = r'$4.7 \times 10^{5}$'
        if 'Mdm_8p0' in galaxy_name: labn = r'$1.5 \times 10^{5}$'
        if 'Mdm_8p5' in galaxy_name: labn = r'$4.7 \times 10^{4}$'
        if 'Mdm_9p0' in galaxy_name: labn = r'$1.5 \times 10^{4}$'

    labnstar = ''
    if 'mu_1' in galaxy_name:
        if 'Mdm_7p5' in galaxy_name: labnstar = r'$590$'
        if 'Mdm_8p0' in galaxy_name: labnstar = r'$180$'
    if 'mu_5' in galaxy_name:
        if 'Mdm_6p0' in galaxy_name: labnstar = r'$9.0 \times 10^{4}$'
        if 'Mdm_6p5' in galaxy_name: labnstar = r'$2.9 \times 10^{4}$'
        if 'Mdm_7p0' in galaxy_name: labnstar = r'$9000$'
        if 'Mdm_7p5' in galaxy_name: labnstar = r'$2900$'
        if 'Mdm_8p0' in galaxy_name: labnstar = r'$900$'

    conc = 10
    if 'c_4' in galaxy_name: conc = 4
    if 'c_7' in galaxy_name: conc = 7
    if 'c_15' in galaxy_name: conc = 15
    if 'c_25' in galaxy_name: conc = 25

    scale_height = 1
    if '2z0' in galaxy_name: scale_height = 2
    if '4z0' in galaxy_name: scale_height = 4

    # plot qunatities
    ls = '-'
    if 'mu_1' in galaxy_name:  ls = ':'
    if 'mu_5' in galaxy_name:  ls = '-'
    if 'mu_25' in galaxy_name: ls = '--'
    if 'smooth' in galaxy_name: ls = '-.'

    lsv = '-'
    if '50kmps' in galaxy_name:  lsv = ':'
    if '100kmps' in galaxy_name: lsv = '--'
    if '200kmps' in galaxy_name: lsv = '-'
    if '400kmps' in galaxy_name: lsv = '-.'
    
    marker='o'
    if 'mu_1' in galaxy_name:  marker = 's'
    if 'mu_5' in galaxy_name:  marker = 'o'
    if 'mu_25' in galaxy_name: marker = 'h'

    col ='C0'
    if 'Mdm_4p0' in galaxy_name: col = 'C8'
    if 'Mdm_4p5' in galaxy_name: col = 'C7'
    if 'Mdm_5p0' in galaxy_name: col = 'C6'
    if 'Mdm_5p5' in galaxy_name: col = 'C5'
    if 'Mdm_6p0' in galaxy_name: col = 'C4'
    if 'Mdm_6p5' in galaxy_name: col = 'C3'
    if 'Mdm_7p0' in galaxy_name: col = 'C2'
    if 'Mdm_7p5' in galaxy_name: col = 'C1'
    if 'Mdm_8p0' in galaxy_name: col = 'C0'
    if 'Mdm_8p5' in galaxy_name: col = 'C9'
    if 'Mdm_9p0' in galaxy_name: col = 'C8'

    if '50kmps' in galaxy_name:
        if 'Mdm_4p0' in galaxy_name: col = 'C4'
        if 'Mdm_4p5' in galaxy_name: col = 'C3'
        if 'Mdm_5p0' in galaxy_name: col = 'C2'
        if 'Mdm_5p5' in galaxy_name: col = 'C1'
        if 'Mdm_6p0' in galaxy_name: col = 'C0'
    if '100kmps' in galaxy_name:
        if 'Mdm_5p0' in galaxy_name: col = 'C4'
        if 'Mdm_5p5' in galaxy_name: col = 'C3'
        if 'Mdm_6p0' in galaxy_name: col = 'C2'
        if 'Mdm_6p5' in galaxy_name: col = 'C1'
        if 'Mdm_7p0' in galaxy_name: col = 'C0'
    if '200kmps' in galaxy_name:
        if 'Mdm_6p0' in galaxy_name: col = 'C4'
        if 'Mdm_6p5' in galaxy_name: col = 'C3'
        if 'Mdm_7p0' in galaxy_name: col = 'C2'
        if 'Mdm_7p5' in galaxy_name: col = 'C1'
        if 'Mdm_8p0' in galaxy_name: col = 'C0'
    if '400kmps' in galaxy_name:
        if 'Mdm_7p0' in galaxy_name: col = 'C4'
        if 'Mdm_7p5' in galaxy_name: col = 'C3'
        if 'Mdm_8p0' in galaxy_name: col = 'C2'
        if 'Mdm_8p5' in galaxy_name: col = 'C1'
        if 'Mdm_9p0' in galaxy_name: col = 'C0'

    if 'eps' in galaxy_name:
        eps = 1
        if '128soft' in galaxy_name: eps = 128
        elif '96soft' in galaxy_name: eps = 96
        elif '64soft' in galaxy_name: eps = 64
        elif '48soft' in galaxy_name: eps = 48
        elif '32soft' in galaxy_name: eps = 32
        elif '24soft' in galaxy_name: eps = 24
        elif '20soft' in galaxy_name: eps = 20
        elif '16soft' in galaxy_name: eps = 16
        elif '8soft' in galaxy_name: eps = 8
        elif '4soft' in galaxy_name: eps = 4
        elif '2soft' in galaxy_name: eps = 2
        elif '1soft' in galaxy_name: eps = 1
        elif '0p5soft' in galaxy_name: eps = 0.5
        elif '0p25soft' in galaxy_name: eps = 0.25
        col = matplotlib.cm.get_cmap('turbo')(((np.log(eps) / np.log(2)) - 3 + 0.3) / 4)

    if 'Mdm_7p0' in galaxy_name and 'c_7' in galaxy_name: col = 'lime'
    if 'Mdm_8p0' in galaxy_name and 'c_7' in galaxy_name: col = 'cyan'
    if 'Mdm_7p0' in galaxy_name and 'c_15' in galaxy_name: col = 'darkolivegreen'
    if 'Mdm_8p0' in galaxy_name and 'c_15' in galaxy_name: col = 'navy'

    if 'Mdm_4p0' in galaxy_name: cmap = matplotlib.cm.get_cmap('Purples')
    if 'Mdm_4p5' in galaxy_name: cmap = matplotlib.cm.get_cmap('Purples')
    if 'Mdm_5p0' in galaxy_name: cmap = matplotlib.cm.get_cmap('Purples')
    if 'Mdm_5p5' in galaxy_name: cmap = matplotlib.cm.get_cmap('Purples')
    # if 'Mdm_6p0' in galaxy_name: cmap = matplotlib.cm.get_cmap('Purples')
    # if 'Mdm_6p5' in galaxy_name: cmap = matplotlib.cm.get_cmap('Reds')
    # if 'Mdm_7p0' in galaxy_name: cmap = matplotlib.cm.get_cmap('Greens')
    # if 'Mdm_7p5' in galaxy_name: cmap = matplotlib.cm.get_cmap('Oranges')
    # if 'Mdm_8p0' in galaxy_name: cmap = matplotlib.cm.get_cmap('Blues')
    if 'Mdm_8p5' in galaxy_name: cmap = matplotlib.cm.get_cmap('Blues')
    if 'Mdm_9p0' in galaxy_name: cmap = matplotlib.cm.get_cmap('Blues')

    # colors = np.array([matplotlib.colors.to_rgba(col)] * 256)
    # colors[:, :3] = colors[:, :3] * np.column_stack([np.linspace(0, 1, 256)] * 3)
    # colors[:, :3] = 1 - (1-colors[:, :3]) * np.column_stack([np.linspace(0, 1, 256)] * 3)

    colors = np.array([matplotlib.colors.to_rgba(col)] * 256)
    # colors[:, :3] = colors[:, :3] / np.linalg.norm(colors[0, :3])

    colors[:128, :3] = 1 - (1-colors[:128, :3]) * np.column_stack([np.linspace(0, 1, 128)] * 3)
    colors[128:, :3] = colors[128:, :3] * np.column_stack([np.linspace(1, 0, 128)] * 3)

    cmap = matplotlib.colors.ListedColormap(colors)

    # if 'Mdm_6p0' in galaxy_name: cmap = matplotlib.cm.get_cmap('Purples')
    # if 'Mdm_6p5' in galaxy_name: cmap = matplotlib.cm.get_cmap('Reds')
    # if 'Mdm_7p0' in galaxy_name: cmap = matplotlib.cm.get_cmap('Greens')
    # if 'Mdm_7p5' in galaxy_name: cmap = matplotlib.cm.get_cmap('Oranges')
    # if 'Mdm_8p0' in galaxy_name: cmap = matplotlib.cm.get_cmap('Blues')

    yh = 0
    if 'Mdm_6p0' in galaxy_name: yh = -1.2
    if 'Mdm_6p5' in galaxy_name: yh = -1.0
    if 'Mdm_7p0' in galaxy_name: yh = -0.8
    if 'Mdm_7p5' in galaxy_name: yh = -0.6
    if 'Mdm_8p0' in galaxy_name: yh = -0.4

    # devisible by 48
    combine_jzonc_bins = 1
    if 'Mdm_8p0' in galaxy_name and 'mu_1' in galaxy_name: combine_jzonc_bins = 8
    if 'Mdm_8p0' in galaxy_name and 'mu_5' in galaxy_name: combine_jzonc_bins = 4  # 6
    if 'Mdm_8p0' in galaxy_name and 'mu_25' in galaxy_name: combine_jzonc_bins = 4
    if 'Mdm_7p5' in galaxy_name and 'mu_1' in galaxy_name: combine_jzonc_bins = 6
    if 'Mdm_7p5' in galaxy_name and 'mu_5' in galaxy_name: combine_jzonc_bins = 4  # 4
    if 'Mdm_7p5' in galaxy_name and 'mu_25' in galaxy_name: combine_jzonc_bins = 2
    if 'Mdm_7p0' in galaxy_name and 'mu_5' in galaxy_name: combine_jzonc_bins = 3
    if 'Mdm_6p5' in galaxy_name and 'mu_5' in galaxy_name: combine_jzonc_bins = 2

    # devisible by 200
    combine_vphi_bins = 1
    if 'Mdm_8p0' in galaxy_name and 'mu_1' in galaxy_name: combine_vphi_bins = 20
    if 'Mdm_8p0' in galaxy_name and 'mu_5' in galaxy_name: combine_vphi_bins = 8  # 10
    if 'Mdm_8p0' in galaxy_name and 'mu_25' in galaxy_name: combine_vphi_bins = 5
    if 'Mdm_7p5' in galaxy_name and 'mu_1' in galaxy_name: combine_vphi_bins = 10
    if 'Mdm_7p5' in galaxy_name and 'mu_5' in galaxy_name: combine_vphi_bins = 4  # 5
    if 'Mdm_7p5' in galaxy_name and 'mu_25' in galaxy_name: combine_vphi_bins = 4
    if 'Mdm_7p0' in galaxy_name and 'mu_1' in galaxy_name: combine_vphi_bins = 4
    if 'Mdm_7p0' in galaxy_name and 'mu_5' in galaxy_name: combine_vphi_bins = 1  # 2

    return (
    {'ls': ls, 'c': col, 'marker': marker, 'vmarker': vmarker, 
     'lab': lab, 'labn': labn, 'labnstar': labnstar, 'labstar': labstar,
     'yh': yh, 'mu': mu, 'mun': mun,
     'mdm': mdm, 'cmap': cmap,
     'jzbins': combine_jzonc_bins, 'vphibins': combine_vphi_bins, 'V200': V200, 'conc': conc,
     'scale_height': scale_height, 'lsv':lsv})


def plot_time_evolution(save_name_append='diff', save=True, name=''):
    _index = 1

    fig = plt.figure(constrained_layout=True)
    widths = [1, 1, 1]
    # fig.set_size_inches(19, 12, forward=True)
    # heights = [1, 1]
    # spec = fig.add_gridspec(ncols=3, nrows=2, width_ratios=widths, height_ratios=heights)
    fig.set_size_inches(18, 10, forward=True)
    heights = [1, 1]
    spec = fig.add_gridspec(ncols=3, nrows=2, width_ratios=widths, height_ratios=heights)

    eps = 1e-3

    axs = []
    xlims = [-eps, 0.06 - eps]
    ylims = [-2.0 + eps, 0.5 - eps]
    for row in range(len(heights)):
        axs.append([])
        for col in range(len(widths)):
            axs[row].append(fig.add_subplot(spec[row, col]))
            axs[row][col].set_xlim(xlims) #0.15 - eps])
            # axs[row][col].set_xlim([-4,0])
            axs[row][col].set_ylim(ylims)
            if col != 0: axs[row][col].set_yticklabels([])
            if row != len(heights) - 1: axs[row][col].set_xticklabels([])

    fig.subplots_adjust(hspace=0.02, wspace=0.02)

    # np.seterr(all='ignore')

    run = []
    # print('\nalpha=0, beta=0, gamma=0')
    # run.append(find_least_log_squares_best_fit(save_name_append, mcmc=False, fixed_a=0, fixed_b=0, fixed_g=0))
    print('\nbeta=0, gamma=0')
    run.append(find_least_log_squares_best_fit(save_name_append, mcmc=False, fixed_b=0, fixed_g=0))
    print('\nbeta=0, gamma=0')
    run.append(find_least_log_squares_best_fit(save_name_append, mcmc=False, fixed_b=0, fixed_g=0))

    # run_ls = [':', '-.']
    # run_funcs = [get_powerlaw_velocity2_array, get_theory_velocity2_array, get_theory_velocity2_array]
    # run_labels = [r'LO85', r'Asymptotic heating']
    # run_colour = ['grey', 'k']

    run_ls = ['-.', '--']
    run_c = ['k', 'k']
    run_funcs = [get_powerlaw_velocity2_array, get_theory_velocity2_array]
    run_labels0 = [r'$\sigma_\phi^2 \propto t$', r'equation (6)']
    run_labels1 = [r'$\overline{v}_\phi \propto t$', r'equation (11)']
    run_zorder = [10, 10]

    theory_best_v_list = []
    theory_best_z2_list = []
    theory_best_r2_list = []
    theory_best_p2_list = []

    experiment_v = []

    # (inital_velocity2_array_v, inital_velocity2_array_z, inital_velocity2_array_r, inital_velocity2_array_p
    #  ) = smooth_burn_ins(1e-2, 400, save_name_append, burn_in, bin_edges)

    theory_times = np.logspace(-5, 1, 1000)
    log_theory_times = np.log10(theory_times)
    fifty = len(theory_times)
    o = np.ones(fifty)

    for j, (ln_Lambda_ks, alphas, betas, gammas) in enumerate(run):
        galaxy_name = 'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps'

        bin_edges = get_bin_edges(save_name_append, galaxy_name=galaxy_name)
        analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                                 save_name_append=save_name_append, galaxy_name=galaxy_name)
        analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)

        v200 = get_v_200()
        (t_c_dm, _, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, 1, galaxy_name=galaxy_name)

        tau_t = get_tau_array(theory_times, t_c_dm[:, np.newaxis])

        # yaxis
        theory_best_p2_list = [run_funcs[j](v200 ** 2 * o, 0 * o,
                                            theory_times, t_c_dm[i] * o, delta_dm[i] * o, upsilon_dm[i] * o,
                                            ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])
                               for i in range(len(bin_edges) // 2)]
        theory_best_v_list = [run_funcs[j](v200 * o, 0 * o,
                                           theory_times, t_c_dm[i] * o, delta_dm[i] * o, np.sqrt(upsilon_circ[i]) * o,
                                           ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
                              for i in range(len(bin_edges) // 2)]

        #
        tau_heat_z2 = np.array([get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                                   ln_Lambda_ks[1], alphas[1], betas[1], gammas[1]) for i in
                                range(len(bin_edges) // 2)])
        tau_heat_r2 = np.array([get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                                   ln_Lambda_ks[2], alphas[2], betas[2], gammas[2]) for i in
                                range(len(bin_edges) // 2)])
        tau_heat_p2 = np.array([get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                                   ln_Lambda_ks[3], alphas[3], betas[3], gammas[3]) for i in
                                range(len(bin_edges) // 2)])

        # tau_heat_experiment[i] = ((tau_heat_z2[i] + tau_heat_r2[i] + tau_heat_p2[i]
        #                     ) * np.sqrt(upsilon_dm[i]) / np.sqrt(upsilon_circ[i]) / 3) #
        # tau_heat_experiment = (tau_heat_r2 * 1.06126839)
        tau_heat_experiment = (tau_heat_p2 * 0.74987599)

        tau_0_on_tau_heat = np.array([get_tau_zero_array(v200 * o, 0 * o,
                                                         np.sqrt(upsilon_circ[i])) for i in
                                      range(len(bin_edges) // 2)])

        if run_funcs[j] == get_theory_velocity2_array:
            experiment_v = np.array([v200 * np.sqrt(upsilon_circ[i]) ** 2 * (1 - np.exp(
                - np.power(((theory_times / t_c_dm[i]) / tau_heat_experiment[i]) + tau_0_on_tau_heat[i], 1 + gammas[0])))
                                    for i in range(len(bin_edges) // 2)])

            print(np.shape(experiment_v[-1]))

        else:  # run_funcs == get_powerlaw_velocity2_array
            experiment_v = np.array([(v200 * o - 0 * o) * (
                np.power(((theory_times / t_c_dm[i]) / tau_heat_experiment[i]),
                         1 + gammas[0])) + 0 * o for i in range(len(bin_edges) // 2)])

            print(np.shape(experiment_v[-1]))

        # plot
        for i in range(len(widths)):
            if not(j==0 and i==1):
                axs[0][i].errorbar(tau_t[_index], np.log10(theory_best_p2_list[_index]) - 2 * np.log10(v200),
                                   ls=run_ls[j], lw=2, c=run_c[j], label=run_labels0[j], zorder=run_zorder[j])
                # axs[1][i].errorbar(tau_t[_index], np.log10(theory_best_v_list[_index]) - np.log10(v200),
                #                    ls=run_ls[j], lw=2, c=run_c[j], label=run_labels1[j], zorder=run_zorder[j])
                axs[1][i].errorbar(tau_t[_index], np.log10(experiment_v[_index]) - np.log10(v200),
                                   ls=run_ls[j], lw=2, c=run_c[j], label=run_labels1[j], zorder=run_zorder[j])

        if j == 0:
            # maximum cut off
            ls = '--'
            c = 'grey'
            alpha = 0.5
            linewidth = 3

            for i in range(len(widths)):
                axs[0][i].errorbar(tau_t[_index], o - 1 + 2 * np.log10(analytic_dispersion[_index]) - 2 * np.log10(v200),
                                   ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
                axs[1][i].errorbar(tau_t[_index], o - 1 + np.log10(analytic_v_circ[_index]) - np.log10(v200),
                                  ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
        
        if j!=0:
    
            # different concentration models
            for galaxy_name in ['mu_5_c_7/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                                'mu_5_c_15/fdisk_0p01_lgMdm_8p0_V200-200kmps']:
    
                bin_edges = get_bin_edges(save_name_append, galaxy_name=galaxy_name)
                analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                                         save_name_append=save_name_append, galaxy_name=galaxy_name)
                analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                                     save_name_append=save_name_append, galaxy_name=galaxy_name)
    
                v200 = get_v_200(galaxy_name=galaxy_name)
                (t_c_dm, _, delta_dm, upsilon_dm, upsilon_circ
                 ) = get_constants(bin_edges, save_name_append, 1, galaxy_name=galaxy_name)
    
                tau_t = get_tau_array(theory_times, t_c_dm[:, np.newaxis])
    
                # yaxis
                theory_best_p2_list = [run_funcs[j](v200 ** 2 * o, 0 * o,
                                                    theory_times, t_c_dm[i] * o, delta_dm[i] * o, upsilon_dm[i] * o,
                                                    ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])
                                       for i in range(len(bin_edges) // 2)]
                theory_best_v_list = [run_funcs[j](v200 * o, 0 * o,
                                                   theory_times, t_c_dm[i] * o, delta_dm[i] * o,
                                                   np.sqrt(upsilon_circ[i]) * o,
                                                   ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
                                      for i in range(len(bin_edges) // 2)]

                #
                tau_heat_z2 = np.array([get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[1], alphas[1], betas[1], gammas[1]) for i in
                                        range(len(bin_edges) // 2)])
                tau_heat_r2 = np.array([get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[2], alphas[2], betas[2], gammas[2]) for i in
                                        range(len(bin_edges) // 2)])
                tau_heat_p2 = np.array([get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[3], alphas[3], betas[3], gammas[3]) for i in
                                        range(len(bin_edges) // 2)])

                # tau_heat_experiment[i] = ((tau_heat_z2[i] + tau_heat_r2[i] + tau_heat_p2[i]
                #                     ) * np.sqrt(upsilon_dm[i]) / np.sqrt(upsilon_circ[i]) / 3) #
                # tau_heat_experiment = (tau_heat_r2 * 1.06126839)
                tau_heat_experiment = tau_heat_p2 * 0.74987599

                tau_0_on_tau_heat = np.array([get_tau_zero_array(v200 * o, 0 * o,
                                                                 np.sqrt(upsilon_circ[i])) for i in
                                              range(len(bin_edges) // 2)])

                if run_funcs[j] == get_theory_velocity2_array:
                    experiment_v = np.array([v200 * np.sqrt(upsilon_circ[i]) ** 2 * (1 - np.exp(
                        - np.power(((theory_times / t_c_dm[i]) / tau_heat_experiment[i]) + tau_0_on_tau_heat[i],
                                   1 + gammas[0])))
                                       for i in range(len(bin_edges) // 2)])

                    print(np.shape(experiment_v[-1]))

                else:  # run_funcs == get_powerlaw_velocity2_array
                    experiment_v = np.array([v200 * np.sqrt(upsilon_circ[i]) ** 2 * (
                        np.power(((theory_times / t_c_dm[i]) / tau_heat_experiment[i]),
                                 1 + gammas[0])) for i in range(len(bin_edges) // 2)])

                    print(np.shape(experiment_v[-1]))
                    
    
                # plot
                axs[0][1].errorbar(tau_t[_index], np.log10(theory_best_p2_list[_index]
                                                                ) - 2 * np.log10(v200),
                                        ls=run_ls[j], lw=2, c=run_c[j], zorder=run_zorder[j])
                # axs[1][1].errorbar(tau_t[_index], np.log10(theory_best_v_list[_index]) - np.log10(v200),
                #                         ls=run_ls[j], lw=2, c=run_c[j], zorder=run_zorder[j])
                axs[1][1].errorbar(tau_t[_index], np.log10(experiment_v[_index]) - np.log10(v200),
                                        ls=run_ls[j], lw=2, c=run_c[j], zorder=run_zorder[j])
    
                if j == 1:
                    # maximum cut off
                    ls = '--'
                    c = 'grey'
                    alpha = 0.5
                    linewidth = 3
                    axs[0][1].errorbar(tau_t[_index],
                                            o - 1 + 2 * np.log10(analytic_dispersion[_index]) - 2 * np.log10(v200),
                                            ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
                    axs[1][1].errorbar(tau_t[_index], o - 1 + np.log10(analytic_v_circ[_index]) - np.log10(v200),
                                            ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)

    bin_edges = get_bin_edges(save_name_append)
    analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                             save_name_append=save_name_append)
    analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                         save_name_append=save_name_append)
    v200 = get_v_200()

    (t_c_dm, _, delta_dm, upsilon_dm, upsilon_circ
     ) = get_constants(bin_edges, save_name_append, 1)

    tau_t = get_tau_array(theory_times, t_c_dm[:, np.newaxis])

    # fits for comparison
    print('\nbeta=0, gamma=0 (fiducial)')
    (ln_Lambda_ks, alphas, betas, gammas
     ) = find_least_log_squares_best_fit(
        save_name_append, mcmc=False, fixed_g=0, fixed_b=0)
    # save_name_append, mcmc=False, fixed_g=0, fixed_b=-1)
    # save_name_append, mcmc=False, fixed_g=0)

    galaxy_name_list = ['mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps', 'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_1/fdisk_0p01_lgMdm_7p5_V200-200kmps', 'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                        'mu_25/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                        'mu_1/fdisk_0p01_lgMdm_7p0_V200-200kmps', 'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_25/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_1/fdisk_0p01_lgMdm_6p5_V200-200kmps', 'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
                        'mu_25/fdisk_0p01_lgMdm_6p5_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps', 'mu_25/fdisk_0p01_lgMdm_6p0_V200-200kmps',
                        ]
    snaps_list = [400, 400, 400, 400, 400, 400, 101, 101, 101, 101, 101, 101, 101, 101]

    # put data on plots
    for galaxy_name, snaps in zip(galaxy_name_list, snaps_list):
        kwargs = get_name_kwargs(galaxy_name)

        (mean_v_R, mean_v_phi,
         sigma_z, sigma_R, sigma_phi
         ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges)
        
        # if galaxy_name == 'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps':
        #     (mean_v_R, mean_v_phi,
        #      sigma_z, sigma_R, sigma_phi
        #      ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges, overwrite=True)

        bin_edges = get_bin_edges(save_name_append, galaxy_name=galaxy_name)

        # analytic_dispersion = get_v_200(galaxy_name=galaxy_name) * np.ones(len(bin_edges) // 2)
        # analytic_v_circ = get_v_200(galaxy_name=galaxy_name) * np.ones(len(bin_edges) // 2)
        analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                                 save_name_append=save_name_append, galaxy_name=galaxy_name)
        analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)


        # work out constatns
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, kwargs['mdm'], galaxy_name=galaxy_name)
        v200 = get_v_200(galaxy_name)

        mean_v_phi = mean_v_phi.T
        sigma_z2 = sigma_z.T ** 2
        sigma_r2 = sigma_R.T ** 2
        sigma_p2 = sigma_phi.T ** 2

        (inital_velocity_v, inital_velocity2_z, inital_velocity2_r, inital_velocity2_p
         ) = smooth_burn_ins(kwargs['mdm'], snaps, save_name_append, BURN_IN, bin_edges, galaxy_name=galaxy_name)
        inital_velocity2_z **= 2
        inital_velocity2_r **= 2
        inital_velocity2_p **= 2

        time = np.linspace(0, T_TOT, snaps + 1)
        time = time - time[BURN_IN]
        tau = get_tau_array(time, t_c_dm[_index])

        tau_virp = get_tau_heat_array(delta_dm[_index], upsilon_dm[_index], ln_Lambda_ks[3], alphas[3], betas[3],
                                      gammas[3])
        tau_0p = tau_virp * np.log(
            analytic_dispersion[_index] ** 2 / (analytic_dispersion[_index] ** 2 - inital_velocity2_p[_index]))

        tau_virv = get_tau_heat_array(delta_dm[_index], upsilon_circ[_index], ln_Lambda_ks[0], alphas[0], betas[0],
                                      gammas[0])
        tau_0v = tau_virv * np.log(analytic_v_circ[_index] / inital_velocity_v[_index])

        mksz = 3
        alpha = 0.4#0.1
        lw = 6
        lwt = 3
        window_div = 8
        window = snaps // window_div + 1
        if window % 2 == 0: window += 1
        poly_n = 5

        # x = tau #+ tau_0v  # / tau_heat_v
        # y = (inital_velocity_v[_index] - mean_v_phi[_index]) / v200
        # y = (inital_velocity_v[_index]**2 - mean_v_phi[_index]**2) / v200 **2
        # y = (inital_velocity_v[_index] - mean_v_phi[_index])**2 / v200 **2

        x = tau_0v + tau
        # y = (analytic_v_circ[_index] - mean_v_phi[_index]) / analytic_v_circ[_index]
        y = (analytic_v_circ[_index] - mean_v_phi[_index]) / v200
        # # y = (v200 - mean_v_phi[_index]) / v200

        mask = ~np.isnan(y)
        mask[:BURN_IN - 1] = False
        x = x[mask]
        y = y[mask]
        sy = np.log10(savgol_filter(y, window, poly_n, mode='interp'))
        y = np.log10(y)

        # axs[1][0].errorbar(x, y, c=kwargs['c'], marker='', ls='-', lw=lw, alpha=alpha)
        axs[1][0].errorbar(x, sy, c=kwargs['c'], marker='', ls=kwargs['ls'], lw=lwt)
        # axs[1][0].errorbar(x, sy, c=kwargs['c'], marker=kwargs['marker'], ms=mksz, ls='', lw=lw, alpha=alpha)

        # x = tau #+ tau_0p
        # y = (sigma_p2[_index] - inital_velocity2_p[_index]) / v200 ** 2

        x = tau_0p + tau
        # # y = sigma_p2[_index] / analytic_dispersion[_index]**2
        y = sigma_p2[_index] / v200 ** 2

        mask = ~np.isnan(y)
        x = x[mask]
        y = y[mask]
        sy = np.log10(savgol_filter(y, window, poly_n, mode='interp'))
        y = np.log10(y)

        # axs[0][0].errorbar(x, y, c=kwargs['c'], marker='', ls='-', lw=lw, alpha=alpha)
        axs[0][0].errorbar(x, sy, c=kwargs['c'], marker='', ls=kwargs['ls'], lw=lwt)
        # axs[0][0].errorbar(x, sy, c=kwargs['c'], marker=kwargs['marker'], ms=mksz, ls='', lw=lw, alpha=alpha)

    galaxy_name_list = ['mu_5_c_7/fdisk_0p01_lgMdm_8p0_V200-200kmps', 'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_5_c_15/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_5_c_7/fdisk_0p01_lgMdm_7p0_V200-200kmps', 'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_5_c_15/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        ]
    snaps_list = [400, 400, 400, 101, 101, 101]

    # put data on plots
    for galaxy_name, snaps in zip(galaxy_name_list, snaps_list):
        kwargs = get_name_kwargs(galaxy_name)

        if kwargs['conc'] == 7:
            ls_c = (0, (4, 1, 1, 1, 1, 1))
            mk_c = 'v'
        if kwargs['conc'] == 10:
            ls_c = '-'
            mk_c = 'o'
        if kwargs['conc'] == 15:
            ls_c = (0, (0.5, 0.5))
            mk_c = '^'

        (mean_v_R, mean_v_phi,
         sigma_z, sigma_R, sigma_phi
         ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges)

        bin_edges = get_bin_edges(save_name_append, galaxy_name=galaxy_name)

        # analytic_dispersion = get_v_200(galaxy_name=galaxy_name) * np.ones(len(bin_edges) // 2)
        # analytic_v_circ = get_v_200(galaxy_name=galaxy_name) * np.ones(len(bin_edges) // 2)
        analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                                 save_name_append=save_name_append, galaxy_name=galaxy_name)
        analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)


        # work out constatns
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, kwargs['mdm'], galaxy_name=galaxy_name)
        v200 = get_v_200(galaxy_name)

        mean_v_phi = mean_v_phi.T
        sigma_z2 = sigma_z.T ** 2
        sigma_r2 = sigma_R.T ** 2
        sigma_p2 = sigma_phi.T ** 2

        (inital_velocity_v, inital_velocity2_z, inital_velocity2_r, inital_velocity2_p
         ) = smooth_burn_ins(kwargs['mdm'], snaps, save_name_append, BURN_IN, bin_edges, galaxy_name=galaxy_name)
        inital_velocity2_z **= 2
        inital_velocity2_r **= 2
        inital_velocity2_p **= 2

        time = np.linspace(0, T_TOT, snaps + 1)
        time = time - time[BURN_IN]
        tau = get_tau_array(time, t_c_dm[_index])

        tau_virp = get_tau_heat_array(delta_dm[_index], upsilon_dm[_index], ln_Lambda_ks[3], alphas[3], betas[3],
                                      gammas[3])
        tau_0p = tau_virp * np.log(
            analytic_dispersion[_index] ** 2 / (analytic_dispersion[_index] ** 2 - inital_velocity2_p[_index]))

        tau_virv = get_tau_heat_array(delta_dm[_index], upsilon_circ[_index], ln_Lambda_ks[0], alphas[0], betas[0],
                                      gammas[0])
        tau_0v = tau_virv * np.log(analytic_v_circ[_index] / inital_velocity_v[_index])

        window = snaps // window_div + 1
        if window % 2 == 0: window += 1

        # x = tau #+ tau_0v  # / tau_heat_v
        # y = (inital_velocity_v[_index] - mean_v_phi[_index]) / v200
        # y = (inital_velocity_v[_index]**2 - mean_v_phi[_index]**2) / v200 **2
        # y = (inital_velocity_v[_index] - mean_v_phi[_index])**2 / v200 **2

        x = tau_0v + tau
        # y = (analytic_v_circ[_index] - mean_v_phi[_index]) / analytic_v_circ[_index]
        y = (analytic_v_circ[_index] - mean_v_phi[_index]) / v200
        # # y = (v200 - mean_v_phi[_index]) / v200

        mask = ~np.isnan(y)
        mask[:BURN_IN - 1] = False
        x = x[mask]
        y = y[mask]
        sy = np.log10(savgol_filter(y, window, poly_n, mode='interp'))
        y = np.log10(y)

        # axs[1][1].errorbar(x, y, c=kwargs['c'], marker='', ls='-', lw=lw, alpha=alpha)
        axs[1][1].errorbar(x, sy, c=kwargs['c'], marker='', ls=ls_c, lw=lwt)
        # axs[1][1].errorbar(x, y, c=kwargs['c'], marker=mk_c, ms=mksz, ls='', lw=lw, alpha=alpha)

        # x = tau #+ tau_0p
        # y = (sigma_p2[_index] - inital_velocity2_p[_index]) / v200 ** 2

        x = tau_0p + tau
        # # y = sigma_p2[_index] / analytic_dispersion[_index]**2
        y = sigma_p2[_index] / v200 ** 2

        mask = ~np.isnan(y)
        x = x[mask]
        y = y[mask]
        sy = np.log10(savgol_filter(y, window, poly_n, mode='interp'))
        y = np.log10(y)

        # axs[0][1].errorbar(x, y, c=kwargs['c'], marker='', ls='-', lw=lw, alpha=alpha)
        axs[0][1].errorbar(x, sy, c=kwargs['c'], marker='', ls=ls_c, lw=lwt)
        # axs[0][1].errorbar(x, y, c=kwargs['c'], marker=mk_c, ms=mksz, ls='', lw=lw, alpha=alpha)

    galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_2z0_fixed_eps',
                        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_4z0_fixed_eps',
                        'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps_2z0_fixed_eps',
                        'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps_4z0_fixed_eps',
                        ]
    snaps_list = [400, 400, 400, 101, 400, 400]

    # put data on plots
    for galaxy_name, snaps in zip(galaxy_name_list, snaps_list):
        kwargs = get_name_kwargs(galaxy_name)

        if kwargs['scale_height'] == 1:
            ls_z0 = '-'
            mk_z0 = 'X'
        if kwargs['scale_height'] == 2:
            ls_z0 = (0, (4,1))
            mk_z0 = 'o'
        if kwargs['scale_height'] == 4:
            ls_z0 = (0, (8,1,1,1))
            mk_z0 = 'D'

        (mean_v_R, mean_v_phi,
         sigma_z, sigma_R, sigma_phi
         ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges)

        bin_edges = get_bin_edges(save_name_append, galaxy_name=galaxy_name)

        analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                                 save_name_append=save_name_append, galaxy_name=galaxy_name)
        analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)

        # work out constatns
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, kwargs['mdm'], galaxy_name=galaxy_name)
        v200 = get_v_200(galaxy_name)

        mean_v_phi = mean_v_phi.T
        sigma_z2 = sigma_z.T ** 2
        sigma_r2 = sigma_R.T ** 2
        sigma_p2 = sigma_phi.T ** 2

        (inital_velocity_v, inital_velocity2_z, inital_velocity2_r, inital_velocity2_p
         ) = smooth_burn_ins(kwargs['mdm'], snaps, save_name_append, BURN_IN, bin_edges, galaxy_name=galaxy_name)
        inital_velocity2_z **= 2
        inital_velocity2_r **= 2
        inital_velocity2_p **= 2

        time = np.linspace(0, T_TOT, snaps + 1)
        time = time - time[BURN_IN]
        tau = get_tau_array(time, t_c_dm[_index])

        tau_virp = get_tau_heat_array(delta_dm[_index], upsilon_dm[_index], ln_Lambda_ks[3], alphas[3], betas[3],
                                      gammas[3])
        tau_0p = tau_virp * np.log(
            analytic_dispersion[_index] ** 2 / (analytic_dispersion[_index] ** 2 - inital_velocity2_p[_index]))

        tau_virv = get_tau_heat_array(delta_dm[_index], upsilon_circ[_index], ln_Lambda_ks[0], alphas[0], betas[0],
                                      gammas[0])
        tau_0v = tau_virv * np.log(analytic_v_circ[_index] / inital_velocity_v[_index])

        window = snaps // window_div + 1
        if window % 2 == 0: window += 1

        # x = tau #+ tau_0v  # / tau_heat_v
        # y = (inital_velocity_v[_index] - mean_v_phi[_index]) / v200
        # y = (inital_velocity_v[_index]**2 - mean_v_phi[_index]**2) / v200 **2
        # y = (inital_velocity_v[_index] - mean_v_phi[_index])**2 / v200 **2

        x = tau_0v + tau
        # y = (analytic_v_circ[_index] - mean_v_phi[_index]) / analytic_v_circ[_index]
        y = (analytic_v_circ[_index] - mean_v_phi[_index]) / v200
        # # y = (v200 - mean_v_phi[_index]) / v200

        mask = ~np.isnan(y)
        mask[:BURN_IN - 1] = False
        x = x[mask]
        y = y[mask]
        sy = np.log10(savgol_filter(y, window, poly_n, mode='interp'))
        y = np.log10(y)

        # axs[1][2].errorbar(x, y, c=kwargs['c'], marker='', ls='-', lw=lw, alpha=alpha)
        axs[1][2].errorbar(x, sy, c=kwargs['c'], marker='', ls=ls_z0, lw=lwt)
        # axs[1][2].errorbar(x, y, c=kwargs['c'], marker=mk_z0, ms=mksz, ls='', lw=lw, alpha=alpha)

        # x = tau #+ tau_0p
        # y = (sigma_p2[_index] - inital_velocity2_p[_index]) / v200 ** 2

        x = tau_0p + tau
        # # y = sigma_p2[_index] / analytic_dispersion[_index]**2
        y = sigma_p2[_index] / v200 ** 2

        mask = ~np.isnan(y)
        x = x[mask]
        y = y[mask]
        sy = np.log10(savgol_filter(y, window, poly_n, mode='interp'))
        y = np.log10(y)

        # axs[0][2].errorbar(x, y, c=kwargs['c'], marker='', ls='-', lw=lw, alpha=alpha)
        axs[0][2].errorbar(x, sy, c=kwargs['c'], marker='', ls=ls_z0, lw=lwt)
        # axs[0][2].errorbar(x, y, c=kwargs['c'], marker=mk_z0, ms=mksz, ls='', lw=lw, alpha=alpha)

    axs[1][0].set_ylabel(r'$\log ([V_c - \overline{v}_\phi] / V_{200}) $')
    axs[0][0].set_ylabel(r'$\log (\sigma_\phi^2 / V_{200}^2) $')
    axs[1][0].set_xlabel(r'$t / t_c$')
    axs[1][1].set_xlabel(r'$t / t_c$')
    axs[1][2].set_xlabel(r'$t / t_c$')

    # axs[1][0].set_ylabel(r'$\log [(v_c - v_i) / V_{200}] $')
    # axs[0][0].set_ylabel(r'$\log (\Delta \sigma_\phi^2 / V_{200}^2) $')
    # axs[1][0].set_xlabel(r'$\log(\tau)$')
    # axs[1][1].set_xlabel(r'$\log(\tau)$')

    axs[0][1].legend([(lines.Line2D([0, 1], [0, 1], color='k', ls='--', linewidth=2)),
                      (lines.Line2D([0, 1], [0, 1], color='grey', linewidth=3, ls=(0,(0.5,0.5)))),#, marker='v')),
                      (lines.Line2D([0, 1], [0, 1], color='grey', linewidth=3, ls='-')),#, marker='o')),
                      (lines.Line2D([0, 1], [0, 1], color='grey', linewidth=3, ls=(0,(4,1,1,1,1,1)))),#, marker='^'))
                      ],
                     [r'equation (6)',
                      r'$c = 7$', r'$c = 10$', r'$c = 15$'], numpoints=2,
                     loc='lower right', frameon=False, labelspacing=0.3)
    
    axs[1][1].legend([(lines.Line2D([0, 1], [0, 1], color='k', ls='--', linewidth=2))],
                     [r'equation (11)'], loc='lower left', numpoints=2,
                     frameon=False, labelspacing=0.3, bbox_to_anchor=(0.33, 0.18))

    p = 0.05
    xx = xlims[0] + p * (xlims[1] - xlims[0])
    yy = ylims[0] + (1-p) * (ylims[1] - ylims[0])
    axs[0][0].text(xx, yy, r'$R = R_{1/2}$', va='top')
    axs[1][0].text(xx, yy, r'$R = R_{1/2}$', va='top')
    axs[0][1].text(xx, yy, r'Matched to $\rho (R_{1/2})$ for $c=10$', va='top')
    axs[1][1].text(xx, yy, r'Matched to $\rho (R_{1/2})$ for $c=10$', va='top')
    axs[0][2].text(xx, yy, r'$R = R_{1/2}$', va='top')
    axs[1][2].text(xx, yy, r'$R = R_{1/2}$', va='top')

    axs[1][0].text(xx, 0.01, r'$V_c$', va='bottom')
    axs[0][0].text(xx, -0.2, r'$\sigma_{\mathrm{DM}}$', va='bottom')

    axs[0][0].legend([(lines.Line2D([0, 1], [0, 1], color='k', ls='-.', linewidth=2)),
                      (lines.Line2D([0, 1], [0, 1], color='k', ls='--', linewidth=2)),
                      (lines.Line2D([0, 1], [0, 1], color='grey', linewidth=3, ls=':')),#, marker='s')),
                      (lines.Line2D([0, 1], [0, 1], color='grey', linewidth=3, ls='-')),#, marker='o')),
                      (lines.Line2D([0, 1], [0, 1], color='grey', linewidth=3, ls='--')),#, marker='h'))
                      ],
                     [r'$\sigma_\phi^2 \propto t$', r'equation (6)',
                      r'$\mu = 1$', r'$\mu = 5$', r'$\mu = 25$'],
                     loc='lower right', frameon=False, numpoints=2,
                     labelspacing=0.3)  # , numpoints=1, handlelength=1, handletextpad=0.2, frameon=False,
    # labelspacing=0.1, borderpad=0.2)#, bbox_to_anchor=(1, 0))

    axs[1][0].legend([(lines.Line2D([0, 1], [0, 1], color='k', ls='-.', linewidth=2)),
                      (lines.Line2D([0, 1], [0, 1], color='k', ls='--', linewidth=2))],
                     [r'$\overline{v}_\phi \propto t$', r'equation (11)'], loc='lower left',
                     numpoints=2, frameon=False, labelspacing=0.3, bbox_to_anchor=(0.33, 0.43))

    # axs[0][1].legend([(lines.Line2D([0, 1], [0, 1], color='brown', ls='--', linewidth=2)),
    #                   (lines.Line2D([0, 1], [0, 1], color='magenta', ls='-.', linewidth=2)),
    #                   (lines.Line2D([0, 1], [0, 1], color='grey', ls=(0, (3, 1, 1, 1, 1, 1)), linewidth=2)),
    #                   (lines.Line2D([0, 1], [0, 1], color='grey', ls='-', linewidth=2)),
    #                   (lines.Line2D([0, 1], [0, 1], color='grey', ls='--', linewidth=2))],
    #                   [r'$\beta = 0$', r'$\beta = -1$', r'$c = 7$', r'$c = 10$', r'$c = 15$'],
    #                  loc='lower right', frameon=False, labelspacing=0.2)

    axs[0][2].legend([(lines.Line2D([0, 1], [0, 1], color='k', ls='-.', linewidth=2)),
                      (lines.Line2D([0, 1], [0, 1], color='k', ls='--', linewidth=2)),
                      (lines.Line2D([0, 1], [0, 1], color='grey', linewidth=3, ls='-')),#, marker='X')),
                      (lines.Line2D([0, 1], [0, 1], color='grey', linewidth=3, ls=(0,(4,1)))),#, marker='o')),
                      (lines.Line2D([0, 1], [0, 1], color='grey', linewidth=3, ls=(0,(8,1,1,1)))),#, marker='D'))
                      ],
                     [r'$\sigma_\phi^2 \propto t$', r'equation (6)',
                      r'$z_d = 0.05 \times R_d$', r'$z_d = 0.1 \times R_d$',
                      r'$z_d = 0.2 \times R_d$'],  numpoints=2,
                     loc='lower right', frameon=False, labelspacing=0.3)

    axs[1][2].legend([(lines.Line2D([0, 1], [0, 1], color='k', ls='-.', linewidth=2)),
                      (lines.Line2D([0, 1], [0, 1], color='k', ls='--', linewidth=2))],
                     [r'$\overline{v}_\phi \propto t$', r'equation (11)'], loc='lower left',
                     numpoints=2, frameon=False, labelspacing=0.3, bbox_to_anchor=(0.33, 0.2))

    y0 = -1.90
    dy = 0.21
    xx = xlims[1] - 0.002
    axs[1][0].text(xx, y0 + 4 * dy,
                   r'$m_{\mathrm{DM}} = 10^{6.0}$M$_\odot, \mathrm{N}_{\mathrm{DM}} = 1.8 \times 10^6$',
                   va='bottom', ha='right', c='C4')
    axs[1][0].text(xx, y0 + 3 * dy,
                   r'$m_{\mathrm{DM}} = 10^{6.5}$M$_\odot, \mathrm{N}_{\mathrm{DM}} = 5.8 \times 10^5$',
                   va='bottom', ha='right', c='C3')
    axs[1][0].text(xx, y0 + 2 * dy,
                   r'$m_{\mathrm{DM}} = 10^{7.0}$M$_\odot, \mathrm{N}_{\mathrm{DM}} = 1.8 \times 10^5$',
                   va='bottom', ha='right', c='C2')
    axs[1][0].text(xx, y0 + 1 * dy,
                   r'$m_{\mathrm{DM}} = 10^{7.5}$M$_\odot, \mathrm{N}_{\mathrm{DM}} = 5.8 \times 10^4$',
                   va='bottom', ha='right', c='C1')
    axs[1][0].text(xx, y0 + 0 * dy,
                   r'$m_{\mathrm{DM}} = 10^{8.0}$M$_\odot, \mathrm{N}_{\mathrm{DM}} = 1.8 \times 10^4$',
                   va='bottom', ha='right', c='C0')

    axs[1][1].text(xx, y0 + 1 * dy,
                   r'$m_{\mathrm{DM}} = 10^{7.0}$M$_\odot, \mathrm{N}_{\mathrm{DM}} = 1.8 \times 10^5$',
                   va='bottom', ha='right', c='C2')
    axs[1][1].text(xx, y0 + 0 * dy,
                   r'$m_{\mathrm{DM}} = 10^{8.0}$M$_\odot, \mathrm{N}_{\mathrm{DM}} = 1.8 \times 10^4$',
                   va='bottom', ha='right', c='C0')
    axs[1][2].text(xx, y0 + 1 * dy,
                   r'$m_{\mathrm{DM}} = 10^{7.0}$M$_\odot, \mathrm{N}_{\mathrm{DM}} = 1.8 \times 10^5$',
                   va='bottom', ha='right', c='C2')
    axs[1][2].text(xx, y0 + 0 * dy,
                   r'$m_{\mathrm{DM}} = 10^{8.0}$M$_\odot, \mathrm{N}_{\mathrm{DM}} = 1.8 \times 10^4$',
                   va='bottom', ha='right', c='C0')

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    # np.seterr(all='warn')

    return


def plot_time_evolution_experiments(save_name_append='diff', save=True, name=''):
    # _index = 1

    fig = plt.figure(constrained_layout=True)
    widths = [1, 1, 1]
    # fig.set_size_inches(19, 12, forward=True)
    # heights = [1, 1]
    # spec = fig.add_gridspec(ncols=3, nrows=2, width_ratios=widths, height_ratios=heights)
    fig.set_size_inches(19, 24, forward=True)
    heights = [1,1,1,1]
    spec = fig.add_gridspec(ncols=3, nrows=4, width_ratios=widths, height_ratios=heights)

    eps = 1e-3

    axs = []
    for row in range(len(heights)):
        axs.append([])
        for col in range(len(widths)):
            axs[row].append(fig.add_subplot(spec[row, col]))
            axs[row][col].set_xlim([-3.5,-0.2])
            axs[row][col].set_ylim([-2.5+eps, 0.3-eps])
            # axs[row][col].set_xlim([-3.5, 0.5])
            # axs[row][col].set_ylim([-3.5, 0.5])
            if col != 0: axs[row][col].set_yticklabels([])
            if row != len(heights) - 1: axs[row][col].set_xticklabels([])

    fig.subplots_adjust(hspace=0.02, wspace=0.02)

    # np.seterr(all='ignore')

    run = []
    # print('\ngamma=0')
    # run.append(find_least_log_squares_best_fit(save_name_append, mcmc=False, fixed_g=0))
    print('\nbeta=0, gamma=0')
    # run.append(find_least_log_squares_best_fit(save_name_append, mcmc=False, fixed_b=0, fixed_g=0))
    # run.append(find_least_log_squares_best_fit(save_name_append, mcmc=False, fixed_b=0, fixed_g=0))
    run.append(find_least_log_squares_best_fit(save_name_append, mcmc=False, fixed_b=0, fixed_g=0))
    run.append(find_least_log_squares_best_fit(save_name_append, mcmc=False, fixed_b=0, fixed_g=0))
    # run.append(find_least_log_squares_best_fit(save_name_append, mcmc=False, fixed_b=0, fixed_g=0))
    # run.append(find_least_log_squares_best_fit(save_name_append, mcmc=False, fixed_g=0))
    # run.append(find_least_log_squares_best_fit(save_name_append, mcmc=False, fixed_g=0))
    # print('\nbeta=-1, gamma=0')
    # run.append(find_least_log_squares_best_fit(save_name_append, mcmc=False, fixed_b=1, fixed_g=0))
    # run.append(find_least_log_squares_best_fit(save_name_append, mcmc=False, fixed_g=0))

    # run_ls = ['-.', ':']
    # run_funcs = [get_theory_velocity2_array, get_powerlaw_velocity2_array]
    # run_labels = [r'This Paper', r'LO85']
    # run_colour = ['k', 'grey']
    # run_ls = ['--', '-.']
    # run_funcs = [get_theory_velocity2_array, get_theory_velocity2_array]
    # run_labels = [r'$\beta = 0$', r'$\beta = -1$']
    # run_colour = ['brown', 'magenta']
    run_ls = ['--', '-.']
    run_funcs = [get_powerlaw_velocity2_array, get_theory_velocity2_array]
    run_labels = [r'$\beta = 0$', 'Exponential']
    run_colour = ['brown', 'C6']

    theory_best_v_list = []
    theory_best_z2_list = []
    theory_best_r2_list = []
    theory_best_p2_list = []

    # (inital_velocity2_array_v, inital_velocity2_array_z, inital_velocity2_array_r, inital_velocity2_array_p
    #  ) = smooth_burn_ins(1e-2, 400, save_name_append, burn_in, bin_edges)

    theory_times = np.logspace(-5, 1, 1000)
    log_theory_times = np.log10(theory_times)
    fifty = len(theory_times)
    o = np.ones(fifty)

    for j, (ln_Lambda_ks, alphas, betas, gammas) in enumerate(run):
        galaxy_name = 'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps'

        bin_edges = get_bin_edges(save_name_append, galaxy_name=galaxy_name)
        analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                                 save_name_append=save_name_append, galaxy_name=galaxy_name)
        analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)

        v200 = get_v_200()
        (t_c_dm, _, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, 1, galaxy_name=galaxy_name)

        tau_t = np.log10(get_tau_array(theory_times, t_c_dm[:, np.newaxis]))

        # yaxis
        theory_best_p2_list = [run_funcs[j](v200 ** 2 * o, 0 * o,
                                                 theory_times, t_c_dm[i] * o, delta_dm[i] * o, upsilon_dm[i] * o,
                                                 ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])
                                    for i in range(len(bin_edges) // 2)]
        # theory_best_v_list = [run_funcs[j](v200 ** 2 * o, 0 * o,
        #                                         theory_times, t_c_dm[i] * o, delta_dm[i] * o, upsilon_circ[i] * o,
        #                                         ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
        #                            for i in range(len(bin_edges) // 2)]
        theory_best_v_list = [run_funcs[j](v200 * o, 0 * o,
                                                theory_times, t_c_dm[i] * o, delta_dm[i] * o, np.sqrt(upsilon_circ[i]) * o,
                                                ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
                                   for i in range(len(bin_edges) // 2)]

        theory_best_z2_list = [run_funcs[j](v200**2 *o, 0*o,
                                                 theory_times, t_c_dm[i]*o, delta_dm[i] *o, upsilon_dm[i] *o,
                                                 ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
                                    for i in range(len(bin_edges)//2)]
        theory_best_r2_list = [run_funcs[j](v200**2 *o, 0*o,
                                                 theory_times, t_c_dm[i]*o, delta_dm[i] *o, upsilon_dm[i] *o,
                                                 ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
                                    for i in range(len(bin_edges)//2)]

        # plot
        for _index in range(len(widths)):
            axs[0][_index].errorbar(tau_t[_index], np.log10(theory_best_p2_list[_index]
                                                                       ) - 2 * np.log10(v200),
                               ls=run_ls[j], lw=2, c=run_colour[j], label=run_labels[j], zorder=-1)
            axs[1][_index].errorbar(tau_t[_index], np.log10(theory_best_v_list[_index] ) -  np.log10(v200),
                               ls=run_ls[j], lw=2, c=run_colour[j], label=run_labels[j], zorder=-1)

            axs[2][_index].errorbar(tau_t[_index], np.log10(theory_best_z2_list[_index]
                                                            ) - 2 * np.log10(v200),
                               ls=run_ls[j], lw=2, c=run_colour[j], label=run_labels[j], zorder=-1)
            axs[3][_index].errorbar(tau_t[_index], np.log10(theory_best_r2_list[_index]
                                                            ) - 2 * np.log10(v200),
                               ls=run_ls[j], lw=2, c=run_colour[j], label=run_labels[j], zorder=-1)

        # yaxis
        # theory_best_p2_list = [run_funcs[j](analytic_dispersion[i] ** 2 * o, 0 * o,
        #                                          theory_times, t_c_dm[i] * o, delta_dm[i] * o, upsilon_dm[i] * o,
        #                                          ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])
        #                             for i in range(len(bin_edges) // 2)]
        # theory_best_v_list = [run_funcs[j](analytic_v_circ[i] * o, 0 * o,
        #                                         theory_times, t_c_dm[i] * o, delta_dm[i] * o, upsilon_dm[i] * o,
        #                                         ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
        #                            for i in range(len(bin_edges) // 2)]
        #
        # theory_best_z2_list = [run_funcs[j](analytic_dispersion[i]**2 *o, 0*o,
        #                                          theory_times, t_c_dm[i]*o, delta_dm[i] *o, upsilon_dm[i] *o,
        #                                          ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
        #                             for i in range(len(bin_edges)//2)]
        # theory_best_r2_list = [run_funcs[j](analytic_dispersion[i]**2 *o, 0*o,
        #                                          theory_times, t_c_dm[i]*o, delta_dm[i] *o, upsilon_dm[i] *o,
        #                                          ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
        #                             for i in range(len(bin_edges)//2)]


        # different concentration models
        for galaxy_name in ['mu_5_c_7/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                            'mu_5_c_15/fdisk_0p01_lgMdm_8p0_V200-200kmps']:

            bin_edges = get_bin_edges(save_name_append, galaxy_name=galaxy_name)
            analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                                     save_name_append=save_name_append, galaxy_name=galaxy_name)
            analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                                 save_name_append=save_name_append, galaxy_name=galaxy_name)

            v200 = get_v_200(galaxy_name=galaxy_name)
            (t_c_dm, _, delta_dm, upsilon_dm, upsilon_circ
             ) = get_constants(bin_edges, save_name_append, 1, galaxy_name=galaxy_name)

            tau_t = np.log10(get_tau_array(theory_times, t_c_dm[:, np.newaxis]))

            # yaxis
            theory_best_p2_list = [run_funcs[j](v200 ** 2 * o, 0 * o,
                                                     theory_times, t_c_dm[i] * o, delta_dm[i] * o, upsilon_dm[i] * o,
                                                     ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])
                                        for i in range(len(bin_edges) // 2)]
            # theory_best_v_list = [run_funcs[j](v200 ** 2 * o, 0 * o,
            #                                         theory_times, t_c_dm[i] * o, delta_dm[i] * o, upsilon_circ[i] * o,
            #                                         ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
            #                            for i in range(len(bin_edges) // 2)]
            theory_best_v_list = [run_funcs[j](v200 * o, 0 * o,
                                                    theory_times, t_c_dm[i] * o, delta_dm[i] * o, np.sqrt(upsilon_circ[i]) * o,
                                                    ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
                                       for i in range(len(bin_edges) // 2)]

            theory_best_z2_list = [run_funcs[j](v200**2 *o, 0*o,
                                                     theory_times, t_c_dm[i]*o, delta_dm[i] *o, upsilon_dm[i] *o,
                                                     ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
                                        for i in range(len(bin_edges)//2)]
            theory_best_r2_list = [run_funcs[j](v200**2 *o, 0*o,
                                                     theory_times, t_c_dm[i]*o, delta_dm[i] *o, upsilon_dm[i] *o,
                                                     ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
                                        for i in range(len(bin_edges)//2)]

            # plot
            for _index in range(len(widths)):
                axs[0][_index].errorbar(tau_t[_index], np.log10(theory_best_p2_list[_index]
                                                                ) - 2 * np.log10(v200),
                                        ls=run_ls[j], lw=2, c=run_colour[j], label=run_labels[j], zorder=-1)
                axs[1][_index].errorbar(tau_t[_index], np.log10(theory_best_v_list[_index]) - np.log10(v200),
                                        ls=run_ls[j], lw=2, c=run_colour[j], label=run_labels[j], zorder=-1)

                axs[2][_index].errorbar(tau_t[_index], np.log10(theory_best_z2_list[_index]
                                                                ) - 2 * np.log10(v200),
                                        ls=run_ls[j], lw=2, c=run_colour[j], label=run_labels[j], zorder=-1)
                axs[3][_index].errorbar(tau_t[_index], np.log10(theory_best_r2_list[_index]
                                                                ) - 2 * np.log10(v200),
                                        ls=run_ls[j], lw=2, c=run_colour[j], label=run_labels[j], zorder=-1)

                if j == 0:
                    # maximum cut off
                    ls = '--'
                    c = 'grey'
                    alpha = 0.5
                    linewidth = 3
                    axs[0][_index].errorbar(tau_t[_index],
                                       o - 1 + 2 * np.log10(analytic_dispersion[_index]) - 2 * np.log10(v200),
                                       ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
                    axs[1][_index].errorbar(tau_t[_index], o - 1 + np.log10(analytic_v_circ[_index]) - np.log10(v200),
                                       ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)

                    axs[2][_index].errorbar(tau_t[_index], o - 1 + 2 * np.log10(analytic_dispersion[_index]) - 2 * np.log10(v200),
                                       ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
                    axs[3][_index].errorbar(tau_t[_index], o - 1 + 2 * np.log10(analytic_dispersion[_index]) - 2 * np.log10(v200),
                                       ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)

    bin_edges = get_bin_edges(save_name_append)
    analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                             save_name_append=save_name_append)
    analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                         save_name_append=save_name_append)
    v200 = get_v_200()

    # maximum cut off
    ls = '--'
    c = 'grey'
    alpha = 0.5
    linewidth = 3
    for _index in range(len(widths)):
        axs[0][_index].errorbar(tau_t[_index], o - 1 + 2 * np.log10(analytic_dispersion[_index]) - 2 * np.log10(v200),
                           ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
        axs[1][_index].errorbar(tau_t[_index], o - 1 + np.log10(analytic_v_circ[_index]) - np.log10(v200),
                           ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)

        axs[2][_index].errorbar(tau_t[_index], o - 1 + 2 * np.log10(analytic_dispersion[_index]) - 2 * np.log10(v200),
                           ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
        axs[3][_index].errorbar(tau_t[_index], o - 1 + 2 * np.log10(analytic_dispersion[_index]) - 2 * np.log10(v200),
                           ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)

        # fits for comparison
        print('\nbeta=0, gamma=0 (fiducial)')
        (ln_Lambda_ks, alphas, betas, gammas
         ) = find_least_log_squares_best_fit(
            save_name_append, mcmc=False, fixed_g=0, fixed_b=0)
            # save_name_append, mcmc=False, fixed_g=0, fixed_b=-1)
        # save_name_append, mcmc=False, fixed_g=0)

    galaxy_name_list = ['mu_5_c_7/fdisk_0p01_lgMdm_8p0_V200-200kmps', 'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_5_c_15/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_5_c_7/fdisk_0p01_lgMdm_7p0_V200-200kmps', 'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_5_c_15/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        ]
    snaps_list = [400, 400, 400, 101, 101, 101]

    # put data on plots
    for galaxy_name, snaps in zip(galaxy_name_list, snaps_list):
        kwargs = get_name_kwargs(galaxy_name)

        if kwargs['conc'] == 7:
            ls_c = (0, (3, 1, 1, 1, 1, 1))
        if kwargs['conc'] == 10:
            ls_c = '-'
        if kwargs['conc'] == 15:
            ls_c = '--'

        (mean_v_R, mean_v_phi,
         sigma_z, sigma_R, sigma_phi
         ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges)

        bin_edges = get_bin_edges(save_name_append, galaxy_name=galaxy_name)

        # analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
        #                                          save_name_append=save_name_append, galaxy_name=galaxy_name)
        # analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
        #                                      save_name_append=save_name_append, galaxy_name=galaxy_name)
        analytic_dispersion = get_v_200(galaxy_name=galaxy_name) * np.ones(len(bin_edges) // 2)
        analytic_v_circ = get_v_200(galaxy_name=galaxy_name) * np.ones(len(bin_edges) // 2)

        # work out constatns
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, kwargs['mdm'], galaxy_name=galaxy_name)
        v200 = get_v_200(galaxy_name)

        mean_v_phi = mean_v_phi.T
        sigma_z2 = sigma_z.T ** 2
        sigma_r2 = sigma_R.T ** 2
        sigma_p2 = sigma_phi.T ** 2

        (inital_velocity_v, inital_velocity2_z, inital_velocity2_r, inital_velocity2_p
         ) = smooth_burn_ins(kwargs['mdm'], snaps, save_name_append, BURN_IN, bin_edges, galaxy_name=galaxy_name)
        inital_velocity2_z **= 2
        inital_velocity2_r **= 2
        inital_velocity2_p **= 2

        for _index in range(3):

            time = np.linspace(0, T_TOT, snaps + 1)
            time = time - time[BURN_IN]
            tau = get_tau_array(time, t_c_dm[_index])

            tau_virp = get_tau_heat_array(delta_dm[_index], upsilon_dm[_index], ln_Lambda_ks[3], alphas[3], betas[3],
                                          gammas[3])
            tau_0p = tau_virp * np.log(
                analytic_dispersion[_index] ** 2 / (analytic_dispersion[_index] ** 2 - inital_velocity2_p[_index]))

            tau_virv = get_tau_heat_array(delta_dm[_index], np.sqrt(upsilon_circ[_index]), ln_Lambda_ks[0], alphas[0], betas[0],
                                          gammas[0])
            tau_0v = tau_virv * np.log(analytic_v_circ[_index] / inital_velocity_v[_index])

            tau_virz = get_tau_heat_array(delta_dm[_index], upsilon_dm[_index], ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
            tau_0z = tau_virz * np.log(
              analytic_dispersion[_index] ** 2 / (analytic_dispersion[_index] ** 2 - inital_velocity2_z[_index]))
            tau_virr = get_tau_heat_array(delta_dm[_index], upsilon_dm[_index], ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
            tau_0r = tau_virr * np.log(
              analytic_dispersion[_index] ** 2 / (analytic_dispersion[_index] ** 2 - inital_velocity2_r[_index]))

            alpha = 0.1
            lw = 5
            lwt = 2
            window_div = 8
            window = snaps // window_div + 1
            if window % 2 == 0: window += 1
            poly_n = 5
    
            x = np.log10(tau) #/ tau_heat_v
            y = (inital_velocity_v[_index] - mean_v_phi[_index]) / v200
            # y = (inital_velocity_v[_index]**2 - mean_v_phi[_index]**2) / v200 **2
            # y = (inital_velocity_v[_index] - mean_v_phi[_index])**2 / v200 **2

            # x = np.log10(tau_0v + tau)
            # # y = (analytic_v_circ[_index] - mean_v_phi[_index]) / analytic_v_circ[_index]
            # y = (analytic_v_circ[_index] - mean_v_phi[_index]) / v200
            # # y = (v200 - mean_v_phi[_index]) / v200
    
            mask = ~np.isnan(y)
            mask[:BURN_IN - 1] = False
            x = x[mask]
            y = y[mask]
            sy = np.log10(savgol_filter(y, window, poly_n, mode='interp'))
            y = np.log10(y)
    
            axs[1][_index].errorbar(x, y, c=kwargs['c'], marker='', ls='-', lw=lw, alpha=alpha)
            axs[1][_index].errorbar(x, sy, c=kwargs['c'], marker='', ls=ls_c, lw=lwt)
    
            x = np.log10(tau)
            y = (sigma_p2[_index] - inital_velocity2_p[_index]) / v200**2
    
            # x = np.log10(tau_0p + tau)
            # # y = sigma_p2[_index] / analytic_dispersion[_index]**2
            # y = sigma_p2[_index] / v200 ** 2
    
            mask = ~np.isnan(y)
            x = x[mask]
            y = y[mask]
            sy = np.log10(savgol_filter(y, window, poly_n, mode='interp'))
            y = np.log10(y)
    
            axs[0][_index].errorbar(x, y, c=kwargs['c'], marker='', ls='-', lw=lw, alpha=alpha)
            axs[0][_index].errorbar(x, sy, c=kwargs['c'], marker='', ls=ls_c, lw=lwt)
    
            x = np.log10(tau)
            y = (sigma_z2[_index] - inital_velocity2_z[_index])/ v200**2

            # x = np.log10(tau + tau_0z)
            # y = sigma_z2[_index] / v200**2

            mask = ~np.isnan(y)
            x = x[mask]
            y = y[mask]
            sy = np.log10(savgol_filter(y, window, poly_n, mode='interp'))
            y = np.log10(y)
            axs[2][_index].errorbar(x, y,  c=kwargs['c'], marker='', ls='-', lw=lw, alpha=alpha)
            axs[2][_index].errorbar(x, sy, c=kwargs['c'], marker='', ls=kwargs['ls'], lw=lwt)

            x = np.log10(tau)
            y = (sigma_r2[_index] - inital_velocity2_r[_index])/ v200**2

            # x = np.log10(tau + tau_0r)
            # y = sigma_r2[_index] / v200 ** 2

            mask = ~np.isnan(y)
            x = x[mask]
            y = y[mask]
            sy = np.log10(savgol_filter(y, window, poly_n, mode='interp'))
            y = np.log10(y)
            axs[3][_index].errorbar(x, y, c=kwargs['c'], marker='', ls='-', lw=lw, alpha=alpha)
            axs[3][_index].errorbar(x, sy, c=kwargs['c'], marker='', ls=kwargs['ls'], lw=lwt)

    # axs[1][0].set_ylabel(r'$\log [(v_c - \overline{v}_\phi) / V_{200}] $')
    # axs[0][0].set_ylabel(r'$\log (\sigma_\phi^2 / V_{200}^2) $')
    # axs[2][0].set_ylabel(r'$\log (\sigma_z^2 / V_{200}^2) $')
    # axs[3][0].set_ylabel(r'$\log (\sigma_R^2 / V_{200}^2) $')
    # axs[3][0].set_xlabel(r'$\log( \tau_0 + \tau)$')
    # axs[3][1].set_xlabel(r'$\log( \tau_0 + \tau)$')
    # axs[3][2].set_xlabel(r'$\log( \tau_0 + \tau)$')

    axs[1][0].set_ylabel(r'$\log [(v_c - v_i) / V_{200}] $')
    axs[0][0].set_ylabel(r'$\log (\Delta \sigma_\phi^2 / V_{200}^2) $')
    axs[2][0].set_ylabel(r'$\log (\Delta \sigma_z^2 / V_{200}^2) $')
    axs[3][0].set_ylabel(r'$\log (\Delta \sigma_R^2 / V_{200}^2) $')
    axs[3][0].set_xlabel(r'$\log(\tau)$')
    axs[3][1].set_xlabel(r'$\log(\tau)$')
    axs[3][2].set_xlabel(r'$\log(\tau)$')

    # axs[0][0].text(0.01, 0.4, r'$R = R_{1/2}$', va='top')
    # axs[1][0].text(0.01, 0.4, r'$R = R_{1/2}$', va='top')
    # axs[0][1].text(0.01, 0.4, r'$R = R_{1/2}$', va='top')
    # axs[1][1].text(0.01, 0.4, r'$R = R_{1/2}$', va='top')
    # axs[0][1].text(0.01, 0.4, r'Matched to $\rho (R_{1/2})$ for $c=10$', va='top')
    # axs[1][1].text(0.01, 0.4, r'Matched to $\rho (R_{1/2})$ for $c=10$', va='top')
    # axs[0][2].text(0.01, 0.4, r'$R = R_{1/2}$', va='top')
    # axs[1][2].text(0.01, 0.4, r'$R = R_{1/2}$', va='top')
    #
    # axs[1][0].text(0.01, 0.01, r'$v_c$', va='bottom')
    # axs[0][0].text(0.01, -0.2, r'$\sigma_{\mathrm{DM}}^2$', va='bottom')

    axs[0][1].legend([(lines.Line2D([0, 1], [0, 1], color='brown', ls='--', linewidth=2)),
                      # (lines.Line2D([0, 1], [0, 1], color='magenta', ls='-.', linewidth=2)),
                      (lines.Line2D([0, 1], [0, 1], color='grey', ls=(0, (3, 1, 1, 1, 1, 1)), linewidth=2)),
                      (lines.Line2D([0, 1], [0, 1], color='grey', ls='-', linewidth=2)),
                      (lines.Line2D([0, 1], [0, 1], color='grey', ls='--', linewidth=2))],
                      [r'$\beta = 0$', #r'$\beta = -1$',
                       r'$c = 7$', r'$c = 10$', r'$c = 15$'],
                     loc='lower right', frameon=False, labelspacing=0.2)

    # x0 = -1.95
    # dx = 0.21
    # y0 = 0.016
    # axs[1][0].text(y0, x0 + 4 * dx,
    #                r'$m_{\mathrm{DM}} = 10^{6.0}$M$_\odot, \mathrm{N}_{\mathrm{DM}} = 1.8 \times 10^6$',
    #                va='bottom', c='C4')
    # axs[1][0].text(y0, x0 + 3 * dx,
    #                r'$m_{\mathrm{DM}} = 10^{6.5}$M$_\odot, \mathrm{N}_{\mathrm{DM}} = 5.8 \times 10^5$',
    #                va='bottom', c='C3')
    # axs[1][0].text(y0, x0 + 2 * dx,
    #                r'$m_{\mathrm{DM}} = 10^{7.0}$M$_\odot, \mathrm{N}_{\mathrm{DM}} = 1.8 \times 10^5$',
    #                va='bottom', c='C2')
    # axs[1][0].text(y0, x0 + 1 * dx,
    #                r'$m_{\mathrm{DM}} = 10^{7.5}$M$_\odot, \mathrm{N}_{\mathrm{DM}} = 5.8 \times 10^4$',
    #                va='bottom', c='C1')
    # axs[1][0].text(y0, x0 + 0 * dx,
    #                r'$m_{\mathrm{DM}} = 10^{8.0}$M$_\odot, \mathrm{N}_{\mathrm{DM}} = 1.8 \times 10^4$',
    #                va='bottom', c='C0')
    # 
    # axs[1][1].text(y0, x0 + 1 * dx,
    #                r'$m_{\mathrm{DM}} = 10^{7.0}$M$_\odot, \mathrm{N}_{\mathrm{DM}} = 1.8 \times 10^5$',
    #                va='bottom', c='C2')
    # axs[1][1].text(y0, x0 + 0 * dx,
    #                r'$m_{\mathrm{DM}} = 10^{8.0}$M$_\odot, \mathrm{N}_{\mathrm{DM}} = 1.8 \times 10^4$',
    #                va='bottom', c='C0')
    # axs[1][2].text(y0, x0 + 1 * dx,
    #                r'$m_{\mathrm{DM}} = 10^{7.0}$M$_\odot, \mathrm{N}_{\mathrm{DM}} = 1.8 \times 10^5$',
    #                va='bottom', c='C2')
    # axs[1][2].text(y0, x0 + 0 * dx,
    #                r'$m_{\mathrm{DM}} = 10^{8.0}$M$_\odot, \mathrm{N}_{\mathrm{DM}} = 1.8 \times 10^4$',
    #                va='bottom', c='C0')

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    # np.seterr(all='warn')

    return


def plot_time_evolution_v200(save_name_append='diff', save=True, name=''):
    galaxy_name_list = [
        'mu_5/fdisk_0p01_lgMdm_6p0_V200-50kmps',
        'mu_5/fdisk_0p01_lgMdm_5p5_V200-50kmps',
        'mu_5/fdisk_0p01_lgMdm_5p0_V200-50kmps',
        'mu_5/fdisk_0p01_lgMdm_4p5_V200-50kmps',
        'mu_5/fdisk_0p01_lgMdm_4p0_V200-50kmps',
        'mu_5/fdisk_0p01_lgMdm_7p0_V200-100kmps',
        'mu_5/fdisk_0p01_lgMdm_6p5_V200-100kmps',
        'mu_5/fdisk_0p01_lgMdm_6p0_V200-100kmps',
        'mu_5/fdisk_0p01_lgMdm_5p5_V200-100kmps',
        'mu_5/fdisk_0p01_lgMdm_5p0_V200-100kmps',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
        'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
        'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
        'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
        'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps',
        'mu_5/fdisk_0p01_lgMdm_9p0_V200-400kmps',
        'mu_5/fdisk_0p01_lgMdm_8p5_V200-400kmps',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-400kmps',
        'mu_5/fdisk_0p01_lgMdm_7p5_V200-400kmps',
        'mu_5/fdisk_0p01_lgMdm_7p0_V200-400kmps',
    ]
    snaps_list = [400, 400, 101, 101, 101, 400, 400, 101, 101, 101, 400, 400, 101, 101, 101, 400, 400, 101, 101, 101, ]
    # galaxy_name_list = [
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps',
    #                     ]
    # snaps_list = [400,400,101,101,101,]

    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(18, 10, forward=True)
    # fig.set_size_inches(18, 22, forward=True)
    widths = [1, 1, 1]
    heights = [1, 1]
    spec = fig.add_gridspec(ncols=3, nrows=2, width_ratios=widths, height_ratios=heights)
    # heights = [1,1,1,1]
    # spec = fig.add_gridspec(ncols=3, nrows=4, width_ratios=widths, height_ratios=heights)

    pth_w = [path_effects.Stroke(linewidth=4, foreground='white'), path_effects.Normal()]

    # xlims = [-3.8, -0.4]
    # xlims = [0, 0.05]
    # ylims = [-2.7, 0.3]
    
    eps = 1e-3
    xlims = [-eps, 0.06 - eps]
    ylims = [-2.0 + eps, 0.5 - eps]
    
    axs = []
    for row in range(len(heights)):
        axs.append([])
        for col in range(len(widths)):
            axs[row].append(fig.add_subplot(spec[row, col]))
            axs[row][col].set_xlim(xlims)
            axs[row][col].set_ylim(ylims)
            # axs[row][col].set_aspect('equal')
            if col != 0: axs[row][col].set_yticklabels([])
            if row != len(heights) - 1:
                axs[row][col].set_xticklabels([])
            # axs[row][col].set_yticks([-2, -1, 0])

    fig.subplots_adjust(hspace=0.02, wspace=0.02)

    run = []
    # print('\nalpha=0, beta=0, gamma=0')
    # run.append(find_least_log_squares_best_fit(save_name_append, mcmc=False, fixed_a=0, fixed_b=0, fixed_g=0))
    print('\ngamma=0, beta=0')
    run.append(find_least_log_squares_best_fit(save_name_append, mcmc=False, fixed_b=0, fixed_g=0))
    # run.append(find_least_log_squares_best_fit(save_name_append, mcmc=False, fixed_b=0))
    # print(run)
    print('\ngamma=0, beta=0')
    run.append(find_least_log_squares_best_fit(save_name_append, mcmc=False, fixed_b=0, fixed_g=0))

    # run_ls = [':', '-', '--']
    # run_c = ['grey', 'k', 'k']
    # run_funcs = [get_powerlaw_velocity2_array, get_powerlaw_velocity2_array, get_theory_velocity2_array]
    # run_labels = [r'LO85', r'Linear heating', r'Asymptotic heating']
    # run_zorder = [10, -10, 10]
    
    run_ls = ['-.', '--']
    run_c = ['k', 'k']
    run_funcs = [get_powerlaw_velocity2_array, get_theory_velocity2_array]
    run_labels0 = [r'$\sigma_\phi^2 \propto t$', r'equation (6)']
    run_labels1 = [r'$\overline{v}_\phi \propto t$', r'equation (11)']
    run_zorder = [10, 10]

    theory_best_v_list = []
    theory_best_z2_list = []
    theory_best_r2_list = []
    theory_best_p2_list = []

    # (inital_velocity2_array_v, inital_velocity2_array_z, inital_velocity2_array_r, inital_velocity2_array_p
    #  ) = smooth_burn_ins(1e-2, 400, save_name_append, burn_in, bin_edges)

    galaxy_name = 'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps'
    kwargs = get_name_kwargs(galaxy_name)
    snaps = 400

    bin_edges = get_bin_edges(save_name_append, galaxy_name=galaxy_name)
    analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)
    analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                         save_name_append=save_name_append, galaxy_name=galaxy_name)
    # (a_h, vmax) = get_hernquist_params(galaxy_name)
    # analytic_v_circ = get_analytic_hernquist_circular_velocity(bin_edges, vmax, a_h, save_name_append)

    v200 = get_v_200(galaxy_name=galaxy_name)

    (t_c_dm, _, delta_dm, upsilon_dm, upsilon_circ
     ) = get_constants(bin_edges, save_name_append, 1e-2, galaxy_name=galaxy_name)

    theory_times = np.logspace(-5, 3, 1000)
    log_theory_times = np.log10(theory_times)
    fifty = len(theory_times)
    o = np.ones(fifty)
    tau_t = get_tau_array(theory_times, t_c_dm[:, np.newaxis])
    # tau_t = [get_tau_array(theory_times, t_c_dm[i]) for i in range(len(bin_edges)//2)]

    (inital_velocity_v, inital_velocity_z, inital_velocity_r, inital_velocity_p
     ) = smooth_burn_ins(kwargs['mdm'], snaps, save_name_append, BURN_IN, bin_edges, galaxy_name=galaxy_name)

    experiment_v = []

    for j, (ln_Lambda_ks, alphas, betas, gammas) in enumerate(run):

        # yaxis
        # theory_best_v_list.append([run_funcs[j](inital_velocity_v[i]*o, 0*o, #analytic_v_circ[i] - inital_velocity_v[i], #0*o,
        # theory_best_v_list.append([run_funcs[j](analytic_v_circ[i] * o, 0 * o,
        theory_best_v_list.append([run_funcs[j](v200 * o, 0 * o,
                                                theory_times, t_c_dm[i] * o, delta_dm[i] * o, np.sqrt(upsilon_circ[i]) * o,
                                                ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
                                   for i in range(len(bin_edges) // 2)])
        theory_best_p2_list.append([run_funcs[j](v200 ** 2 * o, 0 * o,
                                                 theory_times, t_c_dm[i] * o, delta_dm[i] * o, upsilon_dm[i] * o,
                                                 ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])
                                    for i in range(len(bin_edges) // 2)])

        # theory_best_z2_list.append([run_funcs[j](analytic_dispersion[i]**2 *o, 0*o,
        #                                          theory_times, t_c_dm[i]*o, delta_dm[i] *o, upsilon_dm[i] *o,
        #                                          ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
        #                             for i in range(len(bin_edges)//2)])
        #
        # theory_best_r2_list.append([run_funcs[j](analytic_dispersion[i] ** 2 * o, 0 * o,
        #                                          theory_times, t_c_dm[i] * o, delta_dm[i] * o, upsilon_dm[i] * o,
        #                                          ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
        #                             for i in range(len(bin_edges) // 2)])


        tau_heat_z2 = np.array([get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                ln_Lambda_ks[1], alphas[1], betas[1], gammas[1]) for i in range(len(bin_edges) // 2)])
        tau_heat_r2 = np.array([get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                ln_Lambda_ks[2], alphas[2], betas[2], gammas[2]) for i in range(len(bin_edges) // 2)])
        tau_heat_p2 = np.array([get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                ln_Lambda_ks[3], alphas[3], betas[3], gammas[3]) for i in range(len(bin_edges) // 2)])

        # tau_heat_experiment[i] = ((tau_heat_z2[i] + tau_heat_r2[i] + tau_heat_p2[i]
        #                     ) * np.sqrt(upsilon_dm[i]) / np.sqrt(upsilon_circ[i]) / 3) #
        # tau_heat_experiment = tau_heat_r2 * 1.06126839
        tau_heat_experiment = tau_heat_p2 * 0.74987599

        tau_0_on_tau_heat = np.array([get_tau_zero_array(v200 * o, 0 * o,
                                      np.sqrt(upsilon_circ[i])) for i in range(len(bin_edges) // 2)])

        if run_funcs[j] == get_theory_velocity2_array:
            experiment_v.append(np.array([v200 * np.sqrt(upsilon_circ[i]) ** 2 * (1 - np.exp(
                - np.power(((theory_times / t_c_dm[i]) / tau_heat_experiment[i]) + tau_0_on_tau_heat[i], 1 + gammas[0])))
                for i in range(len(bin_edges) // 2)]))
            
            print(np.shape(experiment_v[-1]))

        else: #run_funcs == get_powerlaw_velocity2_array
            experiment_v.append(np.array([v200 * np.sqrt(upsilon_circ[i]) ** 2 * (
                np.power(((theory_times / t_c_dm[i]) / tau_heat_experiment[i]),
                         1 + gammas[0])) for i in range(len(bin_edges) // 2)]))

            print(np.shape(experiment_v[-1]))
            
        # experiment_v = analytic_v_circ[:, np.newaxis] - experiment_v

        # plot
        ls = run_ls[j]

        for i in range(len(bin_edges) // 2):
            axs[0][i].errorbar(tau_t[i], np.log10(theory_best_p2_list[j][i]) - 2 * np.log10(v200),
                               ls=ls, lw=3, c=run_c[j], label=run_labels0[j], path_effects=pth_w, zorder=run_zorder[j])
            # if j!= 0:
            # axs[1][i].errorbar(tau_t[i], np.log10(theory_best_v_list[j][i]) - np.log10(v200),
            #                    ls=ls, lw=3, c=run_c[j], label=run_labels1[j], path_effects=pth_w, zorder=run_zorder[j])
            axs[1][i].errorbar(tau_t[i], np.log10(experiment_v[j][i]) - np.log10(v200),
                               ls=ls, lw=3, c=run_c[j], label=run_labels1[j], path_effects=pth_w, zorder=run_zorder[j])

          # axs[2][i].errorbar(np.log10(tau_t[i]), np.log10(theory_best_z2_list[j][i]) - 2*np.log10(v200),
            #                    ls=ls, lw=3, c=run_c[j], label=run_labels[j], path_effects=pth_w, zorder=5)
          # axs[3][i].errorbar(np.log10(tau_t[i]), np.log10(theory_best_r2_list[j][i]) - 2*np.log10(v200),
            #                    ls=ls, lw=3, c=run_c[j], label=run_labels[j], path_effects=pth_w, zorder=5)

    # maximum cut off
    ls = '--'
    c = 'grey'
    alpha = 0.5
    linewidth = 5
    for i in range(len(bin_edges) // 2):
        axs[0][i].errorbar(theory_times, 0 * o + 2 * np.log10(analytic_dispersion[i]) - 2 * np.log10(v200),
                           ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
        axs[1][i].errorbar(theory_times, 0 * o + np.log10(analytic_v_circ[i]) - np.log10(v200),
                           # axs[1][i].errorbar(log_theory_times, 0*o + np.log10(inital_velocity_v[i]) - np.log10(v200),
                           ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)

        # axs[2][i].errorbar(log_theory_times, 0*o + 2*np.log10(analytic_dispersion[i]) - 2*np.log10(v200),
        #                    ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)
        # axs[3][i].errorbar(log_theory_times, 0*o + 2*np.log10(analytic_dispersion[i]) - 2*np.log10(v200),
        #                    ls=ls, c=c, alpha=alpha, linewidth=linewidth, zorder=-2)

    # #fits for comparison
    # print('\nbeta=0, gamma=0 (fiducial)')
    # (ln_Lambda_ks, alphas, betas, gammas
    #  ) = find_least_log_squares_best_fit(
    #   save_name_append, mcmc=False, fixed_b=0, fixed_g=0)
    #   # save_name_append, mcmc=False, fixed_g=0)

    col_list = ['C0', 'C1', 'C2', 'C3', 'C4']

    # put data on plots
    for l, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = get_name_kwargs(galaxy_name)

        v200 = get_v_200(galaxy_name)

        (mean_v_R, mean_v_phi,
         sigma_z, sigma_R, sigma_phi
         ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges)

        bin_edges = get_bin_edges(save_name_append, galaxy_name=galaxy_name)

        # work out constatns
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, kwargs['mdm'], galaxy_name=galaxy_name)

        analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                                 save_name_append=save_name_append, galaxy_name=galaxy_name)
        analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)

        mean_v_phi = mean_v_phi.T
        sigma_z2 = sigma_z.T ** 2
        sigma_r2 = sigma_R.T ** 2
        sigma_p2 = sigma_phi.T ** 2

        (inital_velocity_v, inital_velocity2_z, inital_velocity2_r, inital_velocity2_p
         ) = smooth_burn_ins(kwargs['mdm'], snaps, save_name_append, BURN_IN, bin_edges, galaxy_name=galaxy_name)
        inital_velocity2_z **= 2
        inital_velocity2_r **= 2
        inital_velocity2_p **= 2

        for i in range(len(bin_edges) // 2):
            time = np.linspace(0, T_TOT, snaps + 1)
            time = time - time[BURN_IN]

            tau = get_tau_array(time, t_c_dm[i])

            alpha = 0.25
            ms = 3

            # x = np.log10(tau)
            # y = np.log10((sigma_p2[i] - inital_velocity2_p[i]) / v200**2)

            tau_vir = get_tau_heat_array(delta_dm[i], upsilon_dm[i], run[1][0][3], run[1][1][3], run[1][2][3],
                                         run[1][3][3])
            # tau_0 = tau_vir * np.log(
            #     analytic_dispersion[i] ** 2 / (analytic_dispersion[i] ** 2 - inital_velocity2_p[i]))
            tau_0 = tau_vir * np.log(
                analytic_dispersion[i] ** 2 / (analytic_dispersion[i] ** 2 - inital_velocity2_p[i]))
            x = tau + tau_0
            y = np.log10(sigma_p2[i] / v200 ** 2)

            axs[0][i].errorbar(x, y, c=col_list[l % 5], marker=kwargs['vmarker'], ls='', alpha=alpha, ms=ms)

            # x = np.log10(tau)
            # y = np.log10((inital_velocity_v[i] - mean_v_phi[i]) / v200)

            tau_vir = get_tau_heat_array(delta_dm[i], np.sqrt(upsilon_circ[i]), run[1][0][0], run[1][1][0], run[1][2][0],
                                         run[1][3][0])
            # tau_0 = tau_vir * np.log(analytic_v_circ[i] / inital_velocity_v[i])
            tau_0 = tau_vir * np.log(analytic_v_circ[i] / inital_velocity_v[i])

            x = tau + tau_0
            y = np.log10((analytic_v_circ[i] - mean_v_phi[i]) / v200)
            # y = np.log10((inital_velocity_v[i] - mean_v_phi[i]) / v200)

            axs[1][i].errorbar(x, y, c=col_list[l % 5], marker=kwargs['vmarker'], ls='', alpha=alpha, ms=ms)

            # tau_vir = get_tau_heat_array(delta_dm[i], upsilon_dm[i], run[1][0][1], run[1][1][1], run[1][2][1], run[1][3][1])
            # tau_0 = tau_vir * np.log(analytic_dispersion[i]**2 / (analytic_dispersion[i]**2 - inital_velocity2_z[i]))
            # x = np.log10(tau + tau_0)
            # y = np.log10(sigma_z2[i] / v200**2)
            #
            # axs[2][i].errorbar(x, y, c=col_list[l%5], marker=kwargs['vmarker'], ls='', alpha=alpha, ms=ms)
            #
            # tau_vir = get_tau_heat_array(delta_dm[i], upsilon_dm[i], run[1][0][2], run[1][1][2], run[1][2][2], run[1][3][2])
            # tau_0 = tau_vir * np.log(analytic_dispersion[i]**2 / (analytic_dispersion[i]**2 - inital_velocity2_r[i]))
            # x = np.log10(tau + tau_0)
            # y = np.log10(sigma_r2[i] / v200**2)
            #
            # axs[3][i].errorbar(x, y, c=col_list[l%5], marker=kwargs['vmarker'], ls='', alpha=alpha, ms=ms)

            if i == 1 and l % 5 == 0:
                x = xlims[0] + 0.48 * (xlims[1] - xlims[0])
                x1 = xlims[0] + 0.53 * (xlims[1] - xlims[0])
                # y = ylims[0] + (0.34 - l * 0.09 / 5) * (ylims[1] - ylims[0])
                y = -1.00 + 0.26 * -l/5
                axs[0][1].errorbar(x, y, marker=kwargs['vmarker'], c='k', ms=8)
                axs[0][1].text(x1, y, r'$V_{200} = $' + str(v200) + ' km/s', va='center')

    axs[0][0].legend(loc='lower right', frameon=False,
                     handletextpad=0.5, borderpad=0.2)
    axs[1][0].legend(loc='lower right', frameon=False,
                     handletextpad=0.5, borderpad=0.2)
    # axs[1][0].legend(loc='lower right', frameon=False,
    #                  handletextpad=0.2, borderpad=0.2)

    x0 = xlims[0] + 0.001
    dx = 0.1 * (xlims[1] - xlims[0])
    y0 = ylims[0] + 0.05 * (ylims[1] - ylims[0])
    axs[1][1].text(x0 + 1 * dx, y0, r'$\mathrm{N}_{\mathrm{DM}}$', va='bottom', ha='center', c='C4')
    axs[1][1].text(x0 + 3 * dx, y0, r'$\mathrm{N}_{\mathrm{DM}}$', va='bottom', ha='center',c='C3')
    axs[1][1].text(x0 + 5 * dx, y0, r'$\mathrm{N}_{\mathrm{DM}}$', va='bottom', ha='center',c='C2')
    axs[1][1].text(x0 + 7 * dx, y0, r'$\mathrm{N}_{\mathrm{DM}}$', va='bottom', ha='center',c='C1')
    axs[1][1].text(x0 + 9 * dx, y0, r'$\mathrm{N}_{\mathrm{DM}}$', va='bottom', ha='center',c='C0')
    axs[1][1].text(x0 + 2 * dx, y0, r'$>$', va='bottom', ha='center')
    axs[1][1].text(x0 + 4 * dx, y0, r'$>$', va='bottom', ha='center')
    axs[1][1].text(x0 + 6 * dx, y0, r'$>$', va='bottom', ha='center')
    axs[1][1].text(x0 + 8 * dx, y0, r'$>$', va='bottom', ha='center')

    p1 = 0.05
    xx = xlims[0] + p1 * (xlims[1] - xlims[0])
    yy = ylims[0] + (1 - p1) * (ylims[1] - ylims[0])
    axs[0][0].text(xx, yy, r'$R = R_{1/4}$', va='top')
    axs[0][1].text(xx, yy, r'$R = R_{1/2}$', va='top')
    axs[0][2].text(xx, yy, r'$R = R_{3/4}$', va='top')
    axs[1][0].text(xx, yy, r'$R = R_{1/4}$', va='top')
    axs[1][1].text(xx, yy, r'$R = R_{1/2}$', va='top')
    axs[1][2].text(xx, yy, r'$R = R_{3/4}$', va='top')

    axs[1][0].text(xx, -0.03, r'$V_c$', va='bottom')
    axs[0][0].text(xx, -0.2, r'$\sigma_{\mathrm{DM}}$', va='bottom')

    axs[0][0].set_ylabel(r'$\log (\sigma_\phi^2 / V_{200}^2) $')
    axs[1][0].set_ylabel(r'$\log ([V_c - \overline{v}_\phi] / V_{200}) $')
    # axs[2][0].set_ylabel(r'$\log (\sigma_z^2 / V_{200}^2) $')
    # axs[3][0].set_ylabel(r'$\log (\sigma_R^2 / V_{200}^2) $')
    axs[1][0].set_xlabel(r'$t / t_c $')
    axs[1][1].set_xlabel(r'$t / t_c $')
    axs[1][2].set_xlabel(r'$t / t_c $')
    # axs[3][0].set_xlabel(r'$\log (\Delta \tau) $')
    # axs[3][1].set_xlabel(r'$\log (\Delta \tau) $')
    # axs[3][2].set_xlabel(r'$\log (\Delta \tau) $')

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    # np.seterr(all='warn')

    return

###############################################################################
# verification of model plots

def plot_cross_terms(galaxy_name_list, snaps_list, save_name_append='diff',
                     save=True, name=''):
    # set up plot
    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(6, 10, forward=True)
    widths = [1]
    heights = [1, 1, 1]
    spec = fig.add_gridspec(ncols=1, nrows=3, width_ratios=widths,
                            height_ratios=heights)

    axs = []
    for row in range(len(heights)):
        axs.append([])
        for col in range(len(widths)):
            axs[row].append(fig.add_subplot(spec[row, col]))
            axs[row][col].set_xlim([-4.5, -0.5])
            axs[row][col].set_ylim([-0.495, 0.495])
            if col != 0: axs[row][col].set_yticklabels([])
            if row != len(heights) - 1: axs[row][col].set_xticklabels([])

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    running_median = lambda x, N: np.array([np.median(x[i:i + N]) for i in range(len(x) - N)])
    running_mean = lambda x, N: np.array([np.mean(x[i:i + N]) for i in range(len(x) - N)])

    #
    bin_edges = get_bin_edges(save_name_append)

    # put data on plots
    for galaxy_name, snaps in zip(galaxy_name_list, snaps_list):
        kwargs = get_name_kwargs(galaxy_name)

        (mean_v_R, mean_v_phi,
         sigma_z, sigma_R, sigma_phi
         ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges)

        (vRvz, vRvphi, vzvphi) = get_cross_terms(galaxy_name, save_name_append, snaps, bin_edges)

        # work out constatns
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, kwargs['mdm'])

        for i in range(len(bin_edges) // 2):

            time = np.linspace(0, T_TOT, snaps + 1)
            time = time - time[BURN_IN]

            tau = get_tau_array(time, t_c_dm[i])

            ls = ''
            ms = 2
            alpha = 0.1
            axs[0][0].errorbar(np.log10(tau), vRvz[:, i] / (sigma_R[:, i] * sigma_z[:, i]),
                               marker=kwargs['marker'], c=kwargs['cmap']((i + 1) / 3),
                               ls=ls, ms=ms, alpha=alpha, zorder=-2)

            axs[1][0].errorbar(np.log10(tau), vRvphi[:, i] / (sigma_R[:, i] * sigma_phi[:, i]),
                               marker=kwargs['marker'], c=kwargs['cmap']((i + 1) / 3),
                               ls=ls, ms=ms, alpha=alpha, zorder=-2)

            axs[2][0].errorbar(np.log10(tau), vzvphi[:, i] / (sigma_z[:, i] * sigma_phi[:, i]),
                               marker=kwargs['marker'], c=kwargs['cmap']((i + 1) / 3),
                               ls=ls, ms=ms, alpha=alpha, zorder=-2)

            ls = '-'
            ms = 4
            alpha = 0.8
            N = 20
            if kwargs['c'] == 'C0' or kwargs['c'] == 'C1': N *= 4

            if i == 1 and kwargs['c'] == 'C0':
                axs[0][0].errorbar(np.log10(tau[N // 2: -N // 2]),
                                   running_mean(vRvz[:, i] / (sigma_R[:, i] * sigma_z[:, i]), N),
                                   marker=kwargs['marker'], c=kwargs['cmap']((i + 1) / 3),
                                   label=kwargs['mu'])
            else:
                axs[0][0].errorbar(np.log10(tau[N // 2: -N // 2]),
                                   running_mean(vRvz[:, i] / (sigma_R[:, i] * sigma_z[:, i]), N),
                                   marker=kwargs['marker'], c=kwargs['cmap']((i + 1) / 3),
                                   ls=ls, ms=ms, alpha=alpha, zorder=-1)

            if i == 1 and kwargs['marker'] == 'o':
                axs[1][0].errorbar(np.log10(tau[N // 2: -N // 2]),
                                   running_mean(vRvphi[:, i] / (sigma_R[:, i] * sigma_phi[:, i]), N),
                                   marker=kwargs['marker'], c=kwargs['cmap']((i + 1) / 3),
                                   ls=ls, ms=ms, alpha=alpha, zorder=-1,
                                   label=kwargs['lab'])
            else:
                axs[1][0].errorbar(np.log10(tau[N // 2: -N // 2]),
                                   running_mean(vRvphi[:, i] / (sigma_R[:, i] * sigma_phi[:, i]), N),
                                   marker=kwargs['marker'], c=kwargs['cmap']((i + 1) / 3),
                                   ls=ls, ms=ms, alpha=alpha, zorder=-1)

            if kwargs['c'] == 'C0' and kwargs['marker'] == 'o':
                axs[2][0].errorbar(np.log10(tau[N // 2: -N // 2]),
                                   running_mean(vzvphi[:, i] / (sigma_z[:, i] * sigma_phi[:, i]), N),
                                   marker=kwargs['marker'], c=kwargs['cmap']((i + 1) / 3),
                                   label=[r'$r_{1/4}$', r'$r_{1/2}$', r'$r_{3/4}$'][i])
            else:
                axs[2][0].errorbar(np.log10(tau[N // 2: -N // 2]),
                                   running_mean(vzvphi[:, i] / (sigma_z[:, i] * sigma_phi[:, i]), N),
                                   marker=kwargs['marker'], c=kwargs['cmap']((i + 1) / 3),
                                   ls=ls, ms=ms, alpha=alpha, zorder=-1)

    axs[0][0].legend(loc='lower left', labelspacing=-0.2, markerscale=1, handlelength=1,
                     handletextpad=0.0, borderpad=0.2)
    axs[1][0].legend(loc='lower left', labelspacing=-0.3, markerscale=1, handlelength=1,
                     handletextpad=0.0, borderpad=0.2, bbox_to_anchor=(-0.025, -0.05), ncol=3, columnspacing=0.0)
    axs[2][0].legend(loc='lower left', labelspacing=-0.2, markerscale=1, handlelength=1,
                     handletextpad=0.0, borderpad=0.2)

    log_tau_th = np.linspace(-5, 0.5)
    n = len(log_tau_th)
    axs[0][0].errorbar(log_tau_th, np.zeros(n),
                       ls='--', c='k', alpha=1, linewidth=1)
    axs[1][0].errorbar(log_tau_th, np.zeros(n),
                       ls='--', c='k', alpha=1, linewidth=1)
    axs[2][0].errorbar(log_tau_th, np.zeros(n),
                       ls='--', c='k', alpha=1, linewidth=1)

    axs[0][0].set_ylabel(r'$\langle v_R v_z \rangle / (\sigma_R \sigma_z)$')
    axs[1][0].set_ylabel(r'$\langle v_R (\Delta v_\phi) \rangle / (\sigma_R \sigma_\phi)$')
    axs[2][0].set_ylabel(r'$\langle v_z (\Delta v_\phi) \rangle / (\sigma_z \sigma_\phi)$')
    axs[2][0].set_xlabel(r'$\log \tau$')

    plt.show()
    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return

def plot_figure_of_merit(save_name_append='diff', name='', save=True):
    np.seterr(all='ignore')

    # set up plot
    fig = plt.figure()
    fig.set_size_inches(12, 2.4, forward=True)

    ax0 = plt.subplot(1, 4, 1)
    ax1 = plt.subplot(1, 4, 2)
    ax2 = plt.subplot(1, 4, 3)
    ax3 = plt.subplot(1, 4, 4)

    axs = [ax0, ax1, ax2, ax3]

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    alphas = np.linspace(-0.8, 0.4, 21)

    for i, ax in enumerate(axs):
        if i != 0: ax.set_yticklabels([])
        ax.set_xlim([alphas[0], alphas[-1]])
        ax.set_ylim([0, 0.1])
        pass

    (time_scale_array, scale_density_ratio_array, scale_velocity_ratio_array,
     time_array, velocity_scale_array_v, velocity_scale2_array_dispersion,
     measued_velocity_array_v, inital_velocity_array_v,
     measued_velocity2_array_z, inital_velocity2_array_z,
     measued_velocity2_array_r, inital_velocity2_array_r,
     measued_velocity2_array_p, inital_velocity2_array_p, n_array
     ) = get_raw_data(save_name_append=save_name_append)

    n = len(time_scale_array)

    measured = [measued_velocity_array_v, measued_velocity2_array_z,
                measued_velocity2_array_r, measued_velocity2_array_p]

    inital = [inital_velocity_array_v, inital_velocity2_array_z,
              inital_velocity2_array_r, inital_velocity2_array_p]

    v_scale = [velocity_scale_array_v, velocity_scale2_array_dispersion,
               velocity_scale2_array_dispersion, velocity_scale2_array_dispersion]

    for j, (measued_velocity2_array, inital_velocity2_array, velocity_scale2_array
            ) in enumerate(zip(measured, inital, v_scale)):

        data = (measued_velocity2_array, velocity_scale2_array, inital_velocity2_array,
                time_array, time_scale_array,
                scale_density_ratio_array, scale_velocity_ratio_array, n_array)

        # output
        out = np.zeros(len(alphas))

        # range of alphas
        for i, a in enumerate(alphas):
            fixed_args = [None, a, 0, None]
            start_loc = [50, 0]

            # the actual thing
            min_result = minimize(_log_like, start_loc, args=(data, fixed_args))

            # save best fit value
            out[i] = min_result.fun / n

        # plot
        if j == 1:
            axs[j].errorbar(alphas, out, c='C6', ls='-', label=r'best $\gamma$')
        else:
            axs[j].errorbar(alphas, out, c='C6', ls='-')

        # actual best
        fixed_args = [None, None, 0, None]
        start_loc = [50, 0, 0]
        # the actual thing
        min_result = minimize(_log_like, start_loc, args=(data, fixed_args))
        axs[j].scatter(min_result.x[1], min_result.fun / n, c='C6')

        # output
        out = np.zeros(len(alphas))

        for i, a in enumerate(alphas):
            fixed_args = [None, a, 0, 0]
            start_loc = [50]

            # the actual thing
            min_result = minimize(_log_like, start_loc, args=(data, fixed_args))

            # save best fit value
            out[i] = min_result.fun / n

        # plot
        if j == 1:
            axs[j].errorbar(alphas, out, c='C5', ls='--', label=r'$\gamma = 0$')
        else:
            axs[j].errorbar(alphas, out, c='C5', ls='--')

        # actual best
        fixed_args = [None, None, 0, 0]
        start_loc = [50, 0]
        # the actual thing
        min_result = minimize(_log_like, start_loc, args=(data, fixed_args))
        axs[j].scatter(min_result.x[1], min_result.fun / n, c='C5')

        # print(min_result.x)
        # print(min_result.message)

    for i in range(4):
        if i == 2:
            axs[i].axvline(0, 0, 1, ls=':', c='k', label=r'$\alpha=0$')
        else:
            axs[i].axvline(0, 0, 1, ls=':', c='k')

    # from Ludlow 2021 table
    axs[1].axvline(-0.356, 0, 1, ls='-.', c='C9')
    axs[2].axvline(-0.331, 0, 1, ls='-.', c='C9', label='L+21')

    # axs[0].set_ylabel('Least Squares [a.u.]')
    # axs[0].set_ylabel('$\chi^2$ [km/s]')
    # axs[1].set_ylabel('$\chi^2$ [(km/s)$^2$]')
    axs[0].set_ylabel('$\sigma^2[\log($model$)$\n  $-\log($data$)]$')
    # axs[0].set_ylabel('$\sigma[\log($model$)-\log($data$)]$')
    # axs[1].set_ylabel('$\chi^2$ [(km/s)$^2$]')
    # axs[0].set_ylabel('Figure of Merit')

    axs[0].set_xlabel(r'$\alpha_{v_\phi}$')
    axs[1].set_xlabel(r'$\alpha_{\sigma_z}$')
    axs[2].set_xlabel(r'$\alpha_{\sigma_R}$')
    axs[3].set_xlabel(r'$\alpha_{\sigma_\phi}$')

    axs[1].legend()
    axs[2].legend()

    np.seterr(all='warn')

    plt.show()
    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return

###############################################################################
# plots that used fitted values

def plot_velocity_profiles_after_t(galaxy_name_list, snaps_list, t=5,
                                   save_name_append='diff', name='', save=True):
    '''
    '''
    # set up plot
    fig = plt.figure()
    fig.set_size_inches(5, 14, forward=True)

    n_star_min = 20

    # This can be done more cleanly with arrays and stuff
    axm1 = plt.subplot(5, 1, 1)
    ax0 = plt.subplot(5, 1, 2)
    ax1 = plt.subplot(5, 1, 3)
    ax2 = plt.subplot(5, 1, 4)
    ax3 = plt.subplot(5, 1, 5)

    fig.subplots_adjust(hspace=0.03, wspace=0.03)

    axm1.set_xticklabels([])
    ax0.set_xticklabels([])
    ax1.set_xticklabels([])
    ax2.set_xticklabels([])

    xlim = [-2.8, 0.2]
    axm1.set_xlim(xlim)
    ax0.set_xlim(xlim)
    ax1.set_xlim(xlim)
    ax2.set_xlim(xlim)
    ax3.set_xlim(xlim)

    if save_name_append == 'diff':
        vlim = [-2.1, 0.6]
        slim = [-1.3, 0.2]
    elif save_name_append == 'cum':
        vlim = [-1.3, 0.3]
        slim = [-1.3, 0.3]

    axm1.set_ylim(slim)
    ax0.set_ylim(vlim)
    ax1.set_ylim(slim)
    ax2.set_ylim(slim)
    ax3.set_ylim(slim)

    # fits for comparison
    print('\nbeta=0, gamma=0 (fiducial)')
    (ln_Lambda_ks, alphas, betas, gammas
     ) = find_least_log_squares_best_fit(
        save_name_append, mcmc=False, fixed_b=0, fixed_g=0)

    # bins
    bin_edges = np.logspace(-1, 3, 31)

    # bins at a better resolution
    if save_name_append == 'diff':
        bin_centres = 10 ** ((np.log10(bin_edges[:-1]) + np.log10(bin_edges[1:])) / 2)
        bin_edges = np.roll(np.repeat(bin_edges, 2), -1)[:-2]

    elif save_name_append == 'cum':
        bin_centres = bin_edges.copy()
        bin_edge_holer = np.zeros(2 * len(bin_edges))
        bin_edge_holer[1::2] = bin_edges
        bin_edges = bin_edge_holer

    # halo properties
    v200 = get_v_200()
    r200 = get_r_200()

    analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0,
                                             v200=False, dm_dispersion=False, dm_v_circ=False,
                                             analytic_dispersion=True, analytic_v_c=False,
                                             save_name_append=save_name_append)

    analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0,
                                         v200=False, dm_dispersion=False, dm_v_circ=False,
                                         analytic_dispersion=False, analytic_v_c=True,
                                         save_name_append=save_name_append)

    # for plotting
    x_data = np.log10(bin_centres / r200)[:-1]

    # ics
    ics_galaxy_name = 'mu_25/fdisk_0p01_lgMdm_6p0_V200-200kmps'
    # load ics
    (file_name, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
     MassStar, PosStars, VelStars, IDStars, PotStars
     ) = load_nbody.load_and_align(ics_galaxy_name, snap='000')

    r = np.linalg.norm(PosStars, axis=1)

    # half mass radius dispersion
    interesting_radii = get_radii_that_are_interesting(r)
    if save_name_append == 'diff':
        half_mass_edges = get_dex_bins([interesting_radii[1]])
    else:
        half_mass_edges = [0, interesting_radii[1]]

    (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
     ) = get_dispersion_profiles(PosStars, VelStars, half_mass_edges)

    analytic_v_c_half_mass = get_velocity_scale(half_mass_edges, 0, 0, 0, 0, 0, 0,
                                                v200=False, dm_dispersion=False, dm_v_circ=False,
                                                analytic_dispersion=False, analytic_v_c=True,
                                                save_name_append=save_name_append)

    c = 'grey'
    # plot half mass radius dispersion
    axm1.scatter(np.log10(interesting_radii[1] / r200), np.log10(mean_v_phi / v200),
                 color=c)
    # ax0.scatter(np.log10(interesting_radii[1]/r200), np.log10((analytic_v_c_half_mass - mean_v_phi) / v200),
    #             color=c)
    ax1.scatter(np.log10(interesting_radii[1] / r200), np.log10(sigma_z / v200),
                color=c)
    ax2.scatter(np.log10(interesting_radii[1] / r200), np.log10(sigma_R / v200),
                color=c)
    ax3.scatter(np.log10(interesting_radii[1] / r200), np.log10(sigma_phi / v200),
                color=c)

    # calculate profile from here
    # mask out radii with too few stars
    value, _ = np.histogram(r, bin_edges[1::2])
    if save_name_append == 'diff':
        mask = (value > n_star_min)
    else:
        mask = (np.cumsum(value) > n_star_min)

    # calculate dispersion profile
    (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
     ) = get_dispersion_profiles(PosStars, VelStars, bin_edges[:-2])

    c = 'grey'
    ls = '-'
    # plot profile
    axm1.errorbar(x_data[mask], np.log10(mean_v_phi[mask] / v200),
                  ls=ls, c=c)
    # ax0.errorbar(x_data[mask], np.log10(np.abs((analytic_v_circ[:-1][mask] - mean_v_phi[mask]))/v200),
    #               ls=ls, c=c)
    ax1.errorbar(x_data[mask], np.log10(sigma_z[mask] / v200),
                 ls=ls, c=c)
    ax2.errorbar(x_data[mask], np.log10(sigma_R[mask] / v200),
                 ls=ls, c=c)
    ax3.errorbar(x_data[mask], np.log10(sigma_phi[mask] / v200),
                 ls=ls, c=c)

    # each galaxy
    for galaxy_name, snaps in zip(galaxy_name_list, snaps_list):
        kwargs = get_name_kwargs(galaxy_name)
        snap = int(round(t / T_TOT * snaps))

        # load a snap
        (file_name, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
         MassStar, PosStars, VelStars, IDStars, PotStars
         ) = load_nbody.load_and_align(galaxy_name, snap='{0:03d}'.format(int(snap)))

        r = np.linalg.norm(PosStars, axis=1)

        # half mass radius dispersion
        interesting_radii = get_radii_that_are_interesting(r)
        if save_name_append == 'diff':
            half_mass_edges = get_dex_bins([interesting_radii[1]])
        else:
            half_mass_edges = [0, interesting_radii[1]]

        (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
         ) = get_dispersion_profiles(PosStars, VelStars, half_mass_edges)

        analytic_v_c_half_mass = get_velocity_scale(half_mass_edges, 0, 0, 0, 0, 0, 0,
                                                    v200=False, dm_dispersion=False, dm_v_circ=False,
                                                    analytic_dispersion=False, analytic_v_c=True,
                                                    save_name_append=save_name_append)

        # plot half mass radius dispersion
        if kwargs['ls'] == '-':
            axm1.scatter(np.log10(interesting_radii[1] / r200), np.log10(mean_v_phi / v200),
                         color=kwargs['c'])
            ax0.scatter(np.log10(interesting_radii[1] / r200), np.log10((analytic_v_c_half_mass - mean_v_phi) / v200),
                        color=kwargs['c'])
            ax1.scatter(np.log10(interesting_radii[1] / r200), np.log10(sigma_z / v200),
                        color=kwargs['c'])
            ax2.scatter(np.log10(interesting_radii[1] / r200), np.log10(sigma_R / v200),
                        color=kwargs['c'])
            ax3.scatter(np.log10(interesting_radii[1] / r200), np.log10(sigma_phi / v200),
                        color=kwargs['c'])

        # calculate profile from here
        # mask out radii with too few stars
        value, _ = np.histogram(r, bin_edges[1::2])
        if save_name_append == 'diff':
            mask = (value > n_star_min)
        else:
            mask = (np.cumsum(value) > n_star_min)

        # calculate dispersion profile
        (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
         ) = get_dispersion_profiles(PosStars, VelStars, bin_edges[:-2])

        # average over several snaps. should be odd
        smear_number = 3

        for i in range(smear_number):
            # load a snap
            (file_name, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_nbody.load_and_align(galaxy_name, snap='{0:03d}'.format(int(snap + i - smear_number // 2)))

            (v_R, v_phi, s_z, s_R, s_phi
             ) = get_dispersion_profiles(PosStars, VelStars, bin_edges[:-2])

            mean_v_R += v_R
            mean_v_phi += v_phi
            sigma_z += s_z
            sigma_R += s_R
            sigma_phi += s_phi

        mean_v_R /= (smear_number + 1)
        mean_v_phi /= (smear_number + 1)
        sigma_z /= (smear_number + 1)
        sigma_R /= (smear_number + 1)
        sigma_phi /= (smear_number + 1)

        # plot profile
        if kwargs['c'] == 'C0':
            axm1.errorbar(x_data[mask], np.log10(mean_v_phi[mask] / v200),
                          ls=kwargs['ls'], c=kwargs['c'], label=kwargs['mu'])
        else:
            axm1.errorbar(x_data[mask], np.log10(mean_v_phi[mask] / v200),
                          ls=kwargs['ls'], c=kwargs['c'])
        ax0.errorbar(x_data[mask], np.log10(np.abs((analytic_v_circ[:-1][mask] - mean_v_phi[mask])) / v200),
                     ls=kwargs['ls'], c=kwargs['c'])
        ax1.errorbar(x_data[mask], np.log10(sigma_z[mask] / v200),
                     ls=kwargs['ls'], c=kwargs['c'])
        ax2.errorbar(x_data[mask], np.log10(sigma_R[mask] / v200),
                     ls=kwargs['ls'], c=kwargs['c'])
        ax3.errorbar(x_data[mask], np.log10(sigma_phi[mask] / v200),
                     ls=kwargs['ls'], c=kwargs['c'])

        # text
        twenty = 18
        ax2.text(x_data[twenty], kwargs['yh'], kwargs['lab'],
                 ha='left', va='center', color=kwargs['c'])

        # theory
        # work out constatns
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, kwargs['mdm'])

        # burn ins
        (inital_velocity_v, inital_velocity2_z, inital_velocity2_r, inital_velocity2_p
         ) = smooth_zero_snap(kwargs['mdm'], snaps, save_name_append, bin_edges)

        inital_velocity_v = np.amin((inital_velocity_v, analytic_v_circ), axis=0)

        inital_velocity2_z **= 2
        inital_velocity2_r **= 2
        inital_velocity2_p **= 2

        theory_best_v = np.zeros((len(bin_centres), 1))
        theory_best_z2 = np.zeros((len(bin_centres), 1))
        theory_best_r2 = np.zeros((len(bin_centres), 1))
        theory_best_p2 = np.zeros((len(bin_centres), 1))

        for i in range(len(bin_centres)):
            theory_best_v[i, :] = get_theory_velocity2_array(analytic_v_circ[i],
                                                             analytic_v_circ[i] - inital_velocity_v[i],
                                                             np.array([t]), t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                             ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
            theory_best_z2[i, :] = get_theory_velocity2_array(analytic_dispersion[i] ** 2, inital_velocity2_z[i],
                                                              np.array([t]), t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                              ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
            theory_best_r2[i, :] = get_theory_velocity2_array(analytic_dispersion[i] ** 2, inital_velocity2_r[i],
                                                              np.array([t]), t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                              ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
            theory_best_p2[i, :] = get_theory_velocity2_array(analytic_dispersion[i] ** 2, inital_velocity2_p[i],
                                                              np.array([t]), t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                              ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

        theory_best_v = inital_velocity_v[:, np.newaxis] - theory_best_v

        if kwargs['ls'] == ':' or (kwargs['ls'] == '-' and kwargs['c'] == 'C4'):
            alpha = 0.5
            ls = '-.'
            if kwargs['c'] == 'C0':
              axm1.errorbar(x_data, np.log10(theory_best_v[:-1, 0] / v200),
                            ls=ls, c=kwargs['c'], alpha=alpha, label='Model')
            else:
                axm1.errorbar(x_data, np.log10(theory_best_v[:-1, 0] / v200),
                              ls=ls, c=kwargs['c'], alpha=alpha)
            ax0.errorbar(x_data, np.log10(np.abs((analytic_v_circ[:-1] - theory_best_v[:-1, 0])) / v200),
                         ls=ls, c=kwargs['c'], alpha=alpha)
            ax1.errorbar(x_data, np.log10(np.sqrt(theory_best_z2[:-1, 0]) / v200),
                         ls=ls, c=kwargs['c'], alpha=alpha)
            ax2.errorbar(x_data, np.log10(np.sqrt(theory_best_r2[:-1, 0]) / v200),
                         ls=ls, c=kwargs['c'], alpha=alpha)
            ax3.errorbar(x_data, np.log10(np.sqrt(theory_best_p2[:-1, 0]) / v200),
                         ls=ls, c=kwargs['c'], alpha=alpha)

    x_data = np.log10(bin_centres / r200)
    # plot analytic dm
    ls = '-'
    c = 'k'
    axm1.errorbar(x_data, np.log10(analytic_v_circ / v200), ls=ls, c=c)
    ax0.errorbar(x_data, np.log10(analytic_v_circ / v200), ls=ls, c=c)
    ax1.errorbar(x_data, np.log10(analytic_dispersion / v200), ls=ls, c=c)
    ax2.errorbar(x_data, np.log10(analytic_dispersion / v200), ls=ls, c=c)
    ax3.errorbar(x_data, np.log10(analytic_dispersion / v200), ls=ls, c=c)

    axm1.set_ylabel('$\log( \overline{v_\phi} / V_{200})$')
    ax0.set_ylabel('$\log( (v_c - \overline{v_\phi}) / V_{200})$')
    # ax0.set_ylabel('$\overline{v_\phi} / V_{200}$')
    ax1.set_ylabel('$\log( \sigma_z / V_{200})$')
    ax2.set_ylabel('$\log( \sigma_R / V_{200})$')
    ax3.set_ylabel('$\log( \sigma_\phi / V_{200})$')

    if save_name_append == 'diff':
        ax3.set_xlabel('$\log( r / R_{200})$')
    elif save_name_append == 'cum':
      ax3.set_xlabel('$\log( < r / R_{200})$')

    axm1.legend(loc='lower right', frameon=False, labelspacing=0.1)

    ax1.text(xlim[0] + 0.9 * (xlim[1] - xlim[0]), slim[0] + 0.7 * (slim[1] - slim[0]),
             r'$t =$' + str(t) + 'Gyr', ha='right', va='top')

    ax1.text(xlim[0] + 0.28 * (xlim[1] - xlim[0]), slim[0] + 0.02 * (slim[1] - slim[0]),
             r'$r_{1/2}$', ha='left', va='bottom')

    ax2.text(xlim[0] + 0.9 * (xlim[1] - xlim[0]), slim[0] + 0.9 * (slim[1] - slim[0]),
             r'DM', ha='right', va='top')

    ax2.text(xlim[0] + 0.1 * (xlim[1] - xlim[0]), slim[0] + 0.1 * (slim[1] - slim[0]),
             r'ICs', ha='left', va='bottom')

    plt.show()
    if save:
        plt.savefig(name, bbox_inches="tight")
        # plt.close()

    return

def calc_profile_to_plot(galaxy_name, snap, bin_edges, n_star_min=50, smear_number=0,
                         cylindrical=True, save_name_append='diff'):
    # load a snap
    (file_name, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
     MassStar, PosStars, VelStars, IDStars, PotStars
     ) = load_nbody.load_and_align(galaxy_name, snap='{0:03d}'.format(int(snap)))
    
    R = np.sqrt(PosStars[:, 0]**2 + PosStars[:, 1]**2)
    r = np.linalg.norm(PosStars, axis=1)
    if cylindrical:
        value, _ = np.histogram(R, np.unique(bin_edges))
    else:
        value, _ = np.histogram(r, np.unique(bin_edges))

    if save_name_append == 'diff':
        mask = value  #(value > n_star_min)
    else:
        mask = np.cumsum(value) #(np.cumsum(value) > n_star_min)

    #TODO comment out
    if cylindrical:
        interesting_radii = get_radii_that_are_interesting(R)
    else:
        interesting_radii = get_radii_that_are_interesting(r)
    if save_name_append == 'diff':
        half_mass_edges = get_dex_bins([interesting_radii[1]])
    else:
        half_mass_edges = [0, interesting_radii[1]]
    r_half_current = interesting_radii[1]

    (mean_v_R, mean_v_phi_r_half, sigma_z_r_half, sigma_r_r_half, sigma_p_r_half
     ) = get_dispersion_profiles(PosStars, VelStars, half_mass_edges)

    if cylindrical:
        argr = np.argsort(R)
    else:
        argr = np.argsort(r)

    # fiftyr = r[argr][50]
    # hundredr = r[argr][100]
    bin_centres = 10 ** ((np.log10(np.unique(bin_edges)[:-1]) + np.log10(np.unique(bin_edges)[1:])) / 2)
    # mask = np.logical_and(mask, bin_centres > fiftyr)

    # calculate points to plot
    (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
     ) = get_dispersion_profiles(PosStars, VelStars, bin_edges, cylindrical=cylindrical)

    Sigma = np.zeros(len(np.unique(bin_edges)) - 1)
    for i in range(len(np.unique(bin_edges)) - 1):
        if cylindrical:
            N = np.sum(np.logical_and((R > np.unique(bin_edges)[i]), (R < np.unique(bin_edges)[i + 1])))
        else:
            N = np.sum(np.logical_and((r > np.unique(bin_edges)[i]), (r < np.unique(bin_edges)[i + 1])))
    
        Sigma[i] = N * MassStar / (np.pi * (np.unique(bin_edges)[i]**2 - np.unique(bin_edges)[i + 1]**2))

    for i in range(smear_number):
        # load a snap
        (file_name, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
         MassStar, PosStars, VelStars, IDStars, PotStars
         ) = load_nbody.load_and_align(galaxy_name, snap='{0:03d}'.format(int(snap + i - smear_number // 2)))
        
        if cylindrical:
            R = np.sqrt(PosStars[:, 0]**2 + PosStars[:, 1]**2)
            value, _ = np.histogram(R, bin_edges[1::2])
        else:
            r = np.linalg.norm(PosStars, axis=1)
            value, _ = np.histogram(r, bin_edges[1::2])

        if save_name_append == 'diff':
            mask = np.amin((mask, value)) #np.logical_and(mask, (value > n_star_min))
        else:
            mask = np.amin((mask, np.cumsum(value))) #np.logical_mask(mask, (np.cumsum(value) > n_star_min))

        (v_R, v_phi, s_z, s_R, s_phi
         ) = get_dispersion_profiles(PosStars, VelStars, bin_edges, cylindrical=cylindrical)

        Sig = np.zeros(len(bin_edges) // 2)
        for i in range(len(bin_edges) // 2):
            if cylindrical:
                N = np.sum(np.logical_and((R > bin_edges[2 * i]), (R < bin_edges[2 * i + 1])))
            else:
                N = np.sum(np.logical_and((r > bin_edges[2 * i]), (r < bin_edges[2 * i + 1])))

            Sig[i] = N * MassStar / (np.pi * (bin_edges[2 * i + 1] ** 2 - bin_edges[2 * i] ** 2))

        mean_v_R += v_R
        mean_v_phi += v_phi
        sigma_z += s_z
        sigma_R += s_R
        sigma_phi += s_phi
        
        Sigma += Sig

    mean_v_R /= (smear_number + 1)
    mean_v_phi /= (smear_number + 1)
    sigma_z /= (smear_number + 1)
    sigma_R /= (smear_number + 1)
    sigma_phi /= (smear_number + 1)

    Sigma /= (smear_number + 1)

    # return(r_half_current, sigma_p_r_half, mean_v_phi_r_half, mask, sigma_phi, mean_v_phi)
    return(r_half_current, sigma_p_r_half, mean_v_phi_r_half, sigma_r_r_half, sigma_z_r_half,
           mask, sigma_phi, mean_v_phi, sigma_R, sigma_z, Sigma)
    # return (_, _, _, mask, sigma_phi, mean_v_phi)


def plot_poster_velocity_profiles_after_t(galaxy_name_list, snaps_list, t=5, 
                                          save_name_append='diff', name='', model=True, careful_model=False,
                                          slog_yax=False, show_dms=False, save=True):
    '''
    '''
    fig = plt.figure()
    fig.set_size_inches(7.2, 11.3, forward=True)
    # fig.set_size_inches(7.2, 16, forward=True)

    # fits for comparison
    print('\nbeta=0, gamma=0 (fiducial)')
    (ln_Lambda_ks, alphas, betas, gammas
     ) = find_least_log_squares_best_fit(
        save_name_append, mcmc=False, fixed_b=0, fixed_g=0)

    n_star_min = 20

    ax0 = plt.subplot(2, 1, 1)
    ax1 = plt.subplot(2, 1, 2)

    # ax0 = plt.subplot(3, 1, 1)
    # ax1 = plt.subplot(3, 1, 2)
    # ax2 = plt.subplot(3, 1, 3)

    fig.subplots_adjust(hspace=0.03, wspace=0.03)

    ax0.set_xticklabels([])
    # ax0.set_yticks([-2, -1, 0])
    # ax0.set_yticklabels([0.01, 0.1, 1])
    # ax1.set_xticks([-2, -1, 0])
    # ax1.set_xticklabels([0.01, 0.1, 1])
    # ax1.set_yticks([-2, -1, 0])
    # ax1.set_yticklabels([0.01, 0.1, 1])

    xlim = [-2.8, 0.2]
    # xlim = [-2.0, 0.2]
    # xlim = [0, 0.15]
    ax0.set_xlim(xlim)
    ax1.set_xlim(xlim)

    if save_name_append == 'diff':
        # slim = [0, 240]
        # vlim = [0, 260]
        slim = [0, 1.1]
        vlim = [0, 1.3]
        # vlim = [-1.3, 1.3]
        # slim = [-1.3,0.25]
        # vlim = [-0.7,0.15]
    elif save_name_append == 'cum':
        vlim = [-1.5, 0.1]
        slim = [-1.3, 0.3]

    ax0.set_ylim(slim)
    ax1.set_ylim(vlim)

    v200 = get_v_200()
    r200 = get_r_200()

    for ii, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = get_name_kwargs(galaxy_name)
        snap = int(round(t / T_TOT * snaps))
            
        # bins
        bin_edges = np.logspace(-1, 3, 31)
        # bin_edges = np.linspace(0.1, 100, 31)
        # if show_dms:
        #     dm_bin_edges = np.logspace(-1, 3, 31)
        # bin_edges = np.linspace(0, 30, 31)

        # bins at a better resolution
        if save_name_append == 'diff':
            bin_centres = 10 ** ((np.log10(bin_edges[:-1]) + np.log10(bin_edges[1:])) / 2)
            bin_edges = np.roll(np.repeat(bin_edges, 2), -1)[:-2]

            # if show_dms:
            #     dm_bin_centres = 10 ** ((np.log10(dm_bin_edges[:-1]) + np.log10(dm_bin_edges[1:])) / 2)
            #     dm_bin_edges = np.roll(np.repeat(dm_bin_edges, 2), -1)[:-2]

        if ii == 0:
            x_data = np.log10(bin_centres / r200)
            # if show_dms:
            #     dm_x_data = np.log10(dm_bin_centres / r200)
            # x_data = bin_centres / r200

            (inital_velocity_v, inital_velocity_z, inital_velocity_r, inital_velocity_p
             ) = smooth_zero_snap(0, 0, save_name_append, bin_edges)

            if not show_dms:
                ax0.errorbar(x_data, (inital_velocity_p / v200), c='k', ls='-.')
                ax1.errorbar(x_data, (inital_velocity_v / v200), c='k', ls='-.')

        x_data = np.log10(bin_centres / r200)[:-1]
        # if show_dms:
        #     dm_x_data = np.log10(dm_bin_centres / r200)[:-1]
        # x_data = (bin_centres / r200)[:-1]

        ten = 1
        if 'lgMdm_8p0' in galaxy_name: ten = 10
        elif 'lgMdm_7p5' in galaxy_name and 'smooth' in galaxy_name: ten = 4
        elif 'lgMdm_7p5' in galaxy_name: ten = 5

        if ten != 1:

            Sigma = np.zeros((ten, len(x_data)))
            sigma_z = np.zeros((ten, len(x_data)))
            sigma_R = np.zeros((ten, len(x_data)))
            sigma_phi = np.zeros((ten, len(x_data)))
            mean_v_phi = np.zeros((ten, len(x_data)))
            r_current_half_seeds = np.zeros(ten)
            sigma_phi_half_seeds = np.zeros(ten)
            mean_v_phi_half_seeds = np.zeros(ten)

            if show_dms or careful_model:
                sigma_phi_dm = np.zeros((ten, len(x_data)))
                mean_v_phi_dm = np.zeros((ten, len(x_data)))

            for i in range(ten):
                if i > 1:
                    galaxy_name = galaxy_name[:-6]
                if i != 0:
                    galaxy_name += f'_seed{i}'

                (r_half_current_seed, sigma_p_r_half_seed, mean_v_phi_r_half_seed, _, _,
                 mask, sigma_phi_seed, mean_v_phi_seed, sigma_R_seed, sigma_z_seed, Sigma_seed
                 ) = calc_profile_to_plot(galaxy_name, snap, bin_edges, save_name_append=save_name_append)

                Sigma[i, :] = Sigma_seed
                sigma_z[i, :] = sigma_z_seed
                sigma_R[i, :] = sigma_R_seed
                sigma_phi[i, :] = sigma_phi_seed
                mean_v_phi[i, :] = mean_v_phi_seed

                r_current_half_seeds[i] = r_half_current_seed
                sigma_phi_half_seeds[i] = sigma_p_r_half_seed
                mean_v_phi_half_seeds[i] = mean_v_phi_r_half_seed

                if show_dms or careful_model:
                    (file_name, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
                     MassStar, PosStars, VelStars, IDStars, PotStars
                     ) = load_nbody.load_and_align(galaxy_name,
                                                   snap='{0:03d}'.format(int(snap)))
                    
                    r = np.linalg.norm(PosDMs, axis=1)
                    rs = np.linalg.norm(PosStars, axis=1)
                    (v_R, v_phi, s_z, s_R, s_phi
                     ) = get_dispersion_profiles(PosDMs, VelDMs, bin_edges[:-2], cylindrical=False)

                    bin_cents = 10 ** ((np.log10(bin_edges[:-1:2]) + np.log10(bin_edges[1::2])) / 2)
                    bin_cents = bin_cents[:-1]
                    
                    v_phi = np.sqrt(GRAV_CONST * (MassDM * np.array([np.sum(r < bcs) for bcs in bin_cents]) +
                                                  MassStar * np.array([np.sum(rs < bcs) for bcs in bin_cents])) /
                                    bin_cents)

                    if 'dm_vc' in name:
                        v_phi = np.sqrt(GRAV_CONST * MassDM * np.array([np.sum(r < bcs) for bcs in bin_cents]) / bin_cents)

                    mk = False
                    for jk, m in enumerate(mask):
                        if m:
                            mk = True
                        mask[jk] = mk
                        # mask =

                    # sigma_phi_dm[i, :] = np.sqrt((s_phi**2 + s_R**2 + s_z**2) / 3) #s_phi
                    sigma_phi_dm[i, :] = s_phi
                    mean_v_phi_dm[i, :] = v_phi

            Sigma = np.median(Sigma, axis=0)
            sigma_z = np.median(sigma_z, axis=0)
            sigma_R = np.median(sigma_R, axis=0)
            sigma_phi = np.median(sigma_phi, axis=0)
            mean_v_phi = np.median(mean_v_phi, axis=0)

            r_half_current = np.median(r_current_half_seeds)
            sigma_p_r_half = np.median(sigma_phi_half_seeds)
            mean_v_phi_r_half = np.median(mean_v_phi_half_seeds)

            if show_dms or careful_model:
                sigma_phi_dm = np.median(sigma_phi_dm, axis=0)
                mean_v_phi_dm = np.median(mean_v_phi_dm, axis=0)

        else:
            (r_half_current, sigma_p_r_half, mean_v_phi_r_half, _, _,
             mask, sigma_phi, mean_v_phi, sigma_R, sigma_z, Sigma
             ) = calc_profile_to_plot(galaxy_name, snap, bin_edges, save_name_append=save_name_append)

            if show_dms:
                (file_name, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
                 MassStar, PosStars, VelStars, IDStars, PotStars
                 ) = load_nbody.load_and_align(galaxy_name,
                                               snap='{0:03d}'.format(int(snap)))

                r = np.linalg.norm(PosDMs, axis=1)
                rs = np.linalg.norm(PosStars, axis=1)
                (v_R, v_phi, s_z, s_R, s_phi
                 ) = get_dispersion_profiles(PosDMs, VelDMs, bin_edges[:-2], cylindrical=False)

                bin_cents = 10 ** ((np.log10(bin_edges[:-1:2]) + np.log10(bin_edges[1::2])) / 2)
                bin_cents = bin_cents[:-1]

                v_phi = np.sqrt(GRAV_CONST * (MassDM * np.array([np.sum(r < bcs) for bcs in bin_cents]) +
                                              MassStar * np.array([np.sum(rs < bcs) for bcs in bin_cents])) /
                                bin_cents)

                mk = False
                for jk, m in enumerate(mask):
                    if m:
                        mk = True
                    mask[jk] = mk

                if 'dm_vc' in name:
                    v_phi = np.sqrt(GRAV_CONST * MassDM * np.array([np.sum(r < bcs) for bcs in bin_cents]) / bin_cents)

                # sigma_phi_dm = np.sqrt((s_phi**2 + s_R**2 + s_z**2) / 3) #s_phi
                sigma_phi_dm = s_phi
                mean_v_phi_dm = v_phi

        # plot data
        lw = 3
        if not show_dms:
            ax0.errorbar(x_data[mask], (sigma_phi[mask] / v200),  # / analytic_dispersion[:-1][mask],
                         ls='-', c=kwargs['c'], lw=lw)
            ax1.errorbar(x_data[mask], (mean_v_phi[mask] / v200),  # / analytic_v_circ[:-1][mask],
                         ls='-', c=kwargs['c'], lw=lw)
        
        if show_dms:
            ax0.errorbar(x_data[mask], (sigma_phi_dm[mask] / v200),  # / analytic_dispersion[:-1][mask],
                         ls=(0, (3,1,1,1,1,1)), c=kwargs['c'], lw=lw)
            ax1.errorbar(x_data[mask], (mean_v_phi_dm[mask] / v200),  # / analytic_v_circ[:-1][mask],
                         ls=(0, (3,1,1,1,1,1)), c=kwargs['c'], lw=lw)

        if not show_dms:
            ax0.scatter(np.log10(r_half_current/r200), (sigma_p_r_half / v200),  # / analytic_dispersion[:-1][mask],
                        c=kwargs['c'], s=12**2, ec='k', linewidths=1.6, zorder=10)
            ax1.scatter(np.log10(r_half_current/r200), (mean_v_phi_r_half / v200),  # / analytic_v_circ[:-1][mask],
                        c=kwargs['c'], s=12**2, ec='k', linewidths=1.6, zorder=10)
            # ax0.scatter(r_half_current/r200, (sigma_p_r_half / v200),  # / analytic_dispersion[:-1][mask],
            #             c=kwargs['c'], s=12**2, ec='k', linewidths=1.6, zorder=10)
            # ax1.scatter(r_half_current/r200, (mean_v_phi_r_half / v200),  # / analytic_v_circ[:-1][mask],
            #             c=kwargs['c'], s=12**2, ec='k', linewidths=1.6, zorder=10)

        #theory
        x_data_range = [np.amin(x_data[mask]), np.amax(x_data[mask])]
    
        # can use higher resolution bins
        bin_edges = np.logspace(-1, 3, 101)
    
        # bins at a better resolution
        if save_name_append == 'diff':
            bin_centres = 10 ** ((np.log10(bin_edges[:-1]) + np.log10(bin_edges[1:])) / 2)
            bin_edges = np.roll(np.repeat(bin_edges, 2), -1)[:-2]
    
        # halo properties
        V200 = get_v_200(galaxy_name=galaxy_name)
        r200 = get_r_200(galaxy_name=galaxy_name)
    
        analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                                 save_name_append=save_name_append, galaxy_name=galaxy_name)
    
        analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)

        a, vmax = get_hernquist_params(galaxy_name=galaxy_name)
        m_tot = 4 * a * vmax ** 2 / GRAV_CONST
        rd = 4.15
        fbary = 0.01
        md_tot = m_tot * (1 - fbary)
        ms_tot = m_tot * fbary

        rho = lambda r: get_analytic_hernquist_density(np.array([r]*2), md_tot, a)
        # acc = lambda r: get_analytic_hernquist_potential_derivative(r, md_tot, ad)
        acc = lambda r: (get_analytic_hernquist_potential_derivative(r, md_tot, a) +
                         get_analytic_spherical_exponential_potential_derivative(r, ms_tot, rd))
        integrad = lambda r: rho(r) * acc(r)
        sigma_rd = np.sqrt([quad(integrad, r_, 20 * a)[0] / rho(r_) for r_ in bin_centres])

        x_data = np.log10(bin_centres / r200)
        # x_data = bin_centres / r200
        # plot analytic dm
        if ii == 0:
            if 'dm_vc' in name:
                a_h, vmax = get_hernquist_params(galaxy_name)
                analytic_v_circ = get_analytic_hernquist_circular_velocity(bin_edges, vmax, a_h, save_name_append)
                ax1.errorbar(x_data, (analytic_v_circ / V200), ls='-', c='k')
            else:
                ax1.errorbar(x_data, (analytic_v_circ / V200), ls='-', c='k')

            if not show_dms:
                ax0.errorbar(x_data, (analytic_dispersion / V200), ls='-', c='k')

            if show_dms:
                ax0.errorbar(x_data, (sigma_rd / V200), ls='-', c='k')

            # ax1.axhline(0.9 * np.amax(analytic_v_circ / V200), 0, 1, ls='-', c='k')
            # ax1.axhline(0.95 * np.amax(analytic_v_circ / V200), 0, 1, ls='-', c='k')
            # ax1.axhline(1 * np.amax(analytic_v_circ / V200), 0, 1, ls='-', c='k')

        x_data = np.log10(bin_centres / r200)[:-1]
        # x_data = (bin_centres / r200)[:-1]
    
        #update data range
        mask_theory = np.logical_and(x_data_range[0] < x_data, x_data < x_data_range[1])

        # theory
        # work out constatns
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, kwargs['mdm'], galaxy_name=galaxy_name)

        # burn ins
        (inital_velocity_v, inital_velocity2_z, inital_velocity2_r, inital_velocity2_p
         ) = smooth_zero_snap(kwargs['mdm'], snaps, save_name_append, bin_edges)

        inital_velocity_v = np.amin((inital_velocity_v, analytic_v_circ), axis=0)

        # inital_velocity2_z **=2
        # inital_velocity2_r **=2
        inital_velocity2_p **= 2

        theory_best_v = np.zeros(len(bin_centres))
        theory_best_z2 = np.zeros(len(bin_centres))
        theory_best_r2 = np.zeros(len(bin_centres))
        theory_best_p2 = np.zeros(len(bin_centres))
        
        experiment_v = np.zeros(len(bin_centres))
        experiment_v2 = np.zeros(len(bin_centres))
        experiment_v3 = np.zeros(len(bin_centres))
        experiment_v4 = np.zeros(len(bin_centres))

        #experimenting
        tau_heat_v = np.zeros(len(bin_centres))
        tau_heat_z2 = np.zeros(len(bin_centres))
        tau_heat_r2 = np.zeros(len(bin_centres))
        tau_heat_p2 = np.zeros(len(bin_centres))

        tau_heat_experiment = np.zeros(len(bin_centres))
        tau_heat_experiment2 = np.zeros(len(bin_centres))
        tau_heat_experiment3 = np.zeros(len(bin_centres))
        tau_heat_experiment4 = np.zeros(len(bin_centres))

        for i in range(len(bin_centres)):
            theory_best_v[i] = get_theory_velocity2_array(v200, analytic_v_circ[i] - inital_velocity_v[i],
                                                          np.array([t]), t_c_dm[i], delta_dm[i], np.sqrt(upsilon_circ[i]),
                                                          ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
            theory_best_z2[i] = get_theory_velocity2_array(v200 ** 2, inital_velocity2_z[i],
                                                           np.array([t]), t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
            theory_best_r2[i] = get_theory_velocity2_array(v200 ** 2, inital_velocity2_r[i],
                                                           np.array([t]), t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
            theory_best_p2[i] = get_theory_velocity2_array(v200 ** 2, inital_velocity2_p[i],
                                                           np.array([t]), t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

            # def get_theory_velocity2_array(velocity_scale2_array, inital_velocity2_array,
            #                                time_array, time_scale_array,
            #                                scale_density_ratio_array, scale_velocity_ratio_array,
            #                                ln_Lambda_k_i, alpha_i, beta_i, gamma_i):

            tau_heat_v[i] = get_tau_heat_array(delta_dm[i], np.sqrt(upsilon_circ[i]),
                                             ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])

            tau_heat_z2[i] = get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                             ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
            tau_heat_r2[i] = get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                             ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
            tau_heat_p2[i] = get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                             ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

            # tau_heat_experiment[i] = (t_c_dm[i] * (tau_heat_z2[i] + tau_heat_r2[i] + tau_heat_p2[i]
            #                     ) * analytic_dispersion[i] / analytic_v_circ[i] / 3)
            # tau_heat_experiment[i] = (t_c_dm[i] * (tau_heat_p2[i] * 0.7498))
            tau_heat_experiment[i] = (t_c_dm[i] * (tau_heat_r2[i] * 1.0612))
                                # ) * analytic_dispersion[i] / analytic_v_circ[i])
            tau_heat_experiment2[i] = (t_c_dm[i] * (tau_heat_p2[i] * 0.74987599))
            # tau_heat_experiment2[i] = (t_c_dm[i] * (tau_heat_p2[i]
            #                     ) * analytic_v_circ[i] / analytic_dispersion[i])
                                # ) * analytic_v_circ[i] / analytic_dispersion[i] / 2)

            tau_heat_experiment3[i] = (t_c_dm[i] * (tau_heat_r2[i] * 1.31689006))
            tau_heat_experiment4[i] = (t_c_dm[i] * (tau_heat_z2[i] * 0.6806577))

            tau_0_on_tau_heat = get_tau_zero_array(v200, analytic_v_circ[i] - inital_velocity_v[i],
                                                   np.sqrt(upsilon_circ[i]))
            
            # print(tau_0_on_tau_heat)

            experiment_v[i] = analytic_v_circ[i] * np.exp(-np.array([t]) / tau_heat_experiment[i] )# tau_0_on_tau_heat)
            experiment_v2[i] = analytic_v_circ[i] * np.exp(-np.array([t]) / tau_heat_experiment2[i] )# tau_0_on_tau_heat)

            tau_0_on_tau_heat2 = get_tau_zero_array(v200**2, inital_velocity2_p[i],
                                                    upsilon_dm[i])

            experiment_v3[i] = analytic_dispersion[i]**2 * (1 - np.exp(-np.array([t]) / tau_heat_experiment3[i] - tau_0_on_tau_heat2))
            experiment_v4[i] = analytic_dispersion[i]**2 * (1 - np.exp(-np.array([t]) / tau_heat_experiment4[i] - tau_0_on_tau_heat2))

        theory_best_v = analytic_v_circ - theory_best_v

        alpha = 1
        ls = '--'
        thirty = int(len(bin_centres) -1)
        lw = 3
        
        if model:
            ax0.errorbar(x_data[:thirty], (np.sqrt(theory_best_p2[:-1])[:thirty] / V200),
                         ls=ls, c=kwargs['c'], lw=lw, alpha=alpha)

            if not careful_model:
                ax1.errorbar(x_data[:thirty], (experiment_v2[:-1][:thirty] / V200),  # / analytic_v_circ[:-1][:thirty],
                             ls=ls, c=kwargs['c'], lw=lw, alpha=alpha)

            # basic for paper
            # asymmetri drift
            d_sigma_r2_d_R = np.diff(theory_best_r2) / np.diff(bin_centres)

            R_disk = 4.15
    
            v_phi2_assymetry_on_v_c2sq = analytic_v_circ[:-1] * np.sqrt(
                1 - theory_best_p2[:-1] / (analytic_v_circ[:-1] ** 2) +
                theory_best_r2[:-1] / (analytic_v_circ[:-1] ** 2) * (1 - bin_centres[:-1] / R_disk) +
                bin_centres[:-1] / (analytic_v_circ[:-1] ** 2) * d_sigma_r2_d_R)# -
                #(theory_best_r2[:-1,0] - theory_best_z2[:-1,0]) / analytic_v_circ[:-1]**2)

            if not careful_model:
                ax1.errorbar(x_data[mask_theory], (v_phi2_assymetry_on_v_c2sq / V200)[mask_theory],  # / analytic_v_circ[:-1][:thirty],
                             ls=(1,(0.8,0.8)), c=kwargs['c'], lw=4, alpha=alpha)

            # ax1.errorbar(x_data[mask_theory], (analytic_v_circ[:-1] / V200)[mask_theory],
            #              ls='-', c='C5', lw=1, alpha=alpha)
            # ax1.errorbar(x_data[mask_theory], (np.sqrt(theory_best_p2[:-1]) / V200)[mask_theory],
            #              ls='-', c='C6', lw=1, alpha=alpha)
            # ax1.errorbar(x_data[mask_theory], (np.sqrt(np.abs(theory_best_r2[:-1] * (bin_centres[:-1] / R_disk)))
            #               / V200)[mask_theory],
            #              ls='-', c='C7', lw=1, alpha=alpha)
            # ax1.errorbar(x_data[mask_theory], (np.sqrt(np.abs(bin_centres[:-1] * d_sigma_r2_d_R)) / V200)[mask_theory],
            #              ls='-', c='C8', lw=1, alpha=alpha)
            # ax1.errorbar(x_data[mask_theory], (np.sqrt(theory_best_r2[:-1]) / V200)[mask_theory],
            #              ls='-', c='C9', lw=1, alpha=alpha)

            if careful_model:

                bin_edges = np.logspace(-1, 3, 31)
                # bin_edges = np.linspace(0.1, 100, 31)
                bin_centres = 10 ** ((np.log10(bin_edges[:-1]) + np.log10(bin_edges[1:])) / 2)[:-1]
                bin_edges = np.roll(np.repeat(bin_edges, 2), -1)[:-2]
                x_data = np.log10(bin_centres / r200)[:-1]

                dSigma_dR = np.diff(Sigma) / np.diff(bin_centres)
                dlogSigma_dlogR = np.diff(np.log(Sigma)) / np.diff(np.log(bin_centres))
                dsigma_r2_dR = np.diff(sigma_R**2) / np.diff(bin_centres)
                dlogsigma_r2_dlogR = np.diff(2*np.log(sigma_R)) / np.diff(np.log(bin_centres))
                
                v_phi2_full_particle_estimate = (mean_v_phi_dm[:-1]**2 - sigma_phi[:-1]**2 + sigma_R[:-1]**2 +
                                                 # sigma_R[:-1] ** 2 * bin_centres[:-1] / Sigma[:-1] * dSigma_dR +
                                                 sigma_R[:-1] ** 2 * dlogSigma_dlogR +
                                                 # bin_centres[:-1] * dsigma_r2_dR)
                                                 sigma_R[:-1] ** 2 * dlogsigma_r2_dlogR)
                
                ax1.errorbar(x_data[mask[:-1]], (np.sqrt(v_phi2_full_particle_estimate) / V200)[mask[:-1]],
                             ls=(1, (1.5,0.5,0.5)), c=kwargs['c'], lw=4, alpha=alpha)
                ax1.errorbar(x_data[mask[:-1]], (np.sqrt(-v_phi2_full_particle_estimate) / V200)[mask[:-1]],
                             ls=(1, (1.5,0.5,0.5)), c=kwargs['c'], lw=4, alpha=0.3)

                # ax1.errorbar(x_data[mask[:-1]], (mean_v_phi_dm[:-1] / V200)[mask[:-1]],
                #              ls='-', c='C5', lw=2, alpha=alpha)
                # ax1.errorbar(x_data[mask[:-1]], (sigma_phi[:-1] / V200)[mask[:-1]],
                #              ls='-', c='C6', lw=2, alpha=alpha)
                # ax1.errorbar(x_data[mask[:-1]],
                #              (np.sqrt(np.abs(sigma_R[:-1]**2 * bin_centres[:-1] / Sigma[:-1] * dSigma_dR))
                #               / V200)[mask[:-1]],
                #              ls='-', c='C7', lw=2, alpha=alpha)
                # ax1.errorbar(x_data[mask[:-1]],
                #              (np.sqrt(np.abs(sigma_R[:-1]**2 * dlogSigma_dlogR))
                #               / V200)[mask[:-1]],
                #              ls='-', c='C7', lw=2, alpha=alpha)
                # ax1.errorbar(x_data[mask[:-1]], (np.sqrt(np.abs(bin_centres[:-1] * dsigma_r2_dR)) / V200)[mask[:-1]],
                #              ls='-', c='C8', lw=2, alpha=alpha)
                # ax1.errorbar(x_data[mask[:-1]], (sigma_R[:-1] / V200)[mask[:-1]],
                #              ls='-', c='C9', lw=2, alpha=alpha)

        #lagends
        if kwargs['c'] in ['C0', 'C1', 'C2']:
            dx = 0.03
        else:
            dx = 0.42

        if kwargs['c'] in ['C0', 'C3']:
            dy = 0.96
        elif kwargs['c'] in ['C1', 'C4']:
            dy = 0.88
        else:
            dy = 0.80

        ax0.text(xlim[0] + dx * (xlim[1] - xlim[0]), slim[0] + dy * (slim[1] - slim[0]),
                 r'$m_{\mathrm{DM}} = $' + kwargs['lab'], ha='left', va='top', color=kwargs['c'])

    r_half = get_bin_edges('cum')[3]
    ax0.axvline(np.log10(r_half / r200), 0, 0.78, c='grey', ls=':')
    ax1.axvline(np.log10(r_half / r200), 0, 1, c='grey', ls=':')
    # ax0.axvline(r_half / r200, 0, 0.78, c='grey', ls=':')
    # ax1.axvline(r_half / r200, 0, 1, c='grey', ls=':')

    # ax0.set_ylabel('$\sigma_\phi$ [km/s]')
    # ax1.set_ylabel('$\overline{v_\phi}$ [km/s]')
    # ax0.set_ylabel(r'$\log \, \sigma_\phi / V_{200}$')
    # ax1.set_ylabel(r'$\log \,\overline{v}_\phi / V_{200}$')
    ax0.set_ylabel(r'$\sigma_\phi / V_{200}$')
    ax1.set_ylabel(r'$\overline{v}_\phi / V_{200}$')

    if save_name_append == 'diff':
        ax1.set_xlabel('$\log \, r / r_{200} $')
        # ax1.set_xlabel('$r / r_{200}$')
    elif save_name_append == 'cum':
        ax1.set_xlabel('$\log( < r / r_{200} )$')

    # ax0.legend(loc='lower right', frameon=False, labelspacing=-0.2, bbox_to_anchor=(1.08, -0.15))

    ax0.legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='-', linewidth=3)),
                (lines.Line2D([0, 1], [0, 1], color='grey', ls='--', linewidth=3)),
                (lines.Line2D([0, 1], [0, 1], color='k', ls='-.', linewidth=1)),],
               ['Simulations', 'equation (6)', 'ICs'],
               handler_map={tuple: HandlerTupleVertical(ndivide=1)},
               loc='center right', handletextpad=0.2, frameon=True, framealpha=0.95,
               labelspacing=0.4, borderpad=0.2 , bbox_to_anchor=(1, 0.65))

    ax1.legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='-', linewidth=3)),
                (lines.Line2D([0, 1], [0, 1], color='grey', ls='--', linewidth=3)),
                (lines.Line2D([0, 1], [0, 1], color='k', ls='-.', linewidth=1)),
                (lines.Line2D([0, 1], [0, 1], color='grey', ls=(1,(0.8,0.8)), linewidth=4))],
               ['Simulations', 'equation (11)', 'ICs', "Asymmetric\ndrift"],
               handler_map={tuple: HandlerTupleVertical(ndivide=1)},
               loc='lower right', handletextpad=0.2, frameon=False,
               labelspacing=0.4, borderpad=0.2)  # , bbox_to_anchor=(1, 0))
    
    if show_dms:
        ax0.legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls=(0, (3,1,1,1,1,1)), linewidth=3)),
                    (lines.Line2D([0, 1], [0, 1], color='k', ls='-', linewidth=1)),],
                   ['Simulations', "Analytic profile"],
                   handler_map={tuple: HandlerTupleVertical(ndivide=1)},
                   loc='lower right', handletextpad=0.2, frameon=True, framealpha=0.95,
                   labelspacing=0.4, borderpad=0.2) #, bbox_to_anchor=(1, 0.65))
        
        ax1.legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls=(0, (3,1,1,1,1,1)), linewidth=3)),
                    (lines.Line2D([0, 1], [0, 1], color='k', ls='-', linewidth=1)),],
                   ['Simulations', "Analytic profile"],
                   handler_map={tuple: HandlerTupleVertical(ndivide=1)},
                   loc='lower right', handletextpad=0.2, frameon=True, framealpha=0.95,
                   labelspacing=0.4, borderpad=0.2) #, bbox_to_anchor=(1, 0.65))

    # ax0.legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='--', linewidth=3)),
    #             (lines.Line2D([0, 1], [0, 1], color='C6', ls='-.', linewidth=3)),],
    #            ['Best fit', r'$t_{\sigma \phi} = 1.32 t_{\sigma R}$',])
    # 
    # ax1.legend([(lines.Line2D([0, 1], [0, 1], color='C8', ls='--', linewidth=3)),
    #             (lines.Line2D([0, 1], [0, 1], color='C9', ls='-.', linewidth=3)),],
    #            [r'$t_{v \phi} = 1.06 t_{\sigma R}$', r'$t_{v \phi} = 0.75 t_{\sigma \phi}$',])

    ax1.text(xlim[0] + 0.03 * (xlim[1] - xlim[0]), vlim[0] + 0.95 * (vlim[1] - vlim[0]),
             r'$t =$' + str(t) + r' Gyr',  # , $V_{200} = 200$ km/s',
             ha='left', va='top')

    # ax0.text(xlim[0]+0.03*(xlim[1]-xlim[0]), slim[0]+0.02*(slim[1]-slim[0]),
    #          r'ICs', ha='left', va='bottom')
    ax1.text(xlim[0] + 0.445 * (xlim[1] - xlim[0]), vlim[0] + 0.90 * (vlim[1] - vlim[0]),
             r'$R_{1/2}$', ha='right', va='top')

    ax1.text(xlim[0] + 0.95 * (xlim[1] - xlim[0]), vlim[0] + 0.79 * (vlim[1] - vlim[0]),
             r'$V_c$', ha='right', va='top')
    ax0.text(xlim[0] + 0.95 * (xlim[1] - xlim[0]), slim[0] + 0.36 * (slim[1] - slim[0]),
             r'$\sigma_{\mathrm{DM}}$', ha='right', va='top')

    if slog_yax:
        ax0.semilogy()
        ax1.semilogy()
        ax0.set_ylim([0.008, slim[1]])
        ax1.set_ylim([0.08, vlim[1]])
        
    if show_dms:
        ax1.set_ylabel(r'$V_c / V_{200}$')

    # if show_dms:
    #     ax1.set_visible(False)
    # 
    #     ax0.set_xlabel('$\log \, r / r_{200} $')
    #     xticks = [-2.5, -2, -1.5, -1, -0.5, 0]
    #     ax0.set_xticks(xticks)
    #     ax0.set_xticklabels([str(x) for x in xticks])
    #     ax0.text(xlim[0] + 0.03 * (xlim[1] - xlim[0]), slim[0] + 0.05 * (slim[1] - slim[0]),
    #          r'$t =$' + str(t) + r' Gyr',  # , $V_{200} = 200$ km/s',
    #          ha='left', va='bottom')

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return


def plot_poster_velocity_profiles_after_t_asymm_exp(galaxy_name_list, snaps_list, t=5,
                                          save_name_append='diff', name='', model=True, careful_model=False,
                                          slog_yax=False, show_dms=False, save=True):
    '''
    '''
    fig = plt.figure()
    fig.set_size_inches(7.2, 11.3, forward=True)
    # fig.set_size_inches(7.2, 16, forward=True)

    # fits for comparison
    print('\nbeta=0, gamma=0 (fiducial)')
    (ln_Lambda_ks, alphas, betas, gammas
     ) = find_least_log_squares_best_fit(
        save_name_append, mcmc=False, fixed_b=0, fixed_g=0)

    n_star_min = 20

    ax0 = plt.subplot(2, 1, 1)
    ax1 = plt.subplot(2, 1, 2)

    # ax0 = plt.subplot(3, 1, 1)
    # ax1 = plt.subplot(3, 1, 2)
    # ax2 = plt.subplot(3, 1, 3)

    fig.subplots_adjust(hspace=0.03, wspace=0.03)

    ax0.set_xticklabels([])
    # ax0.set_yticks([-2, -1, 0])
    # ax0.set_yticklabels([0.01, 0.1, 1])
    # ax1.set_xticks([-2, -1, 0])
    # ax1.set_xticklabels([0.01, 0.1, 1])
    # ax1.set_yticks([-2, -1, 0])
    # ax1.set_yticklabels([0.01, 0.1, 1])

    xlim = [-2.8, 0.2]
    # xlim = [0, 0.15]
    ax0.set_xlim(xlim)
    ax1.set_xlim(xlim)

    if save_name_append == 'diff':
        # slim = [0, 240]
        # vlim = [0, 260]
        slim = [0, 1.1]
        vlim = [0, 1.3]
        # vlim = [-1.3, 1.3]
        # slim = [-1.3,0.25]
        # vlim = [-0.7,0.15]
    elif save_name_append == 'cum':
        vlim = [-1.5, 0.1]
        slim = [-1.3, 0.3]

    # ax0.set_ylim(slim)
    ax0.set_ylim(vlim)
    ax1.set_ylim(vlim)

    v200 = get_v_200()
    r200 = get_r_200()

    for ii, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = get_name_kwargs(galaxy_name)
        snap = int(round(t / T_TOT * snaps))

        # bins
        bin_edges = np.logspace(-1, 3, 31)
        # bin_edges = np.linspace(0.1, 100, 31)
        # if show_dms:
        #     dm_bin_edges = np.logspace(-1, 3, 31)
        # bin_edges = np.linspace(0, 30, 31)

        # bins at a better resolution
        if save_name_append == 'diff':
            bin_centres = 10 ** ((np.log10(bin_edges[:-1]) + np.log10(bin_edges[1:])) / 2)
            bin_edges = np.roll(np.repeat(bin_edges, 2), -1)[:-2]

            # if show_dms:
            #     dm_bin_centres = 10 ** ((np.log10(dm_bin_edges[:-1]) + np.log10(dm_bin_edges[1:])) / 2)
            #     dm_bin_edges = np.roll(np.repeat(dm_bin_edges, 2), -1)[:-2]

        if ii == 0:
            x_data = np.log10(bin_centres / r200)
            # if show_dms:
            #     dm_x_data = np.log10(dm_bin_centres / r200)
            # x_data = bin_centres / r200

            (inital_velocity_v, inital_velocity_z, inital_velocity_r, inital_velocity_p
             ) = smooth_zero_snap(0, 0, save_name_append, bin_edges)

            # ax0.errorbar(x_data, (inital_velocity_p / v200), c='k', ls='-.')
            ax1.errorbar(x_data, (inital_velocity_v / v200), c='k', ls='-.')

        x_data = np.log10(bin_centres / r200)[:-1]
        # if show_dms:
        #     dm_x_data = np.log10(dm_bin_centres / r200)[:-1]
        # x_data = (bin_centres / r200)[:-1]

        ten = 1
        if 'lgMdm_8p0' in galaxy_name: ten = 10
        if 'lgMdm_7p5' in galaxy_name: ten = 5
        if ten != 1:

            Sigma = np.zeros((ten, len(x_data)))
            sigma_z = np.zeros((ten, len(x_data)))
            sigma_R = np.zeros((ten, len(x_data)))
            sigma_phi = np.zeros((ten, len(x_data)))
            mean_v_phi = np.zeros((ten, len(x_data)))
            r_current_half_seeds = np.zeros(ten)
            sigma_phi_half_seeds = np.zeros(ten)
            mean_v_phi_half_seeds = np.zeros(ten)

            if show_dms or careful_model:
                sigma_phi_dm = np.zeros((ten, len(x_data)))
                mean_v_phi_dm = np.zeros((ten, len(x_data)))

            for i in range(ten):
                if i > 1:
                    galaxy_name = galaxy_name[:-6]
                if i != 0:
                    galaxy_name += f'_seed{i}'

                (r_half_current_seed, sigma_p_r_half_seed, mean_v_phi_r_half_seed, _, _,
                 mask, sigma_phi_seed, mean_v_phi_seed, sigma_R_seed, sigma_z_seed, Sigma_seed
                 ) = calc_profile_to_plot(galaxy_name, snap, bin_edges, save_name_append=save_name_append)

                Sigma[i, :] = Sigma_seed
                sigma_z[i, :] = sigma_z_seed
                sigma_R[i, :] = sigma_R_seed
                sigma_phi[i, :] = sigma_phi_seed
                mean_v_phi[i, :] = mean_v_phi_seed

                r_current_half_seeds[i] = r_half_current_seed
                sigma_phi_half_seeds[i] = sigma_p_r_half_seed
                mean_v_phi_half_seeds[i] = mean_v_phi_r_half_seed

                if show_dms or careful_model:
                    (file_name, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
                     MassStar, PosStars, VelStars, IDStars, PotStars
                     ) = load_nbody.load_and_align(galaxy_name,
                                                   snap='{0:03d}'.format(int(snap)))

                    r = np.linalg.norm(PosDMs, axis=1)
                    rs = np.linalg.norm(PosStars, axis=1)
                    (v_R, v_phi, s_z, s_R, s_phi
                     ) = get_dispersion_profiles(PosDMs, VelDMs, bin_edges[:-2], cylindrical=False)

                    bin_cents = 10 ** ((np.log10(bin_edges[:-1:2]) + np.log10(bin_edges[1::2])) / 2)
                    bin_cents = bin_cents[:-1]

                    v_phi = np.sqrt(GRAV_CONST * (MassDM * np.array([np.sum(r < bcs) for bcs in bin_cents]) +
                                                  MassStar * np.array([np.sum(rs < bcs) for bcs in bin_cents])) /
                                    bin_cents)

                    # sigma_phi_dm[i, :] = np.sqrt((s_phi**2 + s_R**2 + s_z**2) / 3) #s_phi
                    sigma_phi_dm[i, :] = s_phi
                    mean_v_phi_dm[i, :] = v_phi

            Sigma = np.median(Sigma, axis=0)
            sigma_z = np.median(sigma_z, axis=0)
            sigma_R = np.median(sigma_R, axis=0)
            sigma_phi = np.median(sigma_phi, axis=0)
            mean_v_phi = np.median(mean_v_phi, axis=0)

            r_half_current = np.median(r_current_half_seeds)
            sigma_p_r_half = np.median(sigma_phi_half_seeds)
            mean_v_phi_r_half = np.median(mean_v_phi_half_seeds)

            if show_dms or careful_model:
                sigma_phi_dm = np.median(sigma_phi_dm, axis=0)
                mean_v_phi_dm = np.median(mean_v_phi_dm, axis=0)

        else:
            (r_half_current, sigma_p_r_half, mean_v_phi_r_half, _, _,
             mask, sigma_phi, mean_v_phi, sigma_R, sigma_z, Sigma
             ) = calc_profile_to_plot(galaxy_name, snap, bin_edges, save_name_append=save_name_append)

            if show_dms:
                (file_name, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
                 MassStar, PosStars, VelStars, IDStars, PotStars
                 ) = load_nbody.load_and_align(galaxy_name,
                                               snap='{0:03d}'.format(int(snap)))

                r = np.linalg.norm(PosDMs, axis=1)
                rs = np.linalg.norm(PosStars, axis=1)
                (v_R, v_phi, s_z, s_R, s_phi
                 ) = get_dispersion_profiles(PosDMs, VelDMs, bin_edges[:-2], cylindrical=False)

                bin_cents = 10 ** ((np.log10(bin_edges[:-1:2]) + np.log10(bin_edges[1::2])) / 2)
                bin_cents = bin_cents[:-1]

                v_phi = np.sqrt(GRAV_CONST * (MassDM * np.array([np.sum(r < bcs) for bcs in bin_cents]) +
                                              MassStar * np.array([np.sum(rs < bcs) for bcs in bin_cents])) /
                                bin_cents)

                # sigma_phi_dm = np.sqrt((s_phi**2 + s_R**2 + s_z**2) / 3) #s_phi
                sigma_phi_dm = s_phi
                mean_v_phi_dm = v_phi

        # plot data
        lw = 3
        # ax0.errorbar(x_data[mask], (sigma_phi[mask] / v200),  # / analytic_dispersion[:-1][mask],
        #              ls='-', c=kwargs['c'], lw=lw)
        ax1.errorbar(x_data[mask], (mean_v_phi[mask] / v200),  # / analytic_v_circ[:-1][mask],
                     ls='-', c=kwargs['c'], lw=lw)

        if show_dms:
            # ax0.errorbar(x_data[mask], (sigma_phi_dm[mask] / v200),  # / analytic_dispersion[:-1][mask],
            #              ls=(0, (3, 1, 1, 1, 1, 1)), c=kwargs['c'], lw=lw)
            ax1.errorbar(x_data[mask], (mean_v_phi_dm[mask] / v200),  # / analytic_v_circ[:-1][mask],
                         ls=(0, (3, 1, 1, 1, 1, 1)), c=kwargs['c'], lw=lw)

        # ax0.scatter(np.log10(r_half_current / r200), (sigma_p_r_half / v200),  # / analytic_dispersion[:-1][mask],
        #             c=kwargs['c'], s=12 ** 2, ec='k', linewidths=1.6, zorder=10)
        ax1.scatter(np.log10(r_half_current / r200), (mean_v_phi_r_half / v200),  # / analytic_v_circ[:-1][mask],
                    c=kwargs['c'], s=12 ** 2, ec='k', linewidths=1.6, zorder=10)
        # ax0.scatter(r_half_current/r200, (sigma_p_r_half / v200),  # / analytic_dispersion[:-1][mask],
        #             c=kwargs['c'], s=12**2, ec='k', linewidths=1.6, zorder=10)
        # ax1.scatter(r_half_current/r200, (mean_v_phi_r_half / v200),  # / analytic_v_circ[:-1][mask],
        #             c=kwargs['c'], s=12**2, ec='k', linewidths=1.6, zorder=10)

        # theory
        x_data_range = [np.amin(x_data[mask]), np.amax(x_data[mask])]

        # can use higher resolution bins
        bin_edges = np.logspace(-1, 3, 101)

        # bins at a better resolution
        if save_name_append == 'diff':
            bin_centres = 10 ** ((np.log10(bin_edges[:-1]) + np.log10(bin_edges[1:])) / 2)
            bin_edges = np.roll(np.repeat(bin_edges, 2), -1)[:-2]

        # halo properties
        V200 = get_v_200(galaxy_name=galaxy_name)
        r200 = get_r_200(galaxy_name=galaxy_name)

        analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                                 save_name_append=save_name_append, galaxy_name=galaxy_name)

        analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)

        a, vmax = get_hernquist_params(galaxy_name=galaxy_name)
        m_tot = 4 * a * vmax ** 2 / GRAV_CONST
        rd = 4.15
        fbary = 0.01
        md_tot = m_tot * (1 - fbary)
        ms_tot = m_tot * fbary

        rho = lambda r: get_analytic_hernquist_density(np.array([r] * 2), md_tot, a)
        # acc = lambda r: get_analytic_hernquist_potential_derivative(r, md_tot, ad)
        acc = lambda r: (get_analytic_hernquist_potential_derivative(r, md_tot, a) +
                         get_analytic_spherical_exponential_potential_derivative(r, ms_tot, rd))
        integrad = lambda r: rho(r) * acc(r)
        sigma_rd = np.sqrt([quad(integrad, r_, 20 * a)[0] / rho(r_) for r_ in bin_centres])

        x_data = np.log10(bin_centres / r200)
        # x_data = bin_centres / r200
        # plot analytic dm
        if ii == 0:
            # ax0.errorbar(x_data, (analytic_dispersion / V200), ls='-', c='k')
            ax1.errorbar(x_data, (analytic_v_circ / V200), ls='-', c='k')

            # ax0.errorbar(x_data, (sigma_rd / V200), ls='-', c='magenta')

            # ax1.axhline(0.9 * np.amax(analytic_v_circ / V200), 0, 1, ls='-', c='k')
            # ax1.axhline(0.95 * np.amax(analytic_v_circ / V200), 0, 1, ls='-', c='k')
            # ax1.axhline(1 * np.amax(analytic_v_circ / V200), 0, 1, ls='-', c='k')

        x_data = np.log10(bin_centres / r200)[:-1]
        # x_data = (bin_centres / r200)[:-1]

        # update data range
        mask_theory = np.logical_and(x_data_range[0] < x_data, x_data < x_data_range[1])

        # theory
        # work out constatns
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, kwargs['mdm'], galaxy_name=galaxy_name)

        # burn ins
        (inital_velocity_v, inital_velocity2_z, inital_velocity2_r, inital_velocity2_p
         ) = smooth_zero_snap(kwargs['mdm'], snaps, save_name_append, bin_edges)

        inital_velocity_v = np.amin((inital_velocity_v, analytic_v_circ), axis=0)

        # inital_velocity2_z **=2
        # inital_velocity2_r **=2
        inital_velocity2_p **= 2

        theory_best_v = np.zeros(len(bin_centres))
        theory_best_z2 = np.zeros(len(bin_centres))
        theory_best_r2 = np.zeros(len(bin_centres))
        theory_best_p2 = np.zeros(len(bin_centres))

        experiment_v = np.zeros(len(bin_centres))
        experiment_v2 = np.zeros(len(bin_centres))
        experiment_v3 = np.zeros(len(bin_centres))
        experiment_v4 = np.zeros(len(bin_centres))

        # experimenting
        tau_heat_v = np.zeros(len(bin_centres))
        tau_heat_z2 = np.zeros(len(bin_centres))
        tau_heat_r2 = np.zeros(len(bin_centres))
        tau_heat_p2 = np.zeros(len(bin_centres))

        tau_heat_experiment = np.zeros(len(bin_centres))
        tau_heat_experiment2 = np.zeros(len(bin_centres))
        tau_heat_experiment3 = np.zeros(len(bin_centres))
        tau_heat_experiment4 = np.zeros(len(bin_centres))

        for i in range(len(bin_centres)):
            theory_best_v[i] = get_theory_velocity2_array(v200, analytic_v_circ[i] - inital_velocity_v[i],
                                                          np.array([t]), t_c_dm[i], delta_dm[i],
                                                          np.sqrt(upsilon_circ[i]),
                                                          ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
            theory_best_z2[i] = get_theory_velocity2_array(v200 ** 2, inital_velocity2_z[i],
                                                           np.array([t]), t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
            theory_best_r2[i] = get_theory_velocity2_array(v200 ** 2, inital_velocity2_r[i],
                                                           np.array([t]), t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
            theory_best_p2[i] = get_theory_velocity2_array(v200 ** 2, inital_velocity2_p[i],
                                                           np.array([t]), t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

            # def get_theory_velocity2_array(velocity_scale2_array, inital_velocity2_array,
            #                                time_array, time_scale_array,
            #                                scale_density_ratio_array, scale_velocity_ratio_array,
            #                                ln_Lambda_k_i, alpha_i, beta_i, gamma_i):

            tau_heat_v[i] = get_tau_heat_array(delta_dm[i], np.sqrt(upsilon_circ[i]),
                                               ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])

            tau_heat_z2[i] = get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                                ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
            tau_heat_r2[i] = get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                                ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
            tau_heat_p2[i] = get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                                ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

            # tau_heat_experiment[i] = (t_c_dm[i] * (tau_heat_z2[i] + tau_heat_r2[i] + tau_heat_p2[i]
            #                     ) * analytic_dispersion[i] / analytic_v_circ[i] / 3)
            # tau_heat_experiment[i] = (t_c_dm[i] * (tau_heat_p2[i] * 0.7498))
            tau_heat_experiment[i] = (t_c_dm[i] * (tau_heat_r2[i] * 1.0612))
            # ) * analytic_dispersion[i] / analytic_v_circ[i])
            tau_heat_experiment2[i] = (t_c_dm[i] * (tau_heat_p2[i] * 0.74987599))
            # tau_heat_experiment2[i] = (t_c_dm[i] * (tau_heat_p2[i]
            #                     ) * analytic_v_circ[i] / analytic_dispersion[i])
            # ) * analytic_v_circ[i] / analytic_dispersion[i] / 2)

            tau_heat_experiment3[i] = (t_c_dm[i] * (tau_heat_r2[i] * 1.31689006))
            tau_heat_experiment4[i] = (t_c_dm[i] * (tau_heat_z2[i] * 0.6806577))

            tau_0_on_tau_heat = get_tau_zero_array(v200, analytic_v_circ[i] - inital_velocity_v[i],
                                                   np.sqrt(upsilon_circ[i]))

            # print(tau_0_on_tau_heat)

            experiment_v[i] = analytic_v_circ[i] * np.exp(-np.array([t]) / tau_heat_experiment[i])  # tau_0_on_tau_heat)
            experiment_v2[i] = analytic_v_circ[i] * np.exp(
                -np.array([t]) / tau_heat_experiment2[i])  # tau_0_on_tau_heat)

            tau_0_on_tau_heat2 = get_tau_zero_array(v200 ** 2, inital_velocity2_p[i],
                                                    upsilon_dm[i])

            experiment_v3[i] = analytic_dispersion[i] ** 2 * (
                        1 - np.exp(-np.array([t]) / tau_heat_experiment3[i] - tau_0_on_tau_heat2))
            experiment_v4[i] = analytic_dispersion[i] ** 2 * (
                        1 - np.exp(-np.array([t]) / tau_heat_experiment4[i] - tau_0_on_tau_heat2))

        theory_best_v = analytic_v_circ - theory_best_v

        alpha = 1
        ls = '--'
        thirty = int(len(bin_centres) - 1)
        lw = 3

        if model:
            # ax0.errorbar(x_data[:thirty], (np.sqrt(theory_best_p2[:-1])[:thirty] / V200),
            #              ls=ls, c=kwargs['c'], lw=lw, alpha=alpha)

            if not careful_model:
                ax1.errorbar(x_data[:thirty], (experiment_v2[:-1][:thirty] / V200),  # / analytic_v_circ[:-1][:thirty],
                             ls=ls, c=kwargs['c'], lw=lw, alpha=alpha)

            # basic for paper
            # asymmetri drift
            d_sigma_r2_d_R = np.diff(theory_best_r2) / np.diff(bin_centres)

            R_disk = 4.15

            v_phi2_assymetry_on_v_c2sq = analytic_v_circ[:-1] * np.sqrt(
                1 - theory_best_p2[:-1] / (analytic_v_circ[:-1] ** 2) +
                theory_best_r2[:-1] / (analytic_v_circ[:-1] ** 2) * (1 - bin_centres[:-1] / R_disk) +
                bin_centres[:-1] / (analytic_v_circ[:-1] ** 2) * d_sigma_r2_d_R)  # -
            # (theory_best_r2[:-1,0] - theory_best_z2[:-1,0]) / analytic_v_circ[:-1]**2)

            # if not careful_model:
            ax1.errorbar(x_data[mask_theory], (v_phi2_assymetry_on_v_c2sq / V200)[mask_theory],
                         # / analytic_v_circ[:-1][:thirty],
                         ls=(1, (0.8, 0.8)), c=kwargs['c'], lw=4)

            if kwargs['c'] == 'C1' or kwargs['c'] == 'C4':

                lw = 2 + (4-int(kwargs['c'][1]))
                alpha = 1 - int(kwargs['c'][1]) / 10

                ax0.errorbar(x_data[mask_theory], (analytic_v_circ[:-1] / V200)[mask_theory],
                             ls=':', c='C5', lw=lw, alpha=alpha, marker='s')
                ax0.errorbar(x_data[mask_theory], (np.sqrt(theory_best_p2[:-1]) / V200)[mask_theory],
                             ls=':', c='C6', lw=lw, alpha=alpha, marker='s')
                ax0.errorbar(x_data[mask_theory], (np.sqrt(np.abs(theory_best_r2[:-1] * (bin_centres[:-1] / R_disk)))
                              / V200)[mask_theory],
                             ls=':', c='C7', lw=lw, alpha=alpha, marker='s')
                ax0.errorbar(x_data[mask_theory], (np.sqrt(np.abs(bin_centres[:-1] * d_sigma_r2_d_R)) / V200)[mask_theory],
                             ls=':', c='C8', lw=lw, alpha=alpha, marker='s')
                ax0.errorbar(x_data[mask_theory], (np.sqrt(theory_best_r2[:-1]) / V200)[mask_theory],
                             ls=':', c='C9', lw=lw, alpha=alpha, marker='s')

            if careful_model:
                bin_edges = np.logspace(-1, 3, 31)
                # bin_edges = np.linspace(0.1, 100, 31)
                bin_centres = 10 ** ((np.log10(bin_edges[:-1]) + np.log10(bin_edges[1:])) / 2)[:-1]
                bin_edges = np.roll(np.repeat(bin_edges, 2), -1)[:-2]
                x_data = np.log10(bin_centres / r200)[:-1]

                dSigma_dR = np.diff(Sigma) / np.diff(bin_centres)
                dlogSigma_dlogR = np.diff(np.log(Sigma)) / np.diff(np.log(bin_centres))
                dsigma_r2_dR = np.diff(sigma_R ** 2) / np.diff(bin_centres)
                dlogsigma_r2_dlogR = np.diff(2 * np.log(sigma_R)) / np.diff(np.log(bin_centres))

                v_phi2_full_particle_estimate = (mean_v_phi_dm[:-1] ** 2 - sigma_phi[:-1] ** 2 + sigma_R[:-1] ** 2 +
                                                 # sigma_R[:-1] ** 2 * bin_centres[:-1] / Sigma[:-1] * dSigma_dR +
                                                 sigma_R[:-1] ** 2 * dlogSigma_dlogR +
                                                 # bin_centres[:-1] * dsigma_r2_dR)
                                                 sigma_R[:-1] ** 2 * dlogsigma_r2_dlogR)

                ax1.errorbar(x_data[mask[:-1]], (np.sqrt(v_phi2_full_particle_estimate) / V200)[mask[:-1]],
                             ls='--', # ls=(1, (1.5, 0.5, 0.5)),
                             c=kwargs['c'], lw=4, alpha=1, marker='D')
                ax1.errorbar(x_data[mask[:-1]], (np.sqrt(-v_phi2_full_particle_estimate) / V200)[mask[:-1]],
                             ls='--', #ls=(1, (1.5, 0.5, 0.5)),
                             c=kwargs['c'], lw=4, alpha=0.3, marker='D')

                if kwargs['c'] == 'C1' or kwargs['c'] == 'C4':

                    ax0.errorbar(x_data[mask[:-1]], (mean_v_phi_dm[:-1] / V200)[mask[:-1]],
                                 ls='-', c='C5', lw=lw, alpha=alpha, marker='o')
                    ax0.errorbar(x_data[mask[:-1]], (sigma_phi[:-1] / V200)[mask[:-1]],
                                 ls='-', c='C6', lw=lw, alpha=alpha, marker='o')
                    # ax0.errorbar(x_data[mask[:-1]],
                    #              (np.sqrt(np.abs(sigma_R[:-1]**2 * bin_centres[:-1] / Sigma[:-1] * dSigma_dR))
                    #               / V200)[mask[:-1]],
                    #              ls='-', c='C7', lw=lw, alpha=alpha, marker='o')
                    ax0.errorbar(x_data[mask[:-1]],
                                 (np.sqrt(np.abs(sigma_R[:-1]**2 * dlogSigma_dlogR))
                                  / V200)[mask[:-1]],
                                 ls='-', c='C7', lw=lw, alpha=alpha, marker='o')
                    ax0.errorbar(x_data[mask[:-1]], (np.sqrt(np.abs(bin_centres[:-1] * dsigma_r2_dR)) / V200)[mask[:-1]],
                                 ls='-', c='C8', lw=lw, alpha=alpha, marker='o')
                    # ax0.errorbar(x_data[mask[:-1]], (np.sqrt(np.abs(sigma_R[:-1] ** 2 * dlogsigma_r2_dlogR)) / V200)[mask[:-1]],
                    #              ls='-', c='C8', lw=lw, alpha=alpha, marker='o')
                    ax0.errorbar(x_data[mask[:-1]], (sigma_R[:-1] / V200)[mask[:-1]],
                                 ls='-', c='C9', lw=lw, alpha=alpha, marker='o')

    ax0.legend([(lines.Line2D([0, 1], [0, 1], color='C5', ls='-', linewidth=4)),
                (lines.Line2D([0, 1], [0, 1], color='C6', ls='-', linewidth=4)),
                (lines.Line2D([0, 1], [0, 1], color='C9', ls='-', linewidth=4)),
                (lines.Line2D([0, 1], [0, 1], color='C7', ls='-', linewidth=4)),
                (lines.Line2D([0, 1], [0, 1], color='C8', ls='-', linewidth=4)),
                (lines.Line2D([0, 1], [0, 1], color='k', ls='-', marker='o', linewidth=4)),
                (lines.Line2D([0, 1], [0, 1], color='k', ls=':', marker='s', linewidth=4)),],
               [r'$V_c$', r'$\sigma_\phi$', r'$\sigma_R$', r'$\sigma_R^2 \frac{\partial \log \Sigma}{\partial \log R}$',
                r'$R \frac{\partial \sigma_R^2}{\partial R}$', 'Model', 'Data'])

    # ax1.legend(,
    #            handler_map={tuple: HandlerTupleVertical(ndivide=1)},
    #            loc='lower right', handletextpad=0.2, frameon=False,
    #            labelspacing=0.4, borderpad=0.2)  # , bbox_to_anchor=(1, 0))


    r_half = get_bin_edges('cum')[3]
    ax0.axvline(np.log10(r_half / r200), 0, 0.78, c='grey', ls=':')
    ax1.axvline(np.log10(r_half / r200), 0, 1, c='grey', ls=':')
    # ax0.axvline(r_half / r200, 0, 0.78, c='grey', ls=':')
    # ax1.axvline(r_half / r200, 0, 1, c='grey', ls=':')

    # ax0.set_ylabel(r'$\sigma_\phi / V_{200}$')
    ax0.set_ylabel(r'Asym. term $v / V_{200}$')
    ax1.set_ylabel(r'$\overline{v}_\phi / V_{200}$')

    ax1.set_xlabel('$\log \, r / r_{200} $')

    if slog_yax:
        ax0.semilogy()
        ax1.semilogy()
        ax0.set_ylim([0.008, slim[1]])
        ax1.set_ylim([0.08, vlim[1]])

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return


def plot_explicit_asymm_rad(galaxy_name_list, snaps_list, t=5,
                            save_name_append='diff', name='',
                            slog_yax=False, save=True):
    # RHS
    # mean v phi
    # Vc
    # sigma phi
    # sigma R
    # sigma R d log Sigma / d log R
    # R d sigma R / d R
    # R d mean v R v z / d z
    # mean v R v z d log nu / d z

    fig = plt.figure()
    fig.set_size_inches(7, 4.5 * 9, forward=True)

    print('\nbeta=0, gamma=0 (fiducial)')
    (ln_Lambda_ks, alphas, betas, gammas
     ) = find_least_log_squares_best_fit(
        save_name_append, mcmc=False, fixed_b=0, fixed_g=0)

    axs = [plt.subplot(9, 1, i + 1) for i in range(9)]

    fig.subplots_adjust(hspace=0.03, wspace=0.03)

    xlim = [-2.8, 0.2]
    vlim = [0, 1.4]
    vlim2 = [-0.7, 0.7]
    vlim3 = [-1.4, 0]

    for i, ax in enumerate(axs):

        if i != 8:
            ax.set_xticklabels([])

        ax.set_xlim(xlim)
        if i < 6:
            ax.set_ylim(vlim)
        else:
            ax.set_ylim(vlim2)

    axs[3].set_ylim(vlim3)
    axs[5].set_ylim(vlim3)

    axs[0].set_ylabel(r'Asymmetric Drift $ / V_{200}$')
    axs[1].set_ylabel(r'$\overline{v}_\phi / V_{200}$')
    axs[2].set_ylabel(r'$V_c / V_{200}$')
    axs[3].set_ylabel(r'$- \sigma_\phi / V_{200}$')
    axs[4].set_ylabel(r'$\sigma_R / V_{200}$')
    axs[5].set_ylabel(r'$\sqrt{ \sigma_R^2 \frac{\partial \ln \nu}{\partial \ln R}} / V_{200}$')
    axs[6].set_ylabel(r'$\sqrt{ R \frac{\partial \sigma_R^2}{\partial R}} / V_{200}$')
    axs[7].set_ylabel(r'$\sqrt{ R \frac{\partial \overline{v_R v_z}}{\partial z}} / V_{200}$')
    axs[8].set_ylabel(r'$\sqrt{R \overline{v_R v_z} \frac{1}{\nu} \frac{\partial \nu}{\partial z}} / V_{200}$')

    axs[-1].set_xlabel(r'$\log r / R$')

    for ii, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        print(galaxy_name)
        
        kwargs = get_name_kwargs(galaxy_name)
        snap = int(round(t / T_TOT * snaps))

        V200 = get_v_200(galaxy_name)
        r200 = get_r_200(galaxy_name)

        # bins
        bin_edges = np.logspace(-1, 3, 31)
        bin_centres = 10 ** ((np.log10(bin_edges[:-1]) + np.log10(bin_edges[1:])) / 2)
        bin_edges = np.roll(np.repeat(bin_edges, 2), -1)[:-2]
        x_data = np.log10(bin_centres / r200)

        # (_,_,_,_,_, mask, sigma_phi, mean_v_phi, sigma_R, sigma_z, Sigma
        #  ) = calc_profile_to_plot(galaxy_name, snap, np.hstack((bin_edges, np.nan, np.nan)), 
        #                           save_name_append=save_name_append)
        (_,_,_,_,_, mask, sigma_phi, mean_v_phi, sigma_R, sigma_z, Sigma
         ) = calc_profile_to_plot(galaxy_name, snap, np.hstack((bin_edges[:-1], np.nan, np.nan)),
                                  save_name_append=save_name_append)

        # theory
        analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                                 save_name_append=save_name_append, galaxy_name=galaxy_name)

        analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)

        # # update data range
        # mask_theory = np.logical_and(x_data_range[0] < x_data, x_data < x_data_range[1])

        # theory
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, kwargs['mdm'], galaxy_name=galaxy_name)

        # burn ins
        (inital_velocity_v, inital_velocity2_z, inital_velocity2_r, inital_velocity2_p
         ) = smooth_zero_snap(kwargs['mdm'], snaps, save_name_append, bin_edges)

        inital_velocity_v = np.amin((inital_velocity_v, analytic_v_circ), axis=0)

        inital_velocity2_z **=2
        inital_velocity2_r **=2
        inital_velocity2_p **= 2

        theory_best_v = np.zeros(len(bin_centres))
        theory_best_z2 = np.zeros(len(bin_centres))
        theory_best_r2 = np.zeros(len(bin_centres))
        theory_best_p2 = np.zeros(len(bin_centres))

        experiment_v = np.zeros(len(bin_centres))
        experiment_v2 = np.zeros(len(bin_centres))
        experiment_v3 = np.zeros(len(bin_centres))
        experiment_v4 = np.zeros(len(bin_centres))

        # experimenting
        tau_heat_v = np.zeros(len(bin_centres))
        tau_heat_z2 = np.zeros(len(bin_centres))
        tau_heat_r2 = np.zeros(len(bin_centres))
        tau_heat_p2 = np.zeros(len(bin_centres))

        # tau_heat_experiment = np.zeros(len(bin_centres))
        tau_heat_experiment2 = np.zeros(len(bin_centres))

        for i in range(len(bin_centres)):
            theory_best_v[i] = get_theory_velocity2_array(V200, analytic_v_circ[i] - inital_velocity_v[i],
                                                          np.array([t]), t_c_dm[i], delta_dm[i],
                                                          np.sqrt(upsilon_circ[i]),
                                                          ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
            theory_best_z2[i] = get_theory_velocity2_array(V200 ** 2, inital_velocity2_z[i],
                                                           np.array([t]), t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
            theory_best_r2[i] = get_theory_velocity2_array(V200 ** 2, inital_velocity2_r[i],
                                                           np.array([t]), t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
            theory_best_p2[i] = get_theory_velocity2_array(V200 ** 2, inital_velocity2_p[i],
                                                           np.array([t]), t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

            tau_heat_v[i] = get_tau_heat_array(delta_dm[i], np.sqrt(upsilon_circ[i]),
                                               ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])

            tau_heat_z2[i] = get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                                ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
            tau_heat_r2[i] = get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                                ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
            tau_heat_p2[i] = get_tau_heat_array(delta_dm[i], upsilon_dm[i],
                                                ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

            # tau_heat_experiment[i] = (t_c_dm[i] * (tau_heat_r2[i] * 1.0612))
            tau_heat_experiment2[i] = (t_c_dm[i] * (tau_heat_p2[i] * 0.74987599))

            tau_0_on_tau_heat = get_tau_zero_array(V200, analytic_v_circ[i] - inital_velocity_v[i],
                                                   np.sqrt(upsilon_circ[i]))

            # print(tau_0_on_tau_heat)

            # experiment_v[i] = analytic_v_circ[i] * np.exp(-np.array([t]) / tau_heat_experiment[i])  # tau_0_on_tau_heat)
            experiment_v2[i] = analytic_v_circ[i] * np.exp(
                -np.array([t]) / tau_heat_experiment2[i])  # tau_0_on_tau_heat)

            tau_0_on_tau_heat2 = get_tau_zero_array(V200 ** 2, inital_velocity2_p[i],
                                                    upsilon_dm[i])

        # theory_best_v = analytic_v_circ - theory_best_v
        theory_best_v = experiment_v2

        theory_d_sigma_R2_d_R = np.diff(theory_best_r2) / np.diff(bin_centres)
        theory_d_sigma_R2_d_R = np.hstack((theory_d_sigma_R2_d_R, np.nan))

        d_sigma_R2_d_R = get_one_d_sigma_R2_dR(galaxy_name, save_name_append, snap, bin_edges)
        d_ln_Sigma_d_ln_R = get_one_d_ln_Sigma_d_ln_R(galaxy_name, save_name_append, snap, bin_edges)

        (z_integrated_vc2, midplane_vc2, mass_vc2
         ) = get_one_vc2(galaxy_name, save_name_append, snap, bin_edges)

        (velocity_ellipsoid_term1, velocity_ellipsoid_term2
         ) = get_one_velocity_ellipsoid_term(galaxy_name, save_name_append, snap, bin_edges)

        R_disk = 4.15

        ####
        asym_full_theory = (analytic_v_circ ** 2 - theory_best_p2 +
                                   theory_best_r2 - theory_best_r2 * bin_centres / R_disk +
                                   bin_centres * theory_d_sigma_R2_d_R)


        print(np.shape(z_integrated_vc2), np.shape(sigma_phi ** 2),
                                    np.shape(sigma_R ** 2), np.shape(sigma_R ** 2), np.shape(d_ln_Sigma_d_ln_R),
                                    np.shape(bin_centres), np.shape(d_sigma_R2_d_R), np.shape(velocity_ellipsoid_term1),
                                    np.shape(velocity_ellipsoid_term2))

        asym_full_measure = (z_integrated_vc2 - sigma_phi ** 2 +
                                    sigma_R ** 2 + sigma_R ** 2 * d_ln_Sigma_d_ln_R +
                                    bin_centres * d_sigma_R2_d_R + velocity_ellipsoid_term1 +
                                    velocity_ellipsoid_term2)

        axs[0].errorbar(x_data, mean_v_phi / V200,
                        color='C0', linestyle='-', linewidth=3)
        axs[0].errorbar(x_data, np.sign(asym_full_theory) * np.sqrt(np.abs(asym_full_theory)) / V200,
                        color='C1', linestyle='--', linewidth=3)
        axs[0].errorbar(x_data, np.sign(asym_full_measure) * np.sqrt(np.abs(asym_full_measure)) / V200,
                        color='C2', linestyle='-.', linewidth=3)

        axs[0].legend(['Measured $\overline{v}_\phi$', 'All terms modelled', 'All terms measured'])

        ####
        axs[1].errorbar(x_data, mean_v_phi / V200,
                        color='C0', linestyle='-', linewidth=3)
        axs[1].errorbar(x_data, theory_best_v / V200,
                        color='C1', linestyle='--', linewidth=3)

        axs[1].legend([r'Measured', 'Modelled'])

        ####
        axs[2].errorbar(x_data, np.sqrt(z_integrated_vc2) / V200,
                        color='C0', linestyle='-', linewidth=3)

        axs[2].errorbar(x_data, analytic_v_circ / V200,
                        color='C1', linestyle='--', linewidth=3)

        axs[2].errorbar(x_data, np.sqrt(midplane_vc2) / V200,
                        color='C2', linestyle='-.', linewidth=3)

        # axs[2].errorbar(time, np.sqrt(mass_vc2[:, _index]) / V200,
        #                 color='C3', linestyle=':', linewidth=3)
        #
        # (a_h, vmax) = get_hernquist_params(galaxy_name)
        # v_halo = get_analytic_hernquist_circular_velocity(bin_edges, vmax, a_h, save_name_append)
        # (M_disk, r_disk) = get_exponential_disk_params(galaxy_name)
        # r_h = 10**((np.log10(bin_edges[0::2]) + np.log10(bin_edges[1::2]))/2)
        # v_disk = np.sqrt(GRAV_CONST * M_disk * (1 - np.exp(-r_h / r_disk) * (1 + r_h / r_disk)) / r_h)
        # spherical_vc = np.sqrt(v_halo**2 + v_disk**2)
        #
        # axs[2].errorbar(time, spherical_vc[_index] * np.ones(len(time)) / V200,
        #                 color='C4', linestyle=(1,(0.8, 0.8)), linewidth=3)

        axs[2].legend([r'$\int dz \xi V_c^2 / \int dz \xi$', r'$V_c$ from ICs',
                       r'Midplane $V_c$'])  # , r'$GM/r$', r'$GM/r$ from ICs'])

        ####
        axs[3].errorbar(x_data, -sigma_phi / V200,
                        color='C0', linestyle='-', linewidth=3)
        axs[3].errorbar(x_data, -np.sqrt(theory_best_p2) / V200,
                        color='C1', linestyle='--', linewidth=3)

        axs[3].legend([r'Measured', 'Modelled'])

        ####
        axs[4].errorbar(x_data, sigma_R / V200,
                        color='C0', linestyle='-', linewidth=3)
        axs[4].errorbar(x_data, np.sqrt(theory_best_r2) / V200,
                        color='C1', linestyle='--', linewidth=3)

        axs[4].legend([r'Measured', 'Modelled'])

        ####
        axs[5].errorbar(x_data, np.sign(sigma_R ** 2 * d_ln_Sigma_d_ln_R) * np.sqrt(np.abs(sigma_R ** 2 * d_ln_Sigma_d_ln_R)) / V200,
                        color='C0', linestyle='-', linewidth=3)

        axs[5].errorbar(x_data, -np.sqrt(theory_best_r2 * bin_centres / R_disk) / V200,
                        color='C1', linestyle='--', linewidth=3)

        # axs[5].errorbar(x_data, -np.sqrt(sigma_R[:, _index]**2) / V200,
        #                 color='C2', linestyle='-.', linewidth=3)
        # axs[5].errorbar(x_data, -np.sqrt(-sigma_R[0, _index]**2 * d_ln_Sigma_d_ln_R[:, _index]) / V200,
        #                 color='C3', linestyle=':', linewidth=3)

        axs[5].legend([r'Measured', 'Modelled'])

        ####
        axs[6].errorbar(x_data, np.sign(bin_centres * d_sigma_R2_d_R) * np.sqrt(np.abs(bin_centres * d_sigma_R2_d_R)) / V200,
                        color='C0', linestyle='-', linewidth=3)

        axs[6].errorbar(x_data, np.sign(bin_centres * theory_d_sigma_R2_d_R) * np.sqrt(np.abs(bin_centres * theory_d_sigma_R2_d_R)) / V200,
                        color='C1', linestyle='--', linewidth=3)

        axs[6].legend([r'Measured', 'Modelled'])

        ####
        axs[7].errorbar(x_data, np.sign(velocity_ellipsoid_term1) *
                        np.sqrt(np.abs(velocity_ellipsoid_term1)) / V200,
                        color='C0', linestyle='-', linewidth=3)

        axs[7].errorbar(x_data, np.sqrt(theory_best_r2 - theory_best_z2) / V200,
                        color='C1', linestyle='--', linewidth=3)

        axs[7].errorbar(x_data, np.zeros(len(x_data)) / V200,
                        color='C2', linestyle='-.', linewidth=3)

        axs[7].errorbar(x_data, np.sign(sigma_R ** 2 - sigma_z ** 2) *
                        np.sqrt(np.abs(sigma_R ** 2 - sigma_z ** 2)) / V200,
                        color='C3', linestyle=':', linewidth=3)

        axs[7].legend([r'Measured', 'Cylindrical Model', 'Spherical Model', r'Measured Cylindrical'])

        ####
        axs[8].errorbar(x_data, np.sign(velocity_ellipsoid_term2) *
                        np.sqrt(np.abs(velocity_ellipsoid_term2)) / V200,
                        color='C0', linestyle='-', linewidth=3)

        axs[8].errorbar(x_data, np.zeros(len(x_data)) / V200,
                        color='C1', linestyle='--', linewidth=3)

        axs[8].legend([r'Measured', 'Modelled'])

    if slog_yax:
        for ax in axs:
            ax.semilogy()
            ax.set_ylim([0.008, vlim[1]])

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return


def plot_explicit_asymm_time(galaxy_name_list, snaps_list,
                             save_name_append='diff', name='',
                             slog_yax=False, save=True):

    #RHS
    #mean v phi
    #Vc
    #sigma phi
    #sigma R
    #sigma R d log Sigma / d log R
    #R d sigma R / d R
    #R d mean v R v z / d z
    #mean v R v z d log nu / d z

    fig = plt.figure()
    fig.set_size_inches(7, 4.5*9, forward=True)

    _index = 1

    print('\nbeta=0, gamma=0 (fiducial)')
    (ln_Lambda_ks, alphas, betas, gammas
     ) = find_least_log_squares_best_fit(
        save_name_append, mcmc=False, fixed_b=0, fixed_g=0)

    axs = [plt.subplot(9, 1, i+1) for i in range(9)]

    fig.subplots_adjust(hspace=0.03, wspace=0.03)

    xlim = [0, T_TOT]
    vlim = [0, 1.2]
    vlim2 = [-0.6, 0.6]
    vlim3 = [-1.2, 0]

    for i, ax in enumerate(axs):
        
        if i != 8:
            ax.set_xticklabels([])

        ax.set_xlim(xlim)
        if i < 6:
            ax.set_ylim(vlim)
        else: 
            ax.set_ylim(vlim2)

    axs[3].set_ylim(vlim3)
    axs[5].set_ylim(vlim3)

    axs[0].set_ylabel(r'Asymmetric Drift $ / V_{200}$')
    axs[1].set_ylabel(r'$\overline{v}_\phi / V_{200}$')
    axs[2].set_ylabel(r'$V_c / V_{200}$')
    axs[3].set_ylabel(r'$- \sigma_\phi / V_{200}$')
    axs[4].set_ylabel(r'$\sigma_R / V_{200}$')
    axs[5].set_ylabel(r'$\sqrt{ \sigma_R^2 \frac{\partial \ln \nu}{\partial \ln R}} / V_{200}$')
    axs[6].set_ylabel(r'$\sqrt{ R \frac{\partial \sigma_R^2}{\partial R}} / V_{200}$')
    axs[7].set_ylabel(r'$\sqrt{ R \frac{\partial \overline{v_R v_z}}{\partial z}} / V_{200}$')
    axs[8].set_ylabel(r'$\sqrt{R \overline{v_R v_z} \frac{1}{\nu} \frac{\partial \nu}{\partial z}} / V_{200}$')

    axs[-1].set_xlabel(r'$t$ [Gyr]')

    for ii, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        
        kwargs = get_name_kwargs(galaxy_name)
        time = np.linspace(0, T_TOT, snaps+1)

        V200 = get_v_200(galaxy_name)

        bin_edges = get_bin_edges(galaxy_name=galaxy_name)
        bin_centres = get_bin_edges(save_name_append='cum', galaxy_name=galaxy_name)

        # load analytic values
        analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                                 save_name_append=save_name_append, galaxy_name=galaxy_name)
        analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)

        # load data
        (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
         ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges=bin_edges)

        # theory
        (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
         ) = get_physical_from_model(galaxy_name, snaps, save_name_append)
        
        # rad_bin_edges = np.logspace(-1, 3, 101)
        rad_bin_edges = np.logspace(np.log10(bin_centres[2*_index + 1])-0.1, np.log10(bin_centres[2*_index + 1])+0.1, 3)
        rad_bin_centres = 10 ** ((np.log10(rad_bin_edges[:-1]) + np.log10(rad_bin_edges[1:])) / 2)
        rad_bin_edges = np.roll(np.repeat(rad_bin_edges, 2), -1)[:-2]
        
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(rad_bin_edges, save_name_append, kwargs['mdm'], galaxy_name=galaxy_name)

        # burn ins
        (inital_velocity_v, inital_velocity2_z, inital_velocity2_r, inital_velocity2_p
         ) = smooth_zero_snap(kwargs['mdm'], snaps, save_name_append, rad_bin_edges)
        
        inital_velocity2_r **=2

        theory_rad_r2 = np.zeros((len(rad_bin_centres), snaps+1))

        for i in range(len(rad_bin_centres)):
            for j in range(snaps+1):
                theory_rad_r2[i, j] = get_theory_velocity2_array(V200 ** 2, inital_velocity2_r[i], 
                                                                 time[j], t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                                 ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])

        theory_d_sigma_R2_d_R = np.array([np.diff(theory_rad_r2[:, j]) / np.diff(rad_bin_centres) for j in range(snaps+1)]).flatten()

        d_sigma_R2_d_R = get_d_sigma_R2_dR(galaxy_name, save_name_append, snaps, bin_edges)
        d_ln_Sigma_d_ln_R = get_d_ln_Sigma_d_ln_R(galaxy_name, save_name_append, snaps, bin_edges)
        
        (z_integrated_vc2, midplane_vc2, mass_vc2
         ) = get_all_vc2s(galaxy_name, save_name_append, snaps, bin_edges)

        (velocity_ellipsoid_term1, velocity_ellipsoid_term2 
         ) = get_velocity_ellipsoid_terms(galaxy_name, save_name_append, snaps, bin_edges)

        R_disk = 4.15

        ####
        asym_full_theory = np.sqrt(analytic_v_circ[_index]**2 - theory_best_p2[_index, :] +
                            theory_best_r2[_index, :] - theory_best_r2[_index, :] * bin_centres[2*_index+1] / R_disk +
                            bin_centres[2*_index+1] * theory_d_sigma_R2_d_R)

        asym_full_measure = np.sqrt(z_integrated_vc2[:, _index] - sigma_phi[:, _index]**2 +
                            sigma_R[:, _index]**2 + sigma_R[:, _index]**2 * d_ln_Sigma_d_ln_R[:, _index] +
                            bin_centres[2*_index+1] * d_sigma_R2_d_R[:, _index] + velocity_ellipsoid_term1[:, _index] +
                            velocity_ellipsoid_term2[:, _index])
        
        axs[0].errorbar(time, mean_v_phi[:, _index] / V200,
                        color='C0', linestyle='-', linewidth=3)
        axs[0].errorbar(time, asym_full_theory / V200,
                        color='C1', linestyle='--', linewidth=3)
        axs[0].errorbar(time, asym_full_measure / V200,
                        color='C2', linestyle='-.', linewidth=3)
        
        axs[0].legend(['Measured $\overline{v}_\phi$', 'All terms modelled', 'All terms measured'])

        ####
        axs[1].errorbar(time, mean_v_phi[:, _index] / V200,
                        color='C0', linestyle='-', linewidth=3)
        axs[1].errorbar(time, theory_best_v[_index, :] / V200,
                        color='C1', linestyle='--', linewidth=3)

        axs[1].legend([r'Measured', 'Modelled'])
        
        ####
        axs[2].errorbar(time, np.sqrt(z_integrated_vc2[:, _index]) / V200,
                        color='C0', linestyle='-', linewidth=3)

        axs[2].errorbar(time, analytic_v_circ[_index] * np.ones(len(time)) / V200,
                        color='C1', linestyle='--', linewidth=3)
        
        axs[2].errorbar(time, np.sqrt(midplane_vc2[:, _index]) / V200,
                        color='C2', linestyle='-.', linewidth=3)

        # axs[2].errorbar(time, np.sqrt(mass_vc2[:, _index]) / V200,
        #                 color='C3', linestyle=':', linewidth=3)
        #
        # (a_h, vmax) = get_hernquist_params(galaxy_name)
        # v_halo = get_analytic_hernquist_circular_velocity(bin_edges, vmax, a_h, save_name_append)
        # (M_disk, r_disk) = get_exponential_disk_params(galaxy_name)
        # r_h = 10**((np.log10(bin_edges[0::2]) + np.log10(bin_edges[1::2]))/2)
        # v_disk = np.sqrt(GRAV_CONST * M_disk * (1 - np.exp(-r_h / r_disk) * (1 + r_h / r_disk)) / r_h)
        # spherical_vc = np.sqrt(v_halo**2 + v_disk**2)
        #
        # axs[2].errorbar(time, spherical_vc[_index] * np.ones(len(time)) / V200,
        #                 color='C4', linestyle=(1,(0.8, 0.8)), linewidth=3)

        axs[2].legend([r'$\int dz \xi V_c^2 / \int dz \xi$', r'$V_c$ from ICs',
                       r'Midplane $V_c$'])#, r'$GM/r$', r'$GM/r$ from ICs'])

        ####
        axs[3].errorbar(time, -sigma_phi[:, _index] / V200,
                        color='C0', linestyle='-', linewidth=3)
        axs[3].errorbar(time, -np.sqrt(theory_best_p2[_index, :]) / V200,
                        color='C1', linestyle='--', linewidth=3)

        axs[3].legend([r'Measured', 'Modelled'])
        
        ####
        axs[4].errorbar(time, sigma_R[:, _index] / V200,
                        color='C0', linestyle='-', linewidth=3)
        axs[4].errorbar(time, np.sqrt(theory_best_r2[_index, :]) / V200,
                        color='C1', linestyle='--', linewidth=3)

        axs[4].legend([r'Measured', 'Modelled'])

        ####
        axs[5].errorbar(time, -np.sqrt(-sigma_R[:, _index]**2 * d_ln_Sigma_d_ln_R[:, _index]) / V200,
                        color='C0', linestyle='-', linewidth=3)

        axs[5].errorbar(time, -np.sqrt(theory_best_r2[_index, :] * bin_centres[2*_index+1] / R_disk) / V200,
                        color='C1', linestyle='--', linewidth=3)
        
        # axs[5].errorbar(time, -np.sqrt(sigma_R[:, _index]**2) / V200,
        #                 color='C2', linestyle='-.', linewidth=3)
        # axs[5].errorbar(time, -np.sqrt(-sigma_R[0, _index]**2 * d_ln_Sigma_d_ln_R[:, _index]) / V200,
        #                 color='C3', linestyle=':', linewidth=3)

        axs[5].legend([r'Measured', 'Modelled'])

        ####
        axs[6].errorbar(time, -np.sqrt(- bin_centres[2*_index+1] * d_sigma_R2_d_R[:, _index]) / V200,
                        color='C0', linestyle='-', linewidth=3)

        axs[6].errorbar(time, -np.sqrt(- bin_centres[2*_index+1] * theory_d_sigma_R2_d_R) / V200,
                        color='C1', linestyle='--', linewidth=3)

        axs[6].legend([r'Measured', 'Modelled'])

        ####
        axs[7].errorbar(time, np.sign(velocity_ellipsoid_term1[:, _index]) *
                        np.sqrt(np.abs(velocity_ellipsoid_term1[:, _index])) / V200,
                        color='C0', linestyle='-', linewidth=3)

        axs[7].errorbar(time, np.sqrt(theory_best_r2[_index, :] - theory_best_z2[_index, :]) / V200,
                        color='C1', linestyle='--', linewidth=3)

        axs[7].errorbar(time, np.zeros(len(time)) / V200,
                        color='C2', linestyle='-.', linewidth=3)

        axs[7].errorbar(time, np.sign(sigma_R[:, _index]**2 - sigma_z[:, _index]**2) * 
                        np.sqrt(np.abs(sigma_R[:, _index]**2 - sigma_z[:, _index]**2)) / V200,
                        color='C3', linestyle=':', linewidth=3)

        axs[7].legend([r'Measured', 'Cylindrical Model', 'Spherical Model', r'Measured Cylindrical'])

        ####
        axs[8].errorbar(time, np.sign(velocity_ellipsoid_term2[:, _index]) *
                        np.sqrt(np.abs(velocity_ellipsoid_term2[:, _index])) / V200,
                        color='C0', linestyle='-', linewidth=3)

        axs[8].errorbar(time, np.zeros(len(time)) / V200,
                        color='C1', linestyle='--', linewidth=3)

        axs[8].legend([r'Measured', 'Modelled'])

    if slog_yax:
        for ax in axs:
            ax.semilogy()
            ax.set_ylim([0.008, vlim[1]])

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return


def highlight_real_space(galaxy_name_list, snaps_list,
                         save_name_append='diff', name='', model=True, save=False):
    _index = 1

    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(7.2, 11.3, forward=True)

    ax0 = plt.subplot(2, 1, 1)
    ax1 = plt.subplot(2, 1, 2)

    fig.subplots_adjust(hspace=0.03, wspace=0.03)

    ax0.set_xticklabels([])
    # ax0.set_yticks([-2, -1, 0])
    # ax0.set_yticklabels([0.01, 0.1, 1])
    # # ax1.set_xticks([-2, -1, 0])
    # # ax1.set_xticklabels([0.01, 0.1, 1])
    # ax1.set_yticks([-2, -1, 0])
    # ax1.set_yticklabels([0.01, 0.1, 1])

    xlim = [0, T_TOT * (101 - BURN_IN) / 101]
    ax0.set_xlim(xlim)
    ax1.set_xlim(xlim)

    if save_name_append == 'diff':
        # vlim = [0, 260]
        # slim = [0, 240]
        vlim = [0, 1.3]
        slim = [0, 1.1]
    elif save_name_append == 'cum':
        vlim = [-1.3, 0.1]
        slim = [-1.3, 0.3]

    ax0.set_ylim(slim)
    ax1.set_ylim(vlim)

    # ax0.set_ylabel(r'$v_c - \overline{v_\phi}$ [km/s]')
    # ax0.set_ylabel('$\sigma_\phi$ [km/s]')
    # ax1.set_ylabel('$\overline{v_\phi}$ [km/s]')
    ax0.set_ylabel('$\sigma_\phi / V_{200}$')
    ax1.set_ylabel('$\overline{v}_\phi / V_{200}$')
    ax1.set_xlabel(r'$t$ [Gyr]')

    ax1.set_yticks([0,0.2,0.4,0.6,0.8,1,1.2])

    # bin_edges = get_bin_edges(save_name_append)

    bin_edges = get_dex_bins(get_bin_edges(save_name_append='cum')[1::2], dex=0.1)

    # work out constatns
    (_, _, delta_dm, upsilon_dm, upsilon_circ
     ) = get_constants(bin_edges, save_name_append, 0)

    for galaxy_name, snaps in zip(galaxy_name_list, snaps_list):
        kwargs = get_name_kwargs(galaxy_name)

        v200 = get_v_200(galaxy_name)

        # load analytic values
        analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                                 save_name_append=save_name_append, galaxy_name=galaxy_name)
        analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)

        # load data
        (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
         ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges=bin_edges)
        
        # if '7p5' in galaxy_name:
        #     # load data
        #     (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
        #      ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges=bin_edges, overwrite=True)

        time = np.linspace(0, T_TOT, snaps + 1)
        
        # if '8p0' in galaxy_name:
        # 
        #     # zero = 5
        #     print(galaxy_name)
        #     for zero in range(10):
        #         print(time[zero])
        #         # print(sigma_z[zero])
        #         # print(sigma_R[zero])
        #         # print(sigma_phi[zero])
        #         # print(mean_v_phi[zero])
        #         # print(sigma_z[-1])
        #         # print(sigma_R[-1])
        #         # print(sigma_phi[-1])
        #         # print(mean_v_phi[-1])
        #         print(sigma_z[-1] / sigma_z[zero])
        #         print(sigma_R[-1] / sigma_R[zero])
        #         print(sigma_phi[-1] / sigma_phi[zero])
        #         print(mean_v_phi[-1] / mean_v_phi[zero])
        #         # print(sigma_phi[zero]**2 / sigma_R[zero]**2)
        #         # print(sigma_z[zero]**2 / sigma_R[zero]**2)
        #         # print(1 - 2 * sigma_z[zero]**2 / (sigma_R[zero]**2 + sigma_phi[zero]**2))
        #         print()

        t5gyr = np.argmin(np.abs(np.linspace(0, T_TOT, snaps + 1) - 5))
        
        print(galaxy_name)
        print(sigma_phi[t5gyr, _index])
        # t5gyr = np.argmin(np.abs(np.linspace(0, T_TOT, snaps + 1) - 5))
        print(sigma_phi[0, _index])

        # time
        # time = time - time[BURN_IN]

        # #burn ins
        # (burn_max_v, _,_,_
        #  ) = smooth_burn_ins(kwargs['mdm'], snaps, save_name_append, BURN_IN, bin_edges)

        alpha = 0.2
        lwt = 6
        lw = 3
        window = snaps // 5 + 1
        if window % 2 == 0: window += 1
        poly_n = 5
        # vs time
        y = sigma_phi[:, _index] / v200
        ax0.errorbar(time, y,
                     c=kwargs['c'], marker='', ls='-', lw=lwt, alpha=alpha)
        ax0.errorbar(time, savgol_filter(y, window, poly_n),
                     c=kwargs['c'], marker='', lw=lw, ls=kwargs['ls'])

        y = mean_v_phi[:, _index] / v200
        ax1.errorbar(time, y,
                     c=kwargs['c'], marker='', ls='-', lw=lwt, alpha=alpha)
        ax1.errorbar(time, savgol_filter(y, window, poly_n),
                     c=kwargs['c'], marker='', lw=lw, ls=kwargs['ls'])

        if kwargs['mun'] == 5:

            # theory
            (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
             ) = get_physical_from_model(galaxy_name, snaps, save_name_append)

            c = kwargs['c']
            ls = '--'

            if model:
                ax0.errorbar(time, np.sqrt(theory_best_p2[_index, :]) / v200, c=c, ls=ls, lw=3)
                ax1.errorbar(time, theory_best_v[_index, :] / v200, c=c, ls=ls, lw=3)

            if kwargs['c'] in ['C0', 'C1', 'C2']:
                dx = 0.05
            else:
                dx = 0.45

            if kwargs['c'] in ['C0', 'C3']:
                dy = 0.96
            elif kwargs['c'] in ['C1', 'C4']:
                dy = 0.88
            else:
                dy = 0.80

            ax0.text(xlim[0] + dx * (xlim[1] - xlim[0]), slim[0] + dy * (slim[1] - slim[0]),
                     r'$\mathrm{N}_{\mathrm{DM}} = $' + kwargs['labn'], ha='left', va='top', color=kwargs['c'])

    # asymptote max
    ax0.errorbar([0, T_TOT], [analytic_dispersion[_index] / v200] * 2, color='k', ls='-')
    ax1.errorbar([0, T_TOT], [analytic_v_circ[_index] / v200] * 2, color='k', ls='-', zorder=-10)

    # ax0.text(xlim[0]+0.05*(xlim[1]-xlim[0]), vlim[0]+0.95*(vlim[1]-vlim[0]),
    #          r'$t =$' +str(t)+'Gyr', ha='left', va='top')
    # ax0.text(xlim[0]+0.5*(xlim[1]-xlim[0]), vlim[0]+0.45*(vlim[1]-vlim[0]),
    #          r'$r_{1/2}$', ha='left', va='bottom')

    ax1.text(xlim[0] + 0.05 * (xlim[1] - xlim[0]), vlim[0] + 0.95 * (vlim[1] - vlim[0]),
             r'$R = R_{1/2}$',  # , V_{200} = 200$ km/s',
             ha='left', va='top')

    ax0.text(xlim[0] + 0.95 * (xlim[1] - xlim[0]), slim[0] + 0.78 * (slim[1] - slim[0]),
             r'$\sigma_{\mathrm{DM}}$', ha='right', va='top')
    ax1.text(xlim[0] + 0.95 * (xlim[1] - xlim[0]), vlim[0] + 0.84 * (vlim[1] - vlim[0]),
             r'$V_c$', ha='right', va='top')

    ax0.legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='-', linewidth=3)),
                (lines.Line2D([0, 1], [0, 1], color='grey', ls='--', linewidth=3))],
               ['Simulations', 'equation (6)'],
               handler_map={tuple: HandlerTupleVertical(ndivide=1)},
               loc='lower left', handletextpad=0.2, frameon=True, framealpha=0.95,
               labelspacing=0.4, borderpad=0.2, bbox_to_anchor=(0, 0.52))
    ax1.legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='-', linewidth=3)),
                (lines.Line2D([0, 1], [0, 1], color='grey', ls='--', linewidth=3))],
               ['Simulations', 'equation (11)'],
               handler_map={tuple: HandlerTupleVertical(ndivide=1)},
               loc='lower left', handletextpad=0.2, frameon=False,
               labelspacing=0.4, borderpad=0.2)  # , bbox_to_anchor=(1, 0))

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return


def plot_kinetic_energy_change(galaxy_name_list, snaps_list,
                               save_name_append='diff', name='', save=False):
    # galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',]
    # snaps_list = [400]

    # galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',]
    # snaps_list = [101]

    # galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps', 'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',]
    # snaps_list = [101, 400]

    _index = 1

    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(7.2, 11.3, forward=True)

    ax0 = plt.subplot(2, 1, 1)
    ax1 = plt.subplot(2, 1, 2)

    fig.subplots_adjust(hspace=0.03, wspace=0.03)

    ax0.set_xticklabels([])

    # xlim = [0, T_TOT * (101 - BURN_IN) / 101]
    # ax0.set_xlim(xlim)
    # ax1.set_xlim(xlim)

    # vlim = [0, 1.3]
    # slim = [0, 1.1]
    # ax0.set_ylim(slim)
    # ax1.set_ylim(vlim)

    ax0.set_ylabel('$\log (1 + \Delta KE / KE_0)$ at $R_{1/2}$')
    ax1.set_ylabel('$\log (1 + \Delta KE / KE_0)$ total')
    # ax1.set_xlabel(r'$t$ [Gyr]')
    ax1.set_xlabel(r'$\log \, t / \rm Gyr$')

    # lss = [':', ':', ':', '-', '-', '-', '-', '-', '-']
    # labels = [r'$E_{\sigma z}$', r'$E_{\sigma R}$', r'$E_{\sigma \phi}$', r'$E_{v \phi}$',
    #           r'$E_{\sigma \rm tot}$', r'$E_{\rm tot}$']

    bin_edges = get_dex_bins(get_bin_edges(save_name_append='cum')[1::2], dex=0.1)

    # work out constatns
    (_, _, delta_dm, upsilon_dm, upsilon_circ
     ) = get_constants(bin_edges, save_name_append, 0)

    for k, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = get_name_kwargs(galaxy_name)
        kk = 2 * k

        v200 = get_v_200(galaxy_name)

        # load analytic values
        analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                                 save_name_append=save_name_append, galaxy_name=galaxy_name)
        analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)

        # load data
        (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
         ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges=bin_edges)

        # time = np.log10(np.linspace(0, T_TOT, snaps + 1))
        time = np.linspace(0, T_TOT, snaps + 1)

        E_sigma_z = 0.5 * sigma_z**2
        E_sigma_r = 0.5 * sigma_R**2
        E_sigma_p = 0.5 * sigma_phi**2
        E_v_phi = 0.5 * mean_v_phi**2

        E_sigma_tot = 0.5 * (sigma_z**2 + sigma_R**2 + sigma_phi**2)
        E_tot = 0.5 * (sigma_z**2 + sigma_R**2 + sigma_phi**2 + mean_v_phi**2)

        Es = [E_sigma_z, E_sigma_r, E_sigma_p, E_v_phi, E_sigma_tot, E_tot]
        
        #plot
        alpha = 0.2
        lwt = 6
        lw = 3
        window = snaps // 5 + 1
        if window % 2 == 0: window += 1
        poly_n = 5

        one = 0
        func = lambda y: np.amin(y)
        # func = lambda y: y[0]

        # vs time

        # for i, (ys, ls, lab) in enumerate(zip(Es, lss, labels)):
        #     y = ys[:, _index]
        #     ax0.errorbar(time, y - y[0],
        #                  c=f'C{i}', marker='', ls='-', lw=lwt, alpha=alpha)
        #     ax0.errorbar(time, savgol_filter(y - y[0], window, poly_n),
        #                  c=f'C{i}', marker='', lw=lw, ls=ls, label=lab)

        BURN_IN = 2

        y = (E_sigma_tot[:, _index] - E_sigma_tot[BURN_IN, _index]) / E_tot[BURN_IN, _index]
        ax0.errorbar(time, np.log10(1 + y),
                     c=f'C{kk}', marker='', ls='-', lw=lwt, alpha=alpha)
        ax0.errorbar(time, np.log10(1 + savgol_filter(y, window, poly_n)),
                     c=f'C{kk}', marker='', lw=lw, ls='-', label=kwargs['lab'])

        y = (E_tot[:, _index] - E_tot[BURN_IN, _index]) / E_tot[BURN_IN, _index]
        ax0.errorbar(time, np.log10(1 + y),
                     c=f'C{kk}', marker='', ls='-', lw=lwt, alpha=alpha)
        ax0.errorbar(time, np.log10(1 + savgol_filter(y, window, poly_n)),
                     c=f'C{kk}', marker='', lw=lw, ls='--',) #label=r'$E_{\rm TOT} ,\, m_{\rm DM} =$' +
                                                             #                 [r'$10^7$', r'$10^8$'][kk])

        (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
         ) = get_physical_from_model(galaxy_name, snaps, save_name_append)

        E_sigma_z_theory = 0.5 * theory_best_z2
        E_sigma_r_theory = 0.5 * theory_best_r2
        E_sigma_p_theory = 0.5 * theory_best_p2
        E_v_phi_theory = 0.5 * theory_best_v**2

        E_sigma_tot = 0.5 * (theory_best_z2 + theory_best_r2 + theory_best_p2)
        E_tot = 0.5 * (theory_best_z2 + theory_best_r2 + theory_best_p2 + theory_best_v**2)

        lw=2
        y = (E_sigma_tot[_index, :] - E_sigma_tot[_index, BURN_IN]) / E_tot[_index, BURN_IN]
        ax0.errorbar(time, np.log10(1 + savgol_filter(y, window, poly_n)),
                     c=f'C{kk}', marker='', lw=lw, ls=':')

        y = (E_tot[_index, :] - E_tot[_index, BURN_IN]) / E_tot[_index, BURN_IN]
        ax0.errorbar(time, np.log10(1 + savgol_filter(y, window, poly_n)),
                     c=f'C{kk}', marker='', lw=lw, ls=':',) #label=r'$E_{\rm TOT} ,\, m_{\rm DM} =$' +
        lw=3

        # load data
        (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
         ) = get_dispersions(galaxy_name, 'cum', snaps, bin_edges=[0,30], overwrite=False)

        E_sigma_z = 0.5 * sigma_z ** 2
        E_sigma_r = 0.5 * sigma_R ** 2
        E_sigma_p = 0.5 * sigma_phi ** 2
        E_v_phi = 0.5 * mean_v_phi ** 2

        E_sigma_tot = 0.5 * (sigma_z ** 2 + sigma_R ** 2 + sigma_phi ** 2)
        E_tot = 0.5 * (sigma_z ** 2 + sigma_R ** 2 + sigma_phi ** 2 + mean_v_phi ** 2)

        Es = [E_sigma_z, E_sigma_r, E_sigma_p, E_v_phi, E_sigma_tot, E_tot]

        # for i, (ys, ls, lab) in enumerate(zip(Es, lss, labels)):
        #     y = ys[:, 0]
        #     ax1.errorbar(time, y - y[0],
        #                  c=f'C{i}', marker='', ls='-', lw=lwt, alpha=alpha)
        #     ax1.errorbar(time, savgol_filter(y - y[0], window, poly_n),
        #                  c=f'C{i}', marker='', lw=lw, ls=ls, label=lab)

        y = (E_sigma_tot[:, 0] - E_sigma_tot[BURN_IN, 0]) / E_tot[BURN_IN, 0]
        ax1.errorbar(time, np.log10(1 + y),
                     c=f'C{kk}', marker='', ls='-', lw=lwt, alpha=alpha)
        ax1.errorbar(time, np.log10(1 + savgol_filter(y, window, poly_n)),
                     c=f'C{kk}', marker='', lw=lw, ls='-')

        y = (E_tot[:, 0] - E_tot[BURN_IN, 0]) / E_tot[BURN_IN, 0]
        ax1.errorbar(time, np.log10(1 + y),
                     c=f'C{kk}', marker='', ls='-', lw=lwt, alpha=alpha)
        ax1.errorbar(time, np.log10(1 + savgol_filter(y, window, poly_n)),
                     c=f'C{kk}', marker='', lw=lw, ls='--')

        print(galaxy_name)
        print((E_sigma_tot[:, 0] / E_sigma_tot[BURN_IN, 0])[-1])
        print((E_tot[:, 0] / E_tot[BURN_IN, 0])[-1])

    ax0.axhline(0, 0, 1, c='k', ls=':')
    ax1.axhline(0, 0, 1, c='k', ls=':')
    
    ax0.legend(framealpha=0.4)

    ax1.legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='-', linewidth=3)),
                (lines.Line2D([0, 1], [0, 1], color='grey', ls='--', linewidth=3))],
               ['Dispersion KE', 'Total KE', 'Model KE'], framealpha=0.4)

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return


def plot_real_space(galaxy_name_list, snaps_list,
                    save_name_append='diff', name='', save=False):
    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(12, 14, forward=True)
    widths = [1, 1, 1]
    heights = [1, 1, 1, 1]
    spec = fig.add_gridspec(ncols=3, nrows=4, width_ratios=widths,
                            height_ratios=heights)

    min_t = -0.5
    min_y = 8
    max_y = 200
    # min_y2 = -10
    min_y2 = 2
    max_y2 = 240

    axs = []
    for row in range(len(heights)):
        axs.append([])
        for col in range(len(widths)):
            axs[row].append(fig.add_subplot(spec[row, col]))
            axs[row][col].set_xlim([min_t, T_TOT])

            if row == 0:
                axs[row][col].set_ylim([min_y2, max_y2])
            else:
                axs[row][col].set_ylim([min_y, max_y])
            if col != 0: axs[row][col].set_yticklabels([])
            if row != len(heights) - 1: axs[row][col].set_xticklabels([])

            plt.semilogy()

    fig.subplots_adjust(hspace=0.0, wspace=0.0)

    # axs[0][0].set_ylabel(r'$\overline{v_\phi}$ [km/s]')
    axs[0][0].set_ylabel(r'$v_c - \overline{v_\phi}$ [km/s]')
    axs[1][0].set_ylabel(r'$\sigma_z$ [km/s]')
    axs[2][0].set_ylabel(r'$\sigma_R$ [km/s]')
    axs[3][0].set_ylabel(r'$\sigma_\phi$ [km/s]')
    axs[3][1].set_xlabel(r'$t$ [Gyr]')

    burn_in = BURN_IN

    bin_edges = get_bin_edges(save_name_append)

    # work out constatns
    (_, _, delta_dm, upsilon_dm, upsilon_circ
     ) = get_constants(bin_edges, save_name_append, 0)

    # load analytic values
    analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0,
                                             v200=False, dm_dispersion=False, dm_v_circ=False,
                                             analytic_dispersion=True, analytic_v_c=False,
                                             save_name_append=save_name_append)

    analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0,
                                         v200=False, dm_dispersion=False, dm_v_circ=False,
                                         analytic_dispersion=False, analytic_v_c=True,
                                         save_name_append=save_name_append)

    for galaxy_name, snaps in zip(galaxy_name_list, snaps_list):
        kwargs = get_name_kwargs(galaxy_name)

        # load data
        (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
         ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges=bin_edges)

        # time
        time = np.linspace(0, T_TOT, snaps + 1)
        time = time - time[burn_in]

        # burn ins
        (burn_max_v, _, _, _
         ) = smooth_burn_ins(kwargs['mdm'], snaps, save_name_append, burn_in, bin_edges)

        (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
         ) = get_physical_from_model(galaxy_name, snaps, save_name_append)

        for i in range(3):
            axs[0][i].errorbar(time, analytic_v_circ[i] - mean_v_phi[:, i], c=kwargs['c'], marker=kwargs['marker'],
                               alpha=0.2)
            axs[1][i].errorbar(time, sigma_z[:, i], c=kwargs['c'], marker=kwargs['marker'], alpha=0.2)
            axs[2][i].errorbar(time, sigma_R[:, i], c=kwargs['c'], marker=kwargs['marker'], alpha=0.2)
            axs[3][i].errorbar(time, sigma_phi[:, i], c=kwargs['c'], marker=kwargs['marker'], alpha=0.2)

            c = 'k'
            ls = '--'
            axs[0][i].errorbar(time, theory_best_v[:, i], c=c, ls=ls)
            axs[1][i].errorbar(time, np.sqrt(theory_best_z2[:, i]), c=c, ls=ls)
            axs[2][i].errorbar(time, np.sqrt(theory_best_r2[:, i]), c=c, ls=ls)
            axs[3][i].errorbar(time, np.sqrt(theory_best_p2[:, i]), c=c, ls=ls)

    # asymptote max
    for i in range(3):
        axs[0][i].errorbar([min_t, T_TOT], 0, color='grey', ls='-', zorder=-10)
        axs[0][i].errorbar([min_t, T_TOT], burn_max_v[i], color='grey', ls=':')

        for j in range(3):
            axs[j + 1][i].errorbar([min_t, T_TOT], analytic_dispersion[i], color='grey', ls=':')

    plt.show()
    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return


###############################################################################

def edge_on_razor_thin_ring_lambda_r(v_phi, sigma_disk, R_inner, R_outer, drho):
    '''R is disk plane radius, rho is edge on pixel distance from centre.
  '''
    # result of integral of sqrt(r^2 - rho^2) drho
    int_result = lambda r, rho: rho * np.sqrt(r ** 2 - rho ** 2) + r ** 2 * np.arctan(rho / np.sqrt(r ** 2 - rho ** 2))

    rho_edges = np.arange(0, R_outer + drho, drho)
    rho_centres = (rho_edges[1:] + rho_edges[:-1]) / 2

    # v_y = v_phi * cos(phi), phi = arccos(rho / r)
    # TODO this is an ok approximation for a thin ring. Needs to do better for thick ring.

    v_pix = v_phi * rho_centres / ((R_inner + R_outer) / 2)

    # v_pix = v_phi * (rho_edges[:-1] / R_inner + rho_edges[1:] / R_outer)/2

    # calculate pixel area
    outer_R_outer_rho = int_result(R_outer, rho_edges[1:])
    outer_R_inner_rho = int_result(R_outer, rho_edges[:-1])
    inner_R_outer_rho = int_result(R_inner, rho_edges[1:])
    inner_R_inner_rho = int_result(R_inner, rho_edges[:-1])

    # outside of integration region
    outer_R_outer_rho[np.isnan(outer_R_outer_rho)] = R_outer ** 2 * np.pi / 2
    outer_R_inner_rho[np.isnan(outer_R_inner_rho)] = R_outer ** 2 * np.pi / 2
    inner_R_outer_rho[np.isnan(inner_R_outer_rho)] = R_inner ** 2 * np.pi / 2
    inner_R_inner_rho[np.isnan(inner_R_inner_rho)] = R_inner ** 2 * np.pi / 2

    m_pix = outer_R_outer_rho - outer_R_inner_rho - inner_R_outer_rho + inner_R_inner_rho

    # output
    lambda_r = np.sum(m_pix * rho_centres * v_pix) / np.sum(
        m_pix * rho_centres * np.sqrt(v_pix ** 2 + sigma_disk ** 2))

    return lambda_r


def plot_modelled_indicators(galaxy_name_list, snaps_list,
                             save_name_append='diff', name='', save=False):
    _index = 1

    # set up plots
    fig = plt.figure(constrained_layout=True)
    # fig.set_size_inches(9, 12, forward=True)
    fig.set_size_inches(9, 16, forward=True)
    # fig.set_size_inches(18, 8, forward=True)
    # widths = [1, 1]
    # heights = [1, 1] #[1, 1, 1]
    # spec = fig.add_gridspec(ncols=2, nrows=2, width_ratios=widths, height_ratios=heights)
    widths = [1]
    heights = [1, 1, 1, 1]
    spec = fig.add_gridspec(ncols=1, nrows=4, width_ratios=widths, height_ratios=heights)

    xlim1 = [8e5, 1.2e8]

    axs = []
    for row in range(len(heights)):
        axs.append([])
        for col in range(len(widths)):
            axs[row].append(fig.add_subplot(spec[row, col]))
            if col == 0:
                axs[row][col].semilogy()
                # axs[row][col].set_xlim([0, T_TOT * (101 - BURN_IN) / 101])
                axs[row][col].set_xlim([0, T_TOT])
            if col == 1:
                axs[row][col].loglog()
                axs[row][col].set_xlim(xlim1)
            if col != 0: axs[row][col].set_yticklabels([])
            if row != len(heights) - 1: axs[row][col].set_xticklabels([])

    # axs = []
    # for row in range(4):
    #     axs.append([])
    #     for col in range(1):
    #         axs[row].append(fig.add_subplot(spec[row//2, row%2]))
    #         if col == 0:
    #             axs[row][col].semilogy()
    #             # axs[row][col].set_xlim([0, T_TOT * (101 - BURN_IN) / 101])
    #             axs[row][col].set_xlim([0, T_TOT])
    #         if col == 1:
    #             axs[row][col].loglog()
    #             axs[row][col].set_xlim(xlim1)
    #         if col != 0: axs[row][col].set_yticklabels([])
    #         if row//2 == 0: axs[row][col].set_xticklabels([])
    # widths = [1]

    ylim0 = [0.1, 40]
    ylim1 = [0.001, 0.95] #[0.0002, 1.05]
    ylim2 = [0.0015, 5/6] #2 / 3]
    ylim3 = [0.0015, 1]

    for col in range(len(widths)):
        axs[0][col].set_ylim(ylim0)
        axs[1][col].set_ylim(ylim1)
        axs[2][col].set_ylim(ylim2)
        axs[3][col].set_ylim(ylim3)

        # axs[0][col].axhline(0.5, 0, 1, c='grey', ls=':')
        axs[0][col].axhline(1, 0, 1, c='grey', ls=':')
        axs[2][col].axhline(1-0.5, 0, 1, c='grey', ls=':')
        axs[2][col].axhline(1-0.7, 0, 1, c='grey', ls=':')
        axs[3][col].axhline(1-0.4, 0, 1, c='grey', ls=':')
        # axs[2][col].axhline(1 - 0.3, 0, 1, c='grey', ls=':')

    # axs[0][0].set_ylabel(r'$\overline{v_\phi} / \sqrt{\sigma_{\mathrm{tot}}^2 / 3}$')
    axs[0][0].set_ylabel(r'$\overline{v}_\phi / \sigma_{1\mathrm{D}}$')
    axs[1][0].set_ylabel(r'$1 - \lambda_r$')
    axs[2][0].set_ylabel(r'$1 - \kappa_{\mathrm{rot}}$')
    axs[3][0].set_ylabel(r'$1 - \kappa_{\mathrm{co}}$')
    axs[3][0].set_xlabel(r'$t$ [Gyr]')
    axs[2][0].set_xlabel(r'$t$ [Gyr]')

    if save_name_append == 'cum':
        pass
    elif save_name_append == 'diff':
        if _index == 0:
            axs[0][0].text(0.2, 0.22, r'$[R_{1/4} \pm 0.1$dex]', ha='left', va='bottom')
        elif _index == 1:
            axs[0][0].text(0.2, 0.3, r'$V_{200} = 200$ km/s', ha='left', va='bottom')
            axs[0][0].text(0.2, 0.12, r'$R = R_{1/2, 0}$', ha='left', va='bottom')
            # axs[1][0].text(0.2, 0.0018, r'$R = R_{1/2, 0}$', ha='left', va='bottom')
            # axs[2][0].text(0.2, 0.00024, r'$R = R_{1/2, 0}$', ha='left', va='bottom')
        elif _index == 2:
            axs[0][0].text(0.2, 0.22, r'$[R_{3/4} \pm 0.1$dex]', ha='left', va='bottom')

    # fig.subplots_adjust(hspace=0.03, wspace=0.01)
    fig.subplots_adjust(hspace=0.02, wspace=0.15)

    # where to measure
    bin_edges = get_bin_edges(save_name_append)

    for galaxy_name, snaps in zip(galaxy_name_list, snaps_list):
        kwargs = get_name_kwargs(galaxy_name)

        bin_edges = get_bin_edges(save_name_append, galaxy_name=galaxy_name)

        # mdm
        mdm = kwargs['mdm'] * 1e10

        # time
        time = np.linspace(0, T_TOT, snaps + 1)
        # time = time - time[BURN_IN]

        # load data
        (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
         ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges=bin_edges)

        sigma_tot2 = sigma_z ** 2 + sigma_R ** 2 + sigma_phi ** 2
        v_on_sigma = mean_v_phi / np.sqrt(sigma_tot2 / 3)

        kappa_rot = get_kappa_rot(galaxy_name, save_name_append, snaps, bin_edges)
        kappa_co  = get_kappa_co(galaxy_name, save_name_append, snaps, bin_edges)

        # sqrt(3) is not correct but makes lambda_R better
        # lambda_r = mean_v_phi / np.sqrt(mean_v_phi**2 + sigma_tot2/np.sqrt(3))
        lambda_r = mean_v_phi / np.sqrt(mean_v_phi ** 2 + sigma_tot2 / 3)

        # plot
        alpha = 0.2
        lwt = 6
        lw = 3
        window = snaps // 5 + 1
        if window % 2 == 0: window += 1
        poly_n = 5
        # vs time
        y = v_on_sigma[:, _index]
        axs[0][0].errorbar(time, y,
                           c=kwargs['c'], marker='', ls='-', lw=lwt, alpha=alpha)
        try:
            axs[0][0].errorbar(time, savgol_filter(y, window, poly_n),
                               c=kwargs['c'], marker='', lw=lw, ls=kwargs['ls'])
        except np.linalg.LinAlgError:
            pass

        # if not_low_res:
        y = 1 - lambda_r[:, _index]
        axs[1][0].errorbar(time, y,
                           c=kwargs['c'], marker='', ls='-', lw=lwt, alpha=alpha)
        try:
          axs[1][0].errorbar(time, savgol_filter(y, window, poly_n),
                               c=kwargs['c'], marker='', lw=lw, ls=kwargs['ls'])
        except np.linalg.LinAlgError:
            pass

        y = 1 - kappa_rot[:, _index]
        axs[2][0].errorbar(time, y,
                           c=kwargs['c'], marker='', ls='-', lw=lwt, alpha=alpha)

        try:
            axs[2][0].errorbar(time, savgol_filter(y, window, poly_n),
                               c=kwargs['c'], marker='', lw=lw, ls=kwargs['ls'])
        except np.linalg.LinAlgError:
            pass

        y = 1 - kappa_co[:, _index]
        axs[3][0].errorbar(time, y,
                           c=kwargs['c'], marker='', ls='-', lw=lwt, alpha=alpha)

        try:
            axs[3][0].errorbar(time, savgol_filter(y, window, poly_n),
                               c=kwargs['c'], marker='', lw=lw, ls=kwargs['ls'])
        except np.linalg.LinAlgError:
            pass

        if True:  # kwargs['marker'] == '^':
            ####
            # get theory
            (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
             ) = get_physical_from_model(galaxy_name, snaps, save_name_append)

            theory_best_tot2 = theory_best_z2 + theory_best_r2 + theory_best_p2
            theory_best_v_on_sigma = theory_best_v / np.sqrt(theory_best_tot2 / 3)

            theory_best_kappa_rot = (theory_best_v ** 2 + theory_best_p2) / (
                    theory_best_v ** 2 + theory_best_tot2)

            theory_best_kappa_co2 = ((theory_best_v ** 2 + theory_best_p2)/2 *
                                     (erf(theory_best_v / np.sqrt(2 * theory_best_p2)) + 1)
                                      + (theory_best_v * np.sqrt(theory_best_p2 /(2 * np.pi)) *
                                          np.exp(-theory_best_v**2 / (2 * theory_best_p2))
                                          )) / (theory_best_v ** 2 + theory_best_tot2)

            # rms2_positive_v_phi = np.zeros(snaps + 1)
            # v_max = 1000
            # v_min = 0  # -1000
            # for i in range(snaps + 1):
            #     v = theory_best_v[_index, i]
            #     s = np.sqrt(theory_best_p2[_index, i])
            #
            #     v2_int = lambda x: x ** 2 * scipy.stats.norm.pdf(x, loc=v, scale=s)
            #     v2_out = quad(v2_int, v_min, v_max)[0]
            #
            #     # m_frac_out = 1-scipy.stats.norm.cdf(0, loc=v, scale=s)
            #
            #     area_out = 1
            #     rms2_positive_v_phi[i] = v2_out / area_out  # * m_frac_out
            #
            #
            # theory_best_kappa_co = rms2_positive_v_phi / (theory_best_v ** 2 + theory_best_tot2)

            theory_lambda_r = theory_best_v / np.sqrt(theory_best_v ** 2 + theory_best_tot2 / 3)

            # vs time
            axs[0][0].errorbar(time, theory_best_v_on_sigma[_index, :], c=kwargs['c'], ls='--', lw=3, zorder=5)

            axs[1][0].errorbar(time, 1 - theory_lambda_r[_index, :], c=kwargs['c'], ls='--', lw=3, zorder=5)

            axs[2][0].errorbar(time, 1 - theory_best_kappa_rot[_index, :], c=kwargs['c'], ls='--', lw=3, zorder=5)

            # axs[2][0].errorbar(time, 1 - theory_best_kappa_co[_index, :], c=kwargs['c'], ls='--', lw=3, zorder=5)
            axs[3][0].errorbar(time, 1 - theory_best_kappa_co2[_index, :], c=kwargs['c'], ls='--', lw=3, zorder=5)

            if kwargs['c'] in ['C0', 'C1', 'C2']:
                dx = 0.38
            else:
                dx = 0.68
            if kwargs['c'] in ['C0', 'C3']:
                dy = 0.23
            elif kwargs['c'] in ['C1', 'C4']:
              dy = 0.12
            else:
                dy = 0.01

            axs[2][0].text(dx * T_TOT, 10 ** (np.log10(ylim2[0]) + dy * np.log10(ylim2[1] / ylim2[0])),
                           r'$m_{\mathrm{DM}} = $' + kwargs['lab'], ha='left', va='bottom', color=kwargs['c'])
            axs[3][0].text(dx * T_TOT, 10 ** (np.log10(ylim3[0]) + dy * np.log10(ylim3[1] / ylim3[0])),
                           r'$\mathrm{N}_{\mathrm{DM}} = $' + kwargs['labn'], ha='left', va='bottom', color=kwargs['c'])

        snap_2gyr = int((2/T_TOT)*snaps)
        snap_4gyr = int((4/T_TOT)*snaps)
        snap_8gyr = int((8/T_TOT)*snaps)

        # y = v_on_sigma[:, _index]
        # y_smooth = savgol_filter(y, window, poly_n)
        # y_theory = theory_best_v_on_sigma[_index, :]
        # 
        # k = kappa_rot[:, _index]
        # k_smooth = savgol_filter(k, window, poly_n)
        # k_theory = theory_best_kappa_rot[_index, :]
        
        print(galaxy_name)
        print('v/s[0]', v_on_sigma[0, _index])
        # print('v/s(t=2)', v_on_sigma[snap_2gyr, _index])
        # print('v/s(t=4)', v_on_sigma[snap_4gyr, _index])
        # print('v/s(t=8)', v_on_sigma[snap_8gyr, _index])
        #
        # print('v/s(t=2)', theory_best_v_on_sigma[_index, snap_2gyr])
        # print('v/s(t=4)', theory_best_v_on_sigma[_index, snap_4gyr])
        # print('v/s(t=8)', theory_best_v_on_sigma[_index, snap_8gyr])
        
        print('v/s(t=2)', v_on_sigma[snap_2gyr, _index]/theory_best_v_on_sigma[_index, snap_2gyr])
        print('v/s(t=4)', v_on_sigma[snap_4gyr, _index]/theory_best_v_on_sigma[_index, snap_4gyr])
        print('v/s(t=8)', v_on_sigma[snap_8gyr, _index]/theory_best_v_on_sigma[_index, snap_8gyr])


        # print('v/s(t=9.8)', v_on_sigma[-1, _index])
        # print('kappa_rot[0]', kappa_rot[0, _index])
        # print('kappa_rot(t=9.8)', kappa_rot[-1, _index])
        # if y_smooth[-1] < 1:
        #     print('raw v/s<1', time[y < 1][0])
        #     print('smooth v/s<1', time[y_smooth < 1][0])
        #     print('theory v/s<1', time[y_theory < 1][0])
        # if k_smooth[-1] < 0.5:
        #     print('raw k<0.5', time[k < 0.5][0])
        #     print('smooth k<0.5', time[k_smooth < 0.5][0])
        # if k_theory[-1] < 0.5:
        #     print('theory k<0.5', time[k_theory < 0.5][0])
        print()

    axs[0][0].legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='-', lw=3)),
                      (lines.Line2D([0, 1], [0, 1], color='grey', ls='--', lw=3))],
                     [r'Simulations', 'Model'],
                     loc='upper right', ncol=1, labelspacing=0.1, borderpad=0.1, frameon=False)

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return


def plot_poster_kappa(galaxy_name_list, snaps_list,
                      save_name_append='diff', name='', save=False):
    _index = 1

    # set up plots
    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(9, 5, forward=True)
    widths = [1]  # [2, 1]
    heights = [1]
    spec = fig.add_gridspec(ncols=1, nrows=1, width_ratios=widths,
                            height_ratios=heights)

    axs = []
    for row in range(len(heights)):
        axs.append([])
        for col in range(len(widths)):
            axs[row].append(fig.add_subplot(spec[row, col]))
            if col == 0:
                axs[row][col].set_xlim([0, T_TOT])
                
    # ylim0 = [1/3, 1]
    ylim0 = [0.12, 1]

    axs[0][0].set_ylim(ylim0)
    axs[0][0].axhline(1, 0.3, 1, c='grey', ls=':')
    axs[0][0].axhline(1, 0.3, 0.5, c='grey', ls=':')
    axs[0][0].axhline(1, 0.3, 0.7, c='grey', ls=':')

    axs[0][0].set_ylabel(r'$\kappa_{\mathrm{rot}}$')
    axs[0][0].set_xlabel(r'$t$ [Gyr]')

    if save_name_append == 'cum':
        pass
    elif save_name_append == 'diff':
        if _index == 0:
            axs[0][0].text(0.2, 0.22, r'$[R_{1/4} \pm 0.1$dex]', ha='left', va='bottom')
        elif _index == 1:
            axs[0][0].text(0.2, 0.55, r'$R = R_{1/2}$', ha='left', va='bottom')
        elif _index == 2:
            axs[0][0].text(0.2, 0.22, r'$[R_{3/4} \pm 0.1$dex]', ha='left', va='bottom')

    # where to measure
    bin_edges = get_bin_edges(save_name_append)

    for galaxy_name, snaps in zip(galaxy_name_list, snaps_list):
        kwargs = get_name_kwargs(galaxy_name)

        bin_edges = get_bin_edges(save_name_append, galaxy_name=galaxy_name)

        # mdm
        mdm = kwargs['mdm'] * 1e10

        # time
        time = np.linspace(0, T_TOT, snaps + 1)

        kappa_rot = get_kappa_rot(galaxy_name, save_name_append, snaps, bin_edges)
        # kappa_co  = get_kappa_co(galaxy_name, save_name_append, snaps, bin_edges)

        # plot
        alpha = 0.2
        lwt = 6
        lw = 3
        window = snaps // 5 + 1
        if window % 2 == 0: window += 1
        poly_n = 5

        # vs time
        y = kappa_rot[:, _index]
        axs[0][0].errorbar(time, y,
                           c=kwargs['c'], marker='', ls='-', lw=lwt, alpha=alpha)

        try:
            axs[0][0].errorbar(time, savgol_filter(y, window, poly_n),
                               c=kwargs['c'], marker='', lw=lw, ls=kwargs['ls'])
        except np.linalg.LinAlgError:
            pass

        # get theory
        (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
         ) = get_physical_from_model(galaxy_name, snaps, save_name_append)

        theory_best_tot2 = theory_best_z2 + theory_best_r2 + theory_best_p2
        
        theory_best_kappa_rot = (theory_best_v ** 2 + theory_best_p2) / (
                theory_best_v ** 2 + theory_best_tot2)

        # vs time
        axs[0][0].errorbar(time, theory_best_kappa_rot[_index, :], c=kwargs['c'], ls='--', lw=3, zorder=5)

        if kwargs['c'] in ['C0', 'C1', 'C2']:
            dx = 0.05
        else:
            dx = 0.35
        if kwargs['c'] == 'C0':
            dy = 0.23
        elif kwargs['c'] in ['C1', 'C3']:
            dy = 0.12
        else:
            dy = 0.01

        axs[0][0].text(dx * T_TOT, ylim0[0] + dy * (ylim0[1] - ylim0[0]),
                       r'$\mathrm{N}_{\mathrm{DM}} = $' + kwargs['labn'], ha='left', va='bottom', color=kwargs['c'])

    axs[0][0].legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='-', lw=3)),
                      (lines.Line2D([0, 1], [0, 1], color='grey', ls='--', lw=3))],
                     [r'Simulations', 'Model'],
                     loc='lower right', ncol=1, labelspacing=0.1, borderpad=0.1, frameon=False)

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return


def plot_poster_vonsigma(galaxy_name_list, snaps_list,
                         save_name_append='diff', name='', save=False):
    _index = 1

    # set up plots
    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(9, 5, forward=True)
    widths = [1]  # [2, 1]
    heights = [1]
    spec = fig.add_gridspec(ncols=1, nrows=1, width_ratios=widths,
                            height_ratios=heights)

    axs = []
    for row in range(len(heights)):
        axs.append([])
        for col in range(len(widths)):
            axs[row].append(fig.add_subplot(spec[row, col]))
            if col == 0:
                axs[row][col].set_xlim([0, T_TOT])
                axs[row][col].semilogy()

    # ylim0 = [1/3, 1]
    ylim0 = [0.3, 40]

    axs[0][0].set_ylim(ylim0)
    axs[0][0].axhline(1, 0, 1, c='grey', ls=':')

    axs[0][0].set_ylabel(r'$\overline{v}_\phi / \sigma_{1\mathrm{D}}$')
    axs[0][0].set_xlabel(r'$t$ [Gyr]')

    if save_name_append == 'cum':
        pass
    elif save_name_append == 'diff':
        if _index == 0:
            axs[0][0].text(0.2, 0.22, r'$[R_{1/4} \pm 0.1$dex]', ha='left', va='bottom')
        elif _index == 1:
            axs[0][0].text(0.2, 20, r'$R = R_{1/2}$', ha='left', va='bottom')
        elif _index == 2:
            axs[0][0].text(0.2, 0.22, r'$[R_{3/4} \pm 0.1$dex]', ha='left', va='bottom')

    # where to measure
    bin_edges = get_bin_edges(save_name_append)

    for galaxy_name, snaps in zip(galaxy_name_list, snaps_list):
        kwargs = get_name_kwargs(galaxy_name)

        bin_edges = get_bin_edges(save_name_append, galaxy_name=galaxy_name)

        # mdm
        mdm = kwargs['mdm'] * 1e10

        # time
        time = np.linspace(0, T_TOT, snaps + 1)

        # load data
        (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
         ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges=bin_edges)

        sigma_tot2 = sigma_z ** 2 + sigma_R ** 2 + sigma_phi ** 2
        v_on_sigma = mean_v_phi / np.sqrt(sigma_tot2 / 3)

        # plot
        alpha = 0.2
        lwt = 6
        lw = 3
        window = snaps // 5 + 1
        if window % 2 == 0: window += 1
        poly_n = 5

        # vs time
        y = v_on_sigma[:, _index]
        axs[0][0].errorbar(time, y,
                           c=kwargs['c'], marker='', ls='-', lw=lwt, alpha=alpha)
        try:
            axs[0][0].errorbar(time, savgol_filter(y, window, poly_n),
                               c=kwargs['c'], marker='', lw=lw, ls=kwargs['ls'])
        except np.linalg.LinAlgError:
            pass

        # get theory
        (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
         ) = get_physical_from_model(galaxy_name, snaps, save_name_append)

        theory_best_tot2 = theory_best_z2 + theory_best_r2 + theory_best_p2
        theory_best_v_on_sigma = theory_best_v / np.sqrt(theory_best_tot2 / 3)

        # vs time
        axs[0][0].errorbar(time, theory_best_v_on_sigma[_index, :], c=kwargs['c'], ls='--', lw=3, zorder=5)

        if kwargs['c'] in ['C0', 'C1']:
            dx = 0.43
        else:
            dx = 0.73
        if kwargs['c'] in ['C0', 'C2']:
            dy = 0.90
        elif kwargs['c'] in ['C1', 'C3']:
            dy = 0.81
        else:
            dy = 0.72

        axs[0][0].text(dx * T_TOT, 10 ** (np.log10(ylim0[0]) + dy * np.log10(ylim0[1] / ylim0[0])),
                       r'$\mathrm{N}_{\mathrm{DM}} = $' + kwargs['labn'], ha='left', va='bottom', color=kwargs['c'])

    axs[0][0].legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='-', lw=3)),
                      (lines.Line2D([0, 1], [0, 1], color='grey', ls='--', lw=3))],
                     [r'Simulations', 'Model'],
                     loc='lower left', ncol=1, labelspacing=0.1, borderpad=0.1, frameon=False)

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return


def talk_plot_modelled_indicators(galaxy_name_list, snaps_list,
                                  save_name_append='diff', name='', save=False):
    _index = 1

    # set up plots
    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(8, 4, forward=True)
    widths = [1]  # [2, 1]
    heights = [1]
    spec = fig.add_gridspec(ncols=1, nrows=1, width_ratios=widths, height_ratios=heights)

    xlim1 = [8e5, 1.2e8]

    axs = []
    for row in range(len(heights)):
        axs.append([])
        for col in range(len(widths)):
            axs[row].append(fig.add_subplot(spec[row, col]))
            if col == 0:
                axs[row][col].semilogy()
                # axs[row][col].set_xlim([0, T_TOT * (101 - BURN_IN) / 101])
                axs[row][col].set_xlim([0, T_TOT])

    ylim0 = [0.1, 30]

    for col in range(len(widths)):
        axs[0][col].set_ylim(ylim0)

        axs[0][col].axhline(1, 0, 1, c='grey', ls=':')

    # axs[0][0].set_ylabel(r'$\overline{v}_\phi / \sigma_{1\mathrm{D}}$')
    axs[0][0].set_ylabel(r'$v / \sigma$')
    axs[0][0].set_xlabel(r'$t$ [Gyr]')

    # axs[0][0].text(0.2, 0.22, r'$R = R_{1/2}$', ha='left', va='bottom')

    # where to measure
    bin_edges = get_bin_edges(save_name_append)

    for galaxy_name, snaps in zip(galaxy_name_list, snaps_list):
        kwargs = get_name_kwargs(galaxy_name)

        bin_edges = get_bin_edges(save_name_append, galaxy_name=galaxy_name)

        # mdm
        mdm = kwargs['mdm'] * 1e10

        # time
        time = np.linspace(0, T_TOT, snaps + 1)
        # time = time - time[BURN_IN]

        # load data
        (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
         ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges=bin_edges)

        sigma_tot2 = sigma_z ** 2 + sigma_R ** 2 + sigma_phi ** 2
        v_on_sigma = mean_v_phi / np.sqrt(sigma_tot2 / 3)

        kappa_rot = get_kappa_rot(galaxy_name, save_name_append, snaps, bin_edges)
        # kappa_co  = get_kappa_co(galaxy_name, save_name_append, snaps, bin_edges)

        # sqrt(3) is not correct but makes lambda_R better
        # lambda_r = mean_v_phi / np.sqrt(mean_v_phi**2 + sigma_tot2/np.sqrt(3))
        lambda_r = mean_v_phi / np.sqrt(mean_v_phi ** 2 + sigma_tot2 / 3)

        # plot
        alpha = 0.2
        lwt = 5
        lw = 3
        window = snaps // 5 + 1
        if window % 2 == 0: window += 1
        poly_n = 5
        # vs time
        y = v_on_sigma[:, _index]
        axs[0][0].errorbar(time, y,
                           c=kwargs['c'], marker='', ls='-', lw=lwt, alpha=alpha)
        axs[0][0].errorbar(time, savgol_filter(y, window, poly_n),
                               c=kwargs['c'], marker='', lw=lw, ls=kwargs['ls'])
        if True:  # kwargs['marker'] == '^':
            ####
            # get theory
            (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
             ) = get_physical_from_model(galaxy_name, snaps, save_name_append)

            theory_best_tot2 = theory_best_z2 + theory_best_r2 + theory_best_p2
            theory_best_v_on_sigma = theory_best_v / np.sqrt(theory_best_tot2 / 3)

            theory_best_kappa_rot = (theory_best_v ** 2 + theory_best_p2) / (
                    theory_best_v ** 2 + theory_best_tot2)

            theory_lambda_r = theory_best_v / np.sqrt(theory_best_v ** 2 + theory_best_tot2 / 3)

            # vs time
            axs[0][0].errorbar(time, theory_best_v_on_sigma[_index, :], c=kwargs['c'], ls='--', lw=2, zorder=5)

            if kwargs['c'] in ['C0', 'C1', 'C2']:
                dx = 0.01
            else:
                dx = 0.30
            if kwargs['c'] in ['C0']:
                dy = 0.21
            elif kwargs['c'] in ['C1', 'C3']:
                dy = 0.11
            else:
                dy = 0.01

            axs[0][0].text(dx * T_TOT, 10 ** (np.log10(ylim0[0]) + dy * np.log10(ylim0[1] / ylim0[0])),
                           r'$\mathrm{N}_{\mathrm{DM}} = $' + kwargs['labn'], ha='left', va='bottom', color=kwargs['c'])

        snap_8gyr = int((8 / T_TOT) * snaps)

        y = v_on_sigma[:, _index]
        y_smooth = savgol_filter(y, window, poly_n)
        y_theory = theory_best_v_on_sigma[_index, :]

        k = kappa_rot[:, _index]
        k_smooth = savgol_filter(k, window, poly_n)
        k_theory = theory_best_kappa_rot[_index, :]

    axs[0][0].legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='-')),
                      (lines.Line2D([0, 1], [0, 1], color='grey', ls='--'))],
                     [r'Simulations', 'Model'],
                     loc='upper right', ncol=2, labelspacing=0.1, borderpad=0.1,
                     frameon=False)

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return


def plot_angular_evolution(galaxy_name_list, snaps_list,
                             save_name_append='diff', name='', save=False):

    # set up plots
    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(12, 4, forward=True)
    widths = [1, 1, 1]
    heights = [1]
    spec = fig.add_gridspec(ncols=3, nrows=1, width_ratios=widths,
                            height_ratios=heights)

    xlim0 = [0, T_TOT]
    ylim0 = [0, 40]

    axs = []
    for row in range(len(heights)):
        axs.append([])
        for col in range(len(widths)):
            axs[row].append(fig.add_subplot(spec[row, col]))

            axs[row][col].set_ylim(ylim0)
            axs[row][col].set_xlim(xlim0)

            if col != 0: axs[row][col].set_yticklabels([])
            if row != len(heights) - 1: axs[row][col].set_xticklabels([])

    # axs[0][0].set_ylabel(r'RMS$\theta$')
    axs[0][0].set_ylabel(r'$\sigma_\theta$ [deg]')
    
    axs[0][1].set_xlabel(r'$t$ [Gyr]')

    fig.subplots_adjust(hspace=0.01, wspace=0.01)

    # where to measure
    bin_edges = get_bin_edges(save_name_append)

    for galaxy_name, snaps in zip(galaxy_name_list, snaps_list):
        kwargs = get_name_kwargs(galaxy_name)

        bin_edges = get_bin_edges(save_name_append, galaxy_name=galaxy_name)

        # mdm
        mdm = kwargs['mdm'] * 1e10

        # time
        time = np.linspace(0, T_TOT, snaps + 1)
        # time = time - time[BURN_IN]

        # load data
        (rms_theta, sigma_theta
         ) = get_angular_deviation(galaxy_name, save_name_append, snaps, bin_edges=bin_edges)

        # plot
        alpha = 0.2
        lwt = 6
        lw = 3
        window = snaps // 5 + 1
        if window % 2 == 0: window += 1
        poly_n = 5
        # vs time

        for j in range(len(widths)):
            y = rms_theta[:, j] * 180 / np.pi
            axs[0][j].errorbar(time, y,
                               c=kwargs['c'], marker='', ls='-', lw=lwt, alpha=alpha)
            axs[0][j].errorbar(time, savgol_filter(y, window, poly_n),
                               c=kwargs['c'], marker='', lw=lw, ls=kwargs['ls'])

            # y = sigma_theta[:, j] * 180 / np.pi
            # axs[0][j].errorbar(time, y,
            #                    c=kwargs['c'], marker='', ls='-', lw=lwt, alpha=alpha)
            # axs[0][j].errorbar(time, savgol_filter(y, window, poly_n),
            #                    c=kwargs['c'], marker='', lw=lw, ls=kwargs['ls'])

        #theory
        ncx2_pdf = lambda x, args: (0.5 * np.exp(- (args[1] + x) / 2) * (x / args[1]) ** ((args[0] - 2) / 4) *
                                    iv((args[0] - 2) / 2, np.sqrt(args[1] * x)))
        mean_ncx2 = lambda x, args: np.sqrt(x) * ncx2_pdf(x, args)
        two_point_something = quad(mean_ncx2, 0, 30 * (3 + 2), args=[3, 2])[0] / np.sqrt(2)

        ln_Lambda = 10
        
        a = get_hernquist_params(galaxy_name)[0]
        bin_centres = 10 ** ((np.log10(bin_edges[:-1:2]) + np.log10(bin_edges[1::2])) / 2)
        
        fake_c = get_r_200(galaxy_name) / a #r200 / a

        N200 = 1.85 * 10**2 / kwargs['mdm']

        rho_tilde = a / bin_centres
        # rho_tilde = 1 / ((bin_centres / a) * (1 + (bin_centres / a))**3)

        R_d = 4.15

        fudge = 0.1
        # fudge = 0.2

        # const = (fudge * 2**3 / two_point_something ** 4 * 10 * HUBBLE_CONST * fake_c ** 3 * ln_Lambda *
        #          rho_tilde / N200 * 180 ** 2 / np.pi ** 2 * PC_ON_M / GYR_ON_S)

        # const = (0.8**2 * rho_tilde * 1e6 / N200)

        # print(const / rho_tilde)
        # print(np.sqrt(const / rho_tilde))
        # print()
    
        scale = 2.5**2
        
        const = scale * R_d / bin_centres * 1e6 / N200

        time_0 = (sigma_theta[0, :] * 180 / np.pi)**2 / const

        d_theta2 = const[np.newaxis, :] * (time_0[np.newaxis, :] + time[:, np.newaxis])
        d_theta = np.sqrt(d_theta2)
        
        for j in range(len(widths)):
            axs[0][j].errorbar(time, d_theta[:,j],
                               c=kwargs['c'], marker='', ls='--', lw=lw)
            # axs[1][j].errorbar(time, d_theta[:,j],
            #                    c=kwargs['c'], marker='', ls='--', lw=lw)
            
            
            # # lin_const = 0.2 * np.sqrt(rho_tilde[j] * 1e6 / N200)
            # lin_const = 0.1 * rho_tilde[j] * (1e6 / N200)**0.67
            # # lin_const = 0.1 * rho_tilde[j] * 1e6 / N200
            # time_0 = sigma_theta[0, j] * 180 / np.pi / lin_const
            # 
            # lin_model = lin_const * (time + time_0)
            # 
            # axs[0][j].errorbar(time, lin_model,
            #                    c=kwargs['c'], marker='', ls=':', lw=lw)

    axs[0][0].legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='-', lw=3)),
                      (lines.Line2D([0, 1], [0, 1], color='grey', ls='--', lw=3))],
                     [r'Simulations', 'Model'],
                     loc='upper right', ncol=1, labelspacing=0.1, borderpad=0.1, frameon=False)

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return


def plot_profile_dep_indicators(galaxy_name_list, snaps_list,
                                save_name_append='diff', name='', save=False):
    _index = 1

    j_zonc_cuts = [0.5, 0.7, 0.8, 0.9]
    j_zonc_lss = [':', '-', '-.', '--']

    # where to measure
    j_zonc_edges = np.linspace(-1.2, 1.2, 4 * 24 + 1)
    j_zonc_centres = 0.5 * (j_zonc_edges[1:] + j_zonc_edges[:-1])

    # set up plots
    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(9, 9, forward=True)
    widths = [1]
    heights = [1, 1]
    spec = fig.add_gridspec(ncols=1, nrows=2, width_ratios=widths,
                            height_ratios=heights)

    ylim0 = [-0.20, 1.05]
    ylim1 = [-0.05, 1.20]

    axs = []
    for row in range(len(heights)):
        axs.append([])
        for col in range(len(widths)):
            axs[row].append(fig.add_subplot(spec[row, col]))
            # if col == 0: axs[row][col].set_xlim([0, T_TOT * (101 - BURN_IN) / 101])
            if col == 0: axs[row][col].set_xlim([0, T_TOT])
            if col == 1:
                axs[row][col].set_xlim([0.15, 20])
                axs[row][col].semilogx()
            # if col == 2:
            #   axs[row][col].set_xlim([1/3, 1])
            # if col != 0: axs[row][col].set_yticklabels([])
            if row != len(heights) - 1: axs[row][col].set_xticklabels([])

            axs[row][col].set_yticks([0,0.25,0.5,0.75,1])

    for col in range(len(widths)):
        axs[0][col].set_ylim(ylim0)
        axs[1][col].set_ylim(ylim1)

        axs[0][col].axhline(0.5, 0, 1, c='grey', ls=':')
        axs[1][col].axhline(0.5, 0, 1, c='grey', ls=':')

    axs[0][0].axhline(0, 0.65, 1, c='grey', ls=':')
    axs[1][0].axhline(1, 0.65, 1, c='grey', ls=':')

    # axs[0][0].set_ylabel(r'$f(j_z / j_c(E) > 0.7)$')
    axs[0][0].set_ylabel(r'D/T')
    axs[1][0].set_ylabel(r'S/T')
    axs[1][0].set_xlabel(r'$t$ [Gyr]')

    if save_name_append == 'cum':
      pass
    elif save_name_append == 'diff':
        if _index == 1:
            axs[0][0].text(T_TOT * 0.98,  ylim0[0] + 0.03 * (ylim0[1] - ylim0[0]),
                           r'$R = R_{1/2, 0}$', ha='right', va='bottom')

    axs[1][0].text(T_TOT * 0.98,  ylim1[0] + 0.97 * (ylim1[1] - ylim1[0]),
                   r'$V_{200} = 200$ km/s', ha='right', va='top')

    fig.subplots_adjust(hspace=0.03, wspace=0.01)

    fit_name_list = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                     'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                     'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                     'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
                     'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps']
    fit_snaps_list = [400, 400, 101, 101, 101]

    # (F_0, F_inf, n_12, log_tau_12
    #  ) = find_best_fit_j_zonc_frac(fit_name_list, fit_snaps_list, save_name_append, _index, j_zonc_frac_cut)

    for galaxy_name, snaps in zip(galaxy_name_list, snaps_list):
        kwargs = get_name_kwargs(galaxy_name)

        bin_edges = get_bin_edges(save_name_append, galaxy_name=galaxy_name)

        # time
        time = np.linspace(0, T_TOT, snaps + 1)
        # time = time - time[BURN_IN]

        # load data
        (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
         ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges=bin_edges)

        sigma_tot2 = sigma_z ** 2 + sigma_R ** 2 + sigma_phi ** 2
        # v_on_sigma = mean_v_phi / np.sqrt(sigma_tot2 / 3)
        v_on_sigma = mean_v_phi / np.sqrt(sigma_tot2 / 3)

        bulge_to_total = get_bulge_to_total(galaxy_name, save_name_append, snaps, bin_edges)  # , overwrite=True)

        j_zoncs = get_j_zonc_evolution(galaxy_name, save_name_append, snaps, bin_edges,
                                       j_zonc_edges)  # , overwrite=True)

        # plot
        alpha = 0.2
        lwt = 6
        lw = 3
        window = snaps // 5 + 1
        poly_n = 5
        # vs time

        # for j_zonc_frac_cut, jzls in zip(j_zonc_cuts, j_zonc_lss):
        #     if j_zonc_frac_cut == 0.7:# or kwargs['c'] == 'C1':

        j_zonc_frac_cut = 0.7
        j_zonc_mask = j_zonc_centres >= j_zonc_frac_cut
        frac_j_zoncs = np.sum(j_zoncs[:, _index, j_zonc_mask], axis=1) / np.sum(j_zoncs[:, _index, :], axis=1)

        y = frac_j_zoncs
        axs[0][0].errorbar(time, y,
                           c=kwargs['c'], marker='', ls='-', lw=lwt, alpha=alpha)
        axs[0][0].errorbar(time, savgol_filter(y, window, poly_n),
                               c=kwargs['c'], marker='', lw=lw, ls='-')

        y = bulge_to_total[:, _index]
        axs[1][0].errorbar(time, y,
                           c=kwargs['c'], marker='', ls='-', lw=lwt, alpha=alpha)
        axs[1][0].errorbar(time, savgol_filter(y, window, poly_n),
                               c=kwargs['c'], marker='', lw=lw, ls='-')

        # if kwargs['marker'] == 'o':
        ####
        # theory attempts
        (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
         ) = get_physical_from_model(galaxy_name, snaps, save_name_append)

        # (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
        #  ) = get_constants(bin_edges, save_name_append, kwargs['mdm'], galaxy_name=galaxy_name)

        ####
        analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0,
                                             analytic_v_c=True, save_name_append='diff')
        frac = j_zonc_frac_cut #-0.1

        # theory_disk_to_total = np.array([1/2 + erf((v - frac * analytic_v_circ[_index]) / (p*np.sqrt(2)))/2
        #                           for (v, p) in zip(mean_v_phi[:, _index], sigma_phi[:, _index])])
        # axs[0][0].errorbar(time, theory_disk_to_total,
        #                    c=kwargs['c'], ls='--', lw=3, zorder=10)
        theory_disk_to_total = np.array([1/2 + erf((v - frac * analytic_v_circ[_index]) / (np.sqrt(p2*2)))/2
                                  for (v, p2) in zip(theory_best_v[_index, :], theory_best_p2[_index, :])])
        axs[0][0].errorbar(time, theory_disk_to_total,
                           c=kwargs['c'], ls='--', lw=3, zorder=10)
        
        theory_bulge_to_total = np.array([1 + erf(-v / (np.sqrt(p2*2)))
                                  for (v, p2) in zip(theory_best_v[_index, :], theory_best_p2[_index, :])])

        axs[1][0].errorbar(time, theory_bulge_to_total,
                           c=kwargs['c'], ls='--', lw=3, zorder=10)

        #f(jz/jc(E)) model
        # theory_bulge_to_total = np.array([2 * scipy.stats.norm.cdf(0, loc=v, scale=np.sqrt(p2))
        #                                   for (v, p2) in zip(theory_best_v[_index, :], theory_best_p2[_index, :])])

        # tau_edges = np.linspace(0, T_TOT, snaps + 2) / t_c_dm[_index]
        # log_tau_centres = np.log10(0.5 * (tau_edges[1:] + tau_edges[:-1]))
        # frac_model = F_inf + (F_0 - F_inf) * 0.5 * (1 + erf(n_12 * (log_tau_centres - log_tau_12)))
        # vs time
        # axs[0][0].errorbar(time, frac_model,
        #                    c=kwargs['c'], ls='--', zorder=10)


        if kwargs['ls'] == '-':
            if kwargs['c'] in ['C0', 'C1', 'C2']:
                dx = 0.02
            else:
                dx = 0.34
            if kwargs['c'] in ['C0', 'C3']:
                dy = 0.23
            elif kwargs['c'] in ['C1', 'C4']:
                dy = 0.13
            else:
                dy = 0.03

            axs[0][0].text(dx * T_TOT, ylim0[0] + (dy - (dx > 0.3) * 0.12) * (ylim0[1] - ylim0[0]),
                           r'$m_{\mathrm{DM}} = $' + kwargs['lab'], ha='left', va='bottom', color=kwargs['c'])
            axs[1][0].text(dx * T_TOT, ylim1[0] + (dy + (1-0.23-0.02)) * (ylim1[1] - ylim1[0]),
                           r'$\mathrm{N}_{\mathrm{DM}} = $' + kwargs['labn'], ha='left', va='top', color=kwargs['c'])

        y = bulge_to_total[:, _index]
        y_smooth = savgol_filter(y, window, poly_n)
        y_theory = theory_bulge_to_total

        print(galaxy_name)
        if y_smooth[-2] > 0.5:
            print('raw S/T>0.5', time[y > 0.5][0])
            print('smooth S/T>0.5', time[y_smooth > 0.5][0])
            print('theory S/T>0.5', time[y_theory > 0.5][0])

        j_zonc_mask = j_zonc_centres >= 0.7
        frac_j_zoncs = np.sum(j_zoncs[:, _index, j_zonc_mask], axis=1) / np.sum(j_zoncs[:, _index, :], axis=1)
        print('f(jz / jc(E) > 0.7, t=9.8) ', frac_j_zoncs[-2])
        # print('med(jz / jc(E)) t = 9.8 ', )
        j_zonc_mask = j_zonc_centres >= -0.7
        frac_j_zoncs = np.sum(j_zoncs[:, _index, j_zonc_mask], axis=1) / np.sum(j_zoncs[:, _index, :], axis=1)
        print('1 - f(jz / jc(E) < -0.7, t=9.8) ', 1 - frac_j_zoncs[-2])
        print()

    axs[1][0].legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='-', lw=3)),
                      (lines.Line2D([0, 1], [0, 1], color='grey', ls='--', lw=3))],
                     ['Simulations', 'Model'],
                     loc='center left', handletextpad=0.2, frameon=False,
                     labelspacing=0.4, borderpad=0.2, bbox_to_anchor=(0, 0.55))

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return


def plot_spin_parameter_comparison(galaxy_name_list, snaps_list,
                                   save_name_append='diff', name='', save=False):
    _index = 1

    # set up plots
    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(4, 4, forward=True)
    ax = plt.subplot(1, 1, 1)
    ax.set_aspect('equal')

    plt.xlim([0.167, 1.01])
    plt.ylim([0.167, 1.01])

    ax.set_xticks([0.2, 0.4, 0.6, 0.8, 1])

    plt.ylabel(r'$\lambda_R$')
    plt.xlabel(r'$\lambda_r$')

    R_measure = get_bin_edges(save_name_append='cum')[2 * _index + 1]
    # load galaxys properties
    a_h, vmax = get_hernquist_params()
    sigma_dm = get_analytic_hernquist_dispersion([R_measure, R_measure], vmax, a_h, save_name_append='diff')[0]
    v_c_halo = get_analytic_hernquist_circular_velocity([R_measure, R_measure], vmax, a_h, save_name_append='diff')[0]
    v200 = get_v_200()
    r200 = get_r_200()
    M_disk, R_disk = get_exponential_disk_params()
    Sigma_R_star = get_exponential_surface_density(R_measure, M_disk, R_disk, save_name_append='diff')
    z_with_sigma_max = get_disk_height(sigma_dm, R_measure, Sigma_R_star, v_c_halo, v200, r200, a_h)

    bin_edges = get_bin_edges(save_name_append)

    analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0,
                                         v200=False, dm_dispersion=False, dm_v_circ=False,
                                         analytic_dispersion=False, analytic_v_c=True,
                                         save_name_append=save_name_append)

    if save_name_append == 'cum':
        pass
    elif save_name_append == 'diff':
        if _index == 0:
            plt.text(0.98, 0.20, r'$[R_{1/4} \pm 0.1$dex]', ha='right', va='bottom')
        elif _index == 1:
            plt.text(0.98, 0.20, r'$[R_{1/2} \pm 0.1$dex]', ha='right', va='bottom')
        elif _index == 2:
            plt.text(0.98, 0.20, r'$[R_{3/4} \pm 0.1$dex]', ha='right', va='bottom')

    for galaxy_name, snaps in zip(galaxy_name_list, snaps_list):
        kwargs = get_name_kwargs(galaxy_name)

        # for lambda_R
        not_low_res = True
        if kwargs['marker'] == 's' and kwargs['c'] == 'C0':
            not_low_res = False
        if kwargs['marker'] == 's' and kwargs['c'] == 'C1':
            not_low_res = False
        if kwargs['marker'] == 'o' and kwargs['c'] == 'C0':
            not_low_res = False

        # load data
        (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
         ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges=bin_edges)

        sigma_tot2 = sigma_z ** 2 + sigma_R ** 2 + sigma_phi ** 2

        lambda_r = mean_v_phi / np.sqrt(mean_v_phi ** 2 + sigma_tot2 / np.sqrt(3))

        lambda_R = get_lambda_R(galaxy_name, save_name_append, snaps, bin_edges)

        # lambda_dm = get_lambda_dm(galaxy_name, save_name_append, snaps, bin_edges)
        #
        # lambda_bulloc = mean_v_phi / analytic_v_circ / np.sqrt(2) #*2

        # plot
        alpha = 0.2
        lw = 4
        window = snaps // 5 + 1
        poly_n = 5

        if not_low_res:
            x = lambda_r[:, _index]
            y = lambda_R[:, _index]
            plt.errorbar(x, y,
                         c=kwargs['c'], marker='', ls='-', lw=lw, alpha=alpha)
            plt.errorbar(savgol_filter(x, window, poly_n), savgol_filter(y, window, poly_n),
                         c=kwargs['c'], marker='', ls=kwargs['ls'])

        if kwargs['ls'] == '-' and kwargs['c'] == 'C0':
            ####
            (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
             ) = get_physical_from_model(galaxy_name, snaps, save_name_append)

            time = np.linspace(0, T_TOT, snaps + 1)
            time = time - time[BURN_IN]

            theory_best_tot2 = theory_best_z2 + theory_best_r2 + theory_best_p2

            theory_best_simga_disk = np.sqrt((theory_best_r2 + theory_best_p2) / 2)

            theory_lambda_R = [edge_on_razor_thin_ring_lambda_r(theory_best_v[_index, k],
                                                                theory_best_simga_disk[_index, k],
                                                                bin_edges[2 * _index], bin_edges[2 * _index + 1], 0.1)
                               for k in range(len(time))]

            theory_lambda_r = theory_best_v / np.sqrt(theory_best_v ** 2 + theory_best_tot2 / np.sqrt(3))

            plt.errorbar(theory_lambda_r[_index, :], theory_lambda_R,
                         c='k', ls='--', zorder=10)

    plt.errorbar([0, 1], [0, 1], ls=':', c='grey')

    ax.legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-'),
                lines.Line2D([0, 1], [0, 1], color='C1', ls='-'),
                lines.Line2D([0, 1], [0, 1], color='C2', ls='-'),
                lines.Line2D([0, 1], [0, 1], color='C3', ls='-'),
                lines.Line2D([0, 1], [0, 1], color='C4', ls='-')),
               (lines.Line2D([0, 1], [0, 1], color='white', ls='--'),
                lines.Line2D([0, 1], [0, 1], color='white', ls='--'),
                lines.Line2D([0, 1], [0, 1], color='k', ls='--'),
                lines.Line2D([0, 1], [0, 1], color='white', ls='--'),
                lines.Line2D([0, 1], [0, 1], color='white', ls='--')),
               (lines.Line2D([0, 1], [0, 1], color='white', ls=':'),
                lines.Line2D([0, 1], [0, 1], color='white', ls=':'),
                lines.Line2D([0, 1], [0, 1], color='grey', ls=':'),
                lines.Line2D([0, 1], [0, 1], color='white', ls=':'),
                lines.Line2D([0, 1], [0, 1], color='white', ls=':'))],
              [r'Measured', r'Theory', r'$1:1$'],
              handler_map={tuple: HandlerTupleVertical(ndivide=5)},
              loc='upper left', numpoints=1, handlelength=1, handletextpad=0.2,
              labelspacing=0, borderpad=0.2)

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return


def plot_kappa_comparison(galaxy_name_list, snaps_list,
                          save_name_append='diff', name='', save=False):
    _index = 1

    # set up plots
    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(4, 4, forward=True)
    ax = plt.subplot(1, 1, 1)
    # ax.set_aspect('equal')

    ax.axhline(0, 0, 1, c='k')

    plt.xlim([0.167, 1.01])
    plt.ylim([0.167, 1.01])

    # ax.set_xticks([0.2, 0.4, 0.6, 0.8, 1])

    plt.ylabel(r'$\kappa_{co}$')
    plt.xlabel(r'$\kappa_{rot}$')

    if save_name_append == 'cum':
        pass
    elif save_name_append == 'diff':
        if _index == 0:
            plt.text(0.98, 0.20, r'$[R_{1/4} \pm 0.1$dex]', ha='right', va='bottom')
        elif _index == 1:
            plt.text(0.98, 0.20, r'$[R_{1/2} \pm 0.1$dex]', ha='right', va='bottom')
        elif _index == 2:
            plt.text(0.98, 0.20, r'$[R_{3/4} \pm 0.1$dex]', ha='right', va='bottom')

    bin_edges = get_bin_edges(save_name_append)

    for galaxy_name, snaps in zip(galaxy_name_list, snaps_list):
        kwargs = get_name_kwargs(galaxy_name)

        # load data
        (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
         ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges=bin_edges)

        kappa_rot = get_kappa_rot(galaxy_name, save_name_append, snaps, bin_edges)
        kappa_co = get_kappa_co(galaxy_name, save_name_append, snaps, bin_edges)

        # for i in range(len(kappa_rot[:,0])):
        #     print(kappa_rot[i, _index] - kappa_co[i, _index], kappa_rot[i, _index])

        # plot
        alpha = 0.2
        lw = 4
        window = snaps // 5 + 1
        poly_n = 5

        x = kappa_rot[:, _index]
        y = (kappa_co[:, _index] - kappa_rot[:, _index]) / kappa_rot[:, _index]
        plt.errorbar(x, y,
                     c=kwargs['c'], marker='', ls='-', lw=lw, alpha=alpha)
        plt.errorbar(savgol_filter(x, window, poly_n), savgol_filter(y, window, poly_n),
                     c=kwargs['c'], marker='', ls=kwargs['ls'])

        if kwargs['ls'] == '-' and kwargs['c'] == 'C0':
            ####
            (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
             ) = get_physical_from_model(galaxy_name, snaps, save_name_append)

            time = np.linspace(0, T_TOT, snaps + 1)
            time = time - time[BURN_IN]

            theory_best_tot2 = theory_best_z2 + theory_best_r2 + theory_best_p2

            theory_best_kappa_rot = (theory_best_v ** 2 + theory_best_p2) / (
                    theory_best_v ** 2 + theory_best_tot2)

            rms2_positive_v_phi = np.zeros(snaps + 1)

            v_max = 1000
            v_min = 0  # -1000
            for i in range(snaps + 1):
                v = theory_best_v[_index, i]
                s = np.sqrt(theory_best_p2[_index, i])

                v2_int = lambda x: x ** 2 * scipy.stats.norm.pdf(x, loc=v, scale=s)
                v2_out = quad(v2_int, v_min, v_max)[0]

                # m_frac_out = 1-scipy.stats.norm.cdf(0, loc=v, scale=s)

                area_out = 1
                rms2_positive_v_phi[i] = v2_out / area_out  # * m_frac_out

            theory_best_kappa_co = rms2_positive_v_phi / (theory_best_v ** 2 + theory_best_tot2)

            plt.errorbar(theory_best_kappa_rot[_index, :], theory_best_kappa_co[_index, :],
                         c='k', ls='--', zorder=10)

    plt.errorbar([0, 1], [0, 1], ls=':', c='grey')

    ax.legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-'),
                lines.Line2D([0, 1], [0, 1], color='C1', ls='-'),
                lines.Line2D([0, 1], [0, 1], color='C2', ls='-'),
                lines.Line2D([0, 1], [0, 1], color='C3', ls='-'),
                lines.Line2D([0, 1], [0, 1], color='C4', ls='-')),
               (lines.Line2D([0, 1], [0, 1], color='white', ls='--'),
                lines.Line2D([0, 1], [0, 1], color='white', ls='--'),
                lines.Line2D([0, 1], [0, 1], color='k', ls='--'),
                lines.Line2D([0, 1], [0, 1], color='white', ls='--'),
                lines.Line2D([0, 1], [0, 1], color='white', ls='--')),
               (lines.Line2D([0, 1], [0, 1], color='white', ls=':'),
                lines.Line2D([0, 1], [0, 1], color='white', ls=':'),
                lines.Line2D([0, 1], [0, 1], color='grey', ls=':'),
                lines.Line2D([0, 1], [0, 1], color='white', ls=':'),
                lines.Line2D([0, 1], [0, 1], color='white', ls=':'))],
              [r'Measured', r'Theory', r'$1:1$'],
              handler_map={tuple: HandlerTupleVertical(ndivide=5)},
              loc='upper left', numpoints=1, handlelength=1, handletextpad=0.2,
              labelspacing=0, borderpad=0.2)

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return


def find_best_fit_j_zonc_frac(galaxy_name_list, snaps_list, save_name_append='diff',
                              _index=1, j_zonc_frac_cut=0.7):
    # set up data
    j_zonc_edges = np.linspace(-1.2, 1.2, 4 * 24 + 1)
    j_zonc_centres = 0.5 * (j_zonc_edges[1:] + j_zonc_edges[:-1])

    j_zonc_mask = j_zonc_centres >= j_zonc_frac_cut

    log_tau_array = []
    frac_array = []

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = get_name_kwargs(galaxy_name)

        bin_edges = get_bin_edges(save_name_append, galaxy_name=galaxy_name)

        # work out constatns
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, kwargs['mdm'], galaxy_name=galaxy_name)

        tau_edges = np.linspace(0, T_TOT, snaps + 2) / t_c_dm[_index]
        log_tau_centres = np.log10(0.5 * (tau_edges[1:] + tau_edges[:-1]))

        log_tau_array.append(log_tau_centres)

        j_zoncs = get_j_zonc_evolution(galaxy_name, save_name_append, snaps, bin_edges,
                                       j_zonc_edges, overwrite=False)

        frac = np.sum(j_zoncs[:, _index, j_zonc_mask], axis=1) / np.sum(j_zoncs[:, _index, :], axis=1)

        frac_array.append(frac)

    log_tau_array = np.concatenate(log_tau_array)
    frac_array = np.concatenate(frac_array)

    # F_0 = 1, F_inf = 0.1, n_12 = -2, log_tau_12 = -1.7
    # frac_model = (F_0 - F_inf) * 0.5 * ( 1 + np.arctan( n_12 * (log_tau_centres - log_tau_12) ))

    model = lambda params: np.sum((params[1] + (params[0] - params[1]) * 0.5 *
                                   (1 + erf(params[2] * (log_tau_array - params[3])))
                                   - frac_array) ** 2)

    def model_and_priors(params):
        if j_zonc_frac_cut > 0:
            if params[0] <= 0 or params[0] >= 1:
                return np.infty
            if params[1] <= 0 or params[1] >= 1:
                return np.infty

        return model(params)

    out = minimize(model, [0.5, 0.1, -2, -2])
    # out = minimize(model_and_priors, out.x)

    return out.x  # (F_0, F_inf, log_tau_12, n_12)


def plot_jzonc_distributions(save_name_append='all', name='', save=False):
    galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps']
    snaps_list = [400, 400, 101, 101, 101]

    bin_edges = get_bin_edges(save_name_append)

    _index = 1

    j_zonc_edges = np.linspace(-1.2, 1.2, 4 * 24 + 1)
    j_zonc_centres = 0.5 * (j_zonc_edges[1:] + j_zonc_edges[:-1])

    j_zonc_frac_cut = 0.7
    j_zonc_mask = j_zonc_centres >= j_zonc_frac_cut

    v200 = get_v_200()

    fig = plt.figure()  # constrained_layout=True)
    fig.set_size_inches(12, 8.5, forward=True)
    ncols = 5
    four = 6

    spec = fig.add_gridspec(ncols=ncols * four - 1, nrows=4,  # width_ratios=[1]*(ncols*four-1),
                            height_ratios=[1, 0.5, 2.2, 0.5])

    ax = fig.add_subplot(spec[2 + 4 * 0, :])
    # cax = fig.add_subplot(spec[2, four*ncols-2])
    ax_ins = [fig.add_subplot(spec[0, four * i:four * i + four - 1]) for i in range(ncols)]

    # fig.subplots_adjust(hspace=0.25, wspace=0.2)
    fig.subplots_adjust(hspace=0.03, wspace=0.3)

    # work out taus to plot
    taus_to_plot = []

    a_h, vmax = get_hernquist_params()
    analytic_v_esc = get_analytic_hernquist_escape_velocity(bin_edges, vmax, a_h, save_name_append='diff')

    # (F_0, F_inf, n_12, log_tau_12
    #  ) = find_best_fit_j_zonc_frac(galaxy_name_list, snaps_list, save_name_append, _index, j_zonc_frac_cut)

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list[:5], snaps_list[:5])):
        kwargs = get_name_kwargs(galaxy_name)

        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, kwargs['mdm'])

        tau_edges = np.linspace(0, T_TOT, snaps + 2) / t_c_dm[_index]

        snap_ins = snaps - 1
        taus_to_plot.append(tau_edges[snap_ins])

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = get_name_kwargs(galaxy_name)
        kwargs['ls'] = '-'

        # different resolutions
        j = i // 5
        i = i % 5

        alpha = 0.2
        lw = 4
        window = snaps // 5 + 1
        poly_n = 5

        # work out constatns
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, kwargs['mdm'])

        # (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
        #  ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges=bin_edges)

        # t_edges = np.linspace(0, T_TOT, snaps+2)
        # t_centres = 0.5*(t_edges[1:] + t_edges[:-1])

        tau_edges = np.linspace(0, T_TOT, snaps + 2) / t_c_dm[_index]
        log_tau_edges = np.log10(np.linspace(0, T_TOT, snaps + 2) / t_c_dm[_index])
        log_tau_edges[0] = log_tau_edges[1] - 1e-8
        log_tau_centres = np.log10(0.5 * (tau_edges[1:] + tau_edges[:-1]))

        # load data
        j_zoncs = get_j_zonc_evolution(galaxy_name, save_name_append, snaps, bin_edges,
                                       j_zonc_edges, overwrite=False)

        # process array
        log_j_zoncs = np.log10(j_zoncs[:, _index, :] + 1e-4)

        j_zoncs_cumulative = np.cumsum(j_zoncs[:, _index, :], axis=1)
        j_zoncs_cumulative /= np.amax(j_zoncs_cumulative, axis=1)[:, np.newaxis]

        x = 0.5
        xp = 0.84
        xm = 0.16
        j_zonc_medians = np.zeros(snaps + 1)
        j_zonc_plus = np.zeros(snaps + 1)
        j_zonc_minus = np.zeros(snaps + 1)

        for ti in range(snaps + 1):
            f = interp1d(j_zonc_centres, j_zoncs_cumulative[ti, :], kind='linear')

            try:
                j_zonc_medians[ti] = brentq(lambda jzonc: (f(jzonc) - x), -1, 1)
            except ValueError as e:
                print(e)
            try:
                j_zonc_plus[ti] = brentq(lambda jzonc: (f(jzonc) - xp), -1, 1.1)
            except ValueError as e:
                print(e)
            try:
                j_zonc_minus[ti] = brentq(lambda jzonc: (f(jzonc) - xm), -1, 1)
            except ValueError as e:
                print(e)

        y = j_zonc_medians
        ax.errorbar(log_tau_centres, y,
                    c=kwargs['c'], marker='', ls='-', lw=lw, alpha=alpha)
        ax.errorbar(log_tau_centres, savgol_filter(y, window, poly_n),
                    c=kwargs['c'], marker='', ls='-')  # kwargs['ls'])
        y = j_zonc_plus
        ax.errorbar(log_tau_centres, y,
                    c=kwargs['c'], marker='', ls='-', lw=lw, alpha=alpha)
        ax.errorbar(log_tau_centres, savgol_filter(y, window, poly_n),
                    c=kwargs['c'], marker='', ls='--')  # kwargs['ls'])
        y = j_zonc_minus
        ax.errorbar(log_tau_centres, y,
                    c=kwargs['c'], marker='', ls='-', lw=lw, alpha=alpha)
        ax.errorbar(log_tau_centres, savgol_filter(y, window, poly_n),
                    c=kwargs['c'], marker='', ls='--')  # kwargs['ls'])

        # main colormesh j_z/j_c plot
        if j == 0:
            image = ax.pcolormesh(log_tau_edges, j_zonc_edges, log_j_zoncs.T,
                                  vmin=-4, vmax=-1, edgecolors='face', cmap='gray_r')

        # plot insert
        snap_ins = snaps - 1

        if j == 0:
            hl = 0.08
            ax.arrow(np.log10(tau_edges[snap_ins]), -1.2, 0, 2 * hl,
                     fc=kwargs['c'], ec='k', head_width=hl / 1.8, head_length=hl, zorder=2)

        # j_zonc pdfs
        combine_jz_bins = kwargs['jzbins']
        # j_zonc_combined = 0.5 * (j_zonc_edges[1::combine_jz_bins] + j_zonc_edges[:-1:combine_jz_bins])
        j_zonc_combined = j_zonc_edges[::combine_jz_bins]
        d_j_zonc = j_zonc_combined[1] - j_zonc_combined[0]

        if j == 0:
            max_j = 2
            if i == 2: max_j = 4
            if i == 3: max_j = 12
            if i == 4: max_j = 20
            # ax_ins[ncols-i-1].text(-0.95, combine_jz_bins * 1.45 * np.amax(j_zoncs[snap_ins, _index, :]) / d_j_zonc,
            #                        r'$\log \tau =$' + str(round(log_tau_centres[snap_ins], 1)),
            #                        c=kwargs['c'] ,va='top')
            ax_ins[ncols - i - 1].text(-0.95, max_j * 0.92,
                                       r'$\log \tau =$' + str(round(log_tau_centres[snap_ins], 1)),
                                       c=kwargs['c'], va='top')

            # ax_ins[ncols-i-1].set_ylim([0, combine_jz_bins * 1.5 * np.amax(j_zoncs[snap_ins, _index, :]) / d_j_zonc])
            ax_ins[ncols - i - 1].set_ylim([0, max_j])
            ax_ins[ncols - i - 1].set_xlim((j_zonc_edges[0], j_zonc_edges[-1]))

        for k, tau_to_plot in enumerate(taus_to_plot):
            if tau_edges[-1] > tau_to_plot:
                tau_index = np.argmin(np.abs(log_tau_centres - np.log10(tau_to_plot)))

                # j_zonc_data = np.sum([j_zoncs[tau_index-1, _index, i::combine_jz_bins] for i in range(combine_jz_bins)],
                #                      axis=0) / d_j_zonc

                j_zonc_data = np.sum(
                    [j_zoncs[tau_index - 1, _index, i::combine_jz_bins] for i in range(combine_jz_bins)],
                    axis=0)
                total_mass = np.sum(j_zonc_data)
                j_zonc_data *= 1 / total_mass * 1 / d_j_zonc

                ydobuled = np.concatenate(([0], np.repeat(j_zonc_data, 2), [0]))
                ax_ins[ncols - k - 1].plot(np.repeat(j_zonc_combined, 2), ydobuled,
                                           c=kwargs['c'], ls=kwargs['ls'])

    # ax_ins[0].set_ylabel(r'Mass [$10^{10} $M$_\odot$]')
    ax_ins[0].set_ylabel(r'$N^{-1} d N / d (j_z / j_c(E))$')
    ax_ins[ncols // 2].set_xlabel(r'$j_z / j_c(E)$')

    ax.set_ylabel(r'$j_z / j_c(E)$')
    ax.set_xlabel(r'$\log \tau$')

    # guide lines
    ax.plot([-5, 0], [1] * 2, c='k', ls=':', zorder=1)
    ax.plot([-5, 0], [-1] * 2, c='k', ls=':', zorder=1)
    ax.plot([-5, 0], [0] * 2, c='k', ls='--', zorder=1)
    ax.plot([-5, 0], [j_zonc_frac_cut] * 2, c='C6', ls='-.', zorder=5)

    ax.set_xlim((-3.5, -0.996))
    ax.set_ylim((j_zonc_edges[0], j_zonc_edges[-1]))

    if _index == 1:
        ax.text(-3.47, -0.9, r'$R = R_{1/2}$', c='k', va='bottom')

    # cbar = fig.colorbar(image, cax=cax)
    # cbar.set_label(r'$\log (M / 10^{10} $M$_\odot)$')

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return


def plot_jzonc_distributions_experiments(save_name_append='diff', name='', save=False):

    galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps']
    snaps_list = [101, 101, 101, 400, 400]

    # galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',]
    # snaps_list = [400,
    #               400,
    #               400,]
    # galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps']
    # snaps_list = [400]

    bin_edges = get_bin_edges(save_name_append)

    _index = 1
    if save_name_append == 'all':
        _index = 0
    
    j_zonc_edges = np.linspace(-1.2, 1.2, 4 * 24 + 1)
    j_zonc_centres = 0.5 * (j_zonc_edges[1:] + j_zonc_edges[:-1])

    j_zonc_frac_cut = 0.7
    j_zonc_mask = j_zonc_centres >= j_zonc_frac_cut

    fig = plt.figure()  # constrained_layout=True)
    fig.set_size_inches(9, 18, forward=True)

    spec = fig.add_gridspec(ncols=2, nrows=len(galaxy_name_list), width_ratios=[3.2, 1],
                            height_ratios=[1] * len(galaxy_name_list))

    axs = []
    ax_in = []
    for row in range(len(galaxy_name_list)):
        axs.append(fig.add_subplot(spec[row, 0]))
        ax_in.append(fig.add_subplot(spec[row, 1]))

        axs[row].set_xlim([0, T_TOT])
        axs[row].set_ylim([-1.2, 1.2])

        ax_in[row].set_ylim([-1.2, 1.2])
        # ax_in[row].set_xlim([0, 10])
        ax_in[row].set_xlim([-1.5, 1.5])

        if row != len(galaxy_name_list) - 1: axs[row].set_xticklabels([])
        if row != len(galaxy_name_list) - 1: ax_in[row].set_xticklabels([])
        ax_in[row].set_yticklabels([])

        # guide lines
        axs[row].axhline(1, 0, 1, c='grey', ls=':', zorder=5)
        if row == 0:
            axs[row].axhline(-1, 0.41, 0.68, c='grey', ls=':', zorder=5)
            axs[row].axhline(0, 0, 1, c='grey', ls=':', zorder=5)
        elif row == 1:
            axs[row].axhline(-1, 0.41, 0.62, c='grey', ls=':', zorder=5)
            axs[row].axhline(0, 0, 0.62, c='grey', ls=':', zorder=5)
        elif row == 2:
            axs[row].axhline(-1, 0.41, 0.69, c='grey', ls=':', zorder=5)
            axs[row].axhline(0, 0, 1, c='grey', ls=':', zorder=5)
        elif row > 2:
            axs[row].axhline(-1, 0.41, 1, c='grey', ls=':', zorder=5)
            axs[row].axhline(0, 0, 1, c='grey', ls=':', zorder=5)

        ax_in[row].axhline(1, 0, 1, c='grey', ls=':', zorder=5)
        if row > 0: ax_in[row].axhline(-1, 0, 1, c='grey', ls=':', zorder=5)
        ax_in[row].axhline(0, 0, 1, c='grey', ls=':', zorder=5)

    pth_b = [path_effects.Stroke(linewidth=3, foreground='black'), path_effects.Normal()]
    pth_b2 = [path_effects.Stroke(linewidth=5, foreground='black'), path_effects.Normal()]

    fig.subplots_adjust(hspace=0.01, wspace=0.007)

    # clevel = lambda level: (1.3 * level + 0.3)
    clevel = lambda level: (1.2 * level + 0.2)
    n_contours = 6
    contours = [0, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5]  # np.linspace(0, 0.5, n_contours+1)

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        snaps_to_plot = [0, snaps // 2, snaps - 1]
        snaps_ls = ['--', ':', '-']

        kwargs = get_name_kwargs(galaxy_name)
        kwargs['ls'] = '-'

        window = snaps // 5 + 1
        poly_n = 5

        t_centres = np.linspace(0, T_TOT, snaps + 1)

        # load data
        j_zoncs = get_j_zonc_evolution(galaxy_name, save_name_append, snaps, bin_edges,
                                       j_zonc_edges, overwrite=False)

        # process array
        log_j_zoncs = np.log10(j_zoncs[:, _index, :] + 1e-4)

        # image2 = axs[i].pcolormesh(t_centres, j_zonc_edges, log_j_zoncs.T,
        #                         vmin=-4, vmax=-1, edgecolors='face', cmap=kwargs['cmap'])

        j_zoncs_cumulative = np.cumsum(j_zoncs[:, _index, :], axis=1)
        j_zoncs_cumulative /= np.amax(j_zoncs_cumulative, axis=1)[:, np.newaxis]

        x = 0.5
        # xp = 0.84
        # xm = 0.16
        j_zonc_medians = np.zeros(snaps + 1)
        # v_phis_plus = np.zeros(snaps + 1)
        # v_phis_minus = np.zeros(snaps + 1)

        j_zonc_contours = np.zeros((2 * n_contours - 1, snaps + 1))

        for ti in range(snaps + 1):
            g = interp1d(j_zonc_centres, j_zoncs_cumulative[ti, :], kind='linear', fill_value='extrapolate')

            j_zonc_medians[ti] = brentq(lambda v: (g(v) - x), -1.2, 1.2)
            # v_phis_plus[ti] = brentq(lambda v: (g(v) - xp), 0, 2)
            # v_phis_minus[ti] = brentq(lambda v: (g(v) - xm), -1, 1.5)

            for k, level in enumerate(contours[1:-1]):
                j_zonc_contours[k, ti] = brentq(lambda v: (g(v) - level), -1.2, 1.2)
                j_zonc_contours[2 * n_contours - k - 2, ti] = brentq(lambda v: (g(v) - (1 - level)), -1.2, 1.2)

        y = j_zonc_medians
        # axs[i].errorbar(t_centres, y,
        #                 c='k', marker='', ls='-', path_effects=pth_w, lw=lw, alpha=alpha)
        axs[i].errorbar(t_centres, savgol_filter(y, window, poly_n),
                        c=kwargs['c'], marker='', ls='-', path_effects=pth_b2, lw=3)

        for k, level in enumerate(contours[1:-1]):
            axs[i].fill_between(t_centres, j_zonc_contours[k], j_zonc_contours[2 * n_contours - k - 2],
                                color=kwargs['cmap'](clevel(level)))

        # scale = 1e-3
        scale = 1
        combine_jz_bins = kwargs['jzbins']
        
        print(combine_jz_bins)
        j_zonc_combined = j_zonc_edges[::combine_jz_bins]
        d_j_zonc = (j_zonc_combined[1] - j_zonc_combined[0]) * scale

        # plot insert
        lsss = ['-', (1, (0.5, 0.5)), (1, (3, 0.8))]
        for k, snap in enumerate(snaps_to_plot):

            j_zonc_data = np.sum([j_zoncs[snap, _index, i::combine_jz_bins] for i in range(combine_jz_bins)], axis=0)
            total_mass = np.sum(j_zonc_data)
            j_zonc_data *= 1 / total_mass * 1 / d_j_zonc

            ydobuled = np.concatenate(([0], np.repeat(j_zonc_data, 2), [0]))
            ax_in[i].plot(np.log10(ydobuled + 1e-5), np.repeat(j_zonc_combined, 2),
                          c=kwargs['cmap']((k+2)/(len(snaps_to_plot)+3)), ls=lsss[k], lw=3)

        # axs[i].set_ylabel(r'$j_z / j_c(E)$')
        axs[i].set_ylabel(r'$\varepsilon_{\rm{circ}}$')

        axs[i].text(0.3, -0.81, r'$m_{\mathrm{DM}} = $' + kwargs['lab'],
                    va='bottom', c=kwargs['c'])
        axs[i].text(0.3, -1.13, r'$\mathrm{N}_{\mathrm{DM}} = $' + kwargs['labn'],
                    va='bottom', c=kwargs['c'])

        # print(galaxy_name)
        # print('f(jz/jc(E)>0.7,  t=9.8', 1-j_zoncs_cumulative[-2, j_zonc_centres>0.7][0])
        # # print('f(jz/jc(E)>0,    t=9.8', 1-j_zoncs_cumulative[-2, j_zonc_centres>0][0])
        # print('med(jz/jc(E)) t=9.8', j_zonc_medians[-2])
        # print('f(jz/jc(E)<-0.7, t=9.8', j_zoncs_cumulative[-2, j_zonc_centres<-0.7][-1])
        # print()

    if save_name_append == 'diff':
        axs[0].text(T_TOT - 3.1, -1.13, r'$R = R_{1/2, 0}$', c='k', va='bottom', ha='left')
    elif save_name_append == 'all':
        axs[0].text(T_TOT - 3.1, -1.13, r'All', c='k', va='bottom', ha='left')
    
    if 'V200-200' in galaxy_name_list[0]:
        axs[0].text(T_TOT - 3.6, -0.81, r'$V_{200} = 200$ km/s', c='k', va='bottom', ha='left')

    axs[len(galaxy_name_list) - 1].set_xlabel(r'$t \, [ \mathrm{Gyr} ] $')
    # ax_in[len(galaxy_name_list) - 1].set_xlabel(r'$\log \{ N^{-1} d N / d [j_z / j_c(E)] \}$')
    ax_in[len(galaxy_name_list) - 1].set_xlabel(r'$\log ({\rm PDF})$')

    # reds = matplotlib.cm.get_cmap('Reds')
    colors = np.array([matplotlib.colors.to_rgba('C3')] * 256)
    colors[:128, :3] = 1 - (1-colors[:128, :3]) * np.column_stack([np.linspace(0, 1, 128)] * 3)
    colors[128:, :3] = colors[128:, :3] * np.column_stack([np.linspace(1, 0, 128)] * 3)

    reds = matplotlib.colors.ListedColormap(colors)

    colors = np.array([matplotlib.colors.to_rgba('C4')] * 256)
    colors[:128, :3] = 1 - (1-colors[:128, :3]) * np.column_stack([np.linspace(0, 1, 128)] * 3)
    colors[128:, :3] = colors[128:, :3] * np.column_stack([np.linspace(1, 0, 128)] * 3)

    greys = matplotlib.colors.ListedColormap(colors)

    if len(galaxy_name_list) > 1 and save_name_append != 'all':
        axs[1].legend(reversed([(lines.Line2D([0, 1], [0, 1], color=reds(clevel(level)), ls='-', lw=8))
                                for level in contours[1:-1]]),
                      reversed([str(int(100 * level)) + r'$\, - \,$' + str(int(100 * (1 - level))) + r'$\, \%$'
                                for level in contours[1:-1]]),
                      title='Percentiles',
                      handler_map={tuple: HandlerTupleVertical(ndivide=5)},
                      loc='lower right', numpoints=1, handlelength=1, handletextpad=0.3, frameon=False,
                      labelspacing=0.05)

    if len(galaxy_name_list) > 2:
        axs[2].legend([(lines.Line2D([0, 1], [0, 1], color='C2', ls='-', lw=3, path_effects=pth_b2))],
                      ['Median'],
                      handler_map={tuple: HandlerTupleVertical(ndivide=5)},
                      loc='lower right', numpoints=1, handlelength=1, handletextpad=0.3, frameon=False)

    ax_in[0].legend([(lines.Line2D([0, 1], [0, 1], color=greys(2/6), ls='-', lw=3)),
                     (lines.Line2D([0, 1], [0, 1], color=greys(3/6), ls=(1,(0.5,0.5)), lw=3)),
                     (lines.Line2D([0, 1], [0, 1], color=greys(4/6), ls=(1,(3,0.8)), lw=3))],
                    ['ICs', '$4.9$ Gyr', '$9.8$ Gyr'],
                    handler_map={tuple: HandlerTupleVertical(ndivide=5)}, labelspacing=0.06,
                    loc='lower right', numpoints=1, handlelength=1, handletextpad=0.3, frameon=False)

    # cbar = fig.colorbar(image2, cax=cax2)
    # cbar.set_label(r'$\log (M / 10^{10} $M$_\odot)$')

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return


def plot_v_phi_distributions(save_name_append='diff', name='', save=False):
    galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps']
    snaps_list = [400, 400, 101, 101, 101]

    bin_edges = get_bin_edges(save_name_append)

    _index = 1
    
    pth_b3 = [path_effects.Stroke(linewidth=3, foreground='black'), path_effects.Normal()]
    pth_b5 = [path_effects.Stroke(linewidth=5, foreground='black'), path_effects.Normal()]

    v_phi_edges = np.linspace(-500, 500, 201)
    v_phi_centres = 0.5 * (v_phi_edges[1:] + v_phi_edges[:-1])

    v200 = get_v_200()

    v_phi_edges_v200 = v_phi_edges / v200
    v_phi_centres_v200 = v_phi_centres / v200

    fig = plt.figure()  # constrained_layout=True)
    fig.set_size_inches(12, 8.5, forward=True)
    ncols = 5
    four = 6

    spec = fig.add_gridspec(ncols=ncols, nrows=4,  # width_ratios=[1]*(ncols*four-1),
                            height_ratios=[1, 0.5, 2.2, 0.5])

    # ax2 = fig.add_subplot(spec[2, :-1])
    # cax2 = fig.add_subplot(spec[2, four*ncols-2])
    ax2 = fig.add_subplot(spec[2, :])
    ax_ins2 = [fig.add_subplot(spec[i]) for i in range(ncols)]

    # fig.subplots_adjust(hspace=0.25, wspace=0.2)
    fig.subplots_adjust(hspace=0.01, wspace=0.02)

    clevel = lambda level : (1.3 * level + 0.3)
    n_contours = 6#5
    contours = [0, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5] #np.linspace(0, 0.5, n_contours+1)
    
    # work out taus to plot
    taus_to_plot = []

    analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0,
                                         v200=False, dm_dispersion=False, dm_v_circ=False,
                                         analytic_dispersion=False, analytic_v_c=True,
                                         save_name_append=save_name_append)
    a_h, vmax = get_hernquist_params()
    analytic_v_esc = get_analytic_hernquist_escape_velocity(bin_edges, vmax, a_h, save_name_append='diff')

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list[:5], snaps_list[:5])):
        kwargs = get_name_kwargs(galaxy_name)

        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, kwargs['mdm'])

        tau_edges = np.linspace(0, T_TOT, snaps + 2) / t_c_dm[_index]

        snap_ins = snaps - 1
        taus_to_plot.append(tau_edges[snap_ins])

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = get_name_kwargs(galaxy_name)
        kwargs['ls'] = '-'

        # different resolutions
        j = i // 5
        i = i % 5

        alpha = 0.2
        lwt = 5
        lw = 2
        window = snaps // 5 + 1
        poly_n = 5

        # work out constatns
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, kwargs['mdm'])

        # (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
        #  ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges=bin_edges)

        # t_edges = np.linspace(0, T_TOT, snaps+2)
        # t_centres = 0.5*(t_edges[1:] + t_edges[:-1])

        tau_edges = np.linspace(0, T_TOT, snaps + 2) / t_c_dm[_index]
        log_tau_edges = np.log10(tau_edges)
        log_tau_edges[0] = log_tau_edges[1] - 1e-8
        log_tau_centres = np.log10(0.5 * (tau_edges[1:] + tau_edges[:-1]))

        # load data
        v_phis = get_v_phi_evolution(galaxy_name, save_name_append, snaps, bin_edges,
                                     v_phi_edges)

        # process array
        log_v_phis = np.log10(v_phis[:, _index, :] + 1e-4)

        v_phis_cumulative = np.cumsum(v_phis[:, _index, :], axis=1)
        v_phis_cumulative /= np.amax(v_phis_cumulative, axis=1)[:, np.newaxis]

        x = 0.5
        xp = 0.84
        xm = 0.16
        v_phis_medians = np.zeros(snaps + 1)
        v_phis_plus = np.zeros(snaps + 1)
        v_phis_minus = np.zeros(snaps + 1)

        for ti in range(snaps + 1):
            g = interp1d(v_phi_centres_v200, v_phis_cumulative[ti, :], kind='linear')
            try: v_phis_medians[ti] = brentq(lambda v: (g(v) - x), 0, 1.5)
            except: ValueError
            try: v_phis_plus[ti] = brentq(lambda v: (g(v) - xp), 0, 2)
            except: ValueError
            try: v_phis_minus[ti] = brentq(lambda v: (g(v) - xm), -1, 1.5)
            except: ValueError

        y = v_phis_medians
        ax2.errorbar(log_tau_centres, y,
                     c=kwargs['c'], marker='', ls='-', lw=lwt, alpha=alpha, zorder=9)
        ax2.errorbar(log_tau_centres, savgol_filter(y, window, poly_n),
                     c=kwargs['c'], marker='', ls='-', lw=lw, zorder=10, path_effects=pth_b3)  # ls=kwargs['ls'])
        y = v_phis_plus
        ax2.errorbar(log_tau_centres, y,
                     c=kwargs['c'], marker='', ls='-', lw=lwt, alpha=alpha, zorder=9)
        ax2.errorbar(log_tau_centres, savgol_filter(y, window, poly_n),
                     c=kwargs['c'], marker='', ls='--', lw=lw, zorder=10, path_effects=pth_b3)  # ls=kwargs['ls'])
        y = v_phis_minus
        ax2.errorbar(log_tau_centres, y,
                     c=kwargs['c'], marker='', ls='-', lw=lwt, alpha=alpha, zorder=9)
        ax2.errorbar(log_tau_centres, savgol_filter(y, window, poly_n),
                     c=kwargs['c'], marker='', ls='--', lw=lw, zorder=10, path_effects=pth_b3)  # kwargs['ls'])

        v_contours = np.zeros((2*n_contours-1, snaps+1))

        for ti in range(snaps + 1):
            g = interp1d(v_phi_centres_v200, v_phis_cumulative[ti, :], kind='linear')

            for k, level in enumerate(contours[1:-1]):
                v_contours[k, ti] = brentq(lambda v: (g(v) - level), -2, 2)
                v_contours[2*n_contours - k -2, ti] = brentq(lambda v: (g(v) - (1-level)), -2, 2)

        # for k, level in enumerate(contours[1:-1]):
            # ax2.fill_between(log_tau_centres, v_contours[k], v_contours[2*n_contours - k -2],
            #                     color=kwargs['cmap'](clevel(level)), zorder=-5)

            # ax2.errorbar(log_tau_centres, v_contours[k],
            #              c=kwargs['cmap'](clevel(level)), zorder=-1)
            # ax2.errorbar(log_tau_centres, v_contours[2*n_contours - k -2],
            #              c=kwargs['cmap'](clevel(level)), zorder=-1)

        # main colormesh j_z/j_c plot
        image2 = ax2.pcolormesh(log_tau_edges, v_phi_edges_v200, log_v_phis.T,
                                vmin=-4, vmax=-1, edgecolors='face', cmap=kwargs['cmap'], zorder=-5, alpha=0.2)#'grey_r')

        # main plot theory lines
        if j == 0:  # and i==0:
            (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
             ) = get_physical_from_model(galaxy_name, snaps, save_name_append)
            theory_best_p = np.sqrt(theory_best_p2)
            # theory_best_z = np.sqrt(theory_best_z2)
            # theory_best_R = np.sqrt(theory_best_r2)

            ax2.errorbar(log_tau_centres, theory_best_v[_index, :] / v200,
                         c='k', ls='-', zorder=10)
            ax2.errorbar(log_tau_centres, (theory_best_v[_index, :] + theory_best_p[_index, :]) / v200,
                         c='k', ls='--', zorder=10)
            ax2.errorbar(log_tau_centres, (theory_best_v[_index, :] - theory_best_p[_index, :]) / v200,
                         c='k', ls='--', zorder=10)

        # plot insert
        snap_ins = snaps - 1

        if j == 0:
            hl = 0.1
            ax2.arrow(np.log10(tau_edges[snap_ins]), -1.5, 0, 2 * hl,
                      fc=kwargs['c'], ec='k', head_width=hl / 2, head_length=hl, zorder=2)

        # scale = 1e-3
        scale = 1
        combine_vphi_bins = kwargs['vphibins']
        # v_phi_combined = 0.5 * (v_phi_edges_v200[1::combine_vphi_bins] + v_phi_edges_v200[:-1:combine_vphi_bins])
        v_phi_combined = v_phi_edges_v200[::combine_vphi_bins]
        d_v_phi = (v_phi_combined[1] - v_phi_combined[0]) * scale

        if j == 0:
            if j == 0:
                max_j = 2
                # if i == 2: max_j = 2
                if i == 3: max_j = 4
                if i == 4: max_j = 6

            # ax_ins2[ncols-i-1].text(-1.2,
            #                         combine_vphi_bins * 1.45 * np.amax(v_phis[snap_ins, _index, :]) / d_v_phi,
            #                        r'$\log \tau =$' + str(round(log_tau_centres[snap_ins], 1)),
            #                        c=kwargs['c'] ,va='top')

            ax_ins2[ncols - i - 1].text(-1.3, 1.2 * 0.92,
                                        r'$\log \, t/t_c =$' + str(round(log_tau_centres[snap_ins], 1)),
                                        c=kwargs['c'], va='top')

            # ax_ins2[ncols-i-1].set_ylim([0, combine_vphi_bins * 1.5 * np.amax(v_phis[snap_ins, _index, :]) / d_v_phi])
            # ax_ins2[ncols - i - 1].set_ylim([0, max_j])
            ax_ins2[ncols - i - 1].set_ylim([-1.5, 1.2])
            ax_ins2[ncols - i - 1].set_xlim((-1.5, 1.5))

            mass = np.sum(v_phis[snap_ins, _index, :]) / scale
            # print(mass, galaxy_name)
            ax_ins2[ncols - i - 1].errorbar(v_phi_edges_v200,  # mass *
                                            np.log10(scipy.stats.norm.pdf(
                                                v_phi_edges_v200, theory_best_v[_index, snap_ins] / v200,
                                                                  theory_best_p[_index, snap_ins] / v200) + 1e-5),
                                            c='k', ls='-.', zorder=5)

            if ncols - i - 1 != 0:
                ax_ins2[ncols - i - 1].set_yticklabels([])


        for k, tau_to_plot in enumerate(taus_to_plot):
            if tau_edges[-1] > tau_to_plot:
                tau_index = np.argmin(np.abs(log_tau_centres - np.log10(tau_to_plot)))

                v_phi_data = np.sum(
                    [v_phis[tau_index - 1, _index, i::combine_vphi_bins] for i in range(combine_vphi_bins)],
                    axis=0)
                total_mass = np.sum(v_phi_data)
                v_phi_data *= 1 / total_mass * 1 / d_v_phi

                ydobuled = np.concatenate(([0], np.repeat(v_phi_data, 2), [0]))
                ax_ins2[ncols - k - 1].plot(np.repeat(v_phi_combined, 2), np.log10(ydobuled + 1e-5),
                                            c=kwargs['c'], ls='-', lw=3)

        xlim = [-3.84, -1.34]
        ylim = [-1.5, 1.5] 
        if kwargs['c'] in ['C0', 'C1', 'C2']:
            dx = 0.40
        else:
            dx = 0.63
        if kwargs['c'] in ['C0']:
            dy = 0.32
        elif kwargs['c'] in ['C1', 'C3']:
            dy = 0.22
        else:
            dy = 0.12
    
        # ax0.text(dx * T_TOT, 10 ** (np.log10(ylim0[0]) + dy * np.log10(ylim0[1] / ylim0[0])),
        #          r'$m_{\mathrm{DM}} = $' + kwargs['lab'], ha='left', va='bottom', color=kwargs['c'])
        ax2.text(xlim[0] + dx * (xlim[1] - xlim[0]), ylim[0] + dy * (ylim[1] - ylim[0]),
                 r'$\mathrm{N}_{\mathrm{DM}} = $' + kwargs['labn'], ha='left', va='bottom', color=kwargs['c'])


    ax2.errorbar(xlim[0] + np.array([0.02, 0.08]) * (xlim[1] - xlim[0]), ylim[0] + np.array([0.45]*2) * (ylim[1] - ylim[0]),
                 c='grey', marker='', ls='-', lw=lw, path_effects=pth_b3)
    ax2.errorbar(xlim[0] + np.array([0.02, 0.08]) * (xlim[1] - xlim[0]), ylim[0] + np.array([0.35]*2) * (ylim[1] - ylim[0]),
                 c='grey', marker='', ls='--', lw=lw, path_effects=pth_b3)
    ax2.errorbar(xlim[0] + np.array([0.02, 0.08]) * (xlim[1] - xlim[0]), ylim[0] + np.array([0.25]*2) * (ylim[1] - ylim[0]),
                 c='k', marker='', ls='-', lw=lw)
    ax2.errorbar(xlim[0] + np.array([0.02, 0.08]) * (xlim[1] - xlim[0]), ylim[0] + np.array([0.15]*2) * (ylim[1] - ylim[0]),
                 c='k', marker='', ls='--', lw=lw)

    ax2.text(xlim[0] + 0.1 * (xlim[1] - xlim[0]), ylim[0] + 0.45 * (ylim[1] - ylim[0]),
             'median', ha='left', va='center')
    ax2.text(xlim[0] + 0.1 * (xlim[1] - xlim[0]), ylim[0] + 0.35 * (ylim[1] - ylim[0]),
             r'16 & 84th percentiles', ha='left', va='center')
    ax2.text(xlim[0] + 0.1 * (xlim[1] - xlim[0]), ylim[0] + 0.25 * (ylim[1] - ylim[0]),
             r'model $\overline{v}_\phi$', ha='left', va='center')
    ax2.text(xlim[0] + 0.1 * (xlim[1] - xlim[0]), ylim[0] + 0.15 * (ylim[1] - ylim[0]),
             r'mean $\overline{v}_\phi \pm \sigma_\phi$ ', ha='left', va='center')

    # ax_ins2[0].set_ylabel(r'$\log( N^{-1} d N / d (v_\phi / V_{200} ) )$')
    ax_ins2[0].set_ylabel(r'$\log({\rm PDF})$')
    ax_ins2[ncols // 2].set_xlabel(r'$v_\phi / V_{200}$')

    ax2.set_ylabel(r'$v_\phi / V_{200}$')
    ax2.set_xlabel(r'$\log \, t / t_c$')

    # guide lines
    # ax2.plot([-5,0], [analytic_v_circ[_index]/v200]*2, c='k', ls=':',  zorder=5)
    # ax2.plot([-5,0], [-analytic_v_circ[_index]/v200]*2, c='k', ls=':',  zorder=5)
    # ax2.plot([-5,0], [0]*2, c='k', ls='--',  zorder=5)

    # ax2.set_xlim((-3.5, -0.996))
    ax2.set_xlim(xlim)
    ax2.set_ylim(ylim)

    if _index == 1:
        # ax2.text(-3.47, -0.9, r'$[R_{1/2} \pm 0.1$dex]', c='k', va='bottom')
        ax2.text(-1.4, -1.4, r'$R = R_{1/2}$', c='k', va='bottom', ha='right')

    # cbar = fig.colorbar(image2, cax=cax2)
    # cbar.set_label(r'$\log (M / 10^{10} $M$_\odot)$')

    if save:
        plt.savefig(name, bbox_inches="tight")#, dpi=200)
        plt.close()

    return


def plot_v_phi_distributions_experiments(save_name_append='diff', name='', save=False):
    galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps']
    snaps_list = [101, 101, 101, 400, 400]
    # galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps']
    # snaps_list = [101, 101, 400]
    # galaxy_name_list = ['mu_5_smooth/fdisk_0p01_lgMdm_6p0_V200-200kmps',
    #                     'mu_5_smooth/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     'mu_5_smooth/fdisk_0p01_lgMdm_8p0_V200-200kmps']
    # snaps_list = [101, 101, 101]

    bin_edges = get_bin_edges(save_name_append)

    _index = 1

    v_phi_edges = np.linspace(-500, 500, 201)
    v_phi_centres = 0.5 * (v_phi_edges[1:] + v_phi_edges[:-1])

    v200 = get_v_200()

    v_phi_edges_v200 = v_phi_edges / v200
    v_phi_centres_v200 = v_phi_centres / v200
    
    analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0,
                                         v200=False, dm_dispersion=False, dm_v_circ=False,
                                         analytic_dispersion=False, analytic_v_c=True,
                                         save_name_append=save_name_append)
    a_h, vmax = get_hernquist_params()
    analytic_v_esc = get_analytic_hernquist_escape_velocity(bin_edges, vmax, a_h, save_name_append='diff')

    fig = plt.figure()  # constrained_layout=True)
    fig.set_size_inches(16, 10, forward=True)

    spec = fig.add_gridspec(ncols=2, nrows=len(galaxy_name_list), width_ratios=[3, 1],
                            height_ratios=[1] * len(galaxy_name_list))

    axs = []
    ax_in = []
    for row in range(len(galaxy_name_list)):
        axs.append(fig.add_subplot(spec[row,0]))
        ax_in.append(fig.add_subplot(spec[row,1]))

        axs[row].set_xlim([0, T_TOT])
        axs[row].set_ylim([-1.5,1.5])

        ax_in[row].set_ylim([-1.5,1.5])
        ax_in[row].set_xlim([-1.2,0.7])

        if row != len(galaxy_name_list) - 1: axs[row].set_xticklabels([])
        if row != len(galaxy_name_list) - 1: ax_in[row].set_xticklabels([])
        ax_in[row].set_yticklabels([])

        # guide lines
        axs[row].axhline(analytic_v_circ[_index]/v200, 0, 1, c='grey', ls=':',  zorder=5)
        if row == 0:
            axs[row].axhline(-analytic_v_circ[_index] / v200, 0, 0.75, c='grey', ls=':', zorder=5)
            axs[row].axhline(0, 0, 0.75, c='grey', ls=':', zorder=5)
        elif row == 1:
            axs[row].axhline(-analytic_v_circ[_index] / v200, 0, 0.68, c='grey', ls=':', zorder=5)
            axs[row].axhline(0, 0, 1, c='grey', ls=':',  zorder=5)
        elif row == 2:
            axs[row].axhline(-analytic_v_circ[_index] / v200, 0, 0.68, c='grey', ls=':', zorder=5)
            axs[row].axhline(0, 0, 1, c='grey', ls=':',  zorder=5)
        elif row > 2:
            axs[row].axhline(-analytic_v_circ[_index]/v200, 0, 1, c='grey', ls=':',  zorder=5)
            axs[row].axhline(0, 0, 1, c='grey', ls=':',  zorder=5)

        ax_in[row].axhline(analytic_v_circ[_index]/v200, 0, 1, c='grey', ls=':',  zorder=5)
        if row > 1: ax_in[row].axhline(-analytic_v_circ[_index]/v200, 0, 1, c='grey', ls=':',  zorder=5)
        ax_in[row].axhline(0, 0, 1, c='grey', ls=':',  zorder=5)

    pth_w = [path_effects.Stroke(linewidth=4, foreground='white'), path_effects.Normal()]
    pth_b = [path_effects.Stroke(linewidth=3, foreground='black'), path_effects.Normal()]
    pth_b2 = [path_effects.Stroke(linewidth=5, foreground='black'), path_effects.Normal()]

    fig.subplots_adjust(hspace=0.01, wspace=0.007)

    clevel = lambda level : (1.3 * level + 0.3)
    n_contours = 6#5
    contours = [0, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5] #np.linspace(0, 0.5, n_contours+1)

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        # snaps_to_plot = [0, snaps // 2, snaps - 1]
        # snaps_ls = ['--', ':', '-']
        snaps_to_plot = [snaps - 1]
        snaps_ls = ['-']
        
        kwargs = get_name_kwargs(galaxy_name)
        kwargs['ls'] = '-'

        alpha = 0.2
        lw = 8
        window = snaps // 5 + 1
        poly_n = 5

        # work out constatns
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, kwargs['mdm'])

        # (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
        #  ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges=bin_edges)

        t_centres = np.linspace(0, T_TOT, snaps + 1)

        # load data
        v_phis = get_v_phi_evolution(galaxy_name, save_name_append, snaps, bin_edges,
                                     v_phi_edges)

        # process array
        log_v_phis = np.log10(v_phis[:, _index, :] + 1e-4)

        # image2 = axs[i].pcolormesh(t_centres, v_phi_edges_v200, log_v_phis.T,
        #                         vmin=-4, vmax=-1, edgecolors='face', cmap=kwargs['cmap'])

        v_phis_cumulative = np.cumsum(v_phis[:, _index, :], axis=1)
        v_phis_cumulative /= np.amax(v_phis_cumulative, axis=1)[:, np.newaxis]

        x = 0.5
        # xp = 0.84
        # xm = 0.16
        v_phis_medians = np.zeros(snaps + 1)
        # v_phis_plus = np.zeros(snaps + 1)
        # v_phis_minus = np.zeros(snaps + 1)

        v_contours = np.zeros((2*n_contours-1, snaps+1))

        for ti in range(snaps + 1):
            g = interp1d(v_phi_centres_v200, v_phis_cumulative[ti, :], kind='linear')

            v_phis_medians[ti] = brentq(lambda v: (g(v) - x), 0, 1.5)
            # v_phis_plus[ti] = brentq(lambda v: (g(v) - xp), 0, 2)
            # v_phis_minus[ti] = brentq(lambda v: (g(v) - xm), -1, 1.5)

            for k, level in enumerate(contours[1:-1]):
                v_contours[k, ti] = brentq(lambda v: (g(v) - level), -2, 2)
                v_contours[2*n_contours - k -2, ti] = brentq(lambda v: (g(v) - (1-level)), -2, 2)

        y = v_phis_medians
        # axs[i].errorbar(t_centres, y,
        #                 c='k', marker='', ls='-', path_effects=pth_w, lw=lw, alpha=alpha)
        axs[i].errorbar(t_centres, savgol_filter(y, window, poly_n),
                        c=kwargs['c'], marker='', ls='-', path_effects=pth_b2, lw=3)  # ls=kwargs['ls'])
        # y = v_phis_plus
        # axs[i].errorbar(t_centres, y,
        #              c=kwargs['c'], marker='', ls='-', lw=lw, alpha=alpha)
        # axs[i].errorbar(t_centres, savgol_filter(y, window, poly_n),
        #              c=kwargs['c'], marker='', ls='--')  # ls=kwargs['ls'])
        # y = v_phis_minus
        # axs[i].errorbar(t_centres, y,
        #              c=kwargs['c'], marker='', ls='-', lw=lw, alpha=alpha)
        # axs[i].errorbar(t_centres, savgol_filter(y, window, poly_n),
        #              c=kwargs['c'], marker='', ls='--')  # kwargs['ls'])


        for k, level in enumerate(contours[1:-1]):
            axs[i].fill_between(t_centres, v_contours[k], v_contours[2*n_contours - k -2],
                                # color=matplotlib.cm.get_cmap('Greys')(1.3*level + 0.3))
                                color=kwargs['cmap'](clevel(level)))

        (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
         ) = get_physical_from_model(galaxy_name, snaps, save_name_append)
        theory_best_p = np.sqrt(theory_best_p2)
        # theory_best_z = np.sqrt(theory_best_z2)
        # theory_best_R = np.sqrt(theory_best_r2)

        axs[i].errorbar(t_centres, theory_best_v[_index, :] / v200, 
                        c='white', path_effects=pth_b, ls='--', lw=2, zorder=10)
        # axs[i].errorbar(t_centres, (theory_best_v[_index, :] + theory_best_p[_index, :]) / v200,
        #                 c='k', ls='--', zorder=10)
        # axs[i].errorbar(t_centres, (theory_best_v[_index, :] - theory_best_p[_index, :]) / v200,
        #                 c='k', ls='--', zorder=10)

        # scale = 1e-3
        scale = 1
        combine_vphi_bins = kwargs['vphibins']
        v_phi_combined = v_phi_edges_v200[::combine_vphi_bins]
        d_v_phi = (v_phi_combined[1] - v_phi_combined[0]) * scale
        
        # plot insert
        for k, snap in enumerate(snaps_to_plot):

            # mass = np.sum(v_phis[snap, _index, :]) / scale
            
            #theory
            ax_in[i].errorbar(np.log10(scipy.stats.norm.pdf(v_phi_edges_v200, theory_best_v[_index, snap] / v200,
                                                   theory_best_p[_index, snap] / v200)), v_phi_edges_v200,
                              c='k', ls=snaps_ls[k], zorder=-5, lw=3)

            v_phi_data = np.sum([v_phis[snap, _index, i::combine_vphi_bins] for i in range(combine_vphi_bins)], axis=0)
            total_mass = np.sum(v_phi_data)
            v_phi_data *= 1 / total_mass * 1 / d_v_phi

            ydobuled = np.concatenate(([0], np.repeat(v_phi_data, 2), [0]))
            ax_in[i].plot(np.log10(ydobuled), np.repeat(v_phi_combined, 2),
                          c=kwargs['c'], ls=snaps_ls[k], lw=3)

        axs[i].set_ylabel(r'$v_\phi / V_{200}$')

        axs[i].text(0.3, -1.4, r'$\mathrm{N}_{\mathrm{DM}} = $' + kwargs['labn'],
                    va='bottom', c=kwargs['c'])

    axs[2].text(T_TOT - 2.5, -1.4, r'$R = R_{1/2}$', c='k', va='bottom', ha='left')
    axs[2].text(T_TOT - 2.9, -0.95, r'$V_{200} = 200$ km/s', c='k', va='bottom', ha='left')

    axs[len(galaxy_name_list)-1].set_xlabel(r'$t [ \mathrm{Gyr} ] $')
    # ax_in[len(galaxy_name_list)-1].set_xlabel(r'$N^{-1} d N / d (v_\phi / V_{200} ) $')
    ax_in[len(galaxy_name_list)-1].set_xlabel(r'$ \log(PDF) $')

    purple = matplotlib.cm.get_cmap('Purples')
    axs[0].legend(reversed([(lines.Line2D([0, 1], [0, 1], color=purple(clevel(level)), ls='-', lw=8))
                            for level in contours[1:-1]]),
                  reversed([str(int(100*level)) + r'$\, - \,$' + str(int(100*(1-level))) + r'$\, \%$'
                            for level in contours[1:-1]]),
                  title='Percentiles',
                  handler_map={tuple: HandlerTupleVertical(ndivide=5)},
                  loc='lower right', numpoints=1, handlelength=1, handletextpad=0.3, frameon=False,
                  labelspacing=0.05)
    axs[1].legend([(lines.Line2D([0, 1], [0, 1], color='C2', ls='-', lw=3, path_effects=pth_b2)),
                   (lines.Line2D([0, 1], [0, 1], color='white', ls='--', lw=2, path_effects=pth_b))],
                  ['Measured median', 'Model mean'],
                  handler_map={tuple: HandlerTupleVertical(ndivide=5)},
                  loc='lower right', numpoints=1, handlelength=1, handletextpad=0.3, frameon=False)

    # ax_in[0].legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='--')),
    #                   (lines.Line2D([0, 1], [0, 1], color='grey', ls=':')),
    #                   (lines.Line2D([0, 1], [0, 1], color='grey', ls='-'))],
    #                 ['ICs', '$t = 4.9$ Gyr', '$t = 9.8$ Gyr'],
    #                 handler_map={tuple: HandlerTupleVertical(ndivide=5)},
    #                 loc='lower right', numpoints=1, handlelength=1, handletextpad=0.3, frameon=False)
    ax_in[1].legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='-'))],
                    ['$t = 9.8$ Gyr'],
                    handler_map={tuple: HandlerTupleVertical(ndivide=5)},
                    loc='lower left', numpoints=1, handlelength=1, handletextpad=0.3, frameon=False)
    ax_in[0].legend([(lines.Line2D([0, 1], [0, 1], color='C4', ls='-', lw=4)),
                     (lines.Line2D([0, 1], [0, 1], color='k', ls='-', lw=4))],
                     ['Fiducial\nmodel', 'Prediction'],
                     handler_map={tuple: HandlerTupleVertical(ndivide=5)},
                     loc='lower left', numpoints=1, handlelength=1, handletextpad=0.3, frameon=False)

    # cbar = fig.colorbar(image2, cax=cax2)
    # cbar.set_label(r'$\log (M / 10^{10} $M$_\odot)$')

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return


###############################################################################

@lru_cache(maxsize=100000)
def get_thin_disk_SG_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a):
    # Ludlow et al. 2021 Equation (11)
    h_SG = sigma_z ** 2 / (np.pi * GRAV_CONST * Sigma_R_star)

    # #Benitez-Llambey et al. 2018
    # z_SG = 0.55 * h_SG
    # me
    z_SG = np.log(3) / 2 * h_SG

    return (z_SG)


@lru_cache(maxsize=100000)
def get_thin_disk_NSG_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a):
    # Ludlow et al. 2021 Equation (10)
    h_NSG = np.sqrt(2) * sigma_z * R / v_c_halo

    # #Benitez-Llambey et al. 2018
    # z_NSG = 0.477 * h_NSG
    # me
    z_NSG = erfinv(0.5) * h_NSG

    return (z_NSG)


@lru_cache(maxsize=100000)
def get_thin_disk_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a):
    z_SG = get_thin_disk_SG_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a)

    z_NSG = get_thin_disk_NSG_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a)

    # Ludlow et al. 2021 Equation (18)
    z_thin_half = (1 / z_SG ** 2 + 1 / (2 * z_SG * z_NSG) + 1 / z_NSG ** 2) ** (-1 / 2)

    return (z_thin_half)


@lru_cache(maxsize=100000)
def get_thick_disk_NSG_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a):
    # Ludlow et al. 2021 Equation (20)
    nu_2inv = v200 * v200 / (sigma_z * sigma_z)
    rho = lambda z, nu_2inv, R, r200: np.exp(nu_2inv * (1 / (np.sqrt((R / r200) ** 2 + (z / r200) ** 2) + a / r200)
                                                        - 1 / (R / r200 + a / r200)))

    tot = quad(rho, 0, 200, args=(nu_2inv, R, r200))[0]

    half = lambda z_half, nu_2inv, R, r200: quad(rho, 0, z_half, args=(nu_2inv, R, r200))[0] - tot / 2

    out = brentq(half, 0, 200, args=(nu_2inv, R, r200))

    return (out)


# @lru_cache(maxsize=100000)
def get_disk_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a):
    z_thin_half = get_thin_disk_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a)

    z_thick_NSG = get_thick_disk_NSG_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a)

    z_NSG = get_thin_disk_NSG_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a)

    # Ludlow et al. 2021 Equation (21)
    z_thick_half = z_thin_half + (z_thick_NSG - z_NSG)

    return (z_thick_half)


def plot_shapes_global(galaxy_name_list, snaps_list, model=True, name='', save=False):
    _index = 1
    # save_name_append = 'cum'

    # fig = plt.figure()
    # fig.set_size_inches(9, 8, forward=True)
    # ax0 = plt.subplot(2, 1, 1)
    # ax1 = plt.subplot(2, 1, 2)

    fig, axs = plt.subplots(3, 2, figsize=(13, 12), gridspec_kw={'width_ratios': [9, 4]})
    ax0, ax00 = axs[0]
    ax1, ax10 = axs[1]
    ax2, ax20 = axs[2]

    ax10.set_visible(False)

    fig.subplots_adjust(hspace=0.02, wspace=0.01)

    # set up plots
    ax0.semilogy()
    ax1.semilogy()
    ax2.semilogy()
    ax00.loglog()
    ax20.loglog()
    # ax0.set_xlim([0, T_TOT * (101-BURN_IN)/101])
    # ax1.set_xlim([0, T_TOT * (101-BURN_IN)/101])
    ax0.set_xlim([0, T_TOT])
    ax1.set_xlim([0, T_TOT])
    ax2.set_xlim([0, T_TOT])
    
    ax00.set_xlim([0, T_TOT])
    ax20.set_xlim([0, T_TOT])

    ylim1 = [0.015, 1] #c/a
    ylim0 = [0.008, 0.53333] #z/R
    ylim0[1] = ylim0[0] * ylim1[1] / ylim1[0]
    ylim2 = ylim1
    
    ax0.set_ylim(ylim0)
    ax1.set_ylim(ylim1)
    ax2.set_ylim(ylim2)

    ax00.set_xlim(ylim1)
    ax20.set_xlim(ylim1)
    ax00.set_ylim(ylim0)
    ax20.set_ylim(ylim2)
    
    ax0.set_yticks([0.01, 0.03, 0.1, 0.3])
    ax0.set_yticklabels([0.01, 0.03, 0.1, 0.3])
    ax1.set_yticks([0.03, 0.1, 0.3, 1])
    ax1.set_yticklabels([0.03, 0.1, 0.3, 1])
    ax2.set_yticks([0.03, 0.1, 0.3, 1])
    ax2.set_yticklabels([0.03, 0.1, 0.3, 1])

    ax00.set_xticks([0.03, 0.1, 0.3, 1])
    ax00.set_xticklabels([0.03, 0.1, 0.3, 1])
    ax20.set_xticks([0.03, 0.1, 0.3, 1])
    ax20.set_xticklabels([0.03, 0.1, 0.3, 1])
    ax00.set_yticks([0.01, 0.03, 0.1, 0.3])
    ax00.set_yticklabels([])
    ax20.set_yticks([0.03, 0.1, 0.3, 1])
    ax20.set_yticklabels([])

    ax0.set_xticklabels([])
    ax1.set_xticklabels([])

    ax0.set_ylabel(r'$z_{1/2}/R_{1/2}$')
    ax1.set_ylabel(r'$c/a$')
    ax2.set_ylabel(r'$c/a (R = R_{1/2, 0})$')
    ax2.set_xlabel(r'$t$ [Gyr]')
    ax00.set_xlabel(r'$c/a$')
    ax20.set_xlabel(r'$c/a$')

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = get_name_kwargs(galaxy_name)

        # time
        time = np.linspace(0, T_TOT, snaps + 1)
        # time = time - time[BURN_IN]

        # load data
        a, b, c = load_galaxy_abc(galaxy_name, '', snaps, [0, 30])

        a_diff, b_diff, c_diff = load_galaxy_abc(galaxy_name, 'diff', snaps,
                                                 get_bin_edges(save_name_append='diff'))

        z_half = get_median_z(galaxy_name, '', snaps, [0, 30])

        R_half = get_R_half(galaxy_name, snaps)

        alpha = 0.2
        lw = 6
        window = snaps // 5 + 1
        poly_n = 5

        y = z_half.T[0] / R_half
        ax0.errorbar(time, y,
                     c=kwargs['c'], marker='', ls='-', lw=lw, alpha=alpha)
        ax0.errorbar(time, savgol_filter(y, window, poly_n),
                     c=kwargs['c'], marker='', ls=kwargs['ls'], lw=3)

        y = c / a
        ax1.errorbar(time, y,
                     c=kwargs['c'], marker='', ls='-', lw=lw, alpha=alpha)
        ax1.errorbar(time, savgol_filter(y, window, poly_n),
                     c=kwargs['c'], marker='', ls=kwargs['ls'], lw=3)
        
        y = c_diff[:, _index] / a_diff[:, _index]
        ax2.errorbar(time, y,
                     c=kwargs['c'], marker='', ls='-', lw=lw, alpha=alpha)
        ax2.errorbar(time, savgol_filter(y, window, poly_n),
                     c=kwargs['c'], marker='', ls=kwargs['ls'], lw=3)
        

        x = c / a
        y = z_half.T[0] / R_half
        ax00.errorbar(x, y,
                     c=kwargs['c'], marker='', ls='-', lw=lw, alpha=alpha)
        ax00.errorbar(savgol_filter(x, window, poly_n), savgol_filter(y, window, poly_n),
                     c=kwargs['c'], marker='', ls=kwargs['ls'], lw=3)

        x = c / a
        y = c_diff[:, _index] / a_diff[:, _index]
        ax20.errorbar(x, y,
                     c=kwargs['c'], marker='', ls='-', lw=lw, alpha=alpha)
        ax20.errorbar(savgol_filter(x, window, poly_n), savgol_filter(y, window, poly_n),
                     c=kwargs['c'], marker='', ls=kwargs['ls'], lw=3)

        #prints
        print(galaxy_name)
        print((z_half.T[0] / R_half)[0], (z_half.T[0] / R_half)[-1], (z_half.T[0] / R_half)[-1] / (z_half.T[0] / R_half)[0])
        print((c / a)[0], (c/a)[-1], (c/a)[-1] / (c/a)[0])
        print(c_diff[0, _index] / a_diff[0, _index], c_diff[-1, _index] / a_diff[-1, _index],
              (c_diff[-1, _index] / a_diff[-1, _index]) / (c_diff[0, _index] / a_diff[0, _index]))
        print()

        #model
        if model:
            # radius to investigate is half mass radius
            R = get_bin_edges(save_name_append='cum')[2 * _index + 1]
            bin_edges = get_bin_edges(save_name_append='diff')

            # halo stuff
            a_h, vmax = get_hernquist_params()
            v200 = get_v_200()
            r200 = get_r_200()

            # parameters with units for heights
            M_disk, R_disk = get_exponential_disk_params()

            # _c = (R_disk + bin_edges[2*_index+1]) * np.exp(-bin_edges[2*_index+1]/R_disk) + 0.5*(
            #         R_disk + bin_edges[2*_index]) * np.exp(-bin_edges[2*_index]/R_disk)
            # R_analytic_questionmark = -R_disk * (lambertw(-_c / (np.exp(1)*R_disk)) + 1).real

            # exp_int = lambda R: np.exp(- R / R_disk ) / R
            exp_int = lambda R: R * np.exp(- R / R_disk)
            # exp_int = lambda R: R**3 * np.exp(- R / R_disk )
            tot = quad(exp_int, bin_edges[2 * _index], bin_edges[2 * _index + 1])[0]
            zero = lambda R: quad(exp_int, bin_edges[2 * _index], R)[0] - 0.5 * tot
            R_mass_centre = brentq(zero, bin_edges[2 * _index], bin_edges[2 * _index + 1])

            R = R_mass_centre
            # R_mass_centre = R

            sigma_dm = get_analytic_hernquist_dispersion([R, R], vmax, a_h, save_name_append='diff')[0]
            v_c_halo = get_analytic_hernquist_circular_velocity([R, R], vmax, a_h, save_name_append='diff')[0]

            Sigma_R_star = get_exponential_surface_density(R, M_disk, R_disk, save_name_append='diff')

            # set up arrays
            # elevenx = 101  # 101
            # sigma_z2 = np.logspace(np.log10(0.03), np.log10(3 * sigma_dm ** 2), elevenx)
            (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
             ) = get_physical_from_model(galaxy_name, snaps, save_name_append='diff')

            theory_best_z2 = theory_best_z2[_index, :]

            z_thin_SG = np.array([get_thin_disk_SG_height(np.sqrt(s_z), R, Sigma_R_star, v_c_halo, v200, r200, a_h)
                                  for s_z in theory_best_z2])
            z_thin_NSG = np.array([get_thin_disk_NSG_height(np.sqrt(s_z), R, Sigma_R_star, v_c_halo, v200, r200, a_h)
                                   for s_z in theory_best_z2])
            z_thin_half = np.array([get_thin_disk_height(np.sqrt(s_z), R, Sigma_R_star, v_c_halo, v200, r200, a_h)
                                    for s_z in theory_best_z2])

            z_thick_NSG = np.array([get_thick_disk_NSG_height(np.sqrt(s_z), R, Sigma_R_star, v_c_halo, v200, r200, a_h)
                                    for s_z in theory_best_z2])

            z_thick_half = np.array([get_disk_height(np.sqrt(s_z), R, Sigma_R_star, v_c_halo, v200, r200, a_h)
                                     for s_z in theory_best_z2])

            # Round 2
            c_SG = 0.6159978007462151
            c_NSG = 0.5180308801010115

            c_on_a_thin_SG = z_thin_SG / (R / np.sqrt(2)) * 2 * c_SG / np.log(3)
            c_on_a_thin_NSG = z_thin_NSG / (R / np.sqrt(2)) * c_NSG / erfinv(0.5)

            c_on_a_thick_NSG = np.zeros(len(theory_best_z2))
            for i in range(len(theory_best_z2)):
                thick = lambda z: np.exp(1 / (theory_best_z2[i] / v200 ** 2) * (
                        1 / (np.sqrt((R / r200) ** 2 + (z / r200) ** 2) + a_h / r200) - 1 / (R / r200 + a_h / r200)))
                z_distribution = thick
                c_2 = z_thick_NSG[i] ** 2
                for _ in range(10):  # 2000):
                    top_i = lambda z: 1 / (1 + z ** 2 / (2 * c_2)) * z ** 2 * z_distribution(z)
                    bot_i = lambda z: 1 / (1 + z ** 2 / (2 * c_2)) * z_distribution(z)

                    top = quad(top_i, 0, 1 * r200)[0]
                    bot = quad(bot_i, 0, 1 * r200)[0]

                    # c_2 = top / bot
                    c_2 = np.divide(top, bot)

                # print(c_2)
                c_on_a_thick_NSG[i] = (np.sqrt(c_2) / (R / np.sqrt(2)))

            c_on_a_thin_half = np.sqrt(
                1 / (1 / c_on_a_thin_SG ** 2 + 1 / (2 * c_on_a_thin_SG * c_on_a_thin_NSG) + 1 / c_on_a_thin_NSG ** 2))

            c_on_a_thick_half = c_on_a_thin_half + (c_on_a_thick_NSG - c_on_a_thin_NSG)

            mask = ~np.isnan(c_on_a_thick_half)
            estimate = InterpolatedUnivariateSpline(theory_best_z2[mask], c_on_a_thick_half[mask], check_finite=True)

            # ax0.errorbar(time, z_thick_half / R,
            #              ls='--', c='k', lw=3)
            # ax1.errorbar(time, c_on_a_thick_half,
            #              ls='--', c='k', lw=3)

            ax2.errorbar(time, c_on_a_thick_half,
                         ls='--', c=kwargs['c'], lw=3)

        #decorations
        if kwargs['c'] in ['C0', 'C1', 'C2']:
            dx = 0.33
        else:
            dx = 0.66
        if kwargs['c'] in ['C0', 'C3']:
            dy = 0.22
        elif kwargs['c'] in ['C1', 'C4']:
            dy = 0.12
        else:
            dy = 0.02

        ax0.text(dx * T_TOT, 10 ** (np.log10(ylim0[0]) + dy * np.log10(ylim0[1] / ylim0[0])),
                 r'$m_{\mathrm{DM}} = $' + kwargs['lab'], ha='left', va='bottom', color=kwargs['c'])
        ax1.text(dx * T_TOT, 10 ** (np.log10(ylim1[0]) + dy * np.log10(ylim1[1] / ylim1[0])),
                 r'$\mathrm{N}_{\mathrm{DM}} = $' + kwargs['labn'], ha='left', va='bottom', color=kwargs['c'])

        # print(galaxy_name)
        # # print('z[0]', z_half.T[0][0])
        # # print('R[0]', R_half[0])
        # # print('z_d[0]', z_half.T[0][0] / np.log(3)*2)
        # # print('R_d[0]', R_half[0] / (-lambertw(-1/(2*np.exp(1)), -1) - 1).real)
        # print('z/R[0]', z_half.T[0][0] / R_half[0])
        # print('z/R[1Gyr]', z_half.T[0][int(snaps * 1 / T_TOT)] / R_half[int(snaps * 1 / T_TOT)])
        # print('z/R[2Gyr]', z_half.T[0][int(snaps * 2 / T_TOT)] / R_half[int(snaps * 2 / T_TOT)])
        # print('z/R[4Gyr]', z_half.T[0][int(snaps * 4 / T_TOT)] / R_half[int(snaps * 4 / T_TOT)])
        # # print('z/R[0]', z_half.T[0][0] / R_half[0]  / np.log(3)*2 * (-lambertw(-1/(2*np.exp(1)), -1) - 1).real)
        # print('z/R[-1]', z_half.T[0][-1] / R_half[-1])
        # print('z/R[-1]/z/R[0]', (z_half.T[0][-1] / R_half[-1]) / (z_half.T[0][0] / R_half[0]))
        # # print('c/a[0]', c[0]/a[0])
        # # print('c/a[-1]', c[-1]/a[-1])
        # # print('c/a[-1]/c/a[0]', (c[-1]/a[-1])/(c[0]/a[0]))
        # print()

        # print('z/R[0]', z_half.T[0][-1] / R_half[-1])

    ax00.errorbar([1e-4, 1e1], [1e-4, 1e1],
                  c='grey', marker='', ls='--', lw=2, zorder=-10)
    #for uniform density sphere: z12 is one soln to z^3 - 3 r^2 z + r^3, R12 = sqrt(1 - 2^(-2/3))
    z12R12_sphere_const = 0.34730 / 0.608309
    ax00.errorbar([1e-4, 1e1], [1e-4 * z12R12_sphere_const, 1e1 * z12R12_sphere_const],
                  c='grey', marker='', ls='--', lw=2, zorder=-10)


    ax20.errorbar([1e-4, 1e1], [1e-4, 1e1],
                  c='grey', marker='', ls='--', lw=2, zorder=-10)

    ax00.text(0.05, 0.07, '1:1', c='gray',
              rotation=np.arctan(1 / 1) * 180 / np.pi)  # angle0)
    ax00.text(0.095, 0.037, '1$\,$:$\,$' + f'{z12R12_sphere_const:.3f}', c='gray',
              rotation=np.arctan(1 / 1) * 180 / np.pi)  # angle0)

    ax20.text(0.05, 0.07, '1:1', c='gray',
              rotation=np.arctan(1 / 1) * 180 / np.pi)  # angle0)

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    # plt.figure()
    # for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
    #   a, b, c = load_galaxy_abc(galaxy_name, '', snaps, [0,30])
    #
    #   z_half = get_median_z(galaxy_name, '', snaps, [0,30])
    #   R_half = get_R_half(galaxy_name, '', snaps)
    #
    #   plt.scatter(z_half.T[0] / R_half, c/a)
    #
    #   # plt.errorbar([0,1], [0,2])
    #   plt.errorbar([0,7], [0,12])

    return


def plot_poster_morphologies(galaxy_name_list, snaps_list, name='', save=False):
    _index = 1
    save_name_append = 'diff'

    fig = plt.figure()
    fig.set_size_inches(10, 6, forward=True)
    ax0 = plt.subplot(2, 1, 1)
    ax1 = plt.subplot(2, 1, 2)

    fig.subplots_adjust(hspace=0.04, wspace=0.01)

    # set up plots
    # ax0.semilogy()
    # ax1.semilogy()
    # ax0.set_xlim([0, T_TOT * (101-BURN_IN)/101])
    # ax1.set_xlim([0, T_TOT * (101-BURN_IN)/101])
    ax0.set_xlim([0, T_TOT])
    ax1.set_xlim([0, T_TOT])

    # ax0.set_yticks([1.0, 0.8, 0.6, 0.4, 0.2, 0.0])
    # ax1.set_yticks([1.0, 0.8, 0.6, 0.4, 0.2, 0.0])
    # ax0.set_yticks([1.0, 2/3, 1/3, 0.0])
    # ax1.set_yticks([1.0, 2/3, 1/3, 0.0])

    eps = 0.02
    # ylim0 = [0.333-eps, 1+eps]
    ylim0 = [0-eps, 1+eps]
    ylim1 = [0-eps, 1+eps]
    # ylim1 = [0.2-eps, 1+eps]
    ax0.set_ylim(ylim0)
    ax1.set_ylim(ylim1)

    ax0.set_xticklabels([])

    ax0.set_ylabel(r'$\kappa_{\rm rot}$', fontsize=28)
    ax1.set_ylabel(r'$1 - c/a$')
    ax1.set_xlabel(r'$t$ [Gyr]')

    ax0.axhline(1, 0, 1, c='grey', ls=':')
    ax0.axhline(1/3, 0.5, 1, c='grey', ls=':')
    ax0.axhline(1/3, 0, 0.12, c='grey', ls=':')
    ax1.axhline(1, 0, 1, c='grey', ls=':')
    ax1.axhline(0, 0, 1, c='grey', ls=':')

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = get_name_kwargs(galaxy_name)
        bin_edges = get_bin_edges(save_name_append, galaxy_name=galaxy_name)

        # time
        time = np.linspace(0, T_TOT, snaps + 1)
        # time = time - time[BURN_IN]

        # load data
        a, b, c = load_galaxy_abc(galaxy_name, '', snaps, [0, 30])
        kappa_rot = get_kappa_rot(galaxy_name, save_name_append, snaps, bin_edges)

        alpha = 0.2
        lwr = 10
        lws = 4
        window = snaps // 5 + 1
        poly_n = 5

        y = kappa_rot[:, _index]
        ax0.errorbar(time, y,
                     c=kwargs['c'], marker='', ls='-', lw=lwr, alpha=alpha)
        ax0.errorbar(time, savgol_filter(y, window, poly_n),
                     c=kwargs['c'], marker='', lw=lws, ls=kwargs['ls'])

        y = 1 - c / a
        ax1.errorbar(time, y,
                     c=kwargs['c'], marker='', ls='-', lw=lwr, alpha=alpha)
        ax1.errorbar(time, savgol_filter(y, window, poly_n),
                     c=kwargs['c'], marker='', lw=lws, ls=kwargs['ls'])

        # if kwargs['c'] in ['C0', 'C1', 'C2']:
        #     dx = 0.33
        # else:
        #     dx = 0.66
        # if kwargs['c'] in ['C0', 'C3']:
        #     dy = 0.22
        # elif kwargs['c'] in ['C1', 'C4']:
        #     dy = 0.12
        # else:
        #     dy = 0.02
        # 
        # ax0.text(dx * T_TOT, ylim1[0] + dy * (ylim1[1] - ylim1[0]),
        #          r'$\mathrm{N}_{\mathrm{DM}} = $' + kwargs['labn'], ha='left', va='bottom', color=kwargs['c'])
        # ax1.text(dx * T_TOT, ylim0[0] + dy * (ylim0[1] -  ylim0[0]),
        #          r'$m_{\mathrm{DM}} = $' + kwargs['lab'], ha='left', va='bottom', color=kwargs['c'])

    # ax0.legend([(lines.Line2D([0, 1], [0, 1], ls='-', lw=lws, c = 'C' + str(i))) for i in range(len(galaxy_name_list))],
    #            [r'$\mathrm{N}_{\mathrm{DM}} = $' + get_name_kwargs(galaxy_name)['labn'] for galaxy_name in galaxy_name_list],
    #            handlelength=1, handletextpad=0.6, frameon=False, labelspacing=0.2,
    #            ncol=2, columnspacing=0.6, borderpad=0.1, loc='lower left')
    # ax1.legend([(lines.Line2D([0, 1], [0, 1], ls='-', lw=lws, c = 'C' + str(i))) for i in range(len(galaxy_name_list))],
    #            [r'$m_{\mathrm{DM}} = $' + get_name_kwargs(galaxy_name)['lab'] for galaxy_name in galaxy_name_list],
    #            handlelength=1, handletextpad=0.6, frameon=False, labelspacing=0.2,
    #            ncol=2, columnspacing=0.6, borderpad=0.1, loc='lower left')

    ax0.legend([(lines.Line2D([0, 1], [0, 1], ls='-', lw=lws, c = 'C' + str(i))) for i in np.flip(range(len(galaxy_name_list)))],
               [get_name_kwargs(galaxy_name)['labn'] for galaxy_name in np.flip(galaxy_name_list)],
               handlelength=1, handletextpad=0.4, frameon=False, labelspacing=0.1, title_fontsize=19, fontsize=19,
               ncol=3, columnspacing=0.5, borderpad=0.3, borderaxespad=0.2, loc='lower left',
               title=r'Number of particles') #, $\mathrm{N}_{\mathrm{DM}}$')
    ax1.legend([(lines.Line2D([0, 1], [0, 1], ls='-', lw=lws, c = 'C' + str(i))) for i in np.flip(range(len(galaxy_name_list)))],
               [get_name_kwargs(galaxy_name)['lab'] for galaxy_name in np.flip(galaxy_name_list)],
               handlelength=1, handletextpad=0.4, frameon=False, labelspacing=-0.05, title_fontsize=19, fontsize=19,
               ncol=3, columnspacing=0.3, borderpad=0.3, borderaxespad=0.2, loc='lower left',
               title=r'DM particle mass') #, $m_{\mathrm{DM}}$')

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return


def plot_heights(galaxy_name_list, snaps_list, name='', save=False):
    _index = 1
    save_name_append = 'diff'

    # set up plots
    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(15, 4, forward=True)
    widths = [2, 1, 1]
    heights = [1]
    spec = fig.add_gridspec(ncols=3, nrows=1, width_ratios=widths, height_ratios=heights)

    fig.subplots_adjust(wspace=0.02)

    axs = []
    for row in range(len(heights)):
        axs.append([])
        for col in range(len(widths)):
            axs[row].append(fig.add_subplot(spec[row, col]))
            if col == 0:
                axs[row][col].semilogy()
                axs[row][col].set_xlim([0, T_TOT * (101 - BURN_IN) / 101])
            if col == 1:
                axs[row][col].loglog()
                axs[row][col].set_xlim([1e-2, 1.5e0])
            if col == 2:
                axs[row][col].loglog()
                axs[row][col].set_xlim([1.2e-3, 1.5e0])
            axs[row][col].set_ylim(2e-2, 1.5e0)
            if col != 0: axs[row][col].set_yticklabels([])

    pth_w = [path_effects.Stroke(linewidth=4, foreground='white'), path_effects.Normal()]
    pth_b = [path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()]

    # plt.figure()

    # radius to investigate is half mass radius
    R = get_bin_edges(save_name_append='cum')[2 * _index + 1]
    bin_edges = get_bin_edges(save_name_append='diff')

    # halo stuff
    a_h, vmax = get_hernquist_params()
    v200 = get_v_200()
    r200 = 200

    # parameters with units for heights
    M_disk, R_disk = get_exponential_disk_params()

    # _c = (R_disk + bin_edges[2*_index+1]) * np.exp(-bin_edges[2*_index+1]/R_disk) + 0.5*(
    #         R_disk + bin_edges[2*_index]) * np.exp(-bin_edges[2*_index]/R_disk)
    # R_analytic_questionmark = -R_disk * (lambertw(-_c / (np.exp(1)*R_disk)) + 1).real

    # exp_int = lambda R: np.exp(- R / R_disk ) / R
    exp_int = lambda R: R * np.exp(- R / R_disk)
    # exp_int = lambda R: R**3 * np.exp(- R / R_disk )
    tot = quad(exp_int, bin_edges[2 * _index], bin_edges[2 * _index + 1])[0]
    zero = lambda R: quad(exp_int, bin_edges[2 * _index], R)[0] - 0.5 * tot
    R_mass_centre = brentq(zero, bin_edges[2 * _index], bin_edges[2 * _index + 1])

    R = R_mass_centre
    # R_mass_centre = R

    sigma_dm = get_analytic_hernquist_dispersion([R, R], vmax, a_h, save_name_append='diff')[0]
    v_c_halo = get_analytic_hernquist_circular_velocity([R, R], vmax, a_h, save_name_append='diff')[0]

    Sigma_R_star = get_exponential_surface_density(R, M_disk, R_disk, save_name_append='diff')

    # set up arrays
    elevenx = 101  # 101
    sigma_z2 = np.logspace(np.log10(0.03), np.log10(3 * sigma_dm ** 2), elevenx)

    z_thin_SG = np.array([get_thin_disk_SG_height(np.sqrt(s_z), R, Sigma_R_star, v_c_halo, v200, r200, a_h)
                          for s_z in sigma_z2])
    z_thin_NSG = np.array([get_thin_disk_NSG_height(np.sqrt(s_z), R, Sigma_R_star, v_c_halo, v200, r200, a_h)
                           for s_z in sigma_z2])
    z_thin_half = np.array([get_thin_disk_height(np.sqrt(s_z), R, Sigma_R_star, v_c_halo, v200, r200, a_h)
                            for s_z in sigma_z2])

    z_thick_NSG = np.array([get_thick_disk_NSG_height(np.sqrt(s_z), R, Sigma_R_star, v_c_halo, v200, r200, a_h)
                            for s_z in sigma_z2])

    z_thick_half = np.array([get_disk_height(np.sqrt(s_z), R, Sigma_R_star, v_c_halo, v200, r200, a_h)
                             for s_z in sigma_z2])

    # Round 2
    c_SG = 0.6159978007462151
    c_NSG = 0.5180308801010115

    c_on_a_thin_SG = z_thin_SG / (R / np.sqrt(2)) * 2 * c_SG / np.log(3)
    c_on_a_thin_NSG = z_thin_NSG / (R / np.sqrt(2)) * c_NSG / erfinv(0.5)

    c_on_a_thick_NSG = []
    for i in range(len(sigma_z2)):
        thick = lambda z: np.exp(1 / (sigma_z2[i] / v200 ** 2) * (
                1 / (np.sqrt((R / r200) ** 2 + (z / r200) ** 2) + a_h / r200) - 1 / (R / r200 + a_h / r200)))
        z_distribution = thick
        c_2 = z_thick_NSG[i] ** 2
        for _ in range(10):  # 2000):
            top_i = lambda z: 1 / (1 + z ** 2 / (2 * c_2)) * z ** 2 * z_distribution(z)
            bot_i = lambda z: 1 / (1 + z ** 2 / (2 * c_2)) * z_distribution(z)

            top = quad(top_i, 0, 1 * r200)[0]
            bot = quad(bot_i, 0, 1 * r200)[0]

            # c_2 = top / bot
            c_2 = np.divide(top, bot)

        # print(c_2)
        c_on_a_thick_NSG.append(np.sqrt(c_2) / (R / np.sqrt(2)))

    c_on_a_thin_half = np.sqrt(
        1 / (1 / c_on_a_thin_SG ** 2 + 1 / (2 * c_on_a_thin_SG * c_on_a_thin_NSG) + 1 / c_on_a_thin_NSG ** 2))

    c_on_a_thick_half = c_on_a_thin_half + (c_on_a_thick_NSG - c_on_a_thin_NSG)

    mask = ~np.isnan(c_on_a_thick_half)
    estimate = InterpolatedUnivariateSpline(sigma_z2[mask], c_on_a_thick_half[mask], check_finite=True)


    axs[0][1].errorbar(z_thin_NSG / R, c_on_a_thin_NSG,
                       ls='--', c='yellow', lw=3)
    axs[0][1].errorbar(z_thin_SG / R, c_on_a_thin_SG,
                       ls='-.', c='k', lw=2)
    axs[0][1].errorbar(z_thin_half/R, c_on_a_thin_half,
                       ls='--', c='darkorange', lw=3)

    axs[0][1].errorbar(z_thick_NSG / R, c_on_a_thick_NSG,
                       ls='-', c='darkgreen', lw=4)
    axs[0][1].errorbar(z_thick_half/R, c_on_a_thick_half,
                       ls='-', c='mediumblue', lw=3)

    axs[0][2].errorbar(sigma_z2 / v200 ** 2, c_on_a_thin_NSG,
                       ls='--', c='yellow', lw=3, label='thin NSG')
    axs[0][2].errorbar(sigma_z2 / v200 ** 2, c_on_a_thin_SG,
                       ls='-.', c='k', lw=2, label='thin SG')
    axs[0][2].errorbar(sigma_z2 / v200 ** 2, c_on_a_thin_half,
                       ls='--', c='darkorange', lw=3, label='thin')

    axs[0][2].errorbar(sigma_z2 / v200 ** 2, c_on_a_thick_NSG,
                       ls='-', c='darkgreen', lw=4, label='thick NSG')
    axs[0][2].errorbar(sigma_z2 / v200 ** 2, c_on_a_thick_half,
                       ls='-', c='mediumblue', lw=3, label='thick')

    axs[0][2].legend()

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = get_name_kwargs(galaxy_name)

        # time
        time = np.linspace(0, T_TOT, snaps + 1)
        time = time - time[BURN_IN]

        # load data
        (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
         ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges)

        a, b, c = load_galaxy_abc(galaxy_name, save_name_append, snaps, bin_edges)

        z_half = get_median_z(galaxy_name, save_name_append, snaps, bin_edges)
        R_half = get_R_half(galaxy_name, snaps)

        cu = 'C6'
        cd = 'C9'
        mu = 'v'
        md = '^'
        alpha = 0.2

        axs[0][1].errorbar(z_half[:, _index] / R_half, c[:, _index] / a[:, _index],
                           c=cd, alpha=1, marker=md, ls='', ms=2, zorder=-10, label=r'$c/a$')
        axs[0][1].errorbar(z_half[:, _index] / R_half, c[:, _index] / b[:, _index],
                           c=cu, alpha=1, marker=mu, ls='', ms=2, zorder=-10, label=r'$c/b$')
        axs[0][1].errorbar(z_half[:, _index] / R_half, c[:, _index] / a[:, _index],
                           c=cd, alpha=alpha, ls=kwargs['ls'], zorder=-10)
        axs[0][1].errorbar(z_half[:, _index] / R_half, c[:, _index] / b[:, _index],
                           c=cu, alpha=alpha, ls=kwargs['ls'], zorder=-10)

        axs[0][2].errorbar(sigma_z[:, _index] ** 2 / v200 ** 2, c[:, _index] / a[:, _index],
                           c=cd, alpha=1, marker=md, ls='', ms=2, zorder=-10, label=r'$c/a$')
        axs[0][2].errorbar(sigma_z[:, _index] ** 2 / v200 ** 2, c[:, _index] / b[:, _index],
                           c=cu, alpha=1, marker=mu, ls='', ms=2, zorder=-10, label=r'$c/b$')
        axs[0][2].errorbar(sigma_z[:, _index] ** 2 / v200 ** 2, c[:, _index] / a[:, _index],
                           c=cd, alpha=alpha, ls=kwargs['ls'], zorder=-10)
        axs[0][2].errorbar(sigma_z[:, _index] ** 2 / v200 ** 2, c[:, _index] / b[:, _index],
                           c=cu, alpha=alpha, ls=kwargs['ls'], zorder=-10)

        alpha = 0.2
        lw = 4
        window = snaps // 5 + 1
        poly_n = 5

        y = (c[:, _index] / a[:, _index] + c[:, _index] / b[:, _index]) / 2
        axs[0][0].errorbar(time, y,
                           c=kwargs['c'], marker='', ls='-', lw=lw, alpha=alpha)
        axs[0][0].errorbar(time, savgol_filter(y, window, poly_n),
                           c=kwargs['c'], marker='', ls=kwargs['ls'])

        # theory
        if kwargs['ls'] == '-':
            (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
             ) = get_physical_from_model(galaxy_name, snaps, save_name_append)

            axs[0][0].errorbar(time, estimate(theory_best_z2[_index, :]),
                               c='k', ls='-.', lw=3)

            #Checking what Haslbauer et al. 2022 should have done.
            # (t_c_dm, _, _, _, _) = get_constants([R, R], 'diff', kwargs['mdm'], galaxy_name=galaxy_name)
            #
            # q_0 = 0.035
            # tau_0 = - np.log(1 - q_0)
            # # q_0 = 1 - np.exp(-tau_0)
            #
            # # tau = time
            # # beta = 28 / t_c_dm
            # _beta = 28.5 * kwargs['mdm']
            # q_heat = (1 - np.exp(-_beta * time - tau_0))
            #
            # beta_ = 28.5  # * 1e-10
            # my_reheat = lambda q_0, time, mass: 1 + (q_0 - 1) * np.exp(-beta_ * time * mass)
            #
            # mean_time = T_TOT + time[0]  # Gyr
            # beta = 28.5 * mean_time  # * 1e-10
            # my_correction = lambda q_heat, mass: (1 - (1 - q_heat) * np.exp(beta * mass))

            # print()
            # print(1 - (1 - q_heat[-1]) * np.exp(_beta * time[-1]))
            # print(_beta)
            # print(time[-1])
            # print(np.exp(_beta*time[-1]))
            # print(my_correction(q_heat[-1]/1.2, kwargs['mdm']))
            # print(_beta)
            # print(time[-1])
            # print(np.exp(_beta*time[-1]))

            # axs[0][0].errorbar(time, q_0 * np.ones(len(time)), c='pink', ls='--')
            # axs[0][0].errorbar(time, q_heat, c='darkgreen', ls='--')
            # axs[0][0].errorbar(time, my_reheat(q_0, time, kwargs['mdm']), c='darkblue', ls='--')

    axs[0][1].errorbar([1e-3, 1e1], [1e-3, 1e1],
                       color='grey', ls='--', zorder=-10)

    axs[0][0].legend([(lines.Line2D([0, 1], [0, 1], color='k', ls='-.', lw=3))],
                     ['thick disk model'],
                     loc='upper left', labelspacing=0.1, markerscale=2, handlelength=2, frameon=False,
                     handletextpad=0.1, borderpad=0.2)

    axs[0][1].legend([(lines.Line2D([0, 1], [0, 1], color='C6', ls='', marker='v', ms=4)),
                      (lines.Line2D([0, 1], [0, 1], color='C9', ls='', marker='^', ms=4))],
                     [r'$c/a$', r'$c/b$'],
                     handler_map={tuple: HandlerTupleVertical(ndivide=5)},
                     # loc='upper left', numpoints=1, handlelength=1, handletextpad=0.2, frameon=False,
                     # labelspacing=-0.2, borderpad=0.2)
                     loc='upper left', frameon=False)

    axs[0][2].legend([(lines.Line2D([0, 1], [0, 1], color='yellow', ls='--', lw=3)),
                      (lines.Line2D([0, 1], [0, 1], color='k', ls='-.', lw=2)),
                      (lines.Line2D([0, 1], [0, 1], color='darkorange', ls='--', lw=4)),
                      (lines.Line2D([0, 1], [0, 1], color='darkgreen', ls='-', lw=3)),
                      (lines.Line2D([0, 1], [0, 1], color='mediumblue', ls='-', lw=3))],
                     ['thin NSG', 'thin SG', 'thin disk', 'thick NSG', 'thick disk'],
                     loc='lower right', labelspacing=-0.1, markerscale=2, handlelength=1, frameon=False,
                     handletextpad=0.1, borderpad=0.2, bbox_to_anchor=(1.042, -0.045))

    axs[0][0].set_ylabel(r'$c/a$')
    axs[0][0].set_xlabel(r'$t$ [Gyr]')
    axs[0][1].set_xlabel(r'$z_{1/2} / R_{1/2}$')
    axs[0][2].set_xlabel(r'$\sigma_z^2 / V_{200}^2$')

    axs[0][0].text(9.4, 2.5e-2, r'$[R_{1/2} \pm 0.1$dex]', ha='right', va='bottom')

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return


def plot_heights_again(galaxy_name_list, snaps_list, name='', save=False):
    _index = 1
    save_name_append = 'diff'

    # set up plots
    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(8, 8, forward=True)
    widths = [1, 1]
    heights = [1, 1]
    spec = fig.add_gridspec(ncols=2, nrows=2, width_ratios=widths, height_ratios=heights)

    fig.subplots_adjust(wspace=0.02, hspace=0.02)

    ylim1 = [0.012, 1.2] #c/a
    ylim0 = [0.01, 1] #z_1/2/R_1/2
    # ylim0[1] = ylim0[0] * ylim1[1] / ylim1[0]
    ylim2 = [1.2e-3, 1.2e0] #sigma_z / V_200

    axs = []
    for row in range(len(heights)):
        axs.append([])
        for col in range(len(widths)):
            axs[row].append(fig.add_subplot(spec[row, col]))
            axs[row][col].loglog()
            if col == 0:
                axs[row][col].set_xlim(ylim0)
            if col == 1:
                axs[row][col].set_xlim(ylim2)
            if row == 0:
                axs[row][col].set_ylim(ylim0)
            if row == 1:
                axs[row][col].set_ylim(ylim1)

            if row != 1: axs[row][col].set_xticklabels([])

    axs[1][1].set_yticklabels([])
    axs[0][0].set_visible(False)

    axs[1][0].set_xlabel(r'$z_{1/2} / R_{1/2, 0}$')
    axs[1][1].set_xlabel(r'$\sigma_z^2 / V_{200}^2$')
    axs[1][0].set_ylabel(r'$c/a (R = R_{1/2, 0})$')
    axs[0][1].set_ylabel(r'$z_{1/2} / R_{1/2, 0}$')

    # radius to investigate is half mass radius
    R = get_bin_edges(save_name_append='cum')[2 * _index + 1]
    bin_edges = get_bin_edges(save_name_append='diff')

    # halo stuff
    a_h, vmax = get_hernquist_params()
    v200 = get_v_200()
    r200 = 200

    # parameters with units for heights
    M_disk, R_disk = get_exponential_disk_params()

    # _c = (R_disk + bin_edges[2*_index+1]) * np.exp(-bin_edges[2*_index+1]/R_disk) + 0.5*(
    #         R_disk + bin_edges[2*_index]) * np.exp(-bin_edges[2*_index]/R_disk)
    # R_analytic_questionmark = -R_disk * (lambertw(-_c / (np.exp(1)*R_disk)) + 1).real

    # exp_int = lambda R: np.exp(- R / R_disk ) / R
    exp_int = lambda R: R * np.exp(- R / R_disk)
    # exp_int = lambda R: R**3 * np.exp(- R / R_disk )
    tot = quad(exp_int, bin_edges[2 * _index], bin_edges[2 * _index + 1])[0]
    zero = lambda R: quad(exp_int, bin_edges[2 * _index], R)[0] - 0.5 * tot
    R_mass_centre = brentq(zero, bin_edges[2 * _index], bin_edges[2 * _index + 1])

    R = R_mass_centre
    # R_mass_centre = R

    sigma_dm = get_analytic_hernquist_dispersion([R, R], vmax, a_h, save_name_append='diff')[0]
    v_c_halo = get_analytic_hernquist_circular_velocity([R, R], vmax, a_h, save_name_append='diff')[0]

    Sigma_R_star = get_exponential_surface_density(R, M_disk, R_disk, save_name_append='diff')

    # set up arrays
    elevenx = 101  # 101
    sigma_z2 = np.logspace(np.log10(0.03), np.log10(3 * sigma_dm ** 2), elevenx)

    z_thin_SG = np.array([get_thin_disk_SG_height(np.sqrt(s_z), R, Sigma_R_star, v_c_halo, v200, r200, a_h)
                          for s_z in sigma_z2])
    z_thin_NSG = np.array([get_thin_disk_NSG_height(np.sqrt(s_z), R, Sigma_R_star, v_c_halo, v200, r200, a_h)
                           for s_z in sigma_z2])
    z_thin_half = np.array([get_thin_disk_height(np.sqrt(s_z), R, Sigma_R_star, v_c_halo, v200, r200, a_h)
                            for s_z in sigma_z2])

    z_thick_NSG = np.array([get_thick_disk_NSG_height(np.sqrt(s_z), R, Sigma_R_star, v_c_halo, v200, r200, a_h)
                            for s_z in sigma_z2])

    z_thick_half = np.array([get_disk_height(np.sqrt(s_z), R, Sigma_R_star, v_c_halo, v200, r200, a_h)
                             for s_z in sigma_z2])

    # Round 2
    c_SG = 0.6159978007462151
    c_NSG = 0.5180308801010115

    c_on_a_thin_SG = z_thin_SG / (R / np.sqrt(2)) * 2 * c_SG / np.log(3)
    c_on_a_thin_NSG = z_thin_NSG / (R / np.sqrt(2)) * c_NSG / erfinv(0.5)

    c_on_a_thick_NSG = []
    for i in range(len(sigma_z2)):
        thick = lambda z: np.exp(1 / (sigma_z2[i] / v200 ** 2) * (
                1 / (np.sqrt((R / r200) ** 2 + (z / r200) ** 2) + a_h / r200) - 1 / (R / r200 + a_h / r200)))
        z_distribution = thick
        c_2 = z_thick_NSG[i] ** 2
        for _ in range(10):  # 2000):
            top_i = lambda z: 1 / (1 + z ** 2 / (2 * c_2)) * z ** 2 * z_distribution(z)
            bot_i = lambda z: 1 / (1 + z ** 2 / (2 * c_2)) * z_distribution(z)

            top = quad(top_i, 0, 1 * r200)[0]
            bot = quad(bot_i, 0, 1 * r200)[0]

            # c_2 = top / bot
            c_2 = np.divide(top, bot)

        # print(c_2)
        c_on_a_thick_NSG.append(np.sqrt(c_2) / (R / np.sqrt(2)))

    c_on_a_thin_half = np.sqrt(
        1 / (1 / c_on_a_thin_SG ** 2 + 1 / (2 * c_on_a_thin_SG * c_on_a_thin_NSG) + 1 / c_on_a_thin_NSG ** 2))

    c_on_a_thick_half = c_on_a_thin_half + (c_on_a_thick_NSG - c_on_a_thin_NSG)

    mask = ~np.isnan(c_on_a_thick_half)
    estimate = InterpolatedUnivariateSpline(sigma_z2[mask], c_on_a_thick_half[mask], check_finite=True)

    pth_b2 = [path_effects.Stroke(linewidth=2, foreground='black'), path_effects.Normal()]
    pth_b4 = [path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()]
    pth_b6 = [path_effects.Stroke(linewidth=6, foreground='black'), path_effects.Normal()]
    pth_b7 = [path_effects.Stroke(linewidth=7, foreground='black'), path_effects.Normal()]
    pth_w4 = [path_effects.Stroke(linewidth=4, foreground='white'), path_effects.Normal()]
    pth_g6 = [path_effects.Stroke(linewidth=6, foreground='green'), path_effects.Normal()]

    labels = ['thin SG', 'thin NSG', 'thin', 'thick NSG', 'thick']
    zs = [z_thin_SG, z_thin_NSG, z_thin_half, z_thick_NSG, z_thick_half]
    cas = [c_on_a_thin_SG, c_on_a_thin_NSG, c_on_a_thin_half, c_on_a_thick_NSG, c_on_a_thick_half]
    lss = ['-.', '-', (0,(3,2)), (0,(0.8,1)), '-']
    cs = ['royalblue', 'orange', 'mediumseagreen', 'crimson', '#4d0303'] #'maroon']
    lws = [1, 3, 6, 5, 3]
    pes = [pth_b2, pth_b4, pth_b7, pth_b6, pth_w4]

    for label, z, ca, ls, c, lw, pe in zip(labels, zs, cas, lss, cs, lws, pes):
        axs[0][1].errorbar(sigma_z2 / v200 ** 2, z / R,
                           ls=ls, c=c, lw=lw, label=label, path_effects=pe)
        axs[1][0].errorbar(z / R, ca,
                           ls=ls, c=c, lw=lw, label=label, path_effects=pe)
        axs[1][1].errorbar(sigma_z2 / v200 ** 2, ca,
                           ls=ls, c=c, lw=lw, label=label, path_effects=pe)

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = get_name_kwargs(galaxy_name)

        # time
        time = np.linspace(0, T_TOT, snaps + 1)
        time = time - time[BURN_IN]

        # load data
        (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
         ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges)

        a, b, c = load_galaxy_abc(galaxy_name, save_name_append, snaps, bin_edges)

        z_half = get_median_z(galaxy_name, save_name_append, snaps, bin_edges)
        R_half = get_bin_edges(save_name_append='cum')[2 * _index + 1]
        # R_half = get_R_half(galaxy_name, snaps)

        ca = 'C6'
        cb = 'C9'
        ma = '^'
        mb = 'v'
        alpha = 0.2
        alpha2 = 1 #(int(kwargs['c'][1]) + 1)/10 #0.4
        ms = 5

        axs[0][1].errorbar(sigma_z[:, _index] ** 2 / v200 ** 2, z_half[:, _index] / R_half,
                           c=ca, alpha=alpha2, marker=ma, ls='', ms=ms, zorder=-10, label=r'$c/a$')
        axs[0][1].errorbar(sigma_z[:, _index] ** 2 / v200 ** 2, z_half[:, _index] / R_half,
                           c=cb, alpha=alpha2, marker=mb, ls='', ms=ms, zorder=-11, label=r'$c/b$')
        axs[0][1].errorbar(sigma_z[:, _index] ** 2 / v200 ** 2, z_half[:, _index] / R_half,
                           c=ca, alpha=alpha, ls=kwargs['ls'], zorder=-10)
        axs[0][1].errorbar(sigma_z[:, _index] ** 2 / v200 ** 2, z_half[:, _index] / R_half,
                           c=cb, alpha=alpha, ls=kwargs['ls'], zorder=-11)

        axs[1][0].errorbar(z_half[:, _index] / R_half, c[:, _index] / a[:, _index],
                           c=ca, alpha=alpha2, marker=ma, ls='', ms=ms, zorder=-10, label=r'$c/a$')
        axs[1][0].errorbar(z_half[:, _index] / R_half, c[:, _index] / b[:, _index],
                           c=cb, alpha=alpha2, marker=mb, ls='', ms=ms, zorder=-11, label=r'$c/b$')
        axs[1][0].errorbar(z_half[:, _index] / R_half, c[:, _index] / a[:, _index],
                           c=ca, alpha=alpha, ls=kwargs['ls'], zorder=-10)
        axs[1][0].errorbar(z_half[:, _index] / R_half, c[:, _index] / b[:, _index],
                           c=cb, alpha=alpha, ls=kwargs['ls'], zorder=-11)

        axs[1][1].errorbar(sigma_z[:, _index] ** 2 / v200 ** 2, c[:, _index] / a[:, _index],
                           c=ca, alpha=alpha2, marker=ma, ls='', ms=ms, zorder=-10, label=r'$c/a$')
        axs[1][1].errorbar(sigma_z[:, _index] ** 2 / v200 ** 2, c[:, _index] / b[:, _index],
                           c=cb, alpha=alpha2, marker=mb, ls='', ms=ms, zorder=-11, label=r'$c/b$')
        axs[1][1].errorbar(sigma_z[:, _index] ** 2 / v200 ** 2, c[:, _index] / a[:, _index],
                           c=ca, alpha=alpha, ls=kwargs['ls'], zorder=-10)
        axs[1][1].errorbar(sigma_z[:, _index] ** 2 / v200 ** 2, c[:, _index] / b[:, _index],
                           c=cb, alpha=alpha, ls=kwargs['ls'], zorder=-11)

        # theory
        if kwargs['ls'] == '-':
            (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
             ) = get_physical_from_model(galaxy_name, snaps, save_name_append)

            # axs[0][0].errorbar(time, estimate(theory_best_z2[_index, :]),
            #                    c='k', ls='-.', lw=3)

            #Checking what Haslbauer et al. 2022 should have done.
            # (t_c_dm, _, _, _, _) = get_constants([R, R], 'diff', kwargs['mdm'], galaxy_name=galaxy_name)
            #
            # q_0 = 0.035
            # tau_0 = - np.log(1 - q_0)
            # # q_0 = 1 - np.exp(-tau_0)
            #
            # # tau = time
            # # beta = 28 / t_c_dm
            # _beta = 28.5 * kwargs['mdm']
            # q_heat = (1 - np.exp(-_beta * time - tau_0))
            #
            # beta_ = 28.5  # * 1e-10
            # my_reheat = lambda q_0, time, mass: 1 + (q_0 - 1) * np.exp(-beta_ * time * mass)
            #
            # mean_time = T_TOT + time[0]  # Gyr
            # beta = 28.5 * mean_time  # * 1e-10
            # my_correction = lambda q_heat, mass: (1 - (1 - q_heat) * np.exp(beta * mass))

            # print()
            # print(1 - (1 - q_heat[-1]) * np.exp(_beta * time[-1]))
            # print(_beta)
            # print(time[-1])
            # print(np.exp(_beta*time[-1]))
            # print(my_correction(q_heat[-1]/1.2, kwargs['mdm']))
            # print(_beta)
            # print(time[-1])
            # print(np.exp(_beta*time[-1]))

            # axs[0][0].errorbar(time, q_0 * np.ones(len(time)), c='pink', ls='--')
            # axs[0][0].errorbar(time, q_heat, c='lawngreen', ls='--')
            # axs[0][0].errorbar(time, my_reheat(q_0, time, kwargs['mdm']), c='darkblue', ls='--')

    if False:
        axs[1][0].text(0.1, 0.075, '1:1', c='gray',
                  rotation=np.arctan(1 / 1) * 180 / np.pi)  # angle0)
        axs[1][0].errorbar([1e-3, 1e1], [1e-3, 1e1],
                           color='grey', ls='--', zorder=-10)

    axs[1][0].legend([(lines.Line2D([0, 1], [0, 1], color='C9', ls='', marker='v', ms=4)),
                      (lines.Line2D([0, 1], [0, 1], color='C6', ls='', marker='^', ms=4))],
                     [r'$c/b$', r'$c/a$'],
                     # handler_map={tuple: HandlerTupleVertical(ndivide=5)},
                     loc='upper left', labelspacing=0.1, handlelength=1, frameon=False,
                     handletextpad=0.2, borderpad=0.2)
                     # loc='upper left', frameon=False)

    axs[1][1].legend([(lines.Line2D([0, 1], [0, 1], ls=ls, c=c, lw=lw, path_effects=pe))
                      for ls, c, lw, pe in zip(lss, cs, lws, pes)],
                     labels,
                     loc='lower right', labelspacing=0.1, markerscale=2, handlelength=1.2, frameon=False,
                     handletextpad=0.3, borderpad=0.2, bbox_to_anchor=(1.042, -0.045))

    # axs[1][1].legend([(lines.Line2D([0, 1], [0, 1], ls='-.', c='k', lw=2)),
    #                   (lines.Line2D([0, 1], [0, 1], ls='-', c='gold', lw=3, path_effects=pth_b4)),
    #                   (lines.Line2D([0, 1], [0, 1], ls=(0,(3,2)), c='lawngreen', lw=5, path_effects=pth_g6)),
    #                   (lines.Line2D([0, 1], [0, 1], ls=(0,(0.8,1)), c='tomato', lw=6)),
    #                   (lines.Line2D([0, 1], [0, 1], ls='-', c='mediumblue', lw=3))],
    #                  ['thin SG', 'thin NSG', 'thin disk', 'thick NSG', 'thick disk'],
    #                  loc='lower right', labelspacing=0.1, markerscale=2, handlelength=1.2, frameon=False,
    #                  handletextpad=0.3, borderpad=0.2, bbox_to_anchor=(1.042, -0.045))

    # axs[1][0].text(9.4, 2.5e-2, r'$[R_{1/2} \pm 0.1$dex]', ha='right', va='bottom')

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return


def calculate_shape_integral(z_0=1, SG=False, NSG_thin=False, NSG_thick=False, sigma_z_on_dm_2=0.1,
                             hundred=10, mc_neval=4e4, mc_err_tol=0.005, overwrite=False):
    file_name = ('integrals/calculate_shape_integral' + 'SG' * SG + 'NSG_thin' * NSG_thin +
                 'NSG_thick' * NSG_thick + 'z_0' + str(z_0) + 'szdm2' + str(sigma_z_on_dm_2) +
                 'r' + str(hundred) + 'ev' + str(mc_neval) + 'tol' + str(mc_err_tol))

    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        out = np.load(file_name + '.npy')

    else:
        # # scale
        # z_0 = 1

        R_d = 4.15

        # integration bounds need to be bigish
        large = z_0 * 1e2

        dm_frac = 1 - 0.01
        # sigma_z_on_dm_2 = 0.1
        R_cyl = 10
        a = 30
        r200 = 200

        # intial guess
        c_2 = (z_0 / R_d) ** 2

        # epsilon = 1e-4

        thick = lambda z: np.exp(1 / (dm_frac * sigma_z_on_dm_2) * (
                1 / (np.sqrt((R_cyl / r200) ** 2 + (z / r200) ** 2) + a / r200) - 1 / (R_cyl / r200 + a / r200)))

        if SG:
            z_distribution = lambda z: 1 / np.cosh(z / z_0) ** 2
        elif NSG_thin:
            z_distribution = lambda z: np.exp(-(z / z_0) ** 2)
        elif NSG_thick:
            z_distribution = thick
        else:
            print('Warning: No vertical profile selected.')
            return

        R_distribution = lambda x, y: np.exp(-np.sqrt(x ** 2 + y ** 2) / R_d)

        bin_max_2 = large
        volume = lambda x, y: x ** 2 + y ** 2 < bin_max_2

        print('intial (c/a)^2', c_2)
        print('intial c/a', np.sqrt(c_2))
        print()

        for _ in range(hundred):

            one_on_ellpitical_distance_2 = lambda x, y, z: 1 / (x ** 2 + y ** 2 + z ** 2 / (c_2))

            top_zz_i = lambda x: one_on_ellpitical_distance_2(x[0], x[1], x[2]) * x[2] ** 2 * z_distribution(
                x[2]) * R_distribution(x[0], x[1]) * volume(x[0], x[1])
            top_xx_i = lambda x: one_on_ellpitical_distance_2(x[0], x[1], x[2]) * x[0] ** 2 * z_distribution(
                x[2]) * R_distribution(x[0], x[1]) * volume(x[0], x[1])
            bot_i = lambda x: one_on_ellpitical_distance_2(x[0], x[1], x[2]) * z_distribution(
                x[2]) * R_distribution(x[0], x[1]) * volume(x[0], x[1])

            while True:
                integ = vegas.Integrator([[0, np.sqrt(bin_max_2)], [0, np.sqrt(bin_max_2)], [0, large]])
                result = integ(top_zz_i, nitn=15, neval=mc_neval)  # nitn=10, neval=3e4)
                top_zz = result.mean
                error = result.sdev
                if np.divide(error, top_zz) < mc_err_tol:
                    break
                else:
                    print('Failed with')
                    # print('top_zz', top_zz)
                    print('frac error top_zz', error / top_zz)

            # print('top_zz', top_zz)
            print('frac error top_zz', error / top_zz)

            while True:
                integ = vegas.Integrator([[0, np.sqrt(bin_max_2)], [0, np.sqrt(bin_max_2)], [0, large]])
                result = integ(top_xx_i, nitn=15, neval=mc_neval)  # nitn=10, neval=3e4)
                top_xx = result.mean
                error = result.sdev
                if np.divide(error, top_xx) < mc_err_tol:
                  break
                else:
                    print('Failed with')
                    # print('top_xx', top_xx)
                    print('frac error top_xx', error / top_xx)
            # print('top_xx', top_xx)
            print('frac error top_xx', error / top_xx)

            c_2 = top_zz / top_xx

            print('(c/a)^2', c_2)
            print('c/a', np.sqrt(c_2))
            print()

        while True:
            integ = vegas.Integrator([[0, np.sqrt(bin_max_2)], [0, np.sqrt(bin_max_2)], [0, large]])
            result = integ(bot_i, nitn=15, neval=mc_neval)  # nitn=10, neval=3e4)
            bot = result.mean
            error = result.sdev
            if np.divide(error, bot) < mc_err_tol:
                break
            else:
                print('Failed with')
                # print('bot', bot)
                print('frac error bot', error / bot)
        # print('bot', bot)
        print('frac error bot', error / bot)

        print()
        print('M_zz =', top_zz / bot)
        print('M_xx =', top_xx / bot)
        print('sqrt M_zz =', np.sqrt(top_zz / bot))
        print('sqrt M_xx =', np.sqrt(top_xx / bot))

        print()
        print('c/a', np.sqrt(top_zz / top_xx))
        print()

        out = np.sqrt(c_2)

        np.save(file_name, out)

    return (out)


def calculate_heated_profile_shape(tau, SG=False, NSG_thin=False, NSG_thick=False,
                                   hundred=10, mc_neval=4e4, mc_err_tol=0.005, overwrite=False):
    file_name = ('integrals/calculate_heated_profile_shape' + 'SG' * SG + 'NSG_thin' * NSG_thin +
                 'NSG_thick' * NSG_thick + 'tau' + str(tau) +
                 'r' + str(hundred) + 'ev' + str(mc_neval) + 'tol' + str(mc_err_tol))

    if not overwrite and os.path.isfile(file_name + '.npy'):
        # load
        out = np.load(file_name + '.npy')

    else:
        # fits for comparison
        print('\nbeta=0, gamma=0 (fiducial)')
        (ln_Lambda_ks, alphas, betas, gammas
         ) = find_least_log_squares_best_fit(
          save_name_append, mcmc=False, fixed_b=0, fixed_g=0)

        M_disk, R_d = get_exponential_disk_params()
        a_h, vmax = get_hernquist_params()
        R_d = 4.15

        # burn ins
        interp_inital_velocity2_z = smooth_z_zero_snap_interpolator()

        return ()

        def z_distribution(x, y, z):
            R = np.sqrt(x ** 2 + y ** 2)

            (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
             ) = get_constants([R] * 2, save_name_append, 1e-4)

            analytic_dispersion = get_velocity_scale([R] * 2, 0, 0, 0, 0, 0, 0,
                                                     analytic_dispersion=True, save_name_append='diff')

            # burn ins
            inital_velocity2_z = interp_inital_velocity2_z(R)

            theory_best_z2 = get_theory_velocity2_array(
                analytic_dispersion ** 2, inital_velocity2_z,
                np.array([t_c_dm[0] * tau]), t_c_dm[0], delta_dm[0], upsilon_dm[0],
                ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])

            if SG:
                Sigma_R_star = get_exponential_surface_density(R, M_disk, R_d, save_name_append='diff')
                z_median = get_thin_disk_SG_height(np.sqrt(theory_best_z2), R, Sigma_R_star, 0, 0, 0, 0)
                z_0 = z_median * 2 / np.log(3)

                return (1 / np.cosh(z / z_0) ** 2)

            elif NSG_thin:
                v_c_halo = get_analytic_hernquist_circular_velocity([R] * 2, vmax, a_h, save_name_append='diff')
                z_median = get_thin_disk_NSG_height(np.sqrt(theory_best_z2), R, 0, v_c_halo, 0, 0, 0)
                z_0 = z_median / erfinv(0.5)

                return (np.exp(-(z / z_0) ** 2))

            elif NSG_thick:
                dm_frac = 1 - 0.01
                R_cyl = 10
                a = 30
                r200 = 200
                sigma_z_on_dm_2 = theory_best_z2 / analytic_dispersion ** 2

                return (np.exp(1 / (dm_frac * sigma_z_on_dm_2) * (
                        1 / (np.sqrt((R_cyl / r200) ** 2 + (z / r200) ** 2) + a / r200) - 1 / (
                            R_cyl / r200 + a / r200))))

            else:
                print('Warning: No vertical profile selected.')
                return

        # integration bounds need to be bigish
        large = 200

        # intial guess
        c_2 = 0.5

        R_distribution = lambda x, y: np.exp(-np.sqrt(x ** 2 + y ** 2) / R_d)

        bin_max_2 = large
        volume = lambda x, y: x ** 2 + y ** 2 < bin_max_2

        print('intial (c/a)^2', c_2)
        print('intial c/a', np.sqrt(c_2))
        print()

        for _ in range(hundred):

            one_on_ellpitical_distance_2 = lambda x, y, z: 1 / (x ** 2 + y ** 2 + z ** 2 / (c_2))

            top_zz_i = lambda x: one_on_ellpitical_distance_2(x[0], x[1], x[2]) * x[2] ** 2 * z_distribution(
                x[0], x[1], x[2]) * R_distribution(x[0], x[1]) * volume(x[0], x[1])
            top_xx_i = lambda x: one_on_ellpitical_distance_2(x[0], x[1], x[2]) * x[0] ** 2 * z_distribution(
                x[0], x[1], x[2]) * R_distribution(x[0], x[1]) * volume(x[0], x[1])
            bot_i = lambda x: one_on_ellpitical_distance_2(x[0], x[1], x[2]) * z_distribution(
                x[0], x[1], x[2]) * R_distribution(x[0], x[1]) * volume(x[0], x[1])

            while True:
                integ = vegas.Integrator([[0, np.sqrt(bin_max_2)], [0, np.sqrt(bin_max_2)], [0, large]])
                result = integ(top_zz_i, nitn=15, neval=mc_neval)  # nitn=10, neval=3e4)
                top_zz = result.mean
                error = result.sdev
                if np.divide(error, top_zz) < mc_err_tol:
                    break
                else:
                    print('Failed with')
                    # print('top_zz', top_zz)
                    print('frac error top_zz', error / top_zz)

            # print('top_zz', top_zz)
            print('frac error top_zz', error / top_zz)

            while True:
                integ = vegas.Integrator([[0, np.sqrt(bin_max_2)], [0, np.sqrt(bin_max_2)], [0, large]])
                result = integ(top_xx_i, nitn=15, neval=mc_neval)  # nitn=10, neval=3e4)
                top_xx = result.mean
                error = result.sdev
                if np.divide(error, top_xx) < mc_err_tol:
                    break
                else:
                    print('Failed with')
                    # print('top_xx', top_xx)
                    print('frac error top_xx', error / top_xx)
            # print('top_xx', top_xx)
            print('frac error top_xx', error / top_xx)

            c_2 = top_zz / top_xx

            print('(c/a)^2', c_2)
            print('c/a', np.sqrt(c_2))
            print()

        while True:
            integ = vegas.Integrator([[0, np.sqrt(bin_max_2)], [0, np.sqrt(bin_max_2)], [0, large]])
            result = integ(bot_i, nitn=15, neval=mc_neval)  # nitn=10, neval=3e4)
            bot = result.mean
            error = result.sdev
            if np.divide(error, bot) < mc_err_tol:
                break
            else:
                print('Failed with')
                # print('bot', bot)
                print('frac error bot', error / bot)
        # print('bot', bot)
        print('frac error bot', error / bot)

        print()
        print('M_zz =', top_zz / bot)
        print('M_xx =', top_xx / bot)
        print('sqrt M_zz =', np.sqrt(top_zz / bot))
        print('sqrt M_xx =', np.sqrt(top_xx / bot))

        print()
        print('c/a', np.sqrt(top_zz / top_xx))
        print()

        out = np.sqrt(c_2)

        np.save(file_name, out)

    return (out)


def plot_integrated_shapes(galaxy_name_list, snaps_list):
    plt.figure()

    _index = 1
    save_name_append = 'diff'

    # plt.figure()

    # radius to investigate is half mass radius
    R = get_bin_edges(save_name_append='cum')[2 * _index + 1]
    bin_edges = get_bin_edges(save_name_append='diff')

    # halo stuff
    a_h, vmax = get_hernquist_params()
    v200 = get_v_200()
    r200 = 200

    # parameters with units for heights
    M_disk, R_disk = get_exponential_disk_params()

    # _c = (R_disk + bin_edges[2*_index+1]) * np.exp(-bin_edges[2*_index+1]/R_disk) + 0.5*(
    #         R_disk + bin_edges[2*_index]) * np.exp(-bin_edges[2*_index]/R_disk)
    # R_analytic_questionmark = -R_disk * (lambertw(-_c / (np.exp(1)*R_disk)) + 1).real

    R_min = 0
    R_max = 30

    # exp_int = lambda R: np.exp(- R / R_disk ) / R
    exp_int = lambda R: R * np.exp(- R / R_disk)
    tot = quad(exp_int, R_min, R_max)[0]
    zero = lambda R: quad(exp_int, R_min, R)[0] - 0.5 * tot
    R_mass_centre = brentq(zero, R_min, R_max)

    R = R_mass_centre
    print(R)

    # sigma_dm = get_analytic_hernquist_dispersion([R,R], vmax, a_h, save_name_append='diff')[0]
    # v_c_halo = get_analytic_hernquist_circular_velocity([R,R], vmax, a_h, save_name_append='diff')[0]
    #
    # Sigma_R_star = get_exponential_surface_density(R, M_disk, R_disk, save_name_append='diff')
    #
    # #set up arrays
    # elevenx = 101
    # sigma_z2 = np.logspace(np.log10(0.001), np.log10(2*sigma_dm**2), elevenx)
    #
    # z_thin_SG = np.array([get_thin_disk_SG_height(np.sqrt(s_z), R, Sigma_R_star, v_c_halo, v200, r200, a_h)
    #                       for s_z in sigma_z2])
    # z_thin_NSG = np.array([get_thin_disk_NSG_height(np.sqrt(s_z), R, Sigma_R_star, v_c_halo, v200, r200, a_h)
    #                        for s_z in sigma_z2])
    # z_thin_half = np.array([get_thin_disk_height(np.sqrt(s_z), R, Sigma_R_star, v_c_halo, v200, r200, a_h)
    #                         for s_z in sigma_z2])
    #
    # z_thick_NSG = np.array([get_thick_disk_NSG_height(np.sqrt(s_z), R, Sigma_R_star, v_c_halo, v200, r200, a_h)
    #                         for s_z in sigma_z2])
    #
    # z_thick_half = np.array([get_disk_height(np.sqrt(s_z), R, Sigma_R_star, v_c_halo, v200, r200, a_h)
    #                          for s_z in sigma_z2])

    R_half = get_bin_edges(save_name_append='cum')[3]
    ### numerical integrals
    z_0s = [1 / 16, 1 / 8, 1 / 4, 1 / 2, 1, 2, 8]
    # z_0s = [1/8, 1/2, 2, 8]

    c_on_a_SG = [calculate_shape_integral(z_0=z_i, SG=True,
                                          hundred=10, mc_neval=4e4, mc_err_tol=0.005)
                 for z_i in z_0s]
    c_on_a_NSG = [calculate_shape_integral(z_0=z_i, NSG_thin=True,
                                           hundred=10, mc_neval=4e4, mc_err_tol=0.005)
                  for z_i in z_0s]

    sigma_ratios = [0.01 ** 2, 0.02 ** 2, 0.04 ** 2, 0.08 ** 2, 0.16 ** 2, 0.32 ** 2, 0.64 ** 2]

    # c_on_a_NSG_thick = [calculate_shape_integral(sigma_z_on_dm_2=s_r, NSG_thick=True,
    #                                              hundred=10, mc_neval=4e4, mc_err_tol=0.005)
    #                     for s_r in sigma_ratios]

    plt.errorbar(z_0s / R_half, c_on_a_SG, ls='-.', c='C0', label='thin SG')
    plt.errorbar(z_0s / R_half, c_on_a_NSG, ls='--', c='C1', label='thin NSG')
    # plt.errorbar(sigma_ratios, c_on_a_NSG_thick, ls='-', c='C3', label='thick NSG')

    # not meant to be used
    bin_edges = get_bin_edges(save_name_append='diff')

    for galaxy_name, snaps in zip(galaxy_name_list, snaps_list):
        kwargs = get_name_kwargs(galaxy_name)

        # #load data
        # (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
        #   ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges)

        z_half = get_median_z(galaxy_name, save_name_append, snaps, bin_edges)

        a, b, c = load_galaxy_abc(galaxy_name, '', snaps, [0, 30])

        plt.errorbar(z_half[:, 1] / R_half, c / a,
                     c=kwargs['c'], alpha=0.1, marker=kwargs['marker'], ls='', ms=3, zorder=-10)

    plt.xlabel(r'$z_0/R_{1/2}$')
    plt.ylabel(r'$c/a$')

    plt.legend()

    # plt.figure()
    #
    # log_taus = [-4, -3, -2, -1]
    #
    # c_on_a_SG = [calculate_heated_profile_shape(tau=10**ltaui, SG=True,
    #                                       hundred=10, mc_neval=1e4, mc_err_tol=0.02)
    #              for ltaui in log_taus]
    # c_on_a_NSG = [calculate_heated_profile_shape(tau=ltaui, NSG_thin=True,
    #                                        hundred=10, mc_neval=1e4, mc_err_tol=0.02)
    #              for ltaui in log_taus]
    #
    # plt.errorbar(log_taus, c_on_a_SG,  ls='-.', c='C0', label='thin SG')
    # plt.errorbar(log_taus, c_on_a_NSG, ls='--', c='C1', label='thin NSG')
    #
    # for galaxy_name, snaps in zip(galaxy_name_list, snaps_list):
    #   kwargs = get_name_kwargs(galaxy_name)
    #
    #   (t_c_dm , t_c_circ, delta_dm, upsilon_dm, upsilon_circ
    #    ) = get_constants(bin_edges, save_name_append, kwargs['mdm'])
    #
    #   time = np.linspace(0, T_TOT, snaps+1)
    #   time = time - time[BURN_IN]
    #
    #   tau = get_tau_array(time, t_c_dm[1])
    #
    #   a, b, c = load_galaxy_abc(galaxy_name, '', snaps, [0,30])
    #
    #   plt.errorbar(tau, c/a,
    #                c=kwargs['c'], alpha=0.1, marker=kwargs['marker'], ls='', ms=3, zorder=-10)
    #
    # plt.xlabel(r'$tau(R_{1/2})$')
    # plt.ylabel(r'$c/a$')
    #
    # plt.legend()

    return


def traditional_v_on_s_from_shape(ellipticity, delta, alpha=0):
    '''
    '''
    s = 1 - ellipticity

    # Thob et al. 2019 equation 5b
    W_xx_on_W_zz = 1 / (2 * s ** 2) * (np.arccos(s) - s * np.sqrt(1 - s ** 2)) / (
            np.sqrt(1 - s ** 2) / s - np.arccos(s))

    # Binney 2005 equation 26
    v_2_on_s0_2 = ((1 - delta) * W_xx_on_W_zz - 1) / (alpha * (1 - delta) * W_xx_on_W_zz + 1)

    return (np.sqrt(v_2_on_s0_2))


# @lru_cache(maxsize=10000)
def my_ellipticity_from_v_on_s(elevenx=101, eleveny=7, _index=1):
    '''Calculate ellipticity using disk heights from Luwlow et al. (2021)
    '''
    R = get_bin_edges(save_name_append='cum')[2 * _index + 1]
    bin_edges = get_bin_edges(save_name_append='diff')

    # load halo properties
    a_h, vmax = get_hernquist_params()
    sigma_dm = get_analytic_hernquist_dispersion([R, R], vmax, a_h, save_name_append='diff')[0]
    v_c_halo = get_analytic_hernquist_circular_velocity([R, R], vmax, a_h, save_name_append='diff')[0]

    v_c_total = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0,
                                   v200=False, dm_dispersion=False, dm_v_circ=False,
                                   analytic_dispersion=False, analytic_v_c=True,
                                   save_name_append='diff')[_index]

    v200 = get_v_200()
    r200 = get_r_200()

    # parameters with units for heights
    M_disk, R_disk = get_exponential_disk_params()
    Sigma_R_star = get_exponential_surface_density(R, M_disk, R_disk, save_name_append='diff')

    halo_phi = hernquist_potential(R)
    disk_phi = get_exponential_disk_potential(R)

    total_phi = halo_phi + disk_phi

    mean_div_c = 1.0863912206488044

    # set up arrays
    # need better bins
    # maximum_energy = 3*sigma_dm**2
    # maximum_energy = v_c_total**2
    maximum_energy = -total_phi / 2

    # zero = lambda s_z: get_disk_height(s_z, R, Sigma_R_star, v_c_halo, v200, r200, a_h
    #                                    ) - R * mean_div_c / np.sqrt(2)
    #
    # s_z_max = brentq(zero, 1, 3*sigma_dm)
    #
    # maximum_energy = 3 * s_z_max**2

    # mean_v_phi2 = np.reshape(np.repeat(np.logspace(-2, np.log10(maximum_energy) -1e-2, elevenx),
    #                                    eleveny), (elevenx, eleveny)).T

    mean_v_phi2 = np.reshape(np.repeat(np.linspace(0.01, maximum_energy - 1e-2, elevenx),
                                       eleveny), (elevenx, eleveny)).T

    beta = np.linspace(0, (eleveny - 1) / 10, eleveny)

    # need to triple check this line

    sigma_t2 = maximum_energy - mean_v_phi2

    sigma_z2 = (1 - beta[:, np.newaxis]) * sigma_t2 / 2

    # sigma_total2 = maximum_energy - mean_v_phi2
    #
    # sigma_z2 = sigma_total2 / (1 + 2/(1 - beta[:, np.newaxis]))
    # sigma_t2 = sigma_total2 / (1 + 0.5*(1 - beta[:, np.newaxis]))

    # # remove non physical points
    # sigma_z2[np.logical_or(mean_v_phi2 < -1e-10, mean_v_phi2 > 2*maximum_energy+1e-10)] = np.nan
    # sigma_t2[np.logical_or(mean_v_phi2 < -1e-10, mean_v_phi2 > 2*maximum_energy+1e-10)] = np.nan
    # mean_v_phi2[np.logical_or(mean_v_phi2 < -1e-10, mean_v_phi2 > 2*maximum_energy+1e-10)] = np.nan

    # the quantities we actually care about
    beta = 1 - 2 * sigma_z2 / sigma_t2

    # v_on_sigma = np.sqrt(mean_v_phi2) / np.sqrt(sigma_z2 + sigma_t2) * np.sqrt(3)
    v_on_sigma = np.sqrt(mean_v_phi2) / np.sqrt(sigma_z2 + sigma_t2) * np.sqrt(3)

    lambda_R = np.array([[edge_on_razor_thin_ring_lambda_r(
        np.sqrt(mean_v_phi2[i, k]), np.sqrt(sigma_t2[i, k] / 2), bin_edges[2 * _index], bin_edges[2 * _index + 1], 0.1)
        for k in range(elevenx)] for i in range(eleveny)])

    z_disk_half = np.array([[get_disk_height(np.sqrt(s_z), R, Sigma_R_star, v_c_halo, v200, r200, a_h)
                             for s_z in sig_z] for sig_z in sigma_z2])

    ellipticity = 1 - z_disk_half * mean_div_c / R / np.sqrt(2)

    # fix weird ploting
    ellipticity[ellipticity < 0] = np.nan

    return (ellipticity, v_on_sigma, lambda_R, beta)


def plot_shape_plot(galaxy_name_list, snaps_list,
                    save_name_append='cum', name='', save=False):
    # _index = 1
    # bin_edges = get_bin_edges(save_name_append)
    #
    # plt.figure()
    #
    # for galaxy_name, snaps in zip(galaxy_name_list, snaps_list):
    #   kwargs = get_name_kwargs(galaxy_name)
    #
    #   (mean_v_R, mean_v_phi,
    #    sigma_z, sigma_R, sigma_phi
    #    ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges)
    #
    #   v_on_sigma = mean_v_phi[:,_index] / np.sqrt((
    #     sigma_z[:,_index]**2 + sigma_R[:,_index]**2 + sigma_phi[:,_index]**2)/3)
    #
    #   time = np.linspace(0, T_TOT, snaps + 1)
    #   time = time - time[BURN_IN]
    #
    #   beta = 1 - (2*sigma_z[:,_index]**2)/(sigma_phi[:,_index]**2 + sigma_R[:,_index]**2)
    #   plt.errorbar(v_on_sigma, beta, c=kwargs['c'], alpha=0.2)
    #   plt.errorbar(v_on_sigma, savgol_filter(beta, snaps//5+1, 5),
    #                c=kwargs['c'], ls=kwargs['ls'])
    #
    #   # n_betas = 7
    #   # one_o_one = 101
    #   # (ellipticity, v_on_sigma, lambda_R, beta
    #   #  ) = my_ellipticity_from_v_on_s(one_o_one, n_betas, _index=_index)
    #   #
    #   # for i in range(n_betas):
    #   #   plt.errorbar(v_on_sigma[i, :], beta[i, :], c='k', ls=':')
    #
    # plt.xlabel('v/sigma')
    # plt.ylabel('beta')

    # plt.savefig('../galaxies/betas', bbox_inches='tight')

    _index = 1

    fig = plt.figure()
    fig.set_size_inches(4, 7, forward=True)
    ax0 = plt.subplot(2, 1, 1)
    ax1 = plt.subplot(2, 1, 2)

    fig.subplots_adjust(hspace=0.01, wspace=0.01)

    if save_name_append == 'diff':
        ax0.text(0.02, 7, r'$[R_{1/2} \pm 0.1$dex]')

        # rcParams['font.size'] = 15
        # plt.text(0,    0.32, r'$\beta = 0$')
        # plt.text(0.11, 0.32, r'$0.1$')
        # plt.text(0.18, 0.32, r'$0.2$')
        # plt.text(0.26, 0.32, r'$0.3$')
        # plt.text(0.34, 0.32, r'$0.4$')
        # plt.text(0.42, 0.32, r'$0.5$')
        # plt.text(0.5,  0.32, r'$0.6$')
        # rcParams['font.size'] = 20

        ax0.semilogy()
        # ax0.set_yticks([0.3, 1, 3, 10])
        # ax0.set_yticklabels([0.3, 1, 3, 10])

        # plt.ylim(0,15)
        ax0.set_ylim(3e-1, 1.2e1)

    ax1.set_ylim(0.167, 1.01)
    ax0.set_xlim(0, 1)
    ax1.set_xlim(0, 1)

    ax0.set_xticklabels([])

    ax0.set_ylabel(r'$\overline{v_\phi} / (\sigma_{tot} / \sqrt{3}) $')
    ax1.set_ylabel(r'$\lambda_R$')
    ax1.set_xlabel(r'$\epsilon = 1 - c/a$')

    R_measure = get_bin_edges(save_name_append='cum')[2 * _index + 1]
    # load galaxys properties
    a_h, vmax = get_hernquist_params()
    sigma_dm = get_analytic_hernquist_dispersion([R_measure, R_measure], vmax, a_h, save_name_append='diff')[0]
    v_c_halo = get_analytic_hernquist_circular_velocity([R_measure, R_measure], vmax, a_h, save_name_append='diff')[0]
    v200 = get_v_200()
    r200 = get_r_200()
    M_disk, R_disk = get_exponential_disk_params()
    Sigma_R_star = get_exponential_surface_density(R_measure, M_disk, R_disk, save_name_append='diff')
    z_with_sigma_max = get_disk_height(sigma_dm, R_measure, Sigma_R_star, v_c_halo, v200, r200, a_h)

    # data
    bin_edges = get_bin_edges(save_name_append)

    for galaxy_name, snaps in zip(galaxy_name_list, snaps_list):
        kwargs = get_name_kwargs(galaxy_name)

        # for lambda_R
        not_low_res = True
        if kwargs['marker'] == 's' and kwargs['c'] == 'C0':
            not_low_res = False
        if kwargs['marker'] == 's' and kwargs['c'] == 'C1':
            not_low_res = False
        if kwargs['marker'] == 'o' and kwargs['c'] == 'C0':
            not_low_res = False

        (mean_v_R, mean_v_phi,
         sigma_z, sigma_R, sigma_phi
         ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges)

        v_on_sigma = mean_v_phi[:, _index] / np.sqrt((
                                                             sigma_z[:, _index] ** 2 + sigma_R[:,
                                                                                       _index] ** 2 + sigma_phi[:,
                                                                                                      _index] ** 2) / 3)

        lambda_R = get_lambda_R(galaxy_name, save_name_append, snaps, bin_edges)

        # a, b, c = load_galaxy_abc(galaxy_name, '', snaps, [0,30])
        a, b, c = load_galaxy_abc(galaxy_name, save_name_append, snaps, bin_edges)
        # a[a == 1] = np.nan

        ellipticity = 1 - c[:, _index] / a[:, _index]

        # plot
        alpha = 0.2
        lw = 4
        window = snaps // 5 + 1
        poly_n = 5

        if not_low_res:
            x = ellipticity
            y = v_on_sigma
            ax0.errorbar(x, y,
                         c=kwargs['c'], marker='', ls='-', lw=lw, alpha=alpha)
            ax0.errorbar(savgol_filter(x, window, poly_n), savgol_filter(y, window, poly_n),
                         c=kwargs['c'], marker='', ls=kwargs['ls'])

            y = lambda_R[:, _index]
            ax1.errorbar(x, y,
                         c=kwargs['c'], marker='', ls='-', lw=lw, alpha=alpha)
            ax1.errorbar(savgol_filter(x, window, poly_n), savgol_filter(y, window, poly_n),
                         c=kwargs['c'], marker='', ls=kwargs['ls'])

        # theory
        if kwargs['ls'] == '-' and kwargs['c'] == 'C0':
            ####
            (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
             ) = get_physical_from_model(galaxy_name, snaps, save_name_append)

            time = np.linspace(0, T_TOT, snaps + 1)
            time = time - time[BURN_IN]

            theory_best_tot2 = theory_best_z2 + theory_best_r2 + theory_best_p2

            theory_best_v_on_sigma = theory_best_v / np.sqrt(theory_best_tot2 / 3)

            theory_best_simga_disk = np.sqrt((theory_best_r2 + theory_best_p2) / 2)

            theory_lambda_R = [edge_on_razor_thin_ring_lambda_r(theory_best_v[_index, k],
                                                                theory_best_simga_disk[_index, k],
                                                                bin_edges[2 * _index], bin_edges[2 * _index + 1], 0.1)
                               for k in range(len(time))]

            z_half_from_theory_dispersion = np.array([
                get_disk_height(np.sqrt(s_z), R_measure, Sigma_R_star, v_c_halo, v200, r200, a_h)
                for s_z in theory_best_z2[_index, :]])

            R_half_cylinder = get_bin_edges('cum')[2 * _index + 1]

            # comes from ratio of itteratve reduce inertia tensor z to median |z| for sech^2(z/z_0)
            mean_div_c = 1.0863912206488044
            c_on_a_approx = z_half_from_theory_dispersion * mean_div_c / (R_half_cylinder / np.sqrt(2))

            # plot
            ax0.errorbar(1 - c_on_a_approx, theory_best_v_on_sigma[_index, :],
                         fmt='k--', zorder=5)
            ax1.errorbar(1 - c_on_a_approx, theory_lambda_R,
                         fmt='k--', zorder=5)

        # isotropys
        if kwargs['ls'] == '-' and kwargs['c'] == 'C0':
            ####
            n_betas = 7
            one_o_one = 101
            (ellipticity, v_on_sigma, lambda_R, beta
             ) = my_ellipticity_from_v_on_s(one_o_one, n_betas, _index=_index)

            for i in range(n_betas):
                ax0.errorbar(ellipticity[i, :], v_on_sigma[i, :],
                             fmt='k:', zorder=-5)
                ax1.errorbar(ellipticity[i, :], lambda_R[i, :],
                             fmt='k:', zorder=-5)

    ax1.legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-'),
                 lines.Line2D([0, 1], [0, 1], color='C1', ls='-'),
                 lines.Line2D([0, 1], [0, 1], color='C2', ls='-'),
                 lines.Line2D([0, 1], [0, 1], color='C3', ls='-'),
                 lines.Line2D([0, 1], [0, 1], color='C4', ls='-')),
                (lines.Line2D([0, 1], [0, 1], color='k', ls='--'))],
               [r'Measured', r'Theory'],
               handler_map={tuple: HandlerTupleVertical(ndivide=5)},
               loc='upper left', numpoints=1, handlelength=1, handletextpad=0.2,
               labelspacing=0, borderpad=0.2)

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return


def plot_shape_proxies(galaxy_name_list, snaps_list,
                       save_name_append='diff', name='', save=False):
    _index = 1

    plt.figure()

    # fits for comparison
    print('\nbeta=0, gamma=0 (fiducial)')
    (ln_Lambda_ks, alphas, betas, gammas
     ) = find_least_log_squares_best_fit(
        save_name_append, mcmc=False, fixed_b=0, fixed_g=0)

    bin_edges = get_bin_edges(save_name_append)
    R_cylinder_min = bin_edges[2 * _index]

    R_measure = get_bin_edges(save_name_append='cum')[2 * _index + 1]

    # load galaxys properties
    a_h, vmax = get_hernquist_params()
    sigma_dm = get_analytic_hernquist_dispersion([R_measure, R_measure], vmax, a_h, save_name_append='diff')[0]
    v_c_halo = get_analytic_hernquist_circular_velocity([R_measure, R_measure], vmax, a_h, save_name_append='diff')[0]
    v200 = get_v_200()
    r200 = get_r_200()
    M_disk, R_disk = get_exponential_disk_params()
    Sigma_R_star = get_exponential_surface_density(R_measure, M_disk, R_disk, save_name_append='diff')
    z_with_sigma_max = get_disk_height(sigma_dm, R_measure, Sigma_R_star, v_c_halo, v200, r200, a_h)

    analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0,
                                             v200=False, dm_dispersion=False, dm_v_circ=False,
                                             analytic_dispersion=True, analytic_v_c=False,
                                             save_name_append=save_name_append)

    analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0,
                                         v200=False, dm_dispersion=False, dm_v_circ=False,
                                         analytic_dispersion=False, analytic_v_c=True,
                                         save_name_append=save_name_append)

    for galaxy_name, snaps in zip(galaxy_name_list, snaps_list):
        kwargs = get_name_kwargs(galaxy_name)
        print(galaxy_name)
        # [1] is r_half
        (mean_v_R, mean_v_phi,
         sigma_z, sigma_R, sigma_phi
         ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges)

        z_half_total = np.zeros(snaps + 1)
        z_half_at_R_half_0 = np.zeros(snaps + 1)
        z_half_at_R_half_i = np.zeros(snaps + 1)
        R_half = np.zeros(snaps + 1)

        z_mean_total = np.zeros(snaps + 1)
        z_mean_at_R_half_0 = np.zeros(snaps + 1)
        z_mean_at_R_half_i = np.zeros(snaps + 1)

        z2_mean_at_R_half_0 = np.zeros(snaps + 1)

        sigma_x2_at_R_half_i = np.zeros(snaps + 1)
        sigma_y2_at_R_half_i = np.zeros(snaps + 1)

        for i in range(snaps + 1):
            # load a snap
            (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_nbody.load_and_align(galaxy_name, snap='{0:03d}'.format(int(i)))

            R = np.sqrt(PosStars[:, 0] ** 2 + PosStars[:, 1] ** 2)
            # R = np.linalg.norm(PosStars, axis=1)

            z_half_total[i] = np.median(np.abs(PosStars[:, 2]))
            z_mean_total[i] = np.mean(np.abs(PosStars[:, 2]))
            if _index == 0:
                R_half[i] = np.quantile(R, .25)
            elif _index == 1:
                R_half[i] = np.median(R)
            elif _index == 2:
                R_half[i] = np.quantile(R, .75)

            mask_0 = np.logical_and((R > bin_edges[2 * _index]), (R < bin_edges[2 * _index + 1]))
            z_half_at_R_half_0[i] = np.median(np.abs(PosStars[mask_0, 2]))
            z_mean_at_R_half_0[i] = np.mean(np.abs(PosStars[mask_0, 2]))
            z2_mean_at_R_half_0[i] = np.mean(PosStars[mask_0, 2] ** 2)

            edges = get_dex_bins([R_half[i]], dex=0.2)
            mask_i = np.logical_and((R > edges[0]), (R < edges[1]))
            z_half_at_R_half_i[i] = np.median(np.abs(PosStars[mask_i, 2]))
            z_mean_at_R_half_i[i] = np.mean(np.abs(PosStars[mask_i, 2]))

            sigma_x2_at_R_half_i[i] = np.sum(VelStars[mask_i, 0] ** 2) / np.sum(mask_i)
            sigma_y2_at_R_half_i[i] = np.sum(VelStars[mask_i, 1] ** 2) / np.sum(mask_i)

        z_half_from_measured_dispersion = np.array([
            get_disk_height(s_z, R_measure, Sigma_R_star, v_c_halo, v200, r200, a_h)
            for s_z in sigma_z[:, _index]])

        R_half_from_actual = np.array([
            get_disk_height(np.sqrt(s_x), R_measure, Sigma_R_star, v_c_halo, v200, r200, a_h)
            for s_x in sigma_x2_at_R_half_i])

        sigma_x2_from_measured = (mean_v_phi[:, _index] ** 2 +
                                  sigma_R[:, _index] ** 2 + sigma_phi[:, _index] ** 2) / 2
        R_half_from_measured = np.array([
            get_disk_height(np.sqrt(s_x), R_measure, Sigma_R_star, v_c_halo, v200, r200, a_h)
            for s_x in sigma_x2_from_measured])

        # theory
        # time is complex to go backwards in time
        time = np.linspace(0, T_TOT, snaps + 1)  # , dtype=np.complex128)
        time = time - time[BURN_IN]

        # work out constatns
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, kwargs['mdm'])

        # burn ins
        (inital_velocity_v, inital_velocity2_z, inital_velocity2_r, inital_velocity2_p
         ) = smooth_burn_ins(kwargs['mdm'], snaps, save_name_append, BURN_IN, bin_edges)

        inital_velocity2_z **= 2
        inital_velocity2_r **= 2
        inital_velocity2_p **= 2

        for i in [_index]:
            theory_best_v = get_theory_velocity2_array(analytic_v_circ[i], analytic_v_circ[i] - inital_velocity_v[i],
                                                       time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                       ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
            theory_best_z2 = get_theory_velocity2_array(analytic_dispersion[i] ** 2, inital_velocity2_z[i],
                                                        time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                        ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
            theory_best_r2 = get_theory_velocity2_array(analytic_dispersion[i] ** 2, inital_velocity2_r[i],
                                                        time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                        ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
            theory_best_p2 = get_theory_velocity2_array(analytic_dispersion[i] ** 2, inital_velocity2_p[i],
                                                        time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                        ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

            theory_best_v = inital_velocity_v[i] - theory_best_v

        z_half_from_theory_dispersion = np.array([
            get_disk_height(np.sqrt(s_z), R_measure, Sigma_R_star, v_c_halo, v200, r200, a_h)
            for s_z in theory_best_z2])

        sigma_x2_theory = (theory_best_v ** 2 + theory_best_r2 + theory_best_p2) / 2
        R_half_from_theory_dispersion = np.array([
            get_disk_height(np.sqrt(s_x), R_measure, Sigma_R_star, v_c_halo, v200, r200, a_h)
            for s_x in sigma_x2_theory])

        a, b, c = load_galaxy_abc(galaxy_name, save_name_append, snaps, bin_edges)

        # kepler stuff
        sigma_r_on_v_c_2 = [0., 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5,
                            0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95]
        expect_r_on_r_dash = [1., 1.05144499, 1.09290677, 1.13996927, 1.18077245,
                              1.22060039, 1.25676145, 1.28922072, 1.32043973, 1.34888878,
                              1.375627, 1.39867563, 1.42006337, 1.43862709, 1.45491012,
                              1.46852637, 1.47978592, 1.4884485, 1.49475354, 1.4984359]

        spline = InterpolatedUnivariateSpline(sigma_r_on_v_c_2, expect_r_on_r_dash)

        R_spline_out = R_measure * spline(theory_best_r2 / analytic_dispersion[_index] ** 2)
        # need to look at this again

        # plt.errorbar(time, z_half_total, label='z half total')
        # plt.errorbar(time, z_mean_total, label='z mean total')
        plt.errorbar(time, z_half_at_R_half_0, label='median abs(z) at R half (initial)')
        plt.errorbar(time, np.sqrt(z2_mean_at_R_half_0), label='sqrt(mean z^2) at R half (initial)')
        plt.errorbar(time, z_mean_at_R_half_0, label='mean abs(z) at R half (initial)')
        # plt.errorbar(time, z_half_at_R_half_i, label='z half at R half (current)')
        # plt.errorbar(time, z_mean_at_R_half_i, label='z mean at R half (current)')
        plt.errorbar(time, z_half_from_measured_dispersion, label='z median from measured sigma z')
        plt.errorbar(time, z_half_from_theory_dispersion, label='z median from theory sigma z')
        plt.errorbar(time, R_measure * np.ones(len(time)), label='R cylinder')
        plt.errorbar(time, c[:, _index], label='c')
        plt.errorbar(time, c[:, _index], label='c', alpha=0.5, lw=5)

    plt.xlabel('time')
    plt.ylabel('height (z)')

    plt.legend()

    ####
    plt.figure()
    plt.errorbar(time, sigma_dm ** 2 * np.ones(len(time)), label='sigma dm')
    plt.errorbar(time, sigma_x2_at_R_half_i, label='measured sigma x')
    plt.errorbar(time, sigma_y2_at_R_half_i, label='measured sigma y')
    plt.errorbar(time, sigma_x2_from_measured, label='sigma x from cylindrical measurements')
    plt.errorbar(time, sigma_x2_from_measured + sigma_z[:, _index] ** 2 / 2, label='kinetic_energy')
    plt.errorbar(time, sigma_x2_theory, label='sigma x from theory')

    plt.xlabel('time')
    plt.ylabel('dispersion')

    plt.legend()
    ####
    plt.figure()
    # plt.errorbar(time, R_spline_out, label='R kepler a-sym drift')
    # plt.errorbar(time, R_measure * np.ones(len(time)), label='R half initial')
    plt.errorbar(time, R_measure * np.ones(len(time)), label='R cylinder centre')
    plt.errorbar(time, R_cylinder_min * np.ones(len(time)), label='R cylinder minimum')
    plt.errorbar(time, R_measure / np.sqrt(2) * np.ones(len(time)),
                 label='root(2) * R cylinder centre')
    plt.errorbar(time, R_half, label='R half current')
    # plt.errorbar(time, z_with_sigma_max * np.ones(len(time)), label='disk height(sigma dm)')
    plt.errorbar(time, R_half_from_theory_dispersion, label='disk height(theory sigma x)')
    plt.errorbar(time, R_half_from_measured, label='disk height(measured sigma x)')
    plt.errorbar(time, R_half_from_actual, label='disk height(actual sigma x)')
    plt.errorbar(time, a[:, _index], label='a')
    plt.errorbar(time, b[:, _index], label='b')

    plt.xlabel('time')
    plt.ylabel('size (R)')

    plt.legend()

    # experimentally
    mean_div_c = 1.0863912206488044

    ####
    plt.figure()
    # plt.errorbar([0,1], [0,1/np.sqrt(3)], fmt=':k', label='sqrt(3):1')
    # plt.errorbar([0,1], [0,0.5], fmt=':k', label='1:2')
    plt.errorbar([0, 1], [0, 1], fmt='--k', label='1:1')

    c_on_a = c[:, _index] / a[:, _index]
    # plt.errorbar(c_on_a, z_half_at_R_half_i/R_half, label='z half at R half/R half (current)')
    # plt.errorbar(c_on_a, z_half_at_R_half_0/R_half[0], label='z half at R half/R half (initial)')

    plt.errorbar(c_on_a, z_half_at_R_half_0 / (R_measure / np.sqrt(2)),
                 label='z half at R half/root(2) * R cylinder centre (initial)')
    plt.errorbar(c_on_a, z_half_at_R_half_0 * mean_div_c / (R_measure / np.sqrt(2)),
                 label='experiment')
    plt.errorbar(c[:, _index] / (a[:, _index] + b[:, _index]) * 2,
                 z_half_at_R_half_0 * mean_div_c / (R_measure / np.sqrt(2)),
                 label='experiment')

    # plt.errorbar(c[:, _index]/b[:, _index], z_half_at_R_half_i/R_half,
    #              label='c/b, z half at R half/R half (current)')
    # plt.errorbar(c_on_a, z_half_total/R_half, label='z half global/R half (current)')
    # plt.errorbar(c_on_a, z_half_from_theory_dispersion/z_with_sigma_max,
    #              label='disk height(theory sigma z)/disk height(sigma dm)')
    #
    # plt.errorbar(c_on_a, z_half_from_theory_dispersion / R_half_from_theory_dispersion,
    #              label='z half (theory sigma z) / disk height(theory sigma x)')
    # plt.errorbar(c_on_a, z_half_from_measured_dispersion / R_half_from_measured,
    #              label='z half (measured sigma z) / disk height(measured sigmas)')
    # plt.errorbar(c_on_a, z_half_from_measured_dispersion / R_half_from_actual,
    #              label='z half (measured sigma z) / disk height(measured sigma x)')

    plt.xlabel('c/a')
    plt.ylabel('height/size (z/R)')

    plt.legend()
    plt.show()

    return


###############################################################################

def plot_sizes(galaxy_name_list, snaps_list, name='', save=False):
    # _index = 1
    save_name_append = 'cum'

    fig = plt.figure()
    fig.set_size_inches(6, 5, forward=True)
    ax0 = plt.subplot(1, 1, 1)

    fig.subplots_adjust(hspace=0.03, wspace=0.01)

    # set up plots
    # ax0.set_xlim([0, T_TOT * (101-BURN_IN)/101])
    ax0.set_xlim([0, T_TOT])

    ylim0 = [6.8, 11]
    ax0.set_ylim(ylim0)

    ax0.set_ylabel(r'$R_{1/2}$ [kpc]')
    ax0.set_xlabel(r'$t$ [Gyr]')

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = get_name_kwargs(galaxy_name)

        # time
        time = np.linspace(0, T_TOT, snaps + 1)
        # time = time - time[BURN_IN]

        # load data=
        R_half = get_R_half(galaxy_name, snaps)

        alpha = 0.2
        lw = 4
        window = snaps // 5 + 1
        poly_n = 5

        y = R_half
        ax0.errorbar(time, y,
                     c=kwargs['c'], marker='', ls='-', lw=lw, alpha=alpha)
        ax0.errorbar(time, savgol_filter(y, window, poly_n),
                     c=kwargs['c'], marker='', ls=kwargs['ls'])

        if kwargs['c'] in ['C0', 'C1', 'C2']:
            dx = 0.02
        else:
            dx = 0.46
        if kwargs['c'] in ['C0', 'C3']:
            dy = 0.99
        elif kwargs['c'] in ['C1', 'C4']:
          dy = 0.91
        else:
            dy = 0.83

        ax0.text(dx * T_TOT, ylim0[0] + dy * (ylim0[1] - ylim0[0]),
                 r'$m_{\mathrm{DM}} = $' + kwargs['lab'], ha='left', va='top', color=kwargs['c'])

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    # plt.figure()
    # for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
    #   a, b, c = load_galaxy_abc(galaxy_name, '', snaps, [0,30])
    #
    #   z_half = get_median_z(galaxy_name, '', snaps, [0,30])
    #   R_half = get_R_half(galaxy_name, '', snaps)
    #
    #   plt.scatter(z_half.T[0] / R_half, c/a)
    #
    #   # plt.errorbar([0,1], [0,2])
    #   plt.errorbar([0,7], [0,12])

    return


def mean_seperation():
    (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
     MassStar, PosStars, VelStars, IDStars, PotStars
     ) = load_nbody.load_and_align('mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps', '000')

    # inner 1 kpc
    r_DM_mask = np.linalg.norm(PosDMs, axis=1) < 1

    # distance matrix. distance from one point to all other pointss
    distances = distance_matrix(PosDMs[r_DM_mask], PosDMs[r_DM_mask])

    print(np.shape(distances))

    # remove diagonal 0s
    distances[distances == 0] = np.nan

    out = np.median(np.nanmin(distances, axis=1))
    print(out)
    out = np.mean(np.nanmin(distances, axis=1))
    print(out)
    return


# @lru_cache(maxsize=1000)
def get_razor_thin_disk_potential_off_plane(R, z, Sigma_0, R_d):
    # Binney and Tremaine 2.170 with zeta(z) = delta(z)
    integrand = lambda a, R, z, R_d: np.arcsin(
        2 * a / (np.sqrt(z * z + (a + R) * (a + R)) + np.sqrt(z * z + (a - R) * (a - R)))
        ) * a * kn(0, a / R_d)

    result = quad(integrand, 0, 1e2, args=(R, z, R_d))

    out = -4 * GRAV_CONST * Sigma_0 / R_d * result[0]  # (km/s)^2

    return (out)


def get_sech_disk_potential_off_plane(R, z, Sigma_0, R_d, z_d):
    # Binney and Tremaine 2.170 with zeta(z) = delta(z)
    integrand = lambda z_dash, a, R, z, R_d, z_d: 1 / np.cosh(z_dash / z_d) ** 2 * np.arcsin(
        2 * a / (np.sqrt((z - z_dash) * (z - z_dash) + (a + R) * (a + R)) +
                 np.sqrt((z - z_dash) * (z - z_dash) + (a - R) * (a - R)))) * a * kn(0, a / R_d)

    result = dblquad(integrand, 0, 1e2, lambda x: -1e2 * z_d, lambda x: 1e2 * z_d, args=(R, z, R_d, z_d))

    out = -2 * GRAV_CONST * Sigma_0 / R_d / z_d * result[0]  # (km/s)^2

    return (out)


def plot_restoring_force(galaxy_name, snaps, save_name='', save=False):
    r200 = get_r_200()

    # measurements
    # bins
    bin_edges = np.logspace(-0.5, 2.5, 16)
    bin_centres = 10 ** ((np.log10(bin_edges[:-1]) + np.log10(bin_edges[1:])) / 2)

    ymin, ymax = -10, 10

    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(8, 8, forward=True)
    widths = [1]
    heights = [2, 1, 2]
    spec = fig.add_gridspec(ncols=1, nrows=3, width_ratios=widths,
                            height_ratios=heights)

    ax = []
    for row in range(len(heights)):
        ax.append(fig.add_subplot(spec[row]))
        ax[row].set_xlim([-4.5, 0])
        ax[row].set_xlim([bin_centres[0] / r200, bin_centres[-1] / r200])
        if row != len(heights) - 1: ax[row].set_xticklabels([])

    fig.subplots_adjust(hspace=0.0, wspace=0.0)

    greycmap = matplotlib.cm.get_cmap('Greys')
    bluecmap = matplotlib.cm.get_cmap('Blues')
    redcmap = matplotlib.cm.get_cmap('Reds')
    greencmap = matplotlib.cm.get_cmap('Greens')

    # data
    m = 7
    snaps = np.logspace(np.log10(1), np.log10(snaps - 1), m, dtype=int)
    for i, snap in enumerate(snaps):
        (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
         MassStar, PosStars, VelStars, IDStars, PotStars
         ) = load_nbody.load_and_align(galaxy_name, '%03d' % snap)

        Rs = np.sqrt(PosStars[:, 0] ** 2 + PosStars[:, 1] ** 2)
        n_bins, _ = np.histogram(Rs, bins=bin_edges)
        heights, _, _ = binned_statistic(Rs, np.abs(PosStars[:, 2]), np.median, bins=bin_edges)
        heights[n_bins < 50] = np.nan

        # (_, MassDM,   PosDMs,   VelDMs,   IDDMs,   PotDMs,
        #     MassStar, PosStars, VelStars, IDStars, PotStars
        #     ) = load_nbody.load_and_align(galaxy_name, '%03d'%(snap-1))

        # Rs = np.sqrt(PosStars[:, 0]**2 + PosStars[:, 1]**2)
        # h,_,_ = binned_statistic(Rs, np.abs(PosStars[:, 2]), np.median, bins=bin_edges)
        # heights += h

        # (_, MassDM,   PosDMs,   VelDMs,   IDDMs,   PotDMs,
        #     MassStar, PosStars, VelStars, IDStars, PotStars
        #     ) = load_nbody.load_and_align(galaxy_name, '%03d'%(snap+1))

        # Rs = np.sqrt(PosStars[:, 0]**2 + PosStars[:, 1]**2)
        # h,_,_ = binned_statistic(Rs, np.abs(PosStars[:, 2]), np.median, bins=bin_edges)
        # heights += h

        # heights/=3

        disk_height = np.median(heights[np.logical_and(bin_centres > 2, bin_centres < 10)])

        ax[0].errorbar(bin_centres / r200, heights / disk_height, c=bluecmap((i + 1) / m - 1e-3))

    # grid
    bin_edges = np.logspace(-0.5, 2.5, 31)
    bin_centres = 10 ** ((np.log10(bin_edges[:-1]) + np.log10(bin_edges[1:])) / 2)
    yedges = np.linspace(-10, 10, 41)

    xgrid, ygrid = np.meshgrid(bin_centres / r200, 0.5 * (yedges[:-1] + yedges[1:]))

    # heights
    H, _, _ = np.histogram2d(np.abs(PosStars[:, 2] / disk_height), Rs, [yedges, bin_edges])
    radial_total_H, _ = np.histogram(Rs, bin_edges)
    H /= radial_total_H[np.newaxis, :]

    # ax[1].errorbar(Rs/r200, np.abs(PosStars[:, 2]/disk_height), marker=',', ms=1, ls='', c='k', alpha=0.5)
    ax[1].contourf(xgrid, ygrid, H, levels=np.logspace(np.log10(np.nanmin(H[H != 0])), np.log10(np.nanmax(H)), 20),
                   alpha=0.8, zorder=100, locator=ticker.LogLocator())

    ax[1].text(0.3, 8, r'$t=9.6$Gyr', va='top')

    #
    # analytic potentials
    # bins
    bin_edges = np.logspace(-0.5, 3, 16)
    bin_centres = 10 ** ((np.log10(bin_edges[:-1]) + np.log10(bin_edges[1:])) / 2)

    a, vmax = get_hernquist_params()
    GM = 4 * a * vmax ** 2  # kpc (km/s)^2

    m = 5
    z_hs = np.logspace(np.log10(0.25), np.log10(4), m)  # kpc

    M_disk, r_disk = get_exponential_disk_params()
    Sigma_0 = M_disk / (2 * np.pi * r_disk ** 2)  # 10^-10 M_sun / kpc^2
    z_disk = 0.25  # kpc

    for i, z_h in enumerate(z_hs):
        # analytic force from Hernquist halo
        F = z_h * GM / (np.sqrt(bin_centres ** 2 + z_h ** 2) * (
                    a + np.sqrt(bin_centres ** 2 + z_h ** 2)) ** 2)  # (km/s)^2/pc

        disk_force = []
        for R in bin_centres:
            disk_force.append((get_razor_thin_disk_potential_off_plane(R, z_h + 1e-3, Sigma_0, r_disk) -
                               get_razor_thin_disk_potential_off_plane(R, z_h - 1e-3, Sigma_0, r_disk)) / 2e-3)
            # disk_force.append((get_sech_disk_potential_off_plane(R, z_h + 1e-3, Sigma_0, r_disk, z_disk) -
            #                     get_sech_disk_potential_off_plane(R, z_h - 1e-3, Sigma_0, r_disk, z_disk)) / 2e-3)
            # (kpc/s)^2/ kpc

        disk_force = np.array(disk_force)

        if i == 0:
            F_0 = (F[0] + disk_force[0])

        ax[2].errorbar(bin_centres / r200, F / F_0, c=redcmap((m - (i)) / m - 1e-3))
        ax[2].errorbar(bin_centres / r200, disk_force / F_0, c=greencmap((m - (i)) / m - 1e-3))

        ax[2].errorbar(bin_centres / r200, (disk_force + F) / F_0, c=greycmap((m - (i)) / m - 1e-3))

        text_index = 7
        ax[2].text(bin_centres[text_index] / r200, (disk_force + F)[text_index] / F_0 - 1e-3 / z_h,
                   str(round(z_h, 2)) + r'$z_{1/2}$',
                   color=greycmap((m - (i)) / m - 1e-3))

    # bin_edges   = np.roll(np.repeat(bin_edges, 2), -1)[:-2]

    interesting_bin_edges = get_bin_edges()
    interesting_bin_centres = 10 ** (
                (np.log10(interesting_bin_edges[:-1:2]) + np.log10(interesting_bin_edges[1:2])) / 2)
    for i in range(3):
        [ax[i].vlines(r / r200, ymin, ymax, ls='-.', color='k') for r in interesting_bin_centres]
        ax[i].vlines(30 / r200, ymin, ymax, ls='-.', color='hotpink')
        ax[i].vlines(1, ymin, ymax, ls='-.', color='green')

    ax[2].set_xlabel(r'$R / r_{200}$')
    ax[0].set_ylabel(r'$z_{50}(r) / z_{50, SG}$')
    ax[1].set_ylabel(r'$|z| / z_{1/2}$')
    ax[2].set_ylabel(r'$F_z(R,z) / F_z(0, z_{1/2})$')

    ax[0].loglog()
    ax[1].semilogx()
    ax[2].loglog()

    ax[0].set_ylim([7e-1, 5e0])
    # ax[1].set_ylim([-9,9])
    ax[1].set_ylim([0, 9])
    ax[2].set_ylim([1e-2, 1.5])

    plt.show()
    if save:
        plt.savefig(save_name, bbox_inches='tight')
        plt.close()

    return


def plot_indicator_n_dependence(save_name_append='diff', save=False, name=''):
    _index = 1

    # min_mdm = 5.01  # 5.0
    # min_mdm2 = 5.01  # 6.01
    # max_mdm = 9.5
    # m_dms = np.logspace(min_mdm, max_mdm)
    # N200s = 1.85 * 10 ** 12 / m_dms
    # log_N200s = np.log10(N200s)
    # N200_min = np.log10(1.85 * 10 ** 12 / 10 ** min_mdm)
    # N200_min2 = np.log10(1.85 * 10 ** 12 / 10 ** min_mdm2)
    # N200_max = np.log10(1.85 * 10 ** 12 / 10 ** max_mdm)

    min_mdm = 5  # 5.0
    max_mdm = 9.5
    m_dms = np.logspace(min_mdm, max_mdm, 100)
    N200s = 1.85 * 10 ** 12 / m_dms
    log_N200s = np.log10(N200s)
    N200_max = np.log10(1.85 * 10 ** 12 / 10 ** min_mdm)
    N200_min = np.log10(1.85 * 10 ** 12 / 10 ** max_mdm)

    m_DM_EAGLE = 9.70e6 #M_sun
    M200_EAGLE = m_DM_EAGLE * N200s
    log_M200_EAGLE = np.log10(M200_EAGLE)
    M200_max = np.log10(np.amax(M200_EAGLE))
    M200_min = np.log10(np.amin(M200_EAGLE))

    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(22, 15, forward=True)
    widths = [1,1,1]
    heights = [1, 1, 1]
    spec = fig.add_gridspec(ncols=3, nrows=3, width_ratios=widths,
                            height_ratios=heights)

    ax = []
    for col in range(len(widths)):
        ax.append([])
        for row in range(len(heights)):
            ax[col].append(fig.add_subplot(spec[row, col]))

            if row == 0: ax[col][row].semilogy()

            ax[col][row].set_xlim([N200_max, N200_min])

            if row != len(heights) - 1: ax[col][row].set_xticklabels([])
            if col != 0: ax[col][row].set_yticklabels([])

    # not sure why this didn't work the first time
    ax[1][0].axes.get_yaxis().set_visible(False)
    ax[2][0].axes.get_yaxis().set_visible(False)

    fig.subplots_adjust(hspace=0.02, wspace=0.02)

    smol = 1e-2  # 3e-3
    axtwin = []
    vons_range = [0.033, 30]
    for col in range(len(widths)):
        ax[col][0].semilogy()
        ax[col][0].set_ylim(vons_range)
        ax[col][1].set_ylim([1 / 3 - smol, 1 + smol])
        ax[col][2].set_ylim([0 - smol, 1 + smol])

        ax[col][0].axhline(1, 0, 1, c='grey', ls=':')
        ax[col][0].axhline(0.5, 0, 1, c='grey', ls=':')
        ax[col][1].axhline(0.6, 0, 1, c='grey', ls=':')
        ax[col][2].axhline(0.5, 0, 1, c='grey', ls=':')

        axtwin.append(ax[col][0].twiny())
        # axtwin[col].set_xlim([max_mdm, min_mdm])
        axtwin[col].set_xlim([M200_min, M200_max])

        ax[col][2].set_xlabel(r'$\log \, \mathrm{N}_{\mathrm{DM}}$')

    # axtwin[1].set_xlabel(r'$\log(m_{\mathrm{DM}} / $M$_\odot)$ for $M_{200} = 1.85 \times 10^{12}$M$_\odot$ galaxy')
    axtwin[1].set_xlabel(r'$M_{200}$ at EAGLE resolution')

    ax[0][0].set_ylabel(r'$\overline{v_\phi} / \sqrt{\sigma_{\mathrm{tot}}^2 / 3}$')
    ax[0][0].set_ylabel(r'$\overline{v_\phi} / \sigma_{\mathrm{1D}}$')
    ax[0][1].set_ylabel(r'$\kappa_{\mathrm{rot}}$')
    ax[0][2].set_ylabel(r'S/T')

    max_T = 13.8
    ax[0][0].text(N200_max + 0.2, vons_range[1] - 6, r'$\Delta t = $' + str(max_T) + 'Gyr', ha='left', va='top')
    ax[0][0].text(N200_max + 0.2, vons_range[1] - 18, r'at $R = R_{1/2}$', ha='left', va='top')

    ax[1][0].text(N200_max + 0.2, vons_range[1] - 6, r'$\Delta t = $' + str(max_T) + 'Gyr', ha='left', va='top')
    ax[1][0].text(N200_max + 0.2, vons_range[1] - 18, r'$\sigma_i^2 = 0$', ha='left', va='top')

    ax[2][0].text(N200_max + 0.2, vons_range[1] - 6, r'$\sigma_i^2 = 0$', ha='left', va='top')
    ax[2][0].text(N200_max + 0.2, vons_range[1] - 18, r'$R = R_{1/2}$', ha='left', va='top')

    ax[0][2].text(N200_min - 0.2, 0.97, r'$\Delta t = $' + str(max_T) + 'Gyr', ha='right', va='top')
    ax[0][2].text(N200_min - 0.2, 0.86, r'at $R = R_{1/2}$', ha='right', va='top')

    ax[1][2].text(N200_min - 0.2, 0.97, r'$\Delta t = $' + str(max_T) + 'Gyr', ha='right', va='top')
    ax[1][2].text(N200_min - 0.2, 0.86, r'$\sigma_i^2 = 0$', ha='right', va='top')

    ax[2][2].text(N200_min - 0.2, 0.97, r'$\sigma_i^2 = 0$', ha='right', va='top')
    ax[2][2].text(N200_min - 0.2, 0.86, r'$R = R_{1/2}$', ha='right', va='top')

    ####
    # get theory
    print('\nbeta=0, gamma=0 (fiducial)')
    (ln_Lambda_ks, alphas, betas, gammas
     ) = find_least_log_squares_best_fit(
        save_name_append, mcmc=False, fixed_b=0, fixed_g=0)

    print(find_least_log_squares_best_fit(
        save_name_append, mcmc=False, fixed_b=0, fixed_g=0))

    # where to measure
    bin_edges = get_bin_edges(save_name_append)

    # all galaxies
    analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                             save_name_append=save_name_append)
    analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                         save_name_append=save_name_append)

    time = np.array([max_T])

    # ic_heat_fraction = np.array([0.0001,0.01,0.1,0.33,0.5])
    # ic_heat_fraction = np.array([0.0001, 0.05, 0.1, 0.2, 0.4])
    # heat_fraction_labels = [r'$\sigma_i^2=0$', r'$\sigma_i^2=0.05 \sigma_{DM}^2$', r'$\sigma_i^2=0.10 \sigma_{DM}^2$',
    #                         r'$\sigma_i^2=0.20 \sigma_{DM}^2$', r'$\sigma_i^2=0.40 \sigma_{DM}^2$']
    ic_heat_fraction = np.array([0.0001, 0.05, 0.1, 0.2])
    heat_fraction_labels = [r'$\sigma_i^2=0$', r'$\sigma_i^2=0.05 \,\sigma_{DM}^2$',
                            r'$\sigma_i^2=0.10 \,\sigma_{DM}^2$',
                            r'$\sigma_i^2=0.20 \,\sigma_{DM}^2$']
    # ic_heat_fraction = np.array([0.0001,0.01])

    # concentration
    v_on_sigma = np.zeros((len(bin_edges) // 2, 2, len(m_dms)))
    kappa_rot = np.zeros((len(bin_edges) // 2, 2, len(m_dms)))
    # bulge_to_total = np.zeros((len(bin_edges)//2, len(time), len(m_dms)))
    bulge_to_total = np.zeros((2, len(m_dms)))

    time = np.array([max_T])

    for k, m_dm in enumerate(m_dms):
        galaxy_name = 'mu_5_c_15/fdisk_0p01_lgMdm_8p0_V200-200kmps'
        V200 = 200

        # work out constatns
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, m_dm * 1e-10, galaxy_name=galaxy_name)

        analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                                 save_name_append=save_name_append, galaxy_name=galaxy_name)
        analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)

        # burn ins
        one = 0.01
        inital_velocity_v = np.sqrt(analytic_v_circ ** 2 - one ** 2 * np.ones(len(bin_edges) // 2))
        inital_velocity2_z = one * np.ones(len(bin_edges) // 2)
        inital_velocity2_r = one * np.ones(len(bin_edges) // 2)
        inital_velocity2_p = one * np.ones(len(bin_edges) // 2)

        theory_best_v = np.zeros((len(bin_edges) // 2))
        theory_best_z2 = np.zeros((len(bin_edges) // 2))
        theory_best_r2 = np.zeros((len(bin_edges) // 2))
        theory_best_p2 = np.zeros((len(bin_edges) // 2))

        # different times
        for i in range(len(bin_edges) // 2):
            theory_best_v[i] = get_theory_velocity2_array(V200,
                                                          analytic_v_circ[i] - inital_velocity_v[i],
                                                          time, t_c_dm[i], delta_dm[i], np.sqrt(upsilon_circ[i]),
                                                          ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
            theory_best_z2[i] = get_theory_velocity2_array(V200 ** 2, inital_velocity2_z[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
            theory_best_r2[i] = get_theory_velocity2_array(V200 ** 2, inital_velocity2_r[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
            theory_best_p2[i] = get_theory_velocity2_array(V200 ** 2, inital_velocity2_p[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

        theory_best_v = inital_velocity_v - theory_best_v

        # model indicators
        theory_best_tot2 = theory_best_z2 + theory_best_r2 + theory_best_p2
        theory_best_v_on_sigma = theory_best_v / np.sqrt(theory_best_tot2 / 3)

        theory_best_kappa_rot = (theory_best_v ** 2 + theory_best_p2) / (
                theory_best_v ** 2 + theory_best_tot2)

        theory_bulge_to_total = 2 * scipy.stats.norm.cdf(0, loc=theory_best_v[_index],
                                                         scale=np.sqrt(theory_best_p2[_index]))

        v_on_sigma[:, 0, k] = theory_best_v_on_sigma
        kappa_rot[:, 0, k] = theory_best_kappa_rot
        bulge_to_total[0, k] = theory_bulge_to_total

        galaxy_name = 'mu_5_c_5/fdisk_0p01_lgMdm_8p0_V200-200kmps'
        V200 = 200

        # work out constatns
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, m_dm * 1e-10, galaxy_name=galaxy_name)\

        analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)

        # burn ins
        one = 0.01
        inital_velocity_v = np.sqrt(analytic_v_circ ** 2 - one ** 2 * np.ones(len(bin_edges) // 2))
        inital_velocity2_z = one * np.ones(len(bin_edges) // 2)
        inital_velocity2_r = one * np.ones(len(bin_edges) // 2)
        inital_velocity2_p = one * np.ones(len(bin_edges) // 2)

        theory_best_v = np.zeros((len(bin_edges) // 2))
        theory_best_z2 = np.zeros((len(bin_edges) // 2))
        theory_best_r2 = np.zeros((len(bin_edges) // 2))
        theory_best_p2 = np.zeros((len(bin_edges) // 2))

        # different times
        for i in range(len(bin_edges) // 2):
            theory_best_v[i] = get_theory_velocity2_array(V200,
                                                          analytic_v_circ[i] - inital_velocity_v[i],
                                                          time, t_c_dm[i], delta_dm[i], np.sqrt(upsilon_circ[i]),
                                                          ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
            theory_best_z2[i] = get_theory_velocity2_array(V200 ** 2, inital_velocity2_z[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
            theory_best_r2[i] = get_theory_velocity2_array(V200 ** 2, inital_velocity2_r[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
            theory_best_p2[i] = get_theory_velocity2_array(V200 ** 2, inital_velocity2_p[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

        theory_best_v = inital_velocity_v - theory_best_v

        # model indicators
        theory_best_tot2 = theory_best_z2 + theory_best_r2 + theory_best_p2
        theory_best_v_on_sigma = theory_best_v / np.sqrt(theory_best_tot2 / 3)

        theory_best_kappa_rot = (theory_best_v ** 2 + theory_best_p2) / (
                theory_best_v ** 2 + theory_best_tot2)

        theory_bulge_to_total = 2 * scipy.stats.norm.cdf(0, loc=theory_best_v[_index],
                                                         scale=np.sqrt(theory_best_p2[_index]))

        v_on_sigma[:, 1, k] = theory_best_v_on_sigma
        kappa_rot[:, 1, k] = theory_best_kappa_rot
        bulge_to_total[1, k] = theory_bulge_to_total

    for k in range(3):
        ax[k][0].fill_between(log_N200s, v_on_sigma[_index, 0], v_on_sigma[_index, 1], color='k', alpha=0.3)
        if k == 0:
            ax[k][1].fill_between(log_N200s, kappa_rot[_index, 0], kappa_rot[_index, 1], color='k', alpha=0.3,
                                  label=r'$c = 10 \pm 5$')
        else:
            ax[k][1].fill_between(log_N200s, kappa_rot[_index, 0], kappa_rot[_index, 1], color='k', alpha=0.3)
        ax[k][2].fill_between(log_N200s, bulge_to_total[0], bulge_to_total[1], color='k', alpha=0.3)

    v_on_sigma = np.zeros((len(bin_edges) // 2, len(ic_heat_fraction), len(m_dms)))
    kappa_rot = np.zeros((len(bin_edges) // 2, len(ic_heat_fraction), len(m_dms)))
    # bulge_to_total = np.zeros((len(bin_edges)//2, len(time), len(m_dms)))
    bulge_to_total = np.zeros((len(ic_heat_fraction), len(m_dms)))

    galaxy_name = 'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps'
    V200 = 200

    # work out constatns
    bin_edges = get_bin_edges(save_name_append, galaxy_name=galaxy_name)

    analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)
    analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                         save_name_append=save_name_append, galaxy_name=galaxy_name)

    # differet initial velocities
    for k, m_dm in enumerate(m_dms):

        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, m_dm * 1e-10, galaxy_name=galaxy_name)

        for j in range(len(ic_heat_fraction)):
            # burn ins
            inital_velocity_v = np.sqrt((1 - ic_heat_fraction[j]) * analytic_v_circ ** 2)
            inital_velocity2_z = ic_heat_fraction[j] * (analytic_dispersion) ** 2
            inital_velocity2_r = ic_heat_fraction[j] * (analytic_dispersion) ** 2
            inital_velocity2_p = ic_heat_fraction[j] * (analytic_dispersion) ** 2

            theory_best_v = np.zeros((len(bin_edges) // 2, len(time)))
            theory_best_z2 = np.zeros((len(bin_edges) // 2, len(time)))
            theory_best_r2 = np.zeros((len(bin_edges) // 2, len(time)))
            theory_best_p2 = np.zeros((len(bin_edges) // 2, len(time)))

            # the actual thing
            for i in range(len(bin_edges) // 2):
                theory_best_v[i] = get_theory_velocity2_array(V200,
                                                              analytic_v_circ[i] - inital_velocity_v[i],
                                                              time, t_c_dm[i], delta_dm[i], np.sqrt(upsilon_circ[i]),
                                                              ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
                theory_best_z2[i] = get_theory_velocity2_array(V200 ** 2, inital_velocity2_z[i],
                                                               time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                               ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
                theory_best_r2[i] = get_theory_velocity2_array(V200 ** 2, inital_velocity2_r[i],
                                                               time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                               ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
                theory_best_p2[i] = get_theory_velocity2_array(V200 ** 2, inital_velocity2_p[i],
                                                               time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                               ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

            theory_best_v = inital_velocity_v[:, np.newaxis] - theory_best_v

            # model indicators
            theory_best_tot2 = theory_best_z2 + theory_best_r2 + theory_best_p2
            theory_best_v_on_sigma = theory_best_v / np.sqrt(theory_best_tot2 / 3)

            theory_best_kappa_rot = (theory_best_v ** 2 + theory_best_p2) / (
                    theory_best_v ** 2 + theory_best_tot2)

            theory_bulge_to_total = [2 * scipy.stats.norm.cdf(0, loc=v, scale=np.sqrt(p2))
                                     for (v, p2) in zip(theory_best_v[_index, :], theory_best_p2[_index, :])]

            v_on_sigma[:, j, k] = theory_best_v_on_sigma[:, 0]
            kappa_rot[:, j, k] = theory_best_kappa_rot[:, 0]
            bulge_to_total[j, k] = theory_bulge_to_total[0]

    for j in range(len(ic_heat_fraction)):
        if j == 0:
            c = 'k'
            ls = '-'
        else:
            c = 'C' + str(j + 5)
        if j == 1: ls = '-.'
        if j == 2: ls = '--'
        if j == 3: ls = ':'
        if j == 4: ls = (0, (3, 1, 1, 1, 1, 1))
        lw = 3
        ax[0][0].plot(log_N200s, v_on_sigma[_index, j], c=c, ls=ls, lw=lw)
        ax[0][1].plot(log_N200s, kappa_rot[_index, j], c=c, ls=ls, lw=lw, label=heat_fraction_labels[j])
        ax[0][2].plot(log_N200s, bulge_to_total[j], c=c, ls=ls, lw=lw)

    # differenet radii
    time = np.array([max_T])

    r_half = get_bin_edges('cum')[3]
    radii_edges = np.array([r_half / 4, r_half / 4, r_half, r_half, r_half * 4, r_half * 4])

    v_on_sigma = np.zeros((len(radii_edges) // 2, len(m_dms)))
    kappa_rot = np.zeros((len(radii_edges) // 2, len(m_dms)))
    bulge_to_total = np.zeros((len(radii_edges) // 2, len(m_dms)))

    analytic_dispersion = get_velocity_scale(radii_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)
    analytic_v_circ = get_velocity_scale(radii_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                         save_name_append=save_name_append, galaxy_name=galaxy_name)

    for k, m_dm in enumerate(m_dms):

        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(radii_edges, save_name_append, m_dm * 1e-10, galaxy_name=galaxy_name)

        # burn ins
        one = 0.01
        inital_velocity_v = np.sqrt(analytic_v_circ ** 2 - one ** 2 * np.ones(len(radii_edges) // 2))
        inital_velocity2_z = one * np.ones(len(radii_edges) // 2)
        inital_velocity2_r = one * np.ones(len(radii_edges) // 2)
        inital_velocity2_p = one * np.ones(len(radii_edges) // 2)

        theory_best_v = np.zeros((len(radii_edges) // 2, len(time)))
        theory_best_z2 = np.zeros((len(radii_edges) // 2, len(time)))
        theory_best_r2 = np.zeros((len(radii_edges) // 2, len(time)))
        theory_best_p2 = np.zeros((len(radii_edges) // 2, len(time)))

        for i in range(len(radii_edges) // 2):
            theory_best_v[i] = get_theory_velocity2_array(V200,
                                                          analytic_v_circ[i] - inital_velocity_v[i],
                                                          time, t_c_dm[i], delta_dm[i], np.sqrt(upsilon_circ[i]),
                                                          ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
            theory_best_z2[i] = get_theory_velocity2_array(V200 ** 2, inital_velocity2_z[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
            theory_best_r2[i] = get_theory_velocity2_array(V200 ** 2, inital_velocity2_r[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
            theory_best_p2[i] = get_theory_velocity2_array(V200 ** 2, inital_velocity2_p[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

        theory_best_v = inital_velocity_v[:, np.newaxis] - theory_best_v

        # model indicators
        theory_best_tot2 = theory_best_z2 + theory_best_r2 + theory_best_p2
        theory_best_v_on_sigma = theory_best_v / np.sqrt(theory_best_tot2 / 3)

        theory_best_kappa_rot = (theory_best_v ** 2 + theory_best_p2) / (
                theory_best_v ** 2 + theory_best_tot2)

        theory_bulge_to_total = [2 * scipy.stats.norm.cdf(0, loc=v, scale=np.sqrt(p2))
                                 for (v, p2) in zip(theory_best_v[:, 0], theory_best_p2[:, 0])]

        v_on_sigma[:, k] = theory_best_v_on_sigma[:, 0]
        kappa_rot[:, k] = theory_best_kappa_rot[:, 0]
        bulge_to_total[:, k] = theory_bulge_to_total

    ax[1][0].plot(log_N200s, v_on_sigma[0], c='C0', ls='--', lw=lw)
    ax[1][0].plot(log_N200s, v_on_sigma[1], c='k', ls='-', lw=lw)
    ax[1][0].plot(log_N200s, v_on_sigma[2], c='C3', ls='-.', lw=lw)

    ax[1][1].plot(log_N200s, kappa_rot[0], c='C0', ls='--', lw=lw,
                  label=r'$R=0.25 \times R_{1/2}$')
    ax[1][1].plot(log_N200s, kappa_rot[1], c='k', ls='-', lw=lw,
                  label=r'$R=R_{1/2}$')
    ax[1][1].plot(log_N200s, kappa_rot[2], c='C3', ls='-.', lw=lw,
                  label=r'$R=4 \times R_{1/2}$')

    ax[1][2].plot(log_N200s, bulge_to_total[0], c='C0', ls='--', lw=lw)
    ax[1][2].plot(log_N200s, bulge_to_total[1], c='k', ls='-', lw=lw)
    ax[1][2].plot(log_N200s, bulge_to_total[2], c='C3', ls='-.', lw=lw)

    time = np.array([2, 5, 10, max_T])
    # time = np.linspace(0, T_TOT, 100)

    v_on_sigma = np.zeros((len(bin_edges) // 2, len(time), len(m_dms)))
    kappa_rot = np.zeros((len(bin_edges) // 2, len(time), len(m_dms)))
    # bulge_to_total = np.zeros((len(bin_edges)//2, len(time), len(m_dms)))
    bulge_to_total = np.zeros((len(time), len(m_dms)))

    analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)
    analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                         save_name_append=save_name_append, galaxy_name=galaxy_name)

    for k, m_dm in enumerate(m_dms):

        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, m_dm * 1e-10, galaxy_name=galaxy_name)

        # burn ins
        one = 0.01
        inital_velocity_v = np.sqrt(analytic_v_circ ** 2 - one ** 2 * np.ones(len(bin_edges) // 2))
        inital_velocity2_z = one * np.ones(len(bin_edges) // 2)
        inital_velocity2_r = one * np.ones(len(bin_edges) // 2)
        inital_velocity2_p = one * np.ones(len(bin_edges) // 2)

        theory_best_v = np.zeros((len(bin_edges) // 2, len(time)))
        theory_best_z2 = np.zeros((len(bin_edges) // 2, len(time)))
        theory_best_r2 = np.zeros((len(bin_edges) // 2, len(time)))
        theory_best_p2 = np.zeros((len(bin_edges) // 2, len(time)))

        # different times
        for i in range(len(bin_edges) // 2):
            theory_best_v[i] = get_theory_velocity2_array(V200,
                                                          analytic_v_circ[i] - inital_velocity_v[i],
                                                          time, t_c_dm[i], delta_dm[i], np.sqrt(upsilon_circ[i]),
                                                          ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
            theory_best_z2[i] = get_theory_velocity2_array(V200 ** 2, inital_velocity2_z[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
            theory_best_r2[i] = get_theory_velocity2_array(V200 ** 2, inital_velocity2_r[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
            theory_best_p2[i] = get_theory_velocity2_array(V200 ** 2, inital_velocity2_p[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

        theory_best_v = inital_velocity_v[:, np.newaxis] - theory_best_v

        # model indicators
        theory_best_tot2 = theory_best_z2 + theory_best_r2 + theory_best_p2
        theory_best_v_on_sigma = theory_best_v / np.sqrt(theory_best_tot2 / 3)

        theory_best_kappa_rot = (theory_best_v ** 2 + theory_best_p2) / (
                theory_best_v ** 2 + theory_best_tot2)

        theory_bulge_to_total = [2 * scipy.stats.norm.cdf(0, loc=v, scale=np.sqrt(p2))
                                 for (v, p2) in zip(theory_best_v[_index, :], theory_best_p2[_index, :])]

        v_on_sigma[:, :, k] = theory_best_v_on_sigma
        kappa_rot[:, :, k] = theory_best_kappa_rot
        bulge_to_total[:, k] = theory_bulge_to_total

    ax[2][0].plot(log_N200s, v_on_sigma[_index, 0], c='C2', ls='-.', lw=lw)
    ax[2][0].plot(log_N200s, v_on_sigma[_index, 1], c='C1', ls='--', lw=lw)
    ax[2][0].plot(log_N200s, v_on_sigma[_index, 3], c='k', ls='-', lw=lw)

    ax[2][1].plot(log_N200s, kappa_rot[_index, 3], c='k', ls='-', lw=lw,
                  label=r'$\Delta t =$' + str(max_T) + ' Gyr')
    ax[2][1].plot(log_N200s, kappa_rot[_index, 1], c='C1', ls='--', lw=lw,
                  label=r'$\Delta t = 5$ Gyr')
    ax[2][1].plot(log_N200s, kappa_rot[_index, 0], c='C2', ls='-.', lw=lw,
                  label=r'$\Delta t = 2$ Gyr')

    ax[2][2].plot(log_N200s, bulge_to_total[0], c='C2', ls='-.', lw=lw)
    ax[2][2].plot(log_N200s, bulge_to_total[1], c='C1', ls='--', lw=lw)
    ax[2][2].plot(log_N200s, bulge_to_total[3], c='k', ls='-', lw=lw)

    ax[0][1].plot([2.94, 3.35], [0.706] * 2, lw=lw, c='k', ls='-')

    # 'lower right'
    ax[0][1].legend(labelspacing=0.1, handlelength=2, borderpad=0.2,  # handletextpad=0.0, borderpad=0.2,
                    loc='upper left', frameon=False)  # , bbox_to_anchor=(-0.025, 1.04))
    ax[1][1].legend(labelspacing=0.1, handlelength=2, borderpad=0.2,  # handletextpad=0.0, borderpad=0.2,
                    loc='upper left', frameon=False)  # , bbox_to_anchor=(1.045, -0.06))
    ax[2][1].legend(labelspacing=0.1, handlelength=2, borderpad=0.2,  # handletextpad=0.0,
                    loc='upper left', frameon=False)  # , bbox_to_anchor=(1.04, -0.05))

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return


def plot_poster_indicator_n_dependence(save_name_append='diff', save=False, name=''):
    _index = 1

    lw=3

    min_mdm = 5  # 5.0
    max_mdm = 9.5
    m_dms = np.logspace(min_mdm, max_mdm, 100)
    N200s = 1.85 * 10 ** 12 / m_dms
    log_N200s = np.log10(N200s)
    N200_max = np.log10(1.85 * 10 ** 12 / 10 ** min_mdm)
    N200_min = np.log10(1.85 * 10 ** 12 / 10 ** max_mdm)

    m_DM_EAGLE = 9.70e6 #M_sun
    M200_EAGLE = m_DM_EAGLE * N200s
    log_M200_EAGLE = np.log10(M200_EAGLE)
    M200_max = np.log10(np.amax(M200_EAGLE))
    M200_min = np.log10(np.amin(M200_EAGLE))


    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(8.5, 4.5, forward=True)
    # widths = [1,1,1]
    widths = [1]
    heights = [1]
    spec = fig.add_gridspec(ncols=1, nrows=1, width_ratios=widths, height_ratios=heights)
    
    smol = 0.02
    ax = []
    for col in range(len(widths)):
        ax.append([])
        for row in range(len(heights)):
            ax[col].append(fig.add_subplot(spec[row, col]))

            ax[col][row].set_xlim([N200_min, N200_max])
            ax[col][row].set_ylim([1e-2, 2e1])

            ax[col][row].semilogy()

    axtwin = []

    ax[0][0].axhline(1, 0, 1, c='grey', ls=':')
    # ax[0][0].axhline(0.5, 0, 1, c='grey', ls=':')
    # ax[0][0].axhline(1/3, 0, 1, c='grey', ls=':')

    axtwin.append(ax[0][0].twiny())
    axtwin[0].set_xlim([M200_min, M200_max])

    axtwin[0].set_xlabel(r'$M_{200}$ at EAGLE resolution')
    ax[0][0].set_xlabel(r'$\log \, \mathrm{N}_{\mathrm{DM}}$')
    ax[0][0].set_ylabel(r'Maximum possible $v / \sigma$')

    # ax[0][0].set_yticks([0.4,0.6,0.8,1])

    max_T = 13.8
    # ax[0][0].text(N200_max - 0.2, 0.34, r'$\kappa_{\rm rot} = 1$ at $t=0$', ha='right', va='bottom')
    ax[0][0].text(N200_min + 0.02 * (N200_max- N200_min), 14,
                  r'Pure disk at $t=0$ and $R = R_{1/2}$',
                  ha='left', va='top')
    # ax[0][0].text(N200_max - 0.2, 0.44, r'$R = R_{1/2}$', ha='right', va='bottom')

    ####
    # get theory
    print('\nbeta=0, gamma=0 (fiducial)')
    (ln_Lambda_ks, alphas, betas, gammas
     ) = find_least_log_squares_best_fit(
        save_name_append, mcmc=False, fixed_b=0, fixed_g=0)

    # where to measure
    bin_edges = get_bin_edges(save_name_append)

    # all galaxies
    V200 = 200
    analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                             save_name_append=save_name_append)
    analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                         save_name_append=save_name_append)

    time = np.array([max_T])

    # concentration
    kappa_rot = np.zeros((len(bin_edges) // 2, 2, len(m_dms)))

    time = np.array([max_T])

    # for k, m_dm in enumerate(m_dms):
    #     for j, galaxy_name in enumerate(['mu_5_c_15/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                                      'mu_5_c_5/fdisk_0p01_lgMdm_8p0_V200-200kmps']):
    #
    #         V200 = 200
    #
    #         # work out constatns
    #         (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
    #          ) = get_constants(bin_edges, save_name_append, m_dm * 1e-10, galaxy_name=galaxy_name)
    #
    #         analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
    #                                              save_name_append=save_name_append, galaxy_name=galaxy_name)
    #
    #         # burn ins
    #         one = 0.01
    #         inital_velocity_v = np.sqrt(analytic_v_circ ** 2 - one ** 2 * np.ones(len(bin_edges) // 2))
    #         inital_velocity2_z = one * np.ones(len(bin_edges) // 2)
    #         inital_velocity2_r = one * np.ones(len(bin_edges) // 2)
    #         inital_velocity2_p = one * np.ones(len(bin_edges) // 2)
    #
    #         theory_best_v = np.zeros((len(bin_edges) // 2))
    #         theory_best_z2 = np.zeros((len(bin_edges) // 2))
    #         theory_best_r2 = np.zeros((len(bin_edges) // 2))
    #         theory_best_p2 = np.zeros((len(bin_edges) // 2))
    #
    #         # different times
    #         for i in range(len(bin_edges) // 2):
    #             theory_best_v[i] = get_theory_velocity2_array(V200,
    #                                                           analytic_v_circ[i] - inital_velocity_v[i],
    #                                                           time, t_c_dm[i], delta_dm[i], np.sqrt(upsilon_circ[i]),
    #                                                           ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
    #             theory_best_z2[i] = get_theory_velocity2_array(V200 ** 2, inital_velocity2_z[i],
    #                                                            time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
    #                                                            ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
    #             theory_best_r2[i] = get_theory_velocity2_array(V200 ** 2, inital_velocity2_r[i],
    #                                                            time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
    #                                                            ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
    #             theory_best_p2[i] = get_theory_velocity2_array(V200 ** 2, inital_velocity2_p[i],
    #                                                            time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
    #                                                            ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])
    #
    #         theory_best_v = inital_velocity_v - theory_best_v
    #
    #         # model indicators
    #         theory_best_tot2 = theory_best_z2 + theory_best_r2 + theory_best_p2
    #
    #         theory_best_kappa_rot = (theory_best_v ** 2 + theory_best_p2) / (
    #                 theory_best_v ** 2 + theory_best_tot2)
    #
    #         kappa_rot[:, j, k] = theory_best_kappa_rot
    #
    # ax[0][0].fill_between(log_N200s, kappa_rot[_index, 0], kappa_rot[_index, 1], color='k', alpha=0.3,
    #                       label=r'$c = 10 \pm 5$')
    # ax[0][0].fill_between(log_N200s, kappa_rot[_index, 0], kappa_rot[_index, 1], color='k', alpha=0.3)

    #different times
    time = np.array([2, 4, 8, max_T])
    # time = np.linspace(0, T_TOT, 100)

    v_on_sigma = np.zeros((len(bin_edges) // 2, len(time), len(m_dms)))

    #fiducial c=10 galaxy
    galaxy_name = 'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps'
    # analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
    #                                          save_name_append=save_name_append, galaxy_name=galaxy_name)
    analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                         save_name_append=save_name_append, galaxy_name=galaxy_name)
    for k, m_dm in enumerate(m_dms):

        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, m_dm * 1e-10, galaxy_name=galaxy_name)

        # burn ins
        one = 0.01
        inital_velocity_v = np.sqrt(analytic_v_circ ** 2 - one ** 2 * np.ones(len(bin_edges) // 2))
        inital_velocity2_z = one * np.ones(len(bin_edges) // 2)
        inital_velocity2_r = one * np.ones(len(bin_edges) // 2)
        inital_velocity2_p = one * np.ones(len(bin_edges) // 2)

        theory_best_v = np.zeros((len(bin_edges) // 2, len(time)))
        theory_best_z2 = np.zeros((len(bin_edges) // 2, len(time)))
        theory_best_r2 = np.zeros((len(bin_edges) // 2, len(time)))
        theory_best_p2 = np.zeros((len(bin_edges) // 2, len(time)))

        # different times
        for i in range(len(bin_edges) // 2):
            theory_best_v[i] = get_theory_velocity2_array(V200,
                                                          analytic_v_circ[i] - inital_velocity_v[i],
                                                          time, t_c_dm[i], delta_dm[i], np.sqrt(upsilon_circ[i]),
                                                          ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
            theory_best_z2[i] = get_theory_velocity2_array(V200 ** 2, inital_velocity2_z[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
            theory_best_r2[i] = get_theory_velocity2_array(V200 ** 2, inital_velocity2_r[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
            theory_best_p2[i] = get_theory_velocity2_array(V200 ** 2, inital_velocity2_p[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

        theory_best_v = inital_velocity_v[:, np.newaxis] - theory_best_v

        # model indicators
        theory_best_tot2 = theory_best_z2 + theory_best_r2 + theory_best_p2

        theory_best_v_on_sigma = theory_best_v / np.sqrt(theory_best_tot2 / 3)

        theory_best_kappa_rot = (theory_best_v ** 2 + theory_best_p2) / (
                theory_best_v ** 2 + theory_best_tot2)

        v_on_sigma[:, :, k] = theory_best_v_on_sigma

    # ax[0][0].plot(log_N200s, kappa_rot[_index, -1], c='k', ls='-', lw=lw,
    #               label=r'$\Delta t =$' + str(max_T) + ' Gyr')
    # ax[0][0].plot(log_N200s, kappa_rot[_index, 1], c='C1', ls='--', lw=lw,
    #               label=r'$\Delta t = 5$ Gyr')
    # ax[0][0].plot(log_N200s, kappa_rot[_index, 0], c='C2', ls='-.', lw=lw,
    #               label=r'$\Delta t = 2$ Gyr')

    [ax[0][0].plot(log_N200s, v_on_sigma[_index, j], c='C' + str(j), ls='-', lw=lw,
                   label=r'$t =$' + str(t) + ' Gyr') for j, t in enumerate(time)]

    # 'lower right'
    ax[0][0].legend(handlelength=1, handletextpad=0.4, frameon=False, labelspacing=0.05, #title_fontsize=19, fontsize=19,
                    columnspacing=0.3, borderpad=0.3, borderaxespad=0.1, loc='lower right')

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return


def plot_mass_mass_plot(save_name_append='diff', save=False, name=''):
    _index = 1

    # min_mdm = 5.01
    # max_mdm = 9.5
    min_mdm = -0.5
    max_mdm = 11.5
    m_dms = np.logspace(min_mdm, max_mdm)
    N200s = 1.85 * 10 ** 12 / m_dms
    log_N200s = np.log10(N200s)
    N200_min = np.log10(1.85 * 10 ** 12 / 10 ** min_mdm)
    N200_max = np.log10(1.85 * 10 ** 12 / 10 ** max_mdm)

    M200 = 1
    v200 = (M200 * 10 * GRAV_CONST * GALIC_HUBBLE_CONST) ** (1 / 3)

    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(12, 15, forward=True)
    fig.subplots_adjust(hspace=0.02, wspace=0.02)

    ax00 = plt.subplot(3, 2, 1)
    ax10 = plt.subplot(3, 2, 3)
    ax20 = plt.subplot(3, 2, 5)
    ax01 = plt.subplot(3, 2, 2)
    ax11 = plt.subplot(3, 2, 4)
    ax21 = plt.subplot(3, 2, 6)

    axs = [ax00, ax10, ax20, ax01, ax11, ax21]
    for i, ax in enumerate(axs):
        ax.set_xlim([min_mdm, max_mdm])
        ax.set_ylim([4.01, 16 - 0.01])

        ax.set_xticks([0, 2, 4, 6, 8, 10])

        if i < 3:
            ax.text(0, 15.5, r'$\Delta t = 13.8$ Gyr', va='top')
            ax.text(0, 14.5, r'$R = R_{1/2}$', va='top')
        else:
            ax.text(0, 15.5, r'$\Delta t = 2$ Gyr', va='top')
            ax.text(0, 14.5, r'$R = R_{1/2}$', va='top')

    ax20.set_xlabel(r'$\log ( m_{\mathrm{DM}} / \mathrm{M}_\odot )$')
    ax21.set_xlabel(r'$\log ( m_{\mathrm{DM}} / \mathrm{M}_\odot )$')
    ax00.set_ylabel(r'$\log ( M_{200} / \mathrm{M}_\odot )$')
    ax10.set_ylabel(r'$\log ( M_{200} / \mathrm{M}_\odot )$')
    ax20.set_ylabel(r'$\log ( M_{200} / \mathrm{M}_\odot )$')

    ax00.set_xticklabels([])
    ax10.set_xticklabels([])
    ax01.set_xticklabels([])
    ax11.set_xticklabels([])

    ax01.set_yticklabels([])
    ax11.set_yticklabels([])
    ax21.set_yticklabels([])

    ax20.set_xticks([0, 2, 4, 6, 8, 10])
    ax21.set_xticks([0, 2, 4, 6, 8, 10])

    max_T = 13.8

    ####
    # get theory
    print('\nbeta=0, gamma=0 (fiducial)')
    (ln_Lambda_ks, alphas, betas, gammas
     ) = find_least_log_squares_best_fit(
        save_name_append, mcmc=False, fixed_b=0, fixed_g=0)

    # where to measure
    bin_edges = get_bin_edges(save_name_append)

    # all galaxies
    analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                             save_name_append=save_name_append)
    analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                         save_name_append=save_name_append)

    # ic_heat_fraction = np.array([0.03333, 0.1, 0.3333, 1])
    # heat_fraction_labels = [r'$\sigma_i^2=0.03 \,\sigma_{DM}^2$', r'$\sigma_i^2=0.10 \,\sigma_{DM}^2$',
    #                         r'$\sigma_i^2=0.33 \,\sigma_{DM}^2$', r'$\sigma_i^2= \sigma_{DM}^2$']
    ic_heat_fraction = np.array([0.00001])
    heat_fraction_labels = [r'0']

    v_on_sigma_fracs = np.array([3.333, 1, 0.3333, 0.01])
    # kappa_fracs = 1 - np.array([0.025, 0.074, 0.22, 0.66])
    kappa_fracs = 1 - np.array([0.01, 0.33, 0.66])
    # bulge_to_total_fracs = np.array([0.03333,0.1,0.3333,0.99])
    bulge_to_total_fracs = np.array([0.01, 0.5, 0.99])

    ls_list = ['-', '-.', '--', ':']
    lw = 3

    time = np.array([max_T])

    v_on_sigma = np.zeros((len(bin_edges) // 2, len(m_dms)))
    kappa_rot = np.zeros((len(bin_edges) // 2, len(m_dms)))
    # bulge_to_total = np.zeros((len(bin_edges)//2, len(time), len(m_dms)))
    bulge_to_total = np.zeros(len(m_dms))

    # differet initial velocities
    for k, m_dm in enumerate(m_dms):

        # work out constatns
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, m_dm * 1e-10)

        # burn ins
        inital_velocity_v = np.sqrt((1 - ic_heat_fraction[0]) * analytic_v_circ ** 2)
        inital_velocity2_z = ic_heat_fraction[0] * (analytic_dispersion) ** 2
        inital_velocity2_r = ic_heat_fraction[0] * (analytic_dispersion) ** 2
        inital_velocity2_p = ic_heat_fraction[0] * (analytic_dispersion) ** 2

        theory_best_v = np.zeros(len(bin_edges) // 2)
        theory_best_z2 = np.zeros(len(bin_edges) // 2)
        theory_best_r2 = np.zeros(len(bin_edges) // 2)
        theory_best_p2 = np.zeros(len(bin_edges) // 2)

        # the actual thing
        for i in range(len(bin_edges) // 2):
            theory_best_v[i] = get_theory_velocity2_array(inital_velocity_v[i],
                                                          analytic_v_circ[i] - inital_velocity_v[i],
                                                          time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                          ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
            theory_best_z2[i] = get_theory_velocity2_array(analytic_dispersion[i] ** 2, inital_velocity2_z[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
            theory_best_r2[i] = get_theory_velocity2_array(analytic_dispersion[i] ** 2, inital_velocity2_r[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
            theory_best_p2[i] = get_theory_velocity2_array(analytic_dispersion[i] ** 2, inital_velocity2_p[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

        theory_best_v = inital_velocity_v - theory_best_v

        # model indicators
        theory_best_tot2 = theory_best_z2 + theory_best_r2 + theory_best_p2
        theory_best_v_on_sigma = theory_best_v / np.sqrt(theory_best_tot2 / 3)

        theory_best_kappa_rot = (theory_best_v ** 2 + theory_best_p2) / (
                theory_best_v ** 2 + theory_best_tot2)

        theory_bulge_to_total = 2 * scipy.stats.norm.cdf(0, loc=theory_best_v[_index],
                                                         scale=np.sqrt(theory_best_p2[_index]))

        v_on_sigma[:, k] = theory_best_v_on_sigma
        kappa_rot[:, k] = theory_best_kappa_rot
        bulge_to_total[k] = theory_bulge_to_total

    v_on_sigma_interp = InterpolatedUnivariateSpline(m_dms, v_on_sigma[_index])
    kappa_interp = InterpolatedUnivariateSpline(m_dms, kappa_rot[_index])
    bulge_to_total_interp = InterpolatedUnivariateSpline(m_dms, bulge_to_total)

    N200_v_on_sigma_fracs = np.zeros(len(v_on_sigma_fracs))
    N200_kappa_fracs = np.zeros(len(kappa_fracs))
    N200_bulge_to_total_fracs = np.zeros(len(bulge_to_total_fracs))

    for i, v_on_sigma_frac in enumerate(v_on_sigma_fracs):
        out = brentq(lambda m: v_on_sigma_interp(m) - v_on_sigma_frac, 10 ** min_mdm, 10 ** max_mdm)
        # print(np.log10(out))
        N200_v_on_sigma_fracs[i] = (1.85 * 10 ** 12) / out
    for i, N200_v_on_sigma_frac in enumerate(N200_v_on_sigma_fracs):
        ax00.errorbar(np.log10(m_dms), np.log10(m_dms * N200_v_on_sigma_frac), ls=ls_list[i], lw=lw,
                      label=r'$v/\sigma = $' + str(round(v_on_sigma_fracs[i], 2)))

    for i, kappa_frac in enumerate(kappa_fracs):
        out = brentq(lambda m: kappa_interp(m) - kappa_frac, 10 ** min_mdm, 10 ** max_mdm)
        # print(np.log10(out))
        N200_kappa_fracs[i] = (1.85 * 10 ** 12) / out
    for i, N200_kappa_frac in enumerate(N200_kappa_fracs):
        ax10.errorbar(np.log10(m_dms), np.log10(m_dms * N200_kappa_frac), ls=ls_list[i], lw=lw,
                      label=r'$\Delta \kappa_{\mathrm{rot}} = $' + str(round(1 - kappa_fracs[i], 2)))

    for i, bulge_to_total_frac in enumerate(bulge_to_total_fracs):
        out = brentq(lambda m: bulge_to_total_interp(m) - bulge_to_total_frac, 10 ** min_mdm, 10 ** max_mdm)
        # print(np.log10(out))
        N200_bulge_to_total_fracs[i] = (1.85 * 10 ** 12) / out
    for i, N200_bulge_to_total_frac in enumerate(N200_bulge_to_total_fracs):
        ax20.errorbar(np.log10(m_dms), np.log10(m_dms * N200_bulge_to_total_frac), ls=ls_list[i], lw=lw, \
                      label=r'$\Delta \mathrm{S/T} = $' + str(round(bulge_to_total_fracs[i], 2)))

    # concentration
    v_on_sigma = np.zeros((len(bin_edges) // 2, 2, len(m_dms)))
    kappa_rot = np.zeros((len(bin_edges) // 2, 2, len(m_dms)))
    # bulge_to_total = np.zeros((len(bin_edges)//2, len(time), len(m_dms)))
    bulge_to_total = np.zeros((2, len(m_dms)))

    for k, m_dm in enumerate(m_dms):
        galaxy_name = 'mu_5_c_15/fdisk_0p01_lgMdm_8p0_V200-200kmps'
        # work out constatns
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, m_dm * 1e-10, galaxy_name=galaxy_name)

        analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True, \
                                                 save_name_append=save_name_append, galaxy_name=galaxy_name)
        analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)

        # burn ins
        one = 0.01
        inital_velocity_v = np.sqrt(analytic_v_circ ** 2 - one ** 2 * np.ones(len(bin_edges) // 2))
        inital_velocity2_z = one * np.ones(len(bin_edges) // 2)
        inital_velocity2_r = one * np.ones(len(bin_edges) // 2)
        inital_velocity2_p = one * np.ones(len(bin_edges) // 2)

        theory_best_v = np.zeros((len(bin_edges) // 2))
        theory_best_z2 = np.zeros((len(bin_edges) // 2))
        theory_best_r2 = np.zeros((len(bin_edges) // 2))
        theory_best_p2 = np.zeros((len(bin_edges) // 2))

        # different times
        for i in range(len(bin_edges) // 2):
            theory_best_v[i] = get_theory_velocity2_array(inital_velocity_v[i],
                                                          analytic_v_circ[i] - inital_velocity_v[i],
                                                          time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                          ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
            theory_best_z2[i] = get_theory_velocity2_array(analytic_dispersion[i] ** 2, inital_velocity2_z[i], \
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i], \
                                                           ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
            theory_best_r2[i] = get_theory_velocity2_array(analytic_dispersion[i] ** 2, inital_velocity2_r[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
            theory_best_p2[i] = get_theory_velocity2_array(analytic_dispersion[i] ** 2, inital_velocity2_p[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

        theory_best_v = inital_velocity_v - theory_best_v

        # model indicators
        theory_best_tot2 = theory_best_z2 + theory_best_r2 + theory_best_p2
        theory_best_v_on_sigma = theory_best_v / np.sqrt(theory_best_tot2 / 3)

        theory_best_kappa_rot = (theory_best_v ** 2 + theory_best_p2) / (
                theory_best_v ** 2 + theory_best_tot2)

        theory_bulge_to_total = 2 * scipy.stats.norm.cdf(0, loc=theory_best_v[_index],
                                                         scale=np.sqrt(theory_best_p2[_index]))

        v_on_sigma[:, 0, k] = theory_best_v_on_sigma
        kappa_rot[:, 0, k] = theory_best_kappa_rot
        bulge_to_total[0, k] = theory_bulge_to_total

        galaxy_name = 'mu_5_c_5/fdisk_0p01_lgMdm_8p0_V200-200kmps'
        # work out constatns
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, m_dm * 1e-10, galaxy_name=galaxy_name)

        analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                                 save_name_append=save_name_append, galaxy_name=galaxy_name)
        analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)

        # burn ins
        one = 0.01
        inital_velocity_v = np.sqrt(analytic_v_circ ** 2 - one ** 2 * np.ones(len(bin_edges) // 2))
        inital_velocity2_z = one * np.ones(len(bin_edges) // 2)
        inital_velocity2_r = one * np.ones(len(bin_edges) // 2)
        inital_velocity2_p = one * np.ones(len(bin_edges) // 2)

        theory_best_v = np.zeros((len(bin_edges) // 2))
        theory_best_z2 = np.zeros((len(bin_edges) // 2))
        theory_best_r2 = np.zeros((len(bin_edges) // 2))
        theory_best_p2 = np.zeros((len(bin_edges) // 2))

        # different times
        for i in range(len(bin_edges) // 2):
            theory_best_v[i] = get_theory_velocity2_array(inital_velocity_v[i],
                                                          analytic_v_circ[i] - inital_velocity_v[i],
                                                          time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                          ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
            theory_best_z2[i] = get_theory_velocity2_array(analytic_dispersion[i] ** 2, inital_velocity2_z[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
            theory_best_r2[i] = get_theory_velocity2_array(analytic_dispersion[i] ** 2, inital_velocity2_r[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
            theory_best_p2[i] = get_theory_velocity2_array(analytic_dispersion[i] ** 2, inital_velocity2_p[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

        theory_best_v = inital_velocity_v - theory_best_v

        # model indicators
        theory_best_tot2 = theory_best_z2 + theory_best_r2 + theory_best_p2
        theory_best_v_on_sigma = theory_best_v / np.sqrt(theory_best_tot2 / 3)

        theory_best_kappa_rot = (theory_best_v ** 2 + theory_best_p2) / (
                theory_best_v ** 2 + theory_best_tot2)

        theory_bulge_to_total = 2 * scipy.stats.norm.cdf(0, loc=theory_best_v[_index],
                                                         scale=np.sqrt(theory_best_p2[_index]))

        v_on_sigma[:, 1, k] = theory_best_v_on_sigma
        kappa_rot[:, 1, k] = theory_best_kappa_rot
        bulge_to_total[1, k] = theory_bulge_to_total

    v_on_sigma_interp_min = InterpolatedUnivariateSpline(m_dms, v_on_sigma[_index, 0])
    kappa_interp_min = InterpolatedUnivariateSpline(m_dms, kappa_rot[_index, 0])
    bulge_to_total_interp_min = InterpolatedUnivariateSpline(m_dms, bulge_to_total[0])

    v_on_sigma_interp_max = InterpolatedUnivariateSpline(m_dms, v_on_sigma[_index, 1])
    kappa_interp_max = InterpolatedUnivariateSpline(m_dms, kappa_rot[_index, 1])
    bulge_to_total_interp_max = InterpolatedUnivariateSpline(m_dms, bulge_to_total[1])

    alpha = 0.3

    for i, v_on_sigma_frac in enumerate(v_on_sigma_fracs):
        if i == 0:
            out = brentq(lambda m: v_on_sigma_interp_min(m) - v_on_sigma_frac, 10 ** min_mdm, 10 ** max_mdm)
            N200__min = (1.85 * 10 ** 12) / out
            out = brentq(lambda m: v_on_sigma_interp_max(m) - v_on_sigma_frac, 10 ** min_mdm, 10 ** max_mdm)
            N200__max = (1.85 * 10 ** 12) / out

            ax00.fill_between(np.log10(m_dms), np.log10(m_dms * N200__min), np.log10(m_dms * N200__max),
                              color='k', alpha=alpha, zorder=-2, label=r'$c=10 \pm 5$')

    for i, kappa_frac in enumerate(kappa_fracs):
        if i == 0:
            out = brentq(lambda m: kappa_interp_min(m) - kappa_frac, 10 ** min_mdm, 10 ** max_mdm)
            N200__min = (1.85 * 10 ** 12) / out
            out = brentq(lambda m: kappa_interp_max(m) - kappa_frac, 10 ** min_mdm, 10 ** max_mdm)
            N200__max = (1.85 * 10 ** 12) / out

            ax10.fill_between(np.log10(m_dms), np.log10(m_dms * N200__min), np.log10(m_dms * N200__max),
                              color='k', alpha=alpha, zorder=-2, label=r'$c=10 \pm 5$')

    for i, bulge_to_total_frac in enumerate(bulge_to_total_fracs):
        if i == 0:
            out = brentq(lambda m: bulge_to_total_interp_min(m) - bulge_to_total_frac, 10 ** min_mdm, 10 ** max_mdm)
            N200__min = (1.85 * 10 ** 12) / out
            out = brentq(lambda m: bulge_to_total_interp_max(m) - bulge_to_total_frac, 10 ** min_mdm, 10 ** max_mdm)
            N200__max = (1.85 * 10 ** 12) / out

            ax20.fill_between(np.log10(m_dms), np.log10(m_dms * N200__min), np.log10(m_dms * N200__max),
                              color='k', alpha=alpha, zorder=-2, label=r'$c=10 \pm 5$')

    ####
    analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                             save_name_append=save_name_append)
    analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                         save_name_append=save_name_append)

    time = np.array([2])

    v_on_sigma = np.zeros((len(bin_edges) // 2, len(m_dms)))
    kappa_rot = np.zeros((len(bin_edges) // 2, len(m_dms)))
    # bulge_to_total = np.zeros((len(bin_edges)//2, len(time), len(m_dms)))
    bulge_to_total = np.zeros(len(m_dms))

    # differet initial velocities
    for k, m_dm in enumerate(m_dms):

        # work out constatns
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, m_dm * 1e-10)

        # burn ins
        inital_velocity_v = np.sqrt((1 - ic_heat_fraction[0]) * analytic_v_circ ** 2)
        inital_velocity2_z = ic_heat_fraction[0] * (analytic_dispersion) ** 2
        inital_velocity2_r = ic_heat_fraction[0] * (analytic_dispersion) ** 2
        inital_velocity2_p = ic_heat_fraction[0] * (analytic_dispersion) ** 2

        theory_best_v = np.zeros(len(bin_edges) // 2)
        theory_best_z2 = np.zeros(len(bin_edges) // 2)
        theory_best_r2 = np.zeros(len(bin_edges) // 2)
        theory_best_p2 = np.zeros(len(bin_edges) // 2)

        # the actual thing
        for i in range(len(bin_edges) // 2):
            theory_best_v[i] = get_theory_velocity2_array(inital_velocity_v[i],
                                                          analytic_v_circ[i] - inital_velocity_v[i],
                                                          time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                          ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
            theory_best_z2[i] = get_theory_velocity2_array(analytic_dispersion[i] ** 2, inital_velocity2_z[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
            theory_best_r2[i] = get_theory_velocity2_array(analytic_dispersion[i] ** 2, inital_velocity2_r[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
            theory_best_p2[i] = get_theory_velocity2_array(analytic_dispersion[i] ** 2, inital_velocity2_p[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

        theory_best_v = inital_velocity_v - theory_best_v

        # model indicators
        theory_best_tot2 = theory_best_z2 + theory_best_r2 + theory_best_p2
        theory_best_v_on_sigma = theory_best_v / np.sqrt(theory_best_tot2 / 3)

        theory_best_kappa_rot = (theory_best_v ** 2 + theory_best_p2) / (
                theory_best_v ** 2 + theory_best_tot2)

        theory_bulge_to_total = 2 * scipy.stats.norm.cdf(0, loc=theory_best_v[_index],
                                                         scale=np.sqrt(theory_best_p2[_index]))

        v_on_sigma[:, k] = theory_best_v_on_sigma
        kappa_rot[:, k] = theory_best_kappa_rot
        bulge_to_total[k] = theory_bulge_to_total

    v_on_sigma_interp = InterpolatedUnivariateSpline(m_dms, v_on_sigma[_index])
    kappa_interp = InterpolatedUnivariateSpline(m_dms, kappa_rot[_index])
    bulge_to_total_interp = InterpolatedUnivariateSpline(m_dms, bulge_to_total)

    N200_v_on_sigma_fracs = np.zeros(len(v_on_sigma_fracs))
    N200_kappa_fracs = np.zeros(len(kappa_fracs))
    N200_bulge_to_total_fracs = np.zeros(len(bulge_to_total_fracs))

    for i, v_on_sigma_frac in enumerate(v_on_sigma_fracs):
        out = brentq(lambda m: v_on_sigma_interp(m) - v_on_sigma_frac, 10 ** min_mdm, 10 ** max_mdm)
        # print(np.log10(out))
        N200_v_on_sigma_fracs[i] = (1.85 * 10 ** 12) / out
    for i, N200_v_on_sigma_frac in enumerate(N200_v_on_sigma_fracs):
        ax01.errorbar(np.log10(m_dms), np.log10(m_dms * N200_v_on_sigma_frac), ls=ls_list[i], lw=lw)

    for i, kappa_frac in enumerate(kappa_fracs):
        out = brentq(lambda m: kappa_interp(m) - kappa_frac, 10 ** min_mdm, 10 ** max_mdm)
        # print(np.log10(out))
        N200_kappa_fracs[i] = (1.85 * 10 ** 12) / out
    for i, N200_kappa_frac in enumerate(N200_kappa_fracs):
        ax11.errorbar(np.log10(m_dms), np.log10(m_dms * N200_kappa_frac), ls=ls_list[i], lw=lw)

    for i, bulge_to_total_frac in enumerate(bulge_to_total_fracs):
        out = brentq(lambda m: bulge_to_total_interp(m) - bulge_to_total_frac, 10 ** min_mdm, 10 ** max_mdm)
        # print(np.log10(out))
        N200_bulge_to_total_fracs[i] = (1.85 * 10 ** 12) / out
    for i, N200_bulge_to_total_frac in enumerate(N200_bulge_to_total_fracs):
        ax21.errorbar(np.log10(m_dms), np.log10(m_dms * N200_bulge_to_total_frac), ls=ls_list[i], lw=lw)

    # concentration
    v_on_sigma = np.zeros((len(bin_edges) // 2, 2, len(m_dms)))
    kappa_rot = np.zeros((len(bin_edges) // 2, 2, len(m_dms)))
    # bulge_to_total = np.zeros((len(bin_edges)//2, len(time), len(m_dms)))
    bulge_to_total = np.zeros((2, len(m_dms)))

    for k, m_dm in enumerate(m_dms):
        galaxy_name = 'mu_5_c_15/fdisk_0p01_lgMdm_8p0_V200-200kmps'
        # work out constatns
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, m_dm * 1e-10, galaxy_name=galaxy_name)

        analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                                 save_name_append=save_name_append, galaxy_name=galaxy_name)
        analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)

        # burn ins
        one = 0.01
        inital_velocity_v = np.sqrt(analytic_v_circ ** 2 - one ** 2 * np.ones(len(bin_edges) // 2))
        inital_velocity2_z = one * np.ones(len(bin_edges) // 2)
        inital_velocity2_r = one * np.ones(len(bin_edges) // 2)
        inital_velocity2_p = one * np.ones(len(bin_edges) // 2)

        theory_best_v = np.zeros((len(bin_edges) // 2))
        theory_best_z2 = np.zeros((len(bin_edges) // 2))
        theory_best_r2 = np.zeros((len(bin_edges) // 2))
        theory_best_p2 = np.zeros((len(bin_edges) // 2))

        # different times
        for i in range(len(bin_edges) // 2):
            theory_best_v[i] = get_theory_velocity2_array(inital_velocity_v[i],
                                                          analytic_v_circ[i] - inital_velocity_v[i],
                                                          time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                          ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
            theory_best_z2[i] = get_theory_velocity2_array(analytic_dispersion[i] ** 2, inital_velocity2_z[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
            theory_best_r2[i] = get_theory_velocity2_array(analytic_dispersion[i] ** 2, inital_velocity2_r[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
            theory_best_p2[i] = get_theory_velocity2_array(analytic_dispersion[i] ** 2, inital_velocity2_p[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

        theory_best_v = inital_velocity_v - theory_best_v

        # model indicators
        theory_best_tot2 = theory_best_z2 + theory_best_r2 + theory_best_p2
        theory_best_v_on_sigma = theory_best_v / np.sqrt(theory_best_tot2 / 3)

        theory_best_kappa_rot = (theory_best_v ** 2 + theory_best_p2) / (
                theory_best_v ** 2 + theory_best_tot2)

        theory_bulge_to_total = 2 * scipy.stats.norm.cdf(0, loc=theory_best_v[_index],
                                                         scale=np.sqrt(theory_best_p2[_index]))

        v_on_sigma[:, 0, k] = theory_best_v_on_sigma
        kappa_rot[:, 0, k] = theory_best_kappa_rot
        bulge_to_total[0, k] = theory_bulge_to_total

        galaxy_name = 'mu_5_c_5/fdisk_0p01_lgMdm_8p0_V200-200kmps'
        # work out constatns
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, m_dm * 1e-10, galaxy_name=galaxy_name)

        analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                                 save_name_append=save_name_append, galaxy_name=galaxy_name)
        analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)

        # burn ins
        one = 0.01
        inital_velocity_v = np.sqrt(analytic_v_circ ** 2 - one ** 2 * np.ones(len(bin_edges) // 2))
        inital_velocity2_z = one * np.ones(len(bin_edges) // 2)
        inital_velocity2_r = one * np.ones(len(bin_edges) // 2)
        inital_velocity2_p = one * np.ones(len(bin_edges) // 2)

        theory_best_v = np.zeros((len(bin_edges) // 2))
        theory_best_z2 = np.zeros((len(bin_edges) // 2))
        theory_best_r2 = np.zeros((len(bin_edges) // 2))
        theory_best_p2 = np.zeros((len(bin_edges) // 2))

        # different times
        for i in range(len(bin_edges) // 2):
            theory_best_v[i] = get_theory_velocity2_array(inital_velocity_v[i],
                                                          analytic_v_circ[i] - inital_velocity_v[i],
                                                          time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                          ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
            theory_best_z2[i] = get_theory_velocity2_array(analytic_dispersion[i] ** 2, inital_velocity2_z[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
            theory_best_r2[i] = get_theory_velocity2_array(analytic_dispersion[i] ** 2, inital_velocity2_r[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
            theory_best_p2[i] = get_theory_velocity2_array(analytic_dispersion[i] ** 2, inital_velocity2_p[i],
                                                           time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                           ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

        theory_best_v = inital_velocity_v - theory_best_v

        # model indicators
        theory_best_tot2 = theory_best_z2 + theory_best_r2 + theory_best_p2
        theory_best_v_on_sigma = theory_best_v / np.sqrt(theory_best_tot2 / 3)

        theory_best_kappa_rot = (theory_best_v ** 2 + theory_best_p2) / (
                theory_best_v ** 2 + theory_best_tot2)

        theory_bulge_to_total = 2 * scipy.stats.norm.cdf(0, loc=theory_best_v[_index],
                                                         scale=np.sqrt(theory_best_p2[_index]))

        v_on_sigma[:, 1, k] = theory_best_v_on_sigma
        kappa_rot[:, 1, k] = theory_best_kappa_rot
        bulge_to_total[1, k] = theory_bulge_to_total

    v_on_sigma_interp_min = InterpolatedUnivariateSpline(m_dms, v_on_sigma[_index, 0])
    kappa_interp_min = InterpolatedUnivariateSpline(m_dms, kappa_rot[_index, 0])
    bulge_to_total_interp_min = InterpolatedUnivariateSpline(m_dms, bulge_to_total[0])

    v_on_sigma_interp_max = InterpolatedUnivariateSpline(m_dms, v_on_sigma[_index, 1])
    kappa_interp_max = InterpolatedUnivariateSpline(m_dms, kappa_rot[_index, 1])
    bulge_to_total_interp_max = InterpolatedUnivariateSpline(m_dms, bulge_to_total[1])

    alpha = 0.3

    for i, v_on_sigma_frac in enumerate(v_on_sigma_fracs):
        if i == 0:
            out = brentq(lambda m: v_on_sigma_interp_min(m) - v_on_sigma_frac, 10 ** min_mdm, 10 ** max_mdm)
            N200__min = (1.85 * 10 ** 12) / out
            out = brentq(lambda m: v_on_sigma_interp_max(m) - v_on_sigma_frac, 10 ** min_mdm, 10 ** max_mdm)
            N200__max = (1.85 * 10 ** 12) / out

            ax01.fill_between(np.log10(m_dms), np.log10(m_dms * N200__min), np.log10(m_dms * N200__max),
                              color='k', alpha=alpha, zorder=-2, label=r'$c=10 \pm 5$')

    for i, kappa_frac in enumerate(kappa_fracs):
        if i == 0:
            out = brentq(lambda m: kappa_interp_min(m) - kappa_frac, 10 ** min_mdm, 10 ** max_mdm)
            N200__min = (1.85 * 10 ** 12) / out
            out = brentq(lambda m: kappa_interp_max(m) - kappa_frac, 10 ** min_mdm, 10 ** max_mdm)
            N200__max = (1.85 * 10 ** 12) / out

            ax11.fill_between(np.log10(m_dms), np.log10(m_dms * N200__min), np.log10(m_dms * N200__max),
                              color='k', alpha=alpha, zorder=-2, label=r'$c=10 \pm 5$')

    for i, bulge_to_total_frac in enumerate(bulge_to_total_fracs):
        if i == 0:
            out = brentq(lambda m: bulge_to_total_interp_min(m) - bulge_to_total_frac, 10 ** min_mdm, 10 ** max_mdm)
            N200__min = (1.85 * 10 ** 12) / out
            out = brentq(lambda m: bulge_to_total_interp_max(m) - bulge_to_total_frac, 10 ** min_mdm, 10 ** max_mdm)
            N200__max = (1.85 * 10 ** 12) / out

            ax21.fill_between(np.log10(m_dms), np.log10(m_dms * N200__min), np.log10(m_dms * N200__max),
                              color='k', alpha=alpha, zorder=-2, label=r'$c=10 \pm 5$')

    # handletextpad=0.0, borderpad=0.2,
    ax00.legend(frameon=False, labelspacing=0.1, handlelength=2, borderpad=0.1, loc='lower right')
    ax10.legend(frameon=False, labelspacing=0.1, handlelength=2, borderpad=0.1, loc='lower right')
    ax20.legend(frameon=False, labelspacing=0.1, handlelength=2, borderpad=0.1, loc='lower right')

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    return


def plot_v_phi_profile(save_name_append='diff', name='', save=False):
    np.seterr(all='ignore')

    _index = 1
    dex = 0.2  # 0.2

    n_x = 4
    n_y = 3 * 5

    # snaps_to_plot = [0, BURN_IN, 100]
    # snaps_to_plot = [0, 15, 100]
    log_taus_to_plot = [-3, -2.5, -2, -1.5, -1]

    # max_phi = 400
    # min_phi = -200
    # phi_bins = np.linspace(min_phi, max_phi, 61)

    n_bins = 51
    n_bins_from_n_points = lambda n_points: int(np.ceil(np.sqrt(n_points)))
    phi_bins_j = [lambda n_points: np.linspace(100, 280, n_bins_from_n_points(n_points)),
                  lambda n_points: np.linspace(50, 310, n_bins_from_n_points(n_points)),
                  lambda n_points: np.linspace(-50, 340, n_bins_from_n_points(n_points)),
                  lambda n_points: np.linspace(-200, 370, n_bins_from_n_points(n_points)),
                  lambda n_points: np.linspace(-300, 400, n_bins_from_n_points(n_points))]
    z_bins_j = [lambda n_points: np.linspace(-100, 100, n_bins_from_n_points(n_points)), \
                lambda n_points: np.linspace(-150, 150, n_bins_from_n_points(n_points)),
                lambda n_points: np.linspace(-200, 200, n_bins_from_n_points(n_points)),
                lambda n_points: np.linspace(-250, 250, n_bins_from_n_points(n_points)),
                lambda n_points: np.linspace(-300, 300, n_bins_from_n_points(n_points))]

    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(4 * n_x, 3 * n_y, forward=True)
    widths = [1] * n_x
    heights = [1] * n_y
    spec = fig.add_gridspec(ncols=n_x, nrows=n_y, width_ratios=widths,
                            height_ratios=heights)

    small_plot = 0.008
    axs = []
    for row in range(n_y):
        axs.append([])
        for col in range(n_x):
            axs[row].append(fig.add_subplot(spec[row, col]))
            # axs[row][col].set_xlim([min_phi, max_phi])
            if col == 0:
                pass  # axs[row][col].set_ylim([-3.5, 0.5])
            elif col == 1:
                axs[row][col].set_ylim([0 - small_plot, 1 + small_plot])
            elif col == 2:
                axs[row][col].set_ylim([-0.05 - small_plot, 0.05 + small_plot])
            elif col == 3:
                axs[row][col].set_ylim([-0.05 - small_plot, 0.05 + small_plot])
            # elif col == 3: axs[row][col].set_ylim([-1, 1])
            # elif col == 2: axs[row][col].set_ylim([-0.5-small_plot, 2+small_plot])
            # elif col == 3: axs[row][col].set_ylim([-0.5-small_plot, 2+small_plot])
            # if col != 0: axs[row][col].set_yticklabels([])
            if col == 0: axs[row][col].set_yticklabels([])
            # if row != n_y-1: axs[row][col].set_xticklabels([])
            axs[row][col].tick_params(axis='y', which='major', pad=5)
            axs[row][col].tick_params(axis='x', which='major', pad=10)

    # fig.subplots_adjust(hspace=0.01,wspace=0.01)
    fig.subplots_adjust(wspace=0.45, hspace=0.26)

    axs[(n_y - 1) // 2][0].set_ylabel(r'Probability Density [arbitrary units]')
    axs[(n_y - 1) // 2][1].set_ylabel(r'Cumulative Distribution')
    axs[(n_y - 1) // 2][2].set_ylabel(r'Cumulative Data - Cumulative Maxwellian (Heating Model)')
    axs[(n_y - 1) // 2][3].set_ylabel(r'Cumulative Data - Cumulative Maxwellian (Best Fit to Current Snapshot)')

    axs[n_y - 1][0].set_xlabel(r'$v$ [km/s]')
    axs[n_y - 1][1].set_xlabel(r'$v$ [km/s]')
    axs[n_y - 1][2].set_xlabel(r'$v$ [km/s]')
    axs[n_y - 1][3].set_xlabel(r'$v$ [km/s]')

    bin_edges = get_bin_edges(save_name_append)
    bin_centre = get_bin_edges('cum')[2 * _index + 1]

    analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0,
                                             v200=False, dm_dispersion=False, dm_v_circ=False,
                                             analytic_dispersion=True, analytic_v_c=False,
                                             save_name_append=save_name_append)

    analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0,
                                         v200=False, dm_dispersion=False, dm_v_circ=False,
                                         analytic_dispersion=False, analytic_v_c=True,
                                         save_name_append=save_name_append)

    grouped_galaxy_names = [['mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                             'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                             'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps'],
                            ['mu_25/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                             'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                             'mu_1/fdisk_0p01_lgMdm_7p5_V200-200kmps'],
                            ['mu_25/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                             'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                             'mu_1/fdisk_0p01_lgMdm_7p0_V200-200kmps'],
                            ['mu_25/fdisk_0p01_lgMdm_6p5_V200-200kmps',
                             'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
                             'mu_1/fdisk_0p01_lgMdm_6p5_V200-200kmps'],
                            ['mu_25/fdisk_0p01_lgMdm_6p0_V200-200kmps',
                             'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps']]

    grouped_snaps = [[400, 400, 400], [400, 400, 400],
                     [101, 101, 101], [101, 101, 101], [101, 101]]

    for i, (group_names, group_snaps) in enumerate(zip(grouped_galaxy_names, grouped_snaps)):

        for k, (galaxy_name, snaps) in enumerate(zip(group_names, group_snaps)):
            kwargs = get_name_kwargs(galaxy_name)
            # print(galaxy_name)

            (mean_v_R, mean_v_phi,
             sigma_z, sigma_R, sigma_phi
             ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges)

            # theory
            # time is complex to go backwards in time
            time = np.linspace(0, T_TOT, snaps + 1)  # , dtype=np.complex128)
            time = time - time[BURN_IN]

            # work out constatns
            (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
             ) = get_constants(bin_edges, save_name_append, kwargs['mdm'])

            # tau_edges = (np.linspace(0, T_TOT, snaps+2) - time[BURN_IN]) / t_c_dm[_index]
            # log_tau_edges = np.log10(tau_edges)
            # log_tau_edges[0] = log_tau_edges[1]-1e-5
            # log_tau_centres = np.log10(0.5*(tau_edges[1:] + tau_edges[:-1]))
            log_taus = np.log10(time / t_c_dm[_index])

            (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
             ) = get_physical_from_model(galaxy_name, snaps, save_name_append)

            for j, log_tau_to_plot in enumerate(log_taus_to_plot):
                if log_taus[-1] + 0.005 > log_tau_to_plot:
                    snap = np.nanargmin(np.abs(log_taus - log_tau_to_plot))

                    # load a snap
                    (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
                     MassStar, PosStars, VelStars, IDStars, PotStars
                     ) = load_nbody.load_and_align(galaxy_name, snap='{0:03d}'.format(int(snap)))

                    (R, phi, z, v_R, v_phi, v_z) = get_cylindrical(PosStars, VelStars)

                    experiment_bin_edges = get_dex_bins([bin_centre], dex)
                    mask = np.logical_and(R > experiment_bin_edges[0], R < experiment_bin_edges[1])

                    # # #Energy bins
                    # # EnergyStars = PotStars + 0.5 * np.sum(VelStars**2, axis=1)
                    # rStars = np.linalg.norm(PosStars, axis=1)
                    # pot_star_anal = (hernquist_potential(rStars) +
                    #                  get_exponential_disk_potential(rStars))
                    # EnergyStars = pot_star_anal + 0.5 * np.sum(VelStars**2, axis=1)
                    #
                    # average_energy_in_bin = np.mean(EnergyStars[mask])
                    # energy_bins = get_dex_bins([average_energy_in_bin], dex/10)
                    # mask = np.logical_and(EnergyStars > energy_bins[1], EnergyStars < energy_bins[0])

                    # z angular momentum bins
                    # LzStars = R * v_phi
                    #
                    # average_lz_in_bin = np.mean(LzStars[mask])
                    # lz_bins = get_dex_bins([average_lz_in_bin], dex*10)
                    # mask = np.logical_and(LzStars > lz_bins[1], LzStars < lz_bins[0])

                    n_points = np.sum(mask)
                    binned_phi = v_phi[mask]
                    binned_R = v_R[mask]
                    binned_z = v_z[mask]

                    # pdf
                    axs[3 * j][0].hist(binned_phi, phi_bins_j[j](n_points), density=True, histtype='step',
                                       ls=kwargs['ls'], color=kwargs['c'])
                    # R
                    axs[3 * j + 1][0].hist(binned_R, z_bins_j[j](n_points), density=True, histtype='step',
                                           ls=kwargs['ls'], color=kwargs['c'])
                    # z
                    axs[3 * j + 2][0].hist(binned_z, z_bins_j[j](n_points), density=True, histtype='step',
                                           ls=kwargs['ls'], color=kwargs['c'])

                    # cdf
                    binned_phi = np.sort(binned_phi)
                    axs[3 * j][1].plot(binned_phi, np.linspace(0, 1, len(binned_phi)),
                                       ls=kwargs['ls'], c=kwargs['c'])
                    # R
                    binned_R = np.sort(binned_R)
                    axs[3 * j + 1][1].plot(binned_R, np.linspace(0, 1, len(binned_R)),
                                           ls=kwargs['ls'], c=kwargs['c'])
                    # z
                    binned_z = np.sort(binned_z)
                    axs[3 * j + 2][1].plot(binned_z, np.linspace(0, 1, len(binned_z)),
                                           ls=kwargs['ls'], c=kwargs['c'])

            # cdf data - model
                    axs[3 * j][2].plot(binned_phi, np.linspace(0, 1, len(binned_phi)) - scipy.stats.norm.cdf(
                        binned_phi, theory_best_v[_index, snap], np.sqrt(theory_best_p2[_index, snap])),
                                       ls=kwargs['ls'], c=kwargs['c'])
                    # R
                    axs[3 * j + 1][2].plot(binned_R, np.linspace(0, 1, len(binned_R)) - scipy.stats.norm.cdf(
                        binned_R, 0, np.sqrt(theory_best_r2[_index, snap])),
                                           ls=kwargs['ls'], c=kwargs['c'])
                    # z
                    axs[3 * j + 2][2].plot(binned_z, np.linspace(0, 1, len(binned_z)) - scipy.stats.norm.cdf(
                        binned_z, 0, np.sqrt(theory_best_z2[_index, snap])),
                                           ls=kwargs['ls'], c=kwargs['c'])

                    # cdf data - best fit
                    axs[3 * j][3].plot(binned_phi, np.linspace(0, 1, len(binned_phi)) - scipy.stats.norm.cdf(
                        binned_phi, np.mean(binned_phi), np.std(binned_phi)),
                                       ls=kwargs['ls'], c=kwargs['c'])

                    # axs[3*j][3].plot(binned_phi, (np.linspace(0,1,len(binned_phi)) - scipy.stats.norm.cdf(
                    #   binned_phi, np.mean(binned_phi), np.std(binned_phi))) / (scipy.stats.norm.cdf(
                    #   binned_phi, np.mean(binned_phi), np.std(binned_phi))),
                    #                    ls=kwargs['ls'], c=kwargs['c'])
                    # #R
                    # axs[3*j+1][3].plot(binned_R, (np.linspace(0,1,len(binned_R)) - scipy.stats.norm.cdf(
                    #   binned_R, np.mean(binned_R), np.std(binned_R))) / scipy.stats.norm.cdf(
                    #   binned_R, np.mean(binned_R), np.std(binned_R)),
                    #                    ls=kwargs['ls'], c=kwargs['c'])

                    axs[3 * j + 1][3].plot(binned_R, np.linspace(0, 1, len(binned_R)) - scipy.stats.norm.cdf(
                        binned_R, np.mean(binned_R), np.std(binned_R)),
                                           ls=kwargs['ls'], c=kwargs['c'])
                    # z
                    # axs[3*j+2][3].plot(binned_z, (np.linspace(0,1,len(binned_z)) - scipy.stats.norm.cdf(
                    #   binned_z, np.mean(binned_z), np.std(binned_z))) / scipy.stats.norm.cdf(
                    #   binned_z, np.mean(binned_z), np.std(binned_z)),
                    #                    ls=kwargs['ls'], c=kwargs['c'])

                    axs[3 * j + 2][3].plot(binned_z, np.linspace(0, 1, len(binned_z)) - scipy.stats.norm.cdf(
                        binned_z, np.mean(binned_z), np.std(binned_z)),
                                           ls=kwargs['ls'], c=kwargs['c'])

                    if k == 0 and j == (4 - i):
                        # axs[3*j+2][2*i+1].plot(phi_bins, scipy.stats.norm.cdf(
                        #   phi_bins, 0, sigma_z[snap, _index]))

                        axs[3 * j][0].plot(phi_bins_j[j](n_points), scipy.stats.norm.pdf(
                            phi_bins_j[j](n_points), theory_best_v[_index, snap],
                            np.sqrt(theory_best_p2[_index, snap])),
                                           ls='-.', color='k')
                        axs[3 * j + 1][0].plot(z_bins_j[j](n_points), scipy.stats.norm.pdf(
                            z_bins_j[j](n_points), 0, np.sqrt(theory_best_r2[_index, snap])),
                                               ls='-.', color='k')
                        axs[3 * j + 2][0].plot(z_bins_j[j](n_points), scipy.stats.norm.pdf(
                            z_bins_j[j](n_points), 0, np.sqrt(theory_best_z2[_index, snap])),
                                               ls='-.', color='k')
                        axs[3 * j][1].plot(phi_bins_j[j](n_points), scipy.stats.norm.cdf(
                            phi_bins_j[j](n_points), theory_best_v[_index, snap],
                          np.sqrt(theory_best_p2[_index, snap])),
                                           ls='-.', color='k')
                        axs[3 * j + 1][1].plot(z_bins_j[j](n_points), scipy.stats.norm.cdf(
                            z_bins_j[j](n_points), 0, np.sqrt(theory_best_r2[_index, snap])),
                                               ls='-.', color='k')
                        axs[3 * j + 2][1].plot(z_bins_j[j](n_points), scipy.stats.norm.cdf(
                            z_bins_j[j](n_points), 0, np.sqrt(theory_best_z2[_index, snap])),
                                               ls='-.', color='k')
                        axs[3 * j][2].plot(phi_bins_j[j](2), [0, 0],
                                           ls='-.', color='k')
                        axs[3 * j + 1][2].plot(z_bins_j[j](2), [0, 0],
                                               ls='-.', color='k')
                        axs[3 * j + 2][2].plot(z_bins_j[j](2), [0, 0],
                                               ls='-.', color='k')
                        axs[3 * j][3].plot(phi_bins_j[j](2), [0, 0],
                                           ls='-.', color='k')
                        axs[3 * j + 1][3].plot(z_bins_j[j](2), [0, 0],
                                               ls='-.', color='k')
                        axs[3 * j + 2][3].plot(z_bins_j[j](2), [0, 0],
                                               ls='-.', color='k')

                # plt.text(phi_bins[0], 1, str(round(time[i],2)) + 'Gyr', ha='top')
                # plt.text(phi_bins[0], 0.5, kwargs['lab'], ha='top')
                # plt.legend()
                # plt.xlabel(r'$v_\phi$')
                # plt.ylabel(r'CDF')
                # plt.ylim([0,1])
                # plt.xlim([phi_bins[0], phi_bins[-1]])

                if k == 0 and kwargs['c'] == 'C0':
                    top_y = np.amax(scipy.stats.norm.pdf(
                        phi_bins_j[j](100), theory_best_v[_index, snap], np.sqrt(theory_best_p2[_index, snap])))
                    axs[3 * j][0].text(phi_bins_j[j](2)[0], top_y, r'$\log \tau =$' + str(round(log_tau_to_plot, 1)),
                                       va='top')
                    axs[3 * j + 1][0].text(z_bins_j[j](2)[0], top_y, r'$\log \tau =$' + str(round(log_tau_to_plot, 1)),
                                           va='top')
                    axs[3 * j + 2][0].text(z_bins_j[j](2)[0], top_y, r'$\log \tau =$' + str(round(log_tau_to_plot, 1)),
                                           va='top')
                    axs[3 * j][1].text(phi_bins_j[j](2)[0], 1, r'$v_\phi$', va='top')
                    axs[3 * j + 1][1].text(z_bins_j[j](2)[0], 1, r'$v_R$', va='top')
                    axs[3 * j + 2][1].text(z_bins_j[j](2)[0], 1, r'$v_z$', va='top')
                    axs[3 * j][2].text(phi_bins_j[j](2)[1], 0.05, r'$v_\phi$', va='top', ha='right')
                    axs[3 * j + 1][2].text(z_bins_j[j](2)[1], 0.05, r'$v_R$', va='top', ha='right')
                    axs[3 * j + 2][2].text(z_bins_j[j](2)[1], 0.05, r'$v_z$', va='top', ha='right')
                    axs[3 * j][3].text(phi_bins_j[j](2)[1], 0.05, r'$v_\phi$', va='top', ha='right')
                    axs[3 * j + 1][3].text(z_bins_j[j](2)[1], 0.05, r'$v_R$', va='top', ha='right')
                    axs[3 * j + 2][3].text(z_bins_j[j](2)[1], 0.05, r'$v_z$', va='top', ha='right')

                    axs[3 * j][0].set_xlim([phi_bins_j[j](2)[0], phi_bins_j[j](2)[1]])
                    axs[3 * j + 1][0].set_xlim([z_bins_j[j](2)[0], z_bins_j[j](2)[1]])
                    axs[3 * j + 2][0].set_xlim([z_bins_j[j](2)[0], z_bins_j[j](2)[1]])
                    axs[3 * j][1].set_xlim([phi_bins_j[j](2)[0], phi_bins_j[j](2)[1]])
                    axs[3 * j + 1][1].set_xlim([z_bins_j[j](2)[0], z_bins_j[j](2)[1]])
                    axs[3 * j + 2][1].set_xlim([z_bins_j[j](2)[0], z_bins_j[j](2)[1]])
                    axs[3 * j][2].set_xlim([phi_bins_j[j](2)[0], phi_bins_j[j](2)[1]])
                    axs[3 * j + 1][2].set_xlim([z_bins_j[j](2)[0], z_bins_j[j](2)[1]])
                    axs[3 * j + 2][2].set_xlim([z_bins_j[j](2)[0], z_bins_j[j](2)[1]])
                    axs[3 * j][3].set_xlim([phi_bins_j[j](2)[0], phi_bins_j[j](2)[1]])
                    axs[3 * j + 1][3].set_xlim([z_bins_j[j](2)[0], z_bins_j[j](2)[1]])
                    axs[3 * j + 2][3].set_xlim([z_bins_j[j](2)[0], z_bins_j[j](2)[1]])

    if save:
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    np.seterr(all='ignore')

    return


def plot_angular_momentum_evolution(save_name_append='diff', name='', save=False):
    # _index = 1
    save_name_append = 'cum'

    fig = plt.figure()
    fig.set_size_inches(9, 12, forward=True)
    # ax0 = plt.subplot(3,1,1)
    # ax1 = plt.subplot(3,1,2)
    # # ax1p = plt.subplot(4,1,3)
    # ax2 = plt.subplot(3,1,3)

    ylim = [-1.47, -1.25]  # np.log10([0.033, 0.056])
    ylim1 = [-0.022, 0.038]  # [-0.75, 0.05] #np.log10([0.1, 1.2])
    ylim2 = [-1.69, -1.495]  # np.log10([0.019, 0.032])

    spec = fig.add_gridspec(nrows=3, height_ratios=[1]*3, #[np.diff(ylim)[0], np.diff(ylim1)[0], np.diff(ylim2)[0]],
                            ncols=1, width_ratios=[1], wspace=0.02, hspace=0.02)
    axs = []
    for row in range(3):
        axs.append(fig.add_subplot(spec[row, 0]))
    [ax0, ax1, ax2] = axs

    fig.subplots_adjust(hspace=0.02, wspace=0.01)

    ax0.set_xticklabels([])
    ax1.set_xticklabels([])
    # ax1p.set_xticklabels([])

    # ax0.set_ylabel(r'.  $\log (R_{1/2} / R_{200})$')
    # ax1.set_ylabel(r'.    $\log (v_{c, 1/2} / V_{200})$')
    # # ax1p.set_ylabel(r'$\log (\overline{v}_\phi / V_{200})$')
    # ax2.set_ylabel(r'$\log (j_\star / \sqrt{2} R_{200} V_{200})$   .')
    ax0.set_ylabel(r'$\log (R_{1/2} / r_{200})$')
    ax1.set_ylabel(r'$\log (V_c / V_{200})$')
    ax2.set_ylabel(r'$\log (j_\star / \sqrt{2} r_{200} V_{200})$')
    ax2.set_xlabel(r'$t$ [Gyr]')

    ax0.set_xlim(0, T_TOT)
    ax1.set_xlim(0, T_TOT)
    # ax1p.set_xlim(0, T_TOT)
    ax2.set_xlim(0, T_TOT)

    ax0.set_ylim(ylim)
    ax1.set_ylim(ylim1)
    # ylim1p = [-0.75, 0.05]
    # ax1p.set_ylim(ylim1p)
    ax2.set_ylim(ylim2)

    # j_z_high_rez = get_total_angular_momentum('mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps', save_name_append, 101,
    #                                  quarter=False, five=False, all=True)
    # j_z0_best = j_z_high_rez[BURN_IN]

    galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps']
    snaps_list = [400, 400, 101, 101, 101]

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = get_name_kwargs(galaxy_name)
        v200 = get_v_200(galaxy_name)  # km/s
        r200 = v200 * 10 * GALIC_HUBBLE_CONST  # kpc
        # M200 = v200**3 / (10 * GRAV_CONST * GALIC_HUBBLE_CONST)
        # N200 = M200 / partilce_mass
        # partilce_mass = kwargs['mdm']

        dy = 0.06
        alpha = 0.2
        lw = 6
        window_width = 5
        window = snaps // window_width + 1
        if window % 2 == 0:
            window += 1
        poly_n = 5
        n_snaps = 5

        bin_edges = get_bin_edges('diff', True, galaxy_name)

        R_half = get_R_half(galaxy_name, snaps)
        v_circ = get_vc_from_M_at_R_half(galaxy_name, 'diff', snaps)
        (_, v_phi, _, _, _) = get_dispersions(galaxy_name, 'diff', snaps, bin_edges)
        j_z = get_total_angular_momentum(galaxy_name, 'diff', snaps,
                                         quarter=False, five=False, all=True)

        _index = 1

        # TODO comment this out
        # j_z = j_z - j_z[BURN_IN] + j_z0_best

        time = np.linspace(0, T_TOT, snaps + 1)
        # time = time - time[BURN_IN]

        y = np.log10(R_half / r200)
        ax0.errorbar(time, y, c=kwargs['c'], marker='', ls='-', lw=lw, alpha=alpha)
        ax0.errorbar(time, savgol_filter(y, window, poly_n), c=kwargs['c'], marker='', ls=kwargs['ls'], lw=3)

        y = np.log10(v_circ / v200)
        ax1.errorbar(time, y, c=kwargs['c'], marker='', ls='-', lw=lw, alpha=alpha)
        ax1.errorbar(time, savgol_filter(y, window, poly_n), c=kwargs['c'], marker='', ls=kwargs['ls'], lw=3)

        # y = np.log10(v_phi[:, _index] / v200)
        # ax1p.errorbar(time, y, c=kwargs['c'], marker='', ls='-', lw=lw, alpha=alpha)
        # ax1p.errorbar(time, savgol_filter(y, window, poly_n), c=kwargs['c'], marker='', ls=kwargs['ls'])

        y = np.log10(j_z / (np.sqrt(2) * r200 * v200))
        ax2.errorbar(time, y, c=kwargs['c'], marker='', ls='-', lw=lw, alpha=alpha)
        ax2.errorbar(time, savgol_filter(y, window, poly_n), c=kwargs['c'], marker='', ls=kwargs['ls'], lw=3)

        if kwargs['c'] in ['C0', 'C1', 'C2']:
            dx = 0.03
        else:
            dx = 0.36
        if kwargs['c'] in ['C0', 'C3']:
            dy = 0.98
        elif kwargs['c'] in ['C1', 'C4']:
            dy = 0.87
        else:
            dy = 0.76

        ax0.text(dx * T_TOT, ylim[0] + dy * (ylim[1] - ylim[0]),
                 r'$\mathrm{N}_{\mathrm{DM}} = $' + kwargs['labn'], ha='left', va='top', color=kwargs['c'])

        if kwargs['c'] in ['C0', 'C1', 'C2']:
            dx = 0.03
        else:
            dx = 0.36
        if kwargs['c'] == 'C0':
            dy = 0.24
        elif kwargs['c'] in ['C1', 'C3']:
            dy = 0.13
        else:
            dy = 0.02

        ax2.text(dx * T_TOT, ylim2[0] + dy * (ylim2[1] - ylim2[0]),
                 r'$m_{\mathrm{DM}} = $' + kwargs['lab'], ha='left', va='bottom', color=kwargs['c'])

    ax1.text(0.04 * T_TOT, ylim1[0] + 0.96 * (ylim1[1] - ylim1[0]),
             r'at $R = R_{1/2}$', ha='left', va='top', color='k')
    ax1.text(0.04 * T_TOT, ylim1[0] + 0.84 * (ylim1[1] - ylim1[0]),
             r'$V_{200} = 200$ km/s', ha='left', va='top', color='k')

    # #theory
    # # if i == 0:
    # if False:
    #
    #   v_c = lambda R: get_velocity_scale([R]*2, 0,0,0,0,0,0, analytic_v_c=True, save_name_append='diff')
    #   (M_star, R_disk) = get_exponential_disk_params()
    #   Sigma = lambda R: get_exponential_surface_density(R, M_star, R_disk, save_name_append='diff')
    #
    #   j_star_integrand = lambda R: R**2 * v_c(R)[0] * Sigma(R)
    #
    #   j_star = quad(j_star_integrand, 0, R200)[0] * 2 * np.pi / M_star
    #
    #   ax[j].axhline(np.log10(j_star / V200**2), 0, 1,
    #                 color='k', ls='--', lw=1, zorder=10)
    #
    #   #evolution
    #   (t_c_dm , t_c_circ, delta_dm, upsilon_dm, upsilon_circ
    #    ) = get_constants(bin_edges, save_name_append, kwargs['mdm'])
    #   # (inital_velocity_v, inital_velocity2_z, inital_velocity2_r, inital_velocity2_p
    #   #  ) = smooth_burn_ins(kwargs['mdm'], snaps, save_name_append, burn_in, bin_edges)
    #   (inital_velocity_v, _,_,_
    #    ) = smooth_zero_snap(kwargs['mdm'], snaps, save_name_append, bin_edges)
    #
    #   theory_best_v = np.zeros((n_bins, snaps+1))
    #   taus = np.zeros((n_bins, snaps+1))
    #
    #   j_star_theory = np.zeros(snaps+1)
    #
    #   for b in range(n_bins):
    #     time = np.linspace(0, T_TOT, snaps+1)
    #     time = time - time[BURN_IN]
    #
    #     theory_best_v[b, :] = get_theory_velocity2_array(inital_velocity_v[b], 0,
    #                                                 time, t_c_dm[b], delta_dm[b], upsilon_dm[b],
    #                                                 ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
    #
    #     theory_best_v[b, :] = analytic_v_c[b, np.newaxis] - theory_best_v[b, :]
    #
    #     taus[b, :] = get_tau_array(time, t_c_dm[b])
    #
    #   for s in range(snaps+1):
    #
    #     v_c = InterpolatedUnivariateSpline(R_for_interp, theory_best_v[:, s])
    #     # Emperical profile evolution
    #     R_d_ICs = 4.16  # kpc
    #     if 'mu_25' in galaxy_name: slope = 24.0  # kpc / scale time
    #     if 'mu_5' in galaxy_name: slope = 20.7  # kpc / scale time
    #     if 'mu_1' in galaxy_name: slope = 8.27  # kpc / scale time
    #     R_d = R_d_ICs + slope * taus[R_index, s]
    #     Sigma = lambda R: get_exponential_surface_density(R, M_star, R_d, save_name_append='diff')
    #
    #     j_star_integrand = lambda R: R**2 * v_c(R) * Sigma(R)
    #
    #     j_star_theory[s] = quad(j_star_integrand, 0, R200)[0] * 2 * np.pi / M_star
    #
    #   ax[j].errorbar(time, np.log10(j_star_theory / V200**2),
    #                 color='k', ls='-.', zorder=10)

    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return


def plot_angular_momentum_evolution2(save_name_append='diff', name='', save=False, stack=False):
    # _index = 1
    save_name_append = 'cum'

    fig = plt.figure()
    # fig.set_size_inches(22.5, 15, forward=True)
    # fig.set_size_inches(22.5, 16.975, forward=True) #squre
    if stack:
        fig.set_size_inches(36, 13.5, forward=True)
    else:
        fig.set_size_inches(18, 13.5, forward=True) #square

    ylim = [-1.47, -1.25]
    ylim1 = [-0.022, 0.038]
    ylim2 = [-1.69, -1.495]
    
    rows = 3
    cols = 4
    spec = fig.add_gridspec(nrows=rows, height_ratios=[1]*rows,
                            ncols=cols, width_ratios=[1]*cols, wspace=0.02, hspace=0.02)
    axs = []
    for row in range(rows):
        axs.append([])
        for col in range(cols):
            if stack:
                axs[row].append(fig.add_subplot(spec[row, 0]))

            else:
                axs[row].append(fig.add_subplot(spec[row, col]))
                if col !=0 :
                    axs[row][col].set_yticklabels([])
            
            axs[row][col].set_xlim(0, T_TOT)

    for col in range(cols):
        axs[0][col].set_xticklabels([])
        axs[1][col].set_xticklabels([])

        axs[2][col].set_xlabel(r'$t$ [Gyr]')

        axs[0][col].set_ylim(ylim)
        axs[1][col].set_ylim(ylim1)
        axs[2][col].set_ylim(ylim2)

    axs[0][0].set_ylabel(r'$\log (R_{1/2} / r_{200})$')
    axs[1][0].set_ylabel(r'$\log (V_c / V_{200})$')
    axs[2][0].set_ylabel(r'$\log (j_\star / \sqrt{2} r_{200} V_{200})$')

    _index = 1
    
    dy = 0.06
    alpha = 0.2
    lw = 6
    window_width = 5
    poly_n = 5
    n_snaps = 5

    log_n_min = 4
    log_n_max = 6.5
    cmap_name = 'plasma_r'
    cmap = lambda n: matplotlib.cm.get_cmap(cmap_name)((np.log10(n) - log_n_min)/(log_n_max - log_n_min))

    galaxy_name_lists = [['mu_5/fdisk_0p01_lgMdm_6p0_V200-50kmps',
                          'mu_5/fdisk_0p01_lgMdm_5p5_V200-50kmps',
                          'mu_5/fdisk_0p01_lgMdm_5p0_V200-50kmps',
                          'mu_5/fdisk_0p01_lgMdm_4p5_V200-50kmps',
                          'mu_5/fdisk_0p01_lgMdm_4p0_V200-50kmps'],
                         ['mu_5/fdisk_0p01_lgMdm_7p0_V200-100kmps',
                          'mu_5/fdisk_0p01_lgMdm_6p5_V200-100kmps',
                          'mu_5/fdisk_0p01_lgMdm_6p0_V200-100kmps',
                          'mu_5/fdisk_0p01_lgMdm_5p5_V200-100kmps',
                          'mu_5/fdisk_0p01_lgMdm_5p0_V200-100kmps'],
                         ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                          'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                          'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                          'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
                          'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps'],
                         ['mu_5/fdisk_0p01_lgMdm_9p0_V200-400kmps',
                          'mu_5/fdisk_0p01_lgMdm_8p5_V200-400kmps',
                          'mu_5/fdisk_0p01_lgMdm_8p0_V200-400kmps',
                          'mu_5/fdisk_0p01_lgMdm_7p5_V200-400kmps',
                          'mu_5/fdisk_0p01_lgMdm_7p0_V200-400kmps']]
    snaps_lists = [[400, 400, 101, 101, 101],
                   [400, 400, 101, 101, 101],
                   [400, 400, 101, 101, 101],
                   [400, 400, 101, 101, 101]]

    # for col in range(cols):
    #     for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_lists[col], snaps_lists[col])):
    for col, (galaxy_name_list, snaps_list) in enumerate(zip(galaxy_name_lists, snaps_lists)):
        for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
            kwargs = get_name_kwargs(galaxy_name)
            if not stack:
                kwargs['lsv'] = '-'
            v200 = get_v_200(galaxy_name)  # km/s
            r200 = v200 * 10 * GALIC_HUBBLE_CONST  # kpc
    
            bin_edges = get_bin_edges('diff', True, galaxy_name)
    
            R_half = get_R_half(galaxy_name, snaps)
            v_circ = get_vc_from_M_at_R_half(galaxy_name, 'diff', snaps)
            (_, v_phi, _, _, _) = get_dispersions(galaxy_name, 'diff', snaps, bin_edges)
            j_z = get_total_angular_momentum(galaxy_name, 'diff', snaps,
                                             quarter=False, five=False, all=True)
    
            (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
             MassStar, PosStars, VelStars, IDStars, PotStars
             ) = load_nbody.load_and_align(galaxy_name, snap='{0:03d}'.format(int(i)))
            n_DM = len(IDDMs)
            # print(np.log10(n_DM), n_DM)
            colour = cmap(n_DM) 
    
            window = snaps // window_width + 1
            if window % 2 == 0:
                window += 1
            
            time = np.linspace(0, T_TOT, snaps + 1)

            y = np.log10(R_half / r200)
            axs[0][col].errorbar(time, y, c=colour, marker='', ls=kwargs['ls'], lw=lw, alpha=alpha)
            axs[0][col].errorbar(time, savgol_filter(y, window, poly_n), c=colour, marker='', ls=kwargs['lsv'], lw=3)
    
            y = np.log10(v_circ / v200)
            axs[1][col].errorbar(time, y, c=colour, marker='', ls=kwargs['ls'], lw=lw, alpha=alpha)
            axs[1][col].errorbar(time, savgol_filter(y, window, poly_n), c=colour, marker='', ls=kwargs['lsv'], lw=3)
    
            y = np.log10(j_z / (np.sqrt(2) * r200 * v200))
            axs[2][col].errorbar(time, y, c=colour, marker='', ls=kwargs['ls'], lw=lw, alpha=alpha)
            axs[2][col].errorbar(time, savgol_filter(y, window, poly_n), c=colour, marker='', ls=kwargs['lsv'], lw=3)

            if not stack:
                if kwargs['c'] in ['C0', 'C1', 'C2']: dx = 0.03
                else: dx = 0.53
                if kwargs['c'] in ['C0', 'C3']: dy = 0.98
                elif kwargs['c'] in ['C1', 'C4']: dy = 0.88
                else: dy = 0.78

                axs[0][col].text(dx * T_TOT, ylim[0] + dy * (ylim[1] - ylim[0]),
                         r'$\mathrm{N}_{\mathrm{DM}} = $' + kwargs['labn'], ha='left', va='top', color=colour)

                if kwargs['c'] in ['C0', 'C1', 'C2']: dx = 0.03
                else: dx = 0.53
                if kwargs['c'] == 'C0': dy = 0.22
                elif kwargs['c'] in ['C1', 'C3']: dy = 0.12
                else: dy = 0.02

                axs[2][col].text(dx * T_TOT, ylim2[0] + dy * (ylim2[1] - ylim2[0]),
                         r'$m_{\mathrm{DM}} = $' + kwargs['lab'], ha='left', va='bottom', color=colour)

        if (stack and (col == 0)) or (not stack):
            axs[1][col].text(0.04 * T_TOT, ylim1[0] + 0.96 * (ylim1[1] - ylim1[0]),
                             r'at $R = R_{1/2}$', ha='left', va='top', color='k')
        
        if not stack:
            axs[1][col].text(0.04 * T_TOT, ylim1[0] + 0.86 * (ylim1[1] - ylim1[0]),
                             r'$V_{200} =$ ' + f'{v200} km/s', ha='left', va='top', color='k')

    if stack:
        axs[0][0].legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='-.',  lw=3)),
                          (lines.Line2D([0, 1], [0, 1], color='grey', ls='-', lw=3)),
                          (lines.Line2D([0, 1], [0, 1], color='grey', ls='--',  lw=3)),
                          (lines.Line2D([0, 1], [0, 1], color='grey', ls=':', lw=3))],
                         # ['50 km/s', '100 km/s', '200 km/s', '400 km/s'], title=r'$V_{200}$',
                         ['400', '200', '100', '50'], title=r'$V_{200}$ [km/s]',
                         loc='upper left', frameon=False, labelspacing=0.3,
                         # handlelength=2, handletextpad=0.2, labelspacing=0.2, 
                         borderpad=0.2)
        #, bbox_to_anchor=(1.05, -0.05))

        Bbox_top = axs[1][0].get_position()
        # Bbox_bot = axs[1][0].get_position()

        width = 0.08
        pad = -0.02

        cax = plt.axes([Bbox_top.x1,
                        Bbox_top.y0,
                        (Bbox_top.x1 - Bbox_top.x0) * (width + pad),
                        Bbox_top.y1 - Bbox_top.y0])

        n_col_grad = 101
        x_ = np.linspace(1e-3 + 0, 1 - 1e-3, n_col_grad)
        x, _ = np.meshgrid(x_, np.array([0, 1]))
        cax.imshow(x.T, cmap=cmap_name, vmin=0, vmax=0.8,
                   aspect= 1 / width / (log_n_max - log_n_min), origin='lower', extent=[0, 1, log_n_min, log_n_max])
        
        cax.set_xticks([])
        cax.set_xticklabels([])
        # cax.set_yticks([0.6, 0.7, 0.8, 0.9, 1])
        # cax.set_yticklabels([])
        cax.yaxis.set_label_position('right')
        cax.yaxis.tick_right()
        cax.set_ylabel(r'$\log \, N_{\rm DM}$')


    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return


def plot_softening_size(name='', save=False):
    # _index = 1
    save_name_append = 'cum'

    fig = plt.figure()
    fig.set_size_inches(3.3, 3.3 +0.04, forward=True)
    # ax0 = plt.subplot(3,1,1)
    # ax1 = plt.subplot(3,1,2)
    # # ax1p = plt.subplot(4,1,3)
    # ax2 = plt.subplot(3,1,3)

    ylim = [0.7, 1.7]  # np.log10([0.033, 0.056])

    spec = fig.add_gridspec(nrows=1, height_ratios=[1],
                            ncols=1, width_ratios=[1], wspace=0.02, hspace=0.02)
    axs = []
    for row in range(1):
        axs.append(fig.add_subplot(spec[row, 0]))
    [ax0] = axs

    fig.subplots_adjust(hspace=0.02, wspace=0.01)

    # ax0.set_ylabel(r'$\log (R_{1/2} / r_{200})$')
    ax0.set_ylabel(r'$\log \, R_{1/2}/$ kpc')
    ax0.set_xlabel(r'$t$ [Gyr]')

    ax0.set_xlim(0, T_TOT)

    ax0.set_ylim(ylim)

    # galaxy_name_list = [
    #                     'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_24soft',
    #                     'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_20soft',
    #                     'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_16soft',
    #                     'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_8soft',
    #                     'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_4soft',
    #                     'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_2soft',
    #                     'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_1soft',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_0p5soft',
    #                     'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_0p25soft',
    #                     # 'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_4soft',
    #                     # 'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_2soft',
    #                     # 'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     # 'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_0p5soft',
    #                     # 'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_0p25soft'
    #     ]
    # snaps_list = [400, 400, 400, 400, 101, 101, 400, 400, 101, 101,]
    #               # 101, 101, 101, 101, 101]
    # eps_list = [24, 20, 16, 8, 4, 2, 1, 1, 0.5, 0.25]
    
    if '8' in name:
        galaxy_name_list = [
                            'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_128soft',
                            'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_96soft',
                            'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_64soft',
                            'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_48soft',
                            'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_32soft',
                            # 'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_24soft',
                            # 'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_20soft',
                            'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_16soft',
                            'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_8soft',
                            # 'eps/fdisk_0p01_lgMdm_8p0_V200-200kmps_1soft',
            ]
        snaps_list = [400 for _ in galaxy_name_list]
                      # 101, 101, 101, 101, 101]
        eps_list = [128, 96, 64, 48, 32, 16, 8,] #24, 20, 16, 8,] #1]
    
    if '7' in name:
        galaxy_name_list = [
                            'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_128soft',
                            'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_96soft',
                            'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_64soft',
                            'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_48soft',
                            'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_32soft',
                            'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_16soft',
                            'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_8soft',
                            # 'eps/fdisk_0p01_lgMdm_7p0_V200-200kmps_1soft',
            ]
        snaps_list = [100 for _ in galaxy_name_list]
        eps_list = [128, 96, 64, 48, 32, 16, 8,] #1]

    # cmap = 'twilight_r'  # 'cividis_r' #'cool' #'gist_rainbow' #
    cmap = 'turbo'  # 'cividis_r' #'cool' #'gist_rainbow' #

    for i, (galaxy_name, snaps, eps) in enumerate(zip(galaxy_name_list, snaps_list, eps_list)):
        kwargs = get_name_kwargs(galaxy_name)
        v200 = get_v_200(galaxy_name)  # km/s
        r200 = v200 * 10 * GALIC_HUBBLE_CONST  # kpc
        # M200 = v200**3 / (10 * GRAV_CONST * GALIC_HUBBLE_CONST)
        # N200 = M200 / partilce_mass
        # partilce_mass = kwargs['mdm']

        dy = 0.06
        alpha = 0.2
        lw = 6
        window_width = 7
        window = snaps // window_width + 1
        if window % 2 == 0:
            window += 1
        poly_n = 5
        n_snaps = 5

        bin_edges = get_bin_edges('diff', True, galaxy_name)

        R_half = get_R_half(galaxy_name, snaps)

        _index = 1

        time = np.linspace(0, T_TOT, snaps + 1)
        # time = time - time[BURN_IN]

        # _c = (i + 0.5) / (len(galaxy_name_list))
        # c = matplotlib.cm.get_cmap(cmap)(_c)

        c = matplotlib.cm.get_cmap(cmap)(((np.log(eps) / np.log(2)) - 3 + 0.3) / 4)

        y = np.log10(R_half)
        # ax0.errorbar(time, y, c=c, marker='', ls='-', lw=lw, alpha=alpha)
        # ax0.errorbar(time, savgol_filter(y, window, poly_n), c=c, marker='', ls=kwargs['ls'], lw=3)
        ax0.errorbar(time, y, c=c, marker='', ls=kwargs['ls'], lw=3)

        ax0.axhline(np.log10(0.2 * eps), 0, 1, c=c, ls=':', lw=1)

    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return


def smooth_angular_momentum_comparison(save_name_append='diff', name='', save=False):
    fig = plt.figure()
    fig.set_size_inches(6, 5, forward=True)
    ax0 = plt.subplot(1, 1, 1)

    ax0.set_ylabel(r'$\log (j_\star / R_{200} V_{200})$')
    ax0.set_xlabel(r'$t$ [Gyr]')

    ax0.set_xlim(0, T_TOT)
    ylims = [-1.65, -1.31]
    ax0.set_ylim(ylims)

    galaxy_name_list = ['mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_5_smooth/fdisk_0p01_lgMdm_8p0_V200-200kmps']
    snaps_list = [400, 400, 400, 101]

    label_list = [r'$\mu = 1$', r'$\mu = 5$', r'$\mu = 25$', r'Smooth Halo']
    color_list = ['C0', 'C1', 'C2', 'C3']
    ls_list = ['--', '-', ':', '-.']

    # galaxy_name_list = ['mu_5_smooth/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed0',
    #                     'mu_5_smooth/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed1',
    #                     'mu_5_smooth/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed2',
    #                     'mu_5_smooth/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed3',
    #                     'mu_5_smooth/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed4',
    #                     'mu_5_smooth/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed5',
    #                     'mu_5_smooth/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed6',
    #                     'mu_5_smooth/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed7',
    #                     'mu_5_smooth/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed8',
    #                     'mu_5_smooth/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed9',
    #                     'mu_5_smooth/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed0',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed1',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed2',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed3',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed4',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed5',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed6',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed7',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed8',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed9',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps']
    # snaps_list = [101,101,101,101,101,101,101,101,101,101,101,
    #               400,400,400,400,400,400,400,400,400,400,400]

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = get_name_kwargs(galaxy_name)
        v200 = get_v_200(galaxy_name)  # km/s

        dy = 0.06
        alpha = 0.2
        lw = 5
        lwt = 3
        window_width = 5
        window = snaps // window_width + 1
        if window % 2 == 0:
            window += 1
        poly_n = 5
        n_snaps = 5

        j_z = get_total_angular_momentum(galaxy_name, save_name_append, snaps,
                                         quarter=False, five=False, all=True)

        time = np.linspace(0, T_TOT, snaps + 1)
        # time = time - time[BURN_IN]

        x = time
        y = np.log10(j_z / v200 ** 2)

        # if i == 10 or i == 21:
        #   color = 'C1'
        #   ax0.errorbar(x, savgol_filter(y, window, poly_n), c=color, marker='', ls='-', lw=lwt)
        # else:
        #   color = 'C0'
        #   ax0.errorbar(x, y, c=color, marker='', ls='-', lw=lw, alpha=alpha)
        #   ax0.errorbar(x, savgol_filter(y, window, poly_n), c=color, marker='', ls='-', lw=lwt)

        ax0.errorbar(x, y, c=color_list[i], marker='', ls='-', lw=lw, alpha=alpha)
        # smooth
        ax0.errorbar(x, savgol_filter(y, window, poly_n), c=color_list[i], marker='',
                     ls=ls_list[i], lw=lwt, label=label_list[i])

    ax0.text(0.3, ylims[1] - 0.008, r'$V_{200}=200$ km/s, $m_\mathrm{DM} = 10^8 \mathrm{M}_\odot$',
             ha='left', va='top')

    ax0.legend(loc='lower left', frameon=False, labelspacing=0.1)

    # ax0.legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls=':', linewidth=3)),
    #             (lines.Line2D([0, 1], [0, 1], color='grey', ls='-', linewidth=3)),
    #             (lines.Line2D([0, 1], [0, 1], color='grey', ls='-.', linewidth=3)),
    #             (lines.Line2D([0, 1], [0, 1], color='grey', ls='--', linewidth=3))],
    #             [r'Smooth Halo', r'', r'$V_{200} = 200$ km/s', r'$V_{200} = 400$ km/s'],
    #              loc='lower left', frameon=False, labelspacing=0.1)

    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return


def cumulative_angularmomentum_profile(galaxy_names, snap, save=True):
    plt.figure(figsize=(6, 14))
    ax0 = plt.subplot(3, 1, 1)
    ax1 = plt.subplot(3, 1, 2)
    ax2 = plt.subplot(3, 1, 3)

    for i, galaxy_name in enumerate(galaxy_names):
        kwargs = get_name_kwargs(galaxy_name)

        if type(snap) != str:
            snap = '{0:03d}'.format(int(snap))
        # read snap
        # for no align / centering
        # (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
        #  MassStar, PosStars, VelStars, IDStars, PotStars
        #  ) = load_nbody.load_snapshot(galaxy_name, snap)
        (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
         MassStar, PosStars, VelStars, IDStars, PotStars \
         ) = load_nbody.load_and_align(galaxy_name, snap)

        r = np.linalg.norm(PosStars, axis=1)
        R = np.sqrt(PosStars[:, 0] ** 2 + PosStars[:, 1] ** 2)

        arg_order = np.argsort(r)
        aRg_order = np.argsort(R)

        # r_half_current = np.median(R)
        # r_qaurter_current = np.quantile(R, 0.25)
        # ax2.axvline(r_half_current, 0, 1, c='k', ls=kwargs['ls'])
        # ax2.axvline(r_qaurter_current, 0, 1, c='k', ls=kwargs['ls'])

        R_half_inital = get_bin_edges('cum')[3]
        R_qaurter_intial = get_bin_edges('cum')[1]

        # mask = (r < 5 * r_half) \
        # mask = (r < r_half) \
        # mask = np.logical_and(r > r_qaurter, r < 5 * r_half)
        # mask = np.logical_and(r > r_qaurter, r < r_half)

        j = np.cross(PosStars, VelStars)

        # j_z = j[:, 2]
        # j_z_r = j_z[arg_order]
        # j_z_R = j_z[aRg_order]
        # cum_j_z_r = np.cumsum(j_z_r) / len(j_z_r)
        # cum_j_z_R = np.cumsum(j_z_R) / len(j_z_R)

        j_z_r = j[arg_order]
        j_z_R = j[aRg_order]

        # print(np.cumsum(j_z_r, axis=0))

        cum_j_z_r = np.linalg.norm(np.cumsum(j_z_r, axis=0) / len(j_z_r), axis=1)
        cum_j_z_R = np.linalg.norm(np.cumsum(j_z_R, axis=0) / len(j_z_R), axis=1)
        ordered_r = r[arg_order]
        ordered_R = R[aRg_order]

        stellar_mass = 1.859656 * 10 ** 10

        # plot
        ax0.errorbar(ordered_r, cum_j_z_r, ls=kwargs['ls'],  # c=kwargs['c'],
                     label=kwargs['mu'])
        ax1.errorbar(ordered_R, cum_j_z_R, ls=kwargs['ls'],  # c=kwargs['c'],
                     label=kwargs['mu'])

        ax2.errorbar(ordered_R, np.linspace(0, stellar_mass, len(ordered_R)), ls=kwargs['ls'],  # c=kwargs['c'],
                     label=kwargs['mu'])

        twenty = 20
        # ax2.text(1e-1, 10**(9.5+i/3), r'$N(R>20)$ =' + str(int(np.sum(r > twenty))))
        # ax2.text(1e-1, 10**(8.5+i/3), r'$f(N(R>20))$ =' + str(round(np.sum(r > twenty)/len(r), 3)))
        ax2.text(1e-1, (1.2 + i / 5) * 10 ** 10, r'$N(R>20)=$' + str(int(np.sum(r > twenty))))
        ax2.text(1e-1, (0.6 + i / 5) * 10 ** 10, r'$f(N(R>20))=$' + str(round(np.sum(r > twenty) / len(r), 3)))

    ax0.loglog()
    ax1.loglog()
    # ax2.loglog()
    ax2.semilogx()

    ax0.set_xlabel(r'$r$ [kpc]')
    ax0.set_ylabel(r'Cumulative $j_\star$ [kpc km s$^{-1}$]')
    ax1.set_xlabel(r'$R$ [kpc]')
    ax1.set_ylabel(r'Cumulative $j_\star$ [kpc km s$^{-1}$]')
    ax2.set_xlabel(r'$R$ [kpc]')
    ax2.set_ylabel(r'Cumulative $M_\star$ [10$^{10}$ M$_\odot$]')

    ax0.axvline(R_half_inital, 0, 1, c='k', ls='-.')
    ax0.axvline(5 * R_half_inital, 0, 1, c='k', ls='-.')
    ax0.axvline(R_qaurter_intial, 0, 1, c='k', ls='-.')
    ax1.axvline(R_half_inital, 0, 1, c='k', ls='-.')
    ax1.axvline(5 * R_half_inital, 0, 1, c='k', ls='-.')
    ax1.axvline(R_qaurter_intial, 0, 1, c='k', ls='-.')
    ax2.axvline(R_half_inital, 0, 1, c='k', ls='-.')
    ax2.axvline(5 * R_half_inital, 0, 1, c='k', ls='-.')
    ax2.axvline(R_qaurter_intial, 0, 1, c='k', ls='-.')
    # ax2.axhline(stellar_mass/2, 0,1, c='k', ls='-.')
    # ax2.axhline(stellar_mass/4, 0,1, c='k', ls='-.')

    Rs = np.logspace(-1, 2)
    (M_disk, r_disk) = get_exponential_disk_params()
    m_theory = exponential_cumulative_mass(Rs, M_disk * 10 ** 10, r_disk)
    ax2.errorbar(Rs, m_theory, c='k', ls='-.')

    ax0.set_xlim(1e-1, 1e2)
    ax0.set_ylim(5e-2, 2e3)
    ax1.set_xlim(1e-1, 1e2)
    ax1.set_ylim(5e-2, 2e3)
    ax2.set_xlim(1e-1, 1e2)
    # ax2.set_ylim(1e7, 3e10)
    ax2.set_ylim(0, 2e10)

    t = np.linspace(0, T_TOT, 401)
    ax1.text(2e-1, 1e0, 't=' + str(round(t[int(snap)], 1)) + 'Gyr')
    # ax1.text(2e-1, 3e0, 'No centering')

    ax0.legend()

    if save:
        fname = '../galaxies/' + galaxy_name + '/results/cumulative_jz_' + snap
        plt.savefig(fname, bbox_inches='tight')
        plt.close()

    return


def plot_asymmetric_drift(galaxy_name_list, snaps_list, save_name_append='diff',
                          name='', save=False):

    fig = plt.figure(figsize=(20, 15))
    
    for _index in range(3):

        ax1  = plt.subplot(3, 3, _index+1)
        ax2 = plt.subplot(3, 3, _index+4)
        ax3 = plt.subplot(3, 3, _index+7)
    
        fig.subplots_adjust(hspace=0)
    
        ax1.set_xticklabels([])
        ax2.set_xticklabels([])
    
        nRs = 1001
        poly = 5
        # R_for_interp = np.logspace(-1, 1.5, nRs)
        R_for_interp = np.linspace(1, 15, nRs)
        bin_edges = get_dex_bins(R_for_interp, 0.1)
        n_bins = len(bin_edges) // 2
    
        # R_half = get_bin_edges(save_name_append='cum')[3]
    
        R_disk = 4.15
    
        # R_min, R_max = get_bin_edges()[2 * _index], get_bin_edges()[2 * _index + 1]
        # exp_int = lambda R: R * np.exp(- R / R_disk)
        # tot = quad(exp_int, R_min, R_max)[0]
        # zero = lambda R: quad(exp_int, R_min, R)[0] - 0.5 * tot
        # R_mass_centre = brentq(zero, R_min, R_max)
    
        # R_half = get_bin_edges(save_name_append='cum')[2 * _index + 1]
        R_half = 10 ** ((np.log10(get_bin_edges()[0::2]) + np.log10(get_bin_edges()[1::2])) / 2)[_index]
    
        R_index = np.argmin(np.abs(R_half - R_for_interp))
        R_index -= 1
    
        R_for_interp = R_for_interp[R_index - 1: R_index + 2]
        bin_edges = get_dex_bins(R_for_interp, 0.1)
        n_bins = len(bin_edges) // 2
        R_index = 1
    
        (ln_Lambda_ks, alphas, betas, gammas
         ) = find_least_log_squares_best_fit(
            save_name_append=save_name_append, mcmc=False, fixed_b=0, fixed_g=0)
    
        analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0,
                                                 analytic_dispersion=True, save_name_append=save_name_append)
        analytic_v_c = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0,
                                          analytic_v_c=True, save_name_append=save_name_append)

        v200 = get_v_200()
    
        fixed_analytic_v_c = get_velocity_scale(get_bin_edges(), 0, 0, 0, 0, 0, 0,
                                                analytic_v_c=True, save_name_append=save_name_append)

        for galaxy_name, snaps in zip(galaxy_name_list, snaps_list):
            kwargs = get_name_kwargs(galaxy_name)

            time = np.linspace(0, T_TOT, snaps + 1)
            # time = time - time[BURN_IN]

            (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
             ) = get_constants(bin_edges, save_name_append, kwargs['mdm'], galaxy_name=galaxy_name)
            taus = get_tau_array(time, t_c_dm[_index])
            log_taus = np.log10(taus)

            (inital_velocity_v, inital_velocity2_z, inital_velocity2_r, inital_velocity2_p
             ) = smooth_zero_snap(kwargs['mdm'], snaps, save_name_append, bin_edges)
            # (inital_velocity_v, inital_velocity2_z, inital_velocity2_r, inital_velocity2_p
            #  ) = smooth_burn_ins(kwargs['mdm'], snaps, save_name_append, BURN_IN,
            #                      bin_edges, galaxy_name=galaxy_name)
            inital_velocity2_z **=2
            inital_velocity2_r **= 2
            inital_velocity2_p **= 2

            (ln_Lambda_ks, alphas, betas, gammas
             ) = find_least_log_squares_best_fit(
                save_name_append, mcmc=False, fixed_b=0, fixed_g=0)

            tau_vir = get_tau_heat_array(delta_dm[R_index], np.sqrt(upsilon_circ[R_index]),
                                         ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
            tau_0 = tau_vir * np.log(analytic_v_c[R_index] / inital_velocity_v[R_index])

            log_taus_0 = np.log10(taus + tau_0)

            (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
             ) = get_dispersions(galaxy_name, save_name_append, snaps, get_bin_edges())
            # thin_bins = get_dex_bins(get_bin_edges(save_name_append='cum', galaxy_name=galaxy_name)[1::2])
            # print(thin_bins)
            # # print(get_bin_edges())
            # (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi
            #  ) = get_thin_bin_dispersions(galaxy_name, save_name_append, snaps, thin_bins)#, overwrite=True)

            #TODO doesn't match .. probably shoulds
            # (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
            #  ) = get_physical_from_model(galaxy_name, snaps, save_name_append)

            theory_best_v = np.zeros((len(bin_edges) // 2, snaps + 1))
            theory_best_z2 = np.zeros((len(bin_edges) // 2, snaps + 1))
            theory_best_r2 = np.zeros((len(bin_edges) // 2, snaps + 1))
            theory_best_p2 = np.zeros((len(bin_edges) // 2, snaps + 1))

            for i in range(len(bin_edges) // 2):
                theory_best_v[i] = get_theory_velocity2_array(v200, analytic_v_c[i] - inital_velocity_v[i],
                                                              time, t_c_dm[i], delta_dm[i], np.sqrt(upsilon_circ[i]),
                                                              ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
                theory_best_z2[i] = get_theory_velocity2_array(v200 ** 2, inital_velocity2_z[i],
                                                               time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                               ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
                theory_best_r2[i] = get_theory_velocity2_array(v200 ** 2, inital_velocity2_r[i],
                                                               time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                               ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
                theory_best_p2[i] = get_theory_velocity2_array(v200 ** 2, inital_velocity2_p[i],
                                                               time, t_c_dm[i], delta_dm[i], upsilon_dm[i],
                                                               ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

            theory_best_v = analytic_v_c[:, np.newaxis] - theory_best_v

            alpha_plot = 0.2
            lw = int(2*(-1. - np.log10(kwargs['mdm'])))
            window = snaps // 5 + 1
            if window % 2 == 0: window += 1
            poly_n = 5
            # vs time
    
            ax1.errorbar(log_taus_0, np.log10(mean_v_phi[:, _index] / v200),
                         c='C0', marker='', ls='-', lw=lw, alpha=alpha_plot)
            ax1.errorbar(log_taus_0, np.log10(savgol_filter(mean_v_phi[:, _index], window, poly_n) / v200),
                         c='C0', marker='', ls=kwargs['ls'], lw=lw)
            ax2.errorbar(log_taus_0, np.log10(mean_v_phi[:, _index] / v200),
                         c='C0', marker='', ls='-', lw=lw, alpha=alpha_plot)
            ax2.errorbar(log_taus_0, np.log10(savgol_filter(mean_v_phi[:, _index], window, poly_n) / v200),
                         c='C0', marker='', ls=kwargs['ls'], lw=lw)
            
            # ax3.errorbar(log_taus_0[:-1], np.diff(savgol_filter(mean_v_phi[:, _index], window, poly_n)
            #                                  / v200) / np.diff(taus),
            #              c='C0', marker='', ls=kwargs['ls'], lw=lw)

            #collisional heating (exponential)
            ax1.errorbar(log_taus_0, np.log10(theory_best_v[_index] / v200),
                         c='C1', marker='', ls='--', lw=lw, alpha=0.8)
            ax2.errorbar(log_taus_0, np.log10(theory_best_v[_index] / v200),
                         c='C1', marker='', ls='--', lw=lw, alpha=0.8)
            
            ax3.errorbar(log_taus_0[:-1], np.diff(theory_best_v[_index] / v200) / np.diff(taus),
                         c='C1', marker='', ls='--', lw=lw, alpha=0.8)

            #...........................
            #asymmetric drift model.....

            # # Emperical profile evolution
            # R_d_ICs = 4.16  # kpc
            # slope = 23.8  # kpc / scale time
            # R_disk = R_d_ICs + slope * taus

            # maths
            numerical_differential_p = (theory_best_r2[R_index + 1, :] - theory_best_r2[R_index, :]) / (
                    R_for_interp[R_index + 1] - R_for_interp[R_index])
            numerical_differential_m = (theory_best_r2[R_index, :] - theory_best_r2[R_index - 1, :]) / (
                    R_for_interp[R_index] - R_for_interp[R_index - 1])
            d_sigma_r2_d_R = (numerical_differential_p + numerical_differential_m) / 2

            print((1 - theory_best_p2[R_index] / (2 * analytic_v_c[R_index]**2) +
                   theory_best_r2[R_index] / (2 * analytic_v_c[R_index]**2) * (1 - R_for_interp[R_index] / R_disk) +
                   R_for_interp[R_index] / (2 * analytic_v_c[R_index]**2) * d_sigma_r2_d_R)[0])
            print(-(theory_best_p2[R_index] / (2 * analytic_v_c[R_index]**2))[0])
            print(((theory_best_r2[R_index] / (2 * analytic_v_c[R_index]**2) * (1 - R_for_interp[R_index] / R_disk)))[0])
            print(R_for_interp[R_index] / (2 * analytic_v_c[R_index]**2) * d_sigma_r2_d_R[0])
            print(((theory_best_r2[R_index] - theory_best_z2[R_index]) / analytic_v_c[R_index]**2)[0])
            print()

            # # Emperical profile evolution
            # R_d_ICs = 4.16  # kpc
            # slope = 23.8  # kpc / scale time
            # R_d = R_d_ICs + slope * taus[R_index, :]
    
            # v_phi_assymetry_on_v_c = analytic_v_c[R_index] * (
            #         1 - theory_best_p2[R_index] / (2 * analytic_v_c[R_index]**2) +
            #         theory_best_r2[R_index] / (2 * analytic_v_c[R_index]**2) * (1 - R_for_interp[R_index] / R_disk) +
            #         R_for_interp[R_index] / (2 * analytic_v_c[R_index]**2) * d_sigma_r2_d_R)
    
            term_0 = 1
            term_1 = - np.sqrt(theory_best_p2[R_index]) / analytic_v_c[R_index]
            t2 = theory_best_r2[R_index] / (analytic_v_c[R_index]**2) * (1 - R_for_interp[R_index] / R_disk)
            term_2 = np.sign(t2) * np.sqrt(np.abs(t2))
            t3 = R_for_interp[R_index] / (analytic_v_c[R_index]**2) * d_sigma_r2_d_R
            term_3 = np.sign(t3) * np.sqrt(np.abs(t3))
            t4 = (theory_best_r2[R_index] - theory_best_z2[R_index]) / analytic_v_c[R_index]**2
            term_4 = - np.sign(t4) * np.sqrt(np.abs(t4))
            
            total = np.sqrt(term_0**2 + np.sign(term_1) * term_1**2 + np.sign(term_2) * term_2**2 + np.sign(term_3) * term_3**2)
    
            v_phi2_assymetry_on_v_c2sq = analytic_v_c[R_index] * np.sqrt(
                1 - theory_best_p2[R_index] / (analytic_v_c[R_index]**2) +
                theory_best_r2[R_index] / (analytic_v_c[R_index]**2) * (1 - R_for_interp[R_index] / R_disk) +
                R_for_interp[R_index] / (analytic_v_c[R_index]**2) * d_sigma_r2_d_R)# -
                #(theory_best_r2[R_index] - theory_best_z2[R_index]) / analytic_v_c[R_index]**2)
    
            #asymmetric drift model
            # ax1.errorbar(log_taus_0, np.log10(v_phi_assymetry_on_v_c / v200),
            #              c='C2', marker='', ls=':', lw=3, alpha=0.8)
            # ax2.errorbar(log_taus_0, np.log10(v_phi_assymetry_on_v_c / v200),
            #              c='C2', marker='', ls=':', lw=3, alpha=0.8)
            
            ax1.errorbar(log_taus_0, np.log10(v_phi2_assymetry_on_v_c2sq / v200),
                         c='C3', marker='', ls='-.', lw=lw, alpha=0.8)
            ax2.errorbar(log_taus_0, np.log10(v_phi2_assymetry_on_v_c2sq / v200),
                         c='C3', marker='', ls='-.', lw=lw, alpha=0.8)

            ax3.errorbar(log_taus_0[:-1], np.diff(v_phi2_assymetry_on_v_c2sq / v200) / np.diff(taus),
                         c='C3', marker='', ls='-.', lw=lw, alpha=0.8)
            
            v_scale = analytic_v_c[R_index] / v200
            # ax3.errorbar(log_taus_0[:-1], np.diff(term_0 * v_scale) / np.diff(taus),
            #              c='C4', marker='', ls='--', lw=lw, alpha=0.8)
            ax3.errorbar(log_taus_0[:-1], np.diff(term_1 * v_scale) / np.diff(taus),
                         c='C5', marker='', ls='--', lw=lw, alpha=0.8)
            ax3.errorbar(log_taus_0[:-1], np.diff(term_2 * v_scale) / np.diff(taus),
                         c='C6', marker='', ls='--', lw=lw, alpha=0.8)
            ax3.errorbar(log_taus_0[:-1], np.diff(term_3 * v_scale) / np.diff(taus),
                         c='C7', marker='', ls='--', lw=lw, alpha=0.8)
            ax3.errorbar(log_taus_0[:-1], np.diff(term_4 * v_scale) / np.diff(taus),
                         c='C8', marker='', ls='--', lw=lw, alpha=0.8)
            ax3.errorbar(log_taus_0[:-1], np.diff(total * v_scale) / np.diff(taus),
                         c='C9', marker='', ls='--', lw=lw, alpha=0.8)

            ax3.errorbar(log_taus_0[:-1], np.diff(v_phi2_assymetry_on_v_c2sq / v200) / np.diff(taus),
                         c='C3', marker='', ls='-.', lw=lw, alpha=0.8)
    
        if _index == 0:
            # ax.set_ylabel(r'$\overline{v}_\phi$ [km/s]')
            # ax2.set_ylabel(r'$\overline{v}_\phi$ [km/s]')
            ax1.set_ylabel(r'$\log \, \overline{v}_\phi / V_{200}$')
            ax2.set_ylabel(r'$\log \, \overline{v}_\phi / V_{200}$')
            ax3.set_ylabel(r'$ \partial \, (\overline{v}_\phi / V_{200}) / \partial \tau$')
        # ax2.set_xlabel(r'$t$ [Gyr]')
        # ax3.set_xlabel(r'$\log \, t / t_c$')
        ax3.set_xlabel(r'$\log \, \tau$')

        if _index == 1:
            ax1.legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-', lw=3, alpha=0.8)),
                        (lines.Line2D([0, 1], [0, 1], color='C1', ls='--', lw=3, alpha=0.8)),
                        # (lines.Line2D([0, 1], [0, 1], color='C2', ls=':', lw=3, alpha=0.8)),
                        (lines.Line2D([0, 1], [0, 1], color='C3', ls='-.', lw=3, alpha=0.8))],
                       ['Smoothed data', 'Exponenital heating model',
                        "Jean's Asymmetric drift"], loc='lower left')

            ax3.legend([#(lines.Line2D([0, 1], [0, 1], color='C4', ls='--', lw=3, alpha=0.8)),
                        (lines.Line2D([0, 1], [0, 1], color='C5', ls='--', lw=3, alpha=0.8)),
                        (lines.Line2D([0, 1], [0, 1], color='C6', ls='--', lw=3, alpha=0.8)),
                        (lines.Line2D([0, 1], [0, 1], color='C7', ls='--', lw=3, alpha=0.8)),
                        (lines.Line2D([0, 1], [0, 1], color='C8', ls='--', lw=3, alpha=0.8)),
                        (lines.Line2D([0, 1], [0, 1], color='C9', ls='--', lw=3, alpha=0.8))],
                        [#r'$1$',
                         r'$\sigma_\phi^2$', r'$\sigma_R^2 (1 - R/d)$',
                         r'$R \partial \sigma_R^2 / \partial R$', r'$-(\sigma_R^2 - \sigma_z^2)$', 'total'],)
    
        # ax.set_xlim(0, T_TOT)
        # ax.set_ylim(0, analytic_v_c[R_index] * 1.05)
        # ax2.set_xlim(0, T_TOT)
        # ax2.set_ylim(analytic_v_c[R_index] * 0.9, analytic_v_c[R_index] * 1.01)


        ax1.set_ylim(-0.5 +np.log10(analytic_v_c[R_index] /v200), np.log10(analytic_v_c[R_index] /v200) + 0.05)
        ax2.set_ylim(-0.03+np.log10(analytic_v_c[R_index] /v200), np.log10(analytic_v_c[R_index] /v200) + 0.002)

        if _index == 0:
            best = [-3.35, -1.5]
            # ax3.set_ylim(-35, 10)
            ax3.set_ylim(-80, 80)
        elif _index == 1:
            best = [-3.65, -1.5]
            # ax3.set_ylim(-50, 10)
            ax3.set_ylim(-120, 120)
        elif _index == 2:
            best = [-3.68, -1.5]
            # ax3.set_ylim(-80, 10)
            ax3.set_ylim(-200, 200)

        ax1.set_xlim(*best)
        ax2.set_xlim(*best)
        ax3.set_xlim(*best)

        ax1.axhline(np.log10(analytic_v_c[R_index] /v200), 0, 1, c='grey', ls=':')
        ax2.axhline(np.log10(analytic_v_c[R_index] /v200), 0, 1, c='grey', ls=':')

    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return ()


def evaluate_surface_density_profiles(galaxy_name, snap, save=False):
    kwargs = get_name_kwargs(galaxy_name)

    # for snap in range(snaps+1):

    R_bin_edges = np.logspace(-1.5, 1.5, 31)
    R_bin_centres = 0.5 * (R_bin_edges[1:] + R_bin_edges[:-1])
    bin_area = np.pi * (R_bin_edges[1:] ** 2 - R_bin_edges[:-1] ** 2)

    R_d_ICs = 4.15
    M_disk = 1.86

    plt.figure()
    ax0 = plt.subplot(2, 1, 1)
    ax1 = plt.subplot(2, 1, 2)

    if type(snap) != str:
        snap = '{0:03d}'.format(int(snap))

    # read snap
    # for no align / centering
    # (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
    #  MassStar, PosStars, VelStars, IDStars, PotStars
    #  ) = load_nbody.load_snapshot(galaxy_name, snap)
    (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
     MassStar, PosStars, VelStars, IDStars, PotStars \
     ) = load_nbody.load_and_align(galaxy_name, snap)

    # measurements
    R = np.sqrt(PosStars[:, 0] ** 2 + PosStars[:, 1] ** 2)
    R = np.sort(R)
    N_R, _ = np.histogram(R, bins=R_bin_edges)

    sigma = MassStar * N_R / bin_area

    ax0.errorbar(R_bin_centres, np.log10(sigma), c=kwargs['c'], ls=kwargs['ls'])
    ax1.errorbar(R, np.linspace(0, 1, len(R)), c=kwargs['c'], ls=kwargs['ls'])

    # exponential ics
    sigma_ICs = get_exponential_surface_density(R_bin_centres, M_disk, R_d_ICs)
    mass_ICs = exponential_cumulative_mass(R_bin_centres, M_disk, R_d_ICs)

    ax0.errorbar(R_bin_centres, np.log10(sigma_ICs), c='grey', ls=':', lw=1)
    ax1.errorbar(R_bin_centres, mass_ICs / M_disk, c='grey', ls=':', lw=1)

    # work out best fit
    likely = lambda R_d: -np.sum(np.log(R / np.square(R_d) * np.exp(-R / R_d)))

    R_d = minimize(likely, R_d_ICs)
    R_d = R_d.x[0]

    sigma_ICs = get_exponential_surface_density(R_bin_centres, M_disk, R_d)
    mass_ICs = exponential_cumulative_mass(R_bin_centres, M_disk, R_d)

    ax0.errorbar(R_bin_centres, np.log10(sigma_ICs), c='k', ls='-.')
    ax1.errorbar(R_bin_centres, mass_ICs / M_disk, c='k', ls='-.')

    ax0.set_xlim([0, 30])
    ax1.set_xlim([0, 30])
    ax0.set_ylim([-5, -1])
    ax1.set_ylim([0, 1])

    ax1.set_xlabel(r'$R$')
    ax0.set_ylabel(r'$\log \Sigma(R)$ [$10^{10} $M$_\odot$ kpc$^{-2}$]')
    ax1.set_ylabel(r'$M(<R) / M$')

    if save:
        fname = '../galaxies/' + galaxy_name + '/results/surface_density_profile' + snap
        plt.savefig(fname, bbox_inches='tight')
        plt.close()

    return


def surface_density_profile_evolution(galaxy_name_list, snaps_list, save=False, name=''):
    fig = plt.figure(figsize=(12, 4))
    ax0 = plt.subplot(1, 3, 1)
    ax1 = plt.subplot(1, 3, 2)
    ax2 = plt.subplot(1, 3, 3)
    axs = [ax0, ax1, ax2]

    fig.subplots_adjust(wspace=0.01)

    ax1.set_yticklabels([])
    ax2.set_yticklabels([])

    ax0.text(2, 0.85, r'$\mu=1$')
    ax1.text(2, 0.85, r'$\mu=5$')
    ax2.text(2, 0.85, r'$\mu=25$')

    pth = [path_effects.Stroke(linewidth=3, foreground='black'), path_effects.Normal()]

    snaps_range = np.linspace(0, 1, 5)

    R_d_ICs = 4.15
    M_disk = 1.86

    for i, (g_list, s_list) in enumerate(zip(galaxy_name_list, snaps_list)):
        if i > 2: break

        R_bin_edges = np.logspace(-1, 2, 101)
        R_bin_centres = 0.5 * (R_bin_edges[1:] + R_bin_edges[:-1])
        bin_area = np.pi * (R_bin_edges[1:] ** 2 - R_bin_edges[:-1] ** 2)

        # mass_ICs = exponential_cumulative_mass(R_bin_centres, M_disk, R_d_ICs)
        # axs[i].errorbar(R_bin_centres, mass_ICs/M_disk, c='grey', ls=':', lw=1)

        R_d_best_fits = np.zeros((len(snaps_range), len(g_list)))
        R_particle_distances = []

        for k, (galaxy_name, snaps) in enumerate(zip(g_list, s_list)):
            kwargs = get_name_kwargs(galaxy_name)

            R_particle_distances.append([])

            for j, s_frac in enumerate(snaps_range):
                snap = int(snaps * s_frac)
                if snap == snaps:
                    snap -= 1
                snap = '{0:03d}'.format(int(snap))

                color = matplotlib.cm.get_cmap('viridis_r')(j / (len(snaps_range) - 1))

                R_particle_distances[k].append([])

                # read snap
                # for no align / centering
                # (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
                #  MassStar, PosStars, VelStars, IDStars, PotStars
                #  ) = load_nbody.load_snapshot(galaxy_name, snap)
                (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
                 MassStar, PosStars, VelStars, IDStars, PotStars
                 ) = load_nbody.load_and_align(galaxy_name, snap)

                # measurements
                R = np.sqrt(PosStars[:, 0] ** 2 + PosStars[:, 1] ** 2)
                R = np.sort(R)

                axs[i].errorbar(R, np.linspace(0, 1, len(R)), c=color, ls='-', lw=1, alpha=0.2)

                # work out best fit
                likely = lambda R_d: -np.sum(np.log(R / np.square(R_d) * np.exp(-R / R_d)))

                R_d = minimize(likely, R_d_ICs)
                R_d = R_d.x[0]

                R_d_best_fits[j, k] = R_d
                R_particle_distances[k][j].append(R)

        R_particle_distances = np.array(R_particle_distances)
        for j, s_frac in enumerate(snaps_range):
            color = matplotlib.cm.get_cmap('viridis_r')(j / (len(snaps_range) - 1))

            R_d = np.median(R_d_best_fits[j])

            exp_mass = exponential_cumulative_mass(R_bin_centres, M_disk, R_d)
            axs[i].errorbar(R_bin_centres, exp_mass / M_disk, c='k', ls='-.')

            R_medians = np.median(R_particle_distances[:, j, 0], axis=0)
            # R_medians = np.mean(R_particle_distances[:, j], axis=0)

            axs[i].errorbar(R_medians, np.linspace(0, 1, len(R_medians)), c=color, ls='-', lw=2, path_effects=pth)

        for ax in axs:
            ax.set_xlim([0, 29.9])
            ax.set_ylim([0, 1])

            ax.set_xlabel(r'$R$ [kpc]')

        ax0.set_ylabel(r'$M_\star(<R) / M_\star$')

        color_lines = [(lines.Line2D([0, 1], [0, 1],
                                     color=matplotlib.cm.get_cmap('viridis_r')(j / (len(snaps_range) - 1)), ls='-'))
                       for j, s_frac in enumerate(snaps_range)]
        color_text = [r'$t=$' + str(round(s_frac * T_TOT, 1)) + ' Gyr' for s_frac in snaps_range]
        ax0.legend(color_lines, color_text,
                   handler_map={tuple: HandlerTupleVertical(ndivide=5)},
                   loc='lower right', numpoints=1, handlelength=1, handletextpad=0.2, frameon=False,
                   labelspacing=-0.1, borderpad=0.2, bbox_to_anchor=(1.05, -0.05))

        ax1.legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='-', lw=1, alpha=0.2)),
                    (lines.Line2D([0, 1], [0, 1], color='grey', ls='-', lw=2, path_effects=pth)),
                    (lines.Line2D([0, 1], [0, 1], color='grey', ls='-.'))],
                   ['Different Seeds', 'Median Profile', 'Exponential Fit'],
                   handler_map={tuple: HandlerTupleVertical(ndivide=3)},
                   loc='lower right', numpoints=1, handlelength=1, handletextpad=0.2, frameon=False,
                   labelspacing=-0.1, borderpad=0.2, bbox_to_anchor=(1.05, -0.05))

    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return


def plot_exponential_evolution(galaxy_name_list, snaps_list, save_name_append='diff',
                               name='', save=False):
    _index = 1

    R_d_ICs = 4.16

    plt.figure(figsize=(6, 4))
    ax0 = plt.subplot(1, 1, 1)

    lin_fit_data_x_25 = []
    lin_fit_data_y_25 = []
    lin_fit_data_x_5 = []
    lin_fit_data_y_5 = []
    lin_fit_data_x_1 = []
    lin_fit_data_y_1 = []

    y_raw_25 = []
    y_sav_25 = []
    y_raw_5 = []
    y_sav_5 = []
    y_raw_1 = []
    y_sav_1 = []

    for galaxy_name, snaps in zip(galaxy_name_list, snaps_list):
        kwargs = get_name_kwargs(galaxy_name)

        try:
            (_, v_phi, _, _, _
             ) = get_dispersions(galaxy_name, save_name_append, snaps, get_bin_edges())
            R_ds = get_exponential_fit(galaxy_name, snaps, overwrite=False)

        except OSError as e:
            print(galaxy_name)
            print(e)
            pass

        time = np.linspace(0, T_TOT, snaps + 1)
        time -= time[BURN_IN]
        # work out constatns
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(get_bin_edges(), save_name_append, kwargs['mdm'])
        taus = get_tau_array(time, t_c_dm[_index])

        if 'mu_25' in galaxy_name:
            lin_fit_data_x_25.append(taus)
            lin_fit_data_y_25.append(R_ds)
            color = 'C9'
        if 'mu_5' in galaxy_name:
            lin_fit_data_x_5.append(taus)
            lin_fit_data_y_5.append(R_ds)
            color = 'C6'
        if 'mu_1' in galaxy_name:
            lin_fit_data_x_1.append(taus)
            lin_fit_data_y_1.append(R_ds)
            color = 'C5'

        alpha = 0.2
        lw = 4
        window = snaps // 5 + 1
        if window % 2 == 0: window += 1
        poly_n = 5
        # vs time
        x = taus
        y = R_ds
        ax0.errorbar(x, y,
                     # c=color, marker='', ls='-', lw=lw, alpha=alpha)
                     c=color, marker='', ls='-', lw=1, alpha=alpha)
        sav = savgol_filter(y, window, poly_n)
        # ax0.errorbar(x, sav,
        #              c=color, marker='', ls='-', lw=1)

        if 'mu_25' in galaxy_name:
            y_raw_25.append(y)
            y_sav_25.append(sav)
        if 'mu_5' in galaxy_name:
            y_raw_5.append(y)
            y_sav_5.append(sav)
        if 'mu_1' in galaxy_name:
            y_raw_1.append(y)
            y_sav_1.append(sav)

    pth = [path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()]
    alpha = 0.6

    y_raw_25_median = np.median(np.array(y_raw_25), axis=0)
    ax0.errorbar(x, y_raw_25_median,
                 c='C9', marker='', ls='-', lw=lw, alpha=alpha)
    # y_sav_25_median = np.median(np.array(y_sav_25), axis=0)
    # ax0.errorbar(x, y_sav_25_median,
    #                c='C9', marker='', ls='-', lw=3, path_effects=pth)
    ax0.errorbar(x, savgol_filter(y_raw_25_median, window, poly_n),
                 c='C9', marker='', ls='-', lw=3, path_effects=pth)

    y_raw_5_median = np.median(np.array(y_raw_5), axis=0)
    ax0.errorbar(x, y_raw_5_median,
                 c='C6', marker='', ls='-', lw=lw, alpha=alpha)
    # y_sav_5_median = np.median(np.array(y_sav_5), axis=0)
    # ax0.errorbar(x, y_sav_5_median,
    #                c='C6', marker='', ls='-', lw=3, path_effects=pth)
    ax0.errorbar(x, savgol_filter(y_raw_5_median, window, poly_n),
                 c='C6', marker='', ls='--', lw=3, path_effects=pth)

    y_raw_1_median = np.median(np.array(y_raw_1), axis=0)
    ax0.errorbar(x, y_raw_1_median,
                 c='C5', marker='', ls='-', lw=lw, alpha=alpha)
    # y_sav_1_median = np.median(np.array(y_sav_1), axis=0)
    # ax0.errorbar(x, y_sav_1_median,
    #              c='C5', marker='', ls='-', lw=3, path_effects=pth)
    ax0.errorbar(x, savgol_filter(y_raw_1_median, window, poly_n),
                 c='C5', marker='', ls=':', lw=3, path_effects=pth)

    lin_fit_data_x_25 = np.hstack(lin_fit_data_x_25)
    lin_fit_data_y_25 = np.hstack(lin_fit_data_y_25)
    chi2_25 = lambda slope: np.sum((lin_fit_data_y_25 - R_d_ICs - slope * lin_fit_data_x_25) ** 2)
    out_25 = minimize(chi2_25, 25)
    slope_25 = out_25.x

    lin_fit_data_x_5 = np.hstack(lin_fit_data_x_5)
    lin_fit_data_y_5 = np.hstack(lin_fit_data_y_5)
    chi2_5 = lambda slope: np.sum((lin_fit_data_y_5 - R_d_ICs - slope * lin_fit_data_x_5) ** 2)
    out_5 = minimize(chi2_5, 20)
    slope_5 = out_5.x

    lin_fit_data_x_1 = np.hstack(lin_fit_data_x_1)
    lin_fit_data_y_1 = np.hstack(lin_fit_data_y_1)
    chi2_1 = lambda slope: np.sum((lin_fit_data_y_1 - R_d_ICs - slope * lin_fit_data_x_1) ** 2)
    out_1 = minimize(chi2_1, 10)
    slope_1 = out_1.x

    print('mu=25 slope=', slope_25)
    print('mu=5  slope=', slope_5)
    print('mu=1  slope=', slope_1)

    _x = np.linspace(0, 0.101)
    ax0.errorbar(_x, R_d_ICs + slope_25 * _x, c='k', ls='-.', lw=2)
    ax0.errorbar(_x, R_d_ICs + slope_5 * _x, c='k', ls='-.', lw=2)
    ax0.errorbar(_x, R_d_ICs + slope_1 * _x, c='k', ls='-.', lw=2)

    ax0.set_xlabel(r'$\tau(R_{1/2})$')
    ax0.set_ylabel(r'$R_d$ [kpc]')

    ax0.set_xlim([0, 0.101])
    ax0.set_ylim([3.5, 7])

    ax0.legend([(lines.Line2D([0, 1], [0, 1], color='C9', ls='-', lw=3, path_effects=pth)),
                (lines.Line2D([0, 1], [0, 1], color='C6', ls='--', lw=3, path_effects=pth)),
                (lines.Line2D([0, 1], [0, 1], color='C5', ls=':', lw=3, path_effects=pth)),
                (lines.Line2D([0, 1], [0, 1], color='k', ls='-.', lw=2))],
               [r'$\mu = 25$', r'$\mu = 5$', r'$\mu = 1$', 'Linear Fit'],
               handler_map={tuple: HandlerTupleVertical(ndivide=5)},
               loc='upper left', numpoints=1, handlelength=1, handletextpad=0.2,
               labelspacing=0, borderpad=0.2)  # , bbox_to_anchor=(-0.025, -0.053))

    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return ()


def j_zonc_distribution_modelling():
    # where to measure
    R_half = get_bin_edges(save_name_append='cum')[3]
    bin_edges = get_dex_bins([R_half], 0.01)
    print(bin_edges)

    galaxy_name = 'mu_25/fdisk_0p01_lgMdm_6p0_V200-200kmps'
    snaps = 101
    snap = 88

    # load
    (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
     MassStar, PosStars, VelStars, IDStars, PotStars
     ) = load_nbody.load_and_align(galaxy_name, snap='{0:03d}'.format(snap))

    # calculate
    (R, phi, z, v_R, v_phi, v_z
     ) = get_cylindrical(PosStars, VelStars)

    mask = np.logical_and(R > bin_edges[0], R < bin_edges[1])

    R = R[mask]
    phi = phi[mask]
    z = z[mask]
    v_R = v_R[mask]
    v_phi = v_phi[mask]
    v_z = v_z[mask]

    mean_phi = np.mean(v_phi)
    std_phi = np.std(v_phi)
    std_z = np.std(v_z)
    std_R = np.std(v_R)
    std_tot = np.sqrt(std_phi ** 2 + std_z ** 2 + std_R ** 2)
    std_all = np.std(np.linalg.norm(VelStars[mask], axis=1))
    std2_all = np.std(np.linalg.norm(VelStars[mask], axis=1) ** 2)
    mean_all = np.mean(np.linalg.norm(VelStars[mask], axis=1))
    mean2_all = np.mean(np.linalg.norm(VelStars[mask], axis=1) ** 2)

    print(mean_phi)
    print(std_phi)
    print(std_z)
    print(std_R)
    print(std_tot)
    print()
    print(std_all)
    print(std2_all)
    print(mean_all)
    print(mean2_all)
    print()
    # print(np.sqrt(mean_phi**2 + std_tot**2/3))
    # print(4 * mean_phi / np.sqrt(3))

    j_z = R * v_phi
    j_c_E = get_j_c(PosStars[mask], VelStars[mask], PotStars[mask])

    j_c_lazy = R * np.sqrt(v_phi ** 2 + v_z ** 2 + v_R ** 2)

    vx = 210
    dx = 1

    # mask = np.logical_and(v_phi>vx-dx, v_phi<vx+dx)
    #
    # R = R[mask]
    # phi = phi[mask]
    # z = z[mask]
    # v_R = v_R[mask]
    # v_phi = v_phi[mask]
    # v_z = v_z[mask]
    #
    # plt.figure()
    # ax0 = plt.subplot(2,2,1)
    # ax1 = plt.subplot(2,2,2)
    # ax2 = plt.subplot(2,2,3)
    # ax3 = plt.subplot(2,2,4)
    #
    # v = np.linspace(0,1e5,1001)
    # u = np.linspace(0,500,1001)
    #
    # v_2 = v_phi**2 + v_z**2 + v_R**2
    # ax0.hist(v_2, density=True, bins=int(np.sqrt(len(j_z))))
    #
    # scale = (std_phi**2 + std_z**2 + std_R**2)/3
    #
    # # model = scipy.stats.ncx2.pdf(v, nc=mean_phi**2 / scale, df=3, loc=0, scale=scale)
    # model = scipy.stats.chi2.pdf(v, df=2, loc=vx**2, scale=(std_z**2 + std_R**2)/2)
    # ax0.errorbar(v, model)
    #
    # v_perp2 = v_z**2 + v_R**2
    # ax1.hist(v_perp2, density=True, bins=int(np.sqrt(len(j_z))))
    #
    # model = scipy.stats.chi2.pdf(v, df=2, loc=0, scale=(std_z**2 + std_R**2)/2)
    # ax1.errorbar(v, model)
    # ax1.set_xlim([0,5000])
    #
    # v_phi2 = v_phi**2
    # ax2.hist(v_phi2, density=True, bins=int(np.sqrt(len(j_z))), alpha=0.5)
    # # model = scipy.stats.ncx2.pdf(v, nc=mean_phi**2 / std_phi**2, df=1, loc=0, scale=std_phi**2)
    # # ax2.errorbar(v, model)
    # ax2.errorbar([vx**2]*2, [0,3e-3])
    #
    # v_1 = R_half * np.sqrt(v_phi**2 + v_z**2 + v_R**2)
    # ax3.hist(v_1, density=True, bins=int(np.sqrt(len(j_z))), alpha=0.5)
    #
    # ax3.hist(j_c_E[mask], density=True, bins=int(np.sqrt(len(j_z))), alpha=0.5)
    #
    # # model = scipy.stats.ncx2.pdf(u**2, nc=mean_phi**2 / scale, df=3, loc=0, scale=scale) * 2 * u
    # model = scipy.stats.chi2.pdf(u**2, df=2, loc=vx**2, scale=(std_z**2 + std_R**2)/2) * 2 * u
    # ax3.errorbar(R_half * u, model / R_half)
    #
    # jc = np.linspace(1e3,2e3, 1001)
    #
    # (interp, derive_interp) = get_speed_jc_interpolator(R_half)
    # model = (scipy.stats.chi2.pdf(speed_from_jc(jc, interp), df=2, loc=vx**2, scale=(std_z**2 + std_R**2)/2) *
    #          np.abs(derive_speed_from_jc(jc, derive_interp)))
    # ax3.errorbar(jc, model)
    # print(model)
    #
    # ax0.set_xlabel(r'$|\vec{v}|^2$')
    # ax1.set_xlabel(r'$v_R^2 + v_z^2$')
    # ax2.set_xlabel(r'$v_\phi^2$')
    # ax3.set_xlabel(r'$R |\vec{v}|$ or $j_c(E)$')
    #
    # ax0.set_xlim([43000,50000])
    # ax1.set_xlim([0,4000])
    # ax2.set_xlim([43500,45000])
    # ax3.set_xlim([1400,1600])

    # plt.figure()
    # ax0 = plt.subplot(2,2,1)
    # ax1 = plt.subplot(2,2,2)
    # ax2 = plt.subplot(2,2,3)
    # ax3 = plt.subplot(2,2,4)
    #
    # v = np.linspace(0,1e5,1001)
    # u = np.linspace(0,500,1001)
    #
    # v_2 = v_phi**2 + v_z**2 + v_R**2
    # ax0.hist(v_2, density=True, bins=int(np.sqrt(len(j_z))))
    #
    # scale = (std_phi**2 + std_z**2 + std_R**2)/3
    #
    # model = scipy.stats.ncx2.pdf(v, nc=mean_phi**2 / scale, df=3, loc=0, scale=scale)
    # ax0.errorbar(v, model)
    #
    # v_perp2 = v_z**2 + v_R**2
    # ax1.hist(v_perp2, density=True, bins=int(np.sqrt(len(j_z))))
    #
    # model = scipy.stats.chi2.pdf(v, df=2, loc=0, scale=(std_z**2 + std_R**2)/2)
    # ax1.errorbar(v, model)
    # ax1.set_xlim([0,5000])
    #
    #
    # v_phi2 = v_phi**2
    # ax2.hist(v_phi2, density=True, bins=int(np.sqrt(len(j_z))))
    #
    # model = scipy.stats.ncx2.pdf(v, nc=mean_phi**2 / std_phi**2, df=1, loc=0, scale=std_phi**2)
    # ax2.errorbar(v, model)
    #
    # v_1 = np.sqrt(v_phi**2 + v_z**2 + v_R**2)
    # ax3.hist(v_1, density=True, bins=int(np.sqrt(len(j_z))))
    #
    # model = scipy.stats.ncx2.pdf(u**2, nc=mean_phi**2 / scale, df=3, loc=0, scale=scale) * 2 * u
    # ax3.errorbar(u, model)
    #
    # ax0.set_xlabel(r'$|\vec{v}|^2$')
    # ax1.set_xlabel(r'$v_R^2 + v_z^2$')
    # ax2.set_xlabel(r'$v_\phi^2$')
    # ax3.set_xlabel(r'$|\vec{v}|$')

    # plt.figure()

    # plt.plot(j_c, speed_2)
    # plt.plot(j_c_E, np.linalg.norm(VelStars[mask]**2, axis=1), ls='', marker='.')

    #########################

    plt.figure()
    ax0 = plt.subplot(2, 2, 1)
    ax1 = plt.subplot(2, 2, 2)
    ax2 = plt.subplot(2, 2, 3)
    ax3 = plt.subplot(2, 2, 4)

    # ax1.set_visible(False)
    ax1.hist(j_z / j_c_E, alpha=0.5, density=True, bins=int(np.sqrt(len(j_z))))
    ax1.hist(j_z / j_c_lazy, alpha=0.5, density=True, bins=int(np.sqrt(len(j_z))))

    # n = 201
    # j_zonc = np.linspace(0,1.1,n)
    n = 21
    j_zonc = np.linspace(0.90, 1.01, n)
    f_j_zonc_lazy = np.zeros(n)
    f_j_zonc = np.zeros(n)

    large_lazy = 1e4
    large = 1e5
    scale_c2 = (std_z ** 2 + std_R ** 2) / 2
    scale_nc = std_phi ** 2
    nc_nc = mean_phi ** 2 / scale_nc

    (interp, derive_interp) = get_speed_jc_interpolator(R_half)
    for i, j_zc in enumerate(j_zonc):
        integrand_lazy = lambda y: (4 * j_zc * np.abs(y) * y ** 2 *
                                    scipy.stats.chi2.pdf(y ** 2, df=2, loc=j_zc ** 2 * y ** 2, scale=scale_c2) *
                                    scipy.stats.ncx2.pdf(j_zc ** 2 * y ** 2, nc=nc_nc, df=1, loc=0, scale=scale_nc))

        # out = quad(integrand, -large_lazy, large_lazy)
        out = quad(integrand_lazy, 0, large_lazy)

        f_j_zonc_lazy[i] = out[0]

        # #need this for negative values
        # out = quad(integrand_lazy, -large_lazy, 0)
        # f_j_zonc[i] += out[0]

        integrand = lambda y: (2 * j_zc * y * np.abs(y) / R_half ** 2 *
                               scipy.stats.chi2.pdf(speed_from_jc(y, interp), df=2,
                                                    loc=j_zc ** 2 * y ** 2 / R_half ** 2, scale=scale_c2) *
                               scipy.stats.ncx2.pdf(j_zc ** 2 * y ** 2 / R_half ** 2, nc=nc_nc / R_half ** 2, df=1,
                                                    loc=0, scale=scale_nc * R_half ** 2) *
                               np.abs(derive_speed_from_jc(y, derive_interp)))

        # out = quad(integrand, -large, large)
        out = quad(integrand, 0, large)

        f_j_zonc[i] = out[0]

        # #need this for negative values
        # out = quad(integrand, -large, 0)
        # f_j_zonc[i] += out[0]

    ax1.errorbar(j_zonc, f_j_zonc_lazy)

    ax1.errorbar(j_zonc, f_j_zonc)

    ax0.hist(j_z, density=True, bins=int(np.sqrt(len(j_z))))

    x = np.linspace(0, 2000, 2001)
    v = np.linspace(-400, 600, 1001)
    u = np.linspace(0, 1e3, 1001)
    w = np.linspace(0, 1e4, 1001)
    ax0.errorbar(R_half * v, scipy.stats.norm.pdf(v, loc=mean_phi, scale=std_phi) / R_half)

    ax2.errorbar(j_z, j_c_E, alpha=0.5, ls='', marker='.')
    ax2.errorbar(j_z, j_c_lazy, alpha=0.5, ls='', marker='.')

    # ax3.hist(j_c_E, alpha=0.5, density=True, bins=int(np.sqrt(len(j_z))))
    # ax3.hist(j_c_lazy, alpha=0.5, density=True, bins=int(np.sqrt(len(j_z))))
    #
    # model = scipy.stats.ncx2.pdf(u**2, nc=mean_phi/np.sqrt(3), df=3, loc=0, scale=(std_phi**2 + std_z**2 + std_R**2)/3) * 2 * u
    # ax3.errorbar(R_half * u, model / R_half)

    ax3.hist(j_c_E, alpha=0.5, density=True, bins=int(np.sqrt(len(j_z))))
    ax3.hist(j_c_lazy, alpha=0.5, density=True, bins=int(np.sqrt(len(j_z))))

    model = scipy.stats.ncx2.pdf(u ** 2, nc=mean_phi ** 2 * 3 / std_tot ** 2, df=3, loc=0,
                                 scale=std_tot ** 2 / 3) * 2 * u
    ax3.errorbar(R_half * u, model / R_half)

    #####

    (interp, derive_interp) = get_speed_jc_interpolator(R_half)

    # j_c = np.linspace(0,1e4,1001)
    # speed_2 = speed_from_jc(j_c, interp)
    # derive = derive_speed_from_jc(j_c, derive_interp)

    model = (scipy.stats.ncx2.pdf(speed_from_jc(w, interp), nc=mean_phi ** 2 * 3 / std_tot ** 2, df=3, loc=0,
                                  scale=std_tot ** 2 / 3) *
             np.abs(derive_speed_from_jc(w, derive_interp)))
    ax3.errorbar(w, model)

    ax2.errorbar([-100, 2500], [-100, 2500], ls=':', c='k')

    ax0.set_xlabel(r'$j_z$ [kpc km/s]')
    ax0.set_xlabel(r'$j_z$ [kpc km/s]')
    ax1.set_xlabel(r'$j_z/j_c$')
    ax2.set_xlabel(r'$j_z$ [kpc km/s]')
    ax2.set_ylabel(r'$j_c$ [kpc km/s]')
    ax3.set_xlabel(r'$j_c$ [kpc km/s]')

    ax0.set_xlim([-100, 2500])
    # ax1.set_xlim([-1.1,1.1])
    ax1.set_xlim([0.9, 1.01])
    ax2.set_xlim([-100, 2500])
    ax2.set_ylim([-100, 2500])
    ax3.set_xlim([-100, 2500])

    return


# def make_EJ_movie(frames=60*16, time=60, end_time=9.7):
def make_EJ_movie(frames=400, time=60):
    galaxy_name = 'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps'
    snaps = 400

    max_snaps = frames #int(np.ceil(end_time / dt_snaps))

    for snap in (0, 399): #range(max_snaps + 1):

        (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
         MassStar, PosStars, VelStars, IDStars, PotStars
         # ) = load_nbody.load_snapshot(galaxy_name, snap)
         ) = load_nbody.load_and_align(galaxy_name, '{0:03d}'.format(snap))

        fig = plt.figure(constrained_layout=True)
        fig.set_size_inches(6, 5, forward=True)

        ax = plt.subplot(1, 1, 1)

        ax.set_xlabel(r'Lz [10$^3$ kpc km/s]')
        ax.set_ylabel(r'E [10$^5$ km$^2$ / s$^2$]')
        
        xlim = [-6.4, 6.4]
        ylim = [-2.6, -1]
        ax.set_xlim(xlim)
        ax.set_ylim(ylim)
        ax.set_yticks([-2.5, -2, -1.5, -1])

        Lz = np.cross(PosStars, VelStars)[:, 2]
        E = 0.5 * np.linalg.norm(VelStars, axis=1)**2 + PotStars

        # plt.hist2d(Lz * 1e-3, E * 1e-5, bins=[np.linspace(xlim[0], xlim[1], 101), np.linspace(ylim[0], ylim[1], 121)],
        #            cmap='Greys', clog=True, cmin=1, cmax=100)
        plt.hexbin(Lz * 1e-3, E * 1e-5, gridsize=100,
                   cmap='Greys', reduce_C_function = lambda x: np.log10(np.sum(x)), vmax=2)

        # plt.errorbar(Lz * 1e-3, E * 1e-5, c='k', ls='', marker='.')

        #
        ax.text(xlim[0] + 0.1 * (xlim[1] - xlim[0]), ylim[0] + 0.1 * (ylim[1] - ylim[0]),
                r'$t = {0:1.2f}$ Gyr'.format(snap / snaps * T_TOT), color='k')

        name = (
            '/home/matt/Documents/UWA/disk-heating/galaxies/mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps/results/projections/EJframe{0:03d}'.format(
                int(snap)))

        print(name)
        
        # plt.show()
        
        plt.savefig(name, bbox_inches="tight")
        plt.close()

    print('making gif')

    fps = frames / time
    print(fps)

    # fname = '/home/matt/Documents/UWA/disk-heating/galaxies/mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps/results/EJmovie'
    #
    # w = imageio.get_writer(fname + '.mp4', format='FFMPEG', mode='I', fps=fps)  # ,
    # # codec='h264_vaapi',
    # # output_params=['-vaapi_device',
    # #               '/dev/dri/renderD128', '-vf',
    # #               'format=gray|nv12,hwupload'],
    # # pixelformat='vaapi_vld')
    #
    # for i in range(frames):
    #     w.append_data(imageio.imread(
    #         '/home/matt/Documents/UWA/disk-heating/galaxies/mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps/results/projections/EJframe{0:03d}.png'.format(
    #             int(i))))
    # w.close()

    return

import healpy as hp

def make_Jangle_movie(frames=400, time=60):
# def make_Jangle_movie(frames=10, time=1.5):
    galaxy_name = 'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps'
    snaps = 400

    max_snaps = frames  # int(np.ceil(end_time / dt_snaps))

    for snap in range(max_snaps + 1):
        (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
         MassStar, PosStars, VelStars, IDStars, PotStars
         # ) = load_nbody.load_snapshot(galaxy_name, snap)
         ) = load_nbody.load_and_align(galaxy_name, '{0:03d}'.format(snap))

        fig = plt.figure(1)#(constrained_layout=True)
        # fig.set_size_inches(10, 10, forward=True)
        # 
        # ax = plt.subplot(1, 1, 1)
        # 
        # ax.set_xlabel(r'$\hat{\phi}_J$')
        # ax.set_ylabel(r'$\hat{\theta}_J$')
        # 
        # xlim = [-np.pi, np.pi]
        # ylim = [-np.pi/2, np.pi/2]
        # ax.set_xlim(xlim)
        # ax.set_ylim(ylim)

        j = np.cross(PosStars, VelStars)
        # j = np.cross(PosDMs, VelDMs)
        
        jt = np.linalg.norm(j, axis=1)
        phi = np.arctan2(j[:, 2], j[:, 1])
        theta = np.arccos(j[:, 0] / (jt + 1e-6))
        
        def norm_phi(phi):
            for i, p in enumerate(phi):
                if p > np.pi:
                    phi[i] -= 2 * np.pi
                if p < -np.pi:
                    phi[i] += 2 * np.pi
            
            return phi

        def norm_theta(phi):
            for i, p in enumerate(phi):
                if p > np.pi/2:
                    phi[i] -= np.pi
                # if p < np.pi/2:
                #     phi[i] += np.pi
                    
            return phi

        phi = norm_phi(phi - np.pi/2)
        theta = norm_theta(theta - np.pi/2)
        
        j = Rotation.from_euler('y', 180, degrees=True).apply(j)
        # j = Rotation.from_euler('yx', [90, 90], degrees=True).apply(j)
        
        # # plt.hist2d(Lz * 1e-3, E * 1e-5, bins=[np.linspace(xlim[0], xlim[1], 101), np.linspace(ylim[0], ylim[1], 121)],
        # #            cmap='Greys', clog=True, cmin=1, cmax=100)
        # plt.hexbin(phi, theta, gridsize=100,
        #            cmap='Greys', reduce_C_function=lambda x: np.log10(np.sum(x)), vmax=10)
        #
        # ax.text(xlim[0] + 0.1 * (xlim[1] - xlim[0]), ylim[0] + 0.1 * (ylim[1] - ylim[0]),
        #         r'$t = {0:1.2f}$ Gyr'.format(snap / snaps * T_TOT), color='k')

        NSIDE = 16
        NPIX = hp.nside2npix(NSIDE)

        m = np.zeros(NPIX)

        for _j in j:
            # r = 1
            # while r >= 1:
            #     _j = 1 - 2*np.random.random(3)
            #     r = np.linalg.norm(_j)

            ipix_disc = hp.vec2pix(NSIDE, _j[0], _j[1], _j[2])
            m[ipix_disc] += 1

        m = np.log10(m)

        # j = np.array((0,0,0))
        # j = Rotation.from_euler('yx', [90, 90], degrees=True).apply(j)
        # ipix_disc = hp.vec2pix(NSIDE, j[0], j[1], j[2])
        # print(ipix_disc)
        # m[ipix_disc] = 10

        # hp.mollview(m, title='', fig=1, max=2.0, cbar=False, cmap='Greys')
        # hp.graticule()
        
        hp.projview(m, title='', fig=1, reuse_axes=True, max=2.0, cbar=False, cmap='Greys',
                    coord=['G'], graticule=True, #graticule_labels=True,
                    projection_type='polar')

        name = (
            '/home/matt/Documents/UWA/disk-heating/galaxies/mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps/results/projections/Jangleframe{0:03d}'.format(
                int(snap)))

        print(name)
        # plt.show()
        
        # raise NotImplimentedError()

        plt.savefig(name, bbox_inches="tight")
        plt.close()

    print('making gif')

    fps = frames / time
    print(fps)

    fname = '/home/matt/Documents/UWA/disk-heating/galaxies/mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps/results/Janglemovie'

    w = imageio.get_writer(fname + '.mp4', format='FFMPEG', mode='I', fps=fps)  # ,
    # codec='h264_vaapi',
    # output_params=['-vaapi_device',
    #               '/dev/dri/renderD128', '-vf',
    #               'format=gray|nv12,hwupload'],
    # pixelformat='vaapi_vld')

    for i in range(frames):
        w.append_data(imageio.imread(
            '/home/matt/Documents/UWA/disk-heating/galaxies/mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps/results/projections/Jangleframe{0:03d}.png'.format(
                int(i))))
    w.close()

    return


# def make_sph_movie(frames=60*16, time=60, end_time=9.7):
def make_sph_movie(frames=20, time=2, end_time=0.2):
    galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed9',
                        'mu_5_smooth/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed9']
    snaps_list = [400, 101]
    particle_numbers = [18405, 930]

    frame_dt = end_time / frames

    dt_snaps = [T_TOT / snaps for snaps in snaps_list]
    max_snaps = [int(np.ceil(end_time / dt_s)) for dt_s in dt_snaps]

    n_gals = 2

    min_hsml = 3e-1
    max_hsml = 3e-1
    dm_min_hsml = 1e0
    dm_max_hsml = 1e0
    # dm_c_max = 1.8
    dm_c_max = 2.5

    extent = [-25, 25, -25, 25]

    cmap_star = matplotlib.cm.Greys_r
    cmap_dm = matplotlib.cm.Blues_r

    cmap_smooth = matplotlib.cm.Greens_r

    xpix = 500
    ypix = 500
    xs = np.linspace(extent[0], extent[1], xpix)
    ys = np.linspace(extent[2], extent[3], ypix)
    xs, ys = np.meshgrid(xs, ys)
    Rs = np.sqrt(xs ** 2 + ys ** 2)

    a_h, vmax = get_hernquist_params(galaxy_name_list[0])
    GM = 4 * a_h * vmax ** 2  # kpc (km/s)**2
    GM *= (PC_ON_M / GYR_ON_S) ** 2  # kpc (kpc / Gyr)**2

    img_smooth_dm = np.log10(hernquist_projected_intenisty(Rs, 3e3, a_h))
    rgb_smooth_dm = cmap_smooth(get_normalized_image(img_smooth_dm, vmax=dm_c_max * np.max(img_smooth_dm)))

    LivePosDMs = np.zeros((max_snaps[0] + 1, particle_numbers[0], 3))
    LiveVelDMs = np.zeros((max_snaps[0] + 1, particle_numbers[0], 3))
    LivePosStars = np.zeros((max_snaps[0] + 1, particle_numbers[1], 3))
    LiveVelStars = np.zeros((max_snaps[0] + 1, particle_numbers[1], 3))

    print('reading files')

    # read a files
    for snap in range(max_snaps[0] + 1):
        ssnap = '{0:03d}'.format(int(snap))
        (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
         MassStar, PosStars, VelStars, IDStars, PotStars
         ) = load_nbody.load_snapshot(galaxy_name_list[0], ssnap)

        LivePosDMs[snap] = PosDMs
        LiveVelDMs[snap] = VelDMs
        LivePosStars[snap] = PosStars
        LiveVelStars[snap] = VelStars

    SmoothPosStars = np.zeros((max_snaps[0] + 1, particle_numbers[1], 3))
    SmoothVelStars = np.zeros((max_snaps[0] + 1, particle_numbers[1], 3))

    for snap in range(max_snaps[1] + 1):
        ssnap = '{0:03d}'.format(int(snap))
        (_, _, _, _, _, _,
         _, PosStars_smooth, VelStars_smooth, IDStars_smooth, PotStars_smooth
         ) = load_nbody.load_snapshot(galaxy_name_list[1], ssnap)

        SmoothPosStars[snap] = PosStars_smooth
        SmoothVelStars[snap] = VelStars_smooth

    print('interpolating particles')

    # km/s to kpc/gyr
    speed_factor = PC_ON_M / GYR_ON_S

    # PC_ON_M = 3.0857e16  # pc/m = kpc/km (wiki)
    # GYR_ON_S = 3.15576e16  # gyr/s

    PosDMs = np.zeros((frames, particle_numbers[0], 3))
    PosStars = np.zeros((frames, particle_numbers[1], 3))

    # From Danail's code https://github.com/obreschkow/simstar/blob/master/R/surfsmovie.R
    for frame in range(frames):
        frame_time = frame_dt * frame  # Gyr
        snap_minus = int(np.floor(frame_time / dt_snaps[0]))
        snap_plus = int(np.ceil(frame_time / dt_snaps[0]))
        if snap_minus == snap_plus:
            snap_plus += 1
        t0 = dt_snaps[0] * snap_minus
        t1 = dt_snaps[0] * snap_plus
        ht = dt_snaps[0]  # Gyr
        dt = (frame_time - t0) / ht

        x0 = LivePosDMs[snap_minus]  # kpc
        v0 = LiveVelDMs[snap_minus] * speed_factor  # * ht #kpc / Gyr
        x1 = LivePosDMs[snap_plus]  # kpc
        v1 = LiveVelDMs[snap_plus] * speed_factor  # * ht #kpc / Gyr

        PosDMs[frame] = rk4_3d_hernquist_forwards_backwards_interpolate(x0, v0, x1, v1, t0, t1, frame_time, GM, a_h)
        # PosDMs[frame] = spline_hernquist_interpolate(x0, v0, x1, v1, t0, t1, frame_time, GM, a_h)

        x0 = LivePosStars[snap_minus]  # kpc
        v0 = LiveVelStars[snap_minus] * speed_factor  # * ht #kpc / Gyr
        x1 = LivePosStars[snap_plus]  # kpc
        v1 = LiveVelStars[snap_plus] * speed_factor  # * ht #kpc / Gyr

        PosStars[frame] = rk4_3d_hernquist_forwards_backwards_interpolate(x0, v0, x1, v1, t0, t1, frame_time, GM, a_h)
        # PosStars[frame] = spline_hernquist_interpolate(x0, v0, x1, v1, t0, t1, frame_time, GM, a_h)

    PosStars_smooth = np.zeros((frames, particle_numbers[1], 3))

    for frame in range(frames):
        frame_time = frame_dt * frame  # Gyr
        snap_minus = int(np.floor(frame_time / dt_snaps[1]))
        snap_plus = int(np.ceil(frame_time / dt_snaps[1]))
        if snap_minus == snap_plus:
            snap_plus += 1
        t0 = dt_snaps[1] * snap_minus
        t1 = dt_snaps[1] * snap_plus
        ht = dt_snaps[1]  # Gyr
        dt = (frame_time - t0) / ht

        x0 = SmoothPosStars[snap_minus]
        v0 = SmoothVelStars[snap_minus] * speed_factor  # * ht #kpc / Gyr
        x1 = SmoothPosStars[snap_plus]
        v1 = SmoothVelStars[snap_plus] * speed_factor  # * ht #kpc / Gyr

        PosStars_smooth[frame] = rk4_3d_hernquist_forwards_backwards_interpolate(x0, v0, x1, v1, t0, t1, frame_time, GM,
                                                                                 a_h)
        # PosStars_smooth[frame] = spline_hernquist_interpolate(x0, v0, x1, v1, t0, t1, frame_time, GM, a_h)

    print('rendering frames')

    for frame in range(frames):
        fig = plt.figure(constrained_layout=True)
        fig.set_size_inches(10, 10, forward=True)

        ax00 = plt.subplot(2, 2, 1)
        ax10 = plt.subplot(2, 2, 2)
        ax01 = plt.subplot(2, 2, 3)
        ax11 = plt.subplot(2, 2, 4)

        plt.subplots_adjust(wspace=0.02, hspace=0.00)

        ax01.set_xlabel('x [kpc]')
        ax11.set_xlabel('x [kpc]')
        # axs[1][2].set_xlabel('x [kpc]')
        ax00.set_ylabel('y [kpc]')
        ax01.set_ylabel('z [kpc]')

        ax00.set_xticklabels([])
        ax10.set_xticklabels([])
        ax10.set_yticklabels([])
        ax11.set_yticklabels([])

        qv_star = QuickView(PosStars[frame], r='infinity', plot=False, logscale=True, min_hsml=min_hsml,
                            max_hsml=max_hsml,
                            x=0, y=0, z=0, extent=extent, t=0, origin='lower')
        qv_dm = QuickView(PosDMs[frame], r='infinity', plot=False, logscale=True, min_hsml=dm_min_hsml,
                          max_hsml=dm_max_hsml,
                          x=0, y=0, z=0, extent=extent, t=0, origin='lower')

        img_star = qv_star.get_image()
        img_dm = qv_dm.get_image()

        rgb_star = cmap_star(get_normalized_image(img_star))
        rgb_dm = cmap_dm(get_normalized_image(img_dm, vmax=dm_c_max * np.max(img_dm)))

        blend = Blend.Blend(rgb_star, rgb_dm)
        rgb_output = blend.Screen()

        ax00.imshow(rgb_output, extent=qv_star.get_extent())
        #

        qv_star = QuickView(PosStars[frame], r='infinity', plot=False, logscale=True, min_hsml=min_hsml,
                            max_hsml=max_hsml,
                            x=0, y=0, z=0, extent=extent, t=90, origin='lower')
        qv_dm = QuickView(PosDMs[frame], r='infinity', plot=False, logscale=True, min_hsml=dm_min_hsml,
                          max_hsml=dm_max_hsml,
                          x=0, y=0, z=0, extent=extent, t=90, origin='lower')

        img_star = qv_star.get_image()
        img_dm = qv_dm.get_image()

        rgb_star = cmap_star(get_normalized_image(img_star))
        rgb_dm = cmap_dm(get_normalized_image(img_dm, vmax=dm_c_max * np.max(img_dm)))

        blend = Blend.Blend(rgb_star, rgb_dm)
        rgb_output = blend.Screen()

        ax01.imshow(rgb_output, extent=qv_star.get_extent())

        ######
        qv_smooth_star = QuickView(PosStars_smooth[frame], r='infinity', plot=False, logscale=True, min_hsml=min_hsml,
                                   max_hsml=max_hsml, x=0, y=0, z=0, extent=extent, t=0, origin='lower')

        img_smooth_star = qv_smooth_star.get_image()

        rgb_smooth_star = cmap_star(get_normalized_image(img_smooth_star))

        blend = Blend.Blend(rgb_smooth_star, rgb_smooth_dm)
        rgb_output = blend.Screen()

        ax10.imshow(rgb_output, extent=qv_star.get_extent())
        #
        #
        qv_smooth_star = QuickView(PosStars_smooth[frame], r='infinity', plot=False, logscale=True, min_hsml=min_hsml,
                                   max_hsml=max_hsml, x=0, y=0, z=0, extent=extent, t=90, origin='lower')

        img_smooth_star = qv_smooth_star.get_image()

        rgb_smooth_star = cmap_star(get_normalized_image(img_smooth_star))

        blend = Blend.Blend(rgb_smooth_star, rgb_smooth_dm)
        rgb_output = blend.Screen()

        ax11.imshow(rgb_output, extent=qv_star.get_extent())

        #
        frame_time = frame_dt * frame  # Gyr
        ax11.text(-20, -20, r'$t = {0:1.2f}$ Gyr'.format(frame_time), color='white')

        name = (
            '/home/matt/Documents/UWA/disk-heating/galaxies/mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps/results/projections/frame{0:03d}'.format(
                int(frame)))

        print(name)

        plt.savefig(name, bbox_inches="tight")
        plt.close()

    print('making gif')

    fps = frames / time
    print(fps)

    fname = '/home/matt/Documents/UWA/disk-heating/galaxies/mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps/results/movie'

    w = imageio.get_writer(fname + '.mp4', format='FFMPEG', mode='I', fps=fps)  # ,
    # codec='h264_vaapi',
    # output_params=['-vaapi_device',
    #               '/dev/dri/renderD128', '-vf',
    #               'format=gray|nv12,hwupload'],
    # pixelformat='vaapi_vld')

    for i in range(frames):
        w.append_data(imageio.imread(
            '/home/matt/Documents/UWA/disk-heating/galaxies/mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps/results/projections/frame{0:03d}.png'.format(
                int(i))))
    w.close()

    return


def rk4_3d_hernquist_forwards_backwards_interpolate(x0, v0, x1, v1, t0, t1, taim, GM, a_h, com=np.zeros(3), ten=10):
    '''
  '''
    # loop
    # for i in np.arange(1, n_tot):

    x0 = x0 - com

    # hernquist acceleration #- gradient of potential
    dv_dt = lambda x, r: - GM / (r[:, np.newaxis] + a_h) ** 2 * (x / r[:, np.newaxis]) * PC_ON_M ** 2 / GYR_ON_S ** 2

    # forwards
    dt = (taim - t0) / ten
    for i in range(ten):
        r0 = np.linalg.norm(x0, axis=1)

        # step 1
        xone = dt * v0
        vone = dt * dv_dt(x0, r0)

        rone = np.linalg.norm(x0 + xone / 2, axis=1)

        # step 2
        xtwo = dt * (v0 + vone / 2)
        vtwo = dt * dv_dt(x0 + xone / 2, rone)

        rtwo = np.linalg.norm(x0 + xtwo / 2, axis=1)

        # step 3
        xthree = dt * (v0 + vtwo / 2)
        vthree = dt * dv_dt(x0 + xtwo / 2, rtwo)

        rthree = np.linalg.norm(x0 + xthree, axis=1)

        # step 4
        xfour = dt * (v0 + vthree)
        vfour = dt * dv_dt(x0 + xthree, rthree)

        # combine everything
        xplus1 = x0 + 1 / 6 * (xone + 2 * xtwo + 2 * xthree + xfour)
        vplus1 = v0 + 1 / 6 * (vone + 2 * vtwo + 2 * vthree + vfour)

        x0 = xplus1
        v0 = vplus1

    forwards_x = xplus1.copy()

    # #backwards
    # v1 = -v1
    # dt = (t1 - taim) / ten
    # for i in range(ten):
    #   r0 = np.linalg.norm(x1, axis=1)
    #
    #   # step 1
    #   xone = dt * v1
    #   vone = dt * dv_dt(x1, r0)
    #
    #   rone = np.linalg.norm(x1 + xone / 2, axis=1)
    #
    #   # step 2
    #   xtwo = dt * (v1 + vone / 2)
    #   vtwo = dt * dv_dt(x1 + xone / 2, rone)
    #
    #   rtwo = np.linalg.norm(x1 + xtwo / 2, axis=1)
    #
    #   # step 3
    #   xthree = dt * (v1 + vtwo / 2)
    #   vthree = dt * dv_dt(x1 + xtwo / 2, rtwo)
    #
    #   rthree = np.linalg.norm(x1 + xthree, axis=1)
    #
    #   # step 4
    #   xfour = dt * (v1 + vthree)
    #   vfour = dt * dv_dt(x1 + xthree, rthree)
    #
    #   # combine everything
    #   xplus1 = x1 + 1 / 6 * (xone + 2 * xtwo + 2 * xthree + xfour)
    #   vplus1 = v1 + 1 / 6 * (vone + 2 * vtwo + 2 * vthree + vfour)
    #
    #   x1 = xplus1
    #   v1 = vplus1
    #
    # backwards_x = xplus1.copy()

    # x_interp = forwards_x + (backwards_x - forwards_x) * (taim - t0)/(t1 - t0)

    x_interp = forwards_x

    # if t1 == t0:
    #   return(forwards_x)
    #
    # forwards_r  = np.linalg.norm(forwards_x, axis=1)
    # backwards_r = np.linalg.norm(backwards_x, axis=1)
    #
    # u_interp = forwards_x + (backwards_x - forwards_x) * (taim - t0)/(t1 - t0)
    # r_interp = forwards_r + (backwards_r - forwards_r) * (taim - t0)/(t1 - t0)
    #
    # x_interp = u_interp * (r_interp / np.linalg.norm(u_interp, axis=1))[:, np.newaxis]

    return (x_interp)


def spline_hernquist_interpolate(x0, v0, x1, v1, t0, t1, taim, GM, a_h, com=np.zeros(3), ten=10):
    '''Wiki https://en.wikipedia.org/wiki/Spline_(mathematics) General Expression For a C2 Interpolating Cubic Spline
  '''
    # x0 = x0 - com

    x = taim - t0
    h_i = t1 - t0

    t_im1 = t0
    t_i = t1

    f_t_im1 = x0
    f_t_i = x1

    # hernquist acceleration #- gradient of potential
    dv_dt = lambda x, r: - GM / (r[:, np.newaxis] + a_h) ** 2 * (x / r[:, np.newaxis]) * PC_ON_M ** 2 / GYR_ON_S ** 2

    r_im1 = np.linalg.norm(f_t_im1, axis=1)
    r_i = np.linalg.norm(f_t_i, axis=1)

    z_im1 = dv_dt(f_t_im1, r_im1)
    z_i = dv_dt(f_t_i, r_i)

    S_i_x = (z_i * (x - t_im1) ** 3 / (6 * h_i) +
             z_im1 * (t_i - x) ** 3 / (6 * h_i) +
             (f_t_i / h_i - z_i * h_i / 6) * (x - t_im1) +
             (f_t_im1 / h_i - z_im1 * h_i / 6) * (t_i - x))

    return (S_i_x)


# from scipy.interpolate import CubicHermiteSpline
# def spline_hernquist_interpolate(x0, v0, x1, v1, t0, t1, taim, GM, a_h, com=np.zeros(3), ten=10):
#   '''Wiki https://en.wikipedia.org/wiki/Spline_(mathematics) General Expression For a C2 Interpolating Cubic Spline
#   '''
#   # x0 = x0 - com
#
#   spline = CubicHermiteSpline(x=np.array((t0, t1)), y=np.array((x0, x1)), dydx=np.array((v0, v1)))
#
#   out = spline(taim - t0)
#
#   return(out)


def plot_scaling_relations(save=False, name=''):
    fig = plt.figure()
    # fig.set_size_inches(22.8, 17.4, forward=True)
    fig.set_size_inches(22.8*0.9, 17.4*0.9, forward=True)
    spec = fig.add_gridspec(nrows=6, height_ratios=[4,2, 4.9,2, 8,2], ncols=3, width_ratios=[1,1,1],
    # spec = fig.add_gridspec(nrows=6, height_ratios=[4, 2, 4.3, 2, 8, 2], ncols=3, width_ratios=[1, 1, 1],
                            wspace=0.02, hspace=0.02)

    axs = []
    for row in range(6):
        axs.append([])
        for col in range(3):
            axs[row].append(fig.add_subplot(spec[row, col]))

    # cax = fig.add_subplot(spec[:, 3])
    # cmap = 'twilight'
    # cmap = 'magma'
    cmap = 'turbo'

    for i, a in enumerate(axs):
        for j, b in enumerate(a):
            if i % 2 == 0:
                b.set_aspect('equal')

            b.set_xticks([8.5, 9, 9.5, 10, 10.5, 11])
            if j != 0:
                b.set_yticklabels([])
            if i != 5:
                b.set_xticklabels([])

            if i == 0:
                b.set(ylim=[0.1, 1.4])
            elif i == 1:
                b.set(ylim=[-0.8, 0.8])
            # elif i == 2:
            #     b.set(ylim=[1.4, 2.8])
            # elif i == 3:
            #     b.set(ylim=[-0.15, 0.15])
            elif i == 2:
              b.set(ylim=[1.2, 2.8])
            elif i == 3:
              b.set(ylim=[-0.9, 0.9])
            elif i == 4:
                b.set(ylim=[1.5, 4.1])
            elif i == 5:
                b.set(ylim=[-0.45, 0.45])

            b.set(xlim=[8.2, 11.5])

    fig.subplots_adjust(hspace=0.02, wspace=0.02)

    M200 = np.array([50, 100, 200, 400]) ** 3 / (10 * GRAV_CONST * GALIC_HUBBLE_CONST)
    f_bary = 0.01
    M_star = f_bary * M200

    ms = np.log10(M_star) + 10

    # Bloom et al. 2017: SMAI
    slope = 0.31
    intercept = -0.93
    vs = slope * ms + intercept

    # #Hardwick et al. 2022: SMAI
    # slope = 1 / 4.05
    # intercept = -1.27 / 4.05
    # vs = slope * ms + intercept

    # Sugessed by Mike
    alpha = 0.67
    j0_d = 3.17
    j0_b = 2.25
    js_d = j0_d + alpha * (ms - 10.5)
    js_b = j0_b + alpha * (ms - 10.5)

    # hernquist
    f_m = 0.01  # stellar mass fraciton
    m_h = 10 ** ms / f_m  # halo mass
    H = 0.1
    V_200 = (10 * H * m_h * GRAV_CONST * 1e-10) ** (1 / 3)
    R_200 = V_200 * (10 * H)
    c = 10
    a_h = (R_200 / c) * np.sqrt(2 * (np.log(1 + c) - c / (1 + c)))
    vmax = np.sqrt(GRAV_CONST * m_h * (1 - f_m) * 1e-10 / (4 * a_h))

    r_disk = 0.12 * a_h
    stellar_half_mass = 1.687 * r_disk

    # actual circular velocity
    # v_c_exp = [get_exponential_disk_velocity(rs, m * 1e-10 * f_m, r, save_name_append='diff')[0]
    #            for (rs, m, r) in zip(stellar_half_mass, m_h, r_disk)]
    # mass inside sphere for exponential
    v_c_exp = [np.sqrt(GRAV_CONST * m * 1e-10 * f_m * (1 - np.exp(- rs / r) * (1 + rs / r)) / rs)
               for (rs, m, r) in zip(stellar_half_mass, m_h, r_disk)]

    stellar_half_mass = np.repeat(stellar_half_mass, 2).reshape(len(stellar_half_mass), 2)

    sigma_1d = [get_analytic_hernquist_dispersion(rs, v, a, save_name_append='diff')[0]
                for (rs, v, a) in zip(stellar_half_mass, vmax, a_h)]
    v_c_hern = [get_analytic_hernquist_circular_velocity(rs, v, a, save_name_append='diff')[0]
                for (rs, v, a) in zip(stellar_half_mass, vmax, a_h)]

    v_c_total = np.sqrt(np.array(v_c_hern) ** 2 + np.array(v_c_exp) ** 2)

    stellar_half_mass = 1.687 * r_disk

    # Hernqusit projcted half mass radius
    r_half_hern = 1.81527 * a_h  # Hernquist 1990

    integrand = lambda R, M_dm, a_h, M_star, R_d: R * R * np.exp(-R / R_d) * np.sqrt(
        GRAV_CONST * 1e-10 * (M_dm * R / (R + a_h) ** 2 + M_star * R ** 2 / (2 * R_d ** 3) * (
                iv(0, R / (2 * R_d)) * kn(0, R / (2 * R_d)) - iv(1, R / (2 * R_d)) * kn(1, R / (2 * R_d)))))
    # integrand =lambda R, M_dm, a_h, M_star, R_d : R*R * np.exp(-R / R_d) * np.sqrt(
    #   GRAV_CONST * 1e-10 * (M_dm * R / (R + a_h)**2))
    # integrand =lambda R, M_dm, a_h, M_star, R_d : R*R * np.exp(-R / R_d) * np.sqrt

    #   GRAV_CONST * 1e-10 * (M_star * R**2 / (2 * R_d**3) * (
    #           iv(0,R/(2*R_d))*kn(0,R/(2*R_d)) - iv(1,R/(2*R_d))*kn(1,R/(2*R_d)))))

    j_star_0 = quad(integrand, a_h[0] * 1e-6, a_h[0] * 1e2, args=(m_h[0], a_h[0], m_h[0] * f_m, r_disk[0]))[0] / r_disk[0] ** 2
    j_star_1 = quad(integrand, a_h[1] * 1e-6, a_h[1] * 1e2, args=(m_h[1], a_h[1], m_h[1] * f_m, r_disk[1]))[0] / r_disk[1] ** 2
    j_star_2 = quad(integrand, a_h[2] * 1e-6, a_h[2] * 1e2, args=(m_h[2], a_h[2], m_h[2] * f_m, r_disk[2]))[0] / r_disk[2] ** 2
    j_star_3 = quad(integrand, a_h[3] * 1e-6, a_h[3] * 1e2, args=(m_h[3], a_h[3], m_h[3] * f_m, r_disk[3]))[0] / r_disk[3] ** 2

    j_star_theory = np.array([j_star_0, j_star_1, j_star_2, j_star_3])

    times_to_plot = np.array([0, 2, 4, 6, 8, T_TOT])  # np.linspace(0, T_TOT, 5)

    # color_calc = lambda l: matplotlib.cm.get_cmap(cmap)((l + 2) / (len(times_to_plot) + 3))
    # color_calc = lambda l: matplotlib.cm.get_cmap(cmap)((l + 0) / (len(times_to_plot) -1 + 0.8))
    color_calc = lambda l: matplotlib.cm.get_cmap(cmap)((l + 0.5) / (len(times_to_plot) -1 + 1))

    for k in range(3):

        if k == 0:
            galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_6p0_V200-50kmps',
                                'mu_5/fdisk_0p01_lgMdm_6p0_V200-100kmps',
                                'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps']
            snaps_list = [400, 101, 101]

        elif k == 1:
            galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_7p0_V200-100kmps',
                                'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                                'mu_5/fdisk_0p01_lgMdm_7p0_V200-400kmps']
            snaps_list = [400, 101, 101]

        elif k == 2:
            galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                                'mu_5/fdisk_0p01_lgMdm_8p0_V200-400kmps']
            snaps_list = [400, 101]

        for galaxy_name, snaps in zip(galaxy_name_list, snaps_list):
            kwargs = get_name_kwargs(galaxy_name)

            if kwargs['V200'] == 50:
                ii = 0
            elif kwargs['V200'] == 100:
                ii = 1
            elif kwargs['V200'] == 200:
                ii = 2
            elif kwargs['V200'] == 400:
                ii = 3

            M200 = kwargs['V200'] ** 3 / (10 * GRAV_CONST * GALIC_HUBBLE_CONST)
            f_bary = 0.01
            M_star = f_bary * M200

            snaps_to_plot = np.floor(times_to_plot / T_TOT * snaps)
            snaps_to_plot = np.array(snaps_to_plot, dtype=int)
            snaps_to_plot[-1] -= 1

            bin_edges = get_bin_edges('diff', galaxy_name=galaxy_name)
            
            R_half = get_R_half(galaxy_name, snaps)
            v_circ = get_vc_from_M_at_R_half(galaxy_name, 'diff', snaps)
            (_, v_phi, _, _, _) = get_dispersions(galaxy_name, 'diff', snaps, bin_edges)
            j_star = get_total_angular_momentum(galaxy_name, 'diff', snaps)

            _index = 1

            print(galaxy_name)
            print('j[0]', j_star[0])
            print('j[-1]', j_star[-1])
            print('1 - j[-1]/j[0]', 1 - j_star[-1]/j_star[0])
            print()

            # qs = np.linspace(0,1,11)[1:-1]
            # half_i = 4
            #
            # (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
            #  MassStar, PosStars, VelStars, IDStars, PotStars
            #  ) = load_nbody.load_and_align(galaxy_name, snap='{0:03d}'.format(int(0)))
            # R = np.sqrt(PosStars[:, 0] ** 2 + PosStars[:, 1] ** 2)
            # R_zeros = np.quantile(R, qs)
            #
            # (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
            #  MassStar, PosStars, VelStars, IDStars, PotStars
            #  ) = load_nbody.load_and_align(galaxy_name, snap='{0:03d}'.format(int(snaps-2)))
            # R = np.sqrt(PosStars[:, 0] ** 2 + PosStars[:, 1] ** 2)
            # R_minus1s = np.quantile(R, qs)
            #
            # # print('R(0)', R_zeros)
            # # print('R(-1)', R_minus1s)
            # # print('R(-1)/R(0)', R_minus1s/R_zeros)
            # print('R(-1)/R(0) / R_half(-1)/R_half(0)', (R_minus1s/R_zeros) / (R_minus1s[half_i]/R_zeros[half_i]))
            # print()

            # (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi) = get_dispersions(galaxy_name, 'diff', snaps, bin_edges)
            # sigma_1D = np.sqrt((sigma_z ** 2 + sigma_R ** 2 + sigma_phi ** 2) / 3)

            for l, snap in enumerate(snaps_to_plot):

                color = color_calc(l)

                marker = 'o'
                marker2 = 's'
                markersize = 12 ** 2  # 22**2
                low_res_bulge_time = 4.5  # Gyr
                mew=3
                if snaps == 400 and snap > low_res_bulge_time / T_TOT * snaps:
                    fc = color
                else:
                    fc = ((1, 1, 1, 0),)

                kwargs = {'ec': color, 'linewidths': mew, 'fc': fc, 'marker': marker, 's': markersize}
                kwargs2 = {'ec': color, 'linewidths': mew, 'fc': fc, 'marker': marker2, 's': markersize}

                ofst = 0.08 #0
                axs[0][k].scatter(np.log10(M_star) + 10, np.log10(R_half[snap]), **kwargs)
                axs[2][k].scatter(np.log10(M_star) + 10 - ofst, np.log10(v_circ[snap]), **kwargs)
                axs[2][k].scatter(np.log10(M_star) + 10 + ofst, np.log10(v_phi[snap, _index]), **kwargs2)
                axs[4][k].scatter(np.log10(M_star) + 10, np.log10(j_star[snap]), **kwargs)

                axs[1][k].scatter(np.log10(M_star) + 10, np.log(R_half[snap] / stellar_half_mass[ii]), **kwargs)
                axs[3][k].scatter(np.log10(M_star) + 10 - ofst, np.log(v_circ[snap] / v_c_total[ii]), **kwargs)
                axs[3][k].scatter(np.log10(M_star) + 10 + ofst, np.log(v_phi[snap, _index] / v_c_total[ii]), **kwargs2)
                axs[5][k].scatter(np.log10(M_star) + 10, np.log(j_star[snap] / j_star_theory[ii]), **kwargs)

    axs[0][0].legend([(lines.Line2D([0, 1], [0, 1], mec=color_calc(len(times_to_plot) - 1), mfc=(1, 1, 1, 0), mew=mew,
                                    ms=np.sqrt(markersize), marker=marker, ls='')),
                      (lines.Line2D([0, 1], [0, 1], mec=color_calc(len(times_to_plot) - 1),
                                    mfc=color_calc(len(times_to_plot) - 1), mew=mew,
                                    ms=np.sqrt(markersize), marker=marker, ls=''))
                      ], ['Disc dominated', 'Spheroid dominated'],
                     handlelength=1, handletextpad=0.6, frameon=False, labelspacing=0.3, loc='upper left')
    axs[2][0].legend([(lines.Line2D([0, 1], [0, 1], mec=color_calc(len(times_to_plot) - 1), mfc=(1, 1, 1, 0), mew=mew,
                                    ms=np.sqrt(markersize), marker=marker, ls='')),
                      (lines.Line2D([0, 1], [0, 1], mec=color_calc(len(times_to_plot) - 1), mfc=(1, 1, 1, 0), mew=mew,
                                    ms=np.sqrt(markersize), marker=marker2, ls=''))
                      ], [r'$V_{c}(R_{1/2})$', r'$\overline{v}_{\phi} (R_{1/2, 0})$'],
                     handlelength=1, handletextpad=0.6, frameon=False, labelspacing=0.3, loc='upper left')
    # axs[4][0].legend([(lines.Line2D([0, 1], [0, 1], mec=color_calc(l),  mfc=(1, 1, 1, 0), mew=mew,
    #                                 ms=np.sqrt(markersize), marker=marker, ls=''))
    #                   for l in range(len(times_to_plot))],
    #                  [r'$t =$' + str(round(time, 1)) + ' Gyr' for time in times_to_plot],
    #                  handlelength=1, handletextpad=0.6, frameon=False, labelspacing=0.3)
    axs[4][0].legend([(lines.Line2D([0, 1], [0, 1], marker='', ls='', color=color_calc(l)))
                      for l in range(len(times_to_plot))],
                     [r'$t =$' + str(round(time, 1)) + ' Gyr' for time in times_to_plot],
                     handlelength=0, handletextpad=0, frameon=False, labelspacing=0.3,
                     labelcolor='linecolor')

    ms = np.linspace(8, 12)

    # Bloom et al. 2017: SMAI
    slope = 0.31
    intercept = -0.93
    vs = slope * ms + intercept

    # #Hardwick et al. 2022: SMAI
    # slope = 1 / 4.05
    # intercept = -1.27 / 4.05
    # vs = slope * ms + intercept

    # Sugessed by Mike
    alpha = 0.67
    j0_d = 3.17
    j0_b = 2.25
    js_d = j0_d + alpha * (ms - 10.5)
    js_b = j0_b + alpha * (ms - 10.5)

    # hernquist
    f_m = 0.01  # stellar mass fraciton
    m_h = 10 ** ms / f_m  # halo mass
    H = 0.1
    V_200 = (10 * H * m_h * GRAV_CONST * 1e-10) ** (1 / 3)
    R_200 = V_200 / (10 * H)
    c = 10
    a_h = (R_200 / c) * np.sqrt(2 * (np.log(1 + c) - c / (1 + c)))
    vmax = np.sqrt(GRAV_CONST * m_h * 1e-10 / (4 * a_h))

    r_disk = 0.12 * a_h
    stellar_half_mass = 1.687 * r_disk
    stellar_half_mass = np.repeat(stellar_half_mass, 2).reshape(len(stellar_half_mass), 2)

    sigma_1d = [get_analytic_hernquist_dispersion(rs, v, a, save_name_append='diff')[0]
                for (rs, v, a) in zip(stellar_half_mass, vmax, a_h)]
    v_c_hern = [get_analytic_hernquist_circular_velocity(rs, v, a, save_name_append='diff')[0]
                for (rs, v, a) in zip(stellar_half_mass, vmax, a_h)]
    v_c_exp = [get_exponential_disk_velocity(rs, m * 1e-10 * f_m, r, save_name_append='diff')[0]
               for (rs, m, r) in zip(stellar_half_mass, m_h, r_disk)]

    v_c_total = np.sqrt(np.array(v_c_hern) ** 2 + np.array(v_c_exp) ** 2)

    # Hernqusit projcted half mass radius
    r_half_hern = 1.81527 * a_h  # Hernquist 1990

    integrand = lambda R, M_dm, a_h, M_star, R_d: R * R * np.exp(-R / R_d) * np.sqrt(
        GRAV_CONST * 1e-10 * (M_dm * R / (R + a_h) ** 2 + M_star * R ** 2 / (2 * R_d ** 3) * (
                iv(0, R / (2 * R_d)) * kn(0, R / (2 * R_d)) - iv(1, R / (2 * R_d)) * kn(1, R / (2 * R_d)))))
    # integrand =lambda R, M_dm, a_h, M_star, R_d : R*R * np.exp(-R / R_d) * np.sqrt(
    #   GRAV_CONST * 1e-10 * (M_dm * R / (R + a_h)**2))
    # integrand =lambda R, M_dm, a_h, M_star, R_d : R*R * np.exp(-R / R_d) * np.sqrt(
    #   GRAV_CONST * 1e-10 * (M_star * R**2 / (2 * R_d**3) * (
    #           iv(0,R/(2*R_d))*kn(0,R/(2*R_d)) - iv(1,R/(2*R_d))*kn(1,R/(2*R_d)))))

    zero = 0
    j_star_small = \
    quad(integrand, a_h[zero] * 1e-6, a_h[zero] * 1e2, args=(m_h[zero], a_h[zero], m_h[zero] * f_m, r_disk[zero]))[0] / \
    r_disk[zero] ** 2
    zalf = len(ms) // 2
    j_star_half = \
    quad(integrand, a_h[zalf] * 1e-6, a_h[zalf] * 1e2, args=(m_h[zalf], a_h[zalf], m_h[zalf] * f_m, r_disk[zalf]))[0] / \
    r_disk[zalf] ** 2
    zend = len(ms) - 1
    j_star_large = \
    quad(integrand, a_h[zend] * 1e-6, a_h[zend] * 1e2, args=(m_h[zend], a_h[zend], m_h[zend] * f_m, r_disk[zend]))[0] / \
    r_disk[zend] ** 2

    # print(V_200)
    # print(a_h)

    # TODO can actually work out expected maximum stellar angular momentum

    N_200s = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8])
    N_200_labels = [r'$10^0$', r'$10^1$', r'$10^2$', r'$10^3$', r'$10^4$', r'$10^5$', r'$10^6$', r'$10^7$', r'$10^8$']

    for i, m_dm in enumerate([6, 7, 8]):

        for n200, label in zip(N_200s, N_200_labels):
            x = n200 + m_dm + np.log10(f_m)
            if 8.2 < x < 11.5:
                axs[4][i].text(x, 1.6, label, color='k')
                for k in range(6):
                    if i == 0 and (n200 == 5 or n200 == 6) and k==0:
                        axs[k][i].axvline(x, 0, 0.55, ls=':', c='grey', alpha=0.6)
                    elif i == 0 and n200 == 5 and k == 2:
                        axs[k][i].axvline(x, 0, 0.6, ls=':', c='grey', alpha=0.6)
                    elif i == 0 and n200 == 5 and k == 4:
                        axs[k][i].axvline(x, 0, 0.43, ls=':', c='grey', alpha=0.6)
                    else:
                        axs[k][i].axvline(x, 0, 1, ls=':', c='grey', alpha=0.6)

    axs[4][0].text(9, 1.6, r'N$_{\mathrm{DM}} = $', color='k', ha='right')

    # print(sigma_1d)
    # print(v_c_hern)
    # print(v_c_exp)

    # print(np.diff(np.log10(stellar_half_mass[:,0])) / np.diff(ms))
    # print(np.diff(np.log10(v_c_total)) / np.diff(ms))
    # print(np.diff(np.log10([j_star_small, j_star_half, j_star_large])) / np.diff([ms[zero], ms[zalf], ms[zend]]))

    # print(np.diff(np.log10(sigma_1d)) / np.diff(ms))

    for i in range(3):
        # axs[0][i].errorbar(ms, np.log10(r_half_hern), ls=':', c='grey')
        axs[0][i].errorbar(ms, np.log10(stellar_half_mass[:, 0]), ls='--', c='grey')
        # axs[1][i].errorbar(ms, np.log10(v_c_hern), ls=':', c='grey')
        # axs[2][i].errorbar(ms, np.log10(v_c_hern), ls=':', c='grey')
        # axs[1][i].errorbar(ms, np.log10(v_c_exp), ls='-.', c='grey')
        # axs[2][i].errorbar(ms, np.log10(v_c_exp), ls='-.', c='grey')
        axs[2][i].errorbar(ms, np.log10(v_c_total), ls='--', c='grey')
        # axs[2][i].errorbar(ms, np.log10(v_c_total), ls='--', c='grey')
        # axs[3][i].errorbar(ms, np.log10(sigma_1d), ls='--', c='grey')

        axs[4][i].errorbar([ms[zero], ms[zalf], ms[zend]], np.log10([j_star_small, j_star_half, j_star_large]),
                           ls='--', c='grey')

        axs[1][i].errorbar([8.1, 11.5], [0, 0], ls='--', c='grey', alpha=0.6)
        axs[3][i].errorbar([8.1, 11.5], [0, 0], ls='--', c='grey', alpha=0.6)
        axs[5][i].errorbar([8.1, 11.5], [0, 0], ls='--', c='grey', alpha=0.6)

        fall_color = 'green'
        # axs[1][i].errorbar(ms, vs, ls=':', c='purple') #need to check this is the same measurement
        axs[4][i].errorbar(ms, js_d, ls=':', c=fall_color)
        axs[4][i].errorbar(ms, js_b, ls=':', c=fall_color)

    axs[0][0].set_ylabel(r'$\,\,\,\,\,\log (R_{1/2} / $kpc)')
    axs[1][0].set_ylabel(r'$\,\Delta \mathrm{ln } \, R_{1/2}$')
    # axs[2][0].set_ylabel(r'$\log (v_{c,1/2} / $kms$^{-1})$')
    # axs[3][0].set_ylabel(r'$\Delta \mathrm{ln } v_{c,1/2}$')
    axs[2][0].set_ylabel(r'$\log (v_{\mathrm{rot}} / $km s$^{-1})$')
    axs[3][0].set_ylabel(r'$\Delta \mathrm{ln } \, v_{\mathrm{rot}}$')
    axs[4][0].set_ylabel(r'$\log (j_\star / $kpc km s$^{-1})$')
    axs[5][0].set_ylabel(r'$\Delta \mathrm{ln } \, j_\star$')

    axs[5][0].set_xlabel(r'$\log( \mathrm{M}_\star / \mathrm{M}_\odot )$')
    axs[5][1].set_xlabel(r'$\log( \mathrm{M}_\star / \mathrm{M}_\odot )$')
    axs[5][2].set_xlabel(r'$\log( \mathrm{M}_\star / \mathrm{M}_\odot )$')

    axs[0][0].set_title(r'$\log( m_\mathrm{DM} / \mathrm{M}_\odot ) = 6$')
    axs[0][1].set_title(r'$\log( m_\mathrm{DM} / \mathrm{M}_\odot ) = 7$')
    axs[0][2].set_title(r'$\log( m_\mathrm{DM} / \mathrm{M}_\odot ) = 8$')

    axs[0][2].text(8.5, 0.345, r'$R_{1/2} \propto $M$^{1/3}_\star$ (ICs)',
                   rotation=np.arctan(1 / 3) * 180 / np.pi)  # angle0)
    axs[2][2].text(8.5, 1.795, r'$V_c \propto $M$^{1/3}_\star$ (ICs)',
                   rotation=np.arctan(1 / 3) * 180 / np.pi)  # angle1)
    axs[4][2].text(8.5, 2.22, r'$j_\star \propto $M$^{2/3}_\star$ (ICs)',
                   rotation=np.arctan(2 / 3) * 180 / np.pi)  # angle2)
    axs[4][2].text(8.90, 2.202, r'FR18 disc', rotation=np.arctan(2 / 3) * 180 / np.pi, c=fall_color)  # angle2)
    axs[4][2].text(10.3, 2.22, r'FR18 bulge', rotation=np.arctan(2 / 3) * 180 / np.pi, c=fall_color)  # angle2)

    # max_c = 978
    # two = 30
    #
    # heights = np.reshape(np.tile(np.linspace(1-1/(len(snaps_to_plot)+3), 1/(len(snaps_to_plot)+3), max_c), two), (two,max_c)).T
    # cax.imshow(heights, cmap=cmap, vmin=0, vmax=1)
    #
    # cax.tick_params(axis='y', which='both', labelleft=False, labelright=True)
    # cax.yaxis.set_label_position('right')
    # cax.set_xticks([])
    # cax.set_yticks([max_c-0, max_c-200, max_c-400, max_c-600, max_c-800])
    # cax.set_yticklabels([0, 2, 4, 6, 8])
    #
    # cax.set_title('')
    # cax.set_xlabel('')
    # cax.set_ylabel(r'$t$ [Gyr]')

    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return


def talk_plot_scaling_relations(save=False, name=''):
    fig = plt.figure()
    fig.set_size_inches(7, 7.4, forward=True)
    spec = fig.add_gridspec(nrows=2, height_ratios=[4, 1], ncols=1, width_ratios=[1],
    # spec = fig.add_gridspec(nrows=6, height_ratios=[4, 2, 4.3, 2, 8, 2], ncols=3, width_ratios=[1, 1, 1],
                            wspace=0.02, hspace=0.00)

    axs = []
    for row in range(2):
        axs.append([])
        for col in range(1):
            axs[row].append(fig.add_subplot(spec[row, col]))

    # cax = fig.add_subplot(spec[:, 3])
    # cmap = 'twilight'
    # cmap = 'magma'
    cmap = 'turbo'

    for i, a in enumerate(axs):
        for j, b in enumerate(a):
            if i % 2 == 0:
                b.set_aspect('equal')

            # b.set_xticks([8.5, 9, 9.5, 10, 10.5, 11])

            if i == 0:
                b.set(ylim=[1.5, 3.6])
            if i == 1:
                b.set(ylim=[-0.4,0.4])

            b.set(xlim=[8.2, 10.7])

    M200 = np.array([50, 100, 200, 400]) ** 3 / (10 * GRAV_CONST * GALIC_HUBBLE_CONST)
    f_bary = 0.01
    M_star = f_bary * M200

    ms = np.log10(M_star) + 10

    # Bloom et al. 2017: SMAI
    slope = 0.31
    intercept = -0.93
    vs = slope * ms + intercept

    # #Hardwick et al. 2022: SMAI
    # slope = 1 / 4.05
    # intercept = -1.27 / 4.05
    # vs = slope * ms + intercept

    # Sugessed by Mike
    alpha = 0.67
    j0_d = 3.17
    j0_b = 2.25
    js_d = j0_d + alpha * (ms - 10.5)
    js_b = j0_b + alpha * (ms - 10.5)

    # hernquist
    f_m = 0.01  # stellar mass fraciton
    m_h = 10 ** ms / f_m  # halo mass
    H = 0.1
    V_200 = (10 * H * m_h * GRAV_CONST * 1e-10) ** (1 / 3)
    R_200 = V_200 * (10 * H)
    c = 10
    a_h = (R_200 / c) * np.sqrt(2 * (np.log(1 + c) - c / (1 + c)))
    vmax = np.sqrt(GRAV_CONST * m_h * (1 - f_m) * 1e-10 / (4 * a_h))

    r_disk = 0.12 * a_h
    stellar_half_mass = 1.687 * r_disk

    # actual circular velocity
    # v_c_exp = [get_exponential_disk_velocity(rs, m * 1e-10 * f_m, r, save_name_append='diff')[0]
    #            for (rs, m, r) in zip(stellar_half_mass, m_h, r_disk)]
    # mass inside sphere for exponential
    v_c_exp = [np.sqrt(GRAV_CONST * m * 1e-10 * f_m * (1 - np.exp(- rs / r) * (1 + rs / r)) / rs)
               for (rs, m, r) in zip(stellar_half_mass, m_h, r_disk)]

    stellar_half_mass = np.repeat(stellar_half_mass, 2).reshape(len(stellar_half_mass), 2)

    sigma_1d = [get_analytic_hernquist_dispersion(rs, v, a, save_name_append='diff')[0]
                for (rs, v, a) in zip(stellar_half_mass, vmax, a_h)]
    v_c_hern = [get_analytic_hernquist_circular_velocity(rs, v, a, save_name_append='diff')[0]
                for (rs, v, a) in zip(stellar_half_mass, vmax, a_h)]

    v_c_total = np.sqrt(np.array(v_c_hern) ** 2 + np.array(v_c_exp) ** 2)

    stellar_half_mass = 1.687 * r_disk

    # Hernqusit projcted half mass radius
    r_half_hern = 1.81527 * a_h  # Hernquist 1990

    integrand = lambda R, M_dm, a_h, M_star, R_d: R * R * np.exp(-R / R_d) * np.sqrt(
        GRAV_CONST * 1e-10 * (M_dm * R / (R + a_h) ** 2 + M_star * R ** 2 / (2 * R_d ** 3) * (
                iv(0, R / (2 * R_d)) * kn(0, R / (2 * R_d)) - iv(1, R / (2 * R_d)) * kn(1, R / (2 * R_d)))))
    # integrand =lambda R, M_dm, a_h, M_star, R_d : R*R * np.exp(-R / R_d) * np.sqrt(
    #   GRAV_CONST * 1e-10 * (M_dm * R / (R + a_h)**2))
    # integrand =lambda R, M_dm, a_h, M_star, R_d : R*R * np.exp(-R / R_d) * np.sqrt

    #   GRAV_CONST * 1e-10 * (M_star * R**2 / (2 * R_d**3) * (
    #           iv(0,R/(2*R_d))*kn(0,R/(2*R_d)) - iv(1,R/(2*R_d))*kn(1,R/(2*R_d)))))

    j_star_0 = quad(integrand, a_h[0] * 1e-6, a_h[0] * 1e2, args=(m_h[0], a_h[0], m_h[0] * f_m, r_disk[0]))[0] / r_disk[0] ** 2
    j_star_1 = quad(integrand, a_h[1] * 1e-6, a_h[1] * 1e2, args=(m_h[1], a_h[1], m_h[1] * f_m, r_disk[1]))[0] / r_disk[1] ** 2
    j_star_2 = quad(integrand, a_h[2] * 1e-6, a_h[2] * 1e2, args=(m_h[2], a_h[2], m_h[2] * f_m, r_disk[2]))[0] / r_disk[2] ** 2
    j_star_3 = quad(integrand, a_h[3] * 1e-6, a_h[3] * 1e2, args=(m_h[3], a_h[3], m_h[3] * f_m, r_disk[3]))[0] / r_disk[3] ** 2

    j_star_theory = np.array([j_star_0, j_star_1, j_star_2, j_star_3])

    times_to_plot = np.array([0, 2, 4, 6, 8, T_TOT])  # np.linspace(0, T_TOT, 5)

    # color_calc = lambda l: matplotlib.cm.get_cmap(cmap)((l + 2) / (len(times_to_plot) + 3))
    # color_calc = lambda l: matplotlib.cm.get_cmap(cmap)((l + 0) / (len(times_to_plot) -1 + 0.8))
    color_calc = lambda l: matplotlib.cm.get_cmap(cmap)((l + 0.5) / (len(times_to_plot) -1 + 1))

    for k in range(1):

        if k == 0:
            galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_6p0_V200-50kmps',
                                'mu_5/fdisk_0p01_lgMdm_6p0_V200-100kmps',
                                'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps']
            snaps_list = [400, 101, 101]

        elif k == 1:
            galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_7p0_V200-100kmps',
                                'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                                'mu_5/fdisk_0p01_lgMdm_7p0_V200-400kmps']
            snaps_list = [400, 101, 101]

        elif k == 2:
            galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                                'mu_5/fdisk_0p01_lgMdm_8p0_V200-400kmps']
            snaps_list = [400, 101]

        for galaxy_name, snaps in zip(galaxy_name_list, snaps_list):
            kwargs = get_name_kwargs(galaxy_name)

            if kwargs['V200'] == 50:
                ii = 0
            elif kwargs['V200'] == 100:
                ii = 1
            elif kwargs['V200'] == 200:
                ii = 2
            elif kwargs['V200'] == 400:
                ii = 3

            M200 = kwargs['V200'] ** 3 / (10 * GRAV_CONST * GALIC_HUBBLE_CONST)
            f_bary = 0.01
            M_star = f_bary * M200

            snaps_to_plot = np.floor(times_to_plot / T_TOT * snaps)
            snaps_to_plot = np.array(snaps_to_plot, dtype=int)
            snaps_to_plot[-1] -= 1

            bin_edges = get_bin_edges('diff', galaxy_name=galaxy_name)

            R_half = get_R_half(galaxy_name, 'diff', snaps)
            v_circ = get_vc_from_M_at_R_half(galaxy_name, 'diff', snaps)
            (_, v_phi, _, _, _) = get_dispersions(galaxy_name, 'diff', snaps, bin_edges)
            j_star = get_total_angular_momentum(galaxy_name, 'diff', snaps)

            _index = 1

            for l, snap in enumerate(snaps_to_plot):

                color = color_calc(l)

                marker = 'o'
                marker2 = 's'
                markersize = 12 ** 2  # 22**2
                low_res_bulge_time = 4.5  # Gyr
                mew=3
                if snaps == 400 and snap > low_res_bulge_time / T_TOT * snaps:
                    fc = ((1, 1, 1, 0),) #color
                else:
                    fc = ((1, 1, 1, 0),)

                kwargs = {'ec': color, 'linewidths': mew, 'fc': fc, 'marker': marker, 's': markersize}
                kwargs2 = {'ec': color, 'linewidths': mew, 'fc': fc, 'marker': marker2, 's': markersize}

                ofst = 0.07 #0
                axs[0][k].scatter(np.log10(M_star) + 10, np.log10(j_star[snap]), **kwargs)

                axs[1][k].scatter(np.log10(M_star) + 10, np.log(j_star[snap] / j_star_theory[ii]), **kwargs)

    # axs[0][0].legend([(lines.Line2D([0, 1], [0, 1], mec=color_calc(len(times_to_plot) - 1), mfc=(1, 1, 1, 0), mew=mew,
    #                                 ms=np.sqrt(markersize), marker=marker, ls='')),
    #                   (lines.Line2D([0, 1], [0, 1], mec=color_calc(len(times_to_plot) - 1),
    #                                 mfc=color_calc(len(times_to_plot) - 1), mew=mew,
    #                                 ms=np.sqrt(markersize), marker=marker, ls=''))
    #                   ], ['Disk Dominated', 'Spheroid Dominated'],
    #                  handlelength=1, handletextpad=0.6, frameon=False, labelspacing=0.8)
    # axs[2][0].legend([(lines.Line2D([0, 1], [0, 1], mec=color_calc(len(times_to_plot) - 1), mfc=(1, 1, 1, 0), mew=mew,
    #                                 ms=np.sqrt(markersize), marker=marker, ls='')),
    #                   (lines.Line2D([0, 1], [0, 1], mec=color_calc(len(times_to_plot) - 1), mfc=(1, 1, 1, 0), mew=mew,
    #                                 ms=np.sqrt(markersize), marker=marker2, ls=''))
    #                   ], [r'$V_{c}(R_{1/2})$', r'$\overline{v}_{\phi} (R_{1/2})$'],
    #                  handlelength=1, handletextpad=0.6, frameon=False, labelspacing=0.8, loc='upper left')
    axs[0][0].legend([(lines.Line2D([0, 1], [0, 1], mec=color_calc(l),  mfc=(1, 1, 1, 0), mew=mew,
                                    ms=np.sqrt(markersize), marker=marker, ls=''))
                      for l in range(len(times_to_plot))],
                     [r'$\Delta t =$' + str(round(time, 1)) + ' Gyr' for time in times_to_plot],
                     handlelength=1, handletextpad=0.6, frameon=False, labelspacing=0.15,
                     loc='upper left')

    ms = np.linspace(8, 12)

    # Bloom et al. 2017: SMAI
    slope = 0.31
    intercept = -0.93
    vs = slope * ms + intercept

    # #Hardwick et al. 2022: SMAI
    # slope = 1 / 4.05
    # intercept = -1.27 / 4.05
    # vs = slope * ms + intercept

    # Sugessed by Mike
    alpha = 0.67
    j0_d = 3.17
    j0_b = 2.25
    js_d = j0_d + alpha * (ms - 10.5)
    js_b = j0_b + alpha * (ms - 10.5)

    # hernquist
    f_m = 0.01  # stellar mass fraciton
    m_h = 10 ** ms / f_m  # halo mass
    H = 0.1
    V_200 = (10 * H * m_h * GRAV_CONST * 1e-10) ** (1 / 3)
    R_200 = V_200 / (10 * H)
    c = 10
    a_h = (R_200 / c) * np.sqrt(2 * (np.log(1 + c) - c / (1 + c)))
    vmax = np.sqrt(GRAV_CONST * m_h * 1e-10 / (4 * a_h))

    r_disk = 0.12 * a_h
    stellar_half_mass = 1.687 * r_disk
    stellar_half_mass = np.repeat(stellar_half_mass, 2).reshape(len(stellar_half_mass), 2)

    sigma_1d = [get_analytic_hernquist_dispersion(rs, v, a, save_name_append='diff')[0]
                for (rs, v, a) in zip(stellar_half_mass, vmax, a_h)]
    v_c_hern = [get_analytic_hernquist_circular_velocity(rs, v, a, save_name_append='diff')[0]
                for (rs, v, a) in zip(stellar_half_mass, vmax, a_h)]
    v_c_exp = [get_exponential_disk_velocity(rs, m * 1e-10 * f_m, r, save_name_append='diff')[0]
               for (rs, m, r) in zip(stellar_half_mass, m_h, r_disk)]

    v_c_total = np.sqrt(np.array(v_c_hern) ** 2 + np.array(v_c_exp) ** 2)

    r_half_hern = 1.81527 * a_h  # Hernquist 1990

    integrand = lambda R, M_dm, a_h, M_star, R_d: R * R * np.exp(-R / R_d) * np.sqrt(
        GRAV_CONST * 1e-10 * (M_dm * R / (R + a_h) ** 2 + M_star * R ** 2 / (2 * R_d ** 3) * (
                iv(0, R / (2 * R_d)) * kn(0, R / (2 * R_d)) - iv(1, R / (2 * R_d)) * kn(1, R / (2 * R_d)))))
    
    zero = 0
    j_star_small = \
    quad(integrand, a_h[zero] * 1e-6, a_h[zero] * 1e2, args=(m_h[zero], a_h[zero], m_h[zero] * f_m, r_disk[zero]))[0] / \
    r_disk[zero] ** 2
    zalf = len(ms) // 2
    j_star_half = \
    quad(integrand, a_h[zalf] * 1e-6, a_h[zalf] * 1e2, args=(m_h[zalf], a_h[zalf], m_h[zalf] * f_m, r_disk[zalf]))[0] / \
    r_disk[zalf] ** 2
    zend = len(ms) - 1
    j_star_large = \
    quad(integrand, a_h[zend] * 1e-6, a_h[zend] * 1e2, args=(m_h[zend], a_h[zend], m_h[zend] * f_m, r_disk[zend]))[0] / \
    r_disk[zend] ** 2

    # print(V_200)
    # print(a_h)

    # TODO can actually work out expected maximum stellar angular momentum

    N_200s = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8])
    N_200_labels = [r'$10^0$', r'$10^1$', r'$10^2$', r'$10^3$', r'$10^4$', r'$10^5$', r'$10^6$', r'$10^7$', r'$10^8$']

    for i, m_dm in enumerate([6]):

        for n200, label in zip(N_200s, N_200_labels):
            x = n200 + m_dm + np.log10(f_m)
            if 8.2 < x < 10.8:
                axs[0][i].text(x, 1.6, label, color='k')
                for k in range(2):
                    if i == 0 and n200 == 5 and k==0:
                        axs[k][i].axvline(x, 0, 0.48, ls=':', c='k')
                        # axs[k][i].axvline(x, 0.62, 1, ls=':', c='k')
                    elif i == 0 and n200 == 6 and k == 0:
                        axs[k][i].axvline(x, 0, 0.58, ls=':', c='k')
                        axs[k][i].axvline(x, 0.69, 1, ls=':', c='k')
                    else:
                        axs[k][i].axvline(x, 0, 1, ls=':', c='k')

    axs[0][0].text(9, 1.6, r'N$_{\mathrm{DM}} = $', color='k', ha='right')

    axs[1][0].errorbar([8.1, 11.5], [0, 0], ls='--', c='grey')

    for i in range(1):
        axs[0][i].errorbar([ms[zero], ms[zalf], ms[zend]], np.log10([j_star_small, j_star_half, j_star_large]), ls='--',
                           c='grey')

    axs[0][0].set_ylabel(r'$\log (j_\star / $ kpc km s$^{-1})$')
    axs[1][0].set_ylabel(r'$\Delta \mathrm{ln } \, j_\star$')

    axs[1][0].set_xlabel(r'$\log( M_\star / \mathrm{M}_\odot )$')

    axs[0][0].set_title(r'$\log( m_\mathrm{DM} / \mathrm{M}_\odot ) = 6$')

    axs[0][0].text(9.7, 2.65, r'$j_\star \propto M^{2/3}_\star$ (ICs)',
                   rotation=np.arctan(2 / 3) * 180 / np.pi)  # angle2)

    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return


def plot_poster_scaling_relations(save=False, name=''):
    fig = plt.figure()
    fig.set_size_inches(8.2, 7.35, forward=True)
    spec = fig.add_gridspec(nrows=2, height_ratios=[1.3,1.6], ncols=1, width_ratios=[1],
                            wspace=0.02, hspace=0.02)

    axs = []
    for row in range(2):
        axs.append([])
        for col in range(1):
            axs[row].append(fig.add_subplot(spec[row, col]))

    # cmap = 'twilight'
    # cmap = 'magma'
    cmap = 'turbo'

    for i, a in enumerate(axs):
        for j, b in enumerate(a):
            b.set_aspect('equal')

            b.set_xticks([8.5, 9, 9.5, 10, 10.5, 11])
            if j != 0: b.set_yticklabels([])
            if i != 1: b.set_xticklabels([])
            else: b.set_xticklabels([None, 9, None, 10, None, 11])

            if i == 0: b.set(ylim=[0.1, 1.4])
            elif i == 1: b.set(ylim=[1.2, 2.8])

            b.set(xlim=[8.2, 11.5])

    fig.subplots_adjust(hspace=0.02, wspace=0.02)

    times_to_plot = np.array([0, 2, 4, 6, 8, T_TOT])  # np.linspace(0, T_TOT, 5)

    # color_calc = lambda l: matplotlib.cm.get_cmap(cmap)((l + 2) / (len(times_to_plot) + 3))
    # color_calc = lambda l: matplotlib.cm.get_cmap(cmap)((l + 0) / (len(times_to_plot) -1 + 0.8))
    color_calc = lambda l: matplotlib.cm.get_cmap(cmap)((l + 0.5) / (len(times_to_plot) -1 + 1))

    galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_7p0_V200-100kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p0_V200-400kmps']
    snaps_list = [400, 101, 101]

    for galaxy_name, snaps in zip(galaxy_name_list, snaps_list):
        kwargs = get_name_kwargs(galaxy_name)

        M200 = kwargs['V200'] ** 3 / (10 * GRAV_CONST * GALIC_HUBBLE_CONST)
        f_bary = 0.01
        M_star = f_bary * M200

        snaps_to_plot = np.floor(times_to_plot / T_TOT * snaps)
        snaps_to_plot = np.array(snaps_to_plot, dtype=int)
        snaps_to_plot[-1] -= 1

        bin_edges = get_bin_edges('diff', galaxy_name=galaxy_name)

        R_half = get_R_half(galaxy_name, 'diff', snaps)
        v_circ = get_vc_from_M_at_R_half(galaxy_name, 'diff', snaps)
        (_, v_phi, _, _, _) = get_dispersions(galaxy_name, 'diff', snaps, bin_edges)

        _index = 1

        # (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi) = get_dispersions(galaxy_name, 'diff', snaps, bin_edges)
        # sigma_1D = np.sqrt((sigma_z ** 2 + sigma_R ** 2 + sigma_phi ** 2) / 3)

        for l, snap in enumerate(snaps_to_plot):

            color = color_calc(l)

            marker = 'o'
            marker2 = 's'
            markersize = 18**2 #12 ** 2  # 22**2
            low_res_bulge_time = 4.5  # Gyr
            mew=np.sqrt(markersize) / 3
            # if snaps == 400 and snap > low_res_bulge_time / T_TOT * snaps:
            #     fc = color
            # else:
            #     fc = ((1, 1, 1, 0),)
            fc = ((1, 1, 1, 0),)

            kwargs = {'ec': color, 'linewidths': mew, 'fc': fc, 'marker': marker, 's': markersize}
            kwargs2 = {'ec': color, 'linewidths': mew, 'fc': fc, 'marker': marker2, 's': markersize}

            ofst = np.sqrt(markersize) * 0.006
            axs[0][0].scatter(np.log10(M_star) + 10, np.log10(R_half[snap]), **kwargs)
            axs[1][0].scatter(np.log10(M_star) + 10 - ofst, np.log10(v_circ[snap]), **kwargs)
            axs[1][0].scatter(np.log10(M_star) + 10 + ofst, np.log10(v_phi[snap, _index]), **kwargs2)

    # axs[0][0].legend([(lines.Line2D([0, 1], [0, 1], mec=color_calc(len(times_to_plot) - 1), mfc=(1, 1, 1, 0), mew=mew,
    #                                 ms=np.sqrt(markersize), marker=marker, ls='')),
    #                   (lines.Line2D([0, 1], [0, 1], mec=color_calc(len(times_to_plot) - 1),
    #                                 mfc=color_calc(len(times_to_plot) - 1), mew=mew,
    #                                 ms=np.sqrt(markersize), marker=marker, ls=''))
    #                   ], ['Disk Dominated', 'Spheroid Dominated'],
    #                  handlelength=1, handletextpad=0.6, frameon=False, labelspacing=0.8)
    
    axs[0][0].legend([(lines.Line2D([0, 1], [0, 1], mec=color_calc(l),  mfc=(1, 1, 1, 0), mew=mew,
                                    ms=np.sqrt(markersize), marker=marker, ls=''))
                      for l in range(len(times_to_plot))],
                     # [r'$\Delta t =$' + str(round(time, 1)) + ' Gyr' for time in times_to_plot],
                     [str(round(time, 1)) + ' Gyr' for time in times_to_plot],
                     handlelength=1, handletextpad=0.6, frameon=False, labelspacing=0.2,
                     ncol=2, columnspacing=0.6, borderpad=0.1, loc='upper left')
    axs[1][0].legend([(lines.Line2D([0, 1], [0, 1], mec=color_calc(len(times_to_plot) - 1), mfc=(1, 1, 1, 0), mew=mew,
                                    ms=np.sqrt(markersize), marker=marker, ls='')),
                      (lines.Line2D([0, 1], [0, 1], mec=color_calc(len(times_to_plot) - 1), mfc=(1, 1, 1, 0), mew=mew,
                                    ms=np.sqrt(markersize), marker=marker2, ls=''))],
                     # [r'$V_{c}(R_{1/2})$', r'$\overline{v}_{\phi} (R_{1/2})$'],
                     [r'Circular Velocity', 'Mean Stellar\nAzimuthal Velocity'],
                     handlelength=1, handletextpad=0.6, frameon=False, labelspacing=0.2, loc='upper left')

    ms = np.linspace(8, 12)

    # Bloom et al. 2017: SMAI
    slope = 0.31
    intercept = -0.93
    vs = slope * ms + intercept

    # #Hardwick et al. 2022: SMAI
    # slope = 1 / 4.05
    # intercept = -1.27 / 4.05
    # vs = slope * ms + intercept

    # Sugessed by Mike
    alpha = 0.67
    j0_d = 3.17
    j0_b = 2.25
    js_d = j0_d + alpha * (ms - 10.5)
    js_b = j0_b + alpha * (ms - 10.5)

    # hernquist
    f_m = 0.01  # stellar mass fraciton
    m_h = 10 ** ms / f_m  # halo mass
    H = 0.1
    V_200 = (10 * H * m_h * GRAV_CONST * 1e-10) ** (1 / 3)
    R_200 = V_200 / (10 * H)
    c = 10
    a_h = (R_200 / c) * np.sqrt(2 * (np.log(1 + c) - c / (1 + c)))
    vmax = np.sqrt(GRAV_CONST * m_h * 1e-10 / (4 * a_h))

    r_disk = 0.12 * a_h
    stellar_half_mass = 1.687 * r_disk
    stellar_half_mass = np.repeat(stellar_half_mass, 2).reshape(len(stellar_half_mass), 2)

    v_c_hern = [get_analytic_hernquist_circular_velocity(rs, v, a, save_name_append='diff')[0]
                for (rs, v, a) in zip(stellar_half_mass, vmax, a_h)]
    v_c_exp = [get_exponential_disk_velocity(rs, m * 1e-10 * f_m, r, save_name_append='diff')[0]
               for (rs, m, r) in zip(stellar_half_mass, m_h, r_disk)]

    v_c_total = np.sqrt(np.array(v_c_hern) ** 2 + np.array(v_c_exp) ** 2)

    N_200s = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8])
    N_200_labels = [r'$10^0$', r'$10^1$', r'$10^2$', r'$10^3$', r'$10^4$', r'$10^5$', r'$10^6$', r'$10^7$', r'$10^8$']

    m_dm = 7
    for n200, label in zip(N_200s, N_200_labels):
        x = n200 + m_dm + np.log10(f_m)
        if 8.2 < x < 11.5:
            axs[1][0].text(x, 1.3, label, color='k')
            for k in range(2):
                if n200 == 4 and k==0:
                    axs[k][0].axvline(x, 0, 0.55, ls=':', c='k')
                elif n200 == 4 and k == 1:
                    axs[k][0].axvline(x, 0, 0.65, ls=':', c='k')
                else:
                    axs[k][0].axvline(x, 0, 1, ls=':', c='k')

    axs[1][0].text(9, 1.3, r'N$_{\mathrm{DM}} = $', color='k', ha='right')

    axs[0][0].errorbar(ms, np.log10(stellar_half_mass[:, 0]), ls='--', c='grey')
    axs[1][0].errorbar(ms, np.log10(v_c_total), ls='--', c='grey')

    axs[0][0].set_ylabel(r'$\log (R_{1/2} / $ kpc)')
    axs[1][0].set_ylabel(r'$\log (v_{\mathrm{rot}} / $kms$^{-1})$')

    axs[1][0].set_xlabel(r'$\log( M_\star / \mathrm{M}_\odot )$')

    # axs[0][0].set_title(r'$m_\mathrm{DM} = 10^{7} \mathrm{ M}_\odot$')
    axs[0][0].set_title(r'DM particle mass$= 10^{7} \mathrm{ M}_\odot$')

    axs[0][0].text(8.3, 0.27, r'$r_{1/2} \propto M^{1/3}_\star$',
                   rotation=np.arctan(1 / 3) * 180 / np.pi)  # angle0)
    axs[1][0].text(8.3, 1.71, r'$v_{\rm rot} \propto M^{1/3}_\star$',
                   rotation=np.arctan(1 / 3) * 180 / np.pi)  # angle1)
    axs[0][0].text(8.3, 0.35, '     (ICs)', va='top',
                   rotation=np.arctan(1 / 3) * 180 / np.pi)  # angle0)
    axs[1][0].text(8.3, 1.79, '     (ICs)', va='top',
                   rotation=np.arctan(1 / 3) * 180 / np.pi)  # angle1)

    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return


def experiment_concentration_dependances(save_name_append='diff', save=False, name=''):
    ''''
    '''
    fig = plt.figure()
    fig.set_size_inches(8, 12, forward=True)

    ax0 = plt.subplot(411)
    ax1 = plt.subplot(412)
    ax2 = plt.subplot(413)
    # ax2p5 = plt.subplot(514)
    ax3 = plt.subplot(414)

    # axs = [ax0, ax1, ax2, ax3, ax2p5]
    axs = [ax0, ax1, ax2, ax3]

    for i, ax in enumerate(axs):
        # ax.set_xlim([-2, 1.5])
        ax.set_xlim([-3, 0.5])
        if i!=len(axs)-1: ax.set_xticklabels([])

    ax0.set_ylim([-0.6,0])
    ax1.set_ylim([-0.75,0.25])
    ax2.set_ylim([0.0,2])
    # ax3.set_ylim([-1,5])

    plt.subplots_adjust(wspace=0.00, hspace=0.02)

    galaxy_name_list = ['mu_5_c_7/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_5_c_15/fdisk_0p01_lgMdm_7p0_V200-200kmps']

    colour_list = ['C0', 'C1', 'C2']

    bin_edges = get_bin_edges(save_name_append='diff', galaxy_name='mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps')
    analytic_density = get_density_scale(bin_edges, 0, 0, 0, 0, analytic_dm=True,
                                         save_name_append=save_name_append, galaxy_name='mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps')
    rho200 = get_rho_200('mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps')
    # for dens in analytic_density:
    #     ax3.axhline(np.log10(dens/rho200), 0,1, c='k', ls=':')

    for i, galaxy_name in enumerate(galaxy_name_list):

        (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
         MassStar, PosStars, VelStars, IDStars, PotStars
         ) = load_nbody.load_and_align(galaxy_name, '000') # f'%03d'%BURN_IN)

        # halo properties
        v200 = get_v_200(galaxy_name)
        r200 = get_r_200(galaxy_name)
        (a_h, v_max) = get_hernquist_params(galaxy_name)
        # r200 = a_h
        rho200 = get_rho_200(galaxy_name)

        # bins
        bin_edges = np.logspace(-1, 3, 31)

        # bins at a better resolution
        if save_name_append == 'diff':
            bin_centres = 10 ** ((np.log10(bin_edges[:-1]) + np.log10(bin_edges[1:])) / 2)
            bin_edges = np.roll(np.repeat(bin_edges, 2), -1)[:-2]

        # elif save_name_append == 'cum':
        #   bin_centres    = bin_edges.copy()
        #   bin_edge_holer = np.zeros(2*len(bin_edges))
        #   bin_edge_holer[1::2] = bin_edges
        #   bin_edges      = bin_edge_holer
        
        analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                                 save_name_append=save_name_append, galaxy_name=galaxy_name)
        analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)

        analytic_density = get_density_scale(bin_edges, 0, 0, 0, 0, analytic_dm=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)

        axs[0].errorbar(np.log10(bin_centres/r200), np.log10(analytic_dispersion/v200), c=colour_list[i])
        axs[1].errorbar(np.log10(bin_centres/r200), np.log10(analytic_v_circ/v200), c=colour_list[i])

        # axs[3].errorbar(np.log10(bin_centres/r200), np.log10(analytic_density/rho200), c=colour_list[i])

        gamma = analytic_v_circ/(np.sqrt(2)*analytic_dispersion)

        erf_dash = lambda y : 2 / np.sqrt(np.pi) * np.exp(-y * y)
        G = lambda y: (erf(y) - y * erf_dash(y)) / (2 * y ** 3)
        H = lambda y: ((2 * y ** 2 - 1) * erf(y) + y * erf_dash(y)) / (2 * y ** 3)

        axs[2].errorbar(np.log10(bin_centres/r200), gamma, c='C3')
        axs[2].errorbar(np.log10(bin_centres/r200), G(gamma), c='C4')
        axs[2].errorbar(np.log10(bin_centres/r200), H(gamma), c='C5')
        
        #measured
        (v_R, v_phi, s_z, s_R, s_phi
         ) = get_dispersion_profiles(PosDMs, VelDMs, bin_edges, cylindrical=False)
        s_1d = np.sqrt((s_z**2 + s_R**2 + s_phi**2)/3)

        r_dm = np.linalg.norm(PosDMs, axis=1)
        v2_dm = GRAV_CONST * MassDM * np.array([np.sum(r_dm < r_c) / r_c for r_c in bin_centres])
        r_star = np.linalg.norm(PosStars, axis=1)
        v2_star = GRAV_CONST * MassStar * np.array([np.sum(r_star < r_c) / r_c for r_c in bin_centres])
        v_tot = np.sqrt(v2_dm + v2_star)

        rho = get_dm_density(PosDMs, MassDM, bin_edges, cylindrical=False)

        ls='--'
        axs[0].errorbar(np.log10(bin_centres / r200), np.log10(s_1d / v200),
                        c=colour_list[i], ls=ls)
        axs[1].errorbar(np.log10(bin_centres / r200), np.log10(v_tot / v200),
                        c=colour_list[i], ls=ls)
        axs[2].errorbar(np.log10(bin_centres / r200), v_tot / (np.sqrt(2) * s_1d),
                        c='C3', ls=ls)
        axs[3].errorbar(np.log10(bin_centres / r200), np.log10(rho / rho200),
                        c=colour_list[i], ls=ls)

        # ax2p5.errorbar(np.log10(bin_centres / r200), np.log10((bin_centres / a_h)**2 * (bin_centres / a_h + 1)**-3),
        #                c=colour_list[i], ls=ls)

        #points
        bin_edges = get_bin_edges(save_name_append='diff', galaxy_name=galaxy_name)

        analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                                 save_name_append=save_name_append, galaxy_name=galaxy_name)

        analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)

        analytic_density = get_density_scale(bin_edges, 0, 0, 0, 0, analytic_dm=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)

        bin_edges = get_bin_edges(save_name_append='cum', galaxy_name=galaxy_name)

        marker='o'
        ls=''
        axs[0].errorbar(np.log10(bin_edges[1::2] / r200), np.log10(analytic_dispersion / v200),
                        c=colour_list[i], marker=marker, ls='')
        axs[1].errorbar(np.log10(bin_edges[1::2] / r200), np.log10(analytic_v_circ / v200),
                        c=colour_list[i], marker=marker, ls='')
        axs[2].errorbar(np.log10(bin_edges[1::2] / r200), analytic_v_circ / (np.sqrt(2) * analytic_dispersion),
                        c=colour_list[i], marker=marker, ls='')
        axs[3].errorbar(np.log10(bin_edges[1::2] / r200), np.log10(analytic_density / rho200),
                        c=colour_list[i], marker=marker, ls='')
        
        print(analytic_v_circ / analytic_dispersion)
        
        for ax in axs:
            for edge in np.log10(bin_edges[1::2] / r200):
                ax.axvline(edge, 0,1, c=colour_list[i], ls=':')

    axs[0].legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-', linewidth=2)),
                   (lines.Line2D([0, 1], [0, 1], color='C1', ls='-', linewidth=2)),
                   (lines.Line2D([0, 1], [0, 1], color='C2', ls='-', linewidth=2))],
                   [r'$c=7$', r'$c=10$', r'$c=15$'],
                   frameon=False, labelspacing=0.2)
    
    axs[1].legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='-', linewidth=2)),
                   (lines.Line2D([0, 1], [0, 1], color='grey', ls='--', linewidth=2)),
                   (lines.Line2D([0, 1], [0, 1], color='grey', ls='', marker='o'))],
                   [r'Analytic', r'Snapshot000', r'$R_{1/4}, R_{1/2} & R_{3/4}$'],
                   loc='lower right', frameon=False, labelspacing=0.2)

    axs[2].legend([(lines.Line2D([0, 1], [0, 1], color='C3', ls='-', linewidth=2)),
                   (lines.Line2D([0, 1], [0, 1], color='C4', ls='-', linewidth=2)),
                   (lines.Line2D([0, 1], [0, 1], color='C5', ls='-', linewidth=2))],
                   [r'$\gamma$', r'$G(\gamma)$', r'$H(\gamma)$'],
                   frameon=False, labelspacing=0.2)

    axs[0].set_ylabel(r'$\log \sigma / V_{200} $')
    axs[1].set_ylabel(r'$\log v_c / V_{200} $')
    axs[2].set_ylabel(r'$\gamma = v_c / \sqrt{2} \sigma $')
    # axs[3].set_ylabel(r'$\log \rho / \rho_{200} $')
    
    # axs[2].set_xlabel(r'$\log r / R_{200}$')
    axs[3].set_xlabel(r'$\log r / a$')

    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return


def generate_galaxy_data_sample(galaxy_name_list, snaps_list, radius=None, save_name_append='diff'):

    # things to calculate
    time_scale_array = []
    scale_density_ratio_array = []
    scale_velocity_ratio_array = []
    scale_vcirc_ratio_array = []
    n_array = []

    time_array = []

    velocity_scale_array_v = []
    velocity_scale2_array_dispersion = []

    measued_velocity_array_v = []
    inital_velocity_array_v = []
    measued_velocity2_array_z = []
    inital_velocity2_array_z = []
    measued_velocity2_array_r = []
    inital_velocity2_array_r = []
    measued_velocity2_array_p = []
    inital_velocity2_array_p = []

    for i, (galaxy_name, snaps) in enumerate(zip(galaxy_name_list, snaps_list)):
        kwargs = get_name_kwargs(galaxy_name)

        bin_edges = get_bin_edges(save_name_append, galaxy_name=galaxy_name)

        if radius != None:
            if radius == [0,1]: bin_edges = bin_edges[:4]
            elif radius == [1,2]: bin_edges = bin_edges[2:]
            elif radius == [0,2]: bin_edges = bin_edges[[0,1, 4,5]]
            else:
                bin_edges = bin_edges[2*radius:2*(radius+1)]

        analytic_dispersion = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_dispersion=True,
                                                 save_name_append=save_name_append, galaxy_name=galaxy_name)

        analytic_v_circ = get_velocity_scale(bin_edges, 0, 0, 0, 0, 0, 0, analytic_v_c=True,
                                             save_name_append=save_name_append, galaxy_name=galaxy_name)

        V200 = get_v_200(galaxy_name=galaxy_name)
        
        # calculate constants
        MassDM = kwargs['mdm']
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, save_name_append, MassDM, galaxy_name=galaxy_name)

        # load data
        (mean_v_R, mean_v_phi,
         sigma_z, sigma_R, sigma_phi
         ) = get_dispersions(galaxy_name, save_name_append, snaps, bin_edges)

        n = get_n_for_poisson(galaxy_name, save_name_append, snaps, bin_edges)

        (inital_velocity_v, inital_velocity_z, inital_velocity_r, inital_velocity_p
         ) = smooth_burn_ins(MassDM, snaps, save_name_append, BURN_IN,
                             get_bin_edges(save_name_append, galaxy_name=galaxy_name), galaxy_name=galaxy_name)

        time = np.linspace(0, T_TOT, snaps + 1) - np.linspace(0, T_TOT, snaps + 1)[BURN_IN] + 1e-4

        # burn in
        burn_in_mask = np.ones(snaps + 1, dtype=bool)
        burn_in_mask[:BURN_IN] = False

        max_log_tau_cut = -2.5
        # max_log_tau_cut = 0

        if type(radius) == int:
            mean_v_R = np.array([mean_v_R[:, radius]]).T
            mean_v_phi = np.array([mean_v_phi[:, radius]]).T
            sigma_z = np.array([sigma_z[:, radius]]).T
            sigma_R = np.array([sigma_R[:, radius]]).T
            sigma_phi = np.array([sigma_phi[:, radius]]).T
            n = np.array([n[:, radius]]).T

            inital_velocity_v = [inital_velocity_v[radius]]
            inital_velocity_z = [inital_velocity_z[radius]]
            inital_velocity_r = [inital_velocity_r[radius]]
            inital_velocity_p = [inital_velocity_p[radius]]

        elif type(radius) == list:
            mean_v_R = mean_v_R[:, radius]
            mean_v_phi = mean_v_phi[:, radius]
            sigma_z = sigma_z[:, radius]
            sigma_R = sigma_R[:, radius]
            sigma_phi = sigma_phi[:, radius]
            n = n[:, radius]

            inital_velocity_v = inital_velocity_v[radius]
            inital_velocity_z = inital_velocity_z[radius]
            inital_velocity_r = inital_velocity_r[radius]
            inital_velocity_p = inital_velocity_p[radius]

        log_tau = np.log10(time[:, np.newaxis] / t_c_dm[np.newaxis, :])
        tau_mask = log_tau < max_log_tau_cut
        burn_in_mask = np.logical_and(burn_in_mask[:, np.newaxis], tau_mask)

        time = [np.linspace(0, T_TOT, snaps + 1)[burn_in_mask[:, j]] - np.linspace(0, T_TOT, snaps + 1)[BURN_IN] + 1e-4
                for j in range(len(bin_edges)//2)]

        mean_v_R = mean_v_R.T[burn_in_mask.T]
        mean_v_phi = mean_v_phi.T[burn_in_mask.T]
        sigma_z = sigma_z.T[burn_in_mask.T]
        sigma_R = sigma_R.T[burn_in_mask.T]
        sigma_phi = sigma_phi.T[burn_in_mask.T]
        n = n.T[burn_in_mask.T]

        # shape = np.shape(sigma_z)
        # shape = np.array([np.ones(sum(burn_in_mask[:,0])), np.ones(sum(burn_in_mask[:,1])), np.ones(sum(burn_in_mask[:,2]))])
        data_length = np.sum(burn_in_mask)
        element_mask = np.zeros((len(bin_edges) // 2, data_length), dtype=bool)

        counter = 0
        for i in range(len(bin_edges) // 2):
            move = np.sum(tau_mask[:, i])
            element_mask[i, counter:counter + move] = True
            counter += move

        # #this weird that it is needed. Sometimes need analytic v_c > initial velocity
        # bad_ic_mask = inital_velocity_v > analytic_v_circ
        # analytic_v_circ[bad_ic_mask] = inital_velocity_v[bad_ic_mask] + 1e-3

        in_array = np.zeros(data_length)
        for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = time[i]
        time_array.append(in_array)

        # halo constants
        in_array = np.zeros(data_length)
        for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = t_c_dm[i]
        time_scale_array.append(in_array)
        in_array = np.zeros(data_length)
        for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = delta_dm[i]
        scale_density_ratio_array.append(in_array)
        in_array = np.zeros(data_length)
        for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = upsilon_dm[i]
        scale_velocity_ratio_array.append(in_array)

        in_array = np.zeros(data_length)
        for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = np.sqrt(upsilon_circ[i])
        # for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = upsilon_circ[i]
        scale_vcirc_ratio_array.append(in_array)

        # maximum values
        # in_array = np.zeros(data_length)
        # for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = analytic_v_circ[i]
        # # for i in range(len(bin_edges)//2): in_array[element_mask[i]] = inital_velocity_v[i]
        # velocity_scale_array_v.append(in_array)
        # in_array = np.zeros(data_length)
        # for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = analytic_dispersion[i] ** 2
        # velocity_scale2_array_dispersion.append(in_array)

        in_array = np.zeros(data_length)
        for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = V200
        # for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = V200 **2
        # for i in range(len(bin_edges)//2): in_array[element_mask[i]] = inital_velocity_v[i]
        velocity_scale_array_v.append(in_array)
        in_array = np.zeros(data_length)
        for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = V200 ** 2
        velocity_scale2_array_dispersion.append(in_array)

        # masued velocity data
        in_array = np.zeros(data_length)
        for i in range(len(bin_edges) // 2):
            in_array[element_mask[i]] = np.abs(analytic_v_circ[i, np.newaxis] - mean_v_phi[element_mask[i]])
            # in_array[element_mask[i]] = np.abs(inital_velocity_v[i, np.newaxis] - mean_v_phi[element_mask[i]])
        measued_velocity_array_v.append(in_array)
        measued_velocity2_array_z.append(sigma_z ** 2)
        measued_velocity2_array_r.append(sigma_R ** 2)
        measued_velocity2_array_p.append(sigma_phi ** 2)

        n_array.append(n)

        in_array = np.zeros(data_length)
        for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = (analytic_v_circ - inital_velocity_v)[i]
        inital_velocity_array_v.append(in_array)
        # in_array = np.zeros(data_length)
        # inital_velocity_array_v.append(in_array)

        in_array = np.zeros(data_length)
        for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = inital_velocity_z[i] ** 2
        inital_velocity2_array_z.append(in_array)
        in_array = np.zeros(data_length)
        for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = inital_velocity_r[i] ** 2
        inital_velocity2_array_r.append(in_array)
        in_array = np.zeros(data_length)
        for i in range(len(bin_edges) // 2): in_array[element_mask[i]] = inital_velocity_p[i] ** 2
        inital_velocity2_array_p.append(in_array)

    # flatten
    time_scale_array = np.ravel(np.concatenate(time_scale_array))
    scale_density_ratio_array = np.ravel(np.concatenate(scale_density_ratio_array))
    scale_velocity_ratio_array = np.ravel(np.concatenate(scale_velocity_ratio_array))

    time_array = np.ravel(np.concatenate(time_array))

    velocity_scale_array_v = np.ravel(np.concatenate(velocity_scale_array_v))
    velocity_scale2_array_dispersion = np.ravel(np.concatenate(velocity_scale2_array_dispersion))

    measued_velocity_array_v = np.ravel(np.concatenate(measued_velocity_array_v))
    inital_velocity_array_v = np.ravel(np.concatenate(inital_velocity_array_v))
    measued_velocity2_array_z = np.ravel(np.concatenate(measued_velocity2_array_z))
    inital_velocity2_array_z = np.ravel(np.concatenate(inital_velocity2_array_z))
    measued_velocity2_array_r = np.ravel(np.concatenate(measued_velocity2_array_r))
    inital_velocity2_array_r = np.ravel(np.concatenate(inital_velocity2_array_r))
    measued_velocity2_array_p = np.ravel(np.concatenate(measued_velocity2_array_p))
    inital_velocity2_array_p = np.ravel(np.concatenate(inital_velocity2_array_p))

    n_array = np.ravel(np.concatenate(n_array))

    # return (time_scale_array, scale_density_ratio_array, scale_velocity_ratio_array,
    #         time_array, velocity_scale_array_v, velocity_scale2_array_dispersion,
    #         measued_velocity_array_v, inital_velocity_array_v,
    #         measued_velocity2_array_z, inital_velocity2_array_z,
    #         measued_velocity2_array_r, inital_velocity2_array_r,
    #         measued_velocity2_array_p, inital_velocity2_array_p, n_array)

    data_v = (measued_velocity_array_v, velocity_scale_array_v, inital_velocity_array_v,
              time_array, time_scale_array,
              scale_density_ratio_array, scale_velocity_ratio_array, n_array)

    data_z = (measued_velocity2_array_z, velocity_scale2_array_dispersion, inital_velocity2_array_z,
              time_array, time_scale_array,
              scale_density_ratio_array, scale_velocity_ratio_array, n_array)
    
    data_r = (measued_velocity2_array_r, velocity_scale2_array_dispersion, inital_velocity2_array_r,
              time_array, time_scale_array,
              scale_density_ratio_array, scale_velocity_ratio_array, n_array)
    
    data_p = (measued_velocity2_array_p, velocity_scale2_array_dispersion, inital_velocity2_array_p,
              time_array, time_scale_array,
              scale_density_ratio_array, scale_velocity_ratio_array, n_array)

    return(data_v, data_z, data_r, data_p)


def evaluate_likelihood_grid(l_grid, a_grid, b_grid, data, fixed_args=[None,None,None,0], mcmc=False):

    start = []
    start_ab = []
    start_lb = []
    start_la = []
    start_best = [200, -0.5, 0]

    for i, element in enumerate(fixed_args):
        if element == None:
            start.append(start_best[i])
            if i != 0: start_ab.append(start_best[i])
            if i != 1: start_lb.append(start_best[i])
            if i != 2: start_la.append(start_best[i])
    min_result = minimize(_log_like, start, args=(data, fixed_args),
                          method='Nelder-Mead')

    params = np.zeros(len(fixed_args))
    i = 0
    for j in range(len(fixed_args)):
        if fixed_args[j] == None:
            params[j] = min_result.x[i]
            i += 1
        else:
            params[j] = fixed_args[j]

    print(min_result.success)
    if not min_result.success:
        print(min_result)
        raise ValueError('Did not find optimal fit.')

    # evaluation_grid = np.zeros((len(l_grid), len(a_grid), len(b_grid)))
    #
    # for i, l in enumerate(l_grid):
    #     for j, a in enumerate(a_grid):
    #         for k, b in enumerate(b_grid):
    #
    #             evaluation_grid[i,j,k] = _log_like((l,a,b,0), data, (None,None,None,0))
    #
    # return(evaluation_grid, min_result.x)

    evaluation_l = np.zeros((len(a_grid), len(b_grid)))
    evaluation_a = np.zeros((len(l_grid), len(b_grid)))
    evaluation_b = np.zeros((len(l_grid), len(a_grid)))

    evaluation_la = np.zeros(len(b_grid))
    evaluation_lb = np.zeros(len(a_grid))
    evaluation_ab = np.zeros(len(l_grid))

    tol=1e-3
    for j, a in enumerate(a_grid):
        if fixed_args[1] != None:
            a = fixed_args[1]
        for k, b in enumerate(b_grid):
            if fixed_args[2] != None:
                b = fixed_args[2]

            if fixed_args[0] == None:
                evaluation_l[j,k] = minimize(_log_like, (start_best[0]), args=(data, [None,a,b,0]),
                                             method='Nelder-Mead', tol=tol).fun
            else: evaluation_l[j,k] = _log_like([], data, [fixed_args[0], a, b, 0])

    tol=1e-4
    for i, l in enumerate(l_grid):
        if fixed_args[0] != None:
            l = fixed_args[0]
        for k, b in enumerate(b_grid):
            if fixed_args[2] != None:
                b = fixed_args[2]

            if fixed_args[1] == None:
                evaluation_a[i,k] = minimize(_log_like, (start_best[1]), args=(data, [l,None,b,0]),
                                             method='Nelder-Mead', tol=tol).fun
            else: evaluation_a[j,k] = _log_like([], data, [l, fixed_args[1], b, 0])

    tol=1e-6
    for i, l in enumerate(l_grid):
        if fixed_args[0] != None:
            l = fixed_args[0]
        for j, a in enumerate(a_grid):
            if fixed_args[1] != None:
                a = fixed_args[1]

            if fixed_args[2] == None:
                evaluation_b[i, j] = minimize(_log_like, (start_best[2]), args=(data, [l, a, None, 0]),
                                              method='Nelder-Mead', tol=tol).fun
            else: evaluation_b[i, j] = _log_like([], data, [l, a, fixed_args[2], 0])


    tol=1e-6
    for k, b in enumerate(b_grid):
        if fixed_args[2] == None:
            evaluation_la[k] = minimize(_log_like, start_la, args=(data, [fixed_args[0], fixed_args[1], b, 0]),
                                          method='Nelder-Mead', tol=tol).fun
        else: evaluation_la[k] = None #min_result.fun

    for j, a in enumerate(a_grid):
        if fixed_args[1] == None:
            evaluation_lb[j] = minimize(_log_like, start_lb, args=(data, [fixed_args[0], a, fixed_args[2], 0]),
                                          method='Nelder-Mead', tol=tol).fun
        else: evaluation_lb[j] = None #min_result.fun

    for i, l in enumerate(l_grid):
        if fixed_args[0] == None:
            evaluation_ab[i] = minimize(_log_like, start_ab, args=(data, [l, fixed_args[1], fixed_args[2], 0]),
                                          method='Nelder-Mead', tol=tol).fun
        else: evaluation_ab[i] = None #min_result.fun

    if mcmc:

        n_burn_in = int(1_000)
        n_anal = int(2_000)  # int(10_000)

        start_loc = start_best #min_result.x

        # emcee stuff
        # lnprior = lambda args : 0
        def lnprior(args, data, fixed_args, likelihood):
            if np.isnan(likelihood):
                return np.inf
            if np.any(np.isnan(args)) or np.any(np.isinf(args)):
                return np.inf
            return 0

        # lnprob = lambda args : lnprior(args) + lnlike(args)
        def lnprob(args, data, fixed_args):
            
            #random scatter added
            _data = add_random_error_to_data(data, randomness=0.1)
            
            likelihood = _log_like(args, _data, fixed_args)
            prior = lnprior(args, _data, fixed_args, likelihood)

            if not np.isfinite(prior) or np.isnan(prior):
                return (-np.inf)
            # return 10 ** (prior - likelihood)
            return prior - likelihood

        nwalkers = 10
        ndim = len(start_loc)
        np.random.seed(1)

        p0 = [np.array(start_loc) + 1e-10 * np.random.randn(ndim) for i in range(nwalkers)]

        sampler = emcee.EnsembleSampler(nwalkers, ndim, lnprob, args=(data, fixed_args),
                                        live_dangerously=True)

        # print("Running burn-in...")
        # p0, _, _ = sampler.run_mcmc(p0, n_burn_in, skip_initial_state_check=True)
        # sampler.reset()

        print("Running...")
        pos, prob, state = sampler.run_mcmc(p0, n_burn_in + n_anal, skip_initial_state_check=True,
                                            progress=True)

        # get resutls
        chains = sampler.get_chain(thin=4, discard=n_burn_in, flat=True)
        print(chains)

        # figure = corner.corner(samples,
        #                        show_titles=True,
        #                        plot_datapoints=True,
        #                        quantiles=[0.16, 0.5, 0.84],
        #                        title_fmt='.3f')
        
    else:
        chains = None

    return(evaluation_l, evaluation_a, evaluation_b,
           evaluation_ab,evaluation_lb,evaluation_la,
           params, min_result.fun,
           chains)


def add_random_error_to_data(data, randomness=0.05):
    
    data = (np.random.normal(data[0], np.abs(randomness * data[0])), *data[1:])

    return data


def experiment_fitting_constraints(save=False, name='', ns=41, mcmc=True):

    galaxy_name_list_fid = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                            'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                            'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                            'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
                            'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps']
    snaps_list_fid = [400, 400, 101, 101, 101]

    galaxy_name_list_conc = ['mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                            'mu_5_c_7/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                            'mu_5_c_15/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                            'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                            'mu_5_c_7/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                            'mu_5_c_15/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                            ]
    snaps_list_conc = [101, 101, 101,
                       400, 400, 400]

    gnl_conc710 = ['mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                   'mu_5_c_7/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                   'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                   'mu_5_c_7/fdisk_0p01_lgMdm_8p0_V200-200kmps']
    sl_conc710 = [101, 101, 400, 400]

    gnl_conc1015 = ['mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                   'mu_5_c_15/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                   'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                   'mu_5_c_15/fdisk_0p01_lgMdm_8p0_V200-200kmps']
    sl_conc1015 = [101, 101, 400, 400]

    gnl_conc715 = ['mu_5_c_7/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                   'mu_5_c_15/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                   'mu_5_c_7/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                   'mu_5_c_15/fdisk_0p01_lgMdm_8p0_V200-200kmps']
    sl_conc715 = [101, 101, 400, 400]

    # galaxy_name_list = ['mu_5_c_7/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     'mu_5_c_7/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     ]
    # snaps_list = [101, 400,
    #               ]

    # data_index = 1 #[data_v, data_z, data_r, data_p]
    data_name = [r'$\overline{v}_\phi$', r'$\sigma_z^2$', r'$\sigma_R^2$', r'$\sigma_\phi^2$']
    # data_indexes = [0, 1, 2, 3]
    data_indexes = [1]

    # l_grid = np.logspace(1.2,2.2, 11)
    # a_grid = np.linspace(-0.5, 0, 11)
    # b_grid = np.linspace(-2, 0, 11)

    galaxy_list_list = [#galaxy_name_list_fid, #galaxy_name_list_fid, galaxy_name_list_fid,
                        #galaxy_name_list_fid, galaxy_name_list_fid, galaxy_name_list_fid,
                        # gnl_conc710, gnl_conc1015, gnl_conc715
                        galaxy_name_list_conc
                        ]
    snaps_list_list = [#snaps_list_fid, #snaps_list_fid, snaps_list_fid,
                       #snaps_list_fid, snaps_list_fid, snaps_list_fid,
                       # sl_conc710, sl_conc1015, sl_conc715
                       snaps_list_conc
                       ]
    fixed_args_list = [#[None, None, None, 0], #[None, None, 0, 0], [None, None, -1, 0],
                       #[None, None, 0, 0], [None, None, 0, 0], [None, None, 0, 0],
                       # [None, None, None, 0], [None, None, None, 0], [None, None, None, 0]
                       [None, None, None, 0]
                       ]
    radius_list = [None, None, None, None] #[[0,1], [1,2], [0,2]]
    colour_list_list = ['C0', 'C1', 'C2', 'C6'] #['C7', 'C8', 'C9'] #['C3', 'C4', 'C5']
    name_list = ['Fiducual Galaxies', r'Fid. $\beta=0$', r'Fid. $\beta=-1$',
                 # r'Fid. $\beta=0$, $R_{1/4} & R_{1/2}$', r'Fid. $\beta=0$, $R_{1/2} & R_{3/4}$',
                 # r'Fid. $\beta=0$, $R_{1/4} & R_{3/4}$',
                 # r'$c=7, 10$', r'$c=10, 15$', r'$c=7, 15$'
                 r'$c=7,10,15$'
                 ]

    linestyles = ['-', '--']
    #[data_v, data_z, data_r, data_p] = generate_galaxy_data_sample(galaxy_name_list, snaps_list)

    vmin = []
    vmax = []

    for data_index in data_indexes:

        fig = plt.figure()
        fig.set_size_inches(20, 18, forward=True)

        axa = plt.subplot(3,3,1)
        axb = plt.subplot(3,3,5)
        axc = plt.subplot(3,3,9)

        ax0 = plt.subplot(3,3,4)
        ax2 = plt.subplot(3,3,7)
        ax3 = plt.subplot(3,3,8)

        # plt.subplots_adjust(wspace=0.02, hspace=0.02)

        for i in range(len(galaxy_list_list)):

            # if i == 0:
            #     l_grid = np.logspace(0.4, 3.4, ns)
            #     a_grid = np.linspace(-1.2, 0.6, ns)
            #     b_grid = np.linspace(-4, 3, ns)
            # else:
            l_grid = np.logspace(1.4, 2.4, ns)
            a_grid = np.linspace(-0.6, 0, ns)
            b_grid = np.linspace(-2, 1, ns)

            #calc best fits
            datas = generate_galaxy_data_sample(galaxy_list_list[i], snaps_list_list[i], radius_list[i])
            (margin_l, margin_a, margin_b,
             margin_ab,margin_lb,margin_la,
             min_x, min_fun,
             chains) = evaluate_likelihood_grid(l_grid, a_grid, b_grid, datas[data_index], fixed_args_list[i], mcmc)
            # print(min_x)
            min_xp = [min_x[0] / (np.sqrt(2) * np.pi), *min_x[1:]]
            print(min_xp)

            dof = np.shape(datas[data_index])[1] + np.sum([i==None for i in fixed_args_list[i]])

            # print(min_fun)
            # print(np.amin(margin_l))
            # print(np.amin(margin_a))
            # print(np.amin(margin_b))
            # print()

            # levels = np.array((1.02, 1.04)) * min_fun
            levels = np.array((1.05, 1.1)) * min_fun
            # levels = None

            # ax0.contour(np.log10(l_grid), a_grid, margin_b.T, levels = levels,
            #             colors = colour_list_list[i], linestyles=linestyles)
            # ax2.contour(np.log10(l_grid), b_grid, margin_a.T, levels = levels,
            #             colors = colour_list_list[i], linestyles=linestyles)
            # ax3.contour(a_grid,           b_grid, margin_l.T, levels = levels,
            #             colors = colour_list_list[i], linestyles=linestyles)

            transform = lambda out : out #np.exp(out) / dof
            axa.errorbar(np.log10(l_grid), transform(margin_ab), c = colour_list_list[i])
            axb.errorbar(a_grid, transform(margin_lb), c = colour_list_list[i])
            axc.errorbar(b_grid, transform(margin_la), c = colour_list_list[i])

            vmax.append(np.amax((margin_a, margin_b, margin_l)))
            vmin.append(np.amin((margin_a, margin_b, margin_l)))

            if i == 0:
                ax0.pcolormesh(np.log10(l_grid), a_grid, margin_b.T, vmin=vmin[i], vmax=vmax[i], zorder=-10)
                ax2.pcolormesh(np.log10(l_grid), b_grid, margin_a.T, vmin=vmin[i], vmax=vmax[i], zorder=-10)
                ax3.pcolormesh(a_grid,           b_grid, margin_l.T, vmin=vmin[i], vmax=vmax[i], zorder=-10)

            marker = 'P'
            ms = 20**2
            ax0.scatter(np.log10(min_x[0]),min_x[1], 
                        c=colour_list_list[i], marker=marker, s=ms, edgecolors='k', linewidths=3)
            ax2.scatter(np.log10(min_x[0]),min_x[2], 
                        c=colour_list_list[i], marker=marker, s=ms, edgecolors='k', linewidths=3)
            ax3.scatter(min_x[1],min_x[2], 
                        c=colour_list_list[i], marker=marker, s=ms, edgecolors='k', linewidths=3)

            axa.scatter(np.log10(min_x[0]), transform(min_fun), 
                        c=colour_list_list[i], marker=marker, s=ms  , edgecolors='k', linewidths=3)
            axb.scatter(min_x[1], transform(min_fun), 
                        c=colour_list_list[i], marker=marker, s=ms, edgecolors='k', linewidths=3)
            axc.scatter(min_x[2], transform(min_fun), 
                        c=colour_list_list[i], marker=marker, s=ms, edgecolors='k', linewidths=3)
            
            if mcmc:
                levels = np.array([0.5])
                
                n_tight = 201
                tight_l_grid = np.logspace(1.4, 2.4, n_tight)
                tight_a_grid = np.linspace(-0.6, 0, n_tight)
                tight_b_grid = np.linspace(-2, 1, n_tight)
                dl = tight_l_grid[1] - tight_l_grid[0]
                da = tight_a_grid[1] - tight_a_grid[0]
                db = tight_b_grid[1] - tight_b_grid[0]
                
                chain_b,_,_ = np.histogram2d(chains[:, 0], chains[:, 1], bins=[tight_l_grid, tight_a_grid])
                chain_a,_,_ = np.histogram2d(chains[:, 0], chains[:, 2], bins=[tight_l_grid, tight_b_grid])
                chain_l,_,_ = np.histogram2d(chains[:, 1], chains[:, 2], bins=[tight_a_grid, tight_b_grid])
                
                #normed couts per volume
                chain_b /= len(chains) / dl / da
                chain_a /= len(chains) / dl / db
                chain_l /= len(chains) / da / db
                
                print(np.amax(chain_b))
                print(np.amax(chain_a))
                print(np.amax(chain_l))
                
                tight_l_grid = 10**(0.5 * (np.log10(tight_l_grid[1:]) + np.log10(tight_l_grid[:-1])))
                tight_a_grid = 0.5 * (tight_a_grid[1:] + tight_a_grid[:-1])
                tight_b_grid = 0.5 * (tight_b_grid[1:] + tight_b_grid[:-1])
                
                # ax0.contour(np.log10(tight_l_grid), tight_a_grid, chain_b.T, levels = levels,
                #             colors = colour_list_list[i], linestyles=linestyles)
                # ax2.contour(np.log10(tight_l_grid), tight_b_grid, chain_a.T, levels = levels,
                #             colors = colour_list_list[i], linestyles=linestyles)
                # ax3.contour(tight_a_grid,           tight_b_grid, chain_l.T, levels = levels,
                #             colors = colour_list_list[i], linestyles=linestyles)
                
                ax0.pcolormesh(np.log10(tight_l_grid), tight_a_grid, np.log10(chain_b.T))
                ax2.pcolormesh(np.log10(tight_l_grid), tight_b_grid, np.log10(chain_a.T))
                ax3.pcolormesh(tight_a_grid,           tight_b_grid, np.log10(chain_l.T))
                

        #dressing
        ax0.set_xlim(np.log10([l_grid[0], l_grid[-1]]))
        ax2.set_xlim(np.log10([l_grid[0], l_grid[-1]]))
        ax3.set_xlim([a_grid[0], a_grid[-1]])
        axa.set_xlim(np.log10([l_grid[0], l_grid[-1]]))
        axb.set_xlim([a_grid[0], a_grid[-1]])
        axc.set_xlim([b_grid[0], b_grid[-1]])

        ax0.set_ylim([a_grid[0], a_grid[-1]])
        ax2.set_ylim([b_grid[0], b_grid[-1]])
        ax3.set_ylim([b_grid[0], b_grid[-1]])

        vmin = np.amin(vmin)
        vmax = np.amin(vmax)
        dv = vmax - vmin
        vi = vmin - 0.1 * dv
        va = vmax + 0.1 * dv
        yl = (vi, va)

        axa.set_ylim(yl)
        axb.set_ylim(yl)
        axc.set_ylim(yl)

        # ax0.set_xticklabels([])
        # ax3.set_yticklabels([])

        # axa.set_xticklabels([])
        # axb.set_xticklabels([])

        # axa.set_yticklabels([])
        # axb.set_yticklabels([])
        # axc.set_yticklabels([])

        ax0.set_ylabel(r'$\alpha$')
        ax2.set_ylabel(r'$\beta$')
        ax2.set_xlabel(r'$\log k$')
        ax3.set_xlabel(r'$\alpha$')

        axa.set_ylabel(r'$\chi^2 / $d.o.f.')
        axc.set_xlabel(r'$\beta$')

        axb.legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='-', linewidth=2))],
                   [data_name[data_index]], frameon=False)
        axa.legend([(lines.Line2D([0, 1], [0, 1], color=colour, ls='-', linewidth=2))
                    for colour in colour_list_list], name_list,
                    frameon=False, labelspacing=0.2)

        if save:
            plt.savefig(name + data_name[data_index], bbox_inches='tight')
            plt.close()

    return


def print_interesting_quantities(galaxy_name):

    get_bin_edges()

    (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
     MassStar, PosStars, VelStars, IDStars, PotStars
     ) = load_nbody.load_and_align(galaxy_name, '000')

    interesting_radii = get_radii_that_are_interesting(np.sqrt(PosStars[:, 0]**2 + PosStars[:, 1]**2))

    bin_edges = get_dex_bins(interesting_radii, 0.2)

# calculate
    (mean_v_R, mean_v_phi,
     sigma_z, sigma_R, sigma_phi
     ) = get_dispersion_profiles(PosStars, VelStars, bin_edges)

    z_half = np.median(np.abs(PosStars[:, 2]))
    
    r200 = get_r_200(galaxy_name)

    c = get_name_kwargs(galaxy_name)['conc']

    print(galaxy_name)

    print(np.shape(PosDMs)[0], np.log10(np.shape(PosDMs)[0]))
    print(np.log10(MassDM * 10**10))

    # print(interesting_radii[1] / (-lambertw(-1/(2*np.exp(1)), -1) - 1).real / r200)
    r_minus_2 = r200 / c * np.sqrt(2 * (np.log(1 + c) - c / (1+c))) / 2
    print(interesting_radii[1] / (-lambertw(-1/(2*np.exp(1)), -1) - 1).real / r_minus_2)

    print(z_half / interesting_radii[1] / np.log(3) * 2 * (-lambertw(-1 / (2 * np.exp(1)), -1) - 1).real)

    print(sigma_z**2 / sigma_R**2)
    print(sigma_phi**2 / sigma_R**2)

    print()

    return


def find_am_n_dependance():
    galaxy_name_list = [
        'mu_5/fdisk_0p01_lgMdm_6p0_V200-50kmps',
        'mu_5/fdisk_0p01_lgMdm_6p0_V200-50kmps_seed1',
        'mu_5/fdisk_0p01_lgMdm_6p0_V200-50kmps_seed2',
        'mu_5/fdisk_0p01_lgMdm_6p0_V200-50kmps_seed3',
        'mu_5/fdisk_0p01_lgMdm_6p0_V200-50kmps_seed4',
        'mu_5/fdisk_0p01_lgMdm_6p0_V200-50kmps_seed5',
        'mu_5/fdisk_0p01_lgMdm_6p0_V200-50kmps_seed6',
        'mu_5/fdisk_0p01_lgMdm_6p0_V200-50kmps_seed7',
        'mu_5/fdisk_0p01_lgMdm_6p0_V200-50kmps_seed8',
        'mu_5/fdisk_0p01_lgMdm_6p0_V200-50kmps_seed9',
        'mu_5/fdisk_0p01_lgMdm_5p5_V200-50kmps',
        'mu_5/fdisk_0p01_lgMdm_5p0_V200-50kmps',
        'mu_5/fdisk_0p01_lgMdm_4p5_V200-50kmps',
        'mu_5/fdisk_0p01_lgMdm_4p0_V200-50kmps',
        'mu_5/fdisk_0p01_lgMdm_7p0_V200-100kmps',
        'mu_5/fdisk_0p01_lgMdm_7p0_V200-100kmps_seed1',
        'mu_5/fdisk_0p01_lgMdm_7p0_V200-100kmps_seed2',
        'mu_5/fdisk_0p01_lgMdm_7p0_V200-100kmps_seed3',
        'mu_5/fdisk_0p01_lgMdm_7p0_V200-100kmps_seed4',
        'mu_5/fdisk_0p01_lgMdm_7p0_V200-100kmps_seed5',
        'mu_5/fdisk_0p01_lgMdm_7p0_V200-100kmps_seed6',
        'mu_5/fdisk_0p01_lgMdm_7p0_V200-100kmps_seed7',
        'mu_5/fdisk_0p01_lgMdm_7p0_V200-100kmps_seed8',
        'mu_5/fdisk_0p01_lgMdm_7p0_V200-100kmps_seed9',
        'mu_5/fdisk_0p01_lgMdm_6p5_V200-100kmps',
        'mu_5/fdisk_0p01_lgMdm_6p0_V200-100kmps',
        'mu_5/fdisk_0p01_lgMdm_5p5_V200-100kmps',
        'mu_5/fdisk_0p01_lgMdm_5p0_V200-100kmps',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed1',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed2',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed3',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed4',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed5',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed6',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed7',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed8',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed9',
        'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
        'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps_seed1',
        'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps_seed2',
        'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps_seed3',
        'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps_seed4',
        'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
        'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
        'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps',
        'mu_5/fdisk_0p01_lgMdm_9p0_V200-400kmps',
        'mu_5/fdisk_0p01_lgMdm_9p0_V200-400kmps_seed1',
        'mu_5/fdisk_0p01_lgMdm_9p0_V200-400kmps_seed2',
        'mu_5/fdisk_0p01_lgMdm_9p0_V200-400kmps_seed3',
        'mu_5/fdisk_0p01_lgMdm_9p0_V200-400kmps_seed4',
        'mu_5/fdisk_0p01_lgMdm_9p0_V200-400kmps_seed5',
        'mu_5/fdisk_0p01_lgMdm_9p0_V200-400kmps_seed6',
        'mu_5/fdisk_0p01_lgMdm_9p0_V200-400kmps_seed7',
        'mu_5/fdisk_0p01_lgMdm_9p0_V200-400kmps_seed8',
        'mu_5/fdisk_0p01_lgMdm_9p0_V200-400kmps_seed9',
        'mu_5/fdisk_0p01_lgMdm_8p5_V200-400kmps',
        'mu_5/fdisk_0p01_lgMdm_8p0_V200-400kmps',
        'mu_5/fdisk_0p01_lgMdm_7p5_V200-400kmps',
        'mu_5/fdisk_0p01_lgMdm_7p0_V200-400kmps',
    ]

    snaps_list = [
        400, 400, 400, 400, 400, 400, 400, 400, 400,
        400, 400, 101, 101, 101,
        400, 400, 400, 400, 400, 400, 400, 400, 400,
        400, 400, 101, 101, 101,
        400, 400, 400, 400, 400, 400, 400, 400, 400, 400, 400, 400, 400,
        400, 400, 101, 101, 101,
        400, 400, 400, 400, 400, 400, 400, 400, 400,
        400, 400, 101, 101, 101,
    ]

    plt.figure()

    DM = False
    _print = False

    xs = []
    ys = []

    for galaxy, snaps in zip(galaxy_name_list, snaps_list):
        # time = 8
        # snap = '{0:03d}'.format(int(snaps / T_TOT * time))
        snap = '000'

        (file_name, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
         MassStar, PosStars, VelStars, IDStars, PotStars
         ) = load_nbody.load_and_align(galaxy, snap=snap)
        # ) = load_nbody.load_snapshot(galaxy, snap=snap)

        # z_12 = np.median(np.abs(PosStars[:, 2]))
        # R_12 = np.median(np.sqrt(PosStars[:, 0]**2 + PosStars[:, 1]**2))
        # print(z_12)
        # print(R_12)
        # print(z_12/R_12)

        jz0_star = np.sum(MassStar * np.cross(PosStars, VelStars)[:, 2]) / (MassStar * len(PosStars))
        jt0_star = np.linalg.norm(np.sum(MassStar * np.cross(PosStars, VelStars), axis=0)) / (MassStar * len(PosStars))
        if DM:
            jz0_dm = np.sum(MassDM * np.cross(PosDMs, VelDMs)[:, 2]) / (MassDM * len(PosDMs))
            jt0_dm = np.linalg.norm(np.sum(MassDM * np.cross(PosDMs, VelDMs), axis=0)) / (MassDM * len(PosDMs))

        snap = str(snaps)
        (file_name, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
         MassStar, PosStars, VelStars, IDStars, PotStars
         ) = load_nbody.load_and_align(galaxy, snap=snap)
        # ) = load_nbody.load_snapshot(galaxy, snap=snap)

        jzn_star = np.sum(MassStar * np.cross(PosStars, VelStars)[:, 2]) / (MassStar * len(PosStars))
        jtn_star = np.linalg.norm(np.sum(MassStar * np.cross(PosStars, VelStars), axis=0)) / (MassStar * len(PosStars))
        if DM:
            jzn_dm = np.sum(MassDM * np.cross(PosDMs, VelDMs)[:, 2]) / (MassDM * len(PosDMs))
            jtn_dm = np.linalg.norm(np.sum(MassDM * np.cross(PosDMs, VelDMs), axis=0)) / (MassDM * len(PosDMs))

        if _print:
            print(galaxy)
            print(len(PosDMs), np.log10(len(PosDMs)))
            print(jz0_star, jzn_star)
            print(jzn_star - jz0_star)
            print(jzn_star / jz0_star)
            print()
            print(jt0_star, jtn_star)
            print(jtn_star - jt0_star)
            print(jtn_star / jt0_star)
            print()
            if DM:
                print(jz0_dm, jzn_dm)
                print(jzn_dm - jz0_dm)
                print(jzn_dm / jz0_dm)
                print()
                print(jt0_dm, jtn_dm)
                print(jtn_dm - jt0_dm)
                print(jtn_dm / jt0_dm)
                print()

        c = 'C1'
        if 'seed' in galaxy:
            c = 'C2'
        plt.errorbar(np.log10(len(PosDMs)), (jtn_star - jt0_star) / jt0_star, c='C0', marker='o')
        plt.errorbar(np.log10(len(PosDMs)), (jzn_star - jz0_star) / jz0_star, c=c, marker='s')
        if DM:
            plt.errorbar(np.log10(len(PosDMs)), (jtn_dm - jt0_dm) / jt0_dm, c='C2', marker='o')
            plt.errorbar(np.log10(len(PosDMs)), (jzn_dm - jz0_dm) / jz0_dm, c='C3', marker='s')

        xs.append(np.log10(len(PosDMs)))
        ys.append((jtn_star - jt0_star) / jt0_star)

    plt.ylabel(r'[j(t=10) - j(t=0)] / j(t=0)')
    plt.xlabel(r'$\log N_{\rm DM}$')

    xlim = [4, 6.5]
    x = np.linspace(*xlim)
    # y = lambda x, args: - args[0] / (args[1] - x)**2 #args[2]
    y = lambda x, args: - args[0] * np.exp(args[1] - x) ** args[2]

    to_min = lambda args: np.sum((ys - y(xs, args)) ** 2)
    out = minimize(to_min, x0=(1, 3.5, 1.7))
    print(out)

    plt.errorbar(x, y(x, out.x))

    plt.xlim(xlim)

    print(y(4, out.x))
    print(y(5, out.x))
    print(y(6, out.x))

    plt.savefig('../galaxies/jz_N', bbox_inches='tight')

    return

def plot_crazy_radial_projection_evolution(galaxy_name, snaps, save=False,
                                           r1kpc=2, r2kpc=4, widthkpc=10, gridn=41):

    #get IDs from ICs
    (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
     MassStar, PosStars, VelStars, IDStars, PotStars
     ) = load_nbody.load_and_align(galaxy_name, '000')
    R_stars = np.sqrt(PosStars[:, 0]**2 + PosStars[:, 1]**2)

    id_order = np.argsort(IDStars)
    R_stars = R_stars[id_order]

    ID0_inds = R_stars < r1kpc
    ID1_inds = np.logical_and(r1kpc < R_stars, R_stars < r2kpc)
    ID2_inds = r2kpc < R_stars
    
    line_of_sight_bin_edges = [np.linspace(-widthkpc, widthkpc, gridn), np.linspace(-widthkpc, widthkpc, gridn)]

    for i in range(snaps + 1):
        snap = '{0:03d}'.format(int(i))
        # read a file
        (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
         MassStar, PosStars, VelStars, IDStars, PotStars
         ) = load_nbody.load_and_align(galaxy_name, snap)

        id_order = np.argsort(IDStars)
        PosStars = PosStars[id_order]
        
        N_pop_0, _, _, _ = binned_statistic_2d(PosStars[ID0_inds, 0], PosStars[ID0_inds, 1], np.ones(np.sum(ID0_inds)),
                                               statistic='sum', bins=line_of_sight_bin_edges)
        N_pop_1, _, _, _ = binned_statistic_2d(PosStars[ID1_inds, 0], PosStars[ID1_inds, 1], np.ones(np.sum(ID1_inds)),
                                               statistic='sum', bins=line_of_sight_bin_edges)
        N_pop_2, _, _, _ = binned_statistic_2d(PosStars[ID2_inds, 0], PosStars[ID2_inds, 1], np.ones(np.sum(ID2_inds)),
                                               statistic='sum', bins=line_of_sight_bin_edges)

        N_pop_0 = N_pop_0.T
        N_pop_1 = N_pop_1.T
        N_pop_2 = N_pop_2.T

        N_tot = N_pop_0 + N_pop_1 + N_pop_2
        F0 = N_pop_0 / N_tot
        F1 = N_pop_1 / N_tot
        F2 = N_pop_2 / N_tot
        
        out = np.dstack((F0, F1, F2))
        out = np.sqrt(out)

        plt.figure()
        plt.imshow(out,
                   origin='lower', aspect=1, extent=[-widthkpc, widthkpc, -widthkpc, widthkpc])

        print(f'Done {snap}')
            
        if save:
            plt.savefig(f'../galaxies/{galaxy_name}/results/projs/snap{snap}')
            plt.close()
    
    return


###############################################################################

if __name__ == '__main__':
    # galaxy_name_list = ['mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_1/fdisk_0p01_lgMdm_7p5_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
    #                     'mu_25/fdisk_0p01_lgMdm_7p5_V200-200kmps',
    #                     'mu_1/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     'mu_25/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     'mu_1/fdisk_0p01_lgMdm_6p5_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
    #                     'mu_25/fdisk_0p01_lgMdm_6p5_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps',
    #                     'mu_25/fdisk_0p01_lgMdm_6p0_V200-200kmps',]
    # snaps_list = [400,400,400,
    #               400,400,400,
    #               101,101,101,
    #               101,101,101,
    #               101,101]
    #
    galaxy_name_list = [#'mu_5/fdisk_0p01_lgMdm_6p5_V200-50kmps',
                        'mu_1/fdisk_0p01_lgMdm_6p0_V200-50kmps',
                        'mu_1/fdisk_0p01_lgMdm_5p5_V200-50kmps',
                        'mu_1/fdisk_0p01_lgMdm_5p0_V200-50kmps',
                        'mu_1/fdisk_0p01_lgMdm_4p5_V200-50kmps',
                        'mu_1/fdisk_0p01_lgMdm_4p0_V200-50kmps',
                        'mu_5/fdisk_0p01_lgMdm_6p0_V200-50kmps',
                        'mu_5/fdisk_0p01_lgMdm_5p5_V200-50kmps',
                        'mu_5/fdisk_0p01_lgMdm_5p0_V200-50kmps',
                        'mu_5/fdisk_0p01_lgMdm_4p5_V200-50kmps',
                        'mu_5/fdisk_0p01_lgMdm_4p0_V200-50kmps',
                        'mu_25/fdisk_0p01_lgMdm_6p0_V200-50kmps',
                        'mu_25/fdisk_0p01_lgMdm_5p5_V200-50kmps',
                        'mu_25/fdisk_0p01_lgMdm_5p0_V200-50kmps',
                        'mu_25/fdisk_0p01_lgMdm_4p5_V200-50kmps',
                        'mu_25/fdisk_0p01_lgMdm_4p0_V200-50kmps',
                        'mu_1/fdisk_0p01_lgMdm_7p0_V200-100kmps',
                        'mu_1/fdisk_0p01_lgMdm_6p5_V200-100kmps',
                        'mu_1/fdisk_0p01_lgMdm_6p0_V200-100kmps',
                        'mu_1/fdisk_0p01_lgMdm_5p5_V200-100kmps',
                        'mu_1/fdisk_0p01_lgMdm_5p0_V200-100kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p0_V200-100kmps',
                        'mu_5/fdisk_0p01_lgMdm_6p5_V200-100kmps',
                        'mu_5/fdisk_0p01_lgMdm_6p0_V200-100kmps',
                        'mu_5/fdisk_0p01_lgMdm_5p5_V200-100kmps',
                        'mu_5/fdisk_0p01_lgMdm_5p0_V200-100kmps',
                        'mu_25/fdisk_0p01_lgMdm_7p0_V200-100kmps',
                        'mu_25/fdisk_0p01_lgMdm_6p5_V200-100kmps',
                        'mu_25/fdisk_0p01_lgMdm_6p0_V200-100kmps',
                        'mu_25/fdisk_0p01_lgMdm_5p5_V200-100kmps',
                        'mu_25/fdisk_0p01_lgMdm_5p0_V200-100kmps',
                        'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_1/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                        'mu_1/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_1/fdisk_0p01_lgMdm_6p5_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps',
                        'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_25/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                        'mu_25/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_25/fdisk_0p01_lgMdm_6p5_V200-200kmps',
                        'mu_25/fdisk_0p01_lgMdm_6p0_V200-200kmps',
                        'mu_1/fdisk_0p01_lgMdm_9p0_V200-400kmps',
                        'mu_1/fdisk_0p01_lgMdm_8p5_V200-400kmps',
                        'mu_1/fdisk_0p01_lgMdm_8p0_V200-400kmps',
                        'mu_1/fdisk_0p01_lgMdm_7p5_V200-400kmps',
                        'mu_1/fdisk_0p01_lgMdm_7p0_V200-400kmps',
                        'mu_5/fdisk_0p01_lgMdm_9p0_V200-400kmps',
                        'mu_5/fdisk_0p01_lgMdm_8p5_V200-400kmps',
                        'mu_5/fdisk_0p01_lgMdm_8p0_V200-400kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p5_V200-400kmps',
                        'mu_5/fdisk_0p01_lgMdm_7p0_V200-400kmps',
                        'mu_25/fdisk_0p01_lgMdm_9p0_V200-400kmps',
                        'mu_25/fdisk_0p01_lgMdm_8p5_V200-400kmps',
                        'mu_25/fdisk_0p01_lgMdm_8p0_V200-400kmps',
                        'mu_25/fdisk_0p01_lgMdm_7p5_V200-400kmps',
                        'mu_25/fdisk_0p01_lgMdm_7p0_V200-400kmps',
                        # 'mu_5_c_4/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        # 'mu_5_c_4/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_5_c_7/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_5_c_7/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_5_c_15/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        'mu_5_c_15/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        # 'mu_5_c_25/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        # 'mu_5_c_25/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_2z0_fixed_eps',
                        'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps_2z0_fixed_eps',
                        'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_4z0_fixed_eps',
                        'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps_4z0_fixed_eps',
                        ]

    snaps_list = [#101,
                  400,400,101,101,101,400,400,101,101,101,400,400,101,101,101,
                  400,400,101,101,101,400,400,101,101,101,400,400,101,101,101,
                  400,400,101,101,400,400,101,101,101,400,400,101,101,101,
                  400,400,101,101,101,400,400,101,101,101,400,400,101,101,101,
                  # 101,101,
                  400,101,400,101,
                  # 400,101,
                  400,400,400,400,
                  ]

    # galaxy_name_list = [# 'mu_5/fdisk_0p01_lgMdm_6p0_V200-50kmps',
                        # 'mu_5/fdisk_0p01_lgMdm_5p5_V200-50kmps',
                        # 'mu_5/fdisk_0p01_lgMdm_5p0_V200-50kmps',
                        # 'mu_5/fdisk_0p01_lgMdm_7p0_V200-100kmps',
                        # 'mu_5/fdisk_0p01_lgMdm_6p5_V200-100kmps',
                        # 'mu_5/fdisk_0p01_lgMdm_6p0_V200-100kmps',
                        # 'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        # 'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
                        # 'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        # 'mu_5/fdisk_0p01_lgMdm_9p0_V200-400kmps',
                        # 'mu_5/fdisk_0p01_lgMdm_8p5_V200-400kmps',
                        # 'mu_5/fdisk_0p01_lgMdm_8p0_V200-400kmps',
                        # 'mu_5_c_7/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        # 'mu_5_c_7/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed1',
                        # 'mu_5_c_7/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed2',
                        # 'mu_5_c_7/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed3',
                        # 'mu_5_c_7/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed4',
                        # 'mu_5_c_7/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed5',
                        # 'mu_5_c_7/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed6',
                        # 'mu_5_c_7/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed7',
                        # 'mu_5_c_7/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed8',
                        # 'mu_5_c_7/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed9',
                        # 'mu_5_c_7/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        # 'mu_5_c_15/fdisk_0p01_lgMdm_8p0_V200-200kmps',
                        # 'mu_5_c_15/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed1',
                        # 'mu_5_c_15/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed2',
                        # 'mu_5_c_15/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed3',
                        # 'mu_5_c_15/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed4',
                        # 'mu_5_c_15/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed5',
                        # 'mu_5_c_15/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed6',
                        # 'mu_5_c_15/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed7',
                        # 'mu_5_c_15/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed8',
                        # 'mu_5_c_15/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed9',
                        # 'mu_5_c_15/fdisk_0p01_lgMdm_7p0_V200-200kmps',
                        # ]

    # snaps_list = [400,400,101,
    #               400,400,101,
    #               400,400,101,
    #               400,400,101,
    #               400,101,400,101,]

    # galaxy_name_list = ['mu_1/fdisk_0p01_lgMdm_6p0_V200-50kmps',
    #                     'mu_1/fdisk_0p01_lgMdm_7p0_V200-100kmps',
    #                     'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_1/fdisk_0p01_lgMdm_9p0_V200-400kmps',
    #                     'mu_5_c_7/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_5_c_15/fdisk_0p01_lgMdm_8p0_V200-200kmps',]
    # #
    # snaps_list = [400,400,400,400,400,400,]

    # galaxy_name_list = ['mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed0',
    #                     'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed1',
    #                     'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed2',
    #                     'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed3',
    #                     'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed4',
    #                     'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed5',
    #                     'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed6',
    #                     'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed7',
    #                     'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed8',
    #                     'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed9',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed0',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed1',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed2',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed3',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed4',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed5',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed6',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed7',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed8',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed9',
    #                     'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed0',
    #                     'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed1',
    #                     'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed2',
    #                     'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed3',
    #                     'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed4',
    #                     'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed5',
    #                     'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed6',
    #                     'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed7',
    #                     'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed8',
    #                     'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps_seed9',
    #                     ]
    # snaps_list = [400]*30

    # galaxy_name_list = [
    #                     'mu_5/fdisk_0p01_lgMdm_6p0_V200-50kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_5p5_V200-50kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_5p0_V200-50kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_4p5_V200-50kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_4p0_V200-50kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p0_V200-100kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_6p5_V200-100kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_6p0_V200-100kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_5p5_V200-100kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_5p0_V200-100kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_9p0_V200-400kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_8p5_V200-400kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-400kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p5_V200-400kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p0_V200-400kmps',
    #                     ]
    #
    # snaps_list = [
    #               400,400,101,101,101,
    #               400,400,101,101,101,
    #               400,400,101,101,101,
    #               400,400,101,101,101,
    #               ]

    # galaxy_name_list = ['mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_25/fdisk_0p01_lgMdm_7p5_V200-200kmps',
    #                     'mu_25/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     'mu_25/fdisk_0p01_lgMdm_6p5_V200-200kmps',
    #                     'mu_25/fdisk_0p01_lgMdm_6p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps',
    #                     'mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_1/fdisk_0p01_lgMdm_7p5_V200-200kmps',
    #                     'mu_1/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     'mu_1/fdisk_0p01_lgMdm_6p5_V200-200kmps']
    # snaps_list = [400, 400, 101, 101, 101,
    #               400, 400, 101, 101, 101,
    #               400, 400, 101, 101]

    # galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps']
    # snaps_list = [400,400,101,101,101]

    # galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps']
    # snaps_list = [400,101]
    # galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps']
    # snaps_list = [400,101]

    # galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps']
    # snaps_list = [400]

    # galaxy_name_list = ['mu_5_smooth/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_5_smooth/fdisk_0p01_lgMdm_7p5_V200-200kmps',
    #                     'mu_5_smooth/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     # 'mu_5_smooth/fdisk_0p01_lgMdm_6p5_V200-200kmps',
    #                     'mu_5_smooth/fdisk_0p01_lgMdm_6p0_V200-200kmps']
    # # snaps_list = [400,400,101,101]#,101]
    # snaps_list = [101,101,101,101]#,101]

    # galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps',
    #                     'mu_5_smooth/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_5_smooth/fdisk_0p01_lgMdm_7p5_V200-200kmps',
    #                     'mu_5_smooth/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     # 'mu_5_smooth/fdisk_0p01_lgMdm_6p5_V200-200kmps',
    #                     'mu_5_smooth/fdisk_0p01_lgMdm_6p0_V200-200kmps']
    # snaps_list = [400,400,101,101,101,101,101,101,101]#,101]

    # galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps']
    # snaps_list = [400,101,101]

    save_name_append = 'diff'
    save = True


    # for galaxy_name in galaxy_name_list:
    #     print_interesting_quantities(galaxy_name)

    # # # np.seterr(invalid='raise')
    # #
    # # fig 1,2,3 projections
    # plot_projected_evolutions(save=save, name='../galaxies/projections')#+ '.pdf')
    #
    # # #fig 4 left velocity moment profiles vs Re
    # plot_poster_velocity_profiles_after_t(galaxy_name_list, snaps_list, t=5, save=save, model=False, show_dms=True,
    #                                       name='../galaxies/heated_profiles_t5_dm' + save_name_append + '.pdf')
    # plot_poster_velocity_profiles_after_t(galaxy_name_list, snaps_list, t=0, save=save, model=False, show_dms=True,
    #                                       name='../galaxies/heated_profiles_t0_dm' + save_name_append + '.pdf')
    # plot_poster_velocity_profiles_after_t(galaxy_name_list, snaps_list, t=5, save=save, model=False, show_dms=True,
    #                                       name='../galaxies/heated_profiles_t5_dm' + save_name_append + '_dm_vc.pdf')
    # plot_poster_velocity_profiles_after_t(galaxy_name_list, snaps_list, t=0, save=save, model=False, show_dms=True,
    #                                       name='../galaxies/heated_profiles_t0_dm' + save_name_append + '_dm_vc.pdf')
    # plot_poster_velocity_profiles_after_t(galaxy_name_list, snaps_list, t=5, save=save, model=False, show_dms=True,
    #                                       name='../galaxies/heated_profiles_t5_dm' + save_name_append + '_sigonly.pdf')
    # plot_poster_velocity_profiles_after_t(galaxy_name_list, snaps_list, t=0, save=save, model=False, show_dms=True,
    #                                       name='../galaxies/heated_profiles_t0_dm' + save_name_append + '_sigonly.pdf')
    
    # plot_poster_velocity_profiles_after_t(galaxy_name_list, snaps_list, t=5, save=save, model=True,
    #                                     name='../galaxies/heated_profiles_t5_smooth' + save_name_append + '.pdf')
    # for i in range(10):
    #     plot_poster_velocity_profiles_after_t_asymm_exp(galaxy_name_list, snaps_list, t=i, save=save,
    #                                           model=True, careful_model=True,
    #                                           name=f'../galaxies/heated_profiles_careful_model_t{i}_components')
        # plot_poster_velocity_profiles_after_t(galaxy_name_list, snaps_list, t=i, save=save,
        #                                       model=True, careful_model=True,
        #                                       name=f'../galaxies/heated_profiles_careful_model_t{i}')
        # plot_poster_velocity_profiles_after_t(galaxy_name_list, snaps_list, t=i, save=save,
        #                                       model=True, careful_model=True, slog_yax=True,
        #                                       name=f'../galaxies/heated_profiles_careful_model_log_t{i}')
    
    # plot_poster_velocity_profiles_after_t(galaxy_name_list, snaps_list, t=5, save=save, model=True,
    #                                     name='../galaxies/heated_profiles_t5' + save_name_append + '.pdf')
    # # # # #fig 4 right velocity moment
    # highlight_real_space(galaxy_name_list, snaps_list, save_name_append=save_name_append, save = save, model=False,
    #                       name='../galaxies/real_time_space_evolution_smooth' + save_name_append + '.pdf')
    # #
    # # # fig 5 scaling relations
    # plot_scaling_relations(save=save, name='../galaxies/scaling_relations' + '.pdf')
    # 
    # figure 6 angular momentum
    # plot_angular_momentum_evolution(save_name_append=save_name_append, save=save,
    #                                name='../galaxies/fall_relation_zoom' + '.pdf')
    # plot_angular_momentum_evolution2(save_name_append=save_name_append, save=save,
    #                                  name='../galaxies/fall_relation_zoom2' + '.pdf')
    # 
    # # # # # # # #fig 7 size modelling attempts
    # plot_shapes_global(galaxy_name_list, snaps_list, save=save,
    #                    name='../galaxies/shape_global' + '.pdf')
    #
    # #fig 8 modelled kinematic indicators (v/sigma, kappa_rot, lambda_r) vs time
    # plot_modelled_indicators(galaxy_name_list, snaps_list, save_name_append=save_name_append, save=save,
    #                          name='../galaxies/predicted_kinematics_' + save_name_append + '.pdf')

    #fig 9 j_z/j_c profile evolution
    # plot_jzonc_distributions_experiments(save_name_append='all', save=save,
    #                                      name='../galaxies/j_zonc_evolution_only_' + 'all' + '.pdf')
    # plot_jzonc_distributions_experiments(save=save,
    #                                      name='../galaxies/j_zonc_evolution_only_' + save_name_append + '.pdf')
    #
    # # # fig 10 profile dependant kinematic indicators (f(j_z/j_c>0.7) and S/T) vs time, v/sigma
    # plot_profile_dep_indicators(galaxy_name_list, snaps_list, save_name_append=save_name_append, save=save,
    #                             name='../galaxies/unpredicted_kinematics_' + save_name_append + '.pdf')
    #
    # # # # # fig A1 THE MODEL varies with V200
    # plot_time_evolution_v200(save_name_append=save_name_append, save=save,
    #                          name='../galaxies/heating_fitted_data_summary' + save_name_append + '.pdf')
    # # 
    # # # fig A2 velocity moment vs time vs halo properties
    # plot_time_evolution(save_name_append=save_name_append, save=save,
    #                     name='../galaxies/heating_fitted_data_expanded' + save_name_append + '.pdf')


    # TODO line
    #############################################################################

    # plot_kinetic_energy_change(galaxy_name_list, snaps_list,
    #                            name='../galaxies/kinetic_energy_comps_min', save_name_append=save_name_append, save=save,)

    # experiment_concentration_dependances(save_name_append=save_name_append, save=save, name='../galaxies/concentration_experiments')

    #auxillary plots
    #############################################################################

    # plot_explicit_asymm_time(galaxy_name_list, snaps_list,
    #                          save_name_append='diff', name='../galaxies/asym_term_time',
    #                          slog_yax=False, save=save)
    
    # plot_explicit_asymm_rad(galaxy_name_list, snaps_list,
    #                         save_name_append='diff', name='../galaxies/asym_term_rad',
    #                         slog_yax=False, save=save)

    # # # #fig 2 debug version
    # plot_velocity_profiles_after_t(galaxy_name_list, snaps_list, t=0.1,
    #                                 save_name_append=save_name_append,
    #                                 name='../galaxies/heated_profiles_t5' + save_name_append + '.png',
    #                                 save=save)
    # #fig 2 right velocity moment profiles vs t
    # plot_real_space(galaxy_name_list, snaps_list, save_name_append=save_name_append,
    #                 save=save, name='../galaxies/heated_evolution' + save_name_append)# + '.pdf')
    # # fig 3 debug version
    # plot_main_plot(galaxy_name_list, snaps_list, save_name_append,
    #                 save=save, name='../galaxies/heating_fitted_data_' + save_name_append)


    # # plot_poster_velocity_profiles_after_t_experiments(galaxy_name_list, snaps_list, t=5, save=save,
    # #                                     name='../galaxies/heated_profiles_t5_experiment' + save_name_append)
    
    # plot_velocity_relative_after_t(galaxy_name_list, snaps_list, t=5, save_name_append='diff', save=save,
    #                                name='../galaxies/heated_fraction_t5' + save_name_append)#+ '.pdf')

    # plot_jzonc_distributions(save_name_append=save_name_append, save=save,
    #                          name='../galaxies/j_zonc_evolution_only_' + save_name_append)# + '.pdf')

    # plot_poster_kappa(galaxy_name_list, snaps_list, save_name_append=save_name_append, save=save,
    #                          name='../galaxies/poster_kappa_' + save_name_append)
    # plot_poster_vonsigma(galaxy_name_list, snaps_list, save_name_append=save_name_append, save=save,
    #                          name='../galaxies/poster_vonsigma_' + save_name_append)
    
    ###############################################################################
    # #fig 11 morphology change vs m_DM with particular sims
    # plot_indicator_n_dependence(save_name_append=save_name_append, save=save,
    #                             name='../galaxies/indicator_n_dependance' + save_name_append + '.pdf')
    ###############################################################################

    # # fig A3 most intesne modelling
    # plot_heights(galaxy_name_list, snaps_list, save=save,
    #              name='../galaxies/shape_modelling' + '.pdf')
    # plot_heights_again(galaxy_name_list, snaps_list, save=save,
    #                    name='../galaxies/shape_modelling' + '.pdf')

    # # #TODO put Lacey and Ostriker on plot ....
    # # # #fig 8 v_phi profile evolution
    # plot_v_phi_distributions(save_name_append=save_name_append, save=save,
    #                          name='../galaxies/v_phi_evolution_' + save_name_append + '.pdf')
    # plot_v_phi_distributions_experiments(save_name_append=save_name_append, save=save,
    #                                name='../galaxies/v_phi_evolution_experiments_' + save_name_append + '.pdf')

    # # #fig 12 M_DM vs m_DM
    # plot_mass_mass_plot(save_name_append=save_name_append, save=save,
    #                     name='../galaxies/mass_mass_plot_' + save_name_append)# + '.pdf')

    ###############################################################################

    # plot_sizes(galaxy_name_list, snaps_list, save=save,
    #                    name='../galaxies/size' + '.pdf')
    #
    # ###
    # #TODO come up with explination why angular momenumt is not conserved
    # #figure A1 angular momentum
    # smooth_angular_momentum_comparison(save_name_append=save_name_append, save=save, name='../galaxies/smooth_am')# + '.pdf')

    # # # #fig A? v_phi distribution
    # plot_v_phi_profile(galaxy_name_list, snaps_list, save_name_append=save_name_append,
    #                    save=save, name='../galaxies/v_phi_distribution')

    # # fig not apendix maybe
    # surface_density_profile_evolution(galaxy_name_list, snaps_list,
    #                                   save=save, name='../galaxies/mass_profile_evolution')# + '.pdf')

    # # fig not apendix
    # plot_exponential_evolution(galaxy_name_list, snaps_list,
    #                            save=save, name='../galaxies/surface_density_evolution_rng')# + '.pdf')

    # plot_integrated_shapes(galaxy_name_list, snaps_list)

    # galaxy_names = ['mu_1/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                 'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                 'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps']
    # snap = 400

    # cumulative_angularmomentum_profile(galaxy_names, 0, save=save)
    # cumulative_angularmomentum_profile(galaxy_names, snap//4, save=save)
    # cumulative_angularmomentum_profile(galaxy_names, snap//2, save=save)
    # cumulative_angularmomentum_profile(galaxy_names, 3*snap//4, save=save)
    # cumulative_angularmomentum_profile(galaxy_names, snap-1, save=save)
    # cumulative_angularmomentum_profile(galaxy_names, snap, save=save)
    # 
    # plot_asymmetric_drift(galaxy_name_list, snaps_list, save_name_append,
    #                       save=save, name='../galaxies/asymmetric_drift')
    #                       # save=save, name='../galaxies/asymmetric_drift_evolving_exp')

    # galaxy_name = 'mu_1/fdisk_0p01_lgMdm_7p5_V200-200kmps'
    # snap = 400
    # evaluate_surface_density_profiles(galaxy_name, 0, save=save)
    # evaluate_surface_density_profiles(galaxy_name, snap//4, save=save)
    # evaluate_surface_density_profiles(galaxy_name, snap//2, save=save)
    # evaluate_surface_density_profiles(galaxy_name, 3*snap//4, save=save)
    # evaluate_surface_density_profiles(galaxy_name, snap-1, save=save)
    # evaluate_surface_density_profiles(galaxy_name, snap, save=save)

    # j_zonc_distribution_modelling()

    # make_sph_movie()

    # make_EJ_movie()
    # make_Jangle_movie()

    # galaxy_name = 'mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps'
    # bin_edges = get_bin_edges('diff', galaxy_name)
    #
    # (_, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
    #  MassStar, PosStars, VelStars, IDStars, PotStars
    #  ) = load_nbody.load_and_align(galaxy_name, '399')

    # print(bin_edges)
    #
    # R = np.sqrt(PosStars[:,0]**2, PosStars[:,1]**2)
    # print(binned_statistic(R, np.ones(len(R)), statistic='sum', bins=bin_edges)[0])
    #
    # # #
    # rho_dm = get_density_scale(bin_edges, 0,0,0,0, analytic_dm=True,
    #                            save_name_append=save_name_append, galaxy_name=galaxy_name)
    # print(rho_dm)
    # rho_dm = get_density_scale(bin_edges, PosDMs, MassDM, PosStars, MassStar, live_dm=True, analytic_dm=False,
    #                            save_name_append=save_name_append, galaxy_name=galaxy_name)
    # print(rho_dm)
    #
    # #
    # v_c = get_velocity_scale(bin_edges, 0,0,0,0,0,0,0, analytic_v_c=True,
    #                      save_name_append='diff', galaxy_name=galaxy_name)
    # print(v_c)
    # v_c = get_velocity_scale(bin_edges,
    #                      PosDMs, VelDMs, MassDM, PosStars, VelStars, MassStar, dm_v_circ=True,
    #                      save_name_append='diff', galaxy_name='')
    # print(v_c)
    #
    # #
    # sigma_dm = get_velocity_scale(bin_edges, 0,0,0,0,0,0,0, analytic_dispersion=True,
    #                      save_name_append='diff', galaxy_name=galaxy_name)
    # print(sigma_dm)
    # sigma_dm = get_velocity_scale(bin_edges,
    #                      PosDMs, VelDMs, MassDM, PosStars, VelStars, MassStar, dm_dispersion=True,
    #                      save_name_append='diff', galaxy_name='')
    # print(sigma_dm)

    # plot_time_evolution_experiments(save_name_append=save_name_append, save=save,
    #                           name='../galaxies/concentration_fits' + save_name_append)

    # experiment_fitting_constraints(save_name_append=save_name_append, save=save, name='../galaxies/fitting_constraints')

    # plot_poster_bg_projected_evolution(save=save, name='../galaxies/w_slide_bg_sat')

    # plot_poster_fg_projected_evolutions(save=save, name='../galaxies/poster_fg')  # + '.pdf')

    # plot_poster_scaling_relations(save=save, name='../galaxies/poster_scaling_relations')

    # plot_poster_morphologies(galaxy_name_list, snaps_list,
    #                          save=save, name='../galaxies/poster_morphologies')

    # # # # #fig 11 morphology change vs m_DM with particular sims
    # plot_poster_indicator_n_dependence(save_name_append=save_name_append, save=save,
    #                                    name='../galaxies/poster_indicator_n_dependance')

    # talk_plot_modelled_indicators(galaxy_name_list, snaps_list, save_name_append=save_name_append, save=save,
    #                               name='../galaxies/talk_predicted_kinematics_' + save_name_append)

    # talk_plot_scaling_relations(save=save, name='../galaxies/talk_scaling_relations')

    # plot_kappa_comparison(galaxy_name_list, snaps_list)

    # plot_angular_evolution(galaxy_name_list, snaps_list, save_name_append=save_name_append, save=save,
    #                          name='../galaxies/angular_evolution_' + save_name_append)

    # plot_v_phi_distributions_experiments(save_name_append='diff', name='', save=False)
    
    # experiment_fitting_constraints(name='', save=False, ns=6)

    # galaxy_name_list = ['mu_5/fdisk_0p01_lgMdm_8p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p5_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_6p5_V200-200kmps',
    #                     'mu_5/fdisk_0p01_lgMdm_6p0_V200-200kmps',]
    # snaps_list = [400,400,101,101,101]#,101]

    # find_am_n_dependance()
    # 
    # plot_softening_size(save=save, name='../galaxies/softening_size8.pdf')
    # plot_softening_size(save=save, name='../galaxies/softening_size7.pdf')

    plot_crazy_radial_projection_evolution('mu_5/fdisk_0p01_lgMdm_7p0_V200-200kmps', 101,
                                           save=True)

    plt.show()

    pass