#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 30 14:08:07 2020

@author: matt
"""
import os

import numpy as np
import h5py as h5

from scipy.optimize    import brentq
from scipy.optimize    import minimize
# from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.interpolate import interp1d
from scipy.interpolate import UnivariateSpline
# from scipy.signal      import savgol_filter
from scipy.stats       import binned_statistic

import matplotlib.pyplot as plt
# from   matplotlib.colors import LogNorm
import matplotlib.cm
from matplotlib.cm import viridis
from matplotlib.cm import ScalarMappable
from matplotlib.colors import Normalize
from matplotlib.legend_handler import HandlerTuple
import matplotlib.lines as lines

import splotch as splt

# import halo_calculations as halo
import load_nbody

GRAV_CONST = 4.301e4 #kpc (km/s)^2 / (10^10Msun) (source ??)
HUBBLE_CONST = 0.06766 #km/s/kpc (Planck 2018)
RHO_CRIT = 3 * HUBBLE_CONST**2 / (8 * np.pi * GRAV_CONST) #10^10Msun/kpc^3

PC_ON_M = 3.0857e16 #pc/m = kpc/km (wiki)
GYR_ON_S = 3.15576e16 #gyr/s

T_TOT = 9.778943899409334 #Gyr

def get_fiducial_radii(pos_stars):
  '''Currently only works for equal mass stars.
  '''
  rad = np.linalg.norm(pos_stars, axis=1)
  r_25 = np.percentile(rad, 25)
  r_50 = np.percentile(rad, 50)
  r_75 = np.percentile(rad, 75)

  # R = np.linalg.norm(pos_stars[:2], axis=1)
  # R_25 = np.percentile(R, 25)
  # R_50 = np.percentile(R, 50)
  # R_75 = np.percentile(R, 75)

  return(r_25, r_50, r_75)

def get_fiducial_constants(pos_dms, mass_dm, r_25, r_50, r_75, pct=0.1, less_than=False):
  '''Currently only works for equal mass stars.
  '''
  r_dms = np.linalg.norm(pos_dms[:, :2], axis=1)

  rhos = []
  vcs = []
  for r in [r_25, r_50, r_75]:
    if less_than:
      r_mask = (r_dms < r)
      rho = mass_dm * np.sum(r_mask)/ (4 / 3 * np.pi * r**3)
    else:
      rmin = r / (1+pct)
      rmax = r * (1+pct)
      r_mask = np.logical_and((r_dms > rmin), (r_dms < rmax))

      rho = mass_dm * np.sum(r_mask)/ (4 / 3 * np.pi * (rmax**3 - rmin**3))

    rhos.append(rho)

    #TODO this is assuming mass has no effect
    #TODO this is a bad assumption
    v_circ = np.sqrt(GRAV_CONST * mass_dm * np.sum(r_dms < r) / r)
    vcs.append(v_circ)

  return(rhos[0], rhos[1], rhos[2], vcs[0], vcs[1], vcs[2])

def get_cylindrical(pos_stars, vel_stars):
  '''Calculates cylindrical coordinates.
  '''
  rho = np.sqrt(pos_stars[:, 0]**2 + pos_stars[:, 1]**2)
  varphi = np.arctan2(pos_stars[:, 1], pos_stars[:, 0])
  zax = pos_stars[:, 2]

  v_rho = vel_stars[:, 0] * np.cos(varphi) + vel_stars[:, 1] * np.sin(varphi)
  v_varphi = -vel_stars[:, 0]* np.sin(varphi) + vel_stars[:, 1] * np.cos(varphi)

  # v_rho = (pos_stars[:,0] * vel_stars[:,0] + pos_stars[:,1] * vel_stars[:,1]
  #             ) / np.sqrt(pos_stars[:,0]**2 + pos_stars[:,1]**2)
  # v_varphi = (pos_stars[:,0] * vel_stars[:,1] - pos_stars[:,1] * vel_stars[:,0]
  #             ) / np.sqrt(pos_stars[:,0]**2 + pos_stars[:,1]**2)

  v_z = vel_stars[:, 2]

  return(rho, varphi, zax, v_rho, v_varphi, v_z)

def get_v_200(pos_dms, mass_dm, pos_stars, mass_star):
  '''Assume pos_dms and pos_stars are in kpc and mass_dm and mass_star are in 10^10
  Msun.
  Not tested with vector mass but probably works.
  '''
  r_dms = np.linalg.norm(pos_dms, axis=1)
  r_stars = np.linalg.norm(pos_stars, axis=1)

  # M / 4/3 r_200^3 = 200 * 3/(8 pi) * H^2/G
  # M = 100 * r_200^3 * H^2 / G
  const = HUBBLE_CONST**2 / GRAV_CONST * 100

  #Uniform Mass
  zero = lambda r_200: r_200**3 * const - (
      mass_dm * np.sum(r_dms < r_200) + mass_star * np.sum(r_stars < r_200))

  # #Vector mass
  # zero = lambda r200 : r200**3 * const - (
  #   np.sum(mass_dm[r_dms < r200]) + np.sum(mass_star[r_stars < r200]))

  #this could take a little while with large N
  r_200 = brentq(zero, (np.amin(r_dms)+10), np.amax(r_dms))

  m_200 = mass_dm * np.sum(r_dms < r_200) + mass_star * np.sum(r_stars < r_200)

  v_200 = np.sqrt(GRAV_CONST * m_200 / r_200)

  return v_200

def get_dispersion_at_r(r_stars, vels, r, p=0.1, less_than=False):
  '''Currently only works for equal mass stars.
  Gets dispersion of within +/- p% of r.
  Maybe better to go with number of point either side. Logic for that is a bit
  more messy / maybe slow for large n.
  '''
  # print(r)
  # print(10 ** np.log10(r-p))
  # print(10 ** np.log10(r+p))
  # print((1-p/100) * r)
  # print((1+p/100) * r)
  # print()

  if less_than:
    r_mask = (r_stars < r)
  else:
    rmin = r / (1+p)
    rmax = r * (1+p)
    r_mask = np.logical_and((r_stars > rmin),
                            (r_stars < rmax))

  if np.sum(r_mask) < 20:
    print('Warning: Calculating dispersion from too few stars:' + str(np.sum(r_mask)))

  if len(np.shape(vels)) == 2:
    axis = 0
  else:
    axis = None

  disp = np.std(vels[r_mask], axis=axis)

  return disp

def get_halo_constants(name, overwrite=False, less_than=False):
  '''
  '''
  if overwrite or not os.path.isfile(
          '../galaxies/'+name+'/results/zdispersions.npy'):

    #load first snap
    snap = '000'
    (file_name, mass_dm, pos_dms, vel_dms, id_dms,
     mass_star, pos_stars, vel_stars, id_stars,
     j_z_stars, j_p_stars, j_c_stars, energy_stars,
     e_interp, j_circ_interp, r_interp
     ) = load_nbody.load_and_calculate(name, snap)

    #radii
    (r_25, r_50, r_75) = get_fiducial_radii(pos_stars)
    (rho_25, rho_50, rho_75,
     v_25, v_50, v_75) = get_fiducial_constants(pos_dms, mass_dm, r_25, r_50, r_75,
                                                less_than=less_than)

    #scale free scale time
    v_200 = get_v_200(pos_dms, mass_dm, pos_stars, mass_star)
    t_c = GRAV_CONST**2 * 200 * RHO_CRIT * mass_dm / v_200**3 * PC_ON_M / GYR_ON_S

    dm_rhos = np.linalg.norm(pos_dms[:, 0:2], axis=1)

    sigma_dm_25 = np.sqrt((np.square(get_dispersion_at_r(dm_rhos, vel_dms[:, 0], r_25, less_than=less_than)) +
                           np.square(get_dispersion_at_r(dm_rhos, vel_dms[:, 1], r_25, less_than=less_than)) +
                           np.square(get_dispersion_at_r(dm_rhos, vel_dms[:, 2], r_25, less_than=less_than)))/3)
    sigma_dm_50 = np.sqrt((np.square(get_dispersion_at_r(dm_rhos, vel_dms[:, 0], r_50, less_than=less_than)) +
                           np.square(get_dispersion_at_r(dm_rhos, vel_dms[:, 1], r_50, less_than=less_than)) +
                           np.square(get_dispersion_at_r(dm_rhos, vel_dms[:, 2], r_50, less_than=less_than)))/3)
    sigma_dm_75 = np.sqrt((np.square(get_dispersion_at_r(dm_rhos, vel_dms[:, 0], r_75, less_than=less_than)) +
                           np.square(get_dispersion_at_r(dm_rhos, vel_dms[:, 1], r_75, less_than=less_than)) +
                           np.square(get_dispersion_at_r(dm_rhos, vel_dms[:, 2], r_75, less_than=less_than)))/3)

    #save halo constants
    if less_than: lt = '_less_than'
    else: lt = ''
    np.save('../galaxies/' + name + '/results/zconstants' + lt,
            np.array((v_200, t_c, r_25, r_50, r_75, rho_25, rho_50, rho_75,
                      v_25, v_50, v_75, sigma_dm_25, sigma_dm_50, sigma_dm_75)))

  else:
    if less_than: lt = '_less_than'
    else: lt = ''
    (v_200, t_c, r_25, r_50, r_75, rho_25, rho_50, rho_75,
     v_25, v_50, v_75, sigma_dm_25, sigma_dm_50, sigma_dm_75
     ) = np.load('../galaxies/' + name + '/results/zconstants' + lt + '.npy')

  return(v_200, t_c,
         r_25, r_50, r_75,
         rho_25, rho_50, rho_75,
         v_25, v_50, v_75,
         sigma_dm_25, sigma_dm_50, sigma_dm_75)

def get_analytic_hernquist_dispersion(vmax, a_h, r_h):
  '''
  '''
  gmh = 4 * a_h * vmax**2 #kpc km/s
  x_h = r_h / a_h
  y_h = x_h + 1

  #binney and tremaine eq 4.219
  disp_1d = np.sqrt(gmh / a_h * (x_h * y_h**3 * np.log(y_h/x_h) - x_h/y_h * (
      1/4 + 1/3*y_h + 1/2*y_h**2 + y_h**3)))

  return(disp_1d)

def get_dispersions(name, overwrite=False, less_than=False):
  '''
  '''
  if (('7p5' in name) or ('8p0' in name)):
    nsnaps = 401
  else:
    nsnaps = 102

  if overwrite or not os.path.isfile(
          '../galaxies/'+name+'/results/zdispersions.npy'):

    #set up arrays
    sigma_v_r_25 = np.zeros(nsnaps)
    sigma_v_r_50 = np.zeros(nsnaps)
    sigma_v_r_75 = np.zeros(nsnaps)

    sigma_v_z_25 = np.zeros(nsnaps)
    sigma_v_z_50 = np.zeros(nsnaps)
    sigma_v_z_75 = np.zeros(nsnaps)

    sigma_v_phi_25 = np.zeros(nsnaps)
    sigma_v_phi_50 = np.zeros(nsnaps)
    sigma_v_phi_75 = np.zeros(nsnaps)

    mean_v_phi_25 = np.zeros(nsnaps)
    mean_v_phi_50 = np.zeros(nsnaps)
    mean_v_phi_75 = np.zeros(nsnaps)

    #load first snapshot
    (file_name, mass_dm, pos_dms, vel_dms, id_dms, pot_dms,
                mass_star, pos_stars, vel_stars, id_stars, pot_stars
                ) = load_nbody.load_and_align(name, '000')

    #radii
    (r_25, r_50, r_75) = get_fiducial_radii(pos_stars)

    #calculate values at each time
    for i in range(nsnaps):
      if i != 0:
        snap = '{0:03d}'.format(int(i))
        #laod snapshot
        (file_name, mass_dm, pos_dms, vel_dms, id_dms, pot_dms,
                    mass_star, pos_stars, vel_stars, id_stars, pot_stars
                    ) = load_nbody.load_and_align(name, snap)

        ##old code that calculated std(v_phi - v_c)
        # (file_name, mass_dm,   pos_dms,   vel_dms,   id_dms,
        #             mass_star, pos_stars, vel_stars, id_stars,
        #   j_z_stars, j_p_stars, j_c_stars, energy_stars,
        #   e_interp, j_circ_interp, r_interp
        #   ) = load_nbody.load_and_calculate(name, snap)

        # #\tilde{v}_phi
        # v_c_interp = j_circ_interp / r_interp
        # interp = InterpolatedUnivariateSpline(r_interp, v_c_interp,
        #                                       k=1, ext='const')
        # v_c_stars = interp(rho)
        # v_tild = v_phi - v_c_stars

      #cylindrical coords
      (rho, phi, zax, v_rho, v_phi, v_z
       ) = get_cylindrical(pos_stars, vel_stars)

      #do dispersions
      vels = np.array((v_rho, v_phi, v_z)).T

      (sigma_v_r_25[i], sigma_v_phi_25[i], sigma_v_z_25[i]
       ) = get_dispersion_at_r(rho, vels, r_25, less_than=less_than)

      (sigma_v_r_50[i], sigma_v_phi_50[i], sigma_v_z_50[i],
       ) = get_dispersion_at_r(rho, vels, r_50, less_than=less_than)

      (sigma_v_r_75[i], sigma_v_phi_75[i], sigma_v_z_75[i],
       ) = get_dispersion_at_r(rho, vels, r_75, less_than=less_than)

      pct = 0.1
      if less_than:
        r_25_mask = (rho > r_25)
        r_50_mask = (rho > r_50)
        r_75_mask = (rho > r_75)
      else:
        r_25_mask = np.logical_and((rho > r_25 / (1+pct)),
                                   (rho < r_25 * (1+pct)))
        r_50_mask = np.logical_and((rho > r_50 / (1+pct)),
                                   (rho < r_50 * (1+pct)))
        r_75_mask = np.logical_and((rho > r_75 / (1+pct)),
                                   (rho < r_75 * (1+pct)))

      mean_v_phi_25[i] = np.mean(v_phi[r_25_mask])
      mean_v_phi_50[i] = np.mean(v_phi[r_50_mask])
      mean_v_phi_75[i] = np.mean(v_phi[r_75_mask])

      #for time evolution for galaxy #probably re-order at some point
      arr_to_save = np.array((sigma_v_r_25, sigma_v_phi_25, sigma_v_z_25,
                              sigma_v_r_50, sigma_v_phi_50, sigma_v_z_50,
                              sigma_v_r_75, sigma_v_phi_75, sigma_v_z_75,
                              mean_v_phi_25, mean_v_phi_50, mean_v_phi_75))

      #need to change this to a human readable format (hdf5)
      if less_than: lt = '_less_than'
      else: lt = ''
      np.save('../galaxies/' + name + '/results/zdispersions' + lt, arr_to_save)

  #load data
  else:
    if less_than: lt = '_less_than'
    else: lt = ''
    (sigma_v_r_25, sigma_v_phi_25, sigma_v_z_25,
     sigma_v_r_50, sigma_v_phi_50, sigma_v_z_50,
     sigma_v_r_75, sigma_v_phi_75, sigma_v_z_75,
     mean_v_phi_25, mean_v_phi_50, mean_v_phi_75
     ) = np.load('../galaxies/' + name + '/results/zdispersions' + lt + '.npy')

  return(sigma_v_r_25, sigma_v_phi_25, sigma_v_z_25,
         sigma_v_r_50, sigma_v_phi_50, sigma_v_z_50,
         sigma_v_r_75, sigma_v_phi_75, sigma_v_z_75,
         mean_v_phi_25, mean_v_phi_50, mean_v_phi_75)

def get_velocity_scale(name, v_200_scale=True, v_c_scale=False,
                       sigma_measured_scale=False, sigma_analytic_scale=False):
  '''
  '''
  if (v_200_scale + v_c_scale + sigma_measured_scale + sigma_analytic_scale) != 1:
    print('Warning: Have not decided velocity scaling properly! Double check.')

  (v_200, t_c,
   r_25, r_50, r_75,
   rho_25, rho_50, rho_75,
   v_c_25, v_c_50, v_c_75,
   sigma_dm_25, sigma_dm_50, sigma_dm_75
   ) = get_halo_constants(name, overwrite=False)

  if v_c_scale:
    v_25_scale = v_c_25
    v_50_scale = v_c_50
    v_75_scale = v_c_75

  if sigma_measured_scale:
    v_25_scale = sigma_dm_25
    v_50_scale = sigma_dm_50
    v_75_scale = sigma_dm_75

  if sigma_analytic_scale:
    #hernquist ananlytic dispersions
    vmax = 250 #km/s
    a_h = 32  #kpc hard coded

    disp_h_25 = get_analytic_hernquist_dispersion(vmax, a_h, r_25)
    disp_h_50 = get_analytic_hernquist_dispersion(vmax, a_h, r_50)
    disp_h_75 = get_analytic_hernquist_dispersion(vmax, a_h, r_75)

    v_25_scale = disp_h_25
    v_50_scale = disp_h_50
    v_75_scale = disp_h_75

  if v_200_scale:
    v_25_scale = v_200
    v_50_scale = v_200
    v_75_scale = v_200

  return(v_25_scale, v_50_scale, v_75_scale)

def get_upsilon(name, overwrite=False, delta=True, less_than=False,
                v_200_scale=True, v_c_scale=False,
                sigma_measured_scale=False, sigma_analytic_scale=False):
  '''
  '''

  (sigma_v_r_25, sigma_v_phi_25, sigma_v_z_25,
   sigma_v_r_50, sigma_v_phi_50, sigma_v_z_50,
   sigma_v_r_75, sigma_v_phi_75, sigma_v_z_75,
   mean_v_phi_25, mean_v_phi_50, mean_v_phi_75
   ) = get_dispersions(name, overwrite=overwrite, less_than=less_than)

  (v_200, t_c,
   r_25, r_50, r_75,
   rho_25, rho_50, rho_75,
   v_c_25, v_c_50, v_c_75,
   sigma_dm_25, sigma_dm_50, sigma_dm_75
   ) = get_halo_constants(name, overwrite=overwrite, less_than=less_than)

  sigma_v_tot_25 = np.sqrt(sigma_v_r_25**2 + sigma_v_z_25**2 + sigma_v_phi_25**2)
  sigma_v_tot_50 = np.sqrt(sigma_v_r_50**2 + sigma_v_z_50**2 + sigma_v_phi_50**2)
  sigma_v_tot_75 = np.sqrt(sigma_v_r_75**2 + sigma_v_z_75**2 + sigma_v_phi_75**2)

  (v_25_scale, v_50_scale, v_75_scale
   ) = get_velocity_scale(name, v_200_scale=v_200_scale, v_c_scale=v_c_scale,
                          sigma_measured_scale=sigma_measured_scale,
                          sigma_analytic_scale=sigma_analytic_scale)

  #calculations
  if delta:
    delta_upsilon_r_25 = (sigma_v_r_25**2 -sigma_v_r_25[0]**2) / v_25_scale**2
    delta_upsilon_z_25 = (sigma_v_z_25**2 -sigma_v_z_25[0]**2) / v_25_scale**2
    delta_upsilon_phi_25 = (sigma_v_phi_25**2 -sigma_v_phi_25[0]**2) / v_25_scale**2
    delta_upsilon_tot_25 = (sigma_v_tot_25**2 -sigma_v_tot_25[0]**2) / v_25_scale**2 /3

    delta_upsilon_r_50 = (sigma_v_r_50**2 -sigma_v_r_50[0]**2) / v_50_scale**2
    delta_upsilon_z_50 = (sigma_v_z_50**2 -sigma_v_z_50[0]**2) / v_50_scale**2
    delta_upsilon_phi_50 = (sigma_v_phi_50**2 -sigma_v_phi_50[0]**2) / v_50_scale**2
    delta_upsilon_tot_50 = (sigma_v_tot_50**2 -sigma_v_tot_50[0]**2) / v_50_scale**2 /3

    delta_upsilon_r_75 = (sigma_v_r_75**2 -sigma_v_r_75[0]**2) / v_75_scale**2
    delta_upsilon_z_75 = (sigma_v_z_75**2 -sigma_v_z_75[0]**2) / v_75_scale**2
    delta_upsilon_phi_75 = (sigma_v_phi_75**2 -sigma_v_phi_75[0]**2) / v_75_scale**2
    delta_upsilon_tot_75 = (sigma_v_tot_75**2 -sigma_v_tot_75[0]**2) / v_75_scale**2 /3

    #not squared is definately the way to go for mean v_phi!
    delta_mean_phi_25 = (mean_v_phi_25 - mean_v_phi_25[0]) / v_25_scale
    delta_mean_phi_50 = (mean_v_phi_50 - mean_v_phi_50[0]) / v_50_scale
    delta_mean_phi_75 = (mean_v_phi_75 - mean_v_phi_75[0]) / v_75_scale

    # delta_mean_phi_25 = mean_v_phi_25 / v_25_scale
    # delta_mean_phi_50 = mean_v_phi_50 / v_50_scale
    # delta_mean_phi_75 = mean_v_phi_75 / v_75_scale

    # delta_mean_phi_25 = -np.abs(mean_v_phi_25 - mean_v_phi_25[0]) / v_25_scale
    # delta_mean_phi_50 = -np.abs(mean_v_phi_50 - mean_v_phi_50[0]) / v_50_scale
    # delta_mean_phi_75 = -np.abs(mean_v_phi_75 - mean_v_phi_75[0]) / v_75_scale

    # delta_mean_phi_25 = (mean_v_phi_25 - np.amax(mean_v_phi_25)) / v_25_scale
    # delta_mean_phi_50 = (mean_v_phi_50 - np.amax(mean_v_phi_50)) / v_50_scale
    # delta_mean_phi_75 = (mean_v_phi_75 - np.amax(mean_v_phi_75)) / v_75_scale


  else:
    delta_upsilon_r_25 = sigma_v_r_25**2 / v_25_scale**2
    delta_upsilon_z_25 = sigma_v_z_25**2 / v_25_scale**2
    delta_upsilon_phi_25 = sigma_v_phi_25**2 / v_25_scale**2
    delta_upsilon_tot_25 = sigma_v_tot_25**2 / v_25_scale**2 /3

    delta_upsilon_r_50 = sigma_v_r_50**2 / v_50_scale**2
    delta_upsilon_z_50 = sigma_v_z_50**2 / v_50_scale**2
    delta_upsilon_phi_50 = sigma_v_phi_50**2 / v_50_scale**2
    delta_upsilon_tot_50 = sigma_v_tot_50**2 / v_50_scale**2 /3

    delta_upsilon_r_75 = sigma_v_r_75**2 /v_75_scale**2
    delta_upsilon_z_75 = sigma_v_z_75**2 / v_75_scale**2
    delta_upsilon_phi_75 = sigma_v_phi_75**2 / v_75_scale**2
    delta_upsilon_tot_75 = sigma_v_tot_75**2 / v_75_scale**2 /3

    delta_mean_phi_25 = mean_v_phi_25 / v_25_scale
    delta_mean_phi_50 = mean_v_phi_50 / v_50_scale
    delta_mean_phi_75 = mean_v_phi_75 / v_75_scale

  return(delta_upsilon_r_25, delta_upsilon_r_50, delta_upsilon_r_75,
         delta_upsilon_z_25, delta_upsilon_z_50, delta_upsilon_z_75,
         delta_upsilon_phi_25, delta_upsilon_phi_50, delta_upsilon_phi_75,
         delta_upsilon_tot_25, delta_upsilon_tot_50, delta_upsilon_tot_75,
         delta_mean_phi_25, delta_mean_phi_50, delta_mean_phi_75)

def get_scale_time(name, alpha=0, beta=0,
                   v_200_scale=True, v_c_scale=False,
                   sigma_measured_scale=False, sigma_analytic_scale=False):
  '''Scale free is a=0, b=0. Completely scaled is alpha=1, beta=-3,
  '''

  if (v_200_scale + v_c_scale + sigma_measured_scale + sigma_analytic_scale) != 1:
    print('Warning: Have not decided velocity scaling properly! Double check.')

  if (('7p5' in name) or ('8p0' in name)): nsnaps = 401
  else: nsnaps = 102

  (v_200, t_c,
   r_25, r_50, r_75,
   rho_25, rho_50, rho_75,
   v_c_25, v_c_50, v_c_75,
   sigma_dm_25, sigma_dm_50, sigma_dm_75
   ) = get_halo_constants(name, overwrite=False)

  time = np.linspace(0, T_TOT, nsnaps)
  tau = time * t_c

  (v_25_scale, v_50_scale, v_75_scale
   ) = get_velocity_scale(name, v_200_scale=v_200_scale, v_c_scale=v_c_scale,
                          sigma_measured_scale=sigma_measured_scale,
                          sigma_analytic_scale=sigma_analytic_scale)

  d_25 = np.power((rho_25 / (200 * RHO_CRIT)),alpha)
  d_50 = np.power((rho_50 / (200 * RHO_CRIT)),alpha)
  d_75 = np.power((rho_75 / (200 * RHO_CRIT)),alpha)

  n_25 = np.power((v_25_scale / v_200), beta)
  n_50 = np.power((v_50_scale / v_200), beta)
  n_75 = np.power((v_75_scale / v_200), beta)

  tau_25 = tau * d_25 * n_25
  tau_50 = tau * d_50 * n_50
  tau_75 = tau * d_75 * n_75

  return(tau_25, tau_50, tau_75)

def get_unsaturated_data(opoint25=0.5, plus_4=False, alpha=0, beta=0,
                         v_200_scale=True, v_c_scale=False,
                         sigma_measured_scale=False, sigma_analytic_scale=False,
                         less_than=False):
  '''Scale free is a=0, b=0. Completely scaled is alpha=1, beta=-3,
  '''

  if (v_200_scale + v_c_scale + sigma_measured_scale + sigma_analytic_scale) != 1:
    print('Warning: Have not decided velocity scaling properly! Double check.')

  names = ['mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps',
           'mu_25/fdisk_0p01_lgMdm_7p5_V200-200kmps',
           'mu_25/fdisk_0p01_lgMdm_7p0_V200-200kmps',
           'mu_25/fdisk_0p01_lgMdm_6p5_V200-200kmps',
           'mu_25/fdisk_0p01_lgMdm_6p0_V200-200kmps']

  stacked_z_25 = np.zeros(0)
  stacked_z_50 = np.zeros(0)
  stacked_z_75 = np.zeros(0)
  times_z_25 = np.zeros(0)
  times_z_50 = np.zeros(0)
  times_z_75 = np.zeros(0)

  stacked_r_25 = np.zeros(0)
  stacked_r_50 = np.zeros(0)
  stacked_r_75 = np.zeros(0)
  times_r_25 = np.zeros(0)
  times_r_50 = np.zeros(0)
  times_r_75 = np.zeros(0)

  stacked_p_25 = np.zeros(0)
  stacked_p_50 = np.zeros(0)
  stacked_p_75 = np.zeros(0)
  times_p_25 = np.zeros(0)
  times_p_50 = np.zeros(0)
  times_p_75 = np.zeros(0)

  stacked_t_25 = np.zeros(0)
  stacked_t_50 = np.zeros(0)
  stacked_t_75 = np.zeros(0)
  times_t_25 = np.zeros(0)
  times_t_50 = np.zeros(0)
  times_t_75 = np.zeros(0)

  stacked_m_25 = np.zeros(0)
  stacked_m_50 = np.zeros(0)
  stacked_m_75 = np.zeros(0)
  times_m_25 = np.zeros(0)
  times_m_50 = np.zeros(0)
  times_m_75 = np.zeros(0)

  for name in names:

    (delta_upsilon_r_25, delta_upsilon_r_50, delta_upsilon_r_75,
     delta_upsilon_z_25, delta_upsilon_z_50, delta_upsilon_z_75,
     delta_upsilon_p_25, delta_upsilon_p_50, delta_upsilon_p_75,
     delta_upsilon_t_25, delta_upsilon_t_50, delta_upsilon_t_75,
     delta_mean_phi_25, delta_mean_phi_50, delta_mean_phi_75
     ) = get_upsilon(name, delta=True,
                     v_200_scale=v_200_scale, v_c_scale=v_c_scale,
                     sigma_measured_scale=sigma_measured_scale,
                     sigma_analytic_scale=sigma_analytic_scale,
                     less_than=less_than)

    (tau_25, tau_50, tau_75
     ) = get_scale_time(name, alpha=alpha, beta=beta,
                        v_200_scale=v_200_scale, v_c_scale=v_c_scale,
                        sigma_measured_scale=sigma_measured_scale,
                        sigma_analytic_scale=sigma_analytic_scale)

    # small = 1e-10
    small = 0.01
    #cut off saturated values
    z_25_not_sat_mask = np.logical_and((delta_upsilon_z_25 < opoint25),
                                       (delta_upsilon_z_25 > small))
    z_50_not_sat_mask = np.logical_and((delta_upsilon_z_50 < opoint25),
                                       (delta_upsilon_z_50 > small))
    z_75_not_sat_mask = np.logical_and((delta_upsilon_z_75 < opoint25),
                                       (delta_upsilon_z_75 > small))

    r_25_not_sat_mask = np.logical_and((delta_upsilon_r_25 < opoint25),
                                       (delta_upsilon_r_25 > small))
    r_50_not_sat_mask = np.logical_and((delta_upsilon_r_50 < opoint25),
                                       (delta_upsilon_r_50 > small))
    r_75_not_sat_mask = np.logical_and((delta_upsilon_r_75 < opoint25),
                                       (delta_upsilon_r_75 > small))

    p_25_not_sat_mask = np.logical_and((delta_upsilon_p_25 < opoint25),
                                       (delta_upsilon_p_25 > small))
    p_50_not_sat_mask = np.logical_and((delta_upsilon_p_50 < opoint25),
                                       (delta_upsilon_p_50 > small))
    p_75_not_sat_mask = np.logical_and((delta_upsilon_p_75 < opoint25),
                                       (delta_upsilon_p_75 > small))

    t_25_not_sat_mask = np.logical_and((delta_upsilon_t_25 < opoint25),
                                       (delta_upsilon_t_25 > small))
    t_50_not_sat_mask = np.logical_and((delta_upsilon_t_50 < opoint25),
                                       (delta_upsilon_t_50 > small))
    t_75_not_sat_mask = np.logical_and((delta_upsilon_t_75 < opoint25),
                                       (delta_upsilon_t_75 > small))

    m_25_not_sat_mask = np.logical_and(np.logical_and((delta_mean_phi_25 > -opoint25),
                                                      (delta_mean_phi_25 < -small)),
                                       (np.log10(tau_25) > -6))
    m_50_not_sat_mask = np.logical_and(np.logical_and((delta_mean_phi_50 > -opoint25),
                                                      (delta_mean_phi_50 < -small)),
                                       (np.log10(tau_50) > -6))
    m_75_not_sat_mask = np.logical_and(np.logical_and((delta_mean_phi_75 > -opoint25),
                                                      (delta_mean_phi_75 < -small)),
                                       (np.log10(tau_75) > -6))

    #stack unsaturead values and times
    stacked_z_25 = np.hstack((stacked_z_25, delta_upsilon_z_25[z_25_not_sat_mask]))
    times_z_25 = np.hstack((times_z_25, tau_25[z_25_not_sat_mask]))
    stacked_z_50 = np.hstack((stacked_z_50, delta_upsilon_z_50[z_50_not_sat_mask]))
    times_z_50 = np.hstack((times_z_50, tau_50[z_50_not_sat_mask]))
    stacked_z_75 = np.hstack((stacked_z_75, delta_upsilon_z_75[z_75_not_sat_mask]))
    times_z_75 = np.hstack((times_z_75, tau_75[z_75_not_sat_mask]))

    stacked_r_25 = np.hstack((stacked_r_25, delta_upsilon_r_25[r_25_not_sat_mask]))
    times_r_25 = np.hstack((times_r_25, tau_25[r_25_not_sat_mask]))
    stacked_r_50 = np.hstack((stacked_r_50, delta_upsilon_r_50[r_50_not_sat_mask]))
    times_r_50 = np.hstack((times_r_50, tau_50[r_50_not_sat_mask]))
    stacked_r_75 = np.hstack((stacked_r_75, delta_upsilon_r_75[r_75_not_sat_mask]))
    times_r_75 = np.hstack((times_r_75, tau_75[r_75_not_sat_mask]))

    stacked_p_25 = np.hstack((stacked_p_25, delta_upsilon_p_25[p_25_not_sat_mask]))
    times_p_25 = np.hstack((times_p_25, tau_25[p_25_not_sat_mask]))
    stacked_p_50 = np.hstack((stacked_p_50, delta_upsilon_p_50[p_50_not_sat_mask]))
    times_p_50 = np.hstack((times_p_50, tau_50[p_50_not_sat_mask]))
    stacked_p_75 = np.hstack((stacked_p_75, delta_upsilon_p_75[p_75_not_sat_mask]))
    times_p_75 = np.hstack((times_p_75, tau_75[p_75_not_sat_mask]))

    stacked_t_25 = np.hstack((stacked_t_25, delta_upsilon_t_25[t_25_not_sat_mask]))
    times_t_25 = np.hstack((times_t_25, tau_25[t_25_not_sat_mask]))
    stacked_t_50 = np.hstack((stacked_t_50, delta_upsilon_t_50[t_50_not_sat_mask]))
    times_t_50 = np.hstack((times_t_50, tau_50[t_50_not_sat_mask]))
    stacked_t_75 = np.hstack((stacked_t_75, delta_upsilon_t_75[t_75_not_sat_mask]))
    times_t_75 = np.hstack((times_t_75, tau_75[t_75_not_sat_mask]))

    stacked_m_25 = np.hstack((stacked_m_25, delta_mean_phi_25[m_25_not_sat_mask]))
    times_m_25 = np.hstack((times_m_25, tau_25[m_25_not_sat_mask]))
    stacked_m_50 = np.hstack((stacked_m_50, delta_mean_phi_50[m_50_not_sat_mask]))
    times_m_50 = np.hstack((times_m_50, tau_50[m_50_not_sat_mask]))
    stacked_m_75 = np.hstack((stacked_m_75, delta_mean_phi_75[m_75_not_sat_mask]))
    times_m_75 = np.hstack((times_m_75, tau_75[m_75_not_sat_mask]))

  if plus_4:
    times_z_25 *= 1e4
    times_z_50 *= 1e4
    times_z_75 *= 1e4

    times_r_25 *= 1e4
    times_r_50 *= 1e4
    times_r_75 *= 1e4

    times_p_25 *= 1e4
    times_p_50 *= 1e4
    times_p_75 *= 1e4

    times_t_25 *= 1e4
    times_t_50 *= 1e4
    times_t_75 *= 1e4

    times_m_25 *= 1e4
    times_m_50 *= 1e4
    times_m_75 *= 1e4

  return(stacked_z_25, times_z_25, stacked_z_50, times_z_50, stacked_z_75, times_z_75,
         stacked_r_25, times_r_25, stacked_r_50, times_r_50, stacked_r_75, times_r_75,
         stacked_p_25, times_p_25, stacked_p_50, times_p_50, stacked_p_75, times_p_75,
         stacked_t_25, times_t_25, stacked_t_50, times_t_50, stacked_t_75, times_t_75,
         stacked_m_25, times_m_25, stacked_m_50, times_m_50, stacked_m_75, times_m_75)

###############################################################################

def plot_heating_grid(save=True, overwrite=False, delta=True):
  '''
  '''
  names = ['mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps',
           'mu_25/fdisk_0p01_lgMdm_7p5_V200-200kmps',
           'mu_25/fdisk_0p01_lgMdm_7p0_V200-200kmps',
           'mu_25/fdisk_0p01_lgMdm_6p5_V200-200kmps',
           'mu_25/fdisk_0p01_lgMdm_6p0_V200-200kmps']

  colors = {'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps':'C0',
            'mu_25/fdisk_0p01_lgMdm_7p5_V200-200kmps':'C1',
            'mu_25/fdisk_0p01_lgMdm_7p0_V200-200kmps':'C2',
            'mu_25/fdisk_0p01_lgMdm_6p5_V200-200kmps':'C3',
            'mu_25/fdisk_0p01_lgMdm_6p0_V200-200kmps':'C4'}

  #set up plot
  fig, axs = plt.subplots(nrows=5, ncols=3, sharex=True, sharey=True,
                          figsize=(10, 15))
  fig.subplots_adjust(hspace=0.03, wspace=0.03)

  for name in names:
    (delta_upsilon_r_25, delta_upsilon_r_50, delta_upsilon_r_75,
     delta_upsilon_z_25, delta_upsilon_z_50, delta_upsilon_z_75,
     delta_upsilon_phi_25, delta_upsilon_phi_50, delta_upsilon_phi_75,
     delta_upsilon_tot_25, delta_upsilon_tot_50, delta_upsilon_tot_75,
     delta_mean_phi_25, delta_mean_phi_50, delta_mean_phi_75
     ) = get_upsilon(name, overwrite, delta, v_200_scale=True)

    (tau_25, tau_50, tau_75
     ) = get_scale_time(name, alpha=0, beta=0, v_200_scale=True)

    #points on plot
    if delta:
      axs[0, 0].errorbar(np.log10(tau_25)+4, np.log10(delta_upsilon_z_25), ls='',
                          fmt='.', alpha=0.2, c=colors[name])
      axs[0, 1].errorbar(np.log10(tau_50)+4, np.log10(delta_upsilon_z_50), ls='',
                          fmt='.', alpha=0.2, c=colors[name])
      axs[0, 2].errorbar(np.log10(tau_75)+4, np.log10(delta_upsilon_z_75), ls='',
                          fmt='.', alpha=0.2, c=colors[name])

      axs[1, 0].errorbar(np.log10(tau_25)+4, np.log10(delta_upsilon_r_25), ls='',
                          fmt='.', alpha=0.2, c=colors[name])
      axs[1, 1].errorbar(np.log10(tau_50)+4, np.log10(delta_upsilon_r_50), ls='',
                          fmt='.', alpha=0.2, c=colors[name])
      axs[1, 2].errorbar(np.log10(tau_75)+4, np.log10(delta_upsilon_r_75), ls='',
                          fmt='.', alpha=0.2, c=colors[name])

      axs[2, 0].errorbar(np.log10(tau_25)+4, np.log10(delta_upsilon_phi_25), ls='',
                          fmt='.', alpha=0.2, c=colors[name])
      axs[2, 1].errorbar(np.log10(tau_50)+4, np.log10(delta_upsilon_phi_50), ls='',
                          fmt='.', alpha=0.2, c=colors[name])
      axs[2, 2].errorbar(np.log10(tau_75)+4, np.log10(delta_upsilon_phi_75), ls='',
                          fmt='.', alpha=0.2, c=colors[name])

      axs[3, 0].errorbar(np.log10(tau_25)+4, np.log10(delta_upsilon_tot_25), ls='',
                          fmt='.', alpha=0.2, c=colors[name])
      axs[3, 1].errorbar(np.log10(tau_50)+4, np.log10(delta_upsilon_tot_50), ls='',
                          fmt='.', alpha=0.2, c=colors[name])
      axs[3, 2].errorbar(np.log10(tau_75)+4, np.log10(delta_upsilon_tot_75), ls='',
                          fmt='.', alpha=0.2, c=colors[name])

      axs[4, 0].errorbar(np.log10(tau_25)+4, np.log10(-delta_mean_phi_25), ls='',
                         fmt='.', alpha=0.2, c=colors[name])
      axs[4, 1].errorbar(np.log10(tau_50)+4, np.log10(-delta_mean_phi_50), ls='',
                         fmt='.', alpha=0.2, c=colors[name])
      axs[4, 2].errorbar(np.log10(tau_75)+4, np.log10(-delta_mean_phi_75), ls='',
                         fmt='.', alpha=0.2, c=colors[name])

  (v_200, t_c,
   r_25, r_50, r_75,
   rho_25, rho_50, rho_75,
   v_c_25, v_c_50, v_c_75,
   sigma_dm_25, sigma_dm_50, sigma_dm_75
   ) = get_halo_constants(name, overwrite=True)

  #hernquist ananlytic dispersions
  vmax = 250 #km/s
  a_h = 32  #kpc hard coded

  disp_h_25 = get_analytic_hernquist_dispersion(vmax, a_h, r_25)
  disp_h_50 = get_analytic_hernquist_dispersion(vmax, a_h, r_50)
  disp_h_75 = get_analytic_hernquist_dispersion(vmax, a_h, r_75)

  log_sigma_h_25 = np.log10(disp_h_25**2 / v_200**2)
  log_sigma_h_50 = np.log10(disp_h_50**2 / v_200**2)
  log_sigma_h_75 = np.log10(disp_h_75**2 / v_200**2)

  for i in range(4):
    axs[i, 0].errorbar([-5, 2], [log_sigma_h_25, log_sigma_h_25], c='grey',
                       ls=':', linewidth=4)
    axs[i, 1].errorbar([-5, 2], [log_sigma_h_50, log_sigma_h_50], c='grey',
                       ls=':', linewidth=4)
    axs[i, 2].errorbar([-5, 2], [log_sigma_h_75, log_sigma_h_75], c='grey',
                       ls=':', linewidth=4)

  (stacked_z_25, times_z_25, stacked_z_50, times_z_50, stacked_z_75, times_z_75,
   stacked_r_25, times_r_25, stacked_r_50, times_r_50, stacked_r_75, times_r_75,
   stacked_p_25, times_p_25, stacked_p_50, times_p_50, stacked_p_75, times_p_75,
   stacked_t_25, times_t_25, stacked_t_50, times_t_50, stacked_t_75, times_t_75,
   stacked_m_25, times_m_25, stacked_m_50, times_m_50, stacked_m_75, times_m_75
   ) = get_unsaturated_data(opoint25=0.25, plus_4=True, v_200_scale=True)

  def likely_line(args, x_0, y_0, x_1, y_1, x_2, y_2):
    '''likelyhood for 3 lines having the same gradient and different intercepts
    just with least squares in the y direction.
    Should probably just be a lambda function...
    '''
    (grad, c_0, c_1, c_2) = args

    out = (np.sum(np.square(y_0 - (grad*x_0 + c_0))) +
           np.sum(np.square(y_1 - (grad*x_1 + c_1))) +
           np.sum(np.square(y_2 - (grad*x_2 + c_2))))

    return out

  #data to fit
  data_z = (np.log10(times_z_25), np.log10(stacked_z_25),
            np.log10(times_z_50), np.log10(stacked_z_50),
            np.log10(times_z_75), np.log10(stacked_z_75))
  data_r = (np.log10(times_r_25), np.log10(stacked_r_25),
            np.log10(times_r_50), np.log10(stacked_r_50),
            np.log10(times_r_75), np.log10(stacked_r_75))
  data_p = (np.log10(times_p_25), np.log10(stacked_p_25),
            np.log10(times_p_50), np.log10(stacked_p_50),
            np.log10(times_p_75), np.log10(stacked_p_75))
  data_t = (np.log10(times_t_25), np.log10(stacked_t_25),
            np.log10(times_t_50), np.log10(stacked_t_50),
            np.log10(times_t_75), np.log10(stacked_t_75))
  data_m = (np.log10(times_m_25), np.log10(-stacked_m_25),
            np.log10(times_m_50), np.log10(-stacked_m_50),
            np.log10(times_m_75), np.log10(-stacked_m_75))

  #do the fit
  fit_z = minimize(likely_line, (1, 0.25, 0, -0.25), args=data_z)
  fit_r = minimize(likely_line, (1, 0.25, 0, -0.25), args=data_r)
  fit_p = minimize(likely_line, (1, 0.25, 0, -0.25), args=data_p)
  fit_t = minimize(likely_line, (1, 0.25, 0, -0.25), args=data_t)
  fit_m = minimize(likely_line, (1, 0.25, 0, -0.25), args=data_m)

  x = np.linspace(-5, 1)
  onep5 = 1.5

  #power law fits
  yz0 = fit_z.x[0] * x + fit_z.x[1]
  yz1 = fit_z.x[0] * x + fit_z.x[2]
  yz2 = fit_z.x[0] * x + fit_z.x[3]
  yr0 = fit_r.x[0] * x + fit_r.x[1]
  yr1 = fit_r.x[0] * x + fit_r.x[2]
  yr2 = fit_r.x[0] * x + fit_r.x[3]
  yp0 = fit_p.x[0] * x + fit_p.x[1]
  yp1 = fit_p.x[0] * x + fit_p.x[2]
  yp2 = fit_p.x[0] * x + fit_p.x[3]
  yt0 = (yz0 + yr0 + yp0) / 3
  yt1 = (yz1 + yr1 + yp1) / 3
  yt2 = (yz2 + yr2 + yp2) / 3
  ym0 = fit_m.x[0] * x + fit_m.x[1]
  ym1 = fit_m.x[0] * x + fit_m.x[2]
  ym2 = fit_m.x[0] * x + fit_m.x[3]

  axs[0, 0].errorbar(x, yz0, c='k')
  axs[0, 1].errorbar(x, yz1, c='k')
  axs[0, 2].errorbar(x, yz2, c='k')

  axs[1, 0].errorbar(x, yr0, c='k')
  axs[1, 1].errorbar(x, yr1, c='k')
  axs[1, 2].errorbar(x, yr2, c='k')

  axs[2, 0].errorbar(x, yp0, c='k')
  axs[2, 1].errorbar(x, yp1, c='k')
  axs[2, 2].errorbar(x, yp2, c='k')

  axs[3, 0].errorbar(x, yt0, c='k')
  axs[3, 1].errorbar(x, yt1, c='k')
  axs[3, 2].errorbar(x, yt2, c='k')

  axs[4, 0].errorbar(x, ym0, c='k')
  axs[4, 1].errorbar(x, ym1, c='k')
  axs[4, 2].errorbar(x, ym2, c='k')

  #saturated fitss
  upsilon2_dm_25 = sigma_dm_25**2 / v_200**2
  upsilon2_dm_50 = sigma_dm_50**2 / v_200**2
  upsilon2_dm_75 = sigma_dm_75**2 / v_200**2

  delta_25 = rho_25 / (200 * RHO_CRIT)
  delta_50 = rho_50 / (200 * RHO_CRIT)
  delta_75 = rho_75 / (200 * RHO_CRIT)

  v_25 = sigma_dm_25 / v_200
  v_50 = sigma_dm_50 / v_200
  v_75 = sigma_dm_75 / v_200

  (sigma_v_r_25, sigma_v_phi_25, sigma_v_z_25,
   sigma_v_r_50, sigma_v_phi_50, sigma_v_z_50,
   sigma_v_r_75, sigma_v_phi_75, sigma_v_z_75,
   mean_v_phi_25, mean_v_phi_50, mean_v_phi_75
   ) = get_dispersions(names[-1], overwrite=False)

  delta_upsilon_dm_z_25 = upsilon2_dm_25 - sigma_v_z_25[0]**2 / v_200**2
  delta_upsilon_dm_z_50 = upsilon2_dm_50 - sigma_v_z_50[0]**2 / v_200**2
  delta_upsilon_dm_z_75 = upsilon2_dm_75 - sigma_v_z_75[0]**2 / v_200**2

  delta_upsilon_dm_r_25 = upsilon2_dm_25 - sigma_v_r_25[0]**2 / v_200**2
  delta_upsilon_dm_r_50 = upsilon2_dm_50 - sigma_v_r_50[0]**2 / v_200**2
  delta_upsilon_dm_r_75 = upsilon2_dm_75 - sigma_v_r_75[0]**2 / v_200**2

  delta_upsilon_dm_p_25 = upsilon2_dm_25 - sigma_v_phi_25[0]**2 / v_200**2
  delta_upsilon_dm_p_50 = upsilon2_dm_50 - sigma_v_phi_50[0]**2 / v_200**2
  delta_upsilon_dm_p_75 = upsilon2_dm_75 - sigma_v_phi_75[0]**2 / v_200**2

  # delta_upsilon_dm_t_25 = (delta_upsilon_dm_z_25 + delta_upsilon_dm_r_25 +
  #                          delta_upsilon_dm_p_25)
  # delta_upsilon_dm_t_50 = (delta_upsilon_dm_z_50 + delta_upsilon_dm_r_50 +
  #                          delta_upsilon_dm_p_50)
  # delta_upsilon_dm_t_75 = (delta_upsilon_dm_z_75 + delta_upsilon_dm_r_75 +
  #                          delta_upsilon_dm_p_75)

  # delta_upsilon_dm_m_25 = v_c_25 / v_200
  # delta_upsilon_dm_m_50 = v_c_50 / v_200
  # delta_upsilon_dm_m_75 = v_c_75 / v_200

  delta_upsilon_dm_m_25 = np.amax(mean_v_phi_25) / v_200
  delta_upsilon_dm_m_50 = np.amax(mean_v_phi_50) / v_200
  delta_upsilon_dm_m_75 = np.amax(mean_v_phi_75) / v_200

  # log_v_c_h_25 = np.log10(v_c_25 / v_200)
  # log_v_c_h_50 = np.log10(v_c_50 / v_200)
  # log_v_c_h_75 = np.log10(v_c_75 / v_200)

  log_v_c_h_25 = np.log10(delta_upsilon_dm_m_25)
  log_v_c_h_50 = np.log10(delta_upsilon_dm_m_50)
  log_v_c_h_75 = np.log10(delta_upsilon_dm_m_75)

  axs[4, 0].errorbar([-5, 2], [log_v_c_h_25, log_v_c_h_25], c='grey',
                     ls=':', linewidth=4)
  axs[4, 1].errorbar([-5, 2], [log_v_c_h_50, log_v_c_h_50], c='grey',
                     ls=':', linewidth=4)
  axs[4, 2].errorbar([-5, 2], [log_v_c_h_75, log_v_c_h_75], c='grey',
                     ls=':', linewidth=4)

  hardcode = False

  if hardcode:
    #hard code best fit values
    # alpha_z  = 0.6617
    # beta_z   = 0
    # gamma_z  = 0.9956
    # const_z  = 1.9642

    # alpha_r  = 0.7519
    # beta_r   = 0
    # gamma_r  = 0.8563
    # const_r  = 1.2774

    # alpha_p  = 0.9528
    # beta_p   = 0
    # gamma_p  = 1.0466
    # const_p  = 1.602

    # alpha_m  = 0.5256
    # beta_m   = 0
    # gamma_m  = 0.8226
    # const_m  = 1.91014

    alpha_z  = 0.545155
    beta_z   = 0.082628
    gamma_z  = 1 -0.004426
    const_z  = 2.1907

    alpha_r  = 0.652506
    beta_r   = 1.019760
    gamma_r  = 1 -0.143716
    const_r  = 1.4352

    alpha_p  = 0.820837
    beta_p   = 1.125597
    gamma_p  = 1 +0.046630
    const_p  = 1.8205

    alpha_m  = 0.531549
    beta_m   = 2.878927
    gamma_m  = 1 -0.177431
    const_m  = 1.7837

    print('const, alpha, beta, gamma')
    print('z: %0.6f, %0.6f, %0.6f, %0.6f'%(const_z, alpha_z, beta_z, gamma_z-1))
    print('R: %0.6f, %0.6f, %0.6f, %0.6f'%(const_r, alpha_r, beta_r, gamma_r-1))
    print('p: %0.6f, %0.6f, %0.6f, %0.6f'%(const_p, alpha_p, beta_p, gamma_p-1))
    # print('t: %0.6f, %0.6f, %0.6f, %0.6f'%(const_t, alpha_t, beta_t, gamma_t))
    print('m: %0.6f, %0.6f, %0.6f, %0.6f'%(const_m, alpha_m, beta_m, gamma_m-1))

  else:
    (const_z, alpha_z, beta_z, gamma_z,
     const_r, alpha_r, beta_r, gamma_r,
     const_p, alpha_p, beta_p, gamma_p,
     const_t, alpha_t, beta_t, gamma_t,
     const_m, alpha_m, beta_m, gamma_m
     ) = evaluate_alpha_beta_gamma(save=False, plot=False)

    print('const, alpha, beta, gamma')
    print('z: %0.6f, %0.6f, %0.6f, %0.6f'%(const_z, alpha_z, beta_z, gamma_z))
    print('R: %0.6f, %0.6f, %0.6f, %0.6f'%(const_r, alpha_r, beta_r, gamma_r))
    print('p: %0.6f, %0.6f, %0.6f, %0.6f'%(const_p, alpha_p, beta_p, gamma_p))
    print('t: %0.6f, %0.6f, %0.6f, %0.6f'%(const_t, alpha_t, beta_t, gamma_t))
    print('m: %0.6f, %0.6f, %0.6f, %0.6f'%(const_m, alpha_m, beta_m, gamma_m))

    gamma_z += 1
    gamma_r += 1
    gamma_p += 1
    gamma_t += 1
    gamma_m += 1

  print(v_25)
  print(v_50)
  print(v_75)

  print(delta_25)
  print(delta_50)
  print(delta_75)

  print(v_200)

  print(delta_upsilon_dm_m_25)
  print(delta_upsilon_dm_m_50)
  print(delta_upsilon_dm_m_75)

  #find all the saturation times
  tau_max_z_25 = np.power(delta_upsilon_dm_z_25 / (np.power(
    10, const_z) * np.power(delta_25, alpha_z) * np.power(v_25, beta_z)), 1/gamma_z)
  tau_max_z_50 = np.power(delta_upsilon_dm_z_50 / (np.power(
    10, const_z) * np.power(delta_50, alpha_z) * np.power(v_50, beta_z)), 1/gamma_z)
  tau_max_z_75 = np.power(delta_upsilon_dm_z_75 / (np.power(
    10, const_z) * np.power(delta_75, alpha_z) * np.power(v_75, beta_z)), 1/gamma_z)

  tau_max_r_25 = np.power(delta_upsilon_dm_r_25 / (np.power(
    10, const_r) * np.power(delta_25, alpha_r) * np.power(v_25, beta_r)), 1/gamma_r)
  tau_max_r_50 = np.power(delta_upsilon_dm_r_50 / (np.power(
    10, const_r) * np.power(delta_50, alpha_r) * np.power(v_50, beta_r)), 1/gamma_r)
  tau_max_r_75 = np.power(delta_upsilon_dm_r_75 / (np.power(
    10, const_r) * np.power(delta_75, alpha_r) * np.power(v_75, beta_r)), 1/gamma_r)

  tau_max_p_25 = np.power(delta_upsilon_dm_p_25 / (np.power(
    10, const_p) * np.power(delta_25, alpha_p) * np.power(v_25, beta_p)), 1/gamma_p)
  tau_max_p_50 = np.power(delta_upsilon_dm_p_50 / (np.power(
    10, const_p) * np.power(delta_50, alpha_p) * np.power(v_50, beta_p)), 1/gamma_p)
  tau_max_p_75 = np.power(delta_upsilon_dm_p_75 / (np.power(
    10, const_p) * np.power(delta_75, alpha_p) * np.power(v_75, beta_p)), 1/gamma_p)

  tau_max_m_25 = np.power(delta_upsilon_dm_m_25 / (np.power(
    10, const_m) * np.power(delta_25, alpha_m) * np.power(v_25, beta_m)), 1/gamma_m)
  tau_max_m_50 = np.power(delta_upsilon_dm_m_50 / (np.power(
    10, const_m) * np.power(delta_50, alpha_m) * np.power(v_50, beta_m)), 1/gamma_m)
  tau_max_m_75 = np.power(delta_upsilon_dm_m_75 / (np.power(
    10, const_m) * np.power(delta_75, alpha_m) * np.power(v_75, beta_m)), 1/gamma_m)

  print()
  print()
  print(tau_max_m_25)
  print()
  print()

  #path that it follows
  y_sat_z0 = delta_upsilon_dm_z_25 * (1 - np.exp(-np.power(10**(x-4) / tau_max_z_25,
                                                 gamma_z)))
  y_sat_z1 = delta_upsilon_dm_z_50 * (1 - np.exp(-np.power(10**(x-4) / tau_max_z_50,
                                                 gamma_z)))
  y_sat_z2 = delta_upsilon_dm_z_75 * (1 - np.exp(-np.power(10**(x-4) / tau_max_z_75,
                                                 gamma_z)))

  y_sat_r0 = delta_upsilon_dm_r_25 * (1 - np.exp(-np.power(10**(x-4) / tau_max_r_25,
                                                 gamma_r)))
  y_sat_r1 = delta_upsilon_dm_r_50 * (1 - np.exp(-np.power(10**(x-4) / tau_max_r_50,
                                                 gamma_r)))
  y_sat_r2 = delta_upsilon_dm_r_75 * (1 - np.exp(-np.power(10**(x-4) / tau_max_r_75,
                                                 gamma_r)))

  y_sat_p0 = delta_upsilon_dm_p_25 * (1 - np.exp(-np.power(10**(x-4) / tau_max_p_25,
                                                 gamma_p)))
  y_sat_p1 = delta_upsilon_dm_p_50 * (1 - np.exp(-np.power(10**(x-4) / tau_max_p_50,
                                                 gamma_p)))
  y_sat_p2 = delta_upsilon_dm_p_75 * (1 - np.exp(-np.power(10**(x-4) / tau_max_p_75,
                                                 gamma_p)))

  y_sat_m0 = delta_upsilon_dm_m_25 * (1 - np.exp(-np.power(10**(x-4) / tau_max_m_25,
                                                 gamma_m)))
  y_sat_m1 = delta_upsilon_dm_m_50 * (1 - np.exp(-np.power(10**(x-4) / tau_max_m_50,
                                                 gamma_m)))
  y_sat_m2 = delta_upsilon_dm_m_75 * (1 - np.exp(-np.power(10**(x-4) / tau_max_m_75,
                                                 gamma_m)))

  #plot saturated times
  axs[0, 0].errorbar(x, np.log10(y_sat_z0), c='k', ls='--')
  axs[0, 1].errorbar(x, np.log10(y_sat_z1), c='k', ls='--')
  axs[0, 2].errorbar(x, np.log10(y_sat_z2), c='k', ls='--')

  axs[1, 0].errorbar(x, np.log10(y_sat_r0), c='k', ls='--')
  axs[1, 1].errorbar(x, np.log10(y_sat_r1), c='k', ls='--')
  axs[1, 2].errorbar(x, np.log10(y_sat_r2), c='k', ls='--')

  axs[2, 0].errorbar(x, np.log10(y_sat_p0), c='k', ls='--')
  axs[2, 1].errorbar(x, np.log10(y_sat_p1), c='k', ls='--')
  axs[2, 2].errorbar(x, np.log10(y_sat_p2), c='k', ls='--')

  axs[3, 0].errorbar(x, np.log10((y_sat_z0 + y_sat_r0 + y_sat_p0)/3), c='k', ls='--')
  axs[3, 1].errorbar(x, np.log10((y_sat_z1 + y_sat_r1 + y_sat_p1)/3), c='k', ls='--')
  axs[3, 2].errorbar(x, np.log10((y_sat_z2 + y_sat_r2 + y_sat_p2)/3), c='k', ls='--')

  axs[4, 0].errorbar(x, np.log10(y_sat_m0), c='k', ls='--')
  axs[4, 1].errorbar(x, np.log10(y_sat_m1), c='k', ls='--')
  axs[4, 2].errorbar(x, np.log10(y_sat_m2), c='k', ls='--')

  #1 to 1 fits
  plus = fit_z.x[1] - onep5 * (fit_z.x[0] - 1)
  axs[0, 0].errorbar([-5, 2], [-5+plus, 2+plus], c='k', ls=':', alpha=0.7)
  plus = fit_z.x[2] - onep5 * (fit_z.x[0] - 1)
  axs[0, 1].errorbar([-5, 2], [-5+plus, 2+plus], c='k', ls=':', alpha=0.7)
  plus = fit_z.x[3] - onep5 * (fit_z.x[0] - 1)
  axs[0, 2].errorbar([-5, 2], [-5+plus, 2+plus], c='k', ls=':', alpha=0.7)

  plus = fit_r.x[1] - onep5 * (fit_r.x[0] - 1)
  axs[1, 0].errorbar([-5, 2], [-5+plus, 2+plus], c='k', ls=':', alpha=0.7)
  plus = fit_r.x[2] - onep5 * (fit_r.x[0] - 1)
  axs[1, 1].errorbar([-5, 2], [-5+plus, 2+plus], c='k', ls=':', alpha=0.7)
  plus = fit_r.x[3] - onep5 * (fit_r.x[0] - 1)
  axs[1, 2].errorbar([-5, 2], [-5+plus, 2+plus], c='k', ls=':', alpha=0.7)

  plus = fit_p.x[1] - onep5 * (fit_p.x[0] - 1)
  axs[2, 0].errorbar([-5, 2], [-5+plus, 2+plus], c='k', ls=':', alpha=0.7)
  plus = fit_p.x[2] - onep5 * (fit_p.x[0] - 1)
  axs[2, 1].errorbar([-5, 2], [-5+plus, 2+plus], c='k', ls=':', alpha=0.7)
  plus = fit_p.x[3] - onep5 * (fit_p.x[0] - 1)
  axs[2, 2].errorbar([-5, 2], [-5+plus, 2+plus], c='k', ls=':', alpha=0.7)

  plus = fit_t.x[1] - onep5 * (fit_t.x[0] - 1)
  axs[3, 0].errorbar([-5, 2], [-5+plus, 2+plus], c='k', ls=':', alpha=0.7)
  plus = fit_t.x[2] - onep5 * (fit_t.x[0] - 1)
  axs[3, 1].errorbar([-5, 2], [-5+plus, 2+plus], c='k', ls=':', alpha=0.7)
  plus = fit_t.x[3] - onep5 * (fit_t.x[0] - 1)
  axs[3, 2].errorbar([-5, 2], [-5+plus, 2+plus], c='k', ls=':', alpha=0.7)

  plus = fit_m.x[1] - onep5 * (fit_m.x[0] - 1)
  axs[4, 0].errorbar([-5, 2], [-5+plus, 2+plus], c='k', ls=':', alpha=0.7)
  plus = fit_m.x[2] - onep5 * (fit_m.x[0] - 1)
  axs[4, 1].errorbar([-5, 2], [-5+plus, 2+plus], c='k', ls=':', alpha=0.7)
  plus = fit_m.x[3] - onep5 * (fit_m.x[0] - 1)
  axs[4, 2].errorbar([-5, 2], [-5+plus, 2+plus], c='k', ls=':', alpha=0.7)

  #labels
  if delta:
    axs[0, 0].set_ylabel(r'$\log \Delta \upsilon_{z}^2$')
    axs[1, 0].set_ylabel(r'$\log \Delta \upsilon_{R}^2$')
    axs[2, 0].set_ylabel(r'$\log \Delta \upsilon_{\phi}^2$')
    axs[3, 0].set_ylabel(r'$\log \Delta \upsilon_{tot}^2$')
    axs[4, 0].set_ylabel(r'$\log |\Delta \overline{v_{\phi}}|$')
  else:
    axs[0, 0].set_ylabel(r'$\log \upsilon_{z}^2$')
    axs[1, 0].set_ylabel(r'$\log \upsilon_{R}^2$')
    axs[2, 0].set_ylabel(r'$\log \upsilon_{\phi}^2$')
    axs[3, 0].set_ylabel(r'$\log \upsilon_{tot}^2$')
    axs[4, 0].set_ylabel(r'$\log |\overline{v_{\phi}}|$')
  axs[4, 0].set_xlabel(r'$\log \tau$ [$\times 10^4$]')
  axs[4, 1].set_xlabel(r'$\log \tau$ [$\times 10^4$]')
  axs[4, 2].set_xlabel(r'$\log \tau$ [$\times 10^4$]')

  for i in range(4):
    for j in range(3):
      axs[i, j].set_xlim(-3.5, 0.5)
      axs[i, j].set_ylim(-3.5, 0.2)
      axs[i, j].set_xticks([-4, -3, -2, -1, 0])
      axs[i, j].set_yticks([-4, -3, -2, -1, 0])

  plt.show()
  if save:
    fname = '../galaxies/dipersion_heating'
    plt.savefig(fname, bbox_inches='tight')
    # plt.close()

  return

def evaluate_alpha_beta_gamma(save=True, opoint25=0.25, plus_4=True, alpha=0, beta=0,
                              v_200_scale=True, v_c_scale=False,
                              sigma_measured_scale=False, sigma_analytic_scale=False,
                              plot=True):
  '''
  '''
  (stacked_z_25, times_z_25, stacked_z_50, times_z_50, stacked_z_75, times_z_75,
   stacked_r_25, times_r_25, stacked_r_50, times_r_50, stacked_r_75, times_r_75,
   stacked_p_25, times_p_25, stacked_p_50, times_p_50, stacked_p_75, times_p_75,
   stacked_t_25, times_t_25, stacked_t_50, times_t_50, stacked_t_75, times_t_75,
   stacked_m_25, times_m_25, stacked_m_50, times_m_50, stacked_m_75, times_m_75
   ) = get_unsaturated_data(opoint25=opoint25, plus_4=plus_4, alpha=alpha, beta=beta,
                            v_200_scale=v_200_scale, v_c_scale=v_c_scale,
                            sigma_measured_scale=sigma_measured_scale,
                            sigma_analytic_scale=sigma_analytic_scale)

  (v_200, t_c,
   r_25, r_50, r_75,
   rho_25, rho_50, rho_75,
   v_c_25, v_c_50, v_c_75,
   sigma_dm_25, sigma_dm_50, sigma_dm_75
   ) = get_halo_constants('mu_25/fdisk_0p01_lgMdm_6p0_V200-200kmps', overwrite=False)

  def likely_line(args, x_0, y_0, x_1, y_1, x_2, y_2):
    '''likelyhood for 3 lines having the same gradient and different intercepts
    just with least squares in the y direction.
    Should probably just be a lambda function...
    '''
    (grad, c_0, c_1, c_2) = args

    out = (np.sum(np.square(y_0 - (grad*x_0 + c_0))) +
           np.sum(np.square(y_1 - (grad*x_1 + c_1))) +
           np.sum(np.square(y_2 - (grad*x_2 + c_2))))

    return out

  #data to fit
  data_z = (np.log10(times_z_25), np.log10(stacked_z_25),
            np.log10(times_z_50), np.log10(stacked_z_50),
            np.log10(times_z_75), np.log10(stacked_z_75))
  data_r = (np.log10(times_r_25), np.log10(stacked_r_25),
            np.log10(times_r_50), np.log10(stacked_r_50),
            np.log10(times_r_75), np.log10(stacked_r_75))
  data_p = (np.log10(times_p_25), np.log10(stacked_p_25),
            np.log10(times_p_50), np.log10(stacked_p_50),
            np.log10(times_p_75), np.log10(stacked_p_75))
  data_t = (np.log10(times_t_25), np.log10(stacked_t_25),
            np.log10(times_t_50), np.log10(stacked_t_50),
            np.log10(times_t_75), np.log10(stacked_t_75))
  data_m = (np.log10(times_m_25), np.log10(-stacked_m_25),
            np.log10(times_m_50), np.log10(-stacked_m_50),
            np.log10(times_m_75), np.log10(-stacked_m_75))

  #do the fit
  fit_z = minimize(likely_line, (1, 0.25, 0, -0.25), args=data_z)
  fit_r = minimize(likely_line, (1, 0.25, 0, -0.25), args=data_r)
  fit_p = minimize(likely_line, (1, 0.25, 0, -0.25), args=data_p)
  fit_t = minimize(likely_line, (1, 0.25, 0, -0.25), args=data_t)
  fit_m = minimize(likely_line, (1, 0.25, 0, -0.25), args=data_m)

  #fitted values
  slope_z, gamma_z = fit_z.x[0], fit_z.x[0] - 1
  slope_r, gamma_r = fit_r.x[0], fit_r.x[0] - 1
  slope_p, gamma_p = fit_p.x[0], fit_p.x[0] - 1
  slope_t, gamma_t = fit_t.x[0], fit_t.x[0] - 1
  slope_m, gamma_m = fit_m.x[0], fit_m.x[0] - 1

  #test radius dependant scaling.
  if v_200_scale:
    sigma_analytic_scale=True
    # sigma_measured_scale=True
    # v_c_scale = True
    # v_200_scale=False

  #get velocity scale
  (v_25_scale, v_50_scale, v_75_scale
   ) = get_velocity_scale('mu_25/fdisk_0p01_lgMdm_6p0_V200-200kmps',
                          v_200_scale=False, v_c_scale=v_c_scale,
                          sigma_measured_scale=sigma_measured_scale,
                          sigma_analytic_scale=sigma_analytic_scale)

  #alpha
  log_delta_25 = np.log10(rho_25 / (200 * RHO_CRIT))
  log_delta_50 = np.log10(rho_50 / (200 * RHO_CRIT))
  log_delta_75 = np.log10(rho_75 / (200 * RHO_CRIT))

  #beta
  log_v_25 = np.log10(v_25_scale / v_200)
  log_v_50 = np.log10(v_50_scale / v_200)
  log_v_75 = np.log10(v_75_scale / v_200)

  def likely_histogram(beta, alpha, gamma, t_0, v_0, t_1, v_1, t_2, v_2):
    '''
    '''
    c_25 = v_0 - (1+gamma)*t_0 - alpha*log_delta_25 - beta*log_v_25
    c_50 = v_1 - (1+gamma)*t_1 - alpha*log_delta_50 - beta*log_v_50
    c_75 = v_2 - (1+gamma)*t_2 - alpha*log_delta_75 - beta*log_v_75

    out = np.std(np.hstack((c_25, c_50, c_75)))

    return out

  (alpha_z, alpha_r, alpha_p, alpha_t, alpha_m) = (0, 0, 0, 0, 0)

  #do the fit
  fit2_z = minimize(likely_histogram, 0.5, args=(alpha_z, gamma_z, *data_z))
  fit2_r = minimize(likely_histogram, 0.5, args=(alpha_r, gamma_r, *data_r))
  fit2_p = minimize(likely_histogram, 0.5, args=(alpha_p, gamma_p, *data_p))
  fit2_t = minimize(likely_histogram, 0.5, args=(alpha_t, gamma_t, *data_t))
  fit2_m = minimize(likely_histogram, 0.5, args=(alpha_m, gamma_m, *data_m))

  beta_z = fit2_z.x[0]
  beta_r = fit2_r.x[0]
  beta_p = fit2_p.x[0]
  beta_t = fit2_t.x[0]
  beta_m = fit2_m.x[0]

  std_z = fit2_z.fun
  std_r = fit2_r.fun
  std_p = fit2_p.fun
  std_t = fit2_t.fun
  std_m = fit2_m.fun

  print('')
  print('Set alpha to 0, fixed gamma.')
  print('std, alpha, beta, gamma')
  print('z: %0.4f, %0.4f, %0.4f, %0.4f'%(std_z, alpha_z, beta_z, gamma_z))
  print('R: %0.4f, %0.4f, %0.4f, %0.4f'%(std_r, alpha_r, beta_r, gamma_r))
  print('p: %0.4f, %0.4f, %0.4f, %0.4f'%(std_p, alpha_p, beta_p, gamma_p))
  print('t: %0.4f, %0.4f, %0.4f, %0.4f'%(std_t, alpha_t, beta_t, gamma_t))
  print('m: %0.4f, %0.4f, %0.4f, %0.4f'%(std_m, alpha_m, beta_m, gamma_m))

  def likely_histogram(alpha, beta, gamma, t_0, v_0, t_1, v_1, t_2, v_2):
    '''
    '''
    c_25 = v_0 - (1+gamma)*t_0 - alpha*log_delta_25 - beta*log_v_25
    c_50 = v_1 - (1+gamma)*t_1 - alpha*log_delta_50 - beta*log_v_50
    c_75 = v_2 - (1+gamma)*t_2 - alpha*log_delta_75 - beta*log_v_75

    out = np.std(np.hstack((c_25, c_50, c_75)))

    return out

  (beta_z, beta_r, beta_p, beta_t, beta_m) = (0, 0, 0, 0, 0)

  #do the fit
  fit2_z = minimize(likely_histogram, 0.5, args=(beta_z, gamma_z, *data_z))
  fit2_r = minimize(likely_histogram, 0.5, args=(beta_r, gamma_r, *data_r))
  fit2_p = minimize(likely_histogram, 0.5, args=(beta_p, gamma_p, *data_p))
  fit2_t = minimize(likely_histogram, 0.5, args=(beta_t, gamma_t, *data_t))
  fit2_m = minimize(likely_histogram, 0.5, args=(beta_m, gamma_m, *data_m))

  alpha_z = fit2_z.x[0]
  alpha_r = fit2_r.x[0]
  alpha_p = fit2_p.x[0]
  alpha_t = fit2_t.x[0]
  alpha_m = fit2_m.x[0]

  std_z = fit2_z.fun
  std_r = fit2_r.fun
  std_p = fit2_p.fun
  std_t = fit2_t.fun
  std_m = fit2_m.fun

  print('')
  print('Set beta to 0, fixed gamma.')
  print('std, alpha, beta, gamma')
  print('z: %0.4f, %0.4f, %0.4f, %0.4f'%(std_z, alpha_z, beta_z, gamma_z))
  print('R: %0.4f, %0.4f, %0.4f, %0.4f'%(std_r, alpha_r, beta_r, gamma_r))
  print('p: %0.4f, %0.4f, %0.4f, %0.4f'%(std_p, alpha_p, beta_p, gamma_p))
  print('t: %0.4f, %0.4f, %0.4f, %0.4f'%(std_t, alpha_t, beta_t, gamma_t))
  print('m: %0.4f, %0.4f, %0.4f, %0.4f'%(std_m, alpha_m, beta_m, gamma_m))

  def likely(abc, t_0, v_0, t_1, v_1, t_2, v_2):
    '''
    '''
    c_25 = v_0 - (1+abc[2])*t_0 - abc[0]*log_delta_25 - abc[1]*log_v_25
    c_50 = v_1 - (1+abc[2])*t_1 - abc[0]*log_delta_50 - abc[1]*log_v_50
    c_75 = v_2 - (1+abc[2])*t_2 - abc[0]*log_delta_75 - abc[1]*log_v_75

    out = np.std(np.hstack((c_25, c_50, c_75)))

    return out

  #do the fit
  fit3_z = minimize(likely, (0, 0, 0), args=data_z)
  fit3_r = minimize(likely, (0, 0, 0), args=data_r)
  fit3_p = minimize(likely, (0, 0, 0), args=data_p)
  fit3_t = minimize(likely, (0, 0, 0), args=data_t)
  fit3_m = minimize(likely, (0, 0, 0), args=data_m)

  alpha_z, beta_z, gamma_z = fit3_z.x
  alpha_r, beta_r, gamma_r = fit3_r.x
  alpha_p, beta_p, gamma_p = fit3_p.x
  alpha_t, beta_t, gamma_t = fit3_t.x
  alpha_m, beta_m, gamma_m = fit3_m.x
  slope_z, std_z = fit3_z.x[2] + 1, fit3_z.fun
  slope_r, std_r = fit3_r.x[2] + 1, fit3_r.fun
  slope_p, std_p = fit3_p.x[2] + 1, fit3_p.fun
  slope_t, std_t = fit3_t.x[2] + 1, fit3_t.fun
  slope_m, std_m = fit3_m.x[2] + 1, fit3_m.fun

  print('Everything Free')
  print('std, alpha, beta, gamma')
  print('z: %0.4f, %0.4f, %0.4f, %0.5f'%(std_z, alpha_z, beta_z, gamma_z))
  print('R: %0.4f, %0.4f, %0.4f, %0.4f'%(std_r, alpha_r, beta_r, gamma_r))
  print('p: %0.4f, %0.4f, %0.4f, %0.4f'%(std_p, alpha_p, beta_p, gamma_p))
  print('t: %0.4f, %0.4f, %0.4f, %0.4f'%(std_t, alpha_t, beta_t, gamma_t))
  print('m: %0.4f, %0.4f, %0.4f, %0.4f'%(std_m, alpha_m, beta_m, gamma_m))

###############################################################################
  if plot:
    #plot
    plt.figure()
    x = np.linspace(-5, 1)

    yz0 = slope_z * x + fit_z.x[1]
    yz1 = slope_z * x + fit_z.x[2]
    yz2 = slope_z * x + fit_z.x[3]
    yR0 = slope_r * x + fit_r.x[1]
    yR1 = slope_r * x + fit_r.x[2]
    yR2 = slope_r * x + fit_r.x[3]
    yp0 = slope_p * x + fit_p.x[1]
    yp1 = slope_p * x + fit_p.x[2]
    yp2 = slope_p * x + fit_p.x[3]
    yt0 = slope_t * x + fit_t.x[1]
    yt1 = slope_t * x + fit_t.x[2]
    yt2 = slope_t * x + fit_t.x[3]
    ym0 = slope_m * x + fit_m.x[1]
    ym1 = slope_m * x + fit_m.x[2]
    ym2 = slope_m * x + fit_m.x[3]

    # print(np.mean(fit_z.x[1:]))
    # print(np.mean(fit_r.x[1:]))
    # print(np.mean(fit_p.x[1:]))
    # print(np.mean(fit_t.x[1:]))

    plt.errorbar(x, yt0+4, c='C0', ls='-',
                  # label='$\sigma_{tot}^2$ slope = ' + str(round(fit_t.x[0], 2)))
                  label='$\gamma_{tot}$ = ' + str(round(fit_t.x[0] -1, 2)))
    plt.errorbar(x, yt1+4, c='C0', ls=':')
    plt.errorbar(x, yt2+4, c='C0', ls='--')
    plt.errorbar(np.log10(times_t_25), np.log10(stacked_t_25)+4,
                  ls='', fmt='.', alpha=0.2, c='C0')
    plt.errorbar(np.log10(times_t_50), np.log10(stacked_t_50)+4,
                  ls='', fmt='+', alpha=0.2, c='C0')
    plt.errorbar(np.log10(times_t_75), np.log10(stacked_t_75)+4,
                  ls='', fmt='*', alpha=0.2, c='C0')

    plt.errorbar(x, yz0+2, c='C1', ls='-',
                  # label=r'$\sigma_z^2$ slope = ' + str(round(fit_z.x[0], 2)))
                  label='$\gamma_{z}$ = ' + str(round(fit_z.x[0] -1, 2)))
    plt.errorbar(x, yz1+2, c='C1', ls=':')
    plt.errorbar(x, yz2+2, c='C1', ls='--')
    plt.errorbar(np.log10(times_z_25), np.log10(stacked_z_25)+2,
                  ls='', fmt='.', alpha=0.2, c='C1')
    plt.errorbar(np.log10(times_z_50), np.log10(stacked_z_50)+2,
                  ls='', fmt='+', alpha=0.2, c='C1')
    plt.errorbar(np.log10(times_z_75), np.log10(stacked_z_75)+2,
                  ls='', fmt='*', alpha=0.2, c='C1')

    plt.errorbar(x, yR0, c='C2', ls='-',
                  # label=r'$\sigma_R^2$ slope = ' + str(round(fit_r.x[0], 2)))
                  label='$\gamma_{R}$ = ' + str(round(fit_r.x[0] -1, 2)))
    plt.errorbar(x, yR1, c='C2', ls=':')
    plt.errorbar(x, yR2, c='C2', ls='--')
    plt.errorbar(np.log10(times_r_25), np.log10(stacked_r_25),
                  ls='', fmt='.', alpha=0.2, c='C2')
    plt.errorbar(np.log10(times_r_50), np.log10(stacked_r_50),
                  ls='', fmt='+', alpha=0.2, c='C2')
    plt.errorbar(np.log10(times_r_75), np.log10(stacked_r_75),
                  ls='', fmt='*', alpha=0.2, c='C2')

    plt.errorbar(x, yp0-2, c='C3', ls='-',
                  # label=r'$\sigma_\phi^2$ slope = ' + str(round(fit_p.x[0], 2)))
                  label='$\gamma_{\phi}$ = ' + str(round(fit_p.x[0] -1, 2)))
    plt.errorbar(x, yp1-2, c='C3', ls=':')
    plt.errorbar(x, yp2-2, c='C3', ls='--')
    plt.errorbar(np.log10(times_p_25), np.log10(stacked_p_25)-2,
                  ls='', fmt='.', alpha=0.2, c='C3')
    plt.errorbar(np.log10(times_p_50), np.log10(stacked_p_50)-2,
                  ls='', fmt='+', alpha=0.2, c='C3')
    plt.errorbar(np.log10(times_p_75), np.log10(stacked_p_75)-2,
                  ls='', fmt='*', alpha=0.2, c='C3')

    plt.errorbar(x, ym0-6, c='C4', ls='-',
                 # label=r'$\overline{v_\phi}$ slope = ' + str(round(fit_m.x[0], 2)))
                 label=r'$\gamma_{\overline{v_\phi}}$ = ' + str(
                   round(fit_m.x[0] -1, 2)))
    plt.errorbar(x, ym1-6, c='C4', ls=':')
    plt.errorbar(x, ym2-6, c='C4', ls='--')
    plt.errorbar(np.log10(times_m_25), np.log10(-stacked_m_25)-6,
                 ls='', fmt='.', alpha=0.2, c='C4')
    plt.errorbar(np.log10(times_m_50), np.log10(-stacked_m_50)-6,
                 ls='', fmt='+', alpha=0.2, c='C4')
    plt.errorbar(np.log10(times_m_75), np.log10(-stacked_m_75)-6,
                 ls='', fmt='*', alpha=0.2, c='C4')

    # plt.text(0, fit_t.x[2]+4, str(round(fit_t.x[0], 2)), c='C0')
    # plt.text(0, fit_z.x[2]+2, str(round(fit_z.x[0], 2)), c='C1')
    # plt.text(0, fit_r.x[2]  , str(round(fit_r.x[0], 2)), c='C2')
    # plt.text(0, fit_p.x[2]-2, str(round(fit_p.x[0], 2)), c='C3')

    plt.xlabel(r'$\log \tau$')
    plt.ylabel(r'$\log \Delta \upsilon^2 + C$')
    # plt.xlim((-4, 0))
    # plt.ylim((-9, 5))
    l = plt.legend()
    for i, text in enumerate(l.get_texts()):
        text.set_color('C' + str(i))

    plt.show() if save:
      fname = '../galaxies/heating-constants-gamma'
      plt.savefig(fname, bbox_inches='tight')
      plt.close()

    #plots
    alpha_grid = np.linspace(0, 1, 101)

    spreds_z = np.zeros(len(alpha_grid))
    spreds_r = np.zeros(len(alpha_grid))
    spreds_p = np.zeros(len(alpha_grid))
    spreds_t = np.zeros(len(alpha_grid))
    spreds_m = np.zeros(len(alpha_grid))

    for i in range(len(alpha_grid)):
      spreds_z[i] = likely_histogram(alpha_grid[i], beta_z, gamma_z, *data_z)
      spreds_r[i] = likely_histogram(alpha_grid[i], beta_r, gamma_r, *data_r)
      spreds_p[i] = likely_histogram(alpha_grid[i], beta_p, gamma_p, *data_p)
      spreds_t[i] = likely_histogram(alpha_grid[i], beta_t, gamma_t, *data_t)
      spreds_m[i] = likely_histogram(alpha_grid[i], beta_m, gamma_m, *data_m)

    plt.figure()

    # plt.vlines(0, 0, 0.3, color='k', ls='--', linewidth=8, alpha=0.5)

    plt.errorbar(alpha_grid, spreds_t, label=r'$\sigma_{tot}^2$')
    plt.scatter(alpha_t, fit2_t.fun)
    plt.errorbar(alpha_grid, spreds_z, label=r'$\sigma_z^2$')
    plt.scatter(alpha_z, fit2_z.fun)
    plt.errorbar(alpha_grid, spreds_r, label=r'$\sigma_R^2$')
    plt.scatter(alpha_r, fit2_r.fun)
    plt.errorbar(alpha_grid, spreds_p, label=r'$\sigma_\phi^2$')
    plt.scatter(alpha_p, fit2_p.fun)
    plt.errorbar(alpha_grid, spreds_m, label=r'$\overline{v_\phi}$')
    plt.scatter(alpha_m, fit2_m.fun)

    plt.xlabel(r'$\alpha$')
    plt.ylabel(r'$\sigma_{\log \sqrt{2} \pi \ln \Lambda_i}$')
    plt.xlim((np.amin(alpha_grid), np.amax(alpha_grid)))
    plt.ylim((0, 0.3))
    plt.legend(loc='upper right')

    plt.show() if save:
      fname = '../galaxies/heating-constants-alpha'
      plt.savefig(fname, bbox_inches='tight')
      plt.close()

  #make histogram
  c_z_25 = (np.log10(stacked_z_25) - (1+gamma_z)*(np.log10(times_z_25) - plus_4*4) -
            alpha_z*log_delta_25 - beta_z*log_v_25)
  c_z_50 = (np.log10(stacked_z_50) - (1+gamma_z)*(np.log10(times_z_50) - plus_4*4) -
            alpha_z*log_delta_50 - beta_z*log_v_50)
  c_z_75 = (np.log10(stacked_z_75) - (1+gamma_z)*(np.log10(times_z_75) - plus_4*4) -
            alpha_z*log_delta_75 - beta_z*log_v_75)

  c_r_25 = (np.log10(stacked_r_25) - (1+gamma_r)*(np.log10(times_r_25) - plus_4*4) -
            alpha_r*log_delta_25 - beta_r*log_v_25)
  c_r_50 = (np.log10(stacked_r_50) - (1+gamma_r)*(np.log10(times_r_50) - plus_4*4) -
            alpha_r*log_delta_50 - beta_r*log_v_50)
  c_r_75 = (np.log10(stacked_r_75) - (1+gamma_r)*(np.log10(times_r_75) - plus_4*4) -
            alpha_r*log_delta_75 - beta_r*log_v_75)

  c_p_25 = (np.log10(stacked_p_25) - (1+gamma_p)*(np.log10(times_p_25) - plus_4*4) -
            alpha_p*log_delta_25 - beta_p*log_v_25)
  c_p_50 = (np.log10(stacked_p_50) - (1+gamma_p)*(np.log10(times_p_50) - plus_4*4) -
            alpha_p*log_delta_50 - beta_p*log_v_50)
  c_p_75 = (np.log10(stacked_p_75) - (1+gamma_p)*(np.log10(times_p_75) - plus_4*4) -
            alpha_p*log_delta_75 - beta_p*log_v_75)

  c_t_25 = (np.log10(stacked_t_25) - (1+gamma_t)*(np.log10(times_t_25) - plus_4*4) -
            alpha_p*log_delta_25 - beta_p*log_v_25)
  c_t_50 = (np.log10(stacked_t_50) - (1+gamma_t)*(np.log10(times_t_50) - plus_4*4) -
            alpha_p*log_delta_50 - beta_p*log_v_50)
  c_t_75 = (np.log10(stacked_t_75) - (1+gamma_t)*(np.log10(times_t_75) - plus_4*4) -
            alpha_p*log_delta_75 - beta_p*log_v_75)

  c_m_25 = (np.log10(-stacked_m_25) - (1+gamma_m)*(np.log10(times_m_25) - plus_4*4) -
            alpha_m*log_delta_25 - beta_m*log_v_25)
  c_m_50 = (np.log10(-stacked_m_50) - (1+gamma_m)*(np.log10(times_m_50) - plus_4*4) -
            alpha_m*log_delta_50 - beta_m*log_v_50)
  c_m_75 = (np.log10(-stacked_m_75) - (1+gamma_m)*(np.log10(times_m_75) - plus_4*4) -
            alpha_m*log_delta_75 - beta_m*log_v_75)

  const_z = np.hstack((c_z_25, c_z_50, c_z_75))
  const_R = np.hstack((c_r_25, c_r_50, c_r_75))
  const_p = np.hstack((c_p_25, c_p_50, c_p_75))
  const_t = np.hstack((c_t_25, c_t_50, c_t_75))
  const_m = np.hstack((c_m_25, c_m_50, c_m_75))

  ln_Lambda_z = np.mean(const_z)
  ln_Lambda_r = np.mean(const_R)
  ln_Lambda_p = np.mean(const_p)
  ln_Lambda_t = np.mean(const_t)
  ln_Lambda_m = np.mean(const_m)

  print(ln_Lambda_z)
  print(ln_Lambda_r)
  print(ln_Lambda_p)
  print(ln_Lambda_t)
  print(ln_Lambda_m)

  if plot:
    plt.figure()
    bins = np.linspace(0.5, 2.5, 81)
    plt.hist(const_t, alpha=0.8, color='C0', bins=bins, label=r'$\sigma_{tot}^2$')
    plt.hist(c_t_25, alpha=0.5, color='C0', bins=bins)
    plt.hist(c_t_50, alpha=0.5, color='C0', bins=bins)
    plt.hist(c_t_75, alpha=0.5, color='C0', bins=bins)

    plt.hist(const_z, alpha=0.8, color='C1', bins=bins, label=r'$\sigma_z^2$')
    plt.hist(c_z_25, alpha=0.5, color='C1', bins=bins)
    plt.hist(c_z_50, alpha=0.5, color='C1', bins=bins)
    plt.hist(c_z_75, alpha=0.5, color='C1', bins=bins)

    plt.hist(const_R, alpha=0.8, color='C2', bins=bins, label=r'$\sigma_R^2$')
    plt.hist(c_r_25, alpha=0.5, color='C2', bins=bins)
    plt.hist(c_r_50, alpha=0.5, color='C2', bins=bins)
    plt.hist(c_r_75, alpha=0.5, color='C2', bins=bins)

    plt.hist(const_p, alpha=0.8, color='C3', bins=bins, label=r'$\sigma_\phi^2$')
    plt.hist(c_p_25, alpha=0.5, color='C3', bins=bins)
    plt.hist(c_p_50, alpha=0.5, color='C3', bins=bins)
    plt.hist(c_p_75, alpha=0.5, color='C3', bins=bins)

    plt.hist(const_m, alpha=0.8, color='C4', bins=bins, label=r'$\overline{v_\phi}$')
    plt.hist(c_m_25, alpha=0.5, color='C4', bins=bins)
    plt.hist(c_m_50, alpha=0.5, color='C4', bins=bins)
    plt.hist(c_m_75, alpha=0.5, color='C4', bins=bins)
    # splt.hist(c_m_25, alpha=0.5, color='C0', bins=bins)
    # splt.hist(c_m_50, alpha=0.5, color='C1', bins=bins)
    # splt.hist(c_m_75, alpha=0.5, color='C2', bins=bins)

    plt.xlabel(r'$\log \sqrt{2} \pi \ln \Lambda_i$')
    plt.ylabel(r'$N$')
    plt.legend()

    plt.show()
    if save:
      fname = '../galaxies/heating-constants-constant'
      plt.savefig(fname, bbox_inches='tight')
      plt.close()

    alpha = 0
    beta = 0
    # gamma = 0

    out_z = likely((alpha, beta, gamma_z), *data_z)
    out_r = likely((alpha, beta, gamma_r), *data_r)
    out_p = likely((alpha, beta, gamma_p), *data_p)
    out_t = likely((alpha, beta, gamma_t), *data_t)
    out_m = likely((alpha, beta, gamma_m), *data_m)

    print('')
    print('set alpha and beta to 0')
    print('std, alpha, beta, gamma')
    print('z: %0.4f, %0.4f, %0.4f, %0.4f'%(out_z, alpha, beta, gamma_z))
    print('R: %0.4f, %0.4f, %0.4f, %0.4f'%(out_r, alpha, beta, gamma_r))
    print('p: %0.4f, %0.4f, %0.4f, %0.4f'%(out_p, alpha, beta, gamma_p))
    print('t: %0.4f, %0.4f, %0.4f, %0.4f'%(out_t, alpha, beta, gamma_t))
    print('m: %0.4f, %0.4f, %0.4f, %0.4f'%(out_m, alpha, beta, gamma_m))

  return(ln_Lambda_z, alpha_z, beta_z, gamma_z,
         ln_Lambda_r, alpha_r, beta_r, gamma_r,
         ln_Lambda_p, alpha_p, beta_p, gamma_p,
         ln_Lambda_t, alpha_t, beta_t, gamma_t,
         ln_Lambda_m, alpha_m, beta_m, gamma_m)

def test_constants(save=True, alpha=1, beta=-3, gamma=0,
                   v_200_scale=False, v_c_scale=False,
                   sigma_measured_scale=True, sigma_analytic_scale=False):
  '''
  '''
  names = ['mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps',
           'mu_25/fdisk_0p01_lgMdm_7p5_V200-200kmps',
           'mu_25/fdisk_0p01_lgMdm_7p0_V200-200kmps',
           'mu_25/fdisk_0p01_lgMdm_6p5_V200-200kmps',
           'mu_25/fdisk_0p01_lgMdm_6p0_V200-200kmps']

  colors = {'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps':0.99,
            'mu_25/fdisk_0p01_lgMdm_7p5_V200-200kmps':0.8,
            'mu_25/fdisk_0p01_lgMdm_7p0_V200-200kmps':0.6,
            'mu_25/fdisk_0p01_lgMdm_6p5_V200-200kmps':0.4,
            'mu_25/fdisk_0p01_lgMdm_6p0_V200-200kmps':0.2}

  bluecmap = matplotlib.cm.get_cmap('Blues')
  orangecmap = matplotlib.cm.get_cmap('Oranges')
  greencmap = matplotlib.cm.get_cmap('Greens')

  #set up plot
  fig, axs = plt.subplots(nrows=5, ncols=1, sharex=True, sharey=True,
                         figsize=(3.5, 15))
  fig.subplots_adjust(hspace=0.03, wspace=0.03)

  #slope - n
  #z  0.0314
  #R -0.1688
  #p  0.0332
  #t -0.0672
  #y intercept
  # 1.1000
  # 0.9317
  # 1.3182
  # 1.5423

  x = np.linspace(-4.2, 0)
  y_z = (1 - 0) * x + 1.1000
  y_r = (1 - 0.1688) * x + 0.9317
  y_rs = (1 - 0) * x + 1.35
  y_p = (1 - 0) * x + 1.24
  y_t = (1 - 0.0672) * x + 1.0652
  y_ts = (1 - 0) * x + 1.245

  axs[0].errorbar(x, y_z, c='k', alpha=0.6, linewidth=3, zorder=20)
  axs[1].errorbar(x, y_rs, c='k', alpha=0.6, linewidth=3, zorder=20)
  axs[1].errorbar(x, y_r, c='C1', alpha=0.6, linewidth=3, zorder=20)
  axs[2].errorbar(x, y_p, c='k', alpha=0.6, linewidth=3, zorder=20)
  axs[3].errorbar(x, y_ts, c='k', alpha=0.6, linewidth=3, zorder=20)
  axs[3].errorbar(x, y_t, c='C1', alpha=0.6, linewidth=3, zorder=20)
  # axs[4].errorbar([-5,2], [-4,3], c='k')

  axs[1].text(x[0]+0.5, y_rs[0], r'$\propto t^{0.83}$', c='C1')
  axs[3].text(x[0]+0.5, y_ts[0], r'$\propto t^{0.93}$', c='C1')

  for name in names:
    (delta_upsilon_r_25, delta_upsilon_r_50, delta_upsilon_r_75,
     delta_upsilon_z_25, delta_upsilon_z_50, delta_upsilon_z_75,
     delta_upsilon_phi_25, delta_upsilon_phi_50, delta_upsilon_phi_75,
     delta_upsilon_tot_25, delta_upsilon_tot_50, delta_upsilon_tot_75,
     delta_mean_phi_25, delta_mean_phi_50, delta_mean_phi_75
     ) = get_upsilon(name, overwrite=False, delta=True,
                     v_200_scale=v_200_scale, v_c_scale=v_c_scale,
                     sigma_measured_scale=sigma_measured_scale,
                     sigma_analytic_scale=sigma_analytic_scale)

    (tau_25, tau_50, tau_75
     ) = get_scale_time(name, alpha=alpha, beta=beta,
                        v_200_scale=v_200_scale, v_c_scale=v_c_scale,
                        sigma_measured_scale=sigma_measured_scale,
                        sigma_analytic_scale=sigma_analytic_scale)

    #points on plot
    axs[0].errorbar(np.log10(tau_25), np.log10(delta_upsilon_z_25), ls='', fmt='.',
                    alpha=0.2, c=bluecmap(colors[name]), zorder=-10)
    axs[0].errorbar(np.log10(tau_50), np.log10(delta_upsilon_z_50), ls='', fmt='.',
                    alpha=0.2, c=orangecmap(colors[name]), zorder=-5)
    axs[0].errorbar(np.log10(tau_75), np.log10(delta_upsilon_z_75), ls='', fmt='.',
                    alpha=0.2, c=greencmap(colors[name]))

    axs[1].errorbar(np.log10(tau_25), np.log10(delta_upsilon_r_25), ls='', fmt='.',
                    alpha=0.2, c=bluecmap(colors[name]), zorder=-10)
    axs[1].errorbar(np.log10(tau_50), np.log10(delta_upsilon_r_50), ls='', fmt='.',
                    alpha=0.2, c=orangecmap(colors[name]), zorder=-5)
    axs[1].errorbar(np.log10(tau_75), np.log10(delta_upsilon_r_75), ls='', fmt='.',
                    alpha=0.2, c=greencmap(colors[name]))

    axs[2].errorbar(np.log10(tau_25), np.log10(delta_upsilon_phi_25), ls='', fmt='.',
                    alpha=0.2, c=bluecmap(colors[name]), zorder=-10)
    axs[2].errorbar(np.log10(tau_50), np.log10(delta_upsilon_phi_50), ls='', fmt='.',
                    alpha=0.2, c=orangecmap(colors[name]), zorder=-5)
    axs[2].errorbar(np.log10(tau_75), np.log10(delta_upsilon_phi_75), ls='', fmt='.',
                    alpha=0.2, c=greencmap(colors[name]))

    axs[3].errorbar(np.log10(tau_25), np.log10(delta_upsilon_tot_25), ls='', fmt='.',
                    alpha=0.2, c=bluecmap(colors[name]), zorder=-10)
    axs[3].errorbar(np.log10(tau_50), np.log10(delta_upsilon_tot_50), ls='', fmt='.',
                    alpha=0.2, c=orangecmap(colors[name]), zorder=-5)
    axs[3].errorbar(np.log10(tau_75), np.log10(delta_upsilon_tot_75), ls='', fmt='.',
                    alpha=0.2, c=greencmap(colors[name]))

    axs[4].errorbar(np.log10(tau_25), np.log10(-delta_mean_phi_25), ls='', fmt='.',
                    alpha=0.2, c=bluecmap(colors[name]), zorder=-10)
    axs[4].errorbar(np.log10(tau_50), np.log10(-delta_mean_phi_50), ls='', fmt='.',
                    alpha=0.2, c=orangecmap(colors[name]), zorder=-5)
    axs[4].errorbar(np.log10(tau_75), np.log10(-delta_mean_phi_75), ls='', fmt='.',
                    alpha=0.2, c=greencmap(colors[name]))

  #finish plots with labels and lines

  # log_sigma_h_25 = np.log10(disp_h_25**2 / sigma_dm_25**2)
  # log_sigma_h_50 = np.log10(disp_h_50**2 / sigma_dm_50**2)
  # log_sigma_h_75 = np.log10(disp_h_75**2 / sigma_dm_75**2)

  axs[0].errorbar([-5, 2], [0, 0], c='grey', ls=':', linewidth=5)
  axs[1].errorbar([-5, 2], [0, 0], c='grey', ls=':', linewidth=5)
  axs[2].errorbar([-5, 2], [0, 0], c='grey', ls=':', linewidth=5)
  axs[3].errorbar([-5, 2], [0, 0], c='grey', ls=':', linewidth=5)

  #labels
  # axs[0].set_ylabel(r"$\log \Delta\sigma_{\star, z}^2(r) / \sigma_{h, z}^2(r)$")
  # axs[1].set_ylabel(r"$\log \Delta\sigma_{\star, R}^2(r) / \sigma_{h, R}^2(r)$")
  # axs[2].set_ylabel(r"$\log \Delta\sigma_{\star, \phi}^2(r)/\sigma_{h, \phi}^2(r)$")
  # axs[3].set_ylabel(r"$\log \Delta\sigma_{\star, tot}^2(r) /\sigma_{h, tot}^2(r)$")
  # axs[4].set_ylabel(r"$\log \Delta\overline{v_{\phi}}(r) / \sigma_{h, \phi}(r) $")
  # axs[4].set_xlabel(r"$\log \, t [\sigma_{DM}(r)^3] / [G^2 \rho_{DM}(r) M_{DM}] $")
  axs[0].set_ylabel(r"$\log\frac{\Delta\sigma_{\star,z}(r)^2}{\sigma_{h,z}(r)^2}$")
  axs[1].set_ylabel(r"$\log\frac{\Delta\sigma_{\star,R}(r)^2}{\sigma_{h,R}(r)^2}$")
  axs[2].set_ylabel(r"$\log\frac{\Delta\sigma_{\star,\phi}(r)^2}{\sigma_{h,\phi}(r)^2}$")
  axs[3].set_ylabel(r"$\log\frac{\Delta\sigma_{\star,tot}(r)^2 }{\sigma_{h,tot}(r)^2}$")
  axs[4].set_ylabel(r"$\log\frac{\Delta \overline{v_{\phi}}(r)}{\sigma_{h, \phi}(r)}$")
  # axs[4].set_xlabel(r"$\log \tau$")
  axs[4].set_xlabel(r"$\log \frac{t \, G^2 \rho_{h}(r) M_{DM}}{\sigma_{h}(r)^3}$")

  axs[0].set_xticks([-4, -3, -2, -1])
  axs[0].set_yticks([-3, -2, -1, 0])
  axs[0].set_xlim(-4.2, -0.5)
  axs[0].set_ylim(-3.2, 0.5)

  axs[0].legend([(lines.Line2D([0, 1], [0, 1], color=bluecmap(0.2), marker='.'),
                  lines.Line2D([0, 1], [0, 1], color=bluecmap(0.4), marker='.'),
                  lines.Line2D([0, 1], [0, 1], color=bluecmap(0.6), marker='.'),
                  lines.Line2D([0, 1], [0, 1], color=bluecmap(0.8), marker='.'),
                  lines.Line2D([0, 1], [0, 1], color=bluecmap(0.99), marker='.')),
                 (lines.Line2D([0, 1], [0, 1], color=orangecmap(0.2), marker='.'),
                  lines.Line2D([0, 1], [0, 1], color=orangecmap(0.4), marker='.'),
                  lines.Line2D([0, 1], [0, 1], color=orangecmap(0.6), marker='.'),
                  lines.Line2D([0, 1], [0, 1], color=orangecmap(0.8), marker='.'),
                  lines.Line2D([0, 1], [0, 1], color=orangecmap(0.99), marker='.')),
                 (lines.Line2D([0, 1], [0, 1], color=greencmap(0.2), marker='.'),
                  lines.Line2D([0, 1], [0, 1], color=greencmap(0.4), marker='.'),
                  lines.Line2D([0, 1], [0, 1], color=greencmap(0.6), marker='.'),
                  lines.Line2D([0, 1], [0, 1], color=greencmap(0.8), marker='.'),
                  lines.Line2D([0, 1], [0, 1], color=greencmap(0.99), marker='.'))],
                [r'$r_{25}$', r'$r_{50}$', r'$r_{75}$'],
                handler_map={tuple:HandlerTuple(ndivide=5)}, loc='upper left',
                bbox_to_anchor=(0.98, 1.065))

  axs[1].legend([
    (lines.Line2D([0, 1], [0, 1], color=bluecmap(0.2), marker='.', linestyle=''),
     lines.Line2D([0, 1], [0, 1], color=orangecmap(0.2), marker='.', linestyle=''),
     lines.Line2D([0, 1], [0, 1], color=greencmap(0.2), marker='.', linestyle='')),
    (lines.Line2D([0, 1], [0, 1], color=bluecmap(0.4), marker='.', linestyle=''),
     lines.Line2D([0, 1], [0, 1], color=orangecmap(0.4), marker='.', linestyle=''),
     lines.Line2D([0, 1], [0, 1], color=greencmap(0.4), marker='.', linestyle='')),
    (lines.Line2D([0, 1], [0, 1], color=bluecmap(0.6), marker='.', linestyle=''),
     lines.Line2D([0, 1], [0, 1], color=orangecmap(0.6), marker='.', linestyle=''),
     lines.Line2D([0, 1], [0, 1], color=greencmap(0.6), marker='.', linestyle='')),
    (lines.Line2D([0, 1], [0, 1], color=bluecmap(0.8), marker='.', linestyle=''),
     lines.Line2D([0, 1], [0, 1], color=orangecmap(0.8), marker='.', linestyle=''),
     lines.Line2D([0, 1], [0, 1], color=greencmap(0.8), marker='.', linestyle='')),
    (lines.Line2D([0, 1], [0, 1], color=bluecmap(0.99), marker='.', linestyle=''),
     lines.Line2D([0, 1], [0, 1], color=orangecmap(0.99), marker='.', linestyle=''),
     lines.Line2D([0, 1], [0, 1], color=greencmap(0.99), marker='.', linestyle=''))],
    [r'$M_{DM} = 10^{6} $M$_\odot$', r'$M_{DM} = 10^{6.5} $M$_\odot$',
     r'$M_{DM} = 10^{7} $M$_\odot$', r'$M_{DM} = 10^{7.5} $M$_\odot$',
     r'$M_{DM} = 10^{8} $M$_\odot$'],
    handler_map={tuple:HandlerTuple(ndivide=3)}, loc='upper left',
    bbox_to_anchor=(0.98, 1.065))

  axs[3].legend([r'$ \propto t$'], loc='upper left',
                bbox_to_anchor=(0.98, 1.065))

  plt.show() if save:
    fname = ('../galaxies/dipersion_heating_rescaled_alpha_' + str(alpha) +
             '_beta_' + str(beta))
    plt.savefig(fname, bbox_inches='tight')
    # plt.close()

  return

def plot_heating_grid_less_than(save=True, overwrite=False, delta=True):
  '''
  '''
  names = ['mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps',
           'mu_25/fdisk_0p01_lgMdm_7p5_V200-200kmps',
           'mu_25/fdisk_0p01_lgMdm_7p0_V200-200kmps',
           'mu_25/fdisk_0p01_lgMdm_6p5_V200-200kmps',
           'mu_25/fdisk_0p01_lgMdm_6p0_V200-200kmps']

  colors = {'mu_25/fdisk_0p01_lgMdm_8p0_V200-200kmps':'C0',
            'mu_25/fdisk_0p01_lgMdm_7p5_V200-200kmps':'C1',
            'mu_25/fdisk_0p01_lgMdm_7p0_V200-200kmps':'C2',
            'mu_25/fdisk_0p01_lgMdm_6p5_V200-200kmps':'C3',
            'mu_25/fdisk_0p01_lgMdm_6p0_V200-200kmps':'C4'}

  #set up plot
  fig, axs = plt.subplots(nrows=5, ncols=3, sharex=True, sharey=True,
                          figsize=(10, 15))
  fig.subplots_adjust(hspace=0.03, wspace=0.03)

  for name in names:
    (delta_upsilon_r_25, delta_upsilon_r_50, delta_upsilon_r_75,
     delta_upsilon_z_25, delta_upsilon_z_50, delta_upsilon_z_75,
     delta_upsilon_phi_25, delta_upsilon_phi_50, delta_upsilon_phi_75,
     delta_upsilon_tot_25, delta_upsilon_tot_50, delta_upsilon_tot_75,
     delta_mean_phi_25, delta_mean_phi_50, delta_mean_phi_75
     ) = get_upsilon(name, overwrite, delta, v_200_scale=True, less_than=True)

    (tau_25, tau_50, tau_75
     ) = get_scale_time(name, alpha=0, beta=0, v_200_scale=True)

    #points on plot
    if delta:
      axs[0, 0].errorbar(np.log10(tau_25)+4, np.log10(delta_upsilon_z_25), ls='',
                          fmt='.', alpha=0.2, c=colors[name])
      axs[0, 1].errorbar(np.log10(tau_50)+4, np.log10(delta_upsilon_z_50), ls='',
                          fmt='.', alpha=0.2, c=colors[name])
      axs[0, 2].errorbar(np.log10(tau_75)+4, np.log10(delta_upsilon_z_75), ls='',
                          fmt='.', alpha=0.2, c=colors[name])

      axs[1, 0].errorbar(np.log10(tau_25)+4, np.log10(delta_upsilon_r_25), ls='',
                          fmt='.', alpha=0.2, c=colors[name])
      axs[1, 1].errorbar(np.log10(tau_50)+4, np.log10(delta_upsilon_r_50), ls='',
                          fmt='.', alpha=0.2, c=colors[name])
      axs[1, 2].errorbar(np.log10(tau_75)+4, np.log10(delta_upsilon_r_75), ls='',
                          fmt='.', alpha=0.2, c=colors[name])

      axs[2, 0].errorbar(np.log10(tau_25)+4, np.log10(delta_upsilon_phi_25), ls='',
                          fmt='.', alpha=0.2, c=colors[name])
      axs[2, 1].errorbar(np.log10(tau_50)+4, np.log10(delta_upsilon_phi_50), ls='',
                          fmt='.', alpha=0.2, c=colors[name])
      axs[2, 2].errorbar(np.log10(tau_75)+4, np.log10(delta_upsilon_phi_75), ls='',
                          fmt='.', alpha=0.2, c=colors[name])

      axs[3, 0].errorbar(np.log10(tau_25)+4, np.log10(delta_upsilon_tot_25), ls='',
                          fmt='.', alpha=0.2, c=colors[name])
      axs[3, 1].errorbar(np.log10(tau_50)+4, np.log10(delta_upsilon_tot_50), ls='',
                          fmt='.', alpha=0.2, c=colors[name])
      axs[3, 2].errorbar(np.log10(tau_75)+4, np.log10(delta_upsilon_tot_75), ls='',
                          fmt='.', alpha=0.2, c=colors[name])

      axs[4, 0].errorbar(np.log10(tau_25)+4, np.log10(-delta_mean_phi_25), ls='',
                         fmt='.', alpha=0.2, c=colors[name])
      axs[4, 1].errorbar(np.log10(tau_50)+4, np.log10(-delta_mean_phi_50), ls='',
                         fmt='.', alpha=0.2, c=colors[name])
      axs[4, 2].errorbar(np.log10(tau_75)+4, np.log10(-delta_mean_phi_75), ls='',
                         fmt='.', alpha=0.2, c=colors[name])

  (v_200, t_c,
   r_25, r_50, r_75,
   rho_25, rho_50, rho_75,
   v_c_25, v_c_50, v_c_75,
   sigma_dm_25, sigma_dm_50, sigma_dm_75
   ) = get_halo_constants(name, overwrite=overwrite)

  #hernquist ananlytic dispersions
  vmax = 250 #km/s
  a_h = 32  #kpc hard coded

  disp_h_25 = get_analytic_hernquist_dispersion(vmax, a_h, r_25)
  disp_h_50 = get_analytic_hernquist_dispersion(vmax, a_h, r_50)
  disp_h_75 = get_analytic_hernquist_dispersion(vmax, a_h, r_75)

  log_sigma_h_25 = np.log10(disp_h_25**2 / v_200**2)
  log_sigma_h_50 = np.log10(disp_h_50**2 / v_200**2)
  log_sigma_h_75 = np.log10(disp_h_75**2 / v_200**2)

  log_v_c_h_25 = np.log10(v_c_25 / v_200)
  log_v_c_h_50 = np.log10(v_c_50 / v_200)
  log_v_c_h_75 = np.log10(v_c_75 / v_200)

  for i in range(4):
    axs[i, 0].errorbar([-5, 2], [log_sigma_h_25, log_sigma_h_25], c='grey',
                       ls=':', linewidth=4)
    axs[i, 1].errorbar([-5, 2], [log_sigma_h_50, log_sigma_h_50], c='grey',
                       ls=':', linewidth=4)
    axs[i, 2].errorbar([-5, 2], [log_sigma_h_75, log_sigma_h_75], c='grey',
                       ls=':', linewidth=4)

  axs[4, 0].errorbar([-5, 2], [log_v_c_h_25, log_v_c_h_25], c='grey',
                     ls=':', linewidth=4)
  axs[4, 1].errorbar([-5, 2], [log_v_c_h_50, log_v_c_h_50], c='grey',
                     ls=':', linewidth=4)
  axs[4, 2].errorbar([-5, 2], [log_v_c_h_75, log_v_c_h_75], c='grey',
                     ls=':', linewidth=4)

  #labels
  if delta:
    axs[0, 0].set_ylabel(r'$\log \Delta \upsilon_{z}^2$')
    axs[1, 0].set_ylabel(r'$\log \Delta \upsilon_{R}^2$')
    axs[2, 0].set_ylabel(r'$\log \Delta \upsilon_{\phi}^2$')
    axs[3, 0].set_ylabel(r'$\log \Delta \upsilon_{tot}^2$')
    axs[4, 0].set_ylabel(r'$\log |\Delta \overline{v_{\phi}}|$')
  else:
    axs[0, 0].set_ylabel(r'$\log \upsilon_{z}^2$')
    axs[1, 0].set_ylabel(r'$\log \upsilon_{R}^2$')
    axs[2, 0].set_ylabel(r'$\log \upsilon_{\phi}^2$')
    axs[3, 0].set_ylabel(r'$\log \upsilon_{tot}^2$')
    axs[4, 0].set_ylabel(r'$\log |\overline{v_{\phi}}|$')
  axs[4, 0].set_xlabel(r'$\log \tau$ [$\times 10^4$]')
  axs[4, 1].set_xlabel(r'$\log \tau$ [$\times 10^4$]')
  axs[4, 2].set_xlabel(r'$\log \tau$ [$\times 10^4$]')

  axs[0, 0].set_xlim(-3.5, 0.5)
  axs[0, 0].set_ylim(-3.5, 0.2)
  axs[0, 0].set_xticks([-4, -3, -2, -1, 0])
  axs[0, 0].set_yticks([-4, -3, -2, -1, 0])

  plt.show() if save:
    fname = '../galaxies/dipersion_heating_less_than'
    plt.savefig(fname, bbox_inches='tight')
    # plt.close()

  return

###############################################################################

def save_time_ellipticity_fractions(overwrite=True):
  '''
  '''
  file = '../galaxies/time_ellipticity_fractions'

  if os.path.isfile(file+'.h5') and not overwrite:

    out = load_time_ellipticity_fractions()

    return(out)

  h5file = h5.File(file+'.h5', 'w')

  muse = ['25', '5', '1']
  m_dm = ['8p0', '7p5', '7p0', '6p5', '6p0']
  snaps = [400, 400, 101, 101, 101]

  radii_edges = np.hstack((np.arange(0, 31, 1), 200))
  radii_bin_names = [str(radii_edges[i])+'to'+str(radii_edges[i+1])
                     for i in range(len(radii_edges)-1)]
  jzonc_edges = np.hstack((-1e4, np.arange(-1.5, 1.6, 0.1), 1e4))
  jzonc_bin_names = [str(np.round(jzonc_edges[i], 1))+'to'+
                     str(np.round(jzonc_edges[i+1], 1))
                     for i in range(len(jzonc_edges)-1)]

  n_mu = len(muse)
  n_m = len(m_dm)

  for i in range(n_mu):

    #h5 folder
    mu_group = h5file.create_group(muse[i])

    for j in range(n_m):

      #skip the loner
      if not((muse[i] == '1') and (m_dm[j] == '6p0')):

        #galaxy name
        name = 'mu_{0}/fdisk_0p01_lgMdm_{1}_v_200-200kmps'.format(muse[i], m_dm[j])

        #h5 folder
        galaxy_group = mu_group.create_group(m_dm[j])

        #store data to be put into h5
        jzonc_values = np.zeros((snaps[j]+1, len(radii_bin_names),
                                 len(jzonc_bin_names)), dtype=int)

        #do the calculation at each time
        for t in range(snaps[j]+1):

          #load galaxy
          snap = '{0:03d}'.format(int(t))
          print('\n' + name + '_snap_' + snap)

          (file_name, mass_dm, pos_dms, vel_dms, id_dms,
                    mass_star, pos_stars, vel_stars, id_stars,
                    j_z_stars, j_p_stars, j_c_stars, energy_stars,
                    e_interp, j_circ_interp, r_interp
                    ) = load_nbody.load_and_calculate(name, snap)

          j_zonc_stars = j_z_stars / j_c_stars
          r_stars = np.linalg.norm(pos_stars, axis=1)

          jzonc_values[t], _, _ = np.histogram2d(
            r_stars, j_zonc_stars, density=False, bins=[radii_edges, jzonc_edges])

        #save values
        radii_bin_groups = np.zeros(len(radii_bin_names), dtype=object)
        for k in range(len(radii_bin_names)):
          radii_bin_groups[k] = galaxy_group.create_group(radii_bin_names[k])
          for l in range(len(jzonc_bin_names)):
            radii_bin_groups[k].create_dataset(jzonc_bin_names[l],
                                               data=jzonc_values[:, k, l])

  h5file.close()

  out = load_time_ellipticity_fractions()

  return(out)

def load_time_ellipticity_fractions():
  '''
  '''
  #arrays data is saved as
  muse = ['25', '5', '1']
  m_dm = ['8p0', '7p5', '7p0', '6p5', '6p0']
  radii_edges = np.hstack((np.arange(0, 31, 1), 200))
  radii_bin_names = [str(radii_edges[i])+'to'+str(radii_edges[i+1])
                     for i in range(len(radii_edges)-1)]
  jzonc_edges = np.hstack((-1e4, np.arange(-1.5, 1.6, 0.1), 1e4))
  jzonc_bin_names = [str(np.round(jzonc_edges[i], 1))+'to'+
                     str(np.round(jzonc_edges[i+1], 1))
                     for i in range(len(jzonc_edges)-1)]

  n_mu = len(muse)
  n_m = len(m_dm)

  #load file
  h5file = h5.File('../galaxies/time_ellipticity_fractions.h5', 'r')

  total_dict = dict()

  for i in range(n_mu):
    #dict
    mu_dict = dict()
    total_dict[muse[i]] = mu_dict

    for j in range(n_m):

      #skip the loner
      if not((muse[i] == '1') and (m_dm[j] == '6p0')):

        #dict
        galaxy_dict = dict()
        mu_dict[m_dm[j]] = galaxy_dict

        for k in range(len(radii_bin_names)):

          #dict
          radii_dict = dict()
          galaxy_dict[radii_bin_names[k]] = radii_dict

          for l in range(len(jzonc_bin_names)):

            radii_dict[jzonc_bin_names[l]] = h5file[
              muse[i]][m_dm[j]][radii_bin_names[k]][jzonc_bin_names[l]][:]

  h5file.close()

  return(total_dict)

def find_tanh_params(xdata, ydata, yerrors=1):
  '''
  '''
  #remove nans
  nan_mask = np.isnan(ydata)
  xdata = xdata[~nan_mask]
  ydata = ydata[~nan_mask]
  yerrors = yerrors[~nan_mask]

  #model
  y_model = lambda x, y_inf, delta_y, m_half, x_half: y_inf + delta_y * (
    (1 - np.tanh(m_half * (x - x_half))) / 2)

  #least squares
  function = lambda theta: np.sum(
    (ydata - y_model(xdata, theta[0], theta[1], theta[2], theta[3]))**2 / yerrors**2)

  guess_0 = np.nanmin(ydata)
  guess_1 = np.nanmax(ydata) - guess_0

  #sort
  x_order = np.argsort(xdata)
  xdata = xdata[x_order][5:]
  ydata = ydata[x_order][5:]
  yerrors = yerrors[x_order][5:]

  problems = np.where(np.diff(xdata)==0)
  for p in problems:
    xdata[p+1] += 1e-4

  # spline = InterpolatedUnivariateSpline(xdata, ydata)
  spline = UnivariateSpline(xdata, ydata)
  find_mid = lambda x: spline(x) - (guess_0 + guess_1/2)
  try:
    guess_3 = brentq(find_mid, -3, 0)
  except ValueError:
    guess_3 = 0

  print(np.around((guess_0, guess_1, 1.5, guess_3), 2))

  out = minimize(function, (guess_0, guess_1, 1.5, guess_3))

  print(np.around(out.x, 2))
  print(out.message)
  print(out.success)

  return(out.x)

def plot_disk_evolution(save=False, log_time=True,
                        radii_from_zero=False, plot_negative=True):
  '''
  '''
  total_dict = load_time_ellipticity_fractions()

  # muse = ['25', '5', '1']
  muse = ['25']
  m_dm = ['8p0', '7p5', '7p0', '6p5', '6p0']
  m_dm_value = [8.0, 7.5, 7.0, 6.5, 6.0]
  snaps = [400, 400, 101, 101, 101]
  m_col = ['C0', 'C1', 'C2', 'C3', 'C4']

  radii_edges = np.hstack((np.arange(0, 31, 1), 200))
  radii_bin_names = [str(radii_edges[i])+'to'+str(radii_edges[i+1])
                     for i in range(len(radii_edges)-1)]
  jzonc_edges = np.hstack((-1e4, np.arange(-1.5, 1.6, 0.1), 1e4))
  jzonc_bin_names = [str(np.round(jzonc_edges[i], 1))+'to'+
                     str(np.round(jzonc_edges[i+1], 1))
                     for i in range(len(jzonc_edges)-1)]

  n_mu = len(muse)
  n_m = len(m_dm)

  snaps = [400, 400, 101, 101, 101]

  #quantities of interest
  radii_bins_of_interest = [0, 4, 7, 11, 31]
  n_r = len(radii_bins_of_interest) - 1
  jzonc_bins_of_interest = [1, 0.8, 0.6, 0.4, 0.2, 0]
  n_j = len(jzonc_bins_of_interest)

  #set up plots
  fig, axs = plt.subplots(nrows=n_j, ncols=n_r, sharex=True, sharey=True,
                          figsize=(18, 18))
  fig.subplots_adjust(hspace=0.03, wspace=0.02)

  #save for fitting
  data_to_fit = np.zeros((n_r, n_j, sum(snaps)+len(snaps), 3))

  #mass ratios
  for i in range(n_mu):
    #dm particle masses
    for j in range(n_m):
      #skip the loner
      if not((muse[i] == '1') and (m_dm[j] == '6p0')):

        #time
        t_c = (GRAV_CONST**2 * 200 * RHO_CRIT * 10**(m_dm_value[j]-10) /
               150**3 * PC_ON_M / GYR_ON_S)

        time = np.linspace(0, T_TOT, snaps[j]+1)
        scale_time = np.log10(time * t_c) + 4

        #radii
        for k in range(n_r):

          if radii_from_zero:
            radii_total = np.sum([[
                total_dict[muse[i]][m_dm[j]][radii_bin_names[m]][jzonc_bin_names[n]]
                for n in range(len(jzonc_bin_names))]
                for m in range(0, radii_bins_of_interest[k+1])], axis=(0, 1))

          else:
            #get total particles from radii of interest[k] to radii of interest[k+1]
            radii_total = np.sum([[
                total_dict[muse[i]][m_dm[j]][radii_bin_names[m]][jzonc_bin_names[n]]
                for n in range(len(jzonc_bin_names))]
                for m in range(radii_bins_of_interest[k],
                               radii_bins_of_interest[k+1])], axis=(0, 1))

          #jzoncs
          for l in range(n_j):

            if radii_from_zero:
              jzonc_total = np.sum([[
                  total_dict[muse[i]][m_dm[j]][radii_bin_names[m]][jzonc_bin_names[n]]
                  for n in range(np.where(np.round(jzonc_edges, 1) ==
                                          jzonc_bins_of_interest[l])[0][0],
                                 len(jzonc_bin_names))]
                  for m in range(0, radii_bins_of_interest[k+1])], axis=(0, 1))

            else:
              #get particles in jzonc range from jzonc_bins_of_interest[l] to 1.5
              #from radii of interest[k] to radii of interest[k+1]
              jzonc_total = np.sum([[
                  total_dict[muse[i]][m_dm[j]][radii_bin_names[m]][jzonc_bin_names[n]]
                  for n in range(np.where(np.round(jzonc_edges, 1) ==
                                          jzonc_bins_of_interest[l])[0][0],
                                 len(jzonc_bin_names))]
                  for m in range(radii_bins_of_interest[k],
                                 radii_bins_of_interest[k+1])], axis=(0, 1))

            fraction = jzonc_total / radii_total

            #plot
            if log_time:
              axs[l, k].errorbar(scale_time, fraction, c=m_col[j])
            else:
              axs[l, k].errorbar(10**scale_time, fraction, c=m_col[j])

            data_to_fit[k, l, (sum(snaps[:j])+j):(sum(snaps[:j+1])+j+1),
                        0] = scale_time
            data_to_fit[k, l, (sum(snaps[:j])+j):(sum(snaps[:j+1])+j+1),
                        1] = fraction
            # data_to_fit[k, l, (sum(snaps[:j])+j):(sum(snaps[:j+1])+j+1),
            #             2] = 1 / np.sqrt(jzonc_total)
            data_to_fit[k, l, (sum(snaps[:j])+j):(sum(snaps[:j+1])+j+1),
                        2] = 1 / np.sqrt(radii_total)

            if plot_negative:

              if radii_from_zero:
                jzonc_total = np.sum([[
                  total_dict[muse[i]][m_dm[j]][radii_bin_names[m]][jzonc_bin_names[n]]
                  for n in range(0, np.where(np.round(jzonc_edges, 1) ==
                                             -jzonc_bins_of_interest[l])[0][0])]
                  for m in range(radii_bins_of_interest[k],
                                 radii_bins_of_interest[k+1])], axis=(0, 1))
              else:
                jzonc_total = np.sum([[
                  total_dict[muse[i]][m_dm[j]][radii_bin_names[m]][jzonc_bin_names[n]]
                  for n in range(0, np.where(np.round(jzonc_edges, 1) ==
                                             -jzonc_bins_of_interest[l])[0][0])]
                  for m in range(radii_bins_of_interest[k],
                                 radii_bins_of_interest[k+1])], axis=(0, 1))

              fraction = jzonc_total / radii_total
              #plot
              if log_time:
                axs[l, k].errorbar(scale_time, fraction, c=m_col[j])
              else:
                axs[l, k].errorbar(10**scale_time, fraction, c=m_col[j])

  scale_times = np.linspace(-5, 1)
  for k in range(n_r):
    for l in range(n_j):
      #fit to tanh
      fit_theta = find_tanh_params(data_to_fit[k, l, :, 0], data_to_fit[k, l, :, 1],
                                   data_to_fit[k, l, :, 2])

      y_model = fit_theta[0] + fit_theta[1] * (
        (1 - np.tanh(fit_theta[2] * (scale_times - fit_theta[3]))) / 2)

      t_1 = -((2* (1- fit_theta[0]) / fit_theta[1]) - 1) / fit_theta[2] + fit_theta[3]
      t_0 = -((2* (0- fit_theta[0]) / fit_theta[1]) - 1) / fit_theta[2] + fit_theta[3]

      #plot
      if log_time:
        axs[l, k].errorbar(scale_times, y_model, c='k', ls=':',
                           linewidth=5, alpha=0.6)

        axs[l, k].axvline(fit_theta[3], 0, 1, c='k', ls='-.',
                          linewidth=1, alpha=0.8)

        axs[l, k].plot([t_1, t_0], [1, 0], c='k', ls='--', alpha=0.8)

      else:
        axs[l, k].errorbar(10**scale_times, y_model, c='k', ls=':',
                           linewidth=5, alpha=0.6)

        axs[l, k].axvline(10**fit_theta[3], 0, 1, c='k', ls='-.',
                          linewidth=1, alpha=0.8)

        # axs[l, k].plot([10**t_1, 10**t_0], [1, 0], c='k', ls='--', alpha=0.8)

  if log_time:
    axs[0, 0].set_xlim((-4, 1))
  else:
    axs[0, 0].set_xlim((0, 1.5))
  axs[0, 0].set_ylim((0, 1))

  if log_time:
    axs[n_j-1, int(np.floor((n_r-1)/2))].set_xlabel(r'$\log \tau$ [$\times 10^4$]')
  else:
    axs[n_j-1, int(np.floor((n_r-1)/2))].set_xlabel(r'$\tau$ [$\times 10^4$]')
  [axs[j, 0].set_ylabel(r'$F(j_z/j_c > $' + str(jzonc_bins_of_interest[j]) + ')')
   for j in range(n_j)]
  [axs[0, i].set_title(str(radii_bins_of_interest[i])+'kpc to '+
                       str(radii_bins_of_interest[i+1]) +'kpc') for i in range(n_r)]

  plt.show() if save:
    fname = '../galaxies/integrated_jzonc_evolution'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  return

def plot_evolution_meta(save=False, radii_from_zero=False):
  '''
  '''
  total_dict = load_time_ellipticity_fractions()

  # muse = ['25', '5', '1']
  muse = ['25']
  m_dm = ['8p0', '7p5', '7p0', '6p5', '6p0']
  m_dm_value = [8.0, 7.5, 7.0, 6.5, 6.0]
  snaps = [400, 400, 101, 101, 101]

  radii_edges = np.hstack((np.arange(0, 31, 1), 200))
  radii_bin_names = [str(radii_edges[i])+'to'+str(radii_edges[i+1])
                     for i in range(len(radii_edges)-1)]
  jzonc_edges = np.hstack((-1e4, np.arange(-1.5, 1.6, 0.1), 1e4))
  jzonc_bin_names = [str(np.round(jzonc_edges[i], 1))+'to'+
                     str(np.round(jzonc_edges[i+1], 1))
                     for i in range(len(jzonc_edges)-1)]

  n_mu = len(muse)
  n_m = len(m_dm)

  snaps = [400, 400, 101, 101, 101]

  #quantities of interest
  # radii_bins_of_interest = [0, 4, 7, 11, 31]
  radii_bins_of_interest = np.arange(0, 32, 1)
  n_r = len(radii_bins_of_interest) - 1
  # jzonc_bins_of_interest = [1, 0.8, 0.6, 0.4, 0.2, 0]
  # jzonc_bins_of_interest = np.arange(1, -1.1, -.1)
  jzonc_bins_of_interest = np.arange(0.9, -0.1, -.1)
  n_j = len(jzonc_bins_of_interest)

  #set up plots
  fig, axs = plt.subplots(nrows=4, ncols=1, sharex=True, #sharey=True,
                          figsize=(7, 10))
  fig.subplots_adjust(hspace=0.03, wspace=0.03)

  #save for fitting
  data_to_fit = np.zeros((n_r, n_j, sum(snaps)+len(snaps), 3))

  #mass ratios
  for i in range(n_mu):
    #dm particle masses
    for j in range(n_m):
      #skip the loner
      if not((muse[i] == '1') and (m_dm[j] == '6p0')):

        #time
        t_c = (GRAV_CONST**2 * 200 * RHO_CRIT * 10**(m_dm_value[j]-10) /
               150**3 * PC_ON_M / GYR_ON_S)

        time = np.linspace(0,T_TOT,snaps[j]+1)
        scale_time = np.log10(time * t_c) + 4

        #radii
        for k in range(n_r):

          if radii_from_zero:
            radii_total = np.sum([[
                total_dict[muse[i]][m_dm[j]][radii_bin_names[m]][jzonc_bin_names[n]]
                for n in range(len(jzonc_bin_names))]
                for m in range(0, radii_bins_of_interest[k+1])], axis=(0, 1))

          else:
            #get total particles from radii of interest[k] to radii of interest[k+1]
            radii_total = np.sum([[
                total_dict[muse[i]][m_dm[j]][radii_bin_names[m]][jzonc_bin_names[n]]
                for n in range(len(jzonc_bin_names))]
                for m in range(radii_bins_of_interest[k],
                               radii_bins_of_interest[k+1])], axis=(0, 1))

          #jzoncs
          for l in range(n_j):

            if radii_from_zero:
              jzonc_total = np.sum([[
                  total_dict[muse[i]][m_dm[j]][radii_bin_names[m]][jzonc_bin_names[n]]
                  for n in range(np.where(np.round(jzonc_edges, 1) ==
                                          jzonc_bins_of_interest[l])[0][0],
                                 len(jzonc_bin_names))]
                  for m in range(0, radii_bins_of_interest[k+1])], axis=(0, 1))

            else:
              #get particles in jzonc range from jzonc_bins_of_interest[l] to 1.5
              #from radii of interest[k] to radii of interest[k+1]

              jzonc_total = np.sum([[
                  total_dict[muse[i]][m_dm[j]][radii_bin_names[m]][jzonc_bin_names[n]]
                  for n in range(np.where(np.round(jzonc_edges, 1) ==
                                          np.round(jzonc_bins_of_interest[l],
                                                   1))[0][0],
                                 len(jzonc_bin_names))]
                  for m in range(radii_bins_of_interest[k],
                                 radii_bins_of_interest[k+1])], axis=(0, 1))

            #keep data for later
            fraction = jzonc_total / radii_total

            data_to_fit[k, l, (sum(snaps[:j])+j):(sum(snaps[:j+1])+j+1),
                        0] = scale_time
            data_to_fit[k, l, (sum(snaps[:j])+j):(sum(snaps[:j+1])+j+1),
                        1] = fraction
            # data_to_fit[k, l, (sum(snaps[:j])+j):(sum(snaps[:j+1])+j+1),
            #             2] = 1 / np.sqrt(jzonc_total)
            data_to_fit[k, l, (sum(snaps[:j])+j):(sum(snaps[:j+1])+j+1),
                        2] = 1 / np.sqrt(radii_total)

  cols = viridis(np.linspace(0, 1, n_r))
  for k in reversed(range(n_r)):

    #place to save fit values
    f_inf = np.zeros(n_j)
    delta_f = np.zeros(n_j)
    n_12 = np.zeros(n_j)
    t_12 = np.zeros(n_j)

    for l in range(n_j):
      #fit to tanh
      fit_theta = find_tanh_params(data_to_fit[k,l,:,0], data_to_fit[k,l,:,1],
                                   data_to_fit[k,l,:,2])

      #save fit values
      f_inf[l] = fit_theta[0]
      delta_f[l] = fit_theta[1]
      n_12[l] = (1-fit_theta[2]) / 2
      t_12[l] = fit_theta[3]

    #remove crazy bins
    if not np.any(f_inf < 0):

      #plot
      axs[0].plot(jzonc_bins_of_interest, f_inf, c=cols[k])
      axs[1].plot(jzonc_bins_of_interest, delta_f, c=cols[k])
      axs[2].plot(jzonc_bins_of_interest, n_12, c=cols[k])
      axs[3].plot(jzonc_bins_of_interest, t_12, c=cols[k])

  axs[0].set_xlim((jzonc_bins_of_interest[-1], jzonc_bins_of_interest[0]))
  axs[0].set_ylim((0, 1.1))
  axs[1].set_ylim((0, 1.1))
  axs[2].set_ylim((-5, 0.5))
  axs[3].set_ylim((-2.5, 0.5))
  axs[0].set_ylabel(r'$F_\infty$')
  axs[1].set_ylabel(r'$\Delta F$')
  axs[2].set_ylabel(r'$n_{1/2}$')
  axs[3].set_ylabel(r'$\log$ $\tau_{1/2}$')
  axs[3].set_xlabel(r'$F (j_z / j_c(E) > X)$')

  fig.subplots_adjust(right=0.8)
  cbar_ax = fig.add_axes([0.81, 0.11, 0.04, 0.77])
  cbar = fig.colorbar(ScalarMappable(norm=Normalize(vmin=radii_bins_of_interest[0],
                      vmax=radii_bins_of_interest[-1]), cmap=viridis), cax=cbar_ax)
  cbar.set_label(r'R [kpc]')

  plt.show() if save:
    fname = '../galaxies/integrated_evolution_meta'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  return

###############################################################################

def plot_dispersions(name, snaps, save=False):

  (mean_v_R, mean_v_phi, sigma_z, sigma_R, sigma_phi) = get_dispersions(name, snaps)

  sigma_dm = get_velocity_scale(name, v200=False, dm_dispersion=True)

  #color by radius
  #TODO add color bar
  #TODO add smoothing
  t = np.linspace(0, T_TOT, snaps+1)
  thirty = 30
  cols = viridis(np.linspace(1, 0, thirty))

  fig = plt.figure()
  fig.set_size_inches(10, 20)
  fig.subplots_adjust(hspace=0.03,wspace=0)

  ax1 = fig.add_subplot(411)
  ax2 = fig.add_subplot(412)
  ax3 = fig.add_subplot(413)
  ax4 = fig.add_subplot(414)

  for i in range(thirty):
    ax1.errorbar(t, sigma_z[:,i],    alpha=0.6, c=cols[i])
    ax2.errorbar(t, sigma_R[:,i],    alpha=0.6, c=cols[i])
    ax3.errorbar(t, sigma_phi[:,i],  alpha=0.6, c=cols[i])
    ax4.errorbar(t, mean_v_phi[:,i], alpha=0.6, c=cols[i])

  ax4.set_xlabel(r'$t$ [Gyr]')
  ax1.set_ylabel(r'$\sigma_z$ [km/s]')
  ax2.set_ylabel(r'$\sigma_R$ [km/s]')
  ax3.set_ylabel(r'$\sigma_\phi$ [km/s]')
  ax4.set_ylabel(r'$\overline{v_\phi}$ [km/s]')

  ax1.set_xlim([0, T_TOT])
  ax2.set_xlim([0, T_TOT])
  ax3.set_xlim([0, T_TOT])
  ax4.set_xlim([0, T_TOT])

  #color by time
  x = np.linspace(0.5, 29.5, 30)
  cols = viridis(np.linspace(1, 0, snaps+1))

  fig = plt.figure()
  fig.set_size_inches(10, 20)
  fig.subplots_adjust(hspace=0.03,wspace=0)

  ax1 = fig.add_subplot(411)
  ax2 = fig.add_subplot(412)
  ax3 = fig.add_subplot(413)
  ax4 = fig.add_subplot(414)

  for i in range(snaps+1):
    ax1.errorbar(x, sigma_z[i,:], alpha=0.6, c=cols[i])
    ax2.errorbar(x, sigma_R[i,:], alpha=0.6, c=cols[i])
    ax3.errorbar(x, sigma_phi[i,:], alpha=0.6, c=cols[i])
    ax4.errorbar(x, mean_v_phi[i,:], alpha=0.6, c=cols[i])

  ax1.errorbar(x, sigma_dm, c='grey', ls='--')
  ax2.errorbar(x, sigma_dm, c='grey', ls='--')
  ax3.errorbar(x, sigma_dm, c='grey', ls='--')

  ax4.set_xlabel(r'$R$ [kpc]')
  ax1.set_ylabel(r'$\sigma_z$ [km/s]')
  ax2.set_ylabel(r'$\sigma_R$ [km/s]')
  ax3.set_ylabel(r'$\sigma_\phi$ [km/s]')
  ax4.set_ylabel(r'$\overline{v_\phi}$ [km/s]')

  ax1.set_xlim([0, 30])
  ax2.set_xlim([0, 30])
  ax3.set_xlim([0, 30])
  ax4.set_xlim([0, 30])

  return

if __name__ == '__main__':

  # r_25 = 4
  # r_50 = 7
  # r_75 = 11

  # snap = '000'
  # (file_name, mass_dm, pos_dms, vel_dms, id_dms,
  #  mass_star, pos_stars, vel_stars, id_stars,
  #  j_z_stars, j_p_stars, j_c_stars, energy_stars,
  #  e_interp, j_circ_interp, r_interp
  #  ) = load_nbody.load_and_calculate('mu_25/fdisk_0p01_lgMdm_6p0_V200-200kmps', snap)

  # print(get_fiducial_constants(pos_dms, mass_dm, r_25, r_50, r_75, pct=0.1))

  # snap = '101'
  # (file_name, mass_dm, pos_dms, vel_dms, id_dms,
  #  mass_star, pos_stars, vel_stars, id_stars,
  #  j_z_stars, j_p_stars, j_c_stars, energy_stars,
  #  e_interp, j_circ_interp, r_interp
  #  ) = load_nbody.load_and_calculate('mu_25/fdisk_0p01_lgMdm_6p0_V200-200kmps', snap)

  # print(get_fiducial_constants(pos_dms, mass_dm, r_25, r_50, r_75, pct=0.1))


  plot_heating_grid(save=False, overwrite=False)

  # plot_heating_grid_less_than(save=False, overwrite=False)

  # evaluate_alpha_beta_gamma(save=False)

  # test_constants(save=False)

  # save_time_ellipticity_fractions(overwrite=False)
  # load_time_ellipticity_fractions()

  # plot_disk_evolution(save=False, log_time=True)

  # plot_evolution_meta(save=False)

  pass