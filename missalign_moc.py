#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 23 2022

@author: Matthew Wilkinson
"""

import numpy as np

import matplotlib.pyplot as plt

from scipy.optimize import brentq

from scipy.integrate import dblquad
from scipy.integrate import quad

from scipy.interpolate import interp2d

import imageio

from functools import lru_cache

GRAV_CONST = 4.301e4  # kpc (km/s)^2 / (10^10Msun) (source ??)
HUBBLE_CONST = 0.06777  # km/s/kpc (Planck 2018)
GALIC_HUBBLE_CONST = 0.1  # h km/s/kpc
RHO_CRIT = 3 * HUBBLE_CONST ** 2 / (8 * np.pi * GRAV_CONST)  # 10^10Msun/kpc^3

PC_ON_M = 3.0857e16  # pc/m = kpc/km (wiki)
GYR_ON_S = 3.15576e16  # gyr/s

T_TOT = 9.778943899409334  # Gyr



def exponential_cumulative_mass(R, disk_mass, R_d):
    M = disk_mass * (1 - ((R_d + R) / R_d) * np.exp(-R / R_d))
    return (M)


#logarithmic potential
class Ring(object):
    def __init__(self, mass, n_rings, vc, Rd, inclination):
        self.mass = mass
        self.n_rings = n_rings
        self.mass_per_ring = self.mass / self.n_rings

        self.vc = vc
        self.Rd = Rd

        self.ring_radii = np.zeros(self.n_rings)

        self.ring_angles = inclination * np.ones(self.n_rings)
        self.ring_angular_velocity = np.zeros(self.n_rings) #radians / Gyr

        f = lambda R, disk_mass, Rd, target_mass: target_mass - exponential_cumulative_mass(
            R, disk_mass, Rd)

        for i in range(self.n_rings):
            self.ring_radii[i] = brentq(f, 0, 100*Rd,
                                        args=(self.mass, self.Rd, (i + 0.5) / self.n_rings * self.mass))

        self.ring_moment_of_inertia = 0.5 * self.ring_radii**2 * self.mass_per_ring # 10^10 M_sun kpc^2

    # def calculate_jz(self):
    #     jz = np.sum( self.ring_radii * np.cos(self.ring_radii) * self.vc)
    #     return jz

    def calculate_J(self):
        J = self.ring_radii * self.vc
        Jx = np.zeros(len(self.n_rings))
        Jy = J * np.sin(self.ring_angles)
        Jz = J * np.cos(self.ring_angles)

        return (Jx, Jy, Jz)


class Galaxy(object):
    def __init__(self,
                 disk_mass_1 = 1, disk_mass_2 = 1, Rd_1 = 4, Rd_2 = 4,
                 I_tilt = np.pi/4, n_rings=80, vc=200):

        self.Ring_1 = Ring(disk_mass_1, n_rings, vc, Rd_1, I_tilt / 2)
        self.Ring_2 = Ring(disk_mass_2, n_rings, vc, Rd_2, -I_tilt / 2)

        self.interped_normed_torque = None


    def calculate_torque_library(self):
        max_ratio = np.amax((self.Ring_1.ring_radii, self.Ring_2.ring_radii)) / np.amin(
            (self.Ring_1.ring_radii, self.Ring_2.ring_radii))
        min_ratio = np.amin((self.Ring_1.ring_radii, self.Ring_2.ring_radii)) / np.amax(
            (self.Ring_1.ring_radii, self.Ring_2.ring_radii))

        r_1d = np.logspace(np.log10(max_ratio), np.log10(min_ratio), 81)
        angle_1d = np.linspace(0, np.pi, 81)
        # r_grid, angle_1d = np.meshgrid(r_1d, angle_1d)
        normed_torque = np.array([[calculate_torque_between_rings(1, r, angle)
                                   for r in r_1d] for angle in angle_1d])

        self.interped_normed_torque = interp2d(r_1d, angle_1d, normed_torque,
                                               fill_value=0, kind='cubic') #'linear'

        print('Finished Calculating Torque Library')

        return


    def calculate_torque(self, r1, r2, angle):
        if self.interped_normed_torque == None:
            self.calculate_torque_library()

        torque = 1/r1 * self.interped_normed_torque(r2, angle)

        return(torque)


    def calculate_torque_on_all_rings(self):
        ring1_torques = np.zeros(self.Ring_1.n_rings)
        ring2_torques = np.zeros(self.Ring_2.n_rings)

        for i1 in range(self.Ring_1.n_rings):
            for i2 in range(self.Ring_2.n_rings):
                ring1_torques[i1] += self.calculate_torque(
                    self.Ring_1.ring_radii[i1], self.Ring_2.ring_radii[i2],
                    (self.Ring_1.ring_angles[i1] - self.Ring_2.ring_angles[i2]) % np.pi
                ) * self.Ring_2.mass_per_ring #10^10 M_sun kpc^-1
            for i2 in range(self.Ring_1.n_rings):
                ring1_torques[i1] += self.calculate_torque(
                    self.Ring_1.ring_radii[i1], self.Ring_1.ring_radii[i2],
                    (self.Ring_1.ring_angles[i1] - self.Ring_1.ring_angles[i2]) % np.pi
                ) * self.Ring_1.mass_per_ring #10^10 M_sun kpc^-1

        for i2 in range(self.Ring_2.n_rings):
            for i1 in range(self.Ring_1.n_rings):
                ring2_torques[i2] += self.calculate_torque(
                    self.Ring_2.ring_radii[i2], self.Ring_1.ring_radii[i1],
                    (self.Ring_2.ring_angles[i2] - self.Ring_1.ring_angles[i1]) % np.pi
                ) * self.Ring_1.mass_per_ring #10^10 M_sun kpc^-1
            for i1 in range(self.Ring_2.n_rings):
                ring2_torques[i2] += self.calculate_torque(
                    self.Ring_2.ring_radii[i2], self.Ring_2.ring_radii[i1],
                    (self.Ring_2.ring_angles[i2] - self.Ring_2.ring_angles[i1]) % np.pi
                ) * self.Ring_2.mass_per_ring #10^10 M_sun kpc^-1

        ring1_torques *= GRAV_CONST * self.Ring_1.mass_per_ring # 10^10 Msum (km/s)^2
        ring2_torques *= GRAV_CONST * self.Ring_2.mass_per_ring # = (N m (10^10 Msun/kg))

        ring1_torques *= PC_ON_M**2 / GYR_ON_S**2 # 10^10 Msun (kpc/Gyr)^2
        ring2_torques *= PC_ON_M**2 / GYR_ON_S**2

        return(ring1_torques, ring2_torques) # 10^10 Msun (kpc/Gyr)^2


    #dt in Gyr
    def update_angles(self, dt=0.0005):
        # kick-drift angular velocty but I make the velocity the middle step.
        # Don't know why this isn't the default.

        self.Ring_1.ring_angles += self.Ring_1.ring_angular_velocity * dt / 2
        self.Ring_2.ring_angles += self.Ring_2.ring_angular_velocity * dt / 2

        (ring1_torques, ring2_torques) = self.calculate_torque_on_all_rings() #10^10 Msun (kpc/Gyr)^2
        #tau = dJ / dt
        #tau = I d omega / dt
        # d^2 phi / dt^2 = tau / I

        self.Ring_1.ring_angular_velocity += (ring1_torques / self.Ring_1.ring_moment_of_inertia * dt)
        self.Ring_2.ring_angular_velocity += (ring2_torques / self.Ring_1.ring_moment_of_inertia * dt)

        self.Ring_1.ring_angles += self.Ring_1.ring_angular_velocity * dt / 2
        self.Ring_2.ring_angles += self.Ring_2.ring_angular_velocity * dt / 2

        # print(self.Ring_1.ring_angles)
        # print(self.Ring_2.ring_angles)
        # print()

        return()


def calculate_torque_between_rings(r1, r2, tilt):

    # if -0.1 < tilt < 0.1:
    #     return(0)

    soft2 = 1e-2 #kpc^2

    # ring 1 positions #x-y plane #kpc
    x1 = lambda t1 : r1 * np.cos(t1)
    y1 = lambda t1 : r1 * np.sin(t1)
    z1 = lambda t1 : 0

    # ring 2 positions #rotate about the x axis #kpc
    x2 = lambda t2 : r2 * np.cos(t2) * np.cos(tilt)
    y2 = lambda t2 : r2 * np.sin(t2)
    z2 = lambda t2 : r2 * np.cos(t2) * np.sin(tilt)

    #distance between two points on the rings. #kpc^2
    _r2 = lambda t1, t2 : ((x1(t1) - x2(t2))**2 +
                           (y1(t1) - y2(t2))**2 +
                           (z1(t1) - z2(t2))**2 + soft2)

    #1/r^3 #kpc^-3
    r1on3 = lambda t1, t2 : 1 / np.power(_r2(t1, t2), 3/2)

    #vec{x} / r^3 #kpc^-2
    #force in the x,y,z direction on two points on the each ring
    f_x = lambda t2, t1: (x1(t1) - x2(t2)) * r1on3(t1, t2)
    f_y = lambda t2, t1: (y1(t1) - y2(t2)) * r1on3(t1, t2)
    f_z = lambda t2, t1: (z1(t1) - z2(t2)) * r1on3(t1, t2)

    #TODO get a good analytic approximation to speed up
    #force in the x,y,z direction of the entire ring 2 on a point on ring 1 #kpc^-2
    F_x1 = lambda t1: quad(f_x, 0, 2*np.pi, args=t1, epsabs=1e-2)[0]
    F_y1 = lambda t1: quad(f_y, 0, 2*np.pi, args=t1)[0]
    F_z1 = lambda t1: quad(f_z, 0, 2*np.pi, args=t1, epsabs=1e-2)[0]

    # #total force in the x,y,z direction of the entire ring 2 on ring 1
    # F_x = quad(F_x1, 0, 2*np.pi)[0]
    # F_y = quad(F_y1, 0, 2*np.pi)[0]
    # F_z = quad(F_z1, 0, 2*np.pi)[0]

    #torque in the x,y,z direction of the entire ring 2 on a point on ring 1 #kpc^-1
    # tau_x1 = lambda t1 : y1(t1) * F_z1(t1) - z1(t1) * F_y1(t1)
    tau_x1 = lambda t1 : 0
    tau_y1 = lambda t1 :-x1(t1) * F_z1(t1) + z1(t1) * F_x1(t1)
    # tau_z1 = lambda t1 : x1(t1) * F_y1(t1) - y1(t1) * F_x1(t1)
    tau_z1 = lambda t1 : 0

    #total tarque in the x,y,z direction of the entire ring 2 on ring 1 #kpc^-1
    # tau_x = quad(tau_x1, 0, 2*np.pi)[0]
    tau_x = 0
    tau_y = 2 * quad(tau_y1, 0, np.pi, epsabs=1e-2)[0]
    # tau_z = quad(tau_z1, 0, 2*np.pi)[0]
    tau_z = 0

    #total torque #kpc^-1
    # tau = np.sqrt(tau_x**2 + tau_y**2 + tau_z**2) * np.sign(tau_y)
    tau = -tau_y

    return(tau) # / (G M M)

if __name__ == '__main__':

    This_Galaxy = Galaxy()
    n_steps = 2000
    dt = 0.0005 #gyr

    ring1_angles = np.zeros((This_Galaxy.Ring_1.n_rings, n_steps))
    ring2_angles = np.zeros((This_Galaxy.Ring_2.n_rings, n_steps))

    for i in range(n_steps):
        This_Galaxy.update_angles()
        ring1_angles[:, i] = This_Galaxy.Ring_1.ring_angles
        ring2_angles[:, i] = This_Galaxy.Ring_2.ring_angles

    print('Finished Simulation')

    gif_time = 10 # s
    fps = 30 #frames / s
    n_frames = fps * gif_time

    fname = '/home/matt/Documents/UWA/disk-heating/galaxies/tilt/angle_movie'

    w = imageio.get_writer(fname + '.mp4', format='FFMPEG', mode='I', fps=fps)  # ,
    # codec='h264_vaapi',
    # output_params=['-vaapi_device',
    #               '/dev/dri/renderD128', '-vf',
    #               'format=gray|nv12,hwupload'],
    # pixelformat='vaapi_vld')

    for i in range(n_frames):
        fname = '/home/matt/Documents/UWA/disk-heating/galaxies/tilt/frame{0:04d}.png'.format(int(i))

        plt.figure(figsize=(4,4))
        edge = np.amax((This_Galaxy.Ring_1.ring_radii, This_Galaxy.Ring_2.ring_radii))
        plt.xlim([edge, -edge])
        plt.ylim([edge, -edge])

        plt.text(edge, -edge, 't=' + str(round((i * n_steps // n_frames) * dt, 4)) + 'Gyr', va='top')

        for i1 in range(This_Galaxy.Ring_1.n_rings):
            r = This_Galaxy.Ring_1.ring_radii
            t = ring1_angles[:, i * n_steps // n_frames]
            x = r * np.cos(t)
            y = r * np.sin(t)
            plt.plot( x,  y, c='C0')
            plt.plot(-x, -y, c='C0')

        for i1 in range(This_Galaxy.Ring_2.n_rings):
            r = This_Galaxy.Ring_2.ring_radii
            t = ring2_angles[:, i * n_steps // n_frames]
            x = r * np.cos(t)
            y = r * np.sin(t)
            plt.plot( x,  y, c='C1')
            plt.plot(-x, -y, c='C1')

        plt.savefig(fname, bbox_inches='tight')
        plt.close()

    for i in range(n_frames):
        fname = '/home/matt/Documents/UWA/disk-heating/galaxies/tilt/frame{0:04d}.png'.format(int(i))

        w.append_data(imageio.imread(fname))
    w.close()


    # for i in range(This_Galaxy.Ring_1.n_rings):
    #     plt.figure(i)
    #     plt.plot(range(n_steps), ring1_angles[i,:])
    #     plt.plot(range(n_steps), ring2_angles[i,:])
    #
    # plt.show()

    # x = np.linspace(0, np.pi, 11)[1:-1]
    # torque = [calculate_torque_between_rings(0.25, 0.25, xs) for xs in x]
    # plt.plot(x * 180/np.pi, torque)
    # torque = [calculate_torque_between_rings(0.5, 0.5, xs) for xs in x]
    # plt.plot(x * 180/np.pi, torque)
    # torque = np.array([calculate_torque_between_rings(1, 1, xs) for xs in x])
    # plt.plot(x * 180/np.pi, torque)
    # plt.plot(x * 180/np.pi, torque * 4)
    # plt.plot(x * 180/np.pi, torque * 2)
    # plt.show()

    pass