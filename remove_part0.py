#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import os

import numpy as np
import h5py as h5

def main(snap_name):
    #'/mnt/su3ctm/mwilkinson/GalIC/mu_0p5/fbulge_0p01_lgMdm_6_V200_200kmps/snap_010.hdf5'
    #'/mnt/su3ctm/mwilkinson/GalIC/mu_0p5/fbulge_0p01_lgMdm_6_V200_200kmps_static/snap_010.hdf5'

    explode = snap_name.split('/')
    explode[-2] += '_static'
    out_name = ''
    for explo in explode[:-1]:
        out_name += explo + '/'
    try:
        os.mkdir(out_name)
    except OSError:
        pass

    out_name += explode[-1]
    print(out_name)

    with h5.File(snap_name, 'r') as fs:
        with h5.File(out_name, 'w') as out:
            # header = out.create_group('Header')

            fs.copy('Header', out)

            NumPart_ThisFile = fs['Header'].attrs['NumPart_ThisFile']
            NumPart_ThisFile[1] = 0
            out['Header'].attrs['NumPart_ThisFile'] = NumPart_ThisFile

            NumPart_Total = fs['Header'].attrs['NumPart_Total']
            NumPart_Total[1] = 0
            out['Header'].attrs['NumPart_Total'] = NumPart_Total

            try:
                Mass2 = fs['PartType2/Masses'][:]
                Pos2 = fs['PartType2/Coordinates'][:]
                Vel2 = fs['PartType2/Velocities'][:]
                ID2 = fs['PartType2/ParticleIDs'][:]

                print('Adding PartType2')

                PartType2 = out.create_group('PartType2')
                PartType2.create_dataset('Masses', data=Mass2)
                PartType2.create_dataset('Coordinates', data=Pos2)
                PartType2.create_dataset('Velocities', data=Vel2)
                PartType2.create_dataset('ParticleIDs', data=ID2)

                print('Added PartType2')
            except KeyError:
                pass

            try:
                Mass3 = fs['PartType3/Masses'][:]
                Pos3 = fs['PartType3/Coordinates'][:]
                Vel3 = fs['PartType3/Velocities'][:]
                ID3 = fs['PartType3/ParticleIDs'][:]

                print('Adding PartType3')

                PartType3 = out.create_group('PartType3')
                PartType3.create_dataset('Masses', data=Mass3)
                PartType3.create_dataset('Coordinates', data=Pos3)
                PartType3.create_dataset('Velocities', data=Vel3)
                PartType3.create_dataset('ParticleIDs', data=ID3)

                print('Added PartType3')
            except KeyError:
                pass

    return

if __name__ == '__main__':

    _, snap_name = sys.argv

    print(snap_name)

    main(snap_name)

    print('Finished successfully.')