\documentclass{article}

\usepackage[a4paper, margin=1.0in]{geometry}

\usepackage{amsmath}
\usepackage{amssymb}

\usepackage{hyperref}

\usepackage{listings}
\usepackage{color}

\usepackage{graphicx}
\graphicspath{{./images/}}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  language=Python,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}

\begin{document}

\section{Hernquist}

Assume Hernquist halo. 

\subsection{Ellipticity}

The energy of a particle in a circular orbit with radius $r'$ is
\begin{align}
v_c(r')  &= \sqrt{\frac{G M(<r')}{r'}} = \sqrt{\frac{G}{r'} M_h \left( \frac{r'}{r' + a} \right)^2 } = \sqrt{ G M_h \frac{r'}{(r' + a)^2} } \\
\Phi(r') &= - \frac{G M_h}{r' + a} \\
e_c(r')  &= \frac{1}{2} v_c(r')^2 + \Phi(r') \\
         &= \frac{1}{2} G M_h \frac{r'}{(r' + a)^2} - G M_h \frac{1}{r' + a} \\
         &= - \frac{1}{2} G M_h \frac{(r' + 2a)}{(r' + a)^2},
\end{align}
which only depends on $r'$ and the halo properties $M_h$ and $a$.
We can solve this for $r'$:
\begin{align}
\varepsilon &= -\frac{G M_h}{4 e_c} \\
r' &= \varepsilon - a \pm \sqrt{\varepsilon} \sqrt{2 a + \varepsilon}.
\end{align}
Only the positive solution has $r>0$ so only the positive solution is physical. 
$e_c$ is always negative ranging from $-G M_h / a$ at $r=0$ and as $r \to \infty$, $e_c \to ^-0$. This makes $\varepsilon$ always positive so no imaginary number problems.
The circular angular momentum is
\begin{align}
j_c(e_c) = r' v_c(r') = \sqrt{ G M_h \frac{ r'^3 }{(r' + a)^2} }
\end{align}
The particle has specific energy and angular momentum in $z$ direction of
\begin{align}
e(R, z, v_R, v_\phi, v_z) &= \frac{1}{2} \left( v_R^2 + v_\phi^2 + v_z^2 \right) - \frac{G M_h}{\sqrt{R^2 + z^2} + a} \\
e(r, |\vec{v}|) &= \frac{1}{2} |\vec{v}|^2 - \frac{G M_h}{r + a} \\
j_z(R, v_\phi) &= R v_\phi.
\end{align}
Ellipticity, $j_z(E) / j_c$, is calculated at the angular momentum in the $z$ direction normalised by a particle with the same energy in a circular orbit, i.e. $e = e_c$.
Now we can solve for $r'$ in terms of the particle's properties with this substitution
\begin{align}
\varepsilon &= - \frac{G M_h}{2 \left(  v_R^2 + v_\phi^2 + v_z^2 \right) - \frac{4G M_h}{\sqrt{R^2 + z^2} + a} } \\
\varepsilon(r, |\vec{v}|) &= - \frac{G M_h}{ 2 |\vec{v}|^2 - \frac{4 G M_h}{r + a }} = \frac{1}{4} \frac{1}{ \frac{1}{r + a } -  \frac{|\vec{v}|^2}{2 G M_h}} 
\end{align}
In total, this makes the ellipticity of an individual particle
\begin{align}
&j_z/j_c(R, z, r') = j_z/j_c(R, z, v_\phi, |\vec{v}|) = \frac{R v_\phi}{ r' v_c}
= \frac{R v_\phi}{\sqrt{ G M_h \frac{ r'^3 }{(r' + a)^2} }} 
%= \frac{R v_\phi}{\sqrt{ G M_h \frac{ \left( \frac{- a \varepsilon + \sqrt{2 a \varepsilon + 1} + 1}{\varepsilon} \right)^3 }{\left[ \left( \frac{- a \varepsilon + \sqrt{2 a \varepsilon + 1} + 1}{\varepsilon} \right) + a \right]^2} }} 
\end{align}

\subsection{Equations of motion in the plane.}

In an axisymmetric system $j_z$ is a conserved quantity and therefore ellipticity is also conserved.
By definition:
\begin{align}
v_\phi &= r \dot{\phi} \\
j_z / j_c = \epsilon &= \frac{r^2 \dot{\phi}}{r' v_c(r')} \\
\dot{\phi} &= \epsilon v_c(r') \frac{r'}{r^2} \\
&= \frac{\epsilon}{r^2} \sqrt{ G M_h \frac{r'^3}{(r' + a)^2} }
\end{align}
Equation 3.16 from Binney and Tremaine, then subbing in everything,
\begin{align}
\left( \frac{d r}{d t} \right)^2 &= 2 (e(r') - \Phi(r)) - \frac{L^2}{r^2} \\
&= 2 \left[ - \frac{1}{2} G M_h \frac{(r' + 2a)}{(r' + a)^2} + \frac{G M_h}{r + a} \right] - r^2 \dot{\phi}^2 \\
&= G M_h \left[ -\frac{(r' + 2a)}{(r' + a)^2} + \frac{2}{r + a} \right] -  \epsilon^2 v_c(r')^2 \frac{r'^2}{r^2} \\
&= G M_h \left[ -\frac{(r' + 2a)}{(r' + a)^2} + \frac{2}{r + a} - \epsilon^2 \frac{r'^3}{r^2 (r' + a)^2} \right]
\end{align}

For $ r' \approx r \ll a$
\begin{align}
\dot{\phi} &= \frac{\epsilon}{a r^2} \sqrt{ G M_h r'^3 } \\
\left( \frac{d r}{d t} \right)^2 &= - G M_h \epsilon^2 \frac{r'^3}{r^2 a^2}.
\end{align}
5???its negative???
And for $ r' \approx r \gg a$
\begin{align}
\dot{\phi} &= \frac{\epsilon}{r^2} \sqrt{ G M_h r' } \\
\left( \frac{d r}{d t} \right)^2 &= G M_h \left[ -\frac{1}{r'} + \frac{2}{r} - \epsilon^2 \frac{r'}{r^2} \right]
\end{align}

\subsection{Dispersion in the plane}

\begin{align}
\sigma_{v_r}^2    &= \langle v_r^2    \rangle - \langle v_r    \rangle^2 \\
\sigma_{v_\phi}^2 &= \langle v_\phi^2 \rangle - \langle v_\phi \rangle^2
\end{align}

Attempting to analytically solve solve the equations of motion. Since the orbit is periodic the time averaged quantities can be found by integrating over 1 whole period.

\begin{align}
\frac{dr}{dt} &= \pm \sqrt{ G M_h \left[ -\frac{(r' + 2a)}{(r' + a)^2} + \frac{2}{r + a} - \epsilon^2 \frac{r'^3}{r^2 (r' + a)^2} \right] } \\
\langle v_r \rangle &= \lim_{\tau \to \infty} \int_0^\tau \frac{dr}{dt} dt = 2 \int_{r_{min}}^{r_{max}} \sqrt{ G M_h \left[ -\frac{(r' + 2a)}{(r' + a)^2} + \frac{2}{r + a} - \epsilon^2 \frac{r'^3}{r^2 (r' + a)^2} \right] } dr \\
&= ??? = 0 \\
\langle v_r^2 \rangle &= 2 \int_{r_{min}}^{r_{max}} G M_h \left[ -\frac{(r' + 2a)}{(r' + a)^2} + \frac{2}{r + a} - \epsilon^2 \frac{r'^3}{r^2 (r' + a)^2} \right] dr \\
&= 2 G M_h \left[ -r \frac{(r' + 2a)}{(r' + a)^2} + 2 \log(r + a) + \epsilon^2 \frac{r'^3}{r (r' + a)^2} \right] |_{r_{min}}^{r_{max}}
\end{align}

Finding $r_{min}$ and $r_{max}$.
\begin{align}
0 = \frac{dr}{dt} &= \pm \sqrt{ G M_h \left[ -\frac{(r' + 2a)}{(r' + a)^2} + \frac{2}{r + a} - \epsilon^2 \frac{r'^3}{r^2 (r' + a)^2} \right] } \\
0 &= -\frac{(r' + 2a)}{(r' + a)^2} + \frac{2}{r + a} - \epsilon^2 \frac{r'^3}{r^2 (r' + a)^2} \\
r_{max/min} &= ?
\end{align}

Wolfram gave 1 real and 2 imaginary answers which seemed too complicated.
Try to solve with the cubic formula later.

\begin{align}
\sigma_{v_r}^2    &= ? \\
\sigma_{v_\phi}^2 &= ?
\end{align}


\subsection{Morphology}

\subsubsection{$\kappa$}

\begin{align}
\kappa_{\textup{rot}} = \frac{\sum_i^{r<30\textup{kcp}} \frac{1}{2} m_i \left[ L_{z,i} / (m_i R_i) \right]^2 }{\sum_i^{r<30\textup{kcp}} \frac{1}{2} m_i v_i^2 }
\end{align}

In a system with equal masses, $m_i = m_j$ and notice that $L_{z,i} = m_i v_{\phi,i} R_i$. If we drop the summation bounds, $\kappa_{\textup{rot}}$ can be written:

\begin{align}
\kappa_{\textup{rot}} = \frac{\sum_i v_{\phi,i}^2}{\sum_i |v_i|^2 } = \frac{\sum_i v_{\phi,i}^2}{\sum_i v_{\phi,i}^2 + \sum_i v_{r,i}^2 + \sum_i v_{z,i}^2}
\end{align}

For a normally distributed variable, the root mean squared is

\begin{align}
x^2_{rms} &= \bar{x}^2 + \sigma_x^2 = \bar{x^2} \\
\bar{v_r^2} &= 0^2 + \sigma_{v_r}^2 
&& \Rightarrow \sum_{i=1}^N v_{r,i}^2 = N \sigma_{v_r}^2 \\
\bar{v_z^2} &= \sigma_{v_z}^2 
&& \Rightarrow \sum v_z^2 = N \sigma_{v_z}^2 \\
\bar{v_\phi^2} &= \bar{v_\phi}^2 + \sigma_{v_\phi}^2 
&& \Rightarrow \sum v_\phi^2 = N (\bar{v_\phi}^2 + \sigma_{v_\phi}^2)
\end{align}

Which makes  $\kappa_{\textup{rot}}$

\begin{align}
\kappa_{\textup{rot}} &= \frac{\bar{v_\phi}^2 + \sigma_{v_\phi}^2}{\bar{v_\phi}^2 + \sigma_{v_\phi}^2 + \sigma_{v_r}^2 + \sigma_{v_z}^2} \\
\kappa_{\textup{rot}} &= \frac{\bar{v_\phi}^2 + \sigma_{v_\phi}^2}{\bar{v_\phi}^2 + \sigma_{|\vec{v}|}^2}
\end{align}

The $\bar{v_\phi}$ term is unusual and I can only find calculations for in under extremely specific assumptions.
$\bar{v_\phi}$ is the expectation value of the PDF of $v_\phi$, $f_{v_\phi}$.
\footnote{The notation gets confusing, so just to be clear $f$ means PDF, the subscript of $f$ is variable that $f$ is the PDF of, and the quantity in brackets is the (current) argument of the PDF. 
The goal is to get the variable and the argument to match.}
The PDF of $v_\phi$ is the mass weighted distribution of velocities.
For a first attempt, I assume $v_{\phi,i} \approx v_{c,i}$.
I use Hernquist circular velocities and exponential mass profile for the non-self gravitating case.
So we start with
\begin{align}
f_r(r) &= \frac{1}{R_d^2} R \exp(R / R_d) \approx \frac{1}{R_d^2} r \exp(r / R_d) \\
v_c(r) &= \sqrt{G M_h \frac{r}{(r + a)^2}}
\end{align}
and want to end up with $f_{v_c}(v_c)$.
If we have a monotonic function $y = g(x)$, then a change of variable is 
\begin{align}
f_Y(y) = f_X\left( g^{-1}(y) \right) \left| \frac{d}{dy} g^{-1}(y) \right|.
\end{align}
Hernquist $v_c(r)$ monotonically increases from $r = 0$ to $a$ with $\max(v_c) = v_c(a) = \sqrt{G M_h / 4 a}$ and then monotonically decreases from $r = a$ to $\infty$. 
A typical values for $a$ is ? $\sim 30$kpc, while a typical value for $R_d$ is ? $\sim 5$kpc so it is a good assumption that all the mass of the disk in contained in $r < a$.
Finding $r(v_c)$ is tedious but straightforward:
\begin{align}
(r^2 + 2ar + a^2) v_c^2 &= G M_h r \\
v_c^2 r^2 + (2 a v_c^2 - G M_h) r + a^2 v_c^2 &= 0 \\
r &= \frac{- 2 a v_c^2 + G M_h \pm \sqrt{4 a^2 v_c^4 - 4 a v_c^2 G M_h + G^2 M_h^2 - 4 a^2 v_c^4 }}{2 v_c^2} \\
v_c^{-1} = r(v_c) &= \frac{- 2 a v_c^2 + G M_h \pm \sqrt{G^2 M_h^2 - 4 a v_c^2 G M_h }}{2 v_c^2}
\end{align}
Where the negative solution corresponds to $r < a$.
The derivative is
\begin{align}
\frac{d}{dv_c} r(v_c) &= \frac{\frac{4 a v_c G M_h}{\sqrt{G^2 M_h^2 - 4 a v_c^2 G M_h}} - 4 a v_c}{2 v_c} - \frac{- 2 a v_c^2 + G M_h - \sqrt{G^2 M_h^2 - 4 a v_c^2 G M_h}}{v_c^3}
\end{align}
In total 
\begin{align}
f_{v_c}(v_c) =& \frac{- 2 a v_c^2 + G M_h \pm \sqrt{G^2 M_h^2 - 4 a v_c^2 G M_h }}{2 v_c^2 R_d^2} \exp\left(\frac{- 2 a v_c^2 + G M_h - \sqrt{G^2 M_h^2 - 4 a v_c^2 G M_h }}{2 v_c^2 R_d} \right) \nonumber \\
& \left| \frac{\frac{4 a v_c G M_h}{\sqrt{G^2 M_h^2 - 4 a v_c^2 G M_h}} - 4 a v_c}{2 v_c} - \frac{- 2 a v_c^2 + G M_h - \sqrt{G^2 M_h^2 - 4 a v_c^2 G M_h}}{v_c^3} \right|
\end{align}
Which is defined from $v_c=0$ to $v_c = \sqrt{G M_h / 4 a}$, so the mean circular velocity is
\begin{align}
\bar{v_\phi} \approx \bar{v_c} = \int_0^{\sqrt{GM_h / 4 a}} v_c f_{v_c}(v_c) d v_c
\end{align}

Essentially, the integral is 
\begin{align}
out = \int_a^b x g(x) e^{g(x)} g'(x) dx.
\end{align}
Starting from the tripe product rule
\begin{align}
\int u v dw &= u v w - \int u w dv - \int v w du
\end{align}
\begin{align}
u &= g(x)        &&\Rightarrow du = g'(x) dx \\
v &= x           &&\Rightarrow dv = dx \\
w &= e^{g(x)}    &&\Rightarrow dw = g'(x) e^{g(x)} dx
\end{align}
\begin{align}
out &= x g(x) e^{g(x)} - \int g(x) e^{g(x)} dx - \int x e^{g(x)} g'(x) dx
\end{align}
\begin{align}
u &= x        &&\Rightarrow du = dx \\
v &= e^{g(x)} &&\Rightarrow dv = g'(x) e^{g(x)} dx
\end{align}
\begin{align}
\int u dv &= u  v - \int v du \\
\int x g'(x) e^{g(x)} dx &= x e^{g(x)} - \int e^{g(x)} dx
\end{align}
\begin{align}
out &= x g(x) e^{g(x)} - x e^{g(x)} - \int g(x) e^{g(x)} dx + \int e^{g(x)} dx
\end{align}
Analytically calculating the remaining integrals seems hard ...

%\rule{420pt}{1pt}

\subsubsection{$v/\sigma$}

Equation 9 from Thob et al. (2019) is the definition of $\sigma_0$:

\begin{align}
K - K_{zz} = \frac{1}{2} M_\star V_{\textup{rot}}^2 + M_\star \sigma_0^2
\end{align}

Where $K = \sum_i m_i |v_i|^2$, $K_{zz} = \sum_i m_i v_{i,z}^2$, $M_\star = \sum_i m_i$ and $V_{\textup{rot}}$ is the mass weighted \textit{median} circular velocity $v_{\phi,i}$.
If we assume equal mass particles, $M_\star = N m_i$, the equation can be rewritten,

\begin{align}
\frac{1}{2} M_\star \frac{1}{N} \sum_i |v_i|^2 - \frac{1}{2} M_\star \frac{1}{N} \sum_i v_{z,i}^2 - \frac{1}{2} M_\star V_{\textup{rot}}^2 &= M_\star \sigma_0^2 \\
\frac{1}{N} \sum_i |v_i|^2 - \frac{1}{N} \sum_i v_{z,i}^2 - V_{\textup{rot}}^2 &= 2 \sigma_0^2 \\
\frac{1}{N} \sum_i v_{R,i}^2 + \frac{1}{N} \sum_i v_{\phi,i}^2 + \frac{1}{N} \sum_i v_{z,i}^2 - \frac{1}{N} \sum_i v_{z,i}^2 - V_{\textup{rot}}^2 &= 2 \sigma_0^2 \\
\frac{1}{N} \sum_i v_{R,i}^2 + \frac{1}{N} \sum_i v_{\phi,i}^2 - V_{\textup{rot}}^2 &= 2 \sigma_0^2 \\
\sigma_{v_R}^2 + \sigma_{v_\phi}^2 + \bar{v_\phi}^2 - V_{\textup{rot}}^2 &= 2 \sigma_0^2
\end{align}
If $\bar{v_\phi}^2 = V_{\textup{rot}}^2$ then
\begin{align}
\sigma_0^2 = \frac{\sigma_{v_r}^2 + \sigma_{v_\phi}^2}{2}
\end{align}
but for thin disks I'm finding median($v_\phi$) $>$ mean($v_\phi$) by so much that $\sigma_0^2 < 0$.

$V_{\text{rot}}$ comes from evaluating the mean streaming velocity tensor, defined in Binney and Tremaine eq.(4.241b)

\begin{align}
T_{jk} &\equiv \frac{1}{2} \int d^3 \textbf{x} \rho \bar{v_j} \bar{v_k}.
\end{align}

For an asymmetric rotating but otherwise stationary system
\begin{align}
T_{\phi \phi} &= \frac{1}{2} \int d^3 \textbf{x} \rho(\textbf{x}) \left( \bar{v_\phi}(\textbf{x}) \right)^2 = \frac{1}{2} \bar{v_\phi}^2 \\
T_{RR} = T_{zz} &= 0 \\
T_{ij} &= 0, \qquad \forall i \neq j
\end{align}

Where $\bar{v_\phi}^2$ is the mass weighted mean circular velocity. I think there has been some confusion about the order of operations, so I want to emphasise that $\bar{v_\phi}^2 \neq \bar{v_\phi^2}$.

\subsection{Vertical height profile}

\subsubsection{Self Gravitating}

The density as a function of height above galactic disk for a self gravitating disk is 

\begin{align}
\rho(R, z) &= \rho_0 (R) \textup{sech}^2 \left( \frac{z}{2 z_0} \right).
\end{align}

The PDF of $z$ components is given by $\rho(z,R) / \Sigma(R)$ where 

\begin{align}
\Sigma(R) &= \int_{-\infty}^\infty \rho(z, R) dr = 4 \rho_0(R) z_0(R) \\
\frac{\rho_0(R)}{\Sigma(R)} &= \frac{1}{4 z_0(R)}
\end{align}

standard deviation of height above the disk is

\begin{align}
\sigma_z^2 &= \int_{-\infty}^\infty z^2 \frac{\rho(z)}{\Sigma} dz = 2 \frac{\rho_0}{\Sigma} \int_0^\infty z^2 \textup{sech}^2 \left( \frac{z}{2 z_0} \right) dz \\
&= 2 \frac{\rho_0}{\Sigma} (6.579736267392906) z_0^3 = \frac{1}{2} (6.579736267392906) z_0^2
\end{align}

where the result of the integral was found numerically / experimentally.

Next assume simple harmonic motion in the $z$ direction (Should be valid where the epicycle approximation is also valid). This means we can use

\begin{align}
\sigma_{v_z}^2 = \nu^2 \sigma_z^2.
\end{align}

where $\nu$ is the vertical epicycle frequency in the thin disk approximation and is given by

\begin{align}
\nu^2(R) = \left( \frac{\partial^2 \Phi}{\partial z^2} \right)_{(R=R,z=0)}.
\end{align}

Poisson's equation for a thin disk near $z=0$ is 

\begin{align}
\frac{\partial^2 \Phi(R_z)}{\partial z^2} &= 4 \pi G \rho(R, z)
\end{align}

If we assume the disk is self gravitating, then $\rho(R, z) = \rho_{\textup{disk}}(R, z)$. This makes

\begin{align}
\nu^2(R) &= 4 \pi G \rho_0.
\end{align}

According to Aaron's findings, at the initial half mass radii of a disk

\begin{align}
\log \tau &= \log \Delta \upsilon^2\\
t \frac{G^2 \rho_{cr} 2 \cdot 10^6 M_{DM}}{V_{200}^3} &= \frac{\sigma_{v_z}^2 - \sigma_{v_z,t=0}^2}{V_{200}^2} \\
t &= \frac{V_{200}}{G^2 \rho_{cr} 2 \cdot 10^6 M_{DM}} \left( \sigma_{v_z}^2 - \sigma_{v_z,t=0}^2 \right)
\end{align}

Putting everything together, the scale height at the initial half mass radii as a function of initial velocity dispersion, current mid-plane density and and time is

\begin{align}
t &= \frac{V_{200}}{G^2 \rho_{cr} 2 \cdot 10^6 M_{DM}} \left( \nu^2 \sigma_z^2 - \sigma_{v_z,t=0}^2 \right) \\
t &= \frac{V_{200}}{G^2 \rho_{cr} 2 \cdot 10^6 M_{DM}} \left( 4 \pi G \rho_0 \frac{1}{2} (6.58...) z_0^2 - \sigma_{v_z,t=0}^2 \right) \\
z_0(t) &= \sqrt{\frac{1}{2 (6.58...) \pi G \rho_0(t)} \left( t \frac{G^2 \rho_{cr} 2 \cdot 10^6 M_{DM}}{V_{200}} + \sigma_{v_z,t=0}^2 \right)}.
\end{align}

Compare this to the bottom panel of figure 10 in Park et al. (2020).
This should also be compared to $\textup{median}(|z|)$ by

\begin{align}
\frac{1}{2} &= \int_0^{\textup{median}(|z|)} \rho(|z|) dz \\
z_0 &= 2 z_0 \tanh \left( \frac{\textup{median}(|z|)}{2 z_0} \right) \\
\frac{\textup{median}(|z|)}{2 z_0} &= \frac{1}{2} \ln \left( \frac{1 + 1/2}{1 - 1/2} \right) \\
\textup{median}(|z|) &= z_0 \ln 3 \approx z_0 \cdot 1.09861 ...
\end{align}

\subsubsection{Non-Self Gravitating in a Hernquist Halo}

We have to approach this one differently. The hydrostatic equilibrium equation is 

\begin{align}
\nabla p = - \rho \nabla \Phi.
\end{align}

Assume that our system is separable, we only want to consider $z$ direction equilibrium.
For a system of stars particles, the pressure in the $z$ direction is $p = \rho \sigma_{v_z}^2$. 
This makes the hydrostatic equilibrium

\begin{align}
\frac{\partial}{\partial z} (\rho \sigma_{v_z}^2) &= - \rho \frac{\partial}{\partial z} \Phi \\
\sigma_{v_z}^2 \frac{\partial}{\partial z} (\rho) + \rho \frac{\partial}{\partial z} (\sigma_{v_z}^2) &= - \rho \frac{\partial}{\partial z} \Phi \\
\sigma_{v_z}^2 \frac{\partial}{\partial z} \rho &= - \rho \left( \frac{\partial}{\partial z} \Phi + \frac{\partial}{\partial z} \sigma_{v_z}^2 \right) \\
\frac{\frac{\partial}{\partial z} \rho }{\rho} &= - \frac{1}{\sigma_{v_z}^2} \left( \frac{\partial}{\partial z} \Phi + \frac{\partial}{\partial z} \sigma_{v_z}^2 \right)
\end{align}

Guess $\rho$ is an exponential

\begin{align}
\rho &= e^{f(z)} \quad , \quad \frac{\partial}{\partial z} \rho = f'(z) e^{f(z)} \quad \Rightarrow \quad \frac{\frac{\partial}{\partial z} \rho }{\rho} = f'(z) \\
f(z) &= \int - \frac{1}{\sigma_{v_z}^2} \left( \frac{\partial}{\partial z} \Phi + \frac{\partial}{\partial z} \sigma_{v_z}^2 \right) dz = - \int \frac{1}{\sigma_{v_z}^2} \frac{\partial}{\partial z} \Phi dz - \int \frac{1}{\sigma_{v_z}^2} \frac{\partial}{\partial z} \sigma_{v_z}^2 dz \\
&= ?
\end{align}

...

...

Assume $\sigma_{v_z}$ is independent of $z$ the equilibrium
TODO: check this
I think this is where the current problem is ..

\begin{align}
\frac{\frac{\partial}{\partial z} \rho }{\rho} &= -\frac{1}{\sigma_{v_z}^2} \frac{\partial}{\partial z} \Phi 
\end{align}

Guess $\rho$ is an exponential

\begin{align}
\rho &= e^{f(z)} \quad , \quad \frac{\partial}{\partial z} \rho = f'(z) e^{f(z)} \quad \Rightarrow \quad \frac{\frac{\partial}{\partial z} \rho }{\rho} = f'(z) \\
f(z) &= \int -\frac{1}{\sigma_{v_z}^2} \frac{\partial}{\partial z} \Phi dz = -\frac{1}{\sigma_{v_z}^2} \Phi(z) + c \\
\rho(z) &= \exp \left(-\Phi(z)/\sigma_{v_z}^2 + c \right) = D \exp \left(-\Phi(z)/\sigma_{v_z}^2 \right)
\end{align}

Assume a Hernquist halo so

\begin{align}
\Phi (r) &= \frac{-GM}{r + a} \quad \Rightarrow \quad \Phi (R, z) = \frac{-GM}{\sqrt{R^2 + z^2} + a} \\
\rho(z) &= D \exp \left(\frac{GM}{ \sigma_{v_z}^2 \left(\sqrt{R^2 + z^2} + a \right)} \right)
\end{align}

As $z \to \infty$, $\rho \to \rho_0$ which doesn't seem correct ...
Constant dispersion might not be correct ...

The dispersion of the particles that make up the halo with $x = r/a$ and $y = 1 + x$ is

\begin{align}
\sigma_{v_X}^2 &= \frac{GM}{a} \left[ x y^3 \ln (y/x) - \frac{x}{y} \left( \frac{1}{4} + \frac{1}{3} y + \frac{1}{2} y^2 + y^3 \right) \right] \\
&= \frac{GM}{a} \left[ \frac{r}{a} \left( 1 + \frac{r}{a} \right)^3 \ln \left( \frac{\left( 1 + r/a \right)}{r/a} \right) - \frac{r/a}{\left( 1 + r/a \right)} \left( \frac{1}{4} + \frac{1}{3} \left( 1 + r/a \right) + \frac{1}{2} \left( 1 + r/a \right)^2 + \left( 1 + r/a \right)^3 \right) \right] \\
\frac{\partial}{\partial z}\Phi (R, z) &= \frac{GM z}{\sqrt{R^2 + z^2} \left( \sqrt{R^2 + z^2} + a \right)^2} \\
\end{align}

\newpage

\section{Flat rotation curve potential}

Assume flat rotation curve potential.

\subsection{Ellipticity}

The energy of a particle in a circular orbit is
\begin{align}
v_c(r')  &= v_c \\
\Phi(r') &= v_c^2 \log(r') \\
e_c(r')  &= \frac{1}{2} v_c(r')^2 + \Phi(r') \\
         &= \frac{1}{2} v_c^2 + v_c^2 \log(r') \\
         &= v_c^2 \left( \frac{1}{2} + \log(r') \right) ,
\end{align}
which only depends on $r'$ and the halo property $v_c$.
We can solve this for $r'$:
\begin{align}
\frac{e_c}{v_c^2} - \frac{1}{2} &= \log(r') \\
r' &= \exp\left( \frac{e_c}{v_c^2} - \frac{1}{2} \right)
\end{align}
The circular angular momentum is
\begin{align}
j_c(e_c) = r' v_c = v_c \exp\left( \frac{e_c}{v_c^2} - \frac{1}{2} \right)
\end{align}
The particle has specific energy and angular momentum in $z$ direction of
\begin{align}
e(R, z, v_R, v_\phi, v_z) &= \frac{1}{2} \left( v_R^2 + v_\phi^2 + v_z^2 + v_c^2 \log(R^2 + z^2)\right) \\
e(R, z, |\vec{v}|) &= \frac{1}{2} \left( |\vec{v}|^2 + v_c^2 \log(R^2 + z^2)\right) \\
j_z(R, v_\phi) &= R v_\phi.
\end{align}
Ellipticity, $j_z(E) / j_c$, is calculated at the angular momentum in the $z$ direction normalised by a particle with the same energy in a circular orbit, i.e. $e = e_c$.
This makes 
\begin{align}
r' &= \exp \left[ \frac{1}{2} \left( \frac{|\vec{v}|^2}{v_c^2} + \log(R^2 + z^2) - 1 \right) \right]
\end{align}
In total, this makes the ellipticity
\begin{align}
j_z/j_c(R, z, v_\phi, |\vec{v}|) &= \frac{R v_\phi}{r' v_c} \\
&= \frac{R v_\phi}{v_c \exp \left[ \frac{1}{2} \left( \frac{|v|^2}{v_c^2} + \log(R^2 + z^2) - 1 \right) \right]}
\end{align}

\subsection{Equations of motion in the plane}

Equation 2 from Peebles 2020. By definition:
\begin{align}
v_\phi &= r \dot{\phi} \\
j_z / j_c = \epsilon &= \frac{r^2 \dot{\phi}}{r' v_c} \\
\frac{d \phi}{dt} = \dot{\phi} &= v_c \epsilon \frac{r'}{r^2} \\
\phi(t) &= v_c r' \epsilon \int_0^t \frac{1}{r^2} dt + \phi(t=0) = v_c r' \epsilon \int_{r_{min}}^{r(t - n T_r)} \frac{1}{r^2 v_r} dr + \frac{n}{2} \Delta \phi_r
\end{align}
The other part of equation 2. From Binney and Tremaine, then subbing in everything,
\begin{align}
\left( \frac{d r}{d t} \right)^2 &= 2 (e(r') - \Phi(r)) - \frac{L^2}{r^2} \\
&= 2 \left( v_c^2 \left( \frac{1}{2} + \log(r') \right) - v_c^2 \log(r) \right) - \frac{(r^2 \dot{\phi})^2}{r^2} \\
&= - 2 v_c^2 \log \left( \frac{r}{r'} \right) + v_c^2 - r^2 v_c^2 \epsilon^2 \frac{r'^2}{r^4} \\
&= v_c^2 \left[ 1 - 2 \log \left( \frac{r}{r'} \right) - \left( \epsilon \frac{r'}{r} \right)^2 \right] \\
\Rightarrow \frac{dr}{dt} &= \pm v_c \sqrt{ 1 - 2 \log \left( \frac{r}{r'} \right) - \left( \epsilon \frac{r'}{r} \right)^2 }
\end{align}
The radial period (the time between travelling from apocenter to pericenter and back) is 
\begin{align}
T_r &= 2 \int_{r_{min}}^{r_{max}} \frac{dt}{dr} dr = 2 \int_{r_{min}}^{r_{max}} \frac{1}{v_r} dr.
\end{align}
$r_{min}$ and $r_{max}$ can be found with
\begin{align}
0 = \frac{dr}{dt} &= \pm v_c \sqrt{ 1 - 2 \log \left( \frac{r}{r'} \right) - \left( \epsilon \frac{r'}{r} \right)^2 } \\
1 &= 2 \log \left( \frac{r}{r'} \right) + \left( \epsilon \frac{r'}{r} \right)^2 \\
2 \log \left( \frac{r}{r'} \right) - 1 &= W_{0,-1} \left( - \frac{\epsilon^2}{e} \right) \\
r_{max/min} &= r' \exp \left( \frac{W_{0,-1} \left( - \frac{\epsilon^2}{e} \right) + 1}{2} \right).
\end{align}
where $W$ is the Lambert function.
The angle the orbit sweeps in this time is
\begin{align}
\Delta \phi_r = 2 \int_{r_{min}}^{r_{max}} \frac{d \phi}{dr} dr = 2 \int_{r_{min}}^{r_{max}} \frac{d \phi}{dt} \frac{dt}{dr} dr = 2 \epsilon r' v_c \int_{r_{min}}^{r_{max}} \frac{1}{r^2 v_r} dr.
\end{align}
The integrals do not have results in terms of standard mathematical functions so they have to be solved numerically.

\subsection{Dispersion in the plane}

\begin{align}
\sigma_{v_r}^2    &= \langle v_r^2    \rangle - \langle v_r    \rangle^2 \\
\sigma_{v_\phi}^2 &= \langle v_\phi^2 \rangle - \langle v_\phi \rangle^2
\end{align}

Attempting to analytically solve solve the equations of motion. Since the orbit is periodic the time averaged quantities can be found by integrating over a radial period, $T_r$.

\begin{align}
\langle v_r \rangle &= \frac{1}{T_r} \int_0^{T_r} \frac{dr}{dt} dt = \frac{1}{T_r} \int_{r_{min}}^{r_{max}} dr + \frac{1}{T_r} \int_{r_{max}}^{r_{min}} dr \\
&= 0 \\
\sigma_{v_r}^2 = \langle v_r^2 \rangle &= \frac{1}{T_r} \int_0^{T_r} \frac{dr^2}{dt^2} dt = \frac{1}{T_r} \int_{r_{min}}^{r_{max}} \frac{dr}{dt} dr + \frac{1}{T_r} \int_{r_{max}}^{r_{min}} -\frac{dr}{dt} dr \\
&= \frac{2}{T_r} \int_{r_{min}}^{r_{max}} v_r dr
\end{align}
Again this has to be solved numerically.
Attempting the same thing for the rotational velocity looks like
\begin{align}
\langle v_\phi \rangle &= \frac{1}{T_r} \int_0^{T_r} r \frac{d \phi}{dt} dt = \frac{2}{T_r} \int_{r_{min}}^{r_{max}} r \frac{d \phi}{dt} \frac{dt}{dr} dr \\
&= \frac{2 \epsilon v_c r'}{T_r} \int_{r_{min}}^{r_{max}} \frac{1}{r v_r} dr \\
\langle v_\phi^2 \rangle &= \frac{1}{T_r} \int_0^{T_r} r^2 \left( \frac{d \phi}{dt} \right)^2 dt = \frac{2}{T_r} \int_{r_{min}}^{r_{max}} r^2 \left( \frac{d \phi}{dt} \right)^2 \frac{dt}{dr} dr \\
&= \frac{2 \left( \epsilon v_c r' \right)^2}{T_r} \int_{r_{min}}^{r_{max}} \frac{1}{r^2 v_r} dr = \frac{\epsilon v_c r'}{T_r} \Delta \phi_r \\
\sigma_{v_\phi}^2 &= \frac{2 \left( \epsilon v_c r' \right)^2}{T_r} \left( \int_{r_{min}}^{r_{max}} \frac{1}{r^2 v_r} dr - \frac{2}{T_r} \left[ \int_{r_{min}}^{r_{max}} \frac{1}{r v_r} dr \right]^2 \right)
\end{align}

These integrals can be done numerically.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{epsilon_sigmaonv.png}
\caption{For an orbit in the the plane, here is the integrated dispersion as a function of ellipticity.}
\label{epsilon_sigmaonv}
\end{figure}

\newpage

\section{Kepler rotation curve potential}

Assume flat rotation curve potential.

\subsection{Ellipticity}

The energy of a particle in a circular orbit is
\begin{align}
\Phi(r') &= -\frac{GM}{r'} \\
v_c(r')  &= \sqrt{\frac{GM}{r'}} \\
e_c(r')  &= \frac{1}{2} v_c(r')^2 + \Phi(r') \\
         &= \frac{GM}{2 r'} - \frac{GM}{r'} \\
         &= -\frac{GM}{2 r'} ,
\end{align}
which only depends on $r'$ and the halo property $v_c$.
We can solve this for $r'$:
\begin{align}
r' &= -\frac{GM}{2 e_c}
\end{align}
The circular angular momentum is
\begin{align}
j_c(e_c) = r' v_c = \sqrt{GM r'} = \sqrt{-\frac{(GM)^2}{2e_c}}
\end{align}
The particle has specific energy and angular momentum in $z$ direction of
\begin{align}
e(R, z, |\vec{v}|) &= \frac{1}{2} |\vec{v}|^2 - \frac{GM}{\sqrt{R^2 + z^2}} \\
j_z(R, v_\phi) &= R v_\phi.
\end{align}
Ellipticity, $j_z(E) / j_c$, is calculated at the angular momentum in the $z$ direction normalised by a particle with the same energy in a circular orbit, i.e. $e = e_c$.
This makes 
\begin{align}
r' &= -\frac{GM}{|\vec{v}|^2 - 2 \frac{GM}{\sqrt{R^2 + z^2}}}
\end{align}
In total, this makes the ellipticity
\begin{align}
j_z/j_c(R, z, v_\phi, |\vec{v}|) &= \frac{R v_\phi}{r' v_c}  \\
&= ...
\end{align}

\subsection{Equations of motion in the plane}

For the Kepler case, we have an analytic solution for $r(\theta)$
\begin{align}
r(\theta) &= \frac{a (1 - e^2)}{1 + e \cos(\theta)}
\end{align}
were $a = (r_{max} + r_{min})/2$ is the semimajor axis, $e$ is eccentricity and $\theta = \phi$ because $\phi$ looks weird in that equation.

By definition:
\begin{align}
v_\phi &= r \dot{\phi} \\
j_z / j_c = \epsilon &= \frac{r^2 \dot{\phi}}{r' v_c} \\
\frac{d \phi}{dt} = \dot{\phi} &= \epsilon \frac{\sqrt{GM r'}}{r^2} \\
\phi(t) &= \int_{0}^{t} \frac{d \phi}{dt} dt = \epsilon \sqrt{GM r'} \int_{0}^{t} \frac{1}{r^2} dt \\
\phi(r) &= \int_{r_{min}}^{r} \frac{d \phi}{dt} \frac{dt}{dr} dr + \phi_0 = \epsilon \sqrt{GM r'} \int_{r_{min}}^{r} \frac{1}{r^2} \frac{dt}{dr} dr + \phi_0
\end{align}
The other part of equation 2. From Binney and Tremaine, then subbing in everything,
\begin{align}
\left( \frac{d r}{d t} \right)^2 &= 2 (e(r') - \Phi(r)) - \frac{L^2}{r^2} \\
&= -\frac{GM}{r'} + \frac{2 GM}{r} - \epsilon^2 \frac{GM r'}{r^2} = GM \left[ \frac{2}{r} - \frac{1}{r'} - \frac{\epsilon^2 r'}{r^2} \right]
\end{align}
The radial period (the time between travelling from apocenter to pericenter and back) is 
\begin{align}
T_r &= 2 \int_{r_{min}}^{r_{max}} \frac{dt}{dr} dr = 2 \int_{r_{min}}^{r_{max}} \frac{1}{v_r} dr.
\end{align}
$r_{min}$ and $r_{max}$ can be found with
\begin{align}
0 &= \frac{dr}{dt} \\
0 &= \frac{- r^2 + 2r - \epsilon^2 r'^2}{r' r^2} \\
r_{max/min} &= r' \left(1 \pm \sqrt{1 - \epsilon^2} \right)
\end{align}
Here it is convenient to quickly calculate the eccentricity which is used for the analytic solution.
\begin{align}
e &= \frac{r_{appo} - r_{peri}}{r_{appo} + r_{peri}} = \frac{1 + \sqrt{1 - \epsilon^2} - 1 + \sqrt{1 - \epsilon^2}}{1 + \sqrt{1 - \epsilon^2} + 1 - \sqrt{1 - \epsilon^2}} = \sqrt{1 - \epsilon^2} \\
\sqrt{1 - e^2} &= \epsilon
\end{align}



The angle the orbit sweeps in this time is
\begin{align}
\Delta \phi_r = 2 \int_{r_{min}}^{r_{max}} \frac{d \phi}{dr} dr = 2 \int_{r_{min}}^{r_{max}} \frac{d \phi}{dt} \frac{dt}{dr} dr
\end{align}
The integrals do not have results in terms of standard mathematical functions so they have to be solved numerically.

\subsection{Dispersion in the plane}

\begin{align}
\sigma_{v_r}^2    &= \langle v_r^2    \rangle - \langle v_r    \rangle^2 \\
\sigma_{v_\phi}^2 &= \langle v_\phi^2 \rangle - \langle v_\phi \rangle^2
\end{align}

Attempting to analytically solve solve the equations of motion. Since the orbit is periodic the time averaged quantities can be found by integrating over a radial period, $T_r$.

\begin{align}
\langle v_r \rangle &= \frac{1}{T_r} \int_0^{T_r} \frac{dr}{dt} dt = \frac{1}{T_r} \int_{r_{min}}^{r_{max}} dr + \frac{1}{T_r} \int_{r_{max}}^{r_{min}} dr \\
&= 0 \\
\sigma_{v_r}^2 = \langle v_r^2 \rangle &= \frac{1}{T_r} \int_0^{T_r} \frac{dr^2}{dt^2} dt = \frac{1}{T_r} \int_{r_{min}}^{r_{max}} \frac{dr}{dt} dr + \frac{1}{T_r} \int_{r_{max}}^{r_{min}} -\frac{dr}{dt} dr \\
&= \frac{2}{T_r} \int_{r_{min}}^{r_{max}} v_r dr \\
&= 1 - \epsilon
\end{align}
Again this has to be solved numerically.
Attempting the same thing for the rotational velocity looks like
\begin{align}
\langle v_\phi \rangle &= \frac{1}{T_r} \int_0^{T_r} r \frac{d \phi}{dt} dt = \frac{2}{T_r} \int_{r_{min}}^{r_{max}} r \frac{d \phi}{dt} \frac{dt}{dr} dr \\
\langle v_\phi^2 \rangle &= \frac{1}{T_r} \int_0^{T_r} r^2 \left( \frac{d \phi}{dt} \right)^2 dt = \frac{2}{T_r} \int_{r_{min}}^{r_{max}} r^2 \left( \frac{d \phi}{dt} \right)^2 \frac{dt}{dr} dr \\
\sigma_{v_\phi}^2 &= ... \\
&= \epsilon - \epsilon^2
\end{align}

These integrals can be done numerically.


\subsection{Dispersion out of the plane}

Orbits in spherical potentials are planar and don't precess.
From a 1D planar orbit to 3D orbit requires two angles: inclination, $i$, range $[-\pi/2, \pi/2]$,  and longitude of ascending node, $\Omega$, range $[0, \pi/2)$, which is the angle between a reference $x$ axis and the line passing through a point on the orbit where $z=0$ and $v_z > 0$. In terms of coordinates in the plane ($\parallel$), the transformed coordinates are:
\begin{align}
\tan( i ) & = \frac{z}{\sqrt{x^2 + y^2}} = \frac{v_z}{\sqrt{v_x^2 + v_y^2}} 
\end{align}
\begin{align}
x &= x_\parallel \cos \Omega - y_\parallel \sin \Omega \cos i
&&= R_\parallel ( \cos \phi_\parallel \cos \Omega - \sin \phi_\parallel \sin \Omega \cos i) \\
y &= x_\parallel \sin \Omega + y_\parallel \cos \Omega \cos i
&&= R_\parallel ( \cos \phi_\parallel \sin \Omega + \sin \phi_\parallel \cos \Omega \cos i) \\
z &= y_\parallel \sin i 
&&= R_\parallel \sin \phi_\parallel \sin i
\end{align}
\begin{align}
v_x &= v_{R_\parallel} ( \cos \phi_\parallel \cos \Omega - \sin \phi_\parallel \sin \Omega \cos i) - v_{\phi_\parallel} ( \sin \phi_\parallel \cos \Omega + \cos \phi_\parallel \sin \Omega \cos i) \\
v_y &= v_{R_\parallel} ( \cos \phi_\parallel \sin \Omega + \sin \phi_\parallel \cos \Omega \cos i) + v_{\phi_\parallel} ( - \sin \phi_\parallel \sin \Omega + \cos \phi_\parallel \cos \Omega \cos i)  \\
v_z &= v_{R_\parallel} \sin \phi_\parallel \sin i + v_{\phi_\parallel} \cos \phi_\parallel \sin i 
\end{align}
\begin{align}
\phi &= \arctan2 \left( y, x \right) = \arctan ( \tan \phi_\parallel \cos i ) + \Omega \\
R &= \sqrt{x^2 + y^2} = \sqrt{x_\parallel^2 + y_\parallel^2 \cos^2 i} = R_\parallel \sqrt{\cos^2 \phi_\parallel + \sin^2 \phi_\parallel \cos^2 i} \\
v_\phi &= R \frac{d}{d t} \phi = v_{\phi_\parallel} \frac{\cos i}{\sqrt{\cos^2 \phi_\parallel + \sin^2 \phi_\parallel \cos^2 i}} = v_{\phi_\parallel} \frac{R_\parallel}{R} \cos i \\
v_R &= \frac{d}{dt} R = v_{R_\parallel} \frac{R}{R_\parallel} + v_{\phi_\parallel} \frac{R_\parallel}{R} \left( \cos^2 i - 1 \right) \sin \phi_\parallel \cos \phi_\parallel 
\end{align}

Ellipticity is then
\begin{align}
j_z / j_c(E) &= \frac{R v_\phi}{j_c(E)} = \frac{R_\parallel v_{\phi_\parallel} \cos i}{j_c(E)} \\
&= j_{z\parallel}/j_c(E) \cos i
\end{align}

What does this do to dispersions? 
Seems that $\phi_\parallel(r)$ is needed.  very tricky to calculate...

\begin{align}
\sigma_{v_R}^2    &= \langle v_R^2    \rangle - \langle v_R    \rangle^2 \\
\sigma_{v_\phi}^2 &= \langle v_\phi^2 \rangle - \langle v_\phi \rangle^2 \\
\sigma_{v_z}^2    &= \langle v_z^2    \rangle - \langle v_z    \rangle^2
\end{align}


\newpage

\section{Plots}

\begin{figure}[h]
\includegraphics[width=\textwidth]{components_over_time.png}
\caption{How the components evolve over time. Stars are sorted into components based only on their value of $j_z/j_c$.}
\label{components_over_time}
\end{figure}

\begin{figure}[h]
\includegraphics[width=\textwidth]{jzonc_10.png}
\caption{Histograms of $j_z/j_c$ after 10 simulation times.}
\label{jzonc}
\end{figure}


\end{document}