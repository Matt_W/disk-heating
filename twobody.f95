subroutine star_potential(md,xd,yd,zd,ms,xs,ys,zs,eps,nd,ns,pots)
use omp_lib
implicit none

integer*4, intent(in) :: nd,ns
real*8, intent(in), dimension(nd) :: md,xd,yd,zd
real*8, intent(in), dimension(ns) :: ms,xs,ys,zs
real*8, intent(in) :: eps
real*8, intent(out), dimension(ns) :: pots
integer*4 :: i,j
real*8 :: dist
integer :: num_threads

real*8, dimension(ns) :: ms2,xs2,ys2,zs2

do i=1,ns
  pots(i) = 0.0
  ms2(i) = ms(i)
  xs2(i) = xs(i)
  ys2(i) = ys(i)
  zs2(i) = zs(i)
enddo

!$omp parallel do private(i,j,dist) shared(pots,xs,ys,zs,ms,xd,yd,zd,md)
do i=1,ns
  do j=1,nd
    dist = sqrt( (xs(i)-xd(j))**2 + (ys(i)-yd(j))**2 + (zs(i)-zd(j))**2 + eps**2)
    pots(i) = pots(i)+md(j)/dist
  enddo
enddo
!$omp end parallel do

!$omp parallel do private(i,j,dist) shared(pots,xs,ys,zs,ms,xs2,ys2,zs2,ms2)
do i=1,ns
  do j=1,ns
    dist = sqrt( (xs(i)-xs2(j))**2 + (ys(i)-ys2(j))**2 + (zs(i)-zs2(j))**2 + eps**2)
    pots(i) = pots(i)+ms2(j)/dist
  enddo
enddo
!$omp end parallel do

end subroutine star_potential


subroutine dm_potential(md,xd,yd,zd,ms,xs,ys,zs,eps,nd,ns,potd)
use omp_lib
implicit none

integer*4, intent(in) :: nd,ns
real*8, intent(in), dimension(nd) :: md,xd,yd,zd
real*8, intent(in), dimension(ns) :: ms,xs,ys,zs
real*8, intent(in) :: eps
real*8, intent(out), dimension(nd) :: potd
integer*4 :: i,j
real*8 :: dist
integer :: num_threads

real*8, dimension(nd) :: md2,xd2,yd2,zd2

do i=1,nd
  potd(i) = 0.0
  md2(i) = md(i)
  xd2(i) = xd(i)
  yd2(i) = yd(i)
  zd2(i) = zd(i)
enddo

!$omp parallel do private(i,j,dist) shared(potd,xs,ys,zs,ms,xd,yd,zd,md)
do i=1,nd
  do j=1,ns
    dist = sqrt( (xd(i)-xs(j))**2 + (yd(i)-ys(j))**2 + (zd(i)-zs(j))**2 + eps**2)
    potd(i) = potd(i)+ms(j)/dist
  enddo
enddo
!$omp end parallel do

!$omp parallel do private(i,j,dist) shared(potd,xd,yd,zd,md,xd2,yd2,zd2,md2)
do i=1,nd
  do j=1,nd
    dist = sqrt( (xd(i)-xd2(j))**2 + (yd(i)-yd2(j))**2 + (zd(i)-zd2(j))**2 + eps**2)
    potd(i) = potd(i)+md2(j)/dist
  enddo
enddo
!$omp end parallel do

end subroutine dm_potential


subroutine midplane_potential(md,xd,yd,zd,ms,xs,ys,zs,xin,yin,zin,eps,nd,ns,ni,pote)
implicit none

integer*4, intent(in) :: nd,ns,ni
real*8, intent(in), dimension(nd) :: md,xd,yd,zd
real*8, intent(in), dimension(ns) :: ms,xs,ys,zs
real*8, intent(in), dimension(ni) :: xin,yin,zin
real*8, intent(in) :: eps
real*8, intent(out), dimension(ni) :: pote
integer*4 :: i,j
real*8 :: dist

do i=1,ni
  pote(i) = 0.0
enddo

do i=1,ni
  do j=1,nd
    dist = sqrt( (xin(i)-xd(j))**2 + (yin(i)-yd(j))**2 + (zin(i)-zd(j))**2 + eps**2)
    pote(i) = pote(i)+md(j)/dist
  enddo
enddo

do i=1,ni
  do j=1,ns
    dist = sqrt( (xin(i)-xs(j))**2 + (yin(i)-ys(j))**2 + (zin(i)-zs(j))**2 + eps**2)
    pote(i) = pote(i)+ms(j)/dist
  enddo
enddo

end subroutine midplane_potential


subroutine midplane_acceleration(md,xd,yd,zd,ms,xs,ys,zs,xin,yin,zin,eps,nd,ns,ni,acc_concat)
implicit none

integer*4, intent(in) :: nd,ns,ni
real*8, intent(in), dimension(nd) :: md,xd,yd,zd
real*8, intent(in), dimension(ns) :: ms,xs,ys,zs
real*8, intent(in), dimension(ni) :: xin,yin,zin
real*8, intent(in) :: eps
real*8, intent(out), dimension(3*ni) :: acc_concat
integer*4 :: i,j
real*8 :: dist32
real*8, dimension(ni) :: acc_x, acc_y, acc_z

do i=1,ni
  acc_x(i) = 0.0
  acc_y(i) = 0.0
  acc_z(i) = 0.0
enddo

do i=1,3*ni
  acc_concat(i) = 0.0
enddo

do i=1,ni
  do j=1,nd
    dist32 = (sqrt( (xin(i)-xd(j))**2 + (yin(i)-yd(j))**2 + (zin(i)-zd(j))**2 + eps**2))**3
    acc_x(i) = acc_x(i)+md(j)/dist32*(xin(i)-xd(j))
    acc_y(i) = acc_y(i)+md(j)/dist32*(yin(i)-yd(j))
    acc_z(i) = acc_z(i)+md(j)/dist32*(zin(i)-zd(j))
  enddo
enddo

do i=1,ni
  do j=1,ns
    dist32 = (sqrt( (xin(i)-xs(j))**2 + (yin(i)-ys(j))**2 + (zin(i)-zs(j))**2 + eps**2))**3
    acc_x(i) = acc_x(i)+ms(j)/dist32*(xin(i)-xs(j))
    acc_y(i) = acc_y(i)+ms(j)/dist32*(yin(i)-ys(j))
    acc_z(i) = acc_z(i)+ms(j)/dist32*(zin(i)-zs(j))
  enddo
enddo

do i=1,ni
  acc_concat(i) = acc_x(i)
enddo    
do i=1+ni,2*ni
  acc_concat(i) = acc_y(i - ni)
enddo    
do i=1+2*ni,3*ni
  acc_concat(i) = acc_z(i - 2*ni)
enddo    

end subroutine midplane_acceleration


subroutine midplane_vcirc2(md,xd,yd,zd,ms,xs,ys,zs,xin,yin,zin,eps,nd,ns,ni,vcirc2)
implicit none

integer*4, intent(in) :: nd,ns,ni
real*8, intent(in), dimension(nd) :: md,xd,yd,zd
real*8, intent(in), dimension(ns) :: ms,xs,ys,zs
real*8, intent(in), dimension(ni) :: xin,yin,zin
real*8, intent(in) :: eps
real*8, intent(out), dimension(ni) :: vcirc2
integer*4 :: i,j
real*8 :: dist32
real*8, dimension(ni) :: acc_x, acc_y, acc_z

do i=1,ni
  acc_x(i) = 0.0
  acc_y(i) = 0.0
!   acc_z(i) = 0.0
enddo

do i=1,ni
  do j=1,nd
    dist32 = (sqrt( (xin(i)-xd(j))**2 + (yin(i)-yd(j))**2 + (zin(i)-zd(j))**2 + eps**2))**3
    acc_x(i) = acc_x(i)+md(j)/dist32*(xin(i)-xd(j))
    acc_y(i) = acc_y(i)+md(j)/dist32*(yin(i)-yd(j))
!     acc_z(i) = acc_z(i)+md(j)/dist32*(zin(i)-zd(j))
  enddo
enddo

do i=1,ni
  do j=1,ns
    dist32 = (sqrt( (xin(i)-xs(j))**2 + (yin(i)-ys(j))**2 + (zin(i)-zs(j))**2 + eps**2))**3
    acc_x(i) = acc_x(i)+ms(j)/dist32*(xin(i)-xs(j))
    acc_y(i) = acc_y(i)+ms(j)/dist32*(yin(i)-ys(j))
!     acc_z(i) = acc_z(i)+ms(j)/dist32*(zin(i)-zs(j))
  enddo
enddo

do i=1,ni
  vcirc2(i) = acc_x(i)*xin(i) + acc_y(i)*yin(i)
enddo    

end subroutine midplane_vcirc2


subroutine star_vcirc2(md,xd,yd,zd,ms,xs,ys,zs,eps,nd,ns,vcirc2)
implicit none

integer*4, intent(in) :: nd,ns
real*8, intent(in), dimension(nd) :: md,xd,yd,zd
real*8, intent(in), dimension(ns) :: ms,xs,ys,zs
real*8, intent(in) :: eps
real*8, intent(out), dimension(ns) :: vcirc2
integer*4 :: i,j
real*8 :: dist32
real*8, dimension(ns) :: acc_x, acc_y, acc_z
real*8, dimension(ns) :: ms2,xs2,ys2,zs2

do i=1,ns
  acc_x(i) = 0.0
  acc_y(i) = 0.0
!   acc_z(i) = 0.0
  ms2(i) = ms(i)
  xs2(i) = xs(i)
  ys2(i) = ys(i)
  zs2(i) = zs(i)
enddo

!$omp parallel do private(i,j,dist32) shared(acc_x,acc_y,xs,ys,zs,ms,xd,yd,zd,md)
do i=1,ns
  do j=1,nd
    dist32 = (sqrt( (xs(i)-xd(j))**2 + (ys(i)-yd(j))**2 + (zs(i)-zd(j))**2 + eps**2))**3
    acc_x(i) = acc_x(i)+md(j)/dist32*(xs(i)-xd(j))
    acc_y(i) = acc_y(i)+md(j)/dist32*(ys(i)-yd(j))
!     acc_z(i) = acc_z(i)+md(j)/dist32*(zin(i)-zd(j))
  enddo
enddo
!$omp end parallel do

!$omp parallel do private(i,j,dist32) shared(acc_x,acc_y,xs,ys,zs,ms,xs2,ys2,zs2,ms2)
do i=1,ns
  do j=1,ns
    dist32 = (sqrt( (xs(i)-xs2(j))**2 + (ys(i)-ys2(j))**2 + (zs(i)-zs2(j))**2 + eps**2))**3
    acc_x(i) = acc_x(i)+ms2(j)/dist32*(xs(i)-xs2(j))
    acc_y(i) = acc_y(i)+ms2(j)/dist32*(ys(i)-ys2(j))
!     acc_z(i) = acc_z(i)+ms(j)/dist32*(zs(i)-zs2(j))
  enddo
enddo
!$omp end parallel do

do i=1,ns
  vcirc2(i) = acc_x(i)*xs(i) + acc_y(i)*ys(i)
enddo    

end subroutine star_vcirc2
