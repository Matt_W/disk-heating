#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 26 Apr 2022

@author: matt
"""

from matplotlib import rcParams

rcParams['ps.useafm'] = True
rcParams['pdf.use14corefonts'] = False
rcParams['text.usetex'] = False
rcParams['font.sans-serif'] = ['cmr10', 'Times-Roman']
rcParams['font.weight'] = 'normal'
rcParams['font.size'] = 20
rcParams['xtick.labelsize'] = 20
rcParams['ytick.labelsize'] = 20
rcParams['axes.labelsize'] = 22
rcParams['axes.linewidth'] = 1
rcParams['axes.unicode_minus'] = False
# rcParams['axes.grid'] = True
rcParams['axes.grid'] = False
rcParams['xtick.minor.width'] = 1
rcParams['ytick.minor.width'] = 1
rcParams['xtick.major.width'] = 1
rcParams['ytick.major.width'] = 1
rcParams['xtick.minor.size'] = 4
rcParams['ytick.minor.size'] = 4
rcParams['xtick.major.size'] = 5
rcParams['ytick.major.size'] = 5
rcParams['xtick.direction'] = 'in'
rcParams['ytick.direction'] = 'in'
rcParams['xtick.top'] = True
rcParams['ytick.right'] = True
rcParams['mathtext.fontset'] = 'cm'

import numpy as np

import matplotlib.pyplot as plt

# Create a function to apply boundary conditions
def apply_boundary(position, grid_size=1, periodic=True):
  '''periodic or not
  '''
  if not periodic:
    return position

  #too high
#  while (len(position[position > grid_size]) > 0):
  position[position > grid_size] = position[position > grid_size] - 2*grid_size

  #too low
#  while (len(position[position < -grid_size]) > 0):
  position[position < -grid_size] = position[position < -grid_size] + 2*grid_size

  return position


def distances(position, grid_size=1, periodic=True):
    '''Make an array of distances to each point
    '''
    # find N x N x Nd matrix of particle separations
    grid_r = position[:, None, :] - position[:, :, None]

    min_r = grid_r

    if periodic:
      plus_r = grid_r + 2*grid_size
      minus_r = grid_r - 2*grid_size
      min_r = np.min((np.abs(grid_r), np.abs(plus_r), np.abs(minus_r)), axis=0)

    return np.power(np.sum(min_r * min_r, axis=0), 0.5)


def energy_0(mass, position, velocity, grid_size=1, soft_length=1e-3, periodic=True):

    # # find N x N x Nd matrix of particle separations
    # grid_r = position[:, np.newaxis, :] - position[:, :, np.newaxis]

    # find N x Nd matrix of particle separations
    grid_r = position[:, 0, None] - position

    #vector of minimum distances
    min_r = grid_r

    if periodic:
        plus_r = grid_r + 2*grid_size
        minus_r= grid_r - 2*grid_size
        min_r = np.min((np.abs(grid_r), np.abs(plus_r), np.abs(minus_r)), axis=0)

        grid_r[np.abs(grid_r) != min_r] = 0
        plus_r[np.abs(plus_r) != min_r] = 0
        minus_r[np.abs(minus_r) != min_r] = 0

        min_r = np.sign(grid_r + plus_r + minus_r) * min_r

    # N x N particle seps
    # N
    r = np.linalg.norm(min_r, axis=0)

    # potential_0 = -np.sum(mass[:, np.newaxis] / np.sqrt(r**2 + soft_length**2))
    # kinetic_0 = np.sum(0.5 * mass * np.linalg.norm(velocity, axis=0)**2)

    potential_0 = -np.sum(mass / np.sqrt(r**2 + soft_length**2))
    kinetic_0 = 0.5 * mass[0] * np.linalg.norm(velocity[:, 0])**2

    energy = potential_0 + kinetic_0

    # print(kinetic_0, potential_0, energy)

    return energy


def force(position, mass, grid_size=1, soft_length=1e-3, periodic=True):
    # # find N x N x Nd matrix of particle separations
    # grid_r = position[:, np.newaxis, :] - position[:, :, np.newaxis]
    #
    # # vector of minimum distances
    # min_r = grid_r
    #
    # if periodic:
    #     plus_r = grid_r + 2 * grid_size
    #     minus_r = grid_r - 2 * grid_size
    #     min_r = np.min((np.abs(grid_r), np.abs(plus_r), np.abs(minus_r)), axis=0)
    #
    #     grid_r[np.abs(grid_r) != min_r] = 0
    #     plus_r[np.abs(plus_r) != min_r] = 0
    #     minus_r[np.abs(minus_r) != min_r] = 0
    #
    #     min_r = np.sign(grid_r + plus_r + minus_r) * min_r
    #
    # # N x N particle seps
    # r = np.linalg.norm(min_r, axis=0)
    #
    # # calculate force between each pair of particles. includes softening length
    # # not the usual definition of softening length
    # # force_i = mass[None, :] * mass[:, None] * min_r / np.power(np.sum(min_r * min_r, axis=0) + soft_length, 1.5)
    # force_i = np.sum(mass[np.newaxis, :] * mass[:, np.newaxis] * min_r / np.power(r**2 + soft_length**2, -3/2), axis=2)
    #
    # out = force_i

    # find N x Nd matrix of particle separations
    grid_r = position[:, 0, None] - position

    #vector of minimum distances
    min_r = grid_r

    if periodic:
        plus_r = grid_r + 2*grid_size
        minus_r= grid_r - 2*grid_size
        min_r = np.min((np.abs(grid_r), np.abs(plus_r), np.abs(minus_r)), axis=0)

        grid_r[np.abs(grid_r) != min_r] = 0
        plus_r[np.abs(plus_r) != min_r] = 0
        minus_r[np.abs(minus_r) != min_r] = 0

        min_r = np.sign(grid_r + plus_r + minus_r) * min_r

    r = np.linalg.norm(min_r, axis=0)

    #calculate force
    # force_i = np.nansum(mass[0] * mass * min_r / (r + soft_length)**3, axis=1)
    force_i = np.nansum(mass[0] * mass * min_r * np.power(r**2 + soft_length**2, -3/2), axis=1)

    out = np.zeros((np.shape(grid_r)))
    out[:, 0] = force_i

    return out


def update(mass, position, velocity, acceleration, dt=1e-1, grid_size=1, soft_length=1e-3):

    #kick-drift leap frog
    velocity += acceleration * dt / 2

    position += velocity * dt
    position = apply_boundary(position, grid_size=grid_size) # Apply boundary conditions

    acceleration = force(position, mass, grid_size=grid_size, soft_length=soft_length) / mass

    velocity += acceleration * dt / 2

    # # #4th Yoshida
    # thirdroottwo = np.power(2, 1/3)
    #
    # w_0 = - thirdroottwo / (2 - thirdroottwo)
    # w_1 = 1 / (2 - thirdroottwo)
    # c_1 = c_4 = w_1 / 2
    # c_2 = c_3 = (w_0 + w_1) / 2
    # d_1 = d_3 = w_1
    # d_2 = w_0
    #
    # x_1 = position + c_1 * velocity * dt
    # x_1 = apply_boundary(x_1, grid_size=grid_size)  # Apply boundary conditions
    # v_1 = velocity + d_1 * force(x_1, mass, grid_size=grid_size, soft_length=soft_length) / mass * dt
    # x_2 = x_1 + c_2 * v_1 * dt
    # x_2 = apply_boundary(x_2, grid_size=grid_size)  # Apply boundary conditions
    # v_2 = v_1 + d_2 * force(x_2, mass, grid_size=grid_size, soft_length=soft_length) / mass * dt
    # x_3 = x_2 + c_3 * v_2 * dt
    # x_3 = apply_boundary(x_3, grid_size=grid_size)  # Apply boundary conditions
    #
    # acceleration = force(x_3, mass, grid_size=grid_size, soft_length=soft_length) / mass
    # v_3 = v_2 + d_3 * acceleration * dt
    #
    # position = x_3 + c_4 * v_3 * dt
    # position = apply_boundary(position, grid_size=grid_size)  # Apply boundary conditions
    # velocity = v_3

    return mass, position, velocity, acceleration


def main():
    seeds = 2

    G = 1 #in units of total_box_mass^-1 side_length^3 / time^2

    Nd = 3 #number of dimensions

    # Np = 10 ** Nd #number of particles
    Np = 10 ** Nd #number of particles

    total_box_mass = 1
    mp = total_box_mass / Np #particle mass

    grid_size = 0.5 #0.5 #half side length
    L = 2 * grid_size

    soft_length = 1e-3 * L #units of side_length

    R = L / Np**(1/3) #1D interparticle distance, units of side_length

    v_c = np.sqrt(G * mp / R)
    # T_c = np.sqrt(R**3 / (G * mp))
    T_c = L ** (3 / 2) * (total_box_mass * G) ** (-1 / 2)

    v_esc = np.sqrt(2 * G * mp / soft_length)

    dt = 1e-2
    # Nt = int(1 * T_c / dt) #needs to be > T_c / dt
    Nt = int(2 / dt)
    print('Nt = ', Nt)

    v_part = grid_size #10*v_c #grid_size
    v_bg = 0 #grid_size / 10 #0#v_part

    print('v_part = ', v_part)

    # init_trajs = np.zeros(((seeds, Nd)))
    # init_vels = np.zeros(((seeds, Nd)))
    # init_accs = np.zeros(((seeds, Nd)))

    test_trajs = np.zeros(((seeds, Nt, Nd)))
    test_vels = np.zeros(((seeds, Nt, Nd)))
    test_accs = np.zeros(((seeds, Nt, Nd)))

    energies = np.zeros(((seeds, Nt)))

    for seed in range(seeds):
        np.random.seed(seed + 100)

        position = grid_size * (1 - 2 * np.random.random((Nd, Np)))
        # velocity = v_bg * (1 - 2 * np.random.random((Nd, Np)))
        velocity = np.random.normal(0, v_bg * np.sqrt(np.pi/8), (Nd, Np)) #average speed is v_bg #Maxwellian

        # velocity[:, 0] = 1 - 2 * np.random.random(Nd)
        # velocity[:, 0] = v_part * velocity[:, 0] / np.linalg.norm(velocity[:, 0])

        position[:, 0] = [-grid_size/2, 0, 0]

        velocity[:, 0] = [v_part, 0, 0]

        mass = mp * np.ones(Np)

        acceleration = force(position, mass, grid_size=grid_size, soft_length=soft_length) / mass

        # init_trajs[seed, :] = position[:, 0]
        # init_vels[seed, :] = velocity[:, 0]
        # init_accs[seed, :] = acceleration[:, 0]

        for i in range(Nt):
            (mass, position, velocity, acceleration
             ) = update(mass, position, velocity, acceleration,
                        dt=dt, grid_size=grid_size, soft_length=soft_length)

            test_trajs[seed, i, :] = position[:, 0]
            test_vels[seed, i, :] = velocity[:, 0]
            test_accs[seed, i, :] = acceleration[:, 0]

            energies[seed, i] = energy_0(mass, position, velocity)

        print(seed)

    # test_trajs = test_trajs - np.array([-grid_size/2, 0, 0])[np.newaxis, :] - grid_size
    # test_trajs %= grid_size * 2
    # test_trajs -= grid_size

    periodic_threshold = 0.8

    diffs = np.diff(test_trajs, axis=1)
    xs = np.zeros(np.shape(diffs))

    xs[diffs >  periodic_threshold * 2 * grid_size] = -2 * grid_size
    xs[diffs < -periodic_threshold * 2 * grid_size] =  2 * grid_size

    xs = np.cumsum(xs, axis=1)

    test_trajs[:, 1:, :] += xs

    data = np.mean(energies, axis=0)
    expectations_vs_time(data, Nt=Nt, dt=dt, all_trajs=energies, label=r'$E$',
                         name='../galaxies/particle-tests/evolution_energies', save=True)

    # test_trajs = test_trajs - init_trajs[:, None, :]
    x_max = np.amax([np.amax(np.linalg.norm(test_trajs[seed, :, :], axis=1)) for seed in range(seeds)])
    plot_trajectory_projections(test_trajs, seeds=seeds, Nt=Nt, grid_size=x_max,#grid_size,
                                name='../galaxies/particle-tests/proj_x', save=True)
    v_max = np.amax([np.amax(np.linalg.norm(test_vels[seed, :, :], axis=1)) for seed in range(seeds)])
    plot_trajectory_projections(test_vels, seeds=seeds, Nt=Nt, grid_size=v_max,
                                name='../galaxies/particle-tests/proj_v', save=True)
    a_max = np.amax([np.amax(np.linalg.norm(test_accs[seed, :, :], axis=1)) for seed in range(seeds)])
    plot_trajectory_projections(test_accs, seeds=seeds, Nt=Nt, grid_size=a_max,
                                name='../galaxies/particle-tests/proj_a', save=True)

    #velocities
    data = np.mean(test_vels[:, :, 0], axis=0)
    expectations_vs_time(data, Nt=Nt, dt=dt, all_trajs=test_vels[:, :, 0], label=r'$\overline{v}_\parallel$',
                         name='../galaxies/particle-tests/evolution_median_parallel', save=True)
    data = np.mean(test_vels[:, :, 1], axis=0)
    expectations_vs_time(data, Nt=Nt, dt=dt, all_trajs=test_vels[:, :, 1], label=r'$\overline{v}_y$',
                         name='../galaxies/particle-tests/evolution_median_y', save=True)
    data = np.mean(test_vels[:, :, 2], axis=0)
    expectations_vs_time(data, Nt=Nt, dt=dt, all_trajs=test_vels[:, :, 2], label=r'$\overline{v}_z$',
                         name='../galaxies/particle-tests/evolution_median_z', save=True)
    perp_vels = np.sqrt(test_vels[:, :, 1]**2 + test_vels[:, :, 2]**2)
    data = np.mean(perp_vels, axis=0)
    expectations_vs_time(data, Nt=Nt, dt=dt, all_trajs=perp_vels, label=r'$\overline{v}_\perp$',
                         name='../galaxies/particle-tests/evolution_median_perp', save=True)
    data = np.mean(np.linalg.norm(test_vels[:, :, :], axis=2), axis=0)
    expectations_vs_time(data, Nt=Nt, dt=dt, all_trajs=np.linalg.norm(test_vels[:, :, :], axis=2), label=r'$\overline{|v|}$',
                         name='../galaxies/particle-tests/evolution_median_abs', save=True)

    data = np.std(test_vels[:, :, 0], axis=0)**2
    expectations_vs_time(data, Nt=Nt, dt=dt, label=r'$\sigma_\parallel^2$',
                         name='../galaxies/particle-tests/evolution_sigma_parallel', save=True)
    data = np.std(test_vels[:, :, 1], axis=0)**2
    expectations_vs_time(data, Nt=Nt, dt=dt, label=r'$\sigma_y^2$',
                         name='../galaxies/particle-tests/evolution_sigma_y', save=True)
    data = np.std(test_vels[:, :, 2], axis=0)**2
    expectations_vs_time(data, Nt=Nt, dt=dt, label=r'$\sigma_z^2$',
                         name='../galaxies/particle-tests/evolution_sigma_z', save=True)
    data = np.std(test_vels[:, :, 1], axis=0)**2 + np.std(test_vels[:, :, 2], axis=0)**2
    expectations_vs_time(data, Nt=Nt, dt=dt, label=r'$\sigma_\perp^2$',
                         name='../galaxies/particle-tests/evolution_sigma_perp', save=True)
    data = np.std(np.linalg.norm(test_vels[:, :, :], axis=2), axis=0)**2
    expectations_vs_time(data, Nt=Nt, dt=dt, label=r'$\sigma_{|v|}^2$',
                         name='../galaxies/particle-tests/evolution_sigma_abs', save=True)

    #acceleration
    data = np.mean(test_accs[:, :, 0], axis=0)
    expectations_vs_time(data, Nt=Nt, dt=dt, all_trajs=test_accs[:, :, 0], label=r'$\overline{a}_\parallel$',
                         name='../galaxies/particle-tests/evolution_median_a_parallel', save=True)
    data = np.mean(test_accs[:, :, 1], axis=0)
    expectations_vs_time(data, Nt=Nt, dt=dt, all_trajs=test_accs[:, :, 1], label=r'$\overline{a}_y$',
                         name='../galaxies/particle-tests/evolution_median_a_y', save=True)
    data = np.mean(test_accs[:, :, 2], axis=0)
    expectations_vs_time(data, Nt=Nt, dt=dt, all_trajs=test_accs[:, :, 2], label=r'$\overline{a}_z$',
                         name='../galaxies/particle-tests/evolution_median_a_z', save=True)

    data = np.std(test_accs[:, :, 0], axis=0)**2
    expectations_vs_time(data, Nt=Nt, dt=dt, label=r'$\sigma_{a,\parallel}^2$',
                         name='../galaxies/particle-tests/evolution_sigma_a_parallel', save=True)
    data = np.std(test_accs[:, :, 1], axis=0)**2
    expectations_vs_time(data, Nt=Nt, dt=dt, label=r'$\sigma_{a,y}^2$',
                         name='../galaxies/particle-tests/evolution_sigma_a_y', save=True)
    data = np.std(test_accs[:, :, 2], axis=0)**2
    expectations_vs_time(data, Nt=Nt, dt=dt, label=r'$\sigma_{a,z}^2$',
                         name='../galaxies/particle-tests/evolution_sigma_a_z', save=True)

    return


def plot_trajectory_projections(test_trajs, seeds, Nt,
                                grid_size=1, name='', save=False):

    fig = plt.figure(figsize=(10,10)) # Create frame and set size

    ax1 = plt.subplot(111) # For normal 2D projection
    plt.xlim(-grid_size,grid_size)  # Set x-axis limits
    plt.ylim(-grid_size,grid_size)  # Set y-axis limits

    for seed in range(seeds):
        plt.scatter(test_trajs[seed, :, 0], test_trajs[seed, :, 1],
                    c=np.linspace(0, 1-1e-3, Nt), cmap='turbo', s=10)
        plt.errorbar(test_trajs[seed, :, 0], test_trajs[seed, :, 1])

    ax1.set_xlabel(r'$x$')
    ax1.set_ylabel(r'$y$')
    # plt.show()

    if save:
        plt.savefig(name, bbox_inches='tight')

    return


def expectations_vs_time(data, Nt, dt, all_trajs=None, label='',
                         name='', save=False):

    fig = plt.figure(figsize=(8,5)) # Create frame and set size

    ax1 = plt.subplot(111) # For normal 2D projection

    plt.xlim(0, dt * Nt)  # Set y-axis limits
    plt.ylim(np.amin(data), np.amax(data))  # Set y-axis limits

    if all_trajs is not None:
        plt.ylim(np.amin(all_trajs), np.amax(all_trajs))  # Set y-axis limits

        for seed in range(np.shape(all_trajs)[0]):
            plt.errorbar(np.linspace(0, dt * Nt, Nt), all_trajs[seed, :], lw=1, c='C0', alpha=0.8)

    plt.errorbar(np.linspace(0, dt * Nt, Nt), data, lw=10, c='k', alpha=0.8)

    ax1.set_xlabel(r'$t$')
    ax1.set_ylabel(label)

    if save:
        plt.savefig(name, bbox_inches='tight')

    return



    return


if __name__ == '__main__':
    main()
    pass