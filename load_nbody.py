#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 26 11:03:15 2020

@author: matt
"""

from functools import lru_cache

import numpy as np
import h5py as h5

import halo_calculations as halo

def load_snapshot(file, snap):
    '''Read the test files and unitify the outputs
    '''
    file_name = '../galaxies/' + str(file) + '/data/snapshot_' + str(snap) + '.hdf5'

    fs = h5.File(file_name,"r")

    try:
        MassDM = fs['PartType1/Masses'][0]
        PosDMs = fs['PartType1/Coordinates'][:]
        VelDMs = fs['PartType1/Velocities'][:]
        IDDMs  = fs['PartType1/ParticleIDs'][:]

        MassStar = fs['PartType2/Masses'][0]
        PosStars = fs['PartType2/Coordinates'][:]
        VelStars = fs['PartType2/Velocities'][:]
        IDStars  = fs['PartType2/ParticleIDs'][:]

        try:
            PotDMs = fs['PartType1/Potential'][:]
            PotStars = fs['PartType2/Potential'][:]
        except KeyError:
            if int(snap) < 10:
                print(f'Warning: {file} has no potentials')
            PotDMs = np.nan * np.ones(len(IDDMs))
            PotStars = np.nan * np.ones(len(IDStars))

    except KeyError:
        print('Trying to load smooth halo')

        MassDM = np.zeros(0)
        PosDMs = np.zeros(0)
        VelDMs = np.zeros(0)
        IDDMs = np.zeros(0)

        MassStar = fs['PartType1/Masses'][0]
        PosStars = fs['PartType1/Coordinates'][:]
        VelStars = fs['PartType1/Velocities'][:]
        IDStars = fs['PartType1/ParticleIDs'][:]

        PotStars = fs['PartType1/Potential'][:]

        try:
            PotStars = fs['PartType2/Potential'][:]
        except KeyError:
            if int(snap) < 10:
                print(f'Warning: {file} has no potentials')
            PotStars = np.nan * np.ones(len(IDStars))

    fs.close()

    #turns out mass unit doesn't matter
    # MassStar = (MassStar * 10**10 * u.Msun).to(10**10 * u.Msun)
    # PosStars *= u.kpc
    # PosStars = PosStars.to(u.Mpc)
    # VelStars *= u.km / u.s

    return(file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,   PotDMs,
                      MassStar, PosStars, VelStars, IDStars, PotStars)

@lru_cache(maxsize=10)
def load_and_align(name, snap):
    '''
    '''
    (file_name, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
      MassStar, PosStars, VelStars, IDStars, PotStars) = load_snapshot(
        name, snap)

    try:
        # centre galaxy. needs potentials done first.
        if not np.isnan(PotDMs[0]):
            (PosDMs, VelDMs, PosStars, VelStars
              ) = halo.centre_maximum_potential(PosDMs,   VelDMs,   MassDM,   PotDMs,
                                          PosStars, VelStars, MassStar, PotStars)
        else:
            if int(snap) < 10:
                print('Shrinking spheres')
            (PosDMs, VelDMs, PosStars, VelStars
              ) = halo.centre_shrink_spheres(PosDMs,   VelDMs,   MassDM,   PotDMs,
                                             PosStars, VelStars, MassStar, PotStars)

        #align orientation of disk
        j_tot = halo.total_angular_momentum(PosStars, VelStars, MassStar)
        #find rotation
        rotation = halo.find_rotaion_matrix(j_tot)

        #rotate DM
        PosDMs = rotation.apply(PosDMs)
        VelDMs = rotation.apply(VelDMs)
        #rotate stars
        PosStars = rotation.apply(PosStars)
        VelStars = rotation.apply(VelStars)

    except IndexError:
        print('Trying to load smooth halo')

        # #align orientation of disk
        # j_tot = halo.total_angular_momentum(PosStars, VelStars, MassStar)
        # #find rotation
        # rotation = halo.find_rotaion_matrix(j_tot)
        #
        # #rotate stars
        # PosStars = rotation.apply(PosStars)
        # VelStars = rotation.apply(VelStars)

    return(file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,   PotDMs,
                      MassStar, PosStars, VelStars, IDStars, PotStars)

def load_and_calculate(file, snap):

    (file_name, MassDM, PosDMs, VelDMs, IDDMs, PotDMs,
      MassStar, PosStars, VelStars, IDStars, PotStars) = load_snapshot(file, snap)

    (PosStars, VelStars, star_mask, PosDMs, VelDMs, dm_mask,
      j_z_stars, j_p_stars, j_c_stars, energy_stars,
      E_interp, j_circ_interp, R_interp) = halo.calculate_star_phase_space(
        PosStars, VelStars, MassStar, PotDMs,
        PosDMs,   VelDMs,   MassDM,   PotStars,
        name=file, snap=snap)

    IDDMs   = IDDMs[dm_mask]
    IDStars = IDStars[star_mask]

    # halo.plot_potential_verify(PosDMs,   VelDMs,   MassDM*np.ones(sum(dm_mask)),
    #                            PotDMs   + 0.5 * np.square(np.linalg.norm(VelDMs, axis=1)),
    #                            PosStars, VelStars, MassStar*np.ones(sum(star_mask)),
    #                            PotStars + 0.5 * np.square(np.linalg.norm(VelStars, axis=1)),
    #                            j_circ_interp, E_interp, R_interp)

    return(file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,
                      MassStar, PosStars, VelStars, IDStars,
                      j_z_stars, j_p_stars, j_c_stars, energy_stars,
                      E_interp, j_circ_interp, R_interp)


if __name__ == '__main__':

    # (file_name, MassDM, PosDMs, VelDMs, IDDMs,
    #             MassStar, PosStars, VelStars, IDStars) =
    load_and_calculate('mu_1/fdisk_0p01_lgMdm_6p5_V200-200kmps', snap='100')

    pass