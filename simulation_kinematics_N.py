#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 23 2022

@author: Matthew Wilkinson
"""

import numpy as np

# import scipy.stats
from scipy.optimize import brentq
from scipy.integrate import quad

from scipy.special import erf
from scipy.special import erfinv
from scipy.special import kn, iv
from scipy.special import lambertw
from scipy.special import spence #aka dilogarithm

from scipy.stats import norm

# from sigfig import round

import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from matplotlib import rcParams

rcParams['ps.useafm'] = True
rcParams['pdf.use14corefonts'] = False
rcParams['text.usetex'] = False
rcParams['font.sans-serif'] = ['cmr10', 'Times-Roman']
rcParams['font.weight'] = 'normal'
rcParams['font.size'] = 20
rcParams['xtick.labelsize'] = 20
rcParams['ytick.labelsize'] = 20
rcParams['axes.labelsize'] = 22
rcParams['axes.linewidth'] = 1
rcParams['axes.unicode_minus'] = False
# rcParams['axes.grid'] = True
rcParams['axes.grid'] = False
rcParams['xtick.minor.width'] = 1
rcParams['ytick.minor.width'] = 1
rcParams['xtick.major.width'] = 1
rcParams['ytick.major.width'] = 1
rcParams['xtick.minor.size'] = 4
rcParams['ytick.minor.size'] = 4
rcParams['xtick.major.size'] = 5
rcParams['ytick.major.size'] = 5
rcParams['xtick.direction'] = 'in'
rcParams['ytick.direction'] = 'in'
rcParams['xtick.top'] = True
rcParams['ytick.right'] = True
rcParams['mathtext.fontset'] = 'cm'

GRAV_CONST = 4.301e4  # kpc (km/s)^2 / (10^10Msun) (source ??)
HUBBLE_CONST = 0.06777  # km/s/kpc (Planck 2018)
GALIC_HUBBLE_CONST = 0.1  # h km/s/kpc
RHO_CRIT = 3 * HUBBLE_CONST ** 2 / (8 * np.pi * GRAV_CONST)  # 10^10Msun/kpc^3

PC_ON_M = 3.0857e16  # pc/m = kpc/km (wiki)
GYR_ON_S = 3.15576e16  # gyr/s

M200_for_v200 = 200 ** 3 / (10 * GRAV_CONST * GALIC_HUBBLE_CONST) # 10^10Msun

def find_least_log_squares_best_fit():
    '''Best fit values in paper
    '''
    return([82.3928 * 2 * np.sqrt(2) * np.pi,
            20.18624 * np.sqrt(2) * np.pi,
            20.17192 * np.sqrt(2) * np.pi,
            9.40073 * np.sqrt(2) * np.pi],
           [-0.47021, -0.30776, -0.18912, -0.11541],
           [0.0, 0.0, 0.0, 0.0],
           [0.0, 0.0, 0.0, 0.0])

########################################################################################################################
# Halo + Hernquist + Exponential Disk Eqns
# + Helper functions

# global params
class Galaxy(object):
    def __init__(self, v200=200, conc=10, Rd=None, f_bary=None):
        self.v200 = v200 # km/s
        self.r200 = self.v200 / (10 * GALIC_HUBBLE_CONST) # kpc (/h?)

        self.conc = conc # unitless
        self.r_s = self.r200 / self.conc # kpc

        #halo spherical anisotrpy = 1 - sigma_phi^2 / sigma_r^2
        self.beta = 0 # unitless

        self.M200 = v200 ** 3 / (10 * GRAV_CONST * GALIC_HUBBLE_CONST) #10^10 M_sun (/h?)

        if Rd != None:
            self.Rd = Rd
        else:
            self.Rd = 0.2 * self.r_s # kpc

        if f_bary != None:
            self.f_bary = f_bary
        else:
            self.f_bary = 0.00 # unitless

        self.M_h = (1 - self.f_bary) * self.M200 #10^10 M_sun / kpc^3
        self.M_disk = self.f_bary * self.M200 #10^10 M_sun / kpc^3

        self.rho_200 = self.M_h / (4 / 3 * np.pi * self.r200 ** 3) #10^10 M_sun / kpc^3

        #Springel 2005
        self.a_h = self.r200 / self.conc * np.sqrt(2 * (np.log(1 + self.conc) - self.conc / (1 + self.conc))) # kpc

        self.vmax = np.sqrt(GRAV_CONST * self.M_h / (4 * self.a_h))  # km/s

        return

    def get_r200(self):
        return (self.r200)

    def get_conc(self):
        return (self.conc)

    def get_v200(self):
        return (self.v200)

    def get_rho_200(self):
        return (self.rho_200)

    def get_Rd(self):
        return (self.Rd)

    def get_hernquist_params(self):
        return (self.a_h, self.vmax)

    def get_nfw_params(self):
        return (self.r_s, self.conc, self.v200)

    def get_exponential_disk_params(self):
        return (self.M_disk, self.Rd)

    def get_disk_spin(self):
        v_c = lambda R : np.sqrt(get_exponential_disk_velocity([R]*2, self.M_disk, self.Rd)**2 +
                                 get_analytic_nfw_circular_velocity([R]*2, self.r_s, self.conc, self.v200)**2)

        f = lambda R : R**2 * np.exp(-R / self.Rd) * v_c(R) / self.Rd**2

        out = quad(f, 1e-3, 1e3)

        print(out[0] / (np.sqrt(2) * self.r200 * self.v200))
        print(out[0] / (np.sqrt(2) * self.r200 * self.v200) / 0.03)

# bins
def get_dex_bins(bin_centres, dex=0.1):
    '''Finds bin edges from centres that span a given dex.
    '''
    edges = np.zeros(2 * len(bin_centres))

    for i, centre in enumerate(bin_centres):
        edges[2 * i] = centre * 10 ** (-dex / 2)
        edges[2 * i + 1] = centre * 10 ** (dex / 2)

    return (edges)


def get_bin_edges(save_name_append='diff', fractions=[0.25, 0.5, 0.75], galaxy=Galaxy()):
    '''Get bin edges. Hard coded to self similar galaxies.
    If "diff" gets dex bins around r_1/4, r_1/2 and r_3/4.
    If "cum" get 0 to r_1/4, r_1/2 and r_3/4 bin edges respectively.
    '''
    Rd = galaxy.get_Rd()

    #radii enclosing f fraction of an exponential profile.
    radii = [(-Rd * (lambertw((f - 1) / np.exp(1), -1) + 1)).real for f in fractions]

    if save_name_append == 'diff':
        bin_edges = get_dex_bins(radii)

    elif save_name_append == 'cumulative':
        bin_edges = np.zeros(2 * len(radii))
        for i, r in enumerate(radii):
            bin_edges[2*i + 1] = r

    return (bin_edges)


def dilog_approx(x):
    # lokas & Mamon 2001
    # return (x * np.power(1 + 1/np.sqrt(10) * np.power(-x, 0.62/0.7), -0.7))
    return (spence(1 - x))


def get_analytic_nfw_dispersion(r_h, r_s, c, v200=200, save_name_append='diff'):
    '''What it says in the function name. r_h is actually bin_edges. If diff,
    it will average (in log space) between bins and evaluate at the centre.
    '''
    #this probably should be an argument..
    beta = 0

    # large_r =r_s * c * 1e2 #kpc

    disp_1d = np.zeros(len(r_h) // 2)

    # differential. evaluate at a point
    if save_name_append == 'diff':
        # to fix if the centre is 0
        r_ = np.copy(r_h)
        if r_[0] == 0:
            r_[0] = 0.1

        # bin centres
        r = 10 ** ((np.log10(r_[0::2]) + np.log10(r_[1::2])) / 2)

        xs = r / (r_s * c)

        # e.g. Lokas & Mamon 2001
        g = 1 / (np.log1p(c) - c / (1 + c))

        integrand = lambda _s: (np.power(_s, 2 * beta - 3) * np.log1p(c*_s) / (1 + c*_s)**2 -
                               c * np.power(_s, 2 * beta - 2) / (1 + c*_s)**3)

        for i, s in enumerate(xs):

            if beta == 0:
                disp_1d[i] = 1/2 * c**2 * g * s * (1 + c*s)**2 * (
                        np.pi**2 - np.log(c*s) - 1 / (c*s) - 1 / (1 + c*s)**2 - 6 / (1 + c*s) +
                        (1 + 1 / (c*s)**2 - 4 / (c*s) - 2 / (1 + c*s)) * np.log1p(c*s) +
                        3 * np.log1p(c*s)**2 + 6 * dilog_approx(- c*s) )

            else:
                disp_1d[i] = (g * (1 + c * s)**2 * np.power(s, 1 - 2 * beta) *
                              quad(integrand, s, np.inf)[0])

        disp_1d = np.sqrt(disp_1d) * v200

    # cumulativeulative. evaluate in a range
    elif save_name_append == 'cumulative':
        raise NotImplementedError

    return (disp_1d)


def get_analytic_nfw_circular_velocity(r_h, r_s, c, v200=200, save_name_append='diff'):
    '''What it says in the function name. If diff, it will average (in log space)
    between bins and evaluate at the centre.
    '''
    v_c = np.zeros(len(r_h) // 2)

    # differential. evaluate at a point
    if save_name_append == 'diff':
        # # to fix if the centre is 0
        r_ = np.copy(r_h)
        if r_[0] == 0:
            r_[0] = 0.1

        # bin centres
        r = 10 ** ((np.log10(r_[0::2]) + np.log10(r_[1::2])) / 2)

        xs = r / (r_s * c)

        for i, x in enumerate(xs):

            v_c = 1 / x * (np.log1p(c * x) - (c*x) / (1 + c*x)) / (np.log1p(c) - c / (1 + c))

        v_c = np.sqrt(v_c) * v200

    # cumulativeulative. evaluate in a range
    elif save_name_append == 'cumulative':
        raise NotImplementedError

    return (v_c)


# profiles
def get_analytic_hernquist_dispersion(r_h, vmax, a_h, save_name_append='diff'):
    '''What it says in the function name. r_h is actually bin_edges. If diff,
    it will average (in log space) between bins and evaluate at the centre.
    If cumulative, will get the (Hernquist) density weighted dispersion.
    '''
    gmh = 4 * a_h * vmax ** 2  # kpc km/s

    disp_1d = np.zeros(len(r_h) // 2)

    # differential. evaluate at a point
    if save_name_append == 'diff':
        # to fix if the centre is 0
        r_s = np.copy(r_h)
        if r_s[0] == 0:
            r_s[0] = 0.1

        # bin centres
        r_hs = 10 ** ((np.log10(r_s[0::2]) + np.log10(r_s[1::2])) / 2)

        for i in range(len(disp_1d)):
            # #binney and tremaine eq 4.219
            # x_h = r_hs[i] / a_h
            # y_h = x_h + 1
            # disp_1d[i] = np.sqrt(gmh / a_h * (x_h * y_h**3 * np.log(y_h/x_h) - x_h/y_h * (
            #   1/4 + 1/3*y_h + 1/2*y_h**2 + y_h**3)))

            # Ludlow et al. 2021 eqn 14 etc.
            disp_1d[i] = np.sqrt(gmh / (12 * a_h) * (
                    12 * r_hs[i] * (r_hs[i] + a_h) ** 3 / a_h ** 4 * np.log1p(a_h / r_hs[i]) -
                    r_hs[i] / (r_hs[i] + a_h) * (25 + 52 * r_hs[i] / a_h +
                                                 42 * (r_hs[i] / a_h) ** 2 + 12 * (r_hs[i] / a_h) ** 3)))

    # cumulative. evaluate in a range
    elif save_name_append == 'cumulative':

        g = lambda r: 0 if r == 0 else np.log1p(a_h / r)
        # sympy answer
        f = lambda r: (4 * r ** 3 / a_h ** 4 * g(r) -
                       4 * r ** 2 / a_h ** 3 + 2 * r / a_h ** 2 +
                       (a_h ** 2 + a_h * r + r ** 2) / (
                               a_h ** 3 + 3 * a_h ** 2 * r + 3 * a_h * r ** 2 + r ** 3))

        for i in range(len(disp_1d)):
            c = gmh / 6 / ((r_h[2 * i + 1] / (r_h[2 * i + 1] + a_h)) ** 2 - (r_h[2 * i] / (r_h[2 * i] + a_h)) ** 2)

            disp_1d[i] = c * (f(r_h[2 * i + 1]) - f(r_h[2 * i]))

        disp_1d = np.sqrt(disp_1d)

    return (disp_1d)


def get_analytic_hernquist_circular_velocity(r_h, vmax, a_h, save_name_append='diff'):
    '''What it says in the function name. If diff, it will average (in log space)
    between bins and evaluate at the centre. If cumulative, will get the (Hernquist) density
    weighted velocity.
    '''
    gmh = 4 * a_h * vmax ** 2  # kpc km/s

    v_c = np.zeros(len(r_h) // 2)

    # differential. evaluate at a point
    if save_name_append == 'diff':
        # to fix if the centre is 0
        r_s = np.copy(r_h)
        if r_s[0] == 0:
            r_s[0] = 0.1

        # bin centres
        r_hs = 10 ** ((np.log10(r_s[0::2]) + np.log10(r_s[1::2])) / 2)

        for i in range(len(v_c)):
            # G * mass enclosed
            gmh_r = gmh * (r_hs[i] / (r_hs[i] + a_h)) ** 2

            v_c[i] = np.sqrt(gmh_r / r_hs[i])

    # cumulative. evaluate in a range
    elif save_name_append == 'cumulative':

        f = lambda r: -a_h * (a_h ** 2 + 4 * a_h * r + 6 * r ** 2) / (a_h + r) ** 4

        for i in range(len(v_c)):
            c = gmh / 6 / ((r_h[2 * i + 1] / (r_h[2 * i + 1] + a_h)) ** 2 - (r_h[2 * i] / (r_h[2 * i] + a_h)) ** 2)

            v_c[i] = c * (f(r_h[2 * i + 1]) - f(r_h[2 * i]))

        v_c = np.sqrt(v_c)

    return (v_c)


def get_exponential_disk_velocity(r_h, M_disk, r_disk, save_name_append='diff'):
    '''Calculates the circular velocity for an analytic thin exponential disk
    '''
    v_c = np.zeros(len(r_h)//2)

    # differential. evaluate at a point
    if save_name_append == 'diff':
        # to fix if the centre is 0
        r_s = np.copy(r_h)
        if r_s[0] == 0:
            r_s[0] = 0.1

        # bin centres
        r_hs = 10 ** ((np.log10(r_s[0::2]) + np.log10(r_s[1::2])) / 2)

        y = r_hs / (2 * r_disk)

        v_c2 = GRAV_CONST * M_disk * r_hs ** 2 / (2 * r_disk ** 3) * (iv(0, y) * kn(0, y) - iv(1, y) * kn(1, y))

    # cumulative. evaluate in a range
    elif save_name_append == 'cumulative':

        integrand = lambda y: y * y * y * np.exp(-2 * y) * (iv(0, y) * kn(0, y) - iv(1, y) * kn(1, y))

        v_c2 = np.array([quad(integrand, r_h[2 * i] / (2 * r_disk), r_h[2 * i + 1] / (2 * r_disk))[0]
                         for i in range(len(r_h) // 2)])

        v_c2 *= 8 * GRAV_CONST * M_disk

        v_c2 /= np.array([np.exp(-r_h[2 * i] / r_disk) * (r_disk + r_h[2 * i]) -
                          np.exp(-r_h[2 * i + 1] / r_disk) * (r_disk + r_h[2 * i + 1]) for i in range(len(r_h) // 2)])

    return (np.sqrt(v_c2))


def get_velocity_scale(bin_edges, v200=False, analytic_dispersion=False, analytic_v_c=False,
                       nfw=True, hernquist=False,
                       save_name_append='diff', galaxy=Galaxy()):
    '''Get a given velocity. The data is not needed for the analytic velocities.
    '''
    if v200:
        out = galaxy.get_v200()

    elif analytic_dispersion:
        if nfw:
            (r_s, conc, v200) = galaxy.get_nfw_params()
            out = get_analytic_nfw_dispersion(bin_edges, r_s, conc, v200, save_name_append)

        elif hernquist:
            (a_h, vmax) = galaxy.get_hernquist_params()
            out = get_analytic_hernquist_dispersion(bin_edges, vmax, a_h, save_name_append)

    elif analytic_v_c:
        if nfw:
            (r_s, conc, v200) = galaxy.get_nfw_params()
            v_halo = get_analytic_nfw_circular_velocity(bin_edges, r_s, conc, v200, save_name_append)

        elif hernquist:
            (a_h, vmax) = galaxy.get_hernquist_params()
            v_halo = get_analytic_hernquist_circular_velocity(bin_edges, vmax, a_h, save_name_append)

        (M_disk, r_disk) = galaxy.get_exponential_disk_params()
        v_disk = get_exponential_disk_velocity(bin_edges, M_disk, r_disk, save_name_append)

        # r_h = 10**((np.log10(bin_edges[0::2]) + np.log10(bin_edges[1::2]))/2)
        # v_disk = np.sqrt(GRAV_CONST * M_disk * (1 - np.exp(-r_h / r_disk) * (1 + r_h / r_disk)) / r_h)

        out = np.sqrt(v_halo ** 2 + v_disk ** 2)

    return (out)


def get_analytic_nfw_density(r_h, r_s, c, v200, f_bary, save_name_append='diff'):
    '''
    '''
    rho = np.zeros(len(r_h) // 2)

    M200 = v200 ** 3 / (10 * GRAV_CONST * GALIC_HUBBLE_CONST)
    # M_h = M200
    M_h = (1-f_bary) * M200

    if save_name_append == 'diff':
        # to fix if the centre is 0
        r_ = np.copy(r_h)
        if r_[0] == 0:
            r_[0] = 0.1

        # bin centres
        r_hs = 10 ** ((np.log10(r_[0::2]) + np.log10(r_[1::2])) / 2)

        g = 1 / (np.log1p(c) - c / (1 + c))

        # binney and tremaine eq 2.64 etc.
        rho = g * M200 / (4 * np.pi * r_s**3) / ((r_hs / r_s) * (1 + r_hs / r_s) ** 2)

    elif save_name_append == 'cumulative':
        raise NotImplementedError

    return (rho)


def get_analytic_hernquist_density(r_h, vmax, a_h, save_name_append='diff'):
    '''
    '''
    rho = np.zeros(len(r_h) // 2)

    m_h = 4 * a_h * vmax ** 2 / GRAV_CONST

    if save_name_append == 'diff':
        # to fix if the centre is 0
        r_s = np.copy(r_h)
        if r_s[0] == 0:
            r_s[0] = 0.1

        # bin centres
        r_hs = 10 ** ((np.log10(r_s[0::2]) + np.log10(r_s[1::2])) / 2)

        for i in range(len(r_hs)):
            # binney and tremaine eq 2.64 etc.
            # rho[i] = m_h / (2 * np.pi) * a_h / ((r_hs[i]) * (r_hs[i] + a_h) ** 3)
            rho[i] = m_h / (2 * np.pi * a_h**3) / ((r_hs[i] / a_h) * (1 + r_hs[i] / a_h) ** 3)

    elif save_name_append == 'cumulative':

        for i in range(len(rho)):
            rho[i] = (m_h * ((r_h[2 * i + 1] / (r_h[2 * i + 1] + a_h)) ** 2 - (r_h[2 * i] / (r_h[2 * i] + a_h)) ** 2)
                      / (4 / 3 * np.pi * (r_h[2 * i + 1] ** 3 - r_h[2 * i] ** 3)))

    return (rho)


def get_exponential_surface_density(R, M_star, R_d, save_name_append='diff'):
    '''
    '''
    if save_name_append == 'diff':
        sigma_0 = M_star / (2 * np.pi * R_d ** 2)
        sigma_R = sigma_0 * np.exp(-R / R_d)
    else:
        raise NotImplementedError

    return (sigma_R)


def get_density_scale(bin_edges, save_name_append='diff',
                      nfw=True, hernquist=False,
                      galaxy=Galaxy()):
    '''Calculate the chosen velocity scale.
    '''
    if nfw:
        (r_s, conc, v200) = galaxy.get_nfw_params()
        f_bary = galaxy.f_bary
        out = get_analytic_nfw_density(bin_edges, r_s, conc, v200, f_bary, save_name_append)

    elif hernquist:
        (a_h, vmax) = galaxy.get_hernquist_params()
        out = get_analytic_hernquist_density(bin_edges, vmax, a_h, save_name_append)

    return (out)


# disk heights see e.g. Benitez-Llambey et al. 2018, Ludlow et al 2021
def get_thin_disk_SG_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a):
    # Ludlow et al. 2021 Equation (11)
    h_SG = sigma_z ** 2 / (np.pi * GRAV_CONST * Sigma_R_star)

    # #Benitez-Llambey et al. 2018
    # z_SG = 0.55 * h_SG
    # me
    z_SG = np.log(3) / 2 * h_SG

    return (z_SG)


def get_thin_disk_NSG_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a):
    # Ludlow et al. 2021 Equation (10)
    h_NSG = np.sqrt(2) * sigma_z * R / v_c_halo

    # #Benitez-Llambey et al. 2018
    # z_NSG = 0.477 * h_NSG
    # me
    z_NSG = erfinv(0.5) * h_NSG

    return (z_NSG)


def get_thin_disk_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a):
    z_SG = get_thin_disk_SG_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a)

    z_NSG = get_thin_disk_NSG_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a)

    # Ludlow et al. 2021 Equation (18)
    z_thin_half = (1 / z_SG ** 2 + 1 / (2 * z_SG * z_NSG) + 1 / z_NSG ** 2) ** (-1 / 2)

    return (z_thin_half)


def get_thick_disk_NSG_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a):
    # Ludlow et al. 2021 Equation (20)
    nu_2inv = v200 * v200 / (sigma_z * sigma_z)
    rho = lambda z, nu_2inv, R, r200: np.exp(nu_2inv * (1 / (np.sqrt((R / r200) ** 2 + (z / r200) ** 2) + a / r200)
                                                        - 1 / (R / r200 + a / r200)))

    tot = quad(rho, 0, 200, args=(nu_2inv, R, r200))[0]

    half = lambda z_half, nu_2inv, R, r200: quad(rho, 0, z_half, args=(nu_2inv, R, r200))[0] - tot / 2

    out = brentq(half, 0, 200, args=(nu_2inv, R, r200))

    return (out)


def get_disk_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a):
    z_thin_half = get_thin_disk_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a)

    z_thick_NSG = get_thick_disk_NSG_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a)

    z_NSG = get_thin_disk_NSG_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a)

    # Ludlow et al. 2021 Equation (21)
    z_thick_half = z_thin_half + (z_thick_NSG - z_NSG)

    return (z_thick_half)


########################################################################################################################
# Heating equaitons from Ludlow et al. 2021 and Wilkinson et al. 2022


def get_constants(bin_edges, save_name_append, MassDM, galaxy=Galaxy()):
    '''Get all the scaled quantities.
    '''
    rho_dm = get_density_scale(bin_edges,
                               save_name_append=save_name_append, galaxy=galaxy)

    sigma_dm = get_velocity_scale(bin_edges, analytic_dispersion=True,
                                  save_name_append=save_name_append, galaxy=galaxy)

    v_circ = get_velocity_scale(bin_edges, analytic_v_c=True,
                                save_name_append=save_name_append, galaxy=galaxy)

    V200 = galaxy.get_v200()
    rho_200 = galaxy.get_rho_200()

    # t_c_dm = sigma_dm ** 3 / (GRAV_CONST ** 2 * rho_dm * MassDM) * GYR_ON_S / PC_ON_M  # 1/Gyr
    # t_c_circ = v_circ**3   / (GRAV_CONST**2 * rho_dm * MassDM) * GYR_ON_S / PC_ON_M # 1/Gyr
    t_c = V200 ** 3 / (GRAV_CONST ** 2 * rho_dm * MassDM) * GYR_ON_S / PC_ON_M  # 1/Gyr

    delta_dm = rho_dm / rho_200
    upsilon_dm = sigma_dm / V200
    upsilon_circ = v_circ / V200

    # return(t_c_dm , t_c_circ, delta_dm, upsilon_dm, upsilon_circ)
    return (t_c, 0, delta_dm, upsilon_dm, upsilon_circ)


def get_tau_array(time_array, time_scale_array):
    # e.g. Wilkinson et al. eqn. 10
    tau_array = time_array / time_scale_array

    return (tau_array)


def get_tau_heat_array(scale_density_ratio_array, scale_velocity_ratio_array,
                       ln_Lambda_k_i, alpha_i, beta_i, gamma_i):

    # e.g. Wilkinson et al. eqn. 9, 13
    tau_heat_array = np.power((ln_Lambda_k_i * np.power(scale_density_ratio_array, alpha_i) *
                               np.power(scale_velocity_ratio_array, -beta_i) / scale_velocity_ratio_array**2), -1.0 / (1 + gamma_i))

    return (tau_heat_array)


def get_tau_zero_array(velocity_scale2_array, inital_velocity2_array, scale_velocity_ratio_array):

    # e.g. Wilkinson et al. eqn. 11, 14
    sigma_dm_2 = velocity_scale2_array * scale_velocity_ratio_array**2
    tau_0_on_tau_heat = np.log(
        sigma_dm_2 / (sigma_dm_2 - inital_velocity2_array))

    return (tau_0_on_tau_heat)


def get_theory_velocity2_array(velocity_scale2_array, inital_velocity2_array,
                               time_array, time_scale_array,
                               scale_density_ratio_array, scale_velocity_ratio_array,
                               ln_Lambda_k_i, alpha_i, beta_i, gamma_i):

    tau_heat_array = get_tau_heat_array(scale_density_ratio_array, scale_velocity_ratio_array,
                                        ln_Lambda_k_i, alpha_i, beta_i, gamma_i)

    tau_0_on_tau_heat = get_tau_zero_array(velocity_scale2_array, inital_velocity2_array, scale_velocity_ratio_array)

    # e.g. Wilkinson et al. eqn. 8, 12
    theory_velocity2_array = velocity_scale2_array * scale_velocity_ratio_array**2 * (1 - np.exp(
        - np.power(((time_array / time_scale_array) / tau_heat_array) +
                   tau_0_on_tau_heat, 1 + gamma_i)))

    return (theory_velocity2_array)


def get_powerlaw_velocity2_array(velocity_scale2_array, inital_velocity2_array,
                                 time_array, time_scale_array,
                                 scale_density_ratio_array, scale_velocity_ratio_array,
                                 ln_Lambda_k_i, alpha_i, beta_i, gamma_i):
    tau_heat_array = get_tau_heat_array(scale_density_ratio_array, scale_velocity_ratio_array,
                                        ln_Lambda_k_i, alpha_i, beta_i, gamma_i) / scale_velocity_ratio_array**2

    # e.g. Wilkinson et al. eqn. 7
    theory_velocity2_array = (velocity_scale2_array - inital_velocity2_array) * (
        np.power(((time_array / time_scale_array) / tau_heat_array),
                 1 + gamma_i)) + inital_velocity2_array

    return (theory_velocity2_array)


########################################################################################################################


def calc_indicators(N200s, bin_edges, time, ic_heat_fraction, concs, bary_f=[0]):

    N_property = np.amax((len(bin_edges)//2, len(time), len(ic_heat_fraction), len(concs), len(bary_f)))
    arg_property = np.argmax((len(bin_edges)//2, len(time), len(ic_heat_fraction), len(concs), len(bary_f)))
    this_property = [bin_edges, time, ic_heat_fraction, concs, bary_f][arg_property]

    v_on_sigma = np.zeros((N_property, len(N200s)))
    kappa_rot = np.zeros((N_property, len(N200s)))
    spheroid_to_total = np.zeros((N_property, len(N200s)))

    theory_best_v = np.zeros((N_property, len(N200s)))
    theory_best_z2 = np.zeros((N_property, len(N200s)))
    theory_best_r2 = np.zeros((N_property, len(N200s)))
    theory_best_p2 = np.zeros((N_property, len(N200s)))

    experiment_v = np.zeros((N_property, len(N200s)))

    # tau_heat_v = np.zeros((N_property, len(N200s)))
    # tau_heat_z2 = np.zeros((N_property, len(N200s)))
    # tau_heat_r2 = np.zeros((N_property, len(N200s)))
    # tau_heat_p2 = np.zeros((N_property, len(N200s)))

    tau_heat_experiment = np.zeros((N_property, len(N200s)))

    # get theory
    (ln_Lambda_ks, alphas, betas, gammas
     ) = find_least_log_squares_best_fit()

    for i in range(N_property):
        galaxy = Galaxy(conc = concs[i % len(concs)], f_bary=bary_f[i % len(bary_f)])

        v200 = galaxy.get_v200()

        b_e = bin_edges[((2 * i) % len(bin_edges)):((2 * i + 1) % (len(bin_edges)) + 1)]

        analytic_dispersion = get_velocity_scale(b_e, analytic_dispersion=True,
                                                 save_name_append='diff', galaxy=galaxy)
        analytic_v_circ = get_velocity_scale(b_e, analytic_v_c=True,
                                             save_name_append='diff', galaxy=galaxy)

        inital_velocity_v = np.sqrt((1 - ic_heat_fraction[i % len(ic_heat_fraction)] ** 2) * analytic_v_circ ** 2)
        inital_velocity2_z = ic_heat_fraction[i % len(ic_heat_fraction)] ** 2 * (analytic_dispersion) ** 2
        inital_velocity2_r = ic_heat_fraction[i % len(ic_heat_fraction)] ** 2 * (analytic_dispersion) ** 2
        inital_velocity2_p = ic_heat_fraction[i % len(ic_heat_fraction)] ** 2 * (analytic_dispersion) ** 2

        for k, N200 in enumerate(N200s):
            m_dm = M200_for_v200 / N200

            # work out constatns
            (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
             ) = get_constants(b_e, 'diff', m_dm, galaxy=galaxy)

            theory_best_v_ = get_theory_velocity2_array(v200, analytic_v_circ[0] - inital_velocity_v[0],
                                                        time[i % len(time)], t_c_dm[0], delta_dm[0], np.sqrt(upsilon_circ[0]),
                                                        ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
            theory_best_v[i, k] = analytic_v_circ - theory_best_v_

            theory_best_z2[i, k] = get_theory_velocity2_array(v200 ** 2, inital_velocity2_z[0],
                                                              time[i % len(time)], t_c_dm[0], delta_dm[0], upsilon_dm[0],
                                                              ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
            theory_best_r2[i, k] = get_theory_velocity2_array(v200 ** 2, inital_velocity2_r[0],
                                                              time[i % len(time)], t_c_dm[0], delta_dm[0], upsilon_dm[0],
                                                              ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
            theory_best_p2[i, k] = get_theory_velocity2_array(v200 ** 2, inital_velocity2_p[0],
                                                              time[i % len(time)], t_c_dm[0], delta_dm[0], upsilon_dm[0],
                                                              ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

            #
            tau_heat_z2 = get_tau_heat_array(delta_dm[0], upsilon_dm[0],
                                                ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
            tau_heat_r2 = get_tau_heat_array(delta_dm[0], upsilon_dm[0],
                                                ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
            tau_heat_p2 = get_tau_heat_array(delta_dm[0], upsilon_dm[0],
                                                ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

            # tau_heat_experiment = ((tau_heat_z2[i] + tau_heat_r2[i] + tau_heat_p2[i]
            #                     ) * np.sqrt(upsilon_dm[i]) / np.sqrt(upsilon_circ[i]) / 3) #
            # tau_heat_experiment = (tau_heat_r2 * 1.06126839)
            tau_heat_experiment = (tau_heat_p2 * 0.74987599)

            tau_0_on_tau_heat = get_tau_zero_array(v200, analytic_v_circ[0] - inital_velocity_v[0],
                                                   np.sqrt(upsilon_circ[0]))

            experiment_v_ = v200 * np.sqrt(upsilon_circ[0]) ** 2 * (1 - np.exp(
                - np.power(((time[i % len(time)] / t_c_dm[0]) / tau_heat_experiment) +  tau_0_on_tau_heat, 1 + gammas[0])))

            theory_best_v[i, k] = analytic_v_circ - experiment_v_

    # model indicators
    theory_best_tot2 = theory_best_z2 + theory_best_r2 + theory_best_p2
    v_on_sigma = theory_best_v / np.sqrt(theory_best_tot2 / 3)

    #theory_best_p2 can be < theory_best_tot2 / 3 when theory_best_v approx 0, so kappa_rot can be < 1/3
    kappa_rot = (theory_best_v ** 2 + theory_best_p2) / (
                 theory_best_v ** 2 + theory_best_tot2)

    # spheroid_to_total = 2 * scipy.stats.norm.cdf(0, loc=theory_best_v, scale=np.sqrt(theory_best_p2))
    spheroid_to_total = 1 + erf(-theory_best_v / (np.sqrt(theory_best_p2* 2)))

    return (v_on_sigma, kappa_rot, spheroid_to_total)


def calc_vels(N200s, bin_edges, time, ic_heat_fraction, concs, bary_f=[0]):

    N_property = np.amax((len(bin_edges)//2, len(time), len(ic_heat_fraction), len(concs), len(bary_f)))
    arg_property = np.argmax((len(bin_edges)//2, len(time), len(ic_heat_fraction), len(concs), len(bary_f)))
    this_property = [bin_edges, time, ic_heat_fraction, concs, bary_f][arg_property]

    v_on_sigma = np.zeros((N_property, len(N200s)))
    kappa_rot = np.zeros((N_property, len(N200s)))
    spheroid_to_total = np.zeros((N_property, len(N200s)))

    theory_best_v = np.zeros((N_property, len(N200s)))
    theory_best_z2 = np.zeros((N_property, len(N200s)))
    theory_best_r2 = np.zeros((N_property, len(N200s)))
    theory_best_p2 = np.zeros((N_property, len(N200s)))

    experiment_v = np.zeros((N_property, len(N200s)))

    # tau_heat_v = np.zeros((N_property, len(N200s)))
    # tau_heat_z2 = np.zeros((N_property, len(N200s)))
    # tau_heat_r2 = np.zeros((N_property, len(N200s)))
    # tau_heat_p2 = np.zeros((N_property, len(N200s)))

    tau_heat_experiment = np.zeros((N_property, len(N200s)))

    # get theory
    (ln_Lambda_ks, alphas, betas, gammas
     ) = find_least_log_squares_best_fit()

    for i in range(N_property):
        galaxy = Galaxy(conc = concs[i % len(concs)], f_bary=bary_f[i % len(bary_f)])

        v200 = galaxy.get_v200()

        b_e = bin_edges[((2 * i) % len(bin_edges)):((2 * i + 1) % (len(bin_edges)) + 1)]

        analytic_dispersion = get_velocity_scale(b_e, analytic_dispersion=True,
                                                 save_name_append='diff', galaxy=galaxy)
        analytic_v_circ = get_velocity_scale(b_e, analytic_v_c=True,
                                             save_name_append='diff', galaxy=galaxy)

        inital_velocity_v = np.sqrt((1 - ic_heat_fraction[i % len(ic_heat_fraction)] ** 2) * analytic_v_circ ** 2)
        inital_velocity2_z = ic_heat_fraction[i % len(ic_heat_fraction)] ** 2 * (analytic_dispersion) ** 2
        inital_velocity2_r = ic_heat_fraction[i % len(ic_heat_fraction)] ** 2 * (analytic_dispersion) ** 2
        inital_velocity2_p = ic_heat_fraction[i % len(ic_heat_fraction)] ** 2 * (analytic_dispersion) ** 2

        for k, N200 in enumerate(N200s):
            m_dm = M200_for_v200 / N200

            # work out constatns
            (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
             ) = get_constants(b_e, 'diff', m_dm, galaxy=galaxy)

            theory_best_v_ = get_theory_velocity2_array(v200, analytic_v_circ[0] - inital_velocity_v[0],
                                                        time[i % len(time)], t_c_dm[0], delta_dm[0], np.sqrt(upsilon_circ[0]),
                                                        ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
            theory_best_v[i, k] = analytic_v_circ - theory_best_v_

            theory_best_z2[i, k] = get_theory_velocity2_array(v200 ** 2, inital_velocity2_z[0],
                                                              time[i % len(time)], t_c_dm[0], delta_dm[0], upsilon_dm[0],
                                                              ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
            theory_best_r2[i, k] = get_theory_velocity2_array(v200 ** 2, inital_velocity2_r[0],
                                                              time[i % len(time)], t_c_dm[0], delta_dm[0], upsilon_dm[0],
                                                              ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
            theory_best_p2[i, k] = get_theory_velocity2_array(v200 ** 2, inital_velocity2_p[0],
                                                              time[i % len(time)], t_c_dm[0], delta_dm[0], upsilon_dm[0],
                                                              ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

            #
            tau_heat_p2 = get_tau_heat_array(delta_dm[0], upsilon_dm[0],
                                                ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

            # tau_heat_experiment = ((tau_heat_z2[i] + tau_heat_r2[i] + tau_heat_p2[i]
            #                     ) * np.sqrt(upsilon_dm[i]) / np.sqrt(upsilon_circ[i]) / 3) #
            # tau_heat_experiment = (tau_heat_r2 * 1.06126839)
            tau_heat_experiment = (tau_heat_p2 * 0.74987599)

            tau_0_on_tau_heat = get_tau_zero_array(v200, analytic_v_circ[0] - inital_velocity_v[0],
                                                   np.sqrt(upsilon_circ[0]))

            experiment_v_ = v200 * np.sqrt(upsilon_circ[0]) ** 2 * (1 - np.exp(
                - np.power(((time[i % len(time)] / t_c_dm[0]) / tau_heat_experiment) +  tau_0_on_tau_heat, 1 + gammas[0])))

            # theory_best_v[i, k] = analytic_v_circ - experiment_v_
            theory_best_v[i, k] = experiment_v_

    return theory_best_v / v200, theory_best_z2 / v200**2, theory_best_r2 / v200**2, theory_best_p2 / v200**2


def calc_raw_vels(N200s, bin_edges, time, ic_heat_fraction, concs, bary_f=[0]):

    N_property = np.amax((len(bin_edges)//2, len(time), len(ic_heat_fraction), len(concs), len(bary_f)))
    arg_property = np.argmax((len(bin_edges)//2, len(time), len(ic_heat_fraction), len(concs), len(bary_f)))
    this_property = [bin_edges, time, ic_heat_fraction, concs, bary_f][arg_property]

    theory_best_v = np.zeros((N_property, len(N200s)))
    theory_best_z2 = np.zeros((N_property, len(N200s)))
    theory_best_r2 = np.zeros((N_property, len(N200s)))
    theory_best_p2 = np.zeros((N_property, len(N200s)))

    experiment_v = np.zeros((N_property, len(N200s)))

    # tau_heat_v = np.zeros((N_property, len(N200s)))
    # tau_heat_z2 = np.zeros((N_property, len(N200s)))
    # tau_heat_r2 = np.zeros((N_property, len(N200s)))
    # tau_heat_p2 = np.zeros((N_property, len(N200s)))

    tau_heat_experiment = np.zeros((N_property, len(N200s)))

    # get theory
    (ln_Lambda_ks, alphas, betas, gammas
     ) = find_least_log_squares_best_fit()

    for i in range(N_property):
        galaxy = Galaxy(conc = concs[i % len(concs)], f_bary=bary_f[i % len(bary_f)])

        v200 = galaxy.get_v200()

        b_e = bin_edges[((2 * i) % len(bin_edges)):((2 * i + 1) % (len(bin_edges)) + 1)]

        analytic_dispersion = get_velocity_scale(b_e, analytic_dispersion=True,
                                                 save_name_append='diff', galaxy=galaxy)
        analytic_v_circ = get_velocity_scale(b_e, analytic_v_c=True,
                                             save_name_append='diff', galaxy=galaxy)

        inital_velocity_v = np.sqrt((1 - ic_heat_fraction[i % len(ic_heat_fraction)] ** 2) * analytic_v_circ ** 2)
        inital_velocity2_z = ic_heat_fraction[i % len(ic_heat_fraction)] ** 2 * (analytic_dispersion) ** 2
        inital_velocity2_r = ic_heat_fraction[i % len(ic_heat_fraction)] ** 2 * (analytic_dispersion) ** 2
        inital_velocity2_p = ic_heat_fraction[i % len(ic_heat_fraction)] ** 2 * (analytic_dispersion) ** 2

        for k, N200 in enumerate(N200s):
            m_dm = M200_for_v200 / N200

            # work out constatns
            (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
             ) = get_constants(b_e, 'diff', m_dm, galaxy=galaxy)

            theory_best_v_ = get_theory_velocity2_array(v200, analytic_v_circ[0] - inital_velocity_v[0],
                                                        time[i % len(time)], t_c_dm[0], delta_dm[0], np.sqrt(upsilon_circ[0]),
                                                        ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
            theory_best_v[i, k] = analytic_v_circ - theory_best_v_

            theory_best_z2[i, k] = get_theory_velocity2_array(v200 ** 2, inital_velocity2_z[0],
                                                              time[i % len(time)], t_c_dm[0], delta_dm[0], upsilon_dm[0],
                                                              ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
            theory_best_r2[i, k] = get_theory_velocity2_array(v200 ** 2, inital_velocity2_r[0],
                                                              time[i % len(time)], t_c_dm[0], delta_dm[0], upsilon_dm[0],
                                                              ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
            theory_best_p2[i, k] = get_theory_velocity2_array(v200 ** 2, inital_velocity2_p[0],
                                                              time[i % len(time)], t_c_dm[0], delta_dm[0], upsilon_dm[0],
                                                              ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

            #
            tau_heat_p2 = get_tau_heat_array(delta_dm[0], upsilon_dm[0],
                                                ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

            # tau_heat_experiment = ((tau_heat_z2[i] + tau_heat_r2[i] + tau_heat_p2[i]
            #                     ) * np.sqrt(upsilon_dm[i]) / np.sqrt(upsilon_circ[i]) / 3) #
            # tau_heat_experiment = (tau_heat_r2 * 1.06126839)
            tau_heat_experiment = (tau_heat_p2 * 0.74987599)

            tau_0_on_tau_heat = get_tau_zero_array(v200, analytic_v_circ[0] - inital_velocity_v[0],
                                                   np.sqrt(upsilon_circ[0]))

            experiment_v_ = v200 * np.sqrt(upsilon_circ[0]) ** 2 * (1 - np.exp(
                - np.power(((time[i % len(time)] / t_c_dm[0]) / tau_heat_experiment) +  tau_0_on_tau_heat, 1 + gammas[0])))

            theory_best_v[i, k] = analytic_v_circ - experiment_v_
            # theory_best_v[i, k] = experiment_v_

    return theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2

def calc_shape_inds(N200s, bin_edges, time, ic_heat_fraction, concs, bary_f=[0], frac=0.7):

    N_property = np.amax((len(bin_edges)//2, len(time), len(ic_heat_fraction), len(concs), len(bary_f)))
    arg_property = np.argmax((len(bin_edges)//2, len(time), len(ic_heat_fraction), len(concs), len(bary_f)))
    this_property = [bin_edges, time, ic_heat_fraction, concs, bary_f][arg_property]

    theory_best_v = np.zeros((N_property, len(N200s)))
    theory_best_z2 = np.zeros((N_property, len(N200s)))
    theory_best_r2 = np.zeros((N_property, len(N200s)))
    theory_best_p2 = np.zeros((N_property, len(N200s)))

    experiment_v = np.zeros((N_property, len(N200s)))

    theory_best_dt = np.zeros((N_property, len(N200s)))
    theory_best_st = np.zeros((N_property, len(N200s)))
    theory_best_ca = np.zeros((N_property, len(N200s)))

    # tau_heat_v = np.zeros((N_property, len(N200s)))
    # tau_heat_z2 = np.zeros((N_property, len(N200s)))
    # tau_heat_r2 = np.zeros((N_property, len(N200s)))
    # tau_heat_p2 = np.zeros((N_property, len(N200s)))

    tau_heat_experiment = np.zeros((N_property, len(N200s)))

    # get theory
    (ln_Lambda_ks, alphas, betas, gammas
     ) = find_least_log_squares_best_fit()

    for i in range(N_property):
        galaxy = Galaxy(conc = concs[i % len(concs)], f_bary=bary_f[i % len(bary_f)])

        v200 = galaxy.get_v200()

        b_e = bin_edges[((2 * i) % len(bin_edges)):((2 * i + 1) % (len(bin_edges)) + 1)]

        analytic_dispersion = get_velocity_scale(b_e, analytic_dispersion=True,
                                                 save_name_append='diff', galaxy=galaxy)
        analytic_v_circ = get_velocity_scale(b_e, analytic_v_c=True,
                                             save_name_append='diff', galaxy=galaxy)

        inital_velocity_v = np.sqrt((1 - ic_heat_fraction[i % len(ic_heat_fraction)] ** 2) * analytic_v_circ ** 2)
        inital_velocity2_z = ic_heat_fraction[i % len(ic_heat_fraction)] ** 2 * (analytic_dispersion) ** 2
        inital_velocity2_r = ic_heat_fraction[i % len(ic_heat_fraction)] ** 2 * (analytic_dispersion) ** 2
        inital_velocity2_p = ic_heat_fraction[i % len(ic_heat_fraction)] ** 2 * (analytic_dispersion) ** 2

        for k, N200 in enumerate(N200s):
            m_dm = M200_for_v200 / N200

            # work out constatns
            (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
             ) = get_constants(b_e, 'diff', m_dm, galaxy=galaxy)

            theory_best_v_ = get_theory_velocity2_array(v200, analytic_v_circ[0] - inital_velocity_v[0],
                                                        time[i % len(time)], t_c_dm[0], delta_dm[0], np.sqrt(upsilon_circ[0]),
                                                        ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
            theory_best_v[i, k] = analytic_v_circ - theory_best_v_

            theory_best_z2[i, k] = get_theory_velocity2_array(v200 ** 2, inital_velocity2_z[0],
                                                              time[i % len(time)], t_c_dm[0], delta_dm[0], upsilon_dm[0],
                                                              ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
            theory_best_r2[i, k] = get_theory_velocity2_array(v200 ** 2, inital_velocity2_r[0],
                                                              time[i % len(time)], t_c_dm[0], delta_dm[0], upsilon_dm[0],
                                                              ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
            theory_best_p2[i, k] = get_theory_velocity2_array(v200 ** 2, inital_velocity2_p[0],
                                                              time[i % len(time)], t_c_dm[0], delta_dm[0], upsilon_dm[0],
                                                              ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

            #
            tau_heat_p2 = get_tau_heat_array(delta_dm[0], upsilon_dm[0],
                                                ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

            # tau_heat_experiment = ((tau_heat_z2[i] + tau_heat_r2[i] + tau_heat_p2[i]
            #                     ) * np.sqrt(upsilon_dm[i]) / np.sqrt(upsilon_circ[i]) / 3) #
            # tau_heat_experiment = (tau_heat_r2 * 1.06126839)
            tau_heat_experiment = (tau_heat_p2 * 0.74987599)

            tau_0_on_tau_heat = get_tau_zero_array(v200, analytic_v_circ[0] - inital_velocity_v[0],
                                                   np.sqrt(upsilon_circ[0]))

            experiment_v_ = v200 * np.sqrt(upsilon_circ[0]) ** 2 * (1 - np.exp(
                - np.power(((time[i % len(time)] / t_c_dm[0]) / tau_heat_experiment) +  tau_0_on_tau_heat, 1 + gammas[0])))

            theory_best_v[i, k] = analytic_v_circ - experiment_v_
            # theory_best_v[i, k] = experiment_v_

            theory_best_dt[i, k] = 1 / 2 + erf(
                (theory_best_v[i, k] - frac * analytic_v_circ) / (np.sqrt(theory_best_p2[i, k] * 2))) / 2
            theory_best_st[i, k] = 1 + erf(-theory_best_v[i, k] / (np.sqrt(theory_best_p2[i, k] * 2)))

            # c/a model
            # radius to investigate is half mass radius
            # R = get_bin_edges(save_name_append='cum')[2 * _index + 1]
            # bin_edges = get_bin_edges(save_name_append='diff')

            # halo stuff
            a_h, vmax = galaxy.a_h, galaxy.vmax
            r200 = galaxy.r200

            # parameters with units for heights
            R_disk = galaxy.Rd
            #TODO is there a better solution?
            M_disk = 0.01 * galaxy.M200

            R = np.mean(b_e)

            v_c_halo = analytic_v_circ[0]

            Sigma_R_star = get_exponential_surface_density(R, M_disk, R_disk, save_name_append='diff')

            z_thin_SG = get_thin_disk_SG_height(np.sqrt(theory_best_z2[i, k]), R, Sigma_R_star, v_c_halo, v200, r200, a_h)
            z_thin_NSG = get_thin_disk_NSG_height(np.sqrt(theory_best_z2[i, k]), R, Sigma_R_star, v_c_halo, v200, r200, a_h)
            # z_thin_half = get_thin_disk_height(np.sqrt(theory_best_z2[i, k]), R, Sigma_R_star, v_c_halo, v200, r200, a_h)

            z_thick_NSG = get_thick_disk_NSG_height(np.sqrt(theory_best_z2[i, k]), R, Sigma_R_star, v_c_halo, v200, r200, a_h)
            # z_thick_half = get_disk_height(np.sqrt(theory_best_z2[i, k]), R, Sigma_R_star, v_c_halo, v200, r200, a_h)

            # Round 2
            c_SG = 0.6159978007462151
            c_NSG = 0.5180308801010115

            c_on_a_thin_SG = z_thin_SG / (R / np.sqrt(2)) * 2 * c_SG / np.log(3)
            c_on_a_thin_NSG = z_thin_NSG / (R / np.sqrt(2)) * c_NSG / erfinv(0.5)

            # c_on_a_thick_NSG = np.zeros(len(theory_best_z2[i, k]))
            # for i in range(len(theory_best_z2)):
            c_on_a_thick_NSG = 0

            thick = lambda z: np.exp(1 / (theory_best_z2[i, k] / v200 ** 2) * (
                    1 / (np.sqrt((R / r200) ** 2 + (z / r200) ** 2) + a_h / r200) - 1 / (R / r200 + a_h / r200)))
            #TODO need to work out what this is for NFW
            # thick = lambda z: np.exp(1 / (theory_best_z2[i, k] / v200 ** 2) * (
            #         1 / (np.sqrt((R / r200) ** 2 + (z / r200) ** 2) + a_h / r200) - 1 / (R / r200 + a_h / r200)))

            z_distribution = thick
            c_2 = z_thick_NSG ** 2
            for _ in range(10):  # 2000):
                top_i = lambda z: 1 / (1 + z ** 2 / (2 * c_2)) * z ** 2 * z_distribution(z)
                bot_i = lambda z: 1 / (1 + z ** 2 / (2 * c_2)) * z_distribution(z)

                top = quad(top_i, 0, 1 * r200)[0]
                bot = quad(bot_i, 0, 1 * r200)[0]

                # c_2 = top / bot
                c_2 = np.divide(top, bot)

                # print(c_2)
                c_on_a_thick_NSG = (np.sqrt(c_2) / (R / np.sqrt(2)))

            c_on_a_thin_half = np.sqrt(
                1 / (1 / c_on_a_thin_SG ** 2 + 1 / (2 * c_on_a_thin_SG * c_on_a_thin_NSG) + 1 / c_on_a_thin_NSG ** 2))

            c_on_a_thick_half = c_on_a_thin_half + (c_on_a_thick_NSG - c_on_a_thin_NSG)

            # theory_best_ca[i, k] = c_on_a_thin_NSG
            # theory_best_ca[i, k] = c_on_a_thin_SG
            # theory_best_ca[i, k] = c_on_a_thin_half
            # theory_best_ca[i, k] = c_on_a_thick_NSG
            theory_best_ca[i, k] = np.amin([c_on_a_thick_half, 1])

    return theory_best_dt, theory_best_st, theory_best_ca


def plot_indicator_n_dependence(save=False, name=''):

    max_T = 13.8 #Gyr

    N200_max = 7.5
    N200_min = 2.5
    N200s = np.logspace(N200_min, N200_max, 101)
    log_N200s = np.log10(N200s)

    m_DM_EAGLE = 1e6 #9.70e6 #M_sun
    M200_EAGLE = m_DM_EAGLE * N200s
    log_M200_EAGLE = np.log10(M200_EAGLE)
    M200_max = np.log10(np.amax(M200_EAGLE))
    M200_min = np.log10(np.amin(M200_EAGLE))

    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(20, 11, forward=True)
    widths = [1, 1, 1]
    heights = [1, 1, 1]
    spec = fig.add_gridspec(ncols=3, nrows=3, width_ratios=widths, height_ratios=heights)

    ax = []
    for col in range(len(widths)):
        ax.append([])
        for row in range(len(heights)):
            ax[col].append(fig.add_subplot(spec[row, col]))

            if row == 0: ax[col][row].semilogy()

            ax[col][row].set_xlim([N200_min, N200_max])

            if row == 1: ax[col][row].set_yticks([2/6, 3/6, 4/6, 5/6, 6/6])
            if row == 1 and col == 0: ax[col][row].set_yticklabels(['0.33', '0.5', '0.67', '0.83', '1.0'])

            if row != len(heights) - 1: ax[col][row].set_xticklabels([])
            if col != 0: ax[col][row].set_yticklabels([])

    fig.subplots_adjust(hspace=0.016, wspace=0.01)

    smol = 3e-2
    axtwin = []
    vons_range = [0.033, 30]
    for col in range(len(widths)):
        ax[col][0].semilogy()
        ax[col][0].set_ylim(vons_range)
        ax[col][1].set_ylim([1 / 3 - smol, 1 + smol])
        ax[col][2].set_ylim([0 - smol, 1 + smol])

        ax[col][0].axhline(1, 0, 1, c='grey', ls=':')
        ax[col][1].axhline(0.5, 0, 1, c='grey', ls=':')
        ax[col][1].axhline(1, 0, 1, c='grey', ls=':')
        ax[col][1].axhline(1/3, 0, 1, c='grey', ls=':')
        ax[col][2].axhline(0.5, 0, 1, c='grey', ls=':')
        ax[col][2].axhline(1, 0, 1, c='grey', ls=':')
        ax[col][2].axhline(0, 0, 1, c='grey', ls=':')

        axtwin.append(ax[col][0].twiny())
        axtwin[col].set_xlim([M200_min, M200_max])

        # ax[col][2].set_xlabel(r'$\log \, \mathrm{N}_{\mathrm{DM}}$')
        ax[col][2].set_xlabel(r'$\log \, \mathrm{N}_{200}$')

    ax[1][0].set_yticklabels([])
    ax[2][0].set_yticklabels([])

########################################################################################################################
    #concentrations
    time = np.array([max_T])

    ic_heat_fraction = np.array([0.0001])

    concs = np.array([5, 15])

    # bin_edges_5  = get_bin_edges(save_name_append, galaxy_name='V200-200kps_c_5')[2:4]
    # bin_edges_15 = get_bin_edges(save_name_append, galaxy_name='V200-200kps_c_15')[2:4]
    # bin_edges = [*bin_edges_5, *bin_edges_15]
    # bin_edges = np.ravel([get_bin_edges('diff', galaxy=Galaxy(conc=c))[2:4] for c in concs])
    bin_edges = np.ravel([[Galaxy(conc=c).get_Rd()]*2 for c in concs])

    #calc
    (v_on_sigma, kappa_rot, spheroid_to_total) = calc_indicators(N200s, bin_edges, time, ic_heat_fraction, concs)

    for k in range(len(widths)):
        ax[k][0].fill_between(log_N200s, v_on_sigma[0, :], v_on_sigma[1, :], color='k', alpha=0.3)
        if k == 0:
            ax[k][1].fill_between(log_N200s, kappa_rot[0, :], kappa_rot[1, :], color='k', alpha=0.3,
                                  label=r'$c = 10 \pm 5$')
        else:
            ax[k][1].fill_between(log_N200s, kappa_rot[0, :], kappa_rot[1, :], color='k', alpha=0.3)

        ax[k][2].fill_between(log_N200s, 1-spheroid_to_total[0, :], 1-spheroid_to_total[1, :], color='k', alpha=0.3)

########################################################################################################################
    # heat fractions
    time = np.array([max_T])

    # bin_edges = get_bin_edges('diff')[2:4]
    bin_edges = np.array([Galaxy().get_Rd()]*2)

    concs = np.array([10])

    v200 = 200
    analytic_dispersion = get_velocity_scale(bin_edges, analytic_dispersion=True)

    # ic_heat_fraction = np.array([0.0001, 0.05, 0.1, 0.2])
    ic_heat_fraction = np.array([0.0001, 0.05, 0.1, 0.2]) * v200 / analytic_dispersion

    #calc
    (v_on_sigma, kappa_rot, spheroid_to_total) = calc_indicators(N200s, bin_edges, time, ic_heat_fraction, concs)

    #plot
    heat_fraction_labels = [r'$\sigma_{i, 0}=0$',
                            r'$\sigma_{i, 0}=0.05 \,V_{200}$',
                            r'$\sigma_{i, 0}=0.10 \,V_{200}$',
                            r'$\sigma_{i, 0}=0.20 \,V_{200}$']

    lw = 4
    for j,ls in zip(range(len(ic_heat_fraction)), ['-', '-.', '--', ':', (0, (3, 1, 1, 1, 1, 1))]):
        if j == 0: c = 'k'
        else: c = 'C' + str(j + 5)

        ax[0][0].plot(log_N200s, v_on_sigma[j], c=c, ls=ls, lw=lw)
        ax[0][1].plot(log_N200s, kappa_rot[j], c=c, ls=ls, lw=lw, label=heat_fraction_labels[j])
        ax[0][2].plot(log_N200s, 1-spheroid_to_total[j], c=c, ls=ls, lw=lw)

########################################################################################################################
    # radii
    time = np.array([max_T])

    ic_heat_fraction = np.array([0.0001])

    r_half = get_bin_edges('cumulative')[3]
    # bin_edges = np.array([r_half / 4, r_half / 4, r_half, r_half, r_half * 4, r_half * 4])
    bin_edges = np.ravel(([Galaxy().get_Rd() / 4]*2, [Galaxy().get_Rd()]*2, [Galaxy().get_Rd() * 4]*2))

    concs = np.array([10])

    #calc
    (v_on_sigma, kappa_rot, spheroid_to_total) = calc_indicators(N200s, bin_edges, time, ic_heat_fraction, concs)

    #plot
    ax[1][0].plot(log_N200s, v_on_sigma[0], c='C0', ls='--', lw=lw)
    ax[1][0].plot(log_N200s, v_on_sigma[1], c='k', ls='-', lw=lw)
    ax[1][0].plot(log_N200s, v_on_sigma[2], c='C3', ls='-.', lw=lw)

    ax[1][1].plot(log_N200s, kappa_rot[0], c='C0', ls='--', lw=lw,
                  label=r'$R=0.25 \, R_{1/2}$')
    ax[1][1].plot(log_N200s, kappa_rot[1], c='k', ls='-', lw=lw,
                  label=r'$R_{1/2}=0.2 \, r_{-2}$')
    ax[1][1].plot(log_N200s, kappa_rot[2], c='C3', ls='-.', lw=lw,
                  label=r'$R=4 \, R_{1/2}$')

    ax[1][2].plot(log_N200s, 1-spheroid_to_total[0], c='C0', ls='--', lw=lw)
    ax[1][2].plot(log_N200s, 1-spheroid_to_total[1], c='k', ls='-', lw=lw)
    ax[1][2].plot(log_N200s, 1-spheroid_to_total[2], c='C3', ls='-.', lw=lw)

########################################################################################################################
    #times

    time = np.array([2, 5, 10, max_T])

    ic_heat_fraction = np.array([0.0001])

    # bin_edges = get_bin_edges('diff')[2:4]
    bin_edges = np.array([Galaxy().get_Rd()]*2)

    concs = np.array([10])

    #calc
    (v_on_sigma, kappa_rot, spheroid_to_total) = calc_indicators(N200s, bin_edges, time, ic_heat_fraction, concs)

    #plot
    ax[2][0].plot(log_N200s, v_on_sigma[0], c='C2', ls='-.', lw=lw)
    ax[2][0].plot(log_N200s, v_on_sigma[1], c='C1', ls='--', lw=lw)
    ax[2][0].plot(log_N200s, v_on_sigma[3], c='k', ls='-', lw=lw)

    ax[2][1].plot(log_N200s, kappa_rot[3], c='k', ls='-', lw=lw,
                  label=r'$t =$' + str(max_T) + ' Gyr')
    ax[2][1].plot(log_N200s, kappa_rot[1], c='C1', ls='--', lw=lw,
                  label=r'$t = 5$ Gyr')
    ax[2][1].plot(log_N200s, kappa_rot[0], c='C2', ls='-.', lw=lw,
                  label=r'$t = 2$ Gyr')

    ax[2][2].plot(log_N200s, 1-spheroid_to_total[0], c='C2', ls='-.', lw=lw)
    ax[2][2].plot(log_N200s, 1-spheroid_to_total[1], c='C1', ls='--', lw=lw)
    ax[2][2].plot(log_N200s, 1-spheroid_to_total[3], c='k', ls='-', lw=lw)

########################################################################################################################

    # ax[0][1].plot([2.42, 2.87], [0.705] * 2, lw=lw, c='k', ls='-')

    # 'lower right'
    ax[0][1].legend(labelspacing=0.3, handlelength=2, borderpad=0.3, handletextpad=0.3,
                    loc='upper left', frameon=False)
    ax[1][1].legend(labelspacing=0.3, handlelength=2, borderpad=0.3, handletextpad=0.2,
                    loc='upper left', frameon=False)
    ax[2][1].legend(labelspacing=0.3, handlelength=2, borderpad=0.3, handletextpad=0.3,
                    loc='upper left', frameon=False)

    # axtwin[1].set_xlabel(r'$\log M_{200} / $M$_\odot$ at EAGLE resolution')
    # axtwin[1].set_xlabel(r'$\log ( \, $M$_{200} / $M$_\odot)$ for DM particle mass $\log ( \, m_{\rm DM} / $M$_\odot) = 6$') #$m_{\rm DM} = 10^6$M$_\odot$')
    axtwin[1].set_xlabel(r'$\log ( \, $M$_{200} / $M$_\odot)$ $[\,$for $\log ( \, m_{\rm DM} / $M$_\odot) = 6\,]$', labelpad=12)
    # axtwin[1].set_xlabel(r'$\log ( \, $M$_{200} / $M$_\odot)$ $[\,$for $m_{\rm DM} = 10^6$M$_\odot \,]$')

    ax[0][0].set_ylabel(r'$\overline{v}_\phi / \sigma_{\mathrm{1D}}$')
    ax[0][1].set_ylabel(r'$\kappa_{\mathrm{rot}}$')
    ax[0][2].set_ylabel(r'$1 -$ S/T')

    ax[0][0].text(N200_min + 0.2, vons_range[1] - 8, r'$t = $' + str(max_T) + 'Gyr', ha='left', va='top')
    ax[0][0].text(N200_min + 0.2, vons_range[1] - 20, r'at $R_{1/2}=0.2 \, r_{-2}$', ha='left', va='top')

    ax[1][0].text(N200_min + 0.2, vons_range[1] - 8, r'$t = $' + str(max_T) + 'Gyr', ha='left', va='top')
    ax[1][0].text(N200_min + 0.2, vons_range[1] - 20, r'$\sigma_{i, 0} = 0$', ha='left', va='top')

    ax[2][0].text(N200_min + 0.2, vons_range[1] - 8, r'$\sigma_{i, 0} = 0$', ha='left', va='top')
    ax[2][0].text(N200_min + 0.2, vons_range[1] - 20, r'at $R_{1/2} = 0.2 \, r_{-2}$', ha='left', va='top')

    # ax[0][2].text(N200_max - 0.2, 0.97, r'$\Delta t = $' + str(max_T) + 'Gyr', ha='right', va='top')
    # ax[0][2].text(N200_max - 0.2, 0.86, r'at $R = R_{1/2}$', ha='right', va='top')
    #
    # ax[1][2].text(N200_max - 0.2, 0.97, r'$\Delta t = $' + str(max_T) + 'Gyr', ha='right', va='top')
    # ax[1][2].text(N200_max - 0.2, 0.86, r'$\sigma_{\rm 1D}^2 = 0$', ha='right', va='top')
    #
    # ax[2][2].text(N200_max - 0.2, 0.97, r'$\sigma_{\rm 1D}^2 = 0$', ha='right', va='top')
    # ax[2][2].text(N200_max - 0.2, 0.86, r'$R = R_{1/2}$', ha='right', va='top')

    # for i in range(3):
    #     for j in range(3):
    #         ax[i][j].axhline(4, 0, 1)
    #         ax[i][j].axvline(np.log10(1_125_000), 0, 1)
    #         ax[i][j].axvline(np.log10(272_500), 0, 1)
    #         ax[i][j].axvline(np.log10(126_400), 0, 1)

    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return

def plot_other_ind_n_dependence(save=False, name=''):

    frac = 0.7 #for j_z/j_c

    max_T = 13.8  # Gyr

    N200_max = 7.5
    N200_min = 3.2534  # 2.5
    N200s = np.logspace(N200_min, N200_max, 101)
    # N200s = np.logspace(N200_min, N200_max, 11)
    log_N200s = np.log10(N200s)

    m_DM_EAGLE = 1e6  # 9.70e6 #M_sun
    M200_EAGLE = m_DM_EAGLE * N200s
    log_M200_EAGLE = np.log10(M200_EAGLE)
    M200_max = np.log10(np.amax(M200_EAGLE))
    M200_min = np.log10(np.amin(M200_EAGLE))

    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(16.8, 10.8, forward=True)
    widths = [1, 1, 1]
    heights = [1, 1, 1]
    spec = fig.add_gridspec(ncols=3, nrows=3)

    ax = []
    for col in range(len(widths)):
        ax.append([])
        for row in range(len(heights)):
            ax[col].append(fig.add_subplot(spec[row, col]))

            ax[col][row].set_xlim([N200_min, N200_max])

            if row != len(heights) - 1: ax[col][row].set_xticklabels([])
            if col != 0: ax[col][row].set_yticklabels([])

            # ax[col][row].set_aspect('equal')

    # fig.subplots_adjust(hspace=0.016, wspace=0.01)
    fig.subplots_adjust(hspace=0.0, wspace=0.0)

    smol = 3e-2
    axtwin = []

    v_range = [0, 1]

    extend_range = lambda rng, pct: [np.mean(rng) - np.diff(rng) / 2 * pct, np.mean(rng) + np.diff(rng) / 2 * pct]

    v_range = extend_range(v_range, 1.1)

    for col in range(len(widths)):
        ax[col][0].set_ylim(v_range)
        ax[col][1].set_ylim(v_range)
        ax[col][2].set_ylim(v_range)

        axtwin.append(ax[col][0].twiny())
        axtwin[col].set_xlim([M200_min, M200_max])

        ax[col][2].set_xlabel(r'$\log \, \mathrm{N}_{200}$')

    ########################################################################################################################
    # concentrations
    time = np.array([max_T])

    ic_heat_fraction = np.array([0.0001])

    concs = np.array([5, 15])

    # bin_edges_5  = get_bin_edges(save_name_append, galaxy_name='V200-200kps_c_5')[2:4]
    # bin_edges_15 = get_bin_edges(save_name_append, galaxy_name='V200-200kps_c_15')[2:4]
    # bin_edges = [*bin_edges_5, *bin_edges_15]
    # bin_edges = np.ravel([get_bin_edges('diff', galaxy=Galaxy(conc=c))[2:4] for c in concs])
    bin_edges = np.ravel([[Galaxy(conc=c).get_Rd()] * 2 for c in concs])

    # calc
    (theory_disk_to_total, theory_bulge_to_total, theory_ca
     ) = calc_shape_inds(N200s, bin_edges, time, ic_heat_fraction, concs)

    for k in range(len(widths)):
        if k == 0:
            ax[k][0].fill_between(log_N200s, theory_disk_to_total[0, :], theory_disk_to_total[1, :], color='k', alpha=0.3,
                                  label=r'$c = 10 \pm 5$')
        else:
            ax[k][0].fill_between(log_N200s, theory_disk_to_total[0, :], theory_disk_to_total[1, :], color='k', alpha=0.3)

        ax[k][1].fill_between(log_N200s, theory_bulge_to_total[0, :], theory_bulge_to_total[1, :], color='k', alpha=0.3)

        if False:
            ax[k][2].fill_between(log_N200s, theory_ca[0, :], theory_ca[1, :], color='k', alpha=0.3,
                                  label=r'$c = 10 \pm 5$')
        else:
            ax[k][2].fill_between(log_N200s, theory_ca[0, :], theory_ca[1, :], color='k', alpha=0.3)

    ########################################################################################################################
    # heat fractions
    time = np.array([max_T])

    # bin_edges = get_bin_edges('diff')[2:4]
    bin_edges = np.array([Galaxy().get_Rd()] * 2)

    concs = np.array([10])

    v200 = 200
    analytic_dispersion = get_velocity_scale(bin_edges, analytic_dispersion=True)

    # ic_heat_fraction = np.array([0.0001, 0.05, 0.1, 0.2])
    ic_heat_fraction = np.array([0.0001, 0.05, 0.1, 0.2]) * v200 / analytic_dispersion

    # calc
    (theory_disk_to_total, theory_bulge_to_total, theory_ca
     ) = calc_shape_inds(N200s, bin_edges, time, ic_heat_fraction, concs)

    # plot
    heat_fraction_labels = [r'$0$',
                            r'$0.05$',
                            r'$0.10$',
                            r'$0.20$']

    lw = 4
    for j, ls in zip(range(len(ic_heat_fraction)), ['-', '-.', '--', ':', (0, (3, 1, 1, 1, 1, 1))]):
        if j == 0:
            c = 'k'
        else:
            c = 'C' + str(j + 5)

        ax[0][0].errorbar(log_N200s, theory_disk_to_total[j], c=c, ls=ls, lw=lw)
        ax[0][1].errorbar(log_N200s, theory_bulge_to_total[j], c=c, ls=ls, lw=lw)
        ax[0][2].errorbar(log_N200s, theory_ca[j], c=c, ls=ls, lw=lw, label=heat_fraction_labels[j])

    ########################################################################################################################
    # radii
    time = np.array([max_T])

    ic_heat_fraction = np.array([0.0001])

    r_half = get_bin_edges('cumulative')[3]
    # bin_edges = np.array([r_half / 4, r_half / 4, r_half, r_half, r_half * 4, r_half * 4])
    bin_edges = np.ravel(([Galaxy().get_Rd() / 4] * 2, [Galaxy().get_Rd()] * 2, [Galaxy().get_Rd() * 4] * 2))

    concs = np.array([10])

    # calc
    (theory_disk_to_total, theory_bulge_to_total, theory_ca
     ) = calc_shape_inds(N200s, bin_edges, time, ic_heat_fraction, concs)

    # plot
    ax[1][0].errorbar(log_N200s, theory_disk_to_total[0], c='C0', ls='--', lw=lw)
    ax[1][0].errorbar(log_N200s, theory_disk_to_total[1], c='k', ls='-', lw=lw)
    ax[1][0].errorbar(log_N200s, theory_disk_to_total[2], c='C3', ls='-.', lw=lw)

    ax[1][1].errorbar(log_N200s, theory_bulge_to_total[0], c='C0', ls='--', lw=lw)
    ax[1][1].errorbar(log_N200s, theory_bulge_to_total[1], c='k', ls='-', lw=lw)
    ax[1][1].errorbar(log_N200s, theory_bulge_to_total[2], c='C3', ls='-.', lw=lw)

    ax[1][2].errorbar(log_N200s, theory_ca[0], c='C0', ls='--', lw=lw,
                      label=r'$1/4$')
    ax[1][2].errorbar(log_N200s, theory_ca[1], c='k', ls='-', lw=lw,
                      label=r'$1$')
    ax[1][2].errorbar(log_N200s, theory_ca[2], c='C3', ls='-.', lw=lw,
                      label=r'$4$')

    ########################################################################################################################
    # times

    time = np.array([2, 5, 10, max_T])

    ic_heat_fraction = np.array([0.0001])

    # bin_edges = get_bin_edges('diff')[2:4]
    bin_edges = np.array([Galaxy().get_Rd()] * 2)

    concs = np.array([10])

    # calc
    (theory_disk_to_total, theory_bulge_to_total, theory_ca
     ) = calc_shape_inds(N200s, bin_edges, time, ic_heat_fraction, concs)

    # plot
    ax[2][0].errorbar(log_N200s, theory_disk_to_total[0], c='C2', ls='-.', lw=lw)
    ax[2][0].errorbar(log_N200s, theory_disk_to_total[1], c='C1', ls='--', lw=lw)
    ax[2][0].errorbar(log_N200s, theory_disk_to_total[3], c='k', ls='-', lw=lw)

    ax[2][1].errorbar(log_N200s, theory_bulge_to_total[0], c='C2', ls='-.', lw=lw)
    ax[2][1].errorbar(log_N200s, theory_bulge_to_total[1], c='C1', ls='--', lw=lw)
    ax[2][1].errorbar(log_N200s, theory_bulge_to_total[3], c='k', ls='-', lw=lw)

    ax[2][2].errorbar(log_N200s, theory_ca[0], c='C2', ls='-.', lw=lw,
                      label=r'$2$')
    ax[2][2].errorbar(log_N200s, theory_ca[1], c='C1', ls='--', lw=lw,
                      label=r'$5$')
    ax[2][2].errorbar(log_N200s, theory_ca[3], c='k', ls='-', lw=lw,
                      label=str(max_T))

    ########################################################################################################################

    # ax[0][1].plot([2.42, 2.87], [0.705] * 2, lw=lw, c='k', ls='-')

    # bbox = (-0.03, 0.95)
    bbox = (1.03, 2.05)
    ax[0][2].legend(labelspacing=0.2, handlelength=2, handletextpad=0.4,  # borderpad=0.3,
                    loc='upper right', bbox_to_anchor=bbox, frameon=False,
                    title=r'$\sigma_{i, 0} / V_{200}$')
    ax[1][2].legend(labelspacing=0.2, handlelength=2, handletextpad=0.4,  # borderpad=0.3,
                    loc='upper right', bbox_to_anchor=bbox, frameon=False,
                    title=r'$R / R_{1/2}$')
    ax[2][2].legend(labelspacing=0.2, handlelength=2, handletextpad=0.4,  # borderpad=0.3,
                    loc='upper right', bbox_to_anchor=bbox, frameon=False,
                    title=r'$t$ [Gyr]')
    # bbox = (-0.03, 0.37)
    bbox = (-0.03, 0.63)
    ax[0][0].legend(labelspacing=0.2, handlelength=2, handletextpad=0.4,  # borderpad=0.3,
                    loc='center left', bbox_to_anchor=bbox, frameon=False)
    ax[0][0].errorbar([3.385, 3.9], [(bbox[1] + 0.01) * (v_range[1] - v_range[0]) + v_range[0]] * 2,
                      lw=3, c='k', ls='-')

    # axtwin[1].set_xlabel(r'$\log M_{200} / $M$_\odot$ at EAGLE resolution')
    # axtwin[1].set_xlabel(r'$\log ( \, $M$_{200} / $M$_\odot)$ for DM particle mass $\log ( \, m_{\rm DM} / $M$_\odot) = 6$') #$m_{\rm DM} = 10^6$M$_\odot$')
    axtwin[1].set_xlabel(r'$\log ( \, $M$_{200} / $M$_\odot)$ $[\,$for $\log ( \, m_{\rm DM} / $M$_\odot) = 6\,]$',
                         labelpad=12)
    # axtwin[1].set_xlabel(r'$\log ( \, $M$_{200} / $M$_\odot)$ $[\,$for $m_{\rm DM} = 10^6$M$_\odot \,]$')

    ax[0][0].set_ylabel(r'D/T')
    ax[0][1].set_ylabel(r'S/T')
    ax[0][2].set_ylabel(r'$c/a (R = R_{1/2})$')

    ax[0][0].text(N200_min + 0.2, v_range[0] + 0.95 * (v_range[1] - v_range[0]), r'$t = $' + str(max_T) + 'Gyr',
                  ha='left', va='top')
    ax[0][0].text(N200_min + 0.2, v_range[0] + 0.82 * (v_range[1] - v_range[0]), r'at $R_{1/2}=0.2 \, r_{-2}$',
                  ha='left', va='top')

    ax[1][0].text(N200_min + 0.2, v_range[0] + 0.95 * (v_range[1] - v_range[0]), r'$t = $' + str(max_T) + 'Gyr',
                  ha='left', va='top')
    ax[1][0].text(N200_min + 0.2, v_range[0] + 0.82 * (v_range[1] - v_range[0]), r'$\sigma_{i, 0} = 0$',
                  ha='left', va='top')

    ax[2][0].text(N200_min + 0.2, v_range[0] + 0.95 * (v_range[1] - v_range[0]), r'$\sigma_{i, 0} = 0$',
                  ha='left', va='top')
    ax[2][0].text(N200_min + 0.2, v_range[0] + 0.82 * (v_range[1] - v_range[0]), r'$R_{1/2} = 0.2 \, r_{-2}$',
                  ha='left', va='top')

    for i in range(3):
        # for j in range(3):
        ax[i][0].axhline(0.5, 0, 1, c='gray', ls=':', zorder=-20)

    ax[0][1].axhline(0.5, 0, 0.68, c='gray', ls=':', zorder=-20)
    ax[1][1].axhline(0.5, 0, 1, c='gray', ls=':', zorder=-20)
    ax[2][1].axhline(0.5, 0, 1, c='gray', ls=':', zorder=-20)

    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return


def plot_kin_ind_n_dependence(save=False, name=''):

    max_T = 13.8 #Gyr

    N200_max = 7.5
    N200_min = 3.2534 #2.5
    N200s = np.logspace(N200_min, N200_max, 101)
    log_N200s = np.log10(N200s)

    m_DM_EAGLE = 1e6 #9.70e6 #M_sun
    M200_EAGLE = m_DM_EAGLE * N200s
    log_M200_EAGLE = np.log10(M200_EAGLE)
    M200_max = np.log10(np.amax(M200_EAGLE))
    M200_min = np.log10(np.amin(M200_EAGLE))

    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(16.8, 14.4, forward=True)
    widths = [1, 1, 1]
    heights = [1, 1, 1, 1]
    spec = fig.add_gridspec(ncols=3, nrows=4)

    ax = []
    for col in range(len(widths)):
        ax.append([])
        for row in range(len(heights)):
            ax[col].append(fig.add_subplot(spec[row, col]))

            ax[col][row].set_xlim([N200_min, N200_max])

            if row != len(heights) - 1: ax[col][row].set_xticklabels([])
            if col != 0: ax[col][row].set_yticklabels([])

            # ax[col][row].set_aspect('equal')

    # fig.subplots_adjust(hspace=0.016, wspace=0.01)
    fig.subplots_adjust(hspace=0.0, wspace=0.0)

    smol = 3e-2
    axtwin = []

    v_range = [np.log10(0.25), np.log10(25)]
    s_range = [0, 1]

    lambdar_range = [0, 1]
    krot_range = [0.3333, 1]
    kco_range = [0.1667, 1]

    extend_range = lambda rng, pct: [np.mean(rng) - np.diff(rng) /2 * pct, np.mean(rng) + np.diff(rng) /2 * pct]

    lambdar_range = extend_range(lambdar_range, 1.1)
    krot_range = extend_range(krot_range, 1.1)
    kco_range = extend_range(kco_range, 1.1)

    for col in range(len(widths)):
        ax[col][0].set_ylim(v_range)
        ax[col][1].set_ylim(lambdar_range)
        ax[col][2].set_ylim(krot_range)
        ax[col][3].set_ylim(kco_range)

        axtwin.append(ax[col][0].twiny())
        axtwin[col].set_xlim([M200_min, M200_max])

        ax[col][3].set_xlabel(r'$\log \, \mathrm{N}_{200}$')

########################################################################################################################
    #concentrations
    time = np.array([max_T])

    ic_heat_fraction = np.array([0.0001])

    concs = np.array([5, 15])

    # bin_edges_5  = get_bin_edges(save_name_append, galaxy_name='V200-200kps_c_5')[2:4]
    # bin_edges_15 = get_bin_edges(save_name_append, galaxy_name='V200-200kps_c_15')[2:4]
    # bin_edges = [*bin_edges_5, *bin_edges_15]
    # bin_edges = np.ravel([get_bin_edges('diff', galaxy=Galaxy(conc=c))[2:4] for c in concs])
    bin_edges = np.ravel([[Galaxy(conc=c).get_Rd()]*2 for c in concs])

    #calc
    (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
     ) = calc_raw_vels(N200s, bin_edges, time, ic_heat_fraction, concs)
    # (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
    #  ) = np.log10((theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2))

    theory_best_t2 = theory_best_z2 + theory_best_r2 + theory_best_p2

    v_on_sigma = theory_best_v / np.sqrt(theory_best_t2 / 3)
    v_on_sigma = np.log10(v_on_sigma)

    lambda_r = theory_best_v / np.sqrt(theory_best_v**2 + theory_best_t2 / np.sqrt(3))

    kappa_rot = (theory_best_v**2 + theory_best_p2) / (theory_best_v**2 + theory_best_t2)

    kappa_co = ((theory_best_v ** 2 + theory_best_p2)/2 * (erf(theory_best_v / np.sqrt(2 * theory_best_p2)) + 1) +
                (theory_best_v * np.sqrt(theory_best_p2 /(2 * np.pi)) * np.exp(-theory_best_v**2 / (2 * theory_best_p2)))
                ) / (theory_best_v ** 2 + theory_best_t2)

    for k in range(len(widths)):
        if k == 0:
            ax[k][0].fill_between(log_N200s, v_on_sigma[0, :], v_on_sigma[1, :], color='k', alpha=0.3,
                                  label=r'$c = 10 \pm 5$')
        else:
            ax[k][0].fill_between(log_N200s, v_on_sigma[0, :], v_on_sigma[1, :], color='k', alpha=0.3)

        ax[k][1].fill_between(log_N200s, lambda_r[0, :], lambda_r[1, :], color='k', alpha=0.3)

        ax[k][2].fill_between(log_N200s, kappa_rot[0, :], kappa_rot[1, :], color='k', alpha=0.3)

        if False:
            ax[k][3].fill_between(log_N200s, kappa_co[0, :], kappa_co[1, :], color='k', alpha=0.3,
                                  label=r'$c = 10 \pm 5$')
        else:
            ax[k][3].fill_between(log_N200s, kappa_co[0, :], kappa_co[1, :], color='k', alpha=0.3)

########################################################################################################################
    # heat fractions
    time = np.array([max_T])

    # bin_edges = get_bin_edges('diff')[2:4]
    bin_edges = np.array([Galaxy().get_Rd()]*2)

    concs = np.array([10])

    v200 = 200
    analytic_dispersion = get_velocity_scale(bin_edges, analytic_dispersion=True)

    # ic_heat_fraction = np.array([0.0001, 0.05, 0.1, 0.2])
    ic_heat_fraction = np.array([0.0001, 0.05, 0.1, 0.2]) * v200 / analytic_dispersion

    #calc
    (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
     ) = calc_raw_vels(N200s, bin_edges, time, ic_heat_fraction, concs)
    # (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
    #  ) = np.log10((theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2))

    theory_best_t2 = theory_best_z2 + theory_best_r2 + theory_best_p2

    v_on_sigma = theory_best_v / np.sqrt(theory_best_t2 / 3)
    v_on_sigma = np.log10(v_on_sigma)

    lambda_r = theory_best_v / np.sqrt(theory_best_v**2 + theory_best_t2 / np.sqrt(3))

    kappa_rot = (theory_best_v**2 + theory_best_p2) / (theory_best_v**2 + theory_best_t2)

    kappa_co = ((theory_best_v ** 2 + theory_best_p2)/2 * (erf(theory_best_v / np.sqrt(2 * theory_best_p2)) + 1) +
                (theory_best_v * np.sqrt(theory_best_p2 /(2 * np.pi)) * np.exp(-theory_best_v**2 / (2 * theory_best_p2)))
                ) / (theory_best_v ** 2 + theory_best_t2)

    #plot
    # heat_fraction_labels = [r'$\sigma_{i, 0}=0$',
    #                         r'$\sigma_{i, 0}=0.05 \,V_{200}$',
    #                         r'$\sigma_{i, 0}=0.10 \,V_{200}$',
    #                         r'$\sigma_{i, 0}=0.20 \,V_{200}$']
    heat_fraction_labels = [r'$0$',
                            r'$0.05$',
                            r'$0.10$',
                            r'$0.20$']

    lw = 4
    for j,ls in zip(range(len(ic_heat_fraction)), ['-', '-.', '--', ':', (0, (3, 1, 1, 1, 1, 1))]):
        if j == 0: c = 'k'
        else: c = 'C' + str(j + 5)

        ax[0][0].errorbar(log_N200s, v_on_sigma[j], c=c, ls=ls, lw=lw)
        ax[0][1].errorbar(log_N200s, lambda_r[j], c=c, ls=ls, lw=lw)
        ax[0][2].errorbar(log_N200s, kappa_rot[j], c=c, ls=ls, lw=lw, label=heat_fraction_labels[j])
        ax[0][3].errorbar(log_N200s, kappa_co[j], c=c, ls=ls, lw=lw)

########################################################################################################################
    # radii
    time = np.array([max_T])

    ic_heat_fraction = np.array([0.0001])

    r_half = get_bin_edges('cumulative')[3]
    # bin_edges = np.array([r_half / 4, r_half / 4, r_half, r_half, r_half * 4, r_half * 4])
    bin_edges = np.ravel(([Galaxy().get_Rd() / 4]*2, [Galaxy().get_Rd()]*2, [Galaxy().get_Rd() * 4]*2))

    concs = np.array([10])

    #calc
    (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
     ) = calc_raw_vels(N200s, bin_edges, time, ic_heat_fraction, concs)
    # (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
    #  ) = np.log10((theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2))

    theory_best_t2 = theory_best_z2 + theory_best_r2 + theory_best_p2

    v_on_sigma = theory_best_v / np.sqrt(theory_best_t2 / 3)
    v_on_sigma = np.log10(v_on_sigma)

    kappa_rot = (theory_best_v**2 + theory_best_p2) / (theory_best_v**2 + theory_best_t2)

    kappa_co = ((theory_best_v ** 2 + theory_best_p2)/2 * (erf(theory_best_v / np.sqrt(2 * theory_best_p2)) + 1) +
                (theory_best_v * np.sqrt(theory_best_p2 /(2 * np.pi)) * np.exp(-theory_best_v**2 / (2 * theory_best_p2)))
                ) / (theory_best_v ** 2 + theory_best_t2)

    lambda_r = theory_best_v / np.sqrt(theory_best_v**2 + theory_best_t2 / np.sqrt(3))

    #plot
    ax[1][0].errorbar(log_N200s, v_on_sigma[0], c='C0', ls='--', lw=lw)
    ax[1][0].errorbar(log_N200s, v_on_sigma[1], c='k', ls='-', lw=lw)
    ax[1][0].errorbar(log_N200s, v_on_sigma[2], c='C3', ls='-.', lw=lw)

    ax[1][1].errorbar(log_N200s, lambda_r[0], c='C0', ls='--', lw=lw)
    ax[1][1].errorbar(log_N200s, lambda_r[1], c='k', ls='-', lw=lw)
    ax[1][1].errorbar(log_N200s, lambda_r[2], c='C3', ls='-.', lw=lw)

    ax[1][2].errorbar(log_N200s, kappa_rot[0], c='C0', ls='--', lw=lw,
                      # label=r'$R=0.25 \, R_{1/2}$')
                      label=r'$1/4$')
    ax[1][2].errorbar(log_N200s, kappa_rot[1], c='k', ls='-', lw=lw,
                      # label=r'$R_{1/2}=0.2 \, r_{-2}$')
                      label=r'$1$')
    ax[1][2].errorbar(log_N200s, kappa_rot[2], c='C3', ls='-.', lw=lw,
                      # label=r'$R=4 \, R_{1/2}$')
                      label=r'$4$')

    ax[1][3].errorbar(log_N200s, kappa_co[0], c='C0', ls='--', lw=lw)
    ax[1][3].errorbar(log_N200s, kappa_co[1], c='k', ls='-', lw=lw)
    ax[1][3].errorbar(log_N200s, kappa_co[2], c='C3', ls='-.', lw=lw)

########################################################################################################################
    #times

    time = np.array([2, 5, 10, max_T])

    ic_heat_fraction = np.array([0.0001])

    # bin_edges = get_bin_edges('diff')[2:4]
    bin_edges = np.array([Galaxy().get_Rd()]*2)

    concs = np.array([10])

    #calc
    (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
     ) = calc_raw_vels(N200s, bin_edges, time, ic_heat_fraction, concs)
    # (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
    #  ) = np.log10((theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2))

    theory_best_t2 = theory_best_z2 + theory_best_r2 + theory_best_p2

    v_on_sigma = theory_best_v / np.sqrt(theory_best_t2 / 3)
    v_on_sigma = np.log10(v_on_sigma)

    kappa_rot = (theory_best_v**2 + theory_best_p2) / (theory_best_v**2 + theory_best_t2)

    kappa_co = ((theory_best_v ** 2 + theory_best_p2)/2 * (erf(theory_best_v / np.sqrt(2 * theory_best_p2)) + 1) +
                (theory_best_v * np.sqrt(theory_best_p2 /(2 * np.pi)) * np.exp(-theory_best_v**2 / (2 * theory_best_p2)))
                ) / (theory_best_v ** 2 + theory_best_t2)

    lambda_r = theory_best_v / np.sqrt(theory_best_v**2 + theory_best_t2 / np.sqrt(3))

    #plot
    ax[2][0].errorbar(log_N200s, v_on_sigma[0], c='C2', ls='-.', lw=lw)
    ax[2][0].errorbar(log_N200s, v_on_sigma[1], c='C1', ls='--', lw=lw)
    ax[2][0].errorbar(log_N200s, v_on_sigma[3], c='k', ls='-', lw=lw)

    ax[2][1].errorbar(log_N200s, lambda_r[0], c='C2', ls='-.', lw=lw)
    ax[2][1].errorbar(log_N200s, lambda_r[1], c='C1', ls='--', lw=lw)
    ax[2][1].errorbar(log_N200s, lambda_r[3], c='k', ls='-', lw=lw)

    ax[2][2].errorbar(log_N200s, kappa_rot[0], c='C2', ls='-.', lw=lw,
                      # label=r'$t = 2$ Gyr')
                      label=r'$2$')
    ax[2][2].errorbar(log_N200s, kappa_rot[1], c='C1', ls='--', lw=lw,
                      # label=r'$t = 5$ Gyr')
                      label=r'$5$')
    ax[2][2].errorbar(log_N200s, kappa_rot[3], c='k', ls='-', lw=lw,
                      # label=r'$t =$' + str(max_T) + ' Gyr')
                      label=str(max_T))

    ax[2][3].errorbar(log_N200s, kappa_co[0], c='C2', ls='-.', lw=lw)
    ax[2][3].errorbar(log_N200s, kappa_co[1], c='C1', ls='--', lw=lw)
    ax[2][3].errorbar(log_N200s, kappa_co[3], c='k', ls='-', lw=lw)

########################################################################################################################

    # ax[0][1].plot([2.42, 2.87], [0.705] * 2, lw=lw, c='k', ls='-')

    bbox = (1.03, 0.95)
    # bbox = (-0.03, 2.05)
    ax[0][2].legend(labelspacing=0.2, handlelength=2, handletextpad=0.4, #borderpad=0.3,
                    loc='lower right', bbox_to_anchor=bbox, frameon=False,
                    title=r'$\sigma_{i, 0} / V_{200}$')
    ax[1][2].legend(labelspacing=0.2, handlelength=2, handletextpad=0.4,  #borderpad=0.3,
                    loc='lower right', bbox_to_anchor=bbox, frameon=False,
                    title=r'$R / R_{1/2}$')
    ax[2][2].legend(labelspacing=0.2, handlelength=2, handletextpad=0.4,  #borderpad=0.3,
                    loc='lower right', bbox_to_anchor=bbox, frameon=False,
                    title=r'$t$ [Gyr]')

    # ax[1][1].text(4, 0.5, r'$R = 0.25 \, R_{1/2}$',
    #               ha='center', va='center', rotation=67)
    # ax[1][1].text(5, 0.5, r'$R_{1/2} = 0.2 \, r_{-2}$',
    #               ha='center', va='center', rotation=67)
    # ax[1][1].text(6, 0.5, r'$R = 4 \, R_{1/2}$',
    #               ha='center', va='center', rotation=67)

    # bbox = (-0.03, 0.37)
    bbox = (-0.03, 0.63)
    ax[0][0].legend(labelspacing=0.2, handlelength=2, handletextpad=0.4, #borderpad=0.3,
                    loc='center left', bbox_to_anchor=bbox, frameon=False)
    ax[0][0].errorbar([3.385, 3.9], [(bbox[1] + 0.01) * (v_range[1] - v_range[0]) + v_range[0]] * 2,
                      lw=3, c='k', ls='-')

    # axtwin[1].set_xlabel(r'$\log M_{200} / $M$_\odot$ at EAGLE resolution')
    # axtwin[1].set_xlabel(r'$\log ( \, $M$_{200} / $M$_\odot)$ for DM particle mass $\log ( \, m_{\rm DM} / $M$_\odot) = 6$') #$m_{\rm DM} = 10^6$M$_\odot$')
    axtwin[1].set_xlabel(r'$\log ( \, $M$_{200} / $M$_\odot)$ $[\,$for $\log ( \, m_{\rm DM} / $M$_\odot) = 6\,]$', labelpad=12)
    # axtwin[1].set_xlabel(r'$\log ( \, $M$_{200} / $M$_\odot)$ $[\,$for $m_{\rm DM} = 10^6$M$_\odot \,]$')

    ax[0][0].set_ylabel(r'$\log \, \overline{v} / \sigma_{\rm 1D}$')
    ax[0][1].set_ylabel(r'$\lambda_r$')
    ax[0][2].set_ylabel(r'$\kappa_{\rm rot}$')
    ax[0][3].set_ylabel(r'$\kappa_{\rm co}$')

    ax[0][0].text(N200_min + 0.2, v_range[0] + 0.95 * (v_range[1] - v_range[0]), r'$t = $' + str(max_T) + 'Gyr',
                  ha='left', va='top')
    ax[0][0].text(N200_min + 0.2, v_range[0] + 0.82 * (v_range[1] - v_range[0]), r'at $R_{1/2}=0.2 \, r_{-2}$',
                  ha='left', va='top')

    ax[1][0].text(N200_min + 0.2, v_range[0] + 0.95 * (v_range[1] - v_range[0]), r'$t = $' + str(max_T) + 'Gyr',
                  ha='left', va='top')
    ax[1][0].text(N200_min + 0.2, v_range[0] + 0.82 * (v_range[1] - v_range[0]), r'$\sigma_{i, 0} = 0$',
                  ha='left', va='top')

    ax[2][0].text(N200_min + 0.2, v_range[0] + 0.95 * (v_range[1] - v_range[0]), r'$\sigma_{i, 0} = 0$',
                  ha='left', va='top')
    ax[2][0].text(N200_min + 0.2, v_range[0] + 0.82 * (v_range[1] - v_range[0]), r'at $R_{1/2} = 0.2 \, r_{-2}$',
                  ha='left', va='top')

    # ax[0][2].text(N200_max - 0.2, 0.97, r'$\Delta t = $' + str(max_T) + 'Gyr', ha='right', va='top')
    # ax[0][2].text(N200_max - 0.2, 0.86, r'at $R = R_{1/2}$', ha='right', va='top')
    #
    # ax[1][2].text(N200_max - 0.2, 0.97, r'$\Delta t = $' + str(max_T) + 'Gyr', ha='right', va='top')
    # ax[1][2].text(N200_max - 0.2, 0.86, r'$\sigma_{\rm 1D}^2 = 0$', ha='right', va='top')
    #
    # ax[2][2].text(N200_max - 0.2, 0.97, r'$\sigma_{\rm 1D}^2 = 0$', ha='right', va='top')
    # ax[2][2].text(N200_max - 0.2, 0.86, r'$R = R_{1/2}$', ha='right', va='top')

    # for i in range(3):
    #     for j in range(3):
    #         ax[i][j].axhline(4, 0, 1)
    #         ax[i][j].axvline(np.log10(1_125_000), 0, 1)
    #         ax[i][j].axvline(np.log10(272_500), 0, 1)
    #         ax[i][j].axvline(np.log10(126_400), 0, 1)

    for i in range(3):
        # for j in range(4):
        ax[i][0].axhline(0, 0, 1, c='gray', ls=':', zorder=-20)
        ax[i][2].axhline(0.5, 0, 1, c='gray', ls=':', zorder=-20)
        ax[i][2].axhline(0.7, 0, 1, c='gray', ls=':', zorder=-20)
        ax[i][3].axhline(0.4, 0, 1, c='gray', ls=':', zorder=-20)

    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return


def plot_vel_n_dependence(save=False, name=''):

    max_T = 13.8 #Gyr

    N200_max = 7.5
    N200_min = 3.2534 #2.5
    N200s = np.logspace(N200_min, N200_max, 101)
    log_N200s = np.log10(N200s)

    m_DM_EAGLE = 1e6 #9.70e6 #M_sun
    M200_EAGLE = m_DM_EAGLE * N200s
    log_M200_EAGLE = np.log10(M200_EAGLE)
    M200_max = np.log10(np.amax(M200_EAGLE))
    M200_min = np.log10(np.amin(M200_EAGLE))

    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(16.8, 14.4, forward=True)
    widths = [1, 1, 1]
    heights = [1, 1, 1, 1]
    spec = fig.add_gridspec(ncols=3, nrows=4)

    ax = []
    for col in range(len(widths)):
        ax.append([])
        for row in range(len(heights)):
            ax[col].append(fig.add_subplot(spec[row, col]))

            ax[col][row].set_xlim([N200_min, N200_max])

            if row != len(heights) - 1: ax[col][row].set_xticklabels([])
            if col != 0: ax[col][row].set_yticklabels([])

            ax[col][row].set_aspect('equal')


    # fig.subplots_adjust(hspace=0.016, wspace=0.01)
    fig.subplots_adjust(hspace=0.0, wspace=0.0)

    smol = 3e-2
    axtwin = []

    v_range = [-2.6, 0.1]
    s_range = [-2.8, -0.1]

    for col in range(len(widths)):
        ax[col][0].set_ylim(v_range)
        ax[col][1].set_ylim(s_range)
        ax[col][2].set_ylim(s_range)
        ax[col][3].set_ylim(s_range)

        axtwin.append(ax[col][0].twiny())
        axtwin[col].set_xlim([M200_min, M200_max])

        ax[col][3].set_xlabel(r'$\log \, \mathrm{N}_{200}$')

########################################################################################################################
    #concentrations
    time = np.array([max_T])

    ic_heat_fraction = np.array([0.0001])

    concs = np.array([5, 15])

    # bin_edges_5  = get_bin_edges(save_name_append, galaxy_name='V200-200kps_c_5')[2:4]
    # bin_edges_15 = get_bin_edges(save_name_append, galaxy_name='V200-200kps_c_15')[2:4]
    # bin_edges = [*bin_edges_5, *bin_edges_15]
    # bin_edges = np.ravel([get_bin_edges('diff', galaxy=Galaxy(conc=c))[2:4] for c in concs])
    bin_edges = np.ravel([[Galaxy(conc=c).get_Rd()]*2 for c in concs])

    #calc
    (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
     ) = calc_vels(N200s, bin_edges, time, ic_heat_fraction, concs)
    (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
     ) = np.log10((theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2))

    for k in range(len(widths)):
        if k == 0:
            ax[k][0].fill_between(log_N200s, theory_best_v[0, :], theory_best_v[1, :], color='k', alpha=0.3,
                                  label=r'$c = 10 \pm 5$')
        else:
            ax[k][0].fill_between(log_N200s, theory_best_v[0, :], theory_best_v[1, :], color='k', alpha=0.3)

        ax[k][1].fill_between(log_N200s, theory_best_z2[0, :], theory_best_z2[1, :], color='k', alpha=0.3)

        if False:
            ax[k][2].fill_between(log_N200s, theory_best_r2[0, :], theory_best_r2[1, :], color='k', alpha=0.3,
                                  label=r'$c = 10 \pm 5$')
        else:
            ax[k][2].fill_between(log_N200s, theory_best_r2[0, :], theory_best_r2[1, :], color='k', alpha=0.3)

        # ax[k][2].fill_between(log_N200s, theory_best_r2[0, :], theory_best_r2[1, :], color='k', alpha=0.3)
        ax[k][3].fill_between(log_N200s, theory_best_p2[0, :], theory_best_p2[1, :], color='k', alpha=0.3)

########################################################################################################################
    # heat fractions
    time = np.array([max_T])

    # bin_edges = get_bin_edges('diff')[2:4]
    bin_edges = np.array([Galaxy().get_Rd()]*2)

    concs = np.array([10])

    v200 = 200
    analytic_dispersion = get_velocity_scale(bin_edges, analytic_dispersion=True)

    # ic_heat_fraction = np.array([0.0001, 0.05, 0.1, 0.2])
    ic_heat_fraction = np.array([0.0001, 0.05, 0.1, 0.2]) * v200 / analytic_dispersion

    #calc
    (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
     ) = calc_vels(N200s, bin_edges, time, ic_heat_fraction, concs)
    (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
     ) = np.log10((theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2))

    #plot
    heat_fraction_labels = [r'$\sigma_{i, 0}=0$',
                            r'$\sigma_{i, 0}=0.05 \,V_{200}$',
                            r'$\sigma_{i, 0}=0.10 \,V_{200}$',
                            r'$\sigma_{i, 0}=0.20 \,V_{200}$']

    lw = 4
    for j,ls in zip(range(len(ic_heat_fraction)), ['-', '-.', '--', ':', (0, (3, 1, 1, 1, 1, 1))]):
        if j == 0: c = 'k'
        else: c = 'C' + str(j + 5)

        ax[0][0].errorbar(log_N200s, theory_best_v[j], c=c, ls=ls, lw=lw)
        ax[0][1].errorbar(log_N200s, theory_best_z2[j], c=c, ls=ls, lw=lw)
        ax[0][2].errorbar(log_N200s, theory_best_r2[j], c=c, ls=ls, lw=lw, label=heat_fraction_labels[j])
        ax[0][3].errorbar(log_N200s, theory_best_p2[j], c=c, ls=ls, lw=lw)

########################################################################################################################
    # radii
    time = np.array([max_T])

    ic_heat_fraction = np.array([0.0001])

    r_half = get_bin_edges('cumulative')[3]
    # bin_edges = np.array([r_half / 4, r_half / 4, r_half, r_half, r_half * 4, r_half * 4])
    bin_edges = np.ravel(([Galaxy().get_Rd() / 4]*2, [Galaxy().get_Rd()]*2, [Galaxy().get_Rd() * 4]*2))

    concs = np.array([10])

    #calc
    (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
     ) = calc_vels(N200s, bin_edges, time, ic_heat_fraction, concs)
    (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
     ) = np.log10((theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2))

    #plot
    ax[1][0].errorbar(log_N200s, theory_best_v[0], c='C0', ls='--', lw=lw)
    ax[1][0].errorbar(log_N200s, theory_best_v[1], c='k', ls='-', lw=lw)
    ax[1][0].errorbar(log_N200s, theory_best_v[2], c='C3', ls='-.', lw=lw)

    ax[1][1].errorbar(log_N200s, theory_best_z2[0], c='C0', ls='--', lw=lw)
    ax[1][1].errorbar(log_N200s, theory_best_z2[1], c='k', ls='-', lw=lw)
    ax[1][1].errorbar(log_N200s, theory_best_z2[2], c='C3', ls='-.', lw=lw)

    ax[1][2].errorbar(log_N200s, theory_best_r2[0], c='C0', ls='--', lw=lw,
                  label=r'$R=0.25 \, R_{1/2}$')
    ax[1][2].errorbar(log_N200s, theory_best_r2[1], c='k', ls='-', lw=lw,
                  label=r'$R_{1/2}=0.2 \, r_{-2}$')
    ax[1][2].errorbar(log_N200s, theory_best_r2[2], c='C3', ls='-.', lw=lw,
                  label=r'$R=4 \, R_{1/2}$')

    ax[1][3].errorbar(log_N200s, theory_best_p2[0], c='C0', ls='--', lw=lw)
    ax[1][3].errorbar(log_N200s, theory_best_p2[1], c='k', ls='-', lw=lw)
    ax[1][3].errorbar(log_N200s, theory_best_p2[2], c='C3', ls='-.', lw=lw)

########################################################################################################################
    #times

    time = np.array([2, 5, 10, max_T])

    ic_heat_fraction = np.array([0.0001])

    # bin_edges = get_bin_edges('diff')[2:4]
    bin_edges = np.array([Galaxy().get_Rd()]*2)

    concs = np.array([10])

    #calc
    (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
     ) = calc_vels(N200s, bin_edges, time, ic_heat_fraction, concs)
    (theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2
     ) = np.log10((theory_best_v, theory_best_z2, theory_best_r2, theory_best_p2))

    #plot
    ax[2][0].errorbar(log_N200s, theory_best_v[0], c='C2', ls='-.', lw=lw)
    ax[2][0].errorbar(log_N200s, theory_best_v[1], c='C1', ls='--', lw=lw)
    ax[2][0].errorbar(log_N200s, theory_best_v[3], c='k', ls='-', lw=lw)

    ax[2][1].errorbar(log_N200s, theory_best_z2[0], c='C2', ls='-.', lw=lw)
    ax[2][1].errorbar(log_N200s, theory_best_z2[1], c='C1', ls='--', lw=lw)
    ax[2][1].errorbar(log_N200s, theory_best_z2[3], c='k', ls='-', lw=lw)

    ax[2][2].errorbar(log_N200s, theory_best_r2[0], c='C2', ls='-.', lw=lw,
                  label=r'$t = 2$ Gyr')
    ax[2][2].errorbar(log_N200s, theory_best_r2[1], c='C1', ls='--', lw=lw,
                  label=r'$t = 5$ Gyr')
    ax[2][2].errorbar(log_N200s, theory_best_r2[3], c='k', ls='-', lw=lw,
                  label=r'$t =$' + str(max_T) + ' Gyr')

    ax[2][3].errorbar(log_N200s, theory_best_p2[0], c='C2', ls='-.', lw=lw)
    ax[2][3].errorbar(log_N200s, theory_best_p2[1], c='C1', ls='--', lw=lw)
    ax[2][3].errorbar(log_N200s, theory_best_p2[3], c='k', ls='-', lw=lw)

########################################################################################################################

    # ax[0][1].plot([2.42, 2.87], [0.705] * 2, lw=lw, c='k', ls='-')

    # 'lower right'
    bbox = (-0.03, 0.95)
    ax[0][2].legend(labelspacing=0.2, handlelength=2, handletextpad=0.4, #borderpad=0.3,
                    loc='lower left', bbox_to_anchor=bbox, frameon=False)
    ax[1][2].legend(labelspacing=0.2, handlelength=2, handletextpad=0.4,  #borderpad=0.3,
                    loc='lower left', bbox_to_anchor=bbox, frameon=False)
    ax[2][2].legend(labelspacing=0.2, handlelength=2, handletextpad=0.4,  #borderpad=0.3,
                    loc='lower left', bbox_to_anchor=bbox, frameon=False)
    bbox = (-0.03, 0.37)
    ax[0][0].legend(labelspacing=0.2, handlelength=2, handletextpad=0.4, #borderpad=0.3,
                    loc='center left', bbox_to_anchor=bbox, frameon=False)
    ax[0][0].errorbar([3.385, 3.9], [-1.575, -1.575], lw=3, c='k', ls='-')

    # axtwin[1].set_xlabel(r'$\log M_{200} / $M$_\odot$ at EAGLE resolution')
    # axtwin[1].set_xlabel(r'$\log ( \, $M$_{200} / $M$_\odot)$ for DM particle mass $\log ( \, m_{\rm DM} / $M$_\odot) = 6$') #$m_{\rm DM} = 10^6$M$_\odot$')
    axtwin[1].set_xlabel(r'$\log ( \, $M$_{200} / $M$_\odot)$ $[\,$for $\log ( \, m_{\rm DM} / $M$_\odot) = 6\,]$', labelpad=12)
    # axtwin[1].set_xlabel(r'$\log ( \, $M$_{200} / $M$_\odot)$ $[\,$for $m_{\rm DM} = 10^6$M$_\odot \,]$')

    ax[0][0].set_ylabel(r'$\log ([V_c - \overline{v}_\phi] / V_{200})$')
    ax[0][1].set_ylabel(r'$\log (\sigma_z^2 / V_{200}^2)$')
    ax[0][2].set_ylabel(r'$\log (\sigma_R^2 / V_{200}^2)$')
    ax[0][3].set_ylabel(r'$\log (\sigma_\phi^2 / V_{200}^2)$')

    ax[0][0].text(N200_min + 0.2, v_range[0] + 0.18 * (v_range[1] - v_range[0]), r'$t = $' + str(max_T) + 'Gyr',
                  ha='left', va='bottom')
    ax[0][0].text(N200_min + 0.2, v_range[0] + 0.05 * (v_range[1] - v_range[0]), r'at $R_{1/2}=0.2 \, r_{-2}$',
                  ha='left', va='bottom')

    ax[1][0].text(N200_min + 0.2, v_range[0] + 0.18 * (v_range[1] - v_range[0]), r'$t = $' + str(max_T) + 'Gyr',
                  ha='left', va='bottom')
    ax[1][0].text(N200_min + 0.2, v_range[0] + 0.05 * (v_range[1] - v_range[0]), r'$\sigma_{i, 0} = 0$',
                  ha='left', va='bottom')

    ax[2][0].text(N200_min + 0.2, v_range[0] + 0.18 * (v_range[1] - v_range[0]), r'$\sigma_{i, 0} = 0$',
                  ha='left', va='bottom')
    ax[2][0].text(N200_min + 0.2, v_range[0] + 0.05 * (v_range[1] - v_range[0]), r'at $R_{1/2} = 0.2 \, r_{-2}$',
                  ha='left', va='bottom')

    # ax[0][2].text(N200_max - 0.2, 0.97, r'$\Delta t = $' + str(max_T) + 'Gyr', ha='right', va='top')
    # ax[0][2].text(N200_max - 0.2, 0.86, r'at $R = R_{1/2}$', ha='right', va='top')
    #
    # ax[1][2].text(N200_max - 0.2, 0.97, r'$\Delta t = $' + str(max_T) + 'Gyr', ha='right', va='top')
    # ax[1][2].text(N200_max - 0.2, 0.86, r'$\sigma_{\rm 1D}^2 = 0$', ha='right', va='top')
    #
    # ax[2][2].text(N200_max - 0.2, 0.97, r'$\sigma_{\rm 1D}^2 = 0$', ha='right', va='top')
    # ax[2][2].text(N200_max - 0.2, 0.86, r'$R = R_{1/2}$', ha='right', va='top')

    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return


def plot_other_poster_n_dependence(save=False, name=''):

    max_T = 13.8 #Gyr

    N200_max = 7.5
    N200_min = 2.5
    N200s = np.logspace(N200_min, N200_max, 101)
    log_N200s = np.log10(N200s)

    m_DM_EAGLE = 1e6 #9.70e6 #M_sun
    M200_EAGLE = m_DM_EAGLE * N200s
    log_M200_EAGLE = np.log10(M200_EAGLE)
    M200_max = np.log10(np.amax(M200_EAGLE))
    M200_min = np.log10(np.amin(M200_EAGLE))

    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(7.5, 5.2, forward=True)
    ax = plt.subplot(111)

    ax.semilogy()

    axtwin = []
    # vons_range = [0.033, 30]
    # vons_range = [0.1, 40]
    vons_range = [0.3, 40]
    ax.set_ylim(vons_range)
    ax.set_xlim([N200_min, N200_max])

    ax.axhline(1, 0, 1, c='grey', ls=':')

    axtwin = ax.twiny()
    axtwin.set_xlim([M200_min, M200_max])

    # ax.set_xlabel(r'$\log \, \mathrm{N}_{\mathrm{DM}}$')
    ax.set_xlabel(r'$\log \, \mathrm{N}_{200}$')

    lw=3

########################################################################################################################
    #concentrations
    time = np.array([max_T])

    ic_heat_fraction = np.array([0.0001])

    concs = np.array([5, 15])

    # bin_edges_5  = get_bin_edges(save_name_append, galaxy_name='V200-200kps_c_5')[2:4]
    # bin_edges_15 = get_bin_edges(save_name_append, galaxy_name='V200-200kps_c_15')[2:4]
    # bin_edges = [*bin_edges_5, *bin_edges_15]
    # bin_edges = np.ravel([get_bin_edges('diff', galaxy=Galaxy(conc=c))[2:4] for c in concs])
    bin_edges = np.ravel([[Galaxy(conc=c).get_Rd()]*2 for c in concs])

    #calc
    (v_on_sigma, kappa_rot, spheroid_to_total) = calc_indicators(N200s, bin_edges, time, ic_heat_fraction, concs)

    ax.fill_between(log_N200s, v_on_sigma[0, :], v_on_sigma[1, :], color='k', alpha=0.3,
                    label=r'$c = 10 \pm 5$')

########################################################################################################################
    # radii
    time = np.array([2, 5, 10, max_T])

    ic_heat_fraction = np.array([0.0001])

    # bin_edges = get_bin_edges('diff')[2:4]
    bin_edges = np.array([Galaxy().get_Rd()]*2)

    concs = np.array([10])

    #calc
    (v_on_sigma, kappa_rot, spheroid_to_total) = calc_indicators(N200s, bin_edges, time, ic_heat_fraction, concs)

    #plot
    ax.plot(log_N200s, v_on_sigma[0], c='C2', ls='-.', lw=lw,
                  label=r'$t = 2$ Gyr')
    ax.plot(log_N200s, v_on_sigma[1], c='C1', ls='--', lw=lw,
                  label=r'$t = 5$ Gyr')
    ax.plot(log_N200s, v_on_sigma[3], c='k', ls='-', lw=lw,
                  label=r'$t =$' + str(max_T) + ' Gyr')

########################################################################################################################

    # ax[0][1].plot([2.42, 2.87], [0.705] * 2, lw=lw, c='k', ls='-')

    # 'lower right'
    ax.legend(labelspacing=0.3, handlelength=2, borderpad=0.3, handletextpad=0.2,
                    loc='upper left', frameon=False)

    # axtwin.set_xlabel(r'$\log ( \, $M$_{200} / $M$_\odot)$ $[\,$for $\log ( \, m_{\rm DM} / $M$_\odot) = 6\,]$', labelpad=12)
    axtwin.set_xlabel(r'$\log ( \, $M$_{200} / $M$_\odot)$ $[\,$for $\, m_{\rm DM} = 10^6 $M$_\odot]$', labelpad=12)

    ax.set_ylabel(r'$\overline{v}_\phi / \sigma_{\mathrm{1D}}$')

    ax.text(N200_max - 0.1, vons_range[0] * 2.05, r'$\sigma_{t = 0} = 0$', ha='right', va='bottom')
    # ax.text(N200_max - 0.1, vons_range[0] * 2.05, r'$t = $' + str(max_T) + 'Gyr', ha='right', va='bottom')
    ax.text(N200_max - 0.1, vons_range[0] * 1.2, r'at $R_{1/2}=0.2 \, r_{-2}$', ha='right', va='bottom')

    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return


def plot_poster_n_dependence(save=False, name=''):

    max_T = 13.8 #Gyr

    N200_max = 7.5
    N200_min = 2.5
    N200s = np.logspace(N200_min, N200_max, 101)
    log_N200s = np.log10(N200s)

    m_DM_EAGLE = 1e6 #9.70e6 #M_sun
    M200_EAGLE = m_DM_EAGLE * N200s
    log_M200_EAGLE = np.log10(M200_EAGLE)
    M200_max = np.log10(np.amax(M200_EAGLE))
    M200_min = np.log10(np.amin(M200_EAGLE))

    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(15, 5.2, forward=True)
    widths = [1, 1]
    heights = [1]
    spec = fig.add_gridspec(ncols=2, nrows=1, width_ratios=widths, height_ratios=heights)

    ax = []
    for col in range(len(widths)):
        ax.append([])
        for row in range(len(heights)):
            ax[col].append(fig.add_subplot(spec[row, col]))

            ax[col][row].set_xlim([N200_min, N200_max])

            if row != len(heights) - 1: ax[col][row].set_xticklabels([])
            if col != 0: ax[col][row].set_yticklabels([])

    fig.subplots_adjust(hspace=0.016, wspace=0.01)

    smol = 3e-2
    axtwin = []
    vons_range = [0.033, 30]
    for col in range(len(widths)):
        ax[col][0].set_ylim([0 - smol, 1 + smol])

        ax[col][0].axhline(0.5, 0, 1, c='grey', ls=':')
        ax[col][0].axhline(1, 0, 1, c='grey', ls=':')
        ax[col][0].axhline(0, 0, 1, c='grey', ls=':')

        axtwin.append(ax[col][0].twiny())
        axtwin[col].set_xlim([M200_min, M200_max])

        # ax[col][2].set_xlabel(r'$\log \, \mathrm{N}_{\mathrm{DM}}$')
        ax[col][0].set_xlabel(r'$\log \, \mathrm{N}_{200}$')

########################################################################################################################
    #concentrations
    time = np.array([max_T])

    ic_heat_fraction = np.array([0.0001])

    concs = np.array([5, 15])

    # bin_edges_5  = get_bin_edges(save_name_append, galaxy_name='V200-200kps_c_5')[2:4]
    # bin_edges_15 = get_bin_edges(save_name_append, galaxy_name='V200-200kps_c_15')[2:4]
    # bin_edges = [*bin_edges_5, *bin_edges_15]
    # bin_edges = np.ravel([get_bin_edges('diff', galaxy=Galaxy(conc=c))[2:4] for c in concs])
    bin_edges = np.ravel([[Galaxy(conc=c).get_Rd()]*2 for c in concs])

    #calc
    (v_on_sigma, kappa_rot, spheroid_to_total) = calc_indicators(N200s, bin_edges, time, ic_heat_fraction, concs)

    for k in range(len(widths)):
        ax[k][0].fill_between(log_N200s, spheroid_to_total[0, :], spheroid_to_total[1, :], color='k', alpha=0.3)

    lw = 4

########################################################################################################################
    # radii
    time = np.array([max_T])

    ic_heat_fraction = np.array([0.0001])

    r_half = get_bin_edges('cumulative')[3]
    # bin_edges = np.array([r_half / 4, r_half / 4, r_half, r_half, r_half * 4, r_half * 4])
    bin_edges = np.ravel(([Galaxy().get_Rd() / 4]*2, [Galaxy().get_Rd()]*2, [Galaxy().get_Rd() * 4]*2))

    concs = np.array([10])

    #calc
    (v_on_sigma, kappa_rot, spheroid_to_total) = calc_indicators(N200s, bin_edges, time, ic_heat_fraction, concs)

    #plot
    ax[0][0].plot(log_N200s, spheroid_to_total[0], c='C0', ls='--', lw=lw,
                  label=r'$0.25 \, R_{1/2}$')
    ax[0][0].plot(log_N200s, spheroid_to_total[1], c='k', ls='-', lw=lw,
                  label=r'$R_{1/2}$')
    ax[0][0].plot(log_N200s, spheroid_to_total[2], c='C3', ls='-.', lw=lw,
                  label=r'$4 \, R_{1/2}$')

########################################################################################################################
    #times

    time = np.array([2, 5, 10, max_T])

    ic_heat_fraction = np.array([0.0001])

    # bin_edges = get_bin_edges('diff')[2:4]
    bin_edges = np.array([Galaxy().get_Rd()]*2)

    concs = np.array([10])

    #calc
    (v_on_sigma, kappa_rot, spheroid_to_total) = calc_indicators(N200s, bin_edges, time, ic_heat_fraction, concs)

    #plot
    ax[1][0].plot(log_N200s, spheroid_to_total[0], c='C2', ls='-.', lw=lw,
                  label=r'$t = 2$ Gyr')
    ax[1][0].plot(log_N200s, spheroid_to_total[1], c='C1', ls='--', lw=lw,
                  label=r'$t = 5$ Gyr')
    ax[1][0].plot(log_N200s, spheroid_to_total[3], c='k', ls='-', lw=lw,
                  label=r'$t =$' + str(max_T) + ' Gyr')

########################################################################################################################

    # ax[0][1].plot([2.42, 2.87], [0.705] * 2, lw=lw, c='k', ls='-')

    # 'lower right'
    ax[0][0].legend(labelspacing=0.3, handlelength=2, borderpad=0.3, handletextpad=0.3,
                    loc='lower left', frameon=False)
    ax[1][0].legend(labelspacing=0.3, handlelength=2, borderpad=0.3, handletextpad=0.2,
                    loc='lower left', frameon=False)

    # axtwin[0].set_xlabel(r'$\log ( \, $M$_{200} / $M$_\odot)$', labelpad=12)
    axtwin[0].set_xlabel(r'$\log ( \, $M$_{200} / $M$_\odot)$ $[\,$for $\log ( \, m_{\rm DM} / $M$_\odot) = 6\,]$', labelpad=12)
    axtwin[1].set_xlabel(r'$\log ( \, $M$_{200} / $M$_\odot)$ $[\,$for $\log ( \, m_{\rm DM} / $M$_\odot) = 6\,]$', labelpad=12)

    ax[0][0].set_ylabel(r'S/T')

    ax[0][0].text(N200_max - 0.2, 0.97, r'$R_{1/2}=0.2 \, r_{-2}$', ha='right', va='top')
    ax[0][0].text(N200_max - 0.2, 0.75,  r'$\sigma_{i, 0} = 0$', ha='right', va='top')
    ax[0][0].text(N200_max - 0.2, 0.86, r'$t = $' + str(max_T) + 'Gyr', ha='right', va='top')

    ax[1][0].text(N200_max - 0.2, 0.97, r'$\sigma_{i, 0} = 0$', ha='right', va='top')
    ax[1][0].text(N200_max - 0.2, 0.86, r'at $R_{1/2} = 0.2 \, r_{-2}$', ha='right', va='top')

    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return


def plot_indicator_n_dependence_bary_frac_experiment(save=False, name=''):

    max_T = 13.8 #Gyr

    N200_max = 7.2
    N200_min = 2.2
    N200s = np.logspace(N200_min, N200_max, 101)
    log_N200s = np.log10(N200s)

    m_DM_EAGLE = 1e6 #9.70e6 #M_sun
    M200_EAGLE = m_DM_EAGLE * N200s
    log_M200_EAGLE = np.log10(M200_EAGLE)
    M200_max = np.log10(np.amax(M200_EAGLE))
    M200_min = np.log10(np.amin(M200_EAGLE))

    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(22/3, 15, forward=True)
    widths = [1]
    heights = [1, 1, 1]
    spec = fig.add_gridspec(ncols=1, nrows=3, width_ratios=widths, height_ratios=heights)

    ax = []
    for col in range(len(widths)):
        ax.append([])
        for row in range(len(heights)):
            ax[col].append(fig.add_subplot(spec[row, col]))

            if row == 0: ax[col][row].semilogy()

            ax[col][row].set_xlim([N200_min, N200_max])

            if row == 1: ax[col][row].set_yticks([2/6, 3/6, 4/6, 5/6, 6/6])
            if row == 1 and col == 0: ax[col][row].set_yticklabels(['0.33', '0.5', '0.67', '0.83', '1.0'])

            if row != len(heights) - 1: ax[col][row].set_xticklabels([])
            if col != 0: ax[col][row].set_yticklabels([])

    # ax[1][0].axes.get_yaxis().set_visible(False)
    # ax[2][0].axes.get_yaxis().set_visible(False)

    fig.subplots_adjust(hspace=0.016, wspace=0.01)

    smol = 3e-2
    axtwin = []
    vons_range = [0.033, 30]
    for col in range(len(widths)):
        ax[col][0].semilogy()
        ax[col][0].set_ylim(vons_range)
        ax[col][1].set_ylim([1 / 3 - smol, 1 + smol])
        ax[col][2].set_ylim([0 - smol, 1 + smol])

        ax[col][0].axhline(1, 0, 1, c='grey', ls=':')
        ax[col][1].axhline(0.5, 0, 1, c='grey', ls=':')
        ax[col][1].axhline(1, 0, 1, c='grey', ls=':')
        ax[col][1].axhline(1/3, 0, 1, c='grey', ls=':')
        ax[col][2].axhline(0.5, 0, 1, c='grey', ls=':')
        ax[col][2].axhline(1, 0, 1, c='grey', ls=':')
        ax[col][2].axhline(0, 0, 1, c='grey', ls=':')

        axtwin.append(ax[col][0].twiny())
        axtwin[col].set_xlim([M200_min, M200_max])

        # ax[col][2].set_xlabel(r'$\log \, \mathrm{N}_{\mathrm{DM}}$')
        ax[col][2].set_xlabel(r'$\log \, \mathrm{N}_{200}$')

########################################################################################################################
    #concentrations
    time = np.array([max_T])

    ic_heat_fraction = np.array([0.0001])

    concs = np.array([5, 15])

    # bin_edges_5  = get_bin_edges(save_name_append, galaxy_name='V200-200kps_c_5')[2:4]
    # bin_edges_15 = get_bin_edges(save_name_append, galaxy_name='V200-200kps_c_15')[2:4]
    # bin_edges = [*bin_edges_5, *bin_edges_15]
    # bin_edges = np.ravel([get_bin_edges('diff', galaxy=Galaxy(conc=c))[2:4] for c in concs])
    bin_edges = np.ravel([[Galaxy(conc=c).get_Rd()]*2 for c in concs])

    #calc
    (v_on_sigma, kappa_rot, spheroid_to_total) = calc_indicators(N200s, bin_edges, time, ic_heat_fraction, concs)

    for k in range(len(widths)):
        ax[k][0].fill_between(log_N200s, v_on_sigma[0, :], v_on_sigma[1, :], color='k', alpha=0.3)
        if k == 0:
            ax[k][1].fill_between(log_N200s, kappa_rot[0, :], kappa_rot[1, :], color='k', alpha=0.3,
                                  label=r'$c = 10 \pm 5$')
        else:
            ax[k][1].fill_between(log_N200s, kappa_rot[0, :], kappa_rot[1, :], color='k', alpha=0.3)

        ax[k][2].fill_between(log_N200s, spheroid_to_total[0, :], spheroid_to_total[1, :], color='k', alpha=0.3)

########################################################################################################################
    time = np.array([max_T])

    # bin_edges = get_bin_edges('diff')[2:4]
    bin_edges = np.array([Galaxy().get_Rd()]*2)

    concs = np.array([10])

    ic_heat_fraction = np.array([0.0001])

    bary_f = np.array([0, 0.01, 0.03, 0.05, 0.10])

    #calc
    (v_on_sigma, kappa_rot, spheroid_to_total) = calc_indicators(N200s, bin_edges, time, ic_heat_fraction, concs,
                                                                 bary_f)

    #plot
    # bary_fraction_labels = [r'$M_\star / M_{\rm total}$ = 0',
    #                         r'$M_\star / M_{\rm total}$ = 0.01',
    #                         r'$M_\star / M_{\rm total}$ = 0.03',
    #                         r'$M_\star / M_{\rm total}$ = 0.05',
    #                         r'$M_\star / M_{\rm total}$ = 0.10',]
    bary_fraction_labels = [r'$f_\star$ = 0',
                            r'$f_\star$ = 0.01',
                            r'$f_\star$ = 0.03',
                            r'$f_\star$ = 0.05',
                            r'$f_\star$ = 0.10',]

    lw = 3
    for j,ls in zip(range(len(bary_f)), ['-', '-.', '--', ':', (0, (3, 1, 1, 1, 1, 1))]):
        if j == 0: c = 'k'
        else: c = 'C' + str(j-1)

        ax[0][0].plot(log_N200s, v_on_sigma[j], c=c, ls=ls, lw=lw)
        ax[0][1].plot(log_N200s, kappa_rot[j], c=c, ls=ls, lw=lw, label=bary_fraction_labels[j])
        ax[0][2].plot(log_N200s, spheroid_to_total[j], c=c, ls=ls, lw=lw)

########################################################################################################################

    # # 'lower right'
    ax[0][1].legend(labelspacing=0.2, handlelength=2, borderpad=0.3,
                    loc='upper left', frameon=False)
    # ax[1][1].legend(labelspacing=0.2, handlelength=2, borderpad=0.3,
    #                 loc='upper left', frameon=False)
    # ax[2][1].legend(labelspacing=0.2, handlelength=2, borderpad=0.3,
    #                 loc='upper left', frameon=False)

    # axtwin[1].set_xlabel(r'$\log M_{200} / $M$_\odot$ at EAGLE resolution')
    axtwin[0].set_xlabel(r'$\log ( \, M_{200} / $M$_\odot)$ for DM particle mass $\log ( \, m_{\rm DM} / $M$_\odot) = 6$') #$m_{\rm DM} = 10^6$M$_\odot$')

    ax[0][0].set_ylabel(r'$\overline{v}_\phi / \sigma_{\mathrm{1D}}$')
    ax[0][1].set_ylabel(r'$\kappa_{\mathrm{rot}}$')
    ax[0][2].set_ylabel(r'S/T')

    ax[0][0].text(N200_min + 0.2, vons_range[1] - 6, r'$\Delta t = $' + str(max_T) + 'Gyr', ha='left', va='top')
    ax[0][0].text(N200_min + 0.2, vons_range[1] - 18, r'at $R_d=0.2 \, r_{-2}$', ha='left', va='top')

    # ax[1][0].text(N200_min + 0.2, vons_range[1] - 6, r'$\Delta t = $' + str(max_T) + 'Gyr', ha='left', va='top')
    # ax[1][0].text(N200_min + 0.2, vons_range[1] - 18, r'$\sigma_{i, 0} = 0$', ha='left', va='top')
    #
    # ax[2][0].text(N200_min + 0.2, vons_range[1] - 6, r'$\sigma_{i, 0} = 0$', ha='left', va='top')
    # ax[2][0].text(N200_min + 0.2, vons_range[1] - 18, r'$R = 0.2 \, r_{-2}$', ha='left', va='top')

    # ax[0][2].text(N200_max - 0.2, 0.97, r'$\Delta t = $' + str(max_T) + 'Gyr', ha='right', va='top')
    # ax[0][2].text(N200_max - 0.2, 0.86, r'at $R = R_{1/2}$', ha='right', va='top')
    #
    # ax[1][2].text(N200_max - 0.2, 0.97, r'$\Delta t = $' + str(max_T) + 'Gyr', ha='right', va='top')
    # ax[1][2].text(N200_max - 0.2, 0.86, r'$\sigma_{\rm 1D}^2 = 0$', ha='right', va='top')
    #
    # ax[2][2].text(N200_max - 0.2, 0.97, r'$\sigma_{\rm 1D}^2 = 0$', ha='right', va='top')
    # ax[2][2].text(N200_max - 0.2, 0.86, r'$R = R_{1/2}$', ha='right', va='top')

    # for i in range(3):
    #     for j in range(3):
    #         ax[i][j].axhline(4, 0, 1)
    #         ax[i][j].axvline(np.log10(1_125_000), 0, 1)
    #         ax[i][j].axvline(np.log10(272_500), 0, 1)
    #         ax[i][j].axvline(np.log10(126_400), 0, 1)

    if save:
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return


########################################################################################################################


def calculate_everything(N200=1e6, v200_kmps=200, concentraion=10, disk_scale_radius_kpc=4,
                         radius_of_interest_kpc=6.713, time_Gyr=13.8,
                         ic_heat_fraction=0.0001, cona=True,
                         m_dm=None):
    '''Describe parameters
    '''
    round_places = 6

    # get theory
    (ln_Lambda_ks, alphas, betas, gammas
     ) = find_least_log_squares_best_fit()

    galaxy = Galaxy(v200=v200_kmps, conc=concentraion, Rd=disk_scale_radius_kpc)

    v200 = galaxy.get_v200()

    bin_edges = get_dex_bins([radius_of_interest_kpc])

    if N200 is None:
        N200 = galaxy.M200 / m_dm

    m_dm = M200_for_v200 / N200

    # work out constatns
    (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
     ) = get_constants(bin_edges, 'diff', m_dm, galaxy=galaxy)

    analytic_dispersion = get_velocity_scale(bin_edges, analytic_dispersion=True,
                                             save_name_append='diff', galaxy=galaxy)
    analytic_v_circ = get_velocity_scale(bin_edges, analytic_v_c=True,
                                         save_name_append='diff', galaxy=galaxy)

    inital_velocity_v = np.sqrt((1 - ic_heat_fraction) * analytic_v_circ ** 2)
    inital_velocity2_z = ic_heat_fraction * analytic_dispersion ** 2
    inital_velocity2_r = ic_heat_fraction * analytic_dispersion ** 2
    inital_velocity2_p = ic_heat_fraction * analytic_dispersion ** 2

    theory_best_v_ = get_theory_velocity2_array(v200, analytic_v_circ[0] - inital_velocity_v[0],
                                                time_Gyr, t_c_dm[0], delta_dm[0], np.sqrt(upsilon_circ[0]),
                                                ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
    theory_best_v = analytic_v_circ - theory_best_v_

    theory_best_z2 = get_theory_velocity2_array(v200 ** 2, inital_velocity2_z[0],
                                                time_Gyr, t_c_dm[0], delta_dm[0], upsilon_dm[0],
                                                ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
    theory_best_r2 = get_theory_velocity2_array(v200 ** 2, inital_velocity2_r[0],
                                                time_Gyr, t_c_dm[0], delta_dm[0], upsilon_dm[0],
                                                ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
    theory_best_p2 = get_theory_velocity2_array(v200 ** 2, inital_velocity2_p[0],
                                                time_Gyr, t_c_dm[0], delta_dm[0], upsilon_dm[0],
                                                ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

    tau_heat_z2 = get_tau_heat_array(delta_dm[0], upsilon_dm[0],
                                        ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
    tau_heat_r2 = get_tau_heat_array(delta_dm[0], upsilon_dm[0],
                                        ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
    tau_heat_p2 = get_tau_heat_array(delta_dm[0], upsilon_dm[0],
                                        ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

    # tau_heat_experiment[0] = ((tau_heat_z2[0] + tau_heat_r2[0] + tau_heat_p2[0]
    #                     ) * np.sqrt(upsilon_dm[0]) / np.sqrt(upsilon_circ[0]) / 3) #
    # tau_heat_experiment = (tau_heat_r2 * 1.06126839)
    tau_heat_experiment = (tau_heat_p2 * 0.74987599)

    tau_0_on_tau_heat = get_tau_zero_array(v200, analytic_v_circ[0] - inital_velocity_v[0],
                                           np.sqrt(upsilon_circ[0]))

    experiment_v_ = v200 * np.sqrt(upsilon_circ[0]) ** 2 * (1 - np.exp(
        - np.power(((time_Gyr / t_c_dm[0]) / tau_heat_experiment) + tau_0_on_tau_heat, 1 + gammas[0])))

    theory_best_v = analytic_v_circ - experiment_v_


    # model indicators
    theory_best_tot2 = theory_best_z2 + theory_best_r2 + theory_best_p2
    v_on_sigma = theory_best_v / np.sqrt(theory_best_tot2 / 3)

    lambda_r = theory_best_v / np.sqrt(theory_best_v**2 + theory_best_tot2/3)

    #theory_best_p2 can be < theory_best_tot2 / 3 when theory_best_v approx 0, so kappa_rot can be < 1/3
    kappa_rot = (theory_best_v ** 2 + theory_best_p2) / (
                 theory_best_v ** 2 + theory_best_tot2)

    #kappa_co
    v_max = analytic_v_circ * 10
    v_min = 0  # -1000
    v = theory_best_v
    s = np.sqrt(theory_best_p2)

    v2_int = lambda x: x ** 2 * norm.pdf(x, loc=v, scale=s)
    v2_out = quad(v2_int, v_min, v_max)[0]

    # m_frac_out = 1-scipy.stats.norm.cdf(0, loc=v, scale=s)

    area_out = 1
    rms2_positive_v_phi = v2_out / area_out  # * m_frac_out

    kappa_co = rms2_positive_v_phi / (theory_best_v ** 2 + theory_best_tot2)

    # spheroid_to_total = 2 * scipy.stats.norm.cdf(0, loc=theory_best_v, scale=np.sqrt(theory_best_p2))
    spheroid_to_total = 1 + erf(-theory_best_v / (np.sqrt(theory_best_p2* 2)))
    disc_to_total = 1/2 * (1 + erf((theory_best_v - 0.7 * analytic_v_circ) / (np.sqrt(theory_best_p2 * 2))))

    r200 = galaxy.get_r200()
    (a_h, _) = galaxy.get_hernquist_params()
    (M_disk, _) = galaxy.get_exponential_disk_params()
    Sigma_R_star = get_exponential_surface_density(radius_of_interest_kpc, M_disk, disk_scale_radius_kpc)

    z_half = get_disk_height(np.sqrt(theory_best_z2), radius_of_interest_kpc,
                             Sigma_R_star, analytic_v_circ, v200_kmps, r200, a_h)

    ca = np.nan
    if cona:
        R_disk = galaxy.Rd
        # TODO is there a better solution?
        M_disk = 0.01 * galaxy.M200

        v_c_halo = analytic_v_circ[0]

        R = radius_of_interest_kpc

        Sigma_R_star = get_exponential_surface_density(R, M_disk, R_disk, save_name_append='diff')

        z_thin_SG = get_thin_disk_SG_height(np.sqrt(theory_best_z2), R, Sigma_R_star, v_c_halo, v200, r200, a_h)
        z_thin_NSG = get_thin_disk_NSG_height(np.sqrt(theory_best_z2), R, Sigma_R_star, v_c_halo, v200, r200, a_h)
        # z_thin_half = get_thin_disk_height(np.sqrt(theory_best_z2[i, k]), R, Sigma_R_star, v_c_halo, v200, r200, a_h)

        z_thick_NSG = get_thick_disk_NSG_height(np.sqrt(theory_best_z2), R, Sigma_R_star, v_c_halo, v200, r200,
                                                a_h)
        # z_thick_half = get_disk_height(np.sqrt(theory_best_z2[i, k]), R, Sigma_R_star, v_c_halo, v200, r200, a_h)

        # Round 2
        c_SG = 0.6159978007462151
        c_NSG = 0.5180308801010115

        c_on_a_thin_SG = z_thin_SG / (R / np.sqrt(2)) * 2 * c_SG / np.log(3)
        c_on_a_thin_NSG = z_thin_NSG / (R / np.sqrt(2)) * c_NSG / erfinv(0.5)

        # c_on_a_thick_NSG = np.zeros(len(theory_best_z2[i, k]))
        # for i in range(len(theory_best_z2)):
        c_on_a_thick_NSG = 0

        thick = lambda z: np.exp(1 / (theory_best_z2 / v200 ** 2) * (
                1 / (np.sqrt((R / r200) ** 2 + (z / r200) ** 2) + a_h / r200) - 1 / (R / r200 + a_h / r200)))
        # TODO need to work out what this is for NFW
        # thick = lambda z: np.exp(1 / (theory_best_z2[i, k] / v200 ** 2) * (
        #         1 / (np.sqrt((R / r200) ** 2 + (z / r200) ** 2) + a_h / r200) - 1 / (R / r200 + a_h / r200)))

        z_distribution = thick
        c_2 = z_thick_NSG ** 2
        for _ in range(10):  # 2000):
            top_i = lambda z: 1 / (1 + z ** 2 / (2 * c_2)) * z ** 2 * z_distribution(z)
            bot_i = lambda z: 1 / (1 + z ** 2 / (2 * c_2)) * z_distribution(z)

            top = quad(top_i, 0, 1 * r200)[0]
            bot = quad(bot_i, 0, 1 * r200)[0]

            # c_2 = top / bot
            c_2 = np.divide(top, bot)

            # print(c_2)
            c_on_a_thick_NSG = (np.sqrt(c_2) / (R / np.sqrt(2)))

        c_on_a_thin_half = np.sqrt(
            1 / (1 / c_on_a_thin_SG ** 2 + 1 / (2 * c_on_a_thin_SG * c_on_a_thin_NSG) + 1 / c_on_a_thin_NSG ** 2))

        c_on_a_thick_half = c_on_a_thin_half + (c_on_a_thick_NSG - c_on_a_thin_NSG)

        ca = c_on_a_thick_half

    out = f'''Model galaxy:

N_DM           = {N200}
M200           = {round(galaxy.M200, round_places)}
V200           = {v200_kmps} km/s
cNFW           = {concentraion}
R_d            = {disk_scale_radius_kpc} kpc

sigma_1D (R)   = {round(np.sqrt(ic_heat_fraction), round_places)} sigma_DM = {round(np.sqrt(inital_velocity2_z)[0], round_places)} km/s
mean_v_phi (R) = {round(np.sqrt(1-ic_heat_fraction), round_places)} V_c      = {round(inital_velocity_v[0], round_places)} km/s

at R = {radius_of_interest_kpc} kpc = {radius_of_interest_kpc / disk_scale_radius_kpc} R_d = {radius_of_interest_kpc / (r200 / concentraion)} R_s
after T = {time_Gyr} Gyr


Prediction:

mean_v_phi (R) = {round(theory_best_v[0] / analytic_v_circ[0], round_places)} V_c      = {round(theory_best_v[0], round_places)} km/s
sigma_z (R)    = {round(np.sqrt(theory_best_z2) / analytic_dispersion[0], round_places)} sigma_DM = {round(np.sqrt(theory_best_z2), round_places)} km/s 
sigma_R (R)    = {round(np.sqrt(theory_best_r2) / analytic_dispersion[0], round_places)} sigma_DM = {round(np.sqrt(theory_best_r2), round_places)} km/s
sigma_phi (R)  = {round(np.sqrt(theory_best_p2) / analytic_dispersion[0], round_places)} sigma_DM = {round(np.sqrt(theory_best_p2), round_places)} km/s

v/sigma (R)   = {round(v_on_sigma[0], round_places)}
lambda_r (R)  = {round(lambda_r[0], round_places)}
kappa_rot (R) = {round(kappa_rot[0], round_places)}
kappa_co (R)  = {round(kappa_co[0], round_places)}
D/T (R)       = {round(disc_to_total[0], round_places)}
S/T (R)       = {round(spheroid_to_total[0], round_places)}
c/a (R)       = {round(ca, round_places)}
|z|_1/2 (R)   = {round(z_half[0], round_places)} kpc = {round(z_half[0] / disk_scale_radius_kpc, round_places)} R_d = {round(z_half[0] / radius_of_interest_kpc, round_places)} radius of interest
'''
    print(out)

    return


def find_N(delta_ST=0.5, delta_kappa=None, v_sigma=None,
           v200_kmps=200, concentraion=10, disk_scale_radius_kpc=4,
           radius_of_interest_kpc=6.713, time_Gyr=13.8,
           ic_heat_fraction=0.0001):

    if delta_ST != None:
        _index = 0
        delta_kappa = 0
        v_sigma = 0
    elif delta_kappa != None:
        _index = 1
        delta_ST = 0
        v_sigma = 0
    elif v_sigma != None:
        _index = 2
        delta_kappa = 0
        delta_ST = 0
    else:
        raise ValueError('Please choose either a Delta S/T or Delta kappa_rot to velaulate N200 for.')

    # get theory
    (ln_Lambda_ks, alphas, betas, gammas
     ) = find_least_log_squares_best_fit()

    galaxy = Galaxy(v200=v200_kmps, conc=concentraion, Rd=disk_scale_radius_kpc)

    v200 = galaxy.get_v200()

    bin_edges = get_dex_bins([radius_of_interest_kpc])

    analytic_dispersion = get_velocity_scale(bin_edges, analytic_dispersion=True,
                                             save_name_append='diff', galaxy=galaxy)
    analytic_v_circ = get_velocity_scale(bin_edges, analytic_v_c=True,
                                         save_name_append='diff', galaxy=galaxy)

    inital_velocity_v = np.sqrt((1 - ic_heat_fraction) * analytic_v_circ ** 2)
    inital_velocity2_z = ic_heat_fraction * analytic_dispersion ** 2
    inital_velocity2_r = ic_heat_fraction * analytic_dispersion ** 2
    inital_velocity2_p = ic_heat_fraction * analytic_dispersion ** 2

    # model indicators
    inital_velocity2_tot = inital_velocity2_z + inital_velocity2_r + inital_velocity2_p
    # v_on_sigma = theory_best_v / np.sqrt(theory_best_tot2 / 3)

    initial_kappa_rot = (inital_velocity_v ** 2 + inital_velocity2_p) / (
            inital_velocity_v ** 2 + inital_velocity2_tot)

    initial_spheroid_to_total = 1 + erf(-inital_velocity_v / (np.sqrt(inital_velocity2_p * 2)))

    def to_min(N200):

        m_dm = M200_for_v200 / N200

        # work out constatns
        (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
         ) = get_constants(bin_edges, 'diff', m_dm, galaxy=galaxy)


        theory_best_v_ = get_theory_velocity2_array(v200, analytic_v_circ[0] - inital_velocity_v[0],
                                                    time_Gyr, t_c_dm[0], delta_dm[0], np.sqrt(upsilon_circ[0]),
                                                    ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
        theory_best_v = analytic_v_circ - theory_best_v_

        theory_best_z2 = get_theory_velocity2_array(v200 ** 2, inital_velocity2_z[0],
                                                    time_Gyr, t_c_dm[0], delta_dm[0], upsilon_dm[0],
                                                    ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
        theory_best_r2 = get_theory_velocity2_array(v200 ** 2, inital_velocity2_r[0],
                                                    time_Gyr, t_c_dm[0], delta_dm[0], upsilon_dm[0],
                                                    ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
        theory_best_p2 = get_theory_velocity2_array(v200 ** 2, inital_velocity2_p[0],
                                                    time_Gyr, t_c_dm[0], delta_dm[0], upsilon_dm[0],
                                                    ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

        #
        tau_heat_z2 = get_tau_heat_array(delta_dm[0], upsilon_dm[0],
                                         ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
        tau_heat_r2 = get_tau_heat_array(delta_dm[0], upsilon_dm[0],
                                         ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
        tau_heat_p2 = get_tau_heat_array(delta_dm[0], upsilon_dm[0],
                                         ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

        # tau_heat_experiment[0] = ((tau_heat_z2[0] + tau_heat_r2[0] + tau_heat_p2[0]
        #                     ) * np.sqrt(upsilon_dm[0]) / np.sqrt(upsilon_circ[0]) / 3) #
        # tau_heat_experiment = (tau_heat_r2 * 1.06126839)
        tau_heat_experiment = (tau_heat_p2 * 0.74987599)

        tau_0_on_tau_heat = get_tau_zero_array(v200, analytic_v_circ[0] - inital_velocity_v[0],
                                               np.sqrt(upsilon_circ[0]))

        experiment_v_ = v200 * np.sqrt(upsilon_circ[0]) ** 2 * (1 - np.exp(
            - np.power(((time_Gyr / t_c_dm[0]) / tau_heat_experiment) + tau_0_on_tau_heat, 1 + gammas[0])))

        theory_best_v = analytic_v_circ - experiment_v_

        # model indicators
        theory_best_tot2 = theory_best_z2 + theory_best_r2 + theory_best_p2
        v_on_sigma = theory_best_v / np.sqrt(theory_best_tot2 / 3)

        #theory_best_p2 can be < theory_best_tot2 / 3 when theory_best_v approx 0, so kappa_rot can be < 1/3
        kappa_rot = (theory_best_v ** 2 + theory_best_p2) / (
                     theory_best_v ** 2 + theory_best_tot2)

        # spheroid_to_total = 2 * scipy.stats.norm.cdf(0, loc=theory_best_v, scale=np.sqrt(theory_best_p2))
        spheroid_to_total = 1 + erf(-theory_best_v / (np.sqrt(theory_best_p2* 2)))

        return([(spheroid_to_total - initial_spheroid_to_total) - delta_ST,
                (initial_kappa_rot - kappa_rot) - delta_kappa,
                (v_on_sigma - v_sigma)][_index][0])

########################################################################################################################
    N200_required = brentq(to_min, 1e-1, 1e15)

    m_dm = M200_for_v200 / N200_required

    # work out constatns
    (t_c_dm, t_c_circ, delta_dm, upsilon_dm, upsilon_circ
     ) = get_constants(bin_edges, 'diff', m_dm, galaxy=galaxy)

    theory_best_v_ = get_theory_velocity2_array(v200, analytic_v_circ[0] - inital_velocity_v[0],
                                                time_Gyr, t_c_dm[0], delta_dm[0], np.sqrt(upsilon_circ[0]),
                                                ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
    theory_best_v = analytic_v_circ - theory_best_v_

    theory_best_z2 = get_theory_velocity2_array(v200 ** 2, inital_velocity2_z[0],
                                                time_Gyr, t_c_dm[0], delta_dm[0], upsilon_dm[0],
                                                ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
    theory_best_r2 = get_theory_velocity2_array(v200 ** 2, inital_velocity2_r[0],
                                                time_Gyr, t_c_dm[0], delta_dm[0], upsilon_dm[0],
                                                ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
    theory_best_p2 = get_theory_velocity2_array(v200 ** 2, inital_velocity2_p[0],
                                                time_Gyr, t_c_dm[0], delta_dm[0], upsilon_dm[0],
                                                ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

    tau_heat_z2 = get_tau_heat_array(delta_dm[0], upsilon_dm[0],
                                     ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
    tau_heat_r2 = get_tau_heat_array(delta_dm[0], upsilon_dm[0],
                                     ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
    tau_heat_p2 = get_tau_heat_array(delta_dm[0], upsilon_dm[0],
                                     ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

    # tau_heat_experiment[0] = ((tau_heat_z2[0] + tau_heat_r2[0] + tau_heat_p2[0]
    #                     ) * np.sqrt(upsilon_dm[0]) / np.sqrt(upsilon_circ[0]) / 3) #
    # tau_heat_experiment = (tau_heat_r2 * 1.06126839)
    tau_heat_experiment = (tau_heat_p2 * 0.74987599)

    tau_0_on_tau_heat = get_tau_zero_array(v200, analytic_v_circ[0] - inital_velocity_v[0],
                                           np.sqrt(upsilon_circ[0]))

    experiment_v_ = v200 * np.sqrt(upsilon_circ[0]) ** 2 * (1 - np.exp(
        - np.power(((time_Gyr / t_c_dm[0]) / tau_heat_experiment) + tau_0_on_tau_heat, 1 + gammas[0])))

    theory_best_v = analytic_v_circ - experiment_v_

    # model indicators
    theory_best_tot2 = theory_best_z2 + theory_best_r2 + theory_best_p2
    v_on_sigma = theory_best_v / np.sqrt(theory_best_tot2 / 3)

    # theory_best_p2 can be < theory_best_tot2 / 3 when theory_best_v approx 0, so kappa_rot can be < 1/3
    kappa_rot = (theory_best_v ** 2 + theory_best_p2) / (
            theory_best_v ** 2 + theory_best_tot2)

    # spheroid_to_total = 2 * scipy.stats.norm.cdf(0, loc=theory_best_v, scale=np.sqrt(theory_best_p2))
    spheroid_to_total = 1 + erf(-theory_best_v / (np.sqrt(theory_best_p2 * 2)))

    r200 = galaxy.get_r200()
    (a_h, _) = galaxy.get_hernquist_params()
    (M_disk, _) = galaxy.get_exponential_disk_params()
    Sigma_R_star = get_exponential_surface_density(radius_of_interest_kpc, M_disk, disk_scale_radius_kpc)

    z_half = get_disk_height(np.sqrt(theory_best_z2), radius_of_interest_kpc,
                             Sigma_R_star, analytic_v_circ, v200_kmps, r200, a_h)


    out='''N200            = {0} 
log10 N200      = {1}
Delta S/T       = {2}
Delta kappa_rot = {3}
v/sigma         = {4}
z_1/2 / R       = {5}
'''.format(round(N200_required, 0),
           round(np.log10(N200_required), 5),
           round(spheroid_to_total[0] - initial_spheroid_to_total[0], 5),
           round(initial_kappa_rot[0] - kappa_rot[0], 5),
           round(v_on_sigma[0], 5),
           round(z_half[0] / radius_of_interest_kpc, 5))
    print(out)

    return (round(spheroid_to_total[0] - initial_spheroid_to_total[0], 4),
            round(initial_kappa_rot[0] - kappa_rot[0], 4),
            round(v_on_sigma[0], 4),
            N200_required)


if __name__ == '__main__':
    plot = True

    save = True


    # gal = Galaxy(v200 = 200, conc = 7)
    #
    # print(gal.r_s)
    # print(gal.a_h)
    #
    # print(6.88 / (-lambertw(-0.5/np.exp(1),-1).real -1) / gal.r_s)
    # print(6.88 / gal.r_s)
    #
    # x = np.logspace(-2,2)
    #
    # rho_nfw  = get_analytic_nfw_density(np.repeat(x, 2), gal.r_s, gal.conc, gal.v200, f_bary=0)
    # rho_hern = get_analytic_hernquist_density(np.repeat(x, 2), gal.vmax, gal.a_h)
    #
    # plt.figure()
    #
    # plt.plot(x, rho_nfw)
    # plt.plot(x, rho_hern)
    #
    # plt.loglog()
    #
    # plt.show()


    calculate_everything(N200=10**4.5, v200_kmps=200, concentraion=10,
                         disk_scale_radius_kpc=4, radius_of_interest_kpc=4, time_Gyr=13.8,
                         ic_heat_fraction=0.0001)
    # calculate_everything(N200=10**5.5, v200_kmps=200, concentraion=10,
    #                      disk_scale_radius_kpc=4, radius_of_interest_kpc=4, time_Gyr=13.8,
    #                      ic_heat_fraction=0.0001)

    # calculate_everything(N200=None, m_dm=4.5e-5, v200_kmps=200, concentraion=10,
    #                      disk_scale_radius_kpc=4, radius_of_interest_kpc=4, time_Gyr=1,
    #                      ic_heat_fraction=0.0001)
    # calculate_everything(N200=None, m_dm=9.7e-4, v200_kmps=200, concentraion=10,
    #                      disk_scale_radius_kpc=4, radius_of_interest_kpc=4, time_Gyr=1,
    #                      ic_heat_fraction=0.0001)
    #
    # print('############################\n')
    #
    # find_N(delta_ST=None, delta_kappa=None, v_sigma=4, disk_scale_radius_kpc=4,
    #        radius_of_interest_kpc=4/4, time_Gyr=13.8)
    # find_N(delta_ST=None, delta_kappa=None, v_sigma=4, disk_scale_radius_kpc=4,
    #        radius_of_interest_kpc=4*4, time_Gyr=13.8)
    # find_N(delta_ST=None, delta_kappa=None, v_sigma=4, disk_scale_radius_kpc=4,
    #        radius_of_interest_kpc=4, time_Gyr=13.8)

#######################################################################################################################

    # ST_list  = [None, None, None, None, None, 0.5]
    # kap_list = [0.01, 0.05, 0.1,  0.2,  0.5,  None]
    # radii_list = [6.713, 6.713/4, 6.713*4, 6.713, 6.713, 6.713, 6.713]
    # time_list = [13.8, 13.8, 13.8, 2, 5, 13.8, 13.8]
    # conc_list = [10, 10, 10, 10, 10, 5, 15]
    #
    # for ST, kap in zip(ST_list, kap_list):
    #     STs = []
    #     kaps = []
    #     vss = []
    #     Ns = []
    #
    #     for radii, time, conc in zip(radii_list, time_list, conc_list):
    #         STi, kapi, vsi, Ni = find_N(delta_ST=ST, delta_kappa=kap, concentraion=conc,
    #                                     radius_of_interest_kpc=radii, time_Gyr=time)
    #         STs.append(STi)
    #         kaps.append(kapi)
    #         vss.append(vsi)
    #         Ns.append(Ni)
    #
    #     if round(np.amax(kaps)-np.amin(kaps), 4)/2 > 1e-4:
    #         print(str(round(kaps[0], (np.amax(kaps)-np.amin(kaps))/2, separation='$\pm$', sigfigs=4)), end=' & ')
    #     else:
    #         print(str(round(kaps[0], sigfigs=4)), end=' & ')
    #     if round(np.amax(STs)-np.amin(STs), 4)/2 > 1e-4:
    #         print(str(round(STs[0], (np.amax(STs)-np.amin(STs))/2, separation='$\pm$', sigfigs=4)), end=' & ')
    #     else:
    #         print(str(round(round(STs[0], decimals=4), sigfigs=4)), end=' & ')
    #     if round(np.amax(vss)-np.amin(vss), 4)/2 > 1e-4:
    #         print(str(round(vss[0], (np.amax(vss)-np.amin(vss))/2, separation='$\pm$', sigfigs=4)), end='')
    #     else:
    #         print(str(round(vss[0], sigfigs=4)), end='')
    #
    #     [print(' & ' + '{:,}'.format(int(round(N, sigfigs=4))), end='') for N in Ns]
    #     print(r' \\')

#######################################################################################################################
    #
    if plot:
        save = True

        # plot_indicator_n_dependence(save=save, name='../galaxies/indicator_n_dependancediff.pdf')

        # plot_kin_ind_n_dependence(save=save, name='../galaxies/kin_ind_n_dependance.pdf')
        # plot_other_ind_n_dependence(save=save, name='../galaxies/other_ind_n_dependance.pdf')

        # plot_vel_n_dependence(save=save, name='../galaxies/vel_n_dependance.pdf')

    #     # plot_indicator_n_dependence_bary_frac_experiment(save=save, name='../galaxies/indicator_n_dep_bary_f.pdf')
    #
        # plot_poster_n_dependence(save=save, name='../galaxies/poster_n_dependancediff')
        # plot_other_poster_n_dependence(save=save, name= '../galaxies/poster_vs_n_dependancediff')
        plt.show()

    pass