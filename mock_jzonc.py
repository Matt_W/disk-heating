#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  1 10:12:03 2020

@author: matt
"""
import os
# import time

import numpy as np
# import h5py as h5

from scipy.integrate   import quad

from scipy.interpolate import InterpolatedUnivariateSpline

from scipy.optimize    import minimize
from scipy.optimize    import brentq

from scipy.spatial.transform import Rotation

from scipy.special     import gamma
from scipy.special     import lambertw
from scipy.special     import erf

from scipy.stats       import binned_statistic

#matplotlib
import matplotlib.pyplot as plt
import splotch as splt

from matplotlib.colors import LogNorm

#my files
import halo_calculations as halo
import load_nbody

from matplotlib import rcParams
rcParams['ps.useafm'] = True
rcParams['pdf.use14corefonts'] = False
rcParams['text.usetex'] = False
rcParams['font.sans-serif'] = ['cmr10', 'Times-Roman']
rcParams['font.weight'] = 'normal'
rcParams['font.size'] = 20
rcParams['xtick.labelsize'] = 20
rcParams['ytick.labelsize'] = 20
rcParams['axes.labelsize'] = 22
rcParams['axes.linewidth'] = 1
rcParams['axes.unicode_minus'] = False
rcParams['axes.grid'] = False
rcParams['xtick.minor.width'] = 1
rcParams['ytick.minor.width'] = 1
rcParams['xtick.major.width'] = 1
rcParams['ytick.major.width'] = 1
rcParams['xtick.minor.size'] = 4
rcParams['ytick.minor.size'] = 4
rcParams['xtick.major.size'] = 5
rcParams['ytick.major.size'] = 5
rcParams['xtick.direction'] = 'in'
rcParams['ytick.direction'] = 'in'
rcParams['xtick.top'] = True
rcParams['ytick.right'] = True
rcParams['mathtext.fontset'] = 'cm'

GRAV_CONST = 4.301e4 #kpc (km/s)^2 / (10^10Msun) (source ??)
HUBBLE_CONST = 0.06766 #km/s/kpc (Planck 2018)
RHO_CRIT = 3 * HUBBLE_CONST**2 / (8 * np.pi * GRAV_CONST) #10^10Msun/kpc^3

PC_ON_M = 3.0857e16 #pc/m = kpc/km (wiki)
GYR_ON_S = 3.15576e16 #gyr/s

def plot_morpholocial_corner(name, save=True):
  '''
  '''
  t_tot = 9.778943899409334 #Gyr

  if (('7p5' in name) or ('8p0' in name)):
    nsnaps = 401
  else:
    nsnaps = 102

  if   '8p0' in name: mass_dm = np.power(10.,8   -10)
  elif '7p5' in name: mass_dm = np.power(10.,7.5 -10)
  elif '7p0' in name: mass_dm = np.power(10.,7   -10)
  elif '6p5' in name: mass_dm = np.power(10.,6.5 -10)
  elif '6p0' in name: mass_dm = np.power(10.,6   -10)

  r_25 = 3.98
  r_50 = 6.95
  r_75 = 11.16
  p=0.1
  thin_disk_threshold = 0.7

  mean_elipticity_value = np.zeros(nsnaps)
  thin_disk_fraction    = np.zeros(nsnaps)
  bulge_fraction        = np.zeros(nsnaps)
  kappa_value           = np.zeros(nsnaps)
  kappa_co_value        = np.zeros(nsnaps)
  v_on_sigma_value      = np.zeros(nsnaps)
  half_abs_z_value      = np.zeros(nsnaps)

  sigma_v_R_value       = np.zeros(nsnaps)
  sigma_v_phi_value     = np.zeros(nsnaps)
  sigma_v_tild_value    = np.zeros(nsnaps)
  sigma_v_z_value       = np.zeros(nsnaps)
  sigma_v_tot_value     = np.zeros(nsnaps)
  # sigma_v_R_dm         = np.zeros(nsnaps)
  # sigma_v_phi_dm       = np.zeros(nsnaps)
  # sigma_v_z_dm         = np.zeros(nsnaps)
  # sigma_v_tot_dm       = np.zeros(nsnaps)
  mean_v_phi_value      = np.zeros(nsnaps)

  # normed_sigma_v_R_value    = np.zeros(nsnaps)
  # normed_sigma_v_phi_value  = np.zeros(nsnaps)
  # # normed_sigma_v_tild_value = np.zeros(nsnaps)
  # normed_sigma_v_z_value    = np.zeros(nsnaps)
  # normed_sigma_v_tot_value  = np.zeros(nsnaps)
  # normed_mean_v_phi_value   = np.zeros(nsnaps)

  mean_elipticity_value_25 = np.zeros(nsnaps)
  thin_disk_fraction_25    = np.zeros(nsnaps)
  bulge_fraction_25        = np.zeros(nsnaps)
  kappa_value_25           = np.zeros(nsnaps)
  kappa_co_value_25        = np.zeros(nsnaps)
  v_on_sigma_value_25      = np.zeros(nsnaps)
  half_abs_z_value_25      = np.zeros(nsnaps)
  sigma_v_R_value_25       = np.zeros(nsnaps)
  sigma_v_phi_value_25     = np.zeros(nsnaps)
  sigma_v_z_value_25       = np.zeros(nsnaps)
  sigma_v_tot_value_25     = np.zeros(nsnaps)
  mean_v_phi_value_25      = np.zeros(nsnaps)
  # normed_sigma_v_R_value_25   = np.zeros(nsnaps)
  # normed_sigma_v_phi_value_25 = np.zeros(nsnaps)
  # normed_sigma_v_z_value_25   = np.zeros(nsnaps)
  # normed_sigma_v_tot_value_25 = np.zeros(nsnaps)
  # normed_mean_v_phi_value_25  = np.zeros(nsnaps)

  mean_elipticity_value_50 = np.zeros(nsnaps)
  thin_disk_fraction_50    = np.zeros(nsnaps)
  bulge_fraction_50        = np.zeros(nsnaps)
  kappa_value_50           = np.zeros(nsnaps)
  kappa_co_value_50        = np.zeros(nsnaps)
  v_on_sigma_value_50      = np.zeros(nsnaps)
  sigma_v_R_value_50       = np.zeros(nsnaps)
  sigma_v_phi_value_50     = np.zeros(nsnaps)
  sigma_v_z_value_50       = np.zeros(nsnaps)
  sigma_v_tot_value_50     = np.zeros(nsnaps)
  mean_v_phi_value_50      = np.zeros(nsnaps)
  half_abs_z_value_50      = np.zeros(nsnaps)
  # normed_sigma_v_R_value_50   = np.zeros(nsnaps)
  # normed_sigma_v_phi_value_50 = np.zeros(nsnaps)
  # normed_sigma_v_z_value_50   = np.zeros(nsnaps)
  # normed_sigma_v_tot_value_50 = np.zeros(nsnaps)
  # normed_mean_v_phi_value_50  = np.zeros(nsnaps)

  mean_elipticity_value_75 = np.zeros(nsnaps)
  thin_disk_fraction_75    = np.zeros(nsnaps)
  bulge_fraction_75        = np.zeros(nsnaps)
  kappa_value_75           = np.zeros(nsnaps)
  kappa_co_value_75        = np.zeros(nsnaps)
  v_on_sigma_value_75      = np.zeros(nsnaps)
  sigma_v_R_value_75       = np.zeros(nsnaps)
  sigma_v_phi_value_75     = np.zeros(nsnaps)
  sigma_v_z_value_75       = np.zeros(nsnaps)
  sigma_v_tot_value_75     = np.zeros(nsnaps)
  mean_v_phi_value_75      = np.zeros(nsnaps)
  half_abs_z_value_75      = np.zeros(nsnaps)
  # normed_sigma_v_R_value_75   = np.zeros(nsnaps)
  # normed_sigma_v_phi_value_75 = np.zeros(nsnaps)
  # normed_sigma_v_z_value_75   = np.zeros(nsnaps)
  # normed_sigma_v_tot_value_75 = np.zeros(nsnaps)
  # normed_mean_v_phi_value_75  = np.zeros(nsnaps)

  for i in range(nsnaps):
    snap = '{0:03d}'.format(int(i))
    (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,
                MassStar, PosStars, VelStars, IDStars,
      j_z_stars, j_p_stars, j_c_stars, energy_stars,
      E_interp, j_circ_interp, R_interp
      ) = load_nbody.load_and_calculate(name, snap)

    n_stars  = len(IDStars)
    #j_zonc
    j_zonc_stars = j_z_stars / j_c_stars
    #cylindrical coords
    (rho, phi, z, v_rho, v_phi, v_z
     ) = halo.get_cylindrical(PosStars, VelStars)

    r_stars = np.linalg.norm(PosStars, axis=1)

    #add values to arrays
    mean_elipticity_value[i] = np.mean(j_zonc_stars)
    #jzoncs
    thin_disk_fraction[i]    = np.sum(j_zonc_stars > thin_disk_threshold) / n_stars
    bulge_fraction[i]        = 2 * np.sum(j_zonc_stars < 0) / n_stars

    #calculate kappas
    K_tot = np.sum(np.square(np.linalg.norm(VelStars,axis=1)))
    K_rot = np.sum(np.square(v_phi))
    K_co  = np.sum(np.square(v_phi[v_phi > 0]))

    kappa_value[i]    = K_rot / K_tot
    kappa_co_value[i] = K_co  / K_tot

    # v_c_interp = j_circ_interp / R_interp
    # interp = InterpolatedUnivariateSpline(R_interp, v_c_interp,
    #                                       k=1, ext='const')
    # v_c_stars = interp(r_stars)
    # v_tild = v_phi - v_c_stars

    #sigmas
    disp_v_rho = np.std(v_rho)
    disp_v_phi = np.std(v_phi)
    disp_v_z   = np.std(v_z)
    disp_v_tot = np.linalg.norm((disp_v_rho, disp_v_phi, disp_v_z))
    mean_v_phi = np.mean(v_phi)

    sigma_v_R_value[i]   = disp_v_rho
    sigma_v_phi_value[i] = disp_v_phi
    # sigma_v_tild_value[i]= np.std(v_tild)
    sigma_v_z_value[i]   = disp_v_z
    sigma_v_tot_value[i] = disp_v_tot
    mean_v_phi_value[i]  = mean_v_phi

    v_on_sigma_value[i]  = mean_v_phi / disp_v_tot

    half_abs_z_value[i] = np.median(np.abs(z))

    #normed
    # n_disp_v_rho = np.std(v_rho/v_c_stars)
    # n_disp_v_phi = np.std(v_phi/v_c_stars)
    # n_disp_v_z   = np.std(v_z/v_c_stars)
    # n_disp_v_tot = np.linalg.norm((n_disp_v_rho, n_disp_v_phi, n_disp_v_z))
    # n_mean_v_phi = np.mean(v_phi/v_c_stars)

    # normed_sigma_v_R_value[i]   = n_disp_v_rho
    # normed_sigma_v_phi_value[i] = n_disp_v_phi
    # normed_sigma_v_z_value[i]   = n_disp_v_z
    # normed_sigma_v_tot_value[i] = n_disp_v_tot
    # normed_mean_v_phi_value[i]  = n_mean_v_phi

    #r25
    #make mask
    # rmin = (10**np.log10(r_25-p))
    # rmax = (10**np.log10(r_25+p))
    rmin = r_25 / (1+p)
    rmax = r_25 * (1+p)
    rMask25 = np.logical_and((rho > rmin), (rho < rmax))

    n_25 = np.sum(rMask25)
    #add values to arrays
    mean_elipticity_value_25[i] = np.mean(j_zonc_stars[rMask25])
    thin_disk_fraction_25[i]    = np.sum(j_zonc_stars[rMask25]>thin_disk_threshold)/n_25
    bulge_fraction_25[i]        = 2 * np.sum(j_zonc_stars[rMask25] < 0) / n_25

    #calculate kappas
    K_tot = np.sum(np.square(np.linalg.norm(VelStars[rMask25],axis=1)))
    K_rot = np.sum(np.square(v_phi[rMask25]))
    K_co  = np.sum(np.square(v_phi[rMask25][v_phi[rMask25] > 0]))

    kappa_value_25[i]    = K_rot / K_tot
    kappa_co_value_25[i] = K_co  / K_tot

    #sigmas
    disp_v_rho = np.std(v_rho[rMask25])
    disp_v_phi = np.std(v_phi[rMask25])
    disp_v_z   = np.std(v_z[rMask25])
    disp_v_tot = np.linalg.norm((disp_v_rho, disp_v_phi, disp_v_z))
    mean_v_phi = np.mean(v_phi[rMask25])

    sigma_v_R_value_25[i]   = disp_v_rho
    sigma_v_phi_value_25[i] = disp_v_phi
    sigma_v_z_value_25[i]   = disp_v_z
    sigma_v_tot_value_25[i] = disp_v_tot
    mean_v_phi_value_25[i]  = mean_v_phi

    v_on_sigma_value_25[i]  = mean_v_phi / disp_v_tot

    half_abs_z_value_25[i] = np.median(np.abs(z[rMask25]))

    #normed
    # n_disp_v_rho = np.std(v_rho[rMask25]/v_c_stars[rMask25])
    # n_disp_v_phi = np.std(v_phi[rMask25]/v_c_stars[rMask25])
    # n_disp_v_z   = np.std(v_z[rMask25]/v_c_stars[rMask25])
    # n_disp_v_tot = np.linalg.norm((n_disp_v_rho, n_disp_v_phi, n_disp_v_z))
    # n_mean_v_phi = np.mean(v_phi[rMask25]/v_c_stars[rMask25])

    # normed_sigma_v_R_value_25[i]   = n_disp_v_rho
    # normed_sigma_v_phi_value_25[i] = n_disp_v_phi
    # normed_sigma_v_z_value_25[i]   = n_disp_v_z
    # normed_sigma_v_tot_value_25[i] = n_disp_v_tot
    # normed_mean_v_phi_value_25[i]  = n_mean_v_phi

    #r50
    #make mask
    rmin = r_50 / (1+p)
    rmax = r_50 * (1+p)
    rMask50 = np.logical_and((rho > rmin), (rho < rmax))

    n_50 = np.sum(rMask50)
    #add values to arrays
    mean_elipticity_value_50[i] = np.mean(j_zonc_stars[rMask50])
    thin_disk_fraction_50[i]    = np.sum(j_zonc_stars[rMask50]>thin_disk_threshold)/n_50
    bulge_fraction_50[i]        = 2 * np.sum(j_zonc_stars[rMask50] < 0) / n_50

    #calculate kappas
    K_tot = np.sum(np.square(np.linalg.norm(VelStars[rMask50],axis=1)))
    K_rot = np.sum(np.square(v_phi[rMask50]))
    K_co  = np.sum(np.square(v_phi[rMask50][v_phi[rMask50] > 0]))

    kappa_value_50[i]    = K_rot / K_tot
    kappa_co_value_50[i] = K_co  / K_tot

    #sigmas
    disp_v_rho = np.std(v_rho[rMask50])
    disp_v_phi = np.std(v_phi[rMask50])
    disp_v_z   = np.std(v_z[rMask50])
    disp_v_tot = np.linalg.norm((disp_v_rho, disp_v_phi, disp_v_z))
    mean_v_phi = np.mean(v_phi[rMask50])

    sigma_v_R_value_50[i]   = disp_v_rho
    sigma_v_phi_value_50[i] = disp_v_phi
    sigma_v_z_value_50[i]   = disp_v_z
    sigma_v_tot_value_50[i] = disp_v_tot
    mean_v_phi_value_50[i]  = mean_v_phi

    v_on_sigma_value_50[i]  = mean_v_phi / disp_v_tot

    half_abs_z_value_50[i] = np.median(np.abs(z[rMask50]))

    #normed
    # n_disp_v_rho = np.std(v_rho[rMask50]/v_c_stars[rMask50])
    # n_disp_v_phi = np.std(v_phi[rMask50]/v_c_stars[rMask50])
    # n_disp_v_z   = np.std(v_z[rMask50]/v_c_stars[rMask50])
    # n_disp_v_tot = np.linalg.norm((n_disp_v_rho, n_disp_v_phi, n_disp_v_z))
    # n_mean_v_phi = np.mean(v_phi[rMask50]/v_c_stars[rMask50])

    # normed_sigma_v_R_value_50[i]   = n_disp_v_rho
    # normed_sigma_v_phi_value_50[i] = n_disp_v_phi
    # normed_sigma_v_z_value_50[i]   = n_disp_v_z
    # normed_sigma_v_tot_value_50[i] = n_disp_v_tot
    # normed_mean_v_phi_value_50[i]  = n_mean_v_phi

    #r75
    #make mask
    rmin = r_75 / (1+p)
    rmax = r_75 * (1+p)
    rMask75 = np.logical_and((rho > rmin), (rho < rmax))

    n_75 = np.sum(rMask75)
    #add values to arrays
    mean_elipticity_value_75[i] = np.mean(j_zonc_stars[rMask75])
    thin_disk_fraction_75[i]    = np.sum(j_zonc_stars[rMask75]>thin_disk_threshold)/n_75
    bulge_fraction_75[i]        = 2 * np.sum(j_zonc_stars[rMask75] < 0) / n_75

    #calculate kappas
    K_tot = np.sum(np.square(np.linalg.norm(VelStars[rMask75],axis=1)))
    K_rot = np.sum(np.square(v_phi[rMask75]))
    K_co  = np.sum(np.square(v_phi[rMask75][v_phi[rMask75] > 0]))

    kappa_value_75[i]    = K_rot / K_tot
    kappa_co_value_75[i] = K_co  / K_tot

    #sigmas
    disp_v_rho = np.std(v_rho[rMask75])
    disp_v_phi = np.std(v_phi[rMask75])
    disp_v_z   = np.std(v_z[rMask75])
    disp_v_tot = np.linalg.norm((disp_v_rho, disp_v_phi, disp_v_z))
    mean_v_phi = np.mean(v_phi[rMask75])

    sigma_v_R_value_75[i]   = disp_v_rho
    sigma_v_phi_value_75[i] = disp_v_phi
    sigma_v_z_value_75[i]   = disp_v_z
    sigma_v_tot_value_75[i] = disp_v_tot
    mean_v_phi_value_75[i]  = mean_v_phi

    v_on_sigma_value_75[i]  = mean_v_phi / disp_v_tot

    half_abs_z_value_75[i] = np.median(np.abs(z[rMask75]))

    #normed
    # n_disp_v_rho = np.std(v_rho[rMask75]/v_c_stars[rMask75])
    # n_disp_v_phi = np.std(v_phi[rMask75]/v_c_stars[rMask75])
    # n_disp_v_z   = np.std(v_z[rMask75]/v_c_stars[rMask75])
    # n_disp_v_tot = np.linalg.norm((n_disp_v_rho, n_disp_v_phi, n_disp_v_z))
    # n_mean_v_phi = np.mean(v_phi[rMask75]/v_c_stars[rMask75])

    # normed_sigma_v_R_value_75[i]   = n_disp_v_rho
    # normed_sigma_v_phi_value_75[i] = n_disp_v_phi
    # normed_sigma_v_z_value_75[i]   = n_disp_v_z
    # normed_sigma_v_tot_value_75[i] = n_disp_v_tot
    # normed_mean_v_phi_value_75[i]  = n_mean_v_phi

  t = np.linspace(0, t_tot, nsnaps)

  data = np.vstack((t, mean_elipticity_value, thin_disk_fraction, bulge_fraction,
                    kappa_value, kappa_co_value, v_on_sigma_value, half_abs_z_value,
                    sigma_v_R_value, sigma_v_phi_value, sigma_v_z_value,
                    sigma_v_tot_value, mean_v_phi_value)).T

  data_25=np.vstack((t, mean_elipticity_value_25,thin_disk_fraction_25,bulge_fraction_25,
                     kappa_value_25, kappa_co_value_25, v_on_sigma_value_25,
                     half_abs_z_value_25,
                     sigma_v_R_value_25, sigma_v_phi_value_25, sigma_v_z_value_25,
                     sigma_v_tot_value_25, mean_v_phi_value_25)).T

  data_50=np.vstack((t, mean_elipticity_value_50,thin_disk_fraction_50,bulge_fraction_50,
                     kappa_value_50, kappa_co_value_50, v_on_sigma_value_50,
                     half_abs_z_value_50,
                     sigma_v_R_value_50, sigma_v_phi_value_50, sigma_v_z_value_50,
                     sigma_v_tot_value_50, mean_v_phi_value_50)).T

  data_75=np.vstack((t, mean_elipticity_value_75,thin_disk_fraction_75,bulge_fraction_75,
                     kappa_value_75, kappa_co_value_75, v_on_sigma_value_75,
                     half_abs_z_value_75,
                     sigma_v_R_value_75, sigma_v_phi_value_75, sigma_v_z_value_75,
                     sigma_v_tot_value_75, mean_v_phi_value_75)).T

  delta_25 = 0.0086942 / (200 * RHO_CRIT)
  delta_50 = 0.0033329 / (200 * RHO_CRIT)
  delta_75 = 0.00134344 / (200 * RHO_CRIT)

  V200     = 160.6

  v_25     = 159.942 / V200
  v_50     = 161.010 / V200
  v_75     = 157.244 / V200

  #in anuli
  # v_c_25   = 224.794
  # v_c_50   = 254.034
  # v_c_75   = 272.389

  #in sphere
  v_c_25   = 145.55
  v_c_50   = 178.91
  v_c_75   = 205.76

  # print('With beta=0')
  # alpha_z  = 0.6465
  # beta_z   = 0
  # gamma_z  = 0.9956
  # const_z  = 2.015

  # alpha_R  = 0.7346
  # beta_R   = 0
  # gamma_R  = 0.8563
  # const_R  = 1.335

  # alpha_p  = 0.9309
  # beta_p   = 0
  # gamma_p  = 1.0466
  # const_p  = 1.675

  # alpha_m  = 0.5134
  # beta_m   = 0
  # gamma_m  = 0.8226
  # const_m  = 1.9506

  alpha_z  = 0.545155
  beta_z   = 0.082628
  gamma_z  = 1 -0.004426
  const_z  = 2.190704

  alpha_R  = 0.652506
  beta_R   = 1.019760
  gamma_R  = 1 -0.143716
  const_R  = 1.435412

  alpha_p  = 0.820837
  beta_p   = 1.125597
  gamma_p  = 1 +0.046630
  const_p  = 1.820499

  alpha_m  = 0.531549
  beta_m   = 2.878927
  gamma_m  = 1 -0.177431
  const_m  = 1.793714

  t_c = GRAV_CONST**2 * 200 * RHO_CRIT * mass_dm / V200**3 * PC_ON_M / GYR_ON_S

  # sigma_z2_theory_25 = sigma_v_z_value_25[0]**2 + np.power(
  #   t*t_c, gamma_z) * np.power(delta_25, alpha_z) * np.power(
  #     v_25, beta_z) * np.power(10, const_z) * np.square(V200)
  # sigma_z2_theory_50 = sigma_v_z_value_50[0]**2 + np.power(
  #   t*t_c, gamma_z) * np.power(delta_50, alpha_z) * np.power(
  #     v_50, beta_z) * np.power(10, const_z) * np.square(V200)
  # sigma_z2_theory_75 = sigma_v_z_value_75[0]**2 + np.power(
  #   t*t_c, gamma_z) * np.power(delta_75, alpha_z) * np.power(
  #     v_75, beta_z) * np.power(10, const_z) * np.square(V200)

  # sigma_R2_theory_25 = sigma_v_R_value_25[0]**2 + np.power(
  #   t*t_c, gamma_R) * np.power(delta_25, alpha_R) * np.power(
  #     v_25, beta_R) * np.power(10, const_R) * np.square(V200)
  # sigma_R2_theory_50 = sigma_v_R_value_50[0]**2 + np.power(
  #   t*t_c, gamma_R) * np.power(delta_50, alpha_R) * np.power(
  #     v_50, beta_R) * np.power(10, const_R) * np.square(V200)
  # sigma_R2_theory_75 = sigma_v_R_value_75[0]**2 + np.power(
  #   t*t_c, gamma_R) * np.power(delta_75, alpha_R) * np.power(
  #     v_75, beta_R) * np.power(10, const_R) * np.square(V200)

  # sigma_p2_theory_25 = sigma_v_phi_value_25[0]**2 + np.power(
  #   t*t_c, gamma_p) * np.power(delta_25, alpha_p) * np.power(
  #     v_25, beta_p) * np.power(10, const_p) * np.square(V200)
  # sigma_p2_theory_50 = sigma_v_phi_value_50[0]**2 + np.power(
  #   t*t_c, gamma_p) * np.power(delta_50, alpha_p) * np.power(
  #     v_50, beta_p) * np.power(10, const_p) * np.square(V200)
  # sigma_p2_theory_75 = sigma_v_phi_value_75[0]**2 + np.power(
  #   t*t_c, gamma_p) * np.power(delta_75, alpha_p) * np.power(
  #     v_75, beta_p) * np.power(10, const_p) * np.square(V200)

  # mean_v_phi_theory_25 = mean_v_phi_value_25[0] - np.power(
  #   t*t_c, gamma_m) * np.power(delta_25, alpha_m) * np.power(
  #     v_25, beta_m) * np.power(10, const_m) * V200
  # mean_v_phi_theory_50 = mean_v_phi_value_50[0] - np.power(
  #   t*t_c, gamma_m) * np.power(delta_50, alpha_m) * np.power(
  #     v_50, beta_m) * np.power(10, const_m) * V200
  # mean_v_phi_theory_75 = mean_v_phi_value_75[0] - np.power(
  #   t*t_c, gamma_m) * np.power(delta_75, alpha_m) * np.power(
  #     v_75, beta_m) * np.power(10, const_m) * V200

  delta_upsilon_dm_z_25 = v_25 - sigma_v_z_value_25[0]**2 / V200**2
  delta_upsilon_dm_z_50 = v_50 - sigma_v_z_value_50[0]**2 / V200**2
  delta_upsilon_dm_z_75 = v_75 - sigma_v_z_value_75[0]**2 / V200**2

  delta_upsilon_dm_R_25 = v_25 - sigma_v_R_value_25[0]**2 / V200**2
  delta_upsilon_dm_R_50 = v_50 - sigma_v_R_value_50[0]**2 / V200**2
  delta_upsilon_dm_R_75 = v_75 - sigma_v_R_value_75[0]**2 / V200**2

  delta_upsilon_dm_p_25 = v_25 - sigma_v_phi_value_25[0]**2 / V200**2
  delta_upsilon_dm_p_50 = v_50 - sigma_v_phi_value_50[0]**2 / V200**2
  delta_upsilon_dm_p_75 = v_75 - sigma_v_phi_value_75[0]**2 / V200**2

  # delta_upsilon_dm_m_25 = 224.544 / V200
  # delta_upsilon_dm_m_50 = 253.689 / V200
  # delta_upsilon_dm_m_75 = 272.854 / V200
  delta_upsilon_dm_m_25 = 161.965 / V200
  delta_upsilon_dm_m_50 = 198.828 / V200
  delta_upsilon_dm_m_75 = 222.651 / V200
  # delta_upsilon_dm_m_25 = mean_v_phi_value_25[0] / V200
  # delta_upsilon_dm_m_50 = mean_v_phi_value_50[0] / V200
  # delta_upsilon_dm_m_75 = mean_v_phi_value_75[0] / V200

  tau_max_z_25 = np.power(delta_upsilon_dm_z_25 / (np.power(
    10, const_z) * np.power(delta_25, alpha_z) * np.power(v_25, beta_z)), 1/gamma_z)
  tau_max_z_50 = np.power(delta_upsilon_dm_z_50 / (np.power(
    10, const_z) * np.power(delta_50, alpha_z) * np.power(v_50, beta_z)), 1/gamma_z)
  tau_max_z_75 = np.power(delta_upsilon_dm_z_75 / (np.power(
    10, const_z) * np.power(delta_75, alpha_z) * np.power(v_75, beta_z)), 1/gamma_z)

  tau_max_R_25 = np.power(delta_upsilon_dm_R_25 / (np.power(
    10, const_R) * np.power(delta_25, alpha_R) * np.power(v_25, beta_R)), 1/gamma_R)
  tau_max_R_50 = np.power(delta_upsilon_dm_R_50 / (np.power(
    10, const_R) * np.power(delta_50, alpha_R) * np.power(v_50, beta_R)), 1/gamma_R)
  tau_max_R_75 = np.power(delta_upsilon_dm_R_75 / (np.power(
    10, const_R) * np.power(delta_75, alpha_R) * np.power(v_75, beta_R)), 1/gamma_R)

  tau_max_p_25 = np.power(delta_upsilon_dm_p_25 / (np.power(
    10, const_p) * np.power(delta_25, alpha_p) * np.power(v_25, beta_p)), 1/gamma_p)
  tau_max_p_50 = np.power(delta_upsilon_dm_p_50 / (np.power(
    10, const_p) * np.power(delta_50, alpha_p) * np.power(v_50, beta_p)), 1/gamma_p)
  tau_max_p_75 = np.power(delta_upsilon_dm_p_75 / (np.power(
    10, const_p) * np.power(delta_75, alpha_p) * np.power(v_75, beta_p)), 1/gamma_p)

  tau_max_m_25 = np.power(delta_upsilon_dm_m_25 / (np.power(
    10, const_m) * np.power(delta_25, alpha_m) * np.power(v_25, beta_m)), 1/gamma_m)
  tau_max_m_50 = np.power(delta_upsilon_dm_m_50 / (np.power(
    10, const_m) * np.power(delta_50, alpha_m) * np.power(v_50, beta_m)), 1/gamma_m)
  tau_max_m_75 = np.power(delta_upsilon_dm_m_75 / (np.power(
    10, const_m) * np.power(delta_75, alpha_m) * np.power(v_75, beta_m)), 1/gamma_m)

  sigma_z2_theory_25 = sigma_v_z_value_25[0]**2 + delta_upsilon_dm_z_25 * (
    1 - np.exp(-np.power(t*t_c / tau_max_z_25, gamma_z))) * V200**2
  sigma_z2_theory_50 = sigma_v_z_value_50[0]**2 + delta_upsilon_dm_z_50 * (
    1 - np.exp(-np.power(t*t_c / tau_max_z_50, gamma_z))) * V200**2
  sigma_z2_theory_75 = sigma_v_z_value_75[0]**2 + delta_upsilon_dm_z_75 * (
    1 - np.exp(-np.power(t*t_c / tau_max_z_75, gamma_z))) * V200**2

  sigma_R2_theory_25 = sigma_v_R_value_25[0]**2 + delta_upsilon_dm_R_25 * (
    1 - np.exp(-np.power(t*t_c / tau_max_R_25, gamma_R))) * V200**2
  sigma_R2_theory_50 = sigma_v_R_value_50[0]**2 + delta_upsilon_dm_R_50 * (
    1 - np.exp(-np.power(t*t_c / tau_max_R_50, gamma_R))) * V200**2
  sigma_R2_theory_75 = sigma_v_R_value_75[0]**2 + delta_upsilon_dm_R_75 * (
    1 - np.exp(-np.power(t*t_c / tau_max_R_75, gamma_R))) * V200**2

  sigma_p2_theory_25 = sigma_v_phi_value_25[0]**2 + delta_upsilon_dm_p_25 * (
    1 - np.exp(-np.power(t*t_c / tau_max_p_25, gamma_p))) * V200**2
  sigma_p2_theory_50 = sigma_v_phi_value_50[0]**2 + delta_upsilon_dm_p_50 * (
    1 - np.exp(-np.power(t*t_c / tau_max_p_50, gamma_p))) * V200**2
  sigma_p2_theory_75 = sigma_v_phi_value_75[0]**2 + delta_upsilon_dm_p_75 * (
    1 - np.exp(-np.power(t*t_c / tau_max_p_75, gamma_p))) * V200**2

  mean_v_phi_theory_25 = mean_v_phi_value_25[0] - delta_upsilon_dm_m_25 * (
    1 - np.exp(-np.power(t*t_c / tau_max_m_25, gamma_m))) * V200
  mean_v_phi_theory_50 = mean_v_phi_value_50[0] - delta_upsilon_dm_m_50 * (
    1 - np.exp(-np.power(t*t_c / tau_max_m_50, gamma_m))) * V200
  mean_v_phi_theory_75 = mean_v_phi_value_75[0] - delta_upsilon_dm_m_75 * (
    1 - np.exp(-np.power(t*t_c / tau_max_m_75, gamma_m))) * V200

  sigma_t2_theory_25 = sigma_z2_theory_25 + sigma_R2_theory_25 + sigma_p2_theory_25
  sigma_t2_theory_50 = sigma_z2_theory_50 + sigma_R2_theory_50 + sigma_p2_theory_50
  sigma_t2_theory_75 = sigma_z2_theory_75 + sigma_R2_theory_75 + sigma_p2_theory_75

  sigma_v_z_theory_25   = np.sqrt(sigma_z2_theory_25)
  sigma_v_z_theory_50   = np.sqrt(sigma_z2_theory_50)
  sigma_v_z_theory_75   = np.sqrt(sigma_z2_theory_75)
  sigma_v_R_theory_25   = np.sqrt(sigma_R2_theory_25)
  sigma_v_R_theory_50   = np.sqrt(sigma_R2_theory_50)
  sigma_v_R_theory_75   = np.sqrt(sigma_R2_theory_75)
  sigma_v_p_theory_25   = np.sqrt(sigma_p2_theory_25)
  sigma_v_p_theory_50   = np.sqrt(sigma_p2_theory_50)
  sigma_v_p_theory_75   = np.sqrt(sigma_p2_theory_75)
  sigma_v_t_theory_25   = np.sqrt(sigma_t2_theory_25)
  sigma_v_t_theory_50   = np.sqrt(sigma_t2_theory_50)
  sigma_v_t_theory_75   = np.sqrt(sigma_t2_theory_75)

  kappa_theory_25 = (mean_v_phi_theory_25**2 + sigma_p2_theory_25) / (
    mean_v_phi_theory_25**2 + sigma_t2_theory_25)
  kappa_theory_50 = (mean_v_phi_theory_50**2 + sigma_p2_theory_50) / (
    mean_v_phi_theory_50**2 + sigma_t2_theory_50)
  kappa_theory_75 = (mean_v_phi_theory_75**2 + sigma_p2_theory_75) / (
    mean_v_phi_theory_75**2 + sigma_t2_theory_75)

  v_on_sigma_theory_25 = mean_v_phi_theory_25 / sigma_v_t_theory_25
  v_on_sigma_theory_50 = mean_v_phi_theory_50 / sigma_v_t_theory_50
  v_on_sigma_theory_75 = mean_v_phi_theory_75 / sigma_v_t_theory_75

  #can cancel out the r's
  #not correct...
  mean_elipticity_theory_25 = r_25 * mean_v_phi_theory_25 / (
    r_25 * v_c_25)
  mean_elipticity_theory_50 = r_50 * mean_v_phi_theory_50 / (
    r_50 * v_c_50)
  mean_elipticity_theory_75 = r_75 * mean_v_phi_theory_75 / (
    r_75 * v_c_75)

  thin_disk_fraction_theory_25 = np.zeros(len(t))
  thin_disk_fraction_theory_50 = np.zeros(len(t))
  thin_disk_fraction_theory_75 = np.zeros(len(t))
  bulge_fraction_theory_25 = np.zeros(len(t))
  bulge_fraction_theory_50 = np.zeros(len(t))
  bulge_fraction_theory_75 = np.zeros(len(t))
  kappa_co_theory_25 = np.zeros(len(t))
  kappa_co_theory_50 = np.zeros(len(t))
  kappa_co_theory_75 = np.zeros(len(t))
  half_abs_z_theory_25 = np.zeros(len(t))
  half_abs_z_theory_50 = np.zeros(len(t))
  half_abs_z_theory_75 = np.zeros(len(t))

  theory_25 = np.vstack((t, mean_elipticity_theory_25,
                         thin_disk_fraction_theory_25, bulge_fraction_theory_25,
                         kappa_theory_25, kappa_co_theory_25, v_on_sigma_theory_25,
                         half_abs_z_theory_25,
                         sigma_v_R_theory_25, sigma_v_p_theory_25, sigma_v_z_theory_25,
                         sigma_v_t_theory_25, mean_v_phi_theory_25)).T

  theory_50 = np.vstack((t, mean_elipticity_theory_50,
                         thin_disk_fraction_theory_50, bulge_fraction_theory_50,
                         kappa_theory_50, kappa_co_theory_50, v_on_sigma_theory_50,
                         half_abs_z_theory_50,
                         sigma_v_R_theory_50, sigma_v_p_theory_50, sigma_v_z_theory_50,
                         sigma_v_t_theory_50, mean_v_phi_theory_50)).T

  theory_75 = np.vstack((t, mean_elipticity_theory_75,
                         thin_disk_fraction_theory_75, bulge_fraction_theory_75,
                         kappa_theory_75, kappa_co_theory_75, v_on_sigma_theory_75,
                         half_abs_z_theory_75,
                         sigma_v_R_theory_75, sigma_v_p_theory_75, sigma_v_z_theory_75,
                         sigma_v_t_theory_75, mean_v_phi_theory_75)).T

  #manual corner plot
  big_data = (data_25,   data_50,   data_75, data,
              theory_25, theory_50, theory_75)
  # cmaps = ('Oranges_r', 'Greens_r', 'Purples_r', 'Greys_r')
  cs = ('C0', 'C1', 'C2', 'k', 'C0', 'C1', 'C2')

  labels=[r'$t$ [Gyr]', r'$\bar{\epsilon}$', r'$F(\epsilon > 0.7)$', r'Spheroid Fraction',
          r'$\kappa_{rot}$', r'$\kappa_{co}$', r'$\overline{v_\phi}/\sigma_{tot}$',
          r'$z_{50}$',
          r'$\sigma_{v_R}$',r'$\sigma_{v_\phi}$',r'$\sigma_{v_z}$',
          r'$\sigma_{tot}$', r'$\overline{v_\phi}$']
  lims = [(0, t_tot), (0,0.999), (0,0.999), (0,0.999), (0,0.999), (0,0.999), (0,2.999),
          (0,4.999), (0,199), (0,199), (0,199), (0,299), (0,299)]
  n = np.shape(data)[1]

  fig, axs = plt.subplots(nrows=n-1, ncols=n-1, sharex=False, sharey=False,
                          figsize=(25,25))
  fig.subplots_adjust(hspace=0.03,wspace=0.03)

  for data_i in range(len(big_data)):
    for axis_y in range(n):
      for axis_x in range(axis_y):
        if data_i > 3:
          axs[axis_y-1, axis_x].errorbar(
            big_data[data_i].T[axis_x], big_data[data_i].T[axis_y],
            ls=':', fmt='', c=cs[data_i], linewidth=3)
        else:
          axs[axis_y-1, axis_x].scatter(
            big_data[data_i].T[axis_x], big_data[data_i].T[axis_y],
            s=5, c=cs[data_i], alpha=0.1)

        axs[axis_y-1, axis_x].set_xlim(lims[axis_x])
        axs[axis_y-1, axis_x].set_ylim(lims[axis_y])
        if axis_x != 0:
          axs[axis_y-1, axis_x].set_yticklabels([])
        if axis_y != n-1:
          axs[axis_y-1, axis_x].set_xticklabels([])

  for axis_y in range(n-1):
    axs[axis_y, 0].set_ylabel(labels[axis_y+1])
  for axis_x in range(n-1):
    axs[n-2, axis_x].set_xlabel(labels[axis_x])

  for axis_y in range(n-1):
    for axis_x in range(n-1):
      if n - axis_y + axis_x > n:
        axs[axis_y, axis_x].set_visible(False)

  axs[0,0].legend([r'$r_{25}$ measured',r'$r_{50}$ measured',r'$r_{75}$ measured',
                   r'Global',
                   r'$r_{25}$ fit',     r'$r_{50}$ fit',     r'$r_{75}$ fit'],
                  bbox_to_anchor=(2, 1), loc='upper left')

  plt.show()
    fname = '../galaxies/' + name + '/results/morphological_corner'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  data = np.vstack((t, mean_elipticity_value, thin_disk_fraction, bulge_fraction,
                    kappa_value, kappa_co_value, v_on_sigma_value)).T

  data_25=np.vstack((t, mean_elipticity_value_25,thin_disk_fraction_25,bulge_fraction_25,
                     kappa_value_25, kappa_co_value_25, v_on_sigma_value_25)).T

  data_50=np.vstack((t, mean_elipticity_value_50,thin_disk_fraction_50,bulge_fraction_50,
                     kappa_value_50, kappa_co_value_50, v_on_sigma_value_50)).T

  data_75=np.vstack((t, mean_elipticity_value_75,thin_disk_fraction_75,bulge_fraction_75,
                     kappa_value_75, kappa_co_value_75, v_on_sigma_value_75)).T

  #manual corner plot
  big_data = (data_25, data_50, data_75, data)
  # cmaps = ('Oranges_r', 'Greens_r', 'Purples_r', 'Greys_r')
  cs = ('C0', 'C1', 'C2', 'k')

  labels=[r'$t$ [Gyr]', r'$\bar{\epsilon}$', r'$F(\epsilon > 0.7)$', r'Spheroid Fraction',
          r'$\kappa_{rot}$', r'$\kappa_{co}$', r'$\overline{v_\phi}/\sigma_{tot}$',
          r'$\sigma_{v_R}$',r'$\sigma_{v_\phi}$',r'$\sigma_{v_z}$',
          r'$\sigma_{tot}$', r'$\overline{v_\phi}$']
  lims = [(0, t_tot), (0,0.999), (0,0.999), (0,0.999), (0,0.999), (0,0.999), (0,2.999),
          (0,199), (0,199), (0,199), (0,299), (0,299)]
  n = np.shape(data)[1]

  fig, axs = plt.subplots(nrows=n-1, ncols=n-1, sharex=False, sharey=False,
                          figsize=(13,13))
  fig.subplots_adjust(hspace=0.03,wspace=0.03)

  for data_i in range(len(big_data)):
    for axis_y in range(n):
      for axis_x in range(axis_y):
        # axs[axis_y-1, axis_x].scatter(
        #   big_data[data_i].T[axis_x], big_data[data_i].T[axis_y],
        #   s=2, c=t, cmap=cmaps[data_i], alpha=0.3)
        axs[axis_y-1, axis_x].scatter(
          big_data[data_i].T[axis_x], big_data[data_i].T[axis_y],
          s=5, c=cs[data_i], alpha=0.3)

        axs[axis_y-1, axis_x].set_xlim(lims[axis_x])
        axs[axis_y-1, axis_x].set_ylim(lims[axis_y])
        if axis_x != 0:
          axs[axis_y-1, axis_x].set_yticklabels([])
        if axis_y != n-1:
          axs[axis_y-1, axis_x].set_xticklabels([])

  for axis_y in range(n-1):
    axs[axis_y, 0].set_ylabel(labels[axis_y+1])
  for axis_x in range(n-1):
    axs[n-2, axis_x].set_xlabel(labels[axis_x])

  for axis_y in range(n-1):
    for axis_x in range(n-1):
      if n - axis_y + axis_x > n:
        axs[axis_y, axis_x].set_visible(False)

  axs[0,0].legend([r'$r_{25}$',r'$r_{50}$',r'$r_{75}$',r'Global'],
                  bbox_to_anchor=(1, 1), loc='upper left')

  plt.show() if save:
    fname = '../galaxies/' + name + '/results/morphological_corner_only'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  # data = np.vstack((mean_elipticity_value, thin_disk_fraction, bulge_fraction,
  #                   kappa_value, kappa_co_value, v_on_sigma_value,
  #                   normed_sigma_v_R_value, normed_sigma_v_phi_value,
  #                   normed_sigma_v_z_value,
  #                   normed_sigma_v_tot_value, normed_mean_v_phi_value)).T

  # data_25=np.vstack((mean_elipticity_value_25,thin_disk_fraction_25,bulge_fraction_25,
  #                    kappa_value_25, kappa_co_value_25, v_on_sigma_value_25,
  #                    normed_sigma_v_R_value_25, normed_sigma_v_phi_value_25,
  #                    normed_sigma_v_z_value_25,
  #                    normed_sigma_v_tot_value_25, normed_mean_v_phi_value_25)).T

  # data_50=np.vstack((mean_elipticity_value_50,thin_disk_fraction_50,bulge_fraction_50,
  #                    kappa_value_50, kappa_co_value_50, v_on_sigma_value_50,
  #                    normed_sigma_v_R_value_50, normed_sigma_v_phi_value_50,
  #                    normed_sigma_v_z_value_50,
  #                    normed_sigma_v_tot_value_50, normed_mean_v_phi_value_50)).T

  # data_75=np.vstack((mean_elipticity_value_75,thin_disk_fraction_75,bulge_fraction_75,
  #                    kappa_value_75, kappa_co_value_75, v_on_sigma_value_75,
  #                    normed_sigma_v_R_value_75, normed_sigma_v_phi_value_75,
  #                    normed_sigma_v_z_value_75,
  #                    normed_sigma_v_tot_value_75, normed_mean_v_phi_value_75)).T

  # #manual corner plot
  # big_data = (data_25, data_50, data_75, data)
  # # cmaps = ('Oranges_r', 'Greens_r', 'Purples_r', 'Greys_r')
  # cs = ('C0', 'C1', 'C2', 'k')

  # labels=[r'$\bar{\epsilon}$', r'$F(\epsilon > 0.7)$', r'Bulge Fraction',
  #         r'$\kappa_{rot}$', r'$\kappa_{co}$', r'$\overline{v_\phi}/\sigma_{tot}$',
  #         r'$\sigma_{v_R}/v_c(r)$',r'$\sigma_{v_\phi}/v_c(r)$',
  #         r'$\sigma_{v_z}/v_c(r)$',
  #         r'$\sigma_{tot}/v_c(r)$', r'$\overline{v_\phi}/v_c(r)$', r'$t$ [Gyr]']
  # lims = [(0,0.999), (0,0.999), (0,0.999), (0,0.999), (0,0.999), (0,2.999),
  #         (0,0.999), (0,0.999), (0,0.999), (0,1.999), (0,0.999), (0, t_tot)]
  # n = np.shape(data)[1]

  # fig, axs = plt.subplots(nrows=n-1, ncols=n-1, sharex=False, sharey=False,
  #                         figsize=(25,25))
  # fig.subplots_adjust(hspace=0.03,wspace=0.03)

  # for data_i in range(len(big_data)):
  #   for axis_y in range(n):
  #     for axis_x in range(axis_y):
  #       # axs[axis_y-1, axis_x].scatter(
  #       #   big_data[data_i].T[axis_x], big_data[data_i].T[axis_y],
  #       #   s=2, c=t, cmap=cmaps[data_i], alpha=0.3)
  #       axs[axis_y-1, axis_x].scatter(
  #         big_data[data_i].T[axis_x], big_data[data_i].T[axis_y],
  #         s=5, c=cs[data_i], alpha=0.3)

  #       axs[axis_y-1, axis_x].set_xlim(lims[axis_x])
  #       axs[axis_y-1, axis_x].set_ylim(lims[axis_y])
  #       if axis_x != 0:
  #         axs[axis_y-1, axis_x].set_yticklabels([])
  #       if axis_y != n-1:
  #         axs[axis_y-1, axis_x].set_xticklabels([])

  # for axis_y in range(n-1):
  #   axs[axis_y, 0].set_ylabel(labels[axis_y+1])
  # for axis_x in range(n-1):
  #   axs[n-2, axis_x].set_xlabel(labels[axis_x])

  # for axis_y in range(n-1):
  #   for axis_x in range(n-1):
  #     if n - axis_y + axis_x > n:
  #       axs[axis_y, axis_x].set_visible(False)

  # axs[0,0].legend([r'$r_{25}$',r'$r_{50}$',r'$r_{75}$',r'Global'],
  #                 bbox_to_anchor=(0.98, 1), loc='upper left')

  # plt.show() if save:
  #   fname = '../galaxies/' + name + '/results/morphological_corner_normed'
  #   plt.savefig(fname, bbox_inches='tight')
  #   plt.close()

  plt.figure()

  plt.scatter(t, v_on_sigma_value_25, s=12, c='C0', alpha=0.2)
  plt.scatter(t, v_on_sigma_value_50, s=12, c='C1', alpha=0.2)
  plt.scatter(t, v_on_sigma_value_75, s=12, c='C2', alpha=0.2)

  plt.errorbar(t, v_on_sigma_theory_25, ls=':', fmt='', c='C0', linewidth=5)
  plt.errorbar(t, v_on_sigma_theory_50, ls=':', fmt='', c='C1', linewidth=5)
  plt.errorbar(t, v_on_sigma_theory_75, ls=':', fmt='', c='C2', linewidth=5)

  plt.legend([r'$r_{25}$ measured',r'$r_{50}$ measured',r'$r_{75}$ measured',
              r'$r_{25}$ theory',  r'$r_{50}$ theory',  r'$r_{75}$ theory'])
  plt.xlabel('$t$ [Gyr]')
  plt.ylabel('$\overline{v_\phi} / \sigma_{tot}$')
  plt.xlim((0, t_tot))
  plt.ylim((0, 3))

  plt.show() if save:
    fname = '../galaxies/' + name + '/results/v_on_sigma_comparison'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  plt.figure(figsize=(8,6))

  plt.scatter(t, kappa_value_25, s=12, c='C0', alpha=0.2)
  plt.scatter(t, kappa_value_50, s=12, c='C1', alpha=0.2)
  plt.scatter(t, kappa_value_75, s=12, c='C2', alpha=0.2)

  plt.errorbar(t, kappa_theory_25, ls=':', fmt='', c='C0', linewidth=5)
  plt.errorbar(t, kappa_theory_50, ls=':', fmt='', c='C1', linewidth=5)
  plt.errorbar(t, kappa_theory_75, ls=':', fmt='', c='C2', linewidth=5)

  plt.legend([r'$r_{25}$ measured',r'$r_{50}$ measured',r'$r_{75}$ measured',
              r'$r_{25}$ theory',  r'$r_{50}$ theory',  r'$r_{75}$ theory'], ncol=2)
  plt.xlabel('$t$ [Gyr]')
  plt.ylabel('$\kappa_{rot}$')
  plt.xlim((0, t_tot))
  plt.ylim((0, 1))

  plt.show() if save:
    fname = '../galaxies/' + name + '/results/kappa_comparison'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  fig, axs = plt.subplots(nrows=2, ncols=1, sharex=False, sharey=False,
                          figsize=(8,8))
  fig.subplots_adjust(hspace=0.03,wspace=0.03)

  axs[0].scatter(t, v_on_sigma_value_25, s=16, c='C0', alpha=0.2)
  axs[0].scatter(t, v_on_sigma_value_50, s=16, c='C1', alpha=0.2)
  axs[0].scatter(t, v_on_sigma_value_75, s=16, c='C2', alpha=0.2)

  axs[0].errorbar(t, v_on_sigma_theory_25, ls='-', fmt='', c='C0', linewidth=4)
  axs[0].errorbar(t, v_on_sigma_theory_50, ls='-', fmt='', c='C1', linewidth=4)
  axs[0].errorbar(t, v_on_sigma_theory_75, ls='-', fmt='', c='C2', linewidth=4)


  axs[1].scatter(t, kappa_value_25, s=16, c='C0', alpha=0.2)
  axs[1].scatter(t, kappa_value_50, s=16, c='C1', alpha=0.2)
  axs[1].scatter(t, kappa_value_75, s=16, c='C2', alpha=0.2)

  axs[1].errorbar(t, kappa_theory_25, ls='-', fmt='', c='C0', linewidth=4)
  axs[1].errorbar(t, kappa_theory_50, ls='-', fmt='', c='C1', linewidth=4)
  axs[1].errorbar(t, kappa_theory_75, ls='-', fmt='', c='C2', linewidth=4)

  axs[0].legend([r'$r_{25}$ measured',r'$r_{50}$ measured',r'$r_{75}$ measured',
                 r'$r_{25}$ theory',  r'$r_{50}$ theory',  r'$r_{75}$ theory'], ncol=2)
  axs[0].set_ylabel('$\overline{v_{\phi}} / \sigma_{tot}$')
  axs[0].set_xlim((0, t_tot))
  axs[0].set_ylim((0, 3))
  axs[0].set_xticklabels([])
  axs[0].grid()

  axs[1].set_xlabel('$t$ [Gyr]')
  axs[1].set_ylabel('$\kappa_{rot}$')
  axs[1].set_xlim((0, t_tot))
  axs[1].set_ylim((0, 1))
  axs[1].grid()

  axs[0].axhline(1, ls='--', c='k', zorder=-1000)
  axs[1].axhline(0.5, ls='--', c='k', zorder=-1000)

  plt.show() if save:
    fname = '../galaxies/' + name + '/results/v_on_sigma_kappa_comparison'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  return

def plot_morpholocial_profile(name, save=True):
  '''
  '''
  t_tot = 9.778943899409334 #Gyr

  if (('7p5' in name) or ('8p0' in name)):
    nsnaps = 401
  else:
    nsnaps = 102

  if   '8p0' in name: mass_dm = np.power(10.,8   -10)
  elif '7p5' in name: mass_dm = np.power(10.,7.5 -10)
  elif '7p0' in name: mass_dm = np.power(10.,7   -10)
  elif '6p5' in name: mass_dm = np.power(10.,6.5 -10)
  elif '6p0' in name: mass_dm = np.power(10.,6   -10)

  #bins: 17
  bin_edges = [0,1,2,3,4,5,6,7,8,9,10,12,15,20,30]
  #<30kpc, <Re, <2Re
  #to put in header
  bins = [0, 1,2,3,4,5,6,7,8,9,10,12,15,20,30, 30, 1]

  n_centrals = 1

  stellar_half_mass_radius = np.zeros(n_centrals, dtype=np.float64)

  bin_mass  = np.zeros((n_centrals,16), dtype=np.float64)
  bin_N     = np.zeros((n_centrals,16), dtype=np.int64)

  # fjzjc09 = np.zeros((n_centrals,16), dtype=np.float64)
  # fjzjc08 = np.zeros((n_centrals,16), dtype=np.float64)
  # fjzjc07 = np.zeros((n_centrals,16), dtype=np.float64)
  fjzjc00 = np.zeros((n_centrals,16), dtype=np.float64)

  kappa_rot = np.zeros((n_centrals,16), dtype=np.float64)
  kappa_co  = np.zeros((n_centrals,16), dtype=np.float64)

  sigma_z    = np.zeros((n_centrals,16), dtype=np.float64)
  sigma_R    = np.zeros((n_centrals,16), dtype=np.float64)
  sigma_phi  = np.zeros((n_centrals,16), dtype=np.float64)
  sigma_tot  = np.zeros((n_centrals,16), dtype=np.float64)
  mean_v_phi = np.zeros((n_centrals,16), dtype=np.float64)

  z_half    = np.zeros((n_centrals,16), dtype=np.float64)

  #assume already 30kpced
  (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,
              MassStar, PosStars, VelStars, IDStars,
    j_z_stars, j_p_stars, j_c_stars, energy_stars,
    E_interp, j_circ_interp, R_interp
    ) = load_nbody.load_and_calculate(name, '000')

  n_stars  = len(IDStars)

  pos = PosStars
  vel = VelStars
  mass = np.ones(n_stars) * MassStar

  #cylindrical coords
  (R, phi, z, v_R, v_phi, v_z
   ) = halo.get_cylindrical(pos, vel)

  # j_z = np.cross(R, v_phi)
  j_z = pos[:,0] * vel[:,1] - pos[:,1] * vel[:,0]

  ########
  hdf_index = 0

  #binned
  bin_mass[hdf_index, 0:14],_,_ = binned_statistic(R, mass, bins=bin_edges, statistic='sum')
  bin_N[hdf_index, 0:14],_      = np.histogram(R, bins=bin_edges)

  #j_z
  fjzjc00[hdf_index, 0:14] = binned_statistic(R[j_z < 0], mass[j_z < 0], bins=bin_edges,
                                            statistic='sum')[0] / bin_mass[hdf_index, 0:14]

  #calculate kappas
  K_tot,_,_ = binned_statistic(R, mass * np.square(np.linalg.norm(vel,axis=1)),
                               bins=bin_edges, statistic='sum')
  K_rot,_,_ = binned_statistic(R, mass * np.square(v_phi),
                               bins=bin_edges, statistic='sum')
  co = (v_phi > 0)
  K_co,_,_ = binned_statistic(R[co], mass[co] * np.square(v_phi[co]),
                              bins=bin_edges, statistic='sum')

  kappa_rot[hdf_index, 0:14] = K_rot / K_tot
  kappa_co[hdf_index, 0:14]  = K_co  / K_tot

  #sigmas
  mean_v_phi[hdf_index, 0:14] = binned_statistic(R, mass * v_phi, bins=bin_edges,
                                                 statistic='sum')[0]  / bin_mass[hdf_index, 0:14]

  sigma_z[hdf_index, 0:14]    = np.sqrt(binned_statistic(
    R, mass * np.square(v_z),   bins=bin_edges, statistic='sum')[0] / bin_mass[hdf_index, 0:14])
  sigma_R[hdf_index, 0:14]    = np.sqrt(binned_statistic(
    R, mass * np.square(v_R),   bins=bin_edges, statistic='sum')[0] / bin_mass[hdf_index, 0:14])
  sigma_phi[hdf_index, 0:14]  = np.sqrt((binned_statistic(
    R, mass * np.square(v_phi), bins=bin_edges, statistic='sum')[0] - np.square(mean_v_phi[hdf_index, 0:14]
    ) * bin_mass[hdf_index, 0:14]) / bin_mass[hdf_index, 0:14])

  sigma_tot[hdf_index, 0:14]  = np.linalg.norm(
    (sigma_z[hdf_index, 0:14], sigma_R[hdf_index, 0:14], sigma_phi[hdf_index, 0:14]), axis=0)

  z_half[hdf_index, 0:14] = binned_statistic(R, np.abs(z), bins=bin_edges, statistic=np.median)[0]

  #30 kpc
  bin_mass[hdf_index, 14] = np.sum(mass)
  bin_N[hdf_index, 14]    = np.size(mass)

  #j_z
  fjzjc00[hdf_index,14] = np.sum(mass[j_z < 0]) / np.sum(mass)

  #calculate kappas
  K_tot = np.sum(mass * np.square(np.linalg.norm(vel,axis=1)))
  K_rot = np.sum(mass * np.square(v_phi))
  K_co  = np.sum(mass[v_phi > 0] * np.square(v_phi[v_phi > 0]))

  kappa_rot[hdf_index, 14] = K_rot / K_tot
  kappa_co[hdf_index, 14]  = K_co  / K_tot

  #sigmas
  sigma_z[hdf_index, 14]    = np.std(v_z)
  sigma_R[hdf_index, 14]    = np.std(v_R)
  sigma_phi[hdf_index, 14]  = np.std(v_phi)
  sigma_tot[hdf_index, 14]  = np.linalg.norm(
    (sigma_z[hdf_index, 14], sigma_R[hdf_index, 14], sigma_phi[hdf_index, 14]))
  mean_v_phi[hdf_index, 14] = np.mean(v_phi)

  z_half[hdf_index, 14] = np.median(np.abs(z))


  #half mass
  R_sort = np.argsort(R)
  if len(R)%2 == 0:
    half_mass_radii = (R[R_sort][len(R)>>1] + R[R_sort][(len(R)>>1)-1])/2
  else:
    half_mass_radii = R[R_sort][int(len(R)/2)]

  stellar_half_mass_radius[hdf_index] = half_mass_radii

  #mask
  mask = (R < half_mass_radii)

  pos  = pos[mask]
  vel  = vel[mask]
  mass = mass[mask]
  (R, v_z, v_R, v_phi) = (R[mask], v_z[mask], v_R[mask], v_phi[mask])
  j_z = j_z[mask]

  bin_mass[hdf_index, 15] = np.sum(mass)
  bin_N[hdf_index, 15]    = np.size(mass)

  #jz/jc
  fjzjc00[hdf_index,15] = np.sum(mass[j_z < 0]) / np.sum(mass)

  #calculate kappas
  K_tot = np.sum(mass * np.square(np.linalg.norm(vel,axis=1)))
  K_rot = np.sum(mass * np.square(v_phi))
  K_co  = np.sum(mass[v_phi > 0] * np.square(v_phi[v_phi > 0]))

  kappa_rot[hdf_index, 15] = K_rot / K_tot
  kappa_co[hdf_index, 15]  = K_co  / K_tot

  #sigmas
  sigma_z[hdf_index, 15]    = np.std(v_z)
  sigma_R[hdf_index, 15]    = np.std(v_R)
  sigma_phi[hdf_index, 15]  = np.std(v_phi)
  sigma_tot[hdf_index, 15]  = np.linalg.norm(
    (sigma_z[hdf_index, 15], sigma_R[hdf_index, 15], sigma_phi[hdf_index, 15]))
  mean_v_phi[hdf_index, 15] = np.mean(v_phi)

  z_half[hdf_index, 15] = np.median(np.abs(z))

  # fig = plt.figure()
  # fig.set_size_inches(5, 20, forward=True)

  # ax1 = plt.subplot(911)
  # ax2 = plt.subplot(912)
  # ax3 = plt.subplot(913)
  # ax4 = plt.subplot(914)
  # ax5 = plt.subplot(915)
  # ax6 = plt.subplot(916)
  # ax7 = plt.subplot(917)
  # ax8 = plt.subplot(918)
  # ax9 = plt.subplot(919)

  # fig.subplots_adjust(hspace=0.03,wspace=0)

  # # ax1.hlines(bin_mass[0,0:14], bins[:14], bins[1:15])
  # # ax1.set_ylabel(r'Bin mass')
  # # ax2.hlines(bin_N[0,0:14], bins[:14], bins[1:15])
  # # ax2.set_ylabel(r'Bin $N$')
  # ax1.hlines(fjzjc00[0,0:14], bins[:14], bins[1:15])
  # ax1.set_ylabel(r'frac $j_z/j_c < 0$')
  # ax2.hlines(kappa_rot[0,0:14], bins[:14], bins[1:15])
  # ax2.set_ylabel(r'$\kappa_{rot}$')
  # ax3.hlines(kappa_co[0,0:14], bins[:14], bins[1:15])
  # ax3.set_ylabel(r'$\kappa_{co}$')
  # ax4.hlines(sigma_z[0,0:14], bins[:14], bins[1:15])
  # ax4.set_ylabel(r'$\sigma_z$')
  # ax5.hlines(sigma_R[0,0:14], bins[:14], bins[1:15])
  # ax5.set_ylabel(r'$\sigma_R$')
  # ax6.hlines(sigma_phi[0,0:14], bins[:14], bins[1:15])
  # ax6.set_ylabel(r'$\sigma_\phi$')
  # ax7.hlines(sigma_tot[0,0:14], bins[:14], bins[1:15])
  # ax7.set_ylabel(r'$\sigma_{tot}$')
  # ax8.hlines(mean_v_phi[0,0:14], bins[:14], bins[1:15])
  # ax8.set_ylabel(r'$\overline{v_\phi}$')
  # ax9.hlines(z_half[0,0:14], bins[:14], bins[1:15])
  # ax9.set_ylabel(r'$z_{1/2}$')

  return

def plot_morpholocial_corner_less_than(name, save=True):
  '''
  '''
  t_tot = 9.778943899409334 #Gyr

  if (('7p5' in name) or ('8p0' in name)):
    nsnaps = 401
  else:
    nsnaps = 102

  if   '8p0' in name: mass_dm = np.power(10,10-8)
  elif '7p5' in name: mass_dm = np.power(10,10-7.5)
  elif '7p0' in name: mass_dm = np.power(10,10-7)
  elif '6p5' in name: mass_dm = np.power(10,10-6.5)
  elif '6p0' in name: mass_dm = np.power(10,10-6)

  r_25 = 3.98
  r_50 = 6.95
  r_75 = 11.16
  thin_disk_threshold = 0.7

  mean_elipticity_value = np.zeros(nsnaps)
  thin_disk_fraction    = np.zeros(nsnaps)
  bulge_fraction        = np.zeros(nsnaps)
  kappa_value           = np.zeros(nsnaps)
  kappa_co_value        = np.zeros(nsnaps)
  v_on_sigma_value      = np.zeros(nsnaps)

  sigma_v_R_value       = np.zeros(nsnaps)
  sigma_v_phi_value     = np.zeros(nsnaps)
  sigma_v_tild_value    = np.zeros(nsnaps)
  sigma_v_z_value       = np.zeros(nsnaps)
  sigma_v_tot_value     = np.zeros(nsnaps)
  # sigma_v_R_dm         = np.zeros(nsnaps)
  # sigma_v_phi_dm       = np.zeros(nsnaps)
  # sigma_v_z_dm         = np.zeros(nsnaps)
  # sigma_v_tot_dm       = np.zeros(nsnaps)
  mean_v_phi_value      = np.zeros(nsnaps)

  normed_sigma_v_R_value    = np.zeros(nsnaps)
  normed_sigma_v_phi_value  = np.zeros(nsnaps)
  # normed_sigma_v_tild_value = np.zeros(nsnaps)
  normed_sigma_v_z_value    = np.zeros(nsnaps)
  normed_sigma_v_tot_value  = np.zeros(nsnaps)
  normed_mean_v_phi_value   = np.zeros(nsnaps)

  mean_elipticity_value_25 = np.zeros(nsnaps)
  thin_disk_fraction_25    = np.zeros(nsnaps)
  bulge_fraction_25        = np.zeros(nsnaps)
  kappa_value_25           = np.zeros(nsnaps)
  kappa_co_value_25        = np.zeros(nsnaps)
  v_on_sigma_value_25      = np.zeros(nsnaps)
  sigma_v_R_value_25       = np.zeros(nsnaps)
  sigma_v_phi_value_25     = np.zeros(nsnaps)
  sigma_v_z_value_25       = np.zeros(nsnaps)
  sigma_v_tot_value_25     = np.zeros(nsnaps)
  mean_v_phi_value_25      = np.zeros(nsnaps)
  normed_sigma_v_R_value_25   = np.zeros(nsnaps)
  normed_sigma_v_phi_value_25 = np.zeros(nsnaps)
  normed_sigma_v_z_value_25   = np.zeros(nsnaps)
  normed_sigma_v_tot_value_25 = np.zeros(nsnaps)
  normed_mean_v_phi_value_25  = np.zeros(nsnaps)

  mean_elipticity_value_50 = np.zeros(nsnaps)
  thin_disk_fraction_50    = np.zeros(nsnaps)
  bulge_fraction_50        = np.zeros(nsnaps)
  kappa_value_50           = np.zeros(nsnaps)
  kappa_co_value_50        = np.zeros(nsnaps)
  v_on_sigma_value_50      = np.zeros(nsnaps)
  sigma_v_R_value_50       = np.zeros(nsnaps)
  sigma_v_phi_value_50     = np.zeros(nsnaps)
  sigma_v_z_value_50       = np.zeros(nsnaps)
  sigma_v_tot_value_50     = np.zeros(nsnaps)
  mean_v_phi_value_50      = np.zeros(nsnaps)
  normed_sigma_v_R_value_50   = np.zeros(nsnaps)
  normed_sigma_v_phi_value_50 = np.zeros(nsnaps)
  normed_sigma_v_z_value_50   = np.zeros(nsnaps)
  normed_sigma_v_tot_value_50 = np.zeros(nsnaps)
  normed_mean_v_phi_value_50  = np.zeros(nsnaps)

  mean_elipticity_value_75 = np.zeros(nsnaps)
  thin_disk_fraction_75    = np.zeros(nsnaps)
  bulge_fraction_75        = np.zeros(nsnaps)
  kappa_value_75           = np.zeros(nsnaps)
  kappa_co_value_75        = np.zeros(nsnaps)
  v_on_sigma_value_75      = np.zeros(nsnaps)
  sigma_v_R_value_75       = np.zeros(nsnaps)
  sigma_v_phi_value_75     = np.zeros(nsnaps)
  sigma_v_z_value_75       = np.zeros(nsnaps)
  sigma_v_tot_value_75     = np.zeros(nsnaps)
  mean_v_phi_value_75      = np.zeros(nsnaps)
  normed_sigma_v_R_value_75   = np.zeros(nsnaps)
  normed_sigma_v_phi_value_75 = np.zeros(nsnaps)
  normed_sigma_v_z_value_75   = np.zeros(nsnaps)
  normed_sigma_v_tot_value_75 = np.zeros(nsnaps)
  normed_mean_v_phi_value_75  = np.zeros(nsnaps)

  for i in range(nsnaps):
    snap = '{0:03d}'.format(int(i))
    (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,
                MassStar, PosStars, VelStars, IDStars,
      j_z_stars, j_p_stars, j_c_stars, energy_stars,
      E_interp, j_circ_interp, R_interp
      ) = load_nbody.load_and_calculate(name, snap)

    n_stars  = len(IDStars)
    #j_zonc
    j_zonc_stars = j_z_stars / j_c_stars
    #cylindrical coords
    (rho, phi, z, v_rho, v_phi, v_z
     ) = halo.get_cylindrical(PosStars, VelStars)

    r_stars = np.linalg.norm(PosStars, axis=1)

    #add values to arrays
    mean_elipticity_value[i] = np.mean(j_zonc_stars)
    #jzoncs
    thin_disk_fraction[i]    = np.sum(j_zonc_stars > thin_disk_threshold) / n_stars
    bulge_fraction[i]        = 2 * np.sum(j_zonc_stars < 0) / n_stars

    #calculate kappas
    K_tot = np.sum(np.square(np.linalg.norm(VelStars,axis=1)))
    K_rot = np.sum(np.square(v_phi))
    K_co  = np.sum(np.square(v_phi[v_phi > 0]))

    kappa_value[i]    = K_rot / K_tot
    kappa_co_value[i] = K_co  / K_tot

    v_c_interp = j_circ_interp / R_interp
    interp = InterpolatedUnivariateSpline(R_interp, v_c_interp,
                                          k=1, ext='const')
    v_c_stars = interp(r_stars)
    v_tild = v_phi - v_c_stars

    #sigmas
    disp_v_rho = np.std(v_rho)
    disp_v_phi = np.std(v_phi)
    disp_v_z   = np.std(v_z)
    disp_v_tot = np.linalg.norm((disp_v_rho, disp_v_phi, disp_v_z))
    mean_v_phi = np.mean(v_phi)

    sigma_v_R_value[i]   = disp_v_rho
    sigma_v_phi_value[i] = disp_v_phi
    sigma_v_tild_value[i]= np.std(v_tild)
    sigma_v_z_value[i]   = disp_v_z
    sigma_v_tot_value[i] = disp_v_tot
    mean_v_phi_value[i]  = mean_v_phi

    v_on_sigma_value[i]  = mean_v_phi / disp_v_tot

    #normed
    n_disp_v_rho = np.std(v_rho/v_c_stars)
    n_disp_v_phi = np.std(v_phi/v_c_stars)
    n_disp_v_z   = np.std(v_z/v_c_stars)
    n_disp_v_tot = np.linalg.norm((n_disp_v_rho, n_disp_v_phi, n_disp_v_z))
    n_mean_v_phi = np.mean(v_phi/v_c_stars)

    normed_sigma_v_R_value[i]   = n_disp_v_rho
    normed_sigma_v_phi_value[i] = n_disp_v_phi
    normed_sigma_v_z_value[i]   = n_disp_v_z
    normed_sigma_v_tot_value[i] = n_disp_v_tot
    normed_mean_v_phi_value[i]  = n_mean_v_phi

    #r25
    #make mask
    rMask25 = (rho < r_25)

    n_25 = np.sum(rMask25)
    #add values to arrays
    mean_elipticity_value_25[i] = np.mean(j_zonc_stars[rMask25])
    thin_disk_fraction_25[i]    = np.sum(j_zonc_stars[rMask25]>thin_disk_threshold)/n_25
    bulge_fraction_25[i]        = 2 * np.sum(j_zonc_stars[rMask25] < 0) / n_25

    #calculate kappas
    K_tot = np.sum(np.square(np.linalg.norm(VelStars[rMask25],axis=1)))
    K_rot = np.sum(np.square(v_phi[rMask25]))
    K_co  = np.sum(np.square(v_phi[rMask25][v_phi[rMask25] > 0]))

    kappa_value_25[i]    = K_rot / K_tot
    kappa_co_value_25[i] = K_co  / K_tot

    #sigmas
    disp_v_rho = np.std(v_rho[rMask25])
    disp_v_phi = np.std(v_phi[rMask25])
    disp_v_z   = np.std(v_z[rMask25])
    disp_v_tot = np.linalg.norm((disp_v_rho, disp_v_phi, disp_v_z))
    mean_v_phi = np.mean(v_phi[rMask25])

    sigma_v_R_value_25[i]   = disp_v_rho
    sigma_v_phi_value_25[i] = disp_v_phi
    sigma_v_z_value_25[i]   = disp_v_z
    sigma_v_tot_value_25[i] = disp_v_tot
    mean_v_phi_value_25[i]  = mean_v_phi

    v_on_sigma_value_25[i]  = mean_v_phi / disp_v_tot

    #normed
    n_disp_v_rho = np.std(v_rho[rMask25]/v_c_stars[rMask25])
    n_disp_v_phi = np.std(v_phi[rMask25]/v_c_stars[rMask25])
    n_disp_v_z   = np.std(v_z[rMask25]/v_c_stars[rMask25])
    n_disp_v_tot = np.linalg.norm((n_disp_v_rho, n_disp_v_phi, n_disp_v_z))
    n_mean_v_phi = np.mean(v_phi[rMask25]/v_c_stars[rMask25])

    normed_sigma_v_R_value_25[i]   = n_disp_v_rho
    normed_sigma_v_phi_value_25[i] = n_disp_v_phi
    normed_sigma_v_z_value_25[i]   = n_disp_v_z
    normed_sigma_v_tot_value_25[i] = n_disp_v_tot
    normed_mean_v_phi_value_25[i]  = n_mean_v_phi

    #r50
    #make mask
    rMask50 = (rho < r_50)

    n_50 = np.sum(rMask50)
    #add values to arrays
    mean_elipticity_value_50[i] = np.mean(j_zonc_stars[rMask50])
    thin_disk_fraction_50[i]    = np.sum(j_zonc_stars[rMask50]>thin_disk_threshold)/n_50
    bulge_fraction_50[i]        = 2 * np.sum(j_zonc_stars[rMask50] < 0) / n_50

    #calculate kappas
    K_tot = np.sum(np.square(np.linalg.norm(VelStars[rMask50],axis=1)))
    K_rot = np.sum(np.square(v_phi[rMask50]))
    K_co  = np.sum(np.square(v_phi[rMask50][v_phi[rMask50] > 0]))

    kappa_value_50[i]    = K_rot / K_tot
    kappa_co_value_50[i] = K_co  / K_tot

    #sigmas
    disp_v_rho = np.std(v_rho[rMask50])
    disp_v_phi = np.std(v_phi[rMask50])
    disp_v_z   = np.std(v_z[rMask50])
    disp_v_tot = np.linalg.norm((disp_v_rho, disp_v_phi, disp_v_z))
    mean_v_phi = np.mean(v_phi[rMask50])

    sigma_v_R_value_50[i]   = disp_v_rho
    sigma_v_phi_value_50[i] = disp_v_phi
    sigma_v_z_value_50[i]   = disp_v_z
    sigma_v_tot_value_50[i] = disp_v_tot
    mean_v_phi_value_50[i]  = mean_v_phi

    v_on_sigma_value_50[i]  = mean_v_phi / disp_v_tot

    #normed
    n_disp_v_rho = np.std(v_rho[rMask50]/v_c_stars[rMask50])
    n_disp_v_phi = np.std(v_phi[rMask50]/v_c_stars[rMask50])
    n_disp_v_z   = np.std(v_z[rMask50]/v_c_stars[rMask50])
    n_disp_v_tot = np.linalg.norm((n_disp_v_rho, n_disp_v_phi, n_disp_v_z))
    n_mean_v_phi = np.mean(v_phi[rMask50]/v_c_stars[rMask50])

    normed_sigma_v_R_value_50[i]   = n_disp_v_rho
    normed_sigma_v_phi_value_50[i] = n_disp_v_phi
    normed_sigma_v_z_value_50[i]   = n_disp_v_z
    normed_sigma_v_tot_value_50[i] = n_disp_v_tot
    normed_mean_v_phi_value_50[i]  = n_mean_v_phi

    #r75
    #make mask
    rMask75 = (rho < r_75)

    n_75 = np.sum(rMask75)
    #add values to arrays
    mean_elipticity_value_75[i] = np.mean(j_zonc_stars[rMask75])
    thin_disk_fraction_75[i]    = np.sum(j_zonc_stars[rMask75]>thin_disk_threshold)/n_75
    bulge_fraction_75[i]        = 2 * np.sum(j_zonc_stars[rMask75] < 0) / n_75

    #calculate kappas
    K_tot = np.sum(np.square(np.linalg.norm(VelStars[rMask75],axis=1)))
    K_rot = np.sum(np.square(v_phi[rMask75]))
    K_co  = np.sum(np.square(v_phi[rMask75][v_phi[rMask75] > 0]))

    kappa_value_75[i]    = K_rot / K_tot
    kappa_co_value_75[i] = K_co  / K_tot

    #sigmas
    disp_v_rho = np.std(v_rho[rMask75])
    disp_v_phi = np.std(v_phi[rMask75])
    disp_v_z   = np.std(v_z[rMask75])
    disp_v_tot = np.linalg.norm((disp_v_rho, disp_v_phi, disp_v_z))
    mean_v_phi = np.mean(v_phi[rMask75])

    sigma_v_R_value_75[i]   = disp_v_rho
    sigma_v_phi_value_75[i] = disp_v_phi
    sigma_v_z_value_75[i]   = disp_v_z
    sigma_v_tot_value_75[i] = disp_v_tot
    mean_v_phi_value_75[i]  = mean_v_phi

    v_on_sigma_value_75[i]  = mean_v_phi / disp_v_tot

    #normed
    n_disp_v_rho = np.std(v_rho[rMask75]/v_c_stars[rMask75])
    n_disp_v_phi = np.std(v_phi[rMask75]/v_c_stars[rMask75])
    n_disp_v_z   = np.std(v_z[rMask75]/v_c_stars[rMask75])
    n_disp_v_tot = np.linalg.norm((n_disp_v_rho, n_disp_v_phi, n_disp_v_z))
    n_mean_v_phi = np.mean(v_phi[rMask75]/v_c_stars[rMask75])

    normed_sigma_v_R_value_75[i]   = n_disp_v_rho
    normed_sigma_v_phi_value_75[i] = n_disp_v_phi
    normed_sigma_v_z_value_75[i]   = n_disp_v_z
    normed_sigma_v_tot_value_75[i] = n_disp_v_tot
    normed_mean_v_phi_value_75[i]  = n_mean_v_phi

  t = np.linspace(0, t_tot, nsnaps)

  data = np.vstack((t, #mean_elipticity_value,
                    thin_disk_fraction, bulge_fraction,
                    kappa_value, kappa_co_value, v_on_sigma_value,
                    sigma_v_R_value, sigma_v_phi_value, sigma_v_z_value,
                    sigma_v_tot_value, mean_v_phi_value)).T

  data_25=np.vstack((t, #mean_elipticity_value_25,
                     thin_disk_fraction_25,bulge_fraction_25,
                     kappa_value_25, kappa_co_value_25, v_on_sigma_value_25,
                     sigma_v_R_value_25, sigma_v_phi_value_25, sigma_v_z_value_25,
                     sigma_v_tot_value_25, mean_v_phi_value_25)).T

  data_50=np.vstack((t, #mean_elipticity_value_50,
                     thin_disk_fraction_50,bulge_fraction_50,
                     kappa_value_50, kappa_co_value_50, v_on_sigma_value_50,
                     sigma_v_R_value_50, sigma_v_phi_value_50, sigma_v_z_value_50,
                     sigma_v_tot_value_50, mean_v_phi_value_50)).T

  data_75=np.vstack((t, #mean_elipticity_value_75,
                     thin_disk_fraction_75,bulge_fraction_75,
                     kappa_value_75, kappa_co_value_75, v_on_sigma_value_75,
                     sigma_v_R_value_75, sigma_v_phi_value_75, sigma_v_z_value_75,
                     sigma_v_tot_value_75, mean_v_phi_value_75)).T

  #manual corner plot
  big_data = (data_25,   data_50,   data_75, data)
  # cmaps = ('Oranges_r', 'Greens_r', 'Purples_r', 'Greys_r')
  cs = ('C0', 'C1', 'C2', 'k')

  labels=[r'$t$ [Gyr]', #r'$\bar{\epsilon}$',
          r'$F(\epsilon > 0.7)$', r'Spheroid Fraction',
          r'$\kappa_{rot}$', r'$\kappa_{co}$', r'$\overline{v_\phi}/\sigma_{tot}$',
          r'$\sigma_{v_R}$',r'$\sigma_{v_\phi}$',r'$\sigma_{v_z}$',
          r'$\sigma_{tot}$', r'$\overline{v_\phi}$']
  lims = [(0, t_tot), #(0,0.999),
          (0,0.999), (0,0.999), (0,0.999), (0,0.999), (0,2.999),
          (0,199), (0,199), (0,199), (0,299), (0,299)]
  n = np.shape(data)[1]

  fig, axs = plt.subplots(nrows=n-1, ncols=n-1, sharex=False, sharey=False,
                          figsize=(25,25))
  fig.subplots_adjust(hspace=0.03,wspace=0.03)

  for data_i in range(len(big_data)):
    for axis_y in range(n):
      for axis_x in range(axis_y):
        if data_i > 3:
          axs[axis_y-1, axis_x].errorbar(
            big_data[data_i].T[axis_x], big_data[data_i].T[axis_y],
            ls=':', fmt='', c=cs[data_i], linewidth=3)
        else:
          axs[axis_y-1, axis_x].scatter(
            big_data[data_i].T[axis_x], big_data[data_i].T[axis_y],
            s=5, c=cs[data_i], alpha=0.1)

        axs[axis_y-1, axis_x].set_xlim(lims[axis_x])
        axs[axis_y-1, axis_x].set_ylim(lims[axis_y])
        if axis_x != 0:
          axs[axis_y-1, axis_x].set_yticklabels([])
        if axis_y != n-1:
          axs[axis_y-1, axis_x].set_xticklabels([])

  for axis_y in range(n-1):
    axs[axis_y, 0].set_ylabel(labels[axis_y+1])
  for axis_x in range(n-1):
    axs[n-2, axis_x].set_xlabel(labels[axis_x])

  for axis_y in range(n-1):
    for axis_x in range(n-1):
      if n - axis_y + axis_x > n:
        axs[axis_y, axis_x].set_visible(False)

  axs[0,0].legend([r'$<r_{25}$ measured',r'$<r_{50}$ measured',r'$<r_{75}$ measured',
                   r'Global'],
                  bbox_to_anchor=(1, 1), loc='upper left')

  plt.show() if save:
    fname = '../galaxies/' + name + '/results/morphological_corner_less'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  data = np.vstack((t, #mean_elipticity_value,
                    thin_disk_fraction, bulge_fraction,
                    kappa_value, kappa_co_value, v_on_sigma_value)).T

  data_25=np.vstack((t, #mean_elipticity_value_25,
                     thin_disk_fraction_25,bulge_fraction_25,
                     kappa_value_25, kappa_co_value_25, v_on_sigma_value_25)).T

  data_50=np.vstack((t, #mean_elipticity_value_50,
                     thin_disk_fraction_50,bulge_fraction_50,
                     kappa_value_50, kappa_co_value_50, v_on_sigma_value_50)).T

  data_75=np.vstack((t, #mean_elipticity_value_75,
                     thin_disk_fraction_75,bulge_fraction_75,
                     kappa_value_75, kappa_co_value_75, v_on_sigma_value_75)).T

  #manual corner plot
  big_data = (data_25, data_50, data_75, data)
  # cmaps = ('Oranges_r', 'Greens_r', 'Purples_r', 'Greys_r')
  cs = ('C0', 'C1', 'C2', 'k')

  labels=[r'$t$ [Gyr]', #r'$\bar{\epsilon}$',
          r'$F(\epsilon > 0.7)$', r'Spheroid Fraction',
          r'$\kappa_{rot}$', r'$\kappa_{co}$', r'$\overline{v_\phi}/\sigma_{tot}$',
          r'$\sigma_{v_R}$',r'$\sigma_{v_\phi}$',r'$\sigma_{v_z}$',
          r'$\sigma_{tot}$', r'$\overline{v_\phi}$']
  lims = [(0, t_tot), #(0,0.999),
          (0,0.999), (0,0.999), (0,0.999), (0,0.999), (0,2.999),
          (0,199), (0,199), (0,199), (0,299), (0,299)]
  n = np.shape(data)[1]

  fig, axs = plt.subplots(nrows=n-1, ncols=n-1, sharex=False, sharey=False,
                          figsize=(13,13))
  fig.subplots_adjust(hspace=0.03,wspace=0.03)

  for data_i in range(len(big_data)):
    for axis_y in range(n):
      for axis_x in range(axis_y):
        # axs[axis_y-1, axis_x].scatter(
        #   big_data[data_i].T[axis_x], big_data[data_i].T[axis_y],
        #   s=2, c=t, cmap=cmaps[data_i], alpha=0.3)
        axs[axis_y-1, axis_x].scatter(
          big_data[data_i].T[axis_x], big_data[data_i].T[axis_y],
          s=5, c=cs[data_i], alpha=0.3)

        axs[axis_y-1, axis_x].set_xlim(lims[axis_x])
        axs[axis_y-1, axis_x].set_ylim(lims[axis_y])
        if axis_x != 0:
          axs[axis_y-1, axis_x].set_yticklabels([])
        if axis_y != n-1:
          axs[axis_y-1, axis_x].set_xticklabels([])

  for axis_y in range(n-1):
    axs[axis_y, 0].set_ylabel(labels[axis_y+1])
  for axis_x in range(n-1):
    axs[n-2, axis_x].set_xlabel(labels[axis_x])

  for axis_y in range(n-1):
    for axis_x in range(n-1):
      if n - axis_y + axis_x > n:
        axs[axis_y, axis_x].set_visible(False)

  axs[0,0].legend([r'$<r_{25}$',r'$<r_{50}$',r'$<r_{75}$',r'Global'],
                  bbox_to_anchor=(1, 1), loc='upper left')

  plt.show() if save:
    fname = '../galaxies/' + name + '/results/morphological_corner_only_less'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  return

def plot_tests(PosDMs, VelDMs):
  '''
  '''
  r = np.linalg.norm(PosDMs, axis=1)

  r_here = 7

  mask = np.logical_and(r < r_here + 2 , r > r_here - 2)

  PosDMs = PosDMs[mask]
  VelDMs = VelDMs[mask]

  sigma_x = np.std(VelDMs[:,0])
  sigma_y = np.std(VelDMs[:,1])
  sigma_z = np.std(VelDMs[:,2])
  sigma_tot = np.linalg.norm((sigma_x, sigma_y, sigma_z))

  _, _, _, v_R, v_phi, v_z = halo.get_cylindrical(PosDMs, VelDMs)

  sigma_R   = np.std(v_R)
  sigma_phi = np.std(v_phi)
  sigma_z   = np.std(v_z)
  sigma_tot2 = np.linalg.norm((sigma_R, sigma_phi, sigma_z))

  vs = np.linalg.norm(VelDMs, axis=1)
  sigma_s = np.std(vs)

  mean = np.mean(vs)

  plt.figure()
  out = plt.hist(vs, bins=20, density=True, cumulative=False)

  plt.vlines(mean - sigma_s, 0, 0.003)
  plt.vlines(mean + sigma_s, 0, 0.003)

  m  = 1
  kb = 1
  # T  = m / kb * sigma_s**2 / (3 - 8/np.pi)
  T  = m / kb * sigma_tot**2 / 3

  v_lin = np.linspace(1,600)
  maxwell_dist = (m / (2 * np.pi * kb * T))**(3/2) * 4 * np.pi * v_lin**2 * np.exp(
    -m * v_lin**2 / (2 * kb * T))

  plt.plot(v_lin, maxwell_dist)

  plt.figure()
  out = plt.hist(vs, bins=20, density=True, cumulative=True)

  maxwell_cumulative = erf(v_lin * np.sqrt(m / (2 * kb * T))) - np.sqrt(
    2 * m / (np.pi * kb * T)) * v_lin * np.exp(-m * v_lin**2 / (2 * kb * T))

  plt.plot(v_lin, maxwell_cumulative)

  n = 10000
  rands = np.random.rand(n)
  maxwell_cum = lambda v, r: erf(v * np.sqrt(m / (2 * kb * T))) - np.sqrt(
    2 * m / (np.pi * kb * T)) * v * np.exp(-m * v**2 / (2 * kb * T)) - r
  v = [brentq(maxwell_cum, 0, 800, args=r) for r in rands]

  plt.hist(v, bins=20, density=True, cumulative=True, alpha=0.2)

  plt.figure()
  out = plt.hist(v_R,   bins=20, density=True, alpha=0.5)
  out = plt.hist(v_phi, bins=20, density=True, alpha=0.5)
  out = plt.hist(v_z,   bins=20, density=True, alpha=0.5)

  plt.vlines(-sigma_R, 0, 0.003)
  plt.vlines( sigma_R, 0, 0.003)

  # Tx = m / kb * sigma_s**2 / (3 - 8/np.pi)
  Tx  = m / kb * sigma_tot**2 / 3

  vx_lin = np.linspace(-600,600)
  maxwell_distx = (m / (2 * np.pi * kb * Tx))**(1/2) * np.exp(
    -m * vx_lin**2 / (2 * kb * Tx))

  plt.plot(vx_lin, maxwell_distx)

  return

def plot_tests2(PosStars, VelStars, j_z_stars, j_c_stars, energy_stars,
                E_interp, j_circ_interp,
                PosDMs, VelDMs):
  '''
  '''
  r = np.linalg.norm(PosStars, axis=1)

  r_half = 7

  mask = np.logical_and(r < r_half + 2 , r > r_half - 2)

  #cartesian
  PosStars = PosStars[mask]
  VelStars = VelStars[mask]
  j_z_stars = j_z_stars[mask]
  j_c_stars = j_c_stars[mask]
  energy_stars = energy_stars[mask]

  #get potential energy of stars
  PotStars = energy_stars - 0.5 * np.linalg.norm(VelStars, axis=1)**2

  #average potential energy
  pot_r_half = np.mean(PotStars)

  sigma_x = np.std(VelStars[:,0])
  sigma_y = np.std(VelStars[:,1])
  sigma_z = np.std(VelStars[:,2])
  sigma_tot2 = np.linalg.norm((sigma_x, sigma_y, sigma_z))

  #cylindrical
  _, _, _, v_R, v_phi, v_z = halo.get_cylindrical(PosStars, VelStars)

  sigma_R   = np.std(v_R)
  sigma_phi = np.std(v_phi)
  sigma_z   = np.std(v_z)
  sigma_tot = np.linalg.norm((sigma_R, sigma_phi, sigma_z))

  #speeds
  vs = np.linalg.norm(VelStars, axis=1)
  sigma_s = np.std(vs)

  print(sigma_x)
  print(sigma_y)
  print(sigma_z)
  print(sigma_tot2)

  print(sigma_R)
  print(sigma_phi)
  print(sigma_z)
  print(sigma_tot)

  print(sigma_s) # should be sigma_x * np.sqrt( 1/(3 - 8*np.pi) )

  mean = np.mean(vs)

  #approximate with Maxwell-Boltzmann distribution
  m  = 1
  kb = 1
  T  = m / kb * sigma_tot**2 / 3

  #speed PDF
  v_lin = np.linspace(1,600)
  maxwell_dist = (m / (2 * np.pi * kb * T))**(3/2) * 4 * np.pi * v_lin**2 * np.exp(
    -m * v_lin**2 / (2 * kb * T))

  #speed CDF
  maxwell_cumulative = erf(v_lin * np.sqrt(m / (2 * kb * T))) - np.sqrt(
    2 * m / (np.pi * kb * T)) * v_lin * np.exp(-m * v_lin**2 / (2 * kb * T))

  #1d velocities
  vx_lin = np.linspace(-600,600)
  maxwell_distx = (m / (2 * np.pi * kb * T))**(1/2) * np.exp(
    -m * vx_lin**2 / (2 * kb * T))

  #monte-carlo the distribution
  n=100000
  #random positions
  x = 2*np.random.rand(n, 3) - 1
  x = x[np.linalg.norm(x, axis=1) < 1]
  x = (x.T / np.linalg.norm(x, axis=1)).T
  #randomly orientated particles at r_half
  x *= r_half

  #randomly orientated velocities
  v = 2*np.random.rand(n, 3) - 1
  v = v[np.linalg.norm(v, axis=1) < 1]
  v = (v.T / np.linalg.norm(v, axis=1)).T

  #fix lengths
  if len(v) > len(x):
    v = v[:len(x)]
  else:
    x = x[:len(v)]

  n = len(v)
  rands = np.random.rand(n)
  maxwell_cum = lambda v, r: erf(v * np.sqrt(m / (2 * kb * T))) - np.sqrt(
    2 * m / (np.pi * kb * T)) * v * np.exp(-m * v**2 / (2 * kb * T)) - r
  v_abs = np.array([brentq(maxwell_cum, 0, 1000, args=r) for r in rands])

  #maxwell-boltzmann distributed velocities
  v = (v.T * v_abs).T

  j = np.cross(x, v)

  #jzonc randoms
  energy_randoms = pot_r_half + 0.5 * np.linalg.norm(v, axis=1)**2

  interp = InterpolatedUnivariateSpline(E_interp, j_circ_interp,
                                        k=1, ext='const')
  j_c_randoms = interp(energy_randoms)

  #DMs
  r_DMs = np.linalg.norm(PosDMs, axis=1)

  mask_DMs = np.logical_and(r_DMs < r_half + 1 , r_DMs > r_half - 1)

  #cartesian
  PosDMs = PosDMs[mask_DMs]
  VelDMs = VelDMs[mask_DMs]

  j_DMs = np.cross(PosDMs, VelDMs)

  v_DMs = np.linalg.norm(VelDMs, axis=1)

  energy_DMs = pot_r_half + 0.5 * v_DMs**2

  j_c_DMs = interp(energy_DMs)

  #plot PDF of speeds
  plt.figure()
  #PDF
  plt.hist(vs,    bins=20, density=True, alpha=0.5, label='Stars')
  plt.hist(v_DMs, bins=20, density=True, alpha=0.5, label='DM')

  #std
  plt.vlines(mean - sigma_s, 0, 0.003)
  plt.vlines(mean + sigma_s, 0, 0.003)

  #theory
  plt.plot(v_lin, maxwell_dist, label='Theory')

  plt.xlabel(r'$|v|$')
  plt.ylabel(r'Probability')
  plt.legend()

  #plot CDF of speeds
  plt.figure()
  #CDF
  plt.hist(vs, bins=20, density=True, cumulative=True, label='Stars')

  #theory
  plt.plot(v_lin, maxwell_cumulative, label='Theory')

  #generated
  plt.hist(v_abs, bins=20, density=True, cumulative=True, alpha=0.2,
           label='Generated')

  plt.xlabel(r'$|v|$')
  plt.ylabel(r'Cumulative Fraction')
  plt.legend()

  #plot 1D distributions
  plt.figure()
  #PDF
  plt.hist(v_R,   bins=20, density=True, alpha=0.5, label=r'$v_R$')
  plt.hist(v_phi, bins=20, density=True, alpha=0.5, label=r'$v_\phi$')
  plt.hist(v_z,   bins=20, density=True, alpha=0.5, label=r'$v_z$')

  #std
  plt.vlines(-sigma_R, 0, 0.003)
  plt.vlines( sigma_R, 0, 0.003)

  #theory
  plt.plot(vx_lin, maxwell_distx, label='Theory')

  plt.xlabel(r'$v_X$')
  plt.ylabel(r'Probability')
  plt.legend()

  #jz
  plt.figure()

  plt.hist(j_z_stars,  density=True, alpha=0.5, label='Stars')
  plt.hist(j_DMs[:,2], density=True, alpha=0.5, label='Dark Matter')
  plt.hist(j[:,2],     density=True, alpha=0.5, label='Monte-Carlo')

  plt.xlabel(r'$j_z$')
  plt.ylabel(r'Probability')
  plt.legend()

  #jzonc
  plt.figure()

  plt.hist(j_z_stars / j_c_stars, bins=np.linspace(-1.5,1.5, 31),
           density=True, alpha=0.5, label='Stars')
  plt.hist(j_DMs[:,2] / j_c_DMs, bins=np.linspace(-1.5,1.5, 31),
           density=True, alpha=0.5, label='Dark Matter')
  plt.hist(j[:,2] / j_c_randoms, bins=np.linspace(-1.5,1.5, 31),
           density=True, alpha=0.5, label='Monte-Carlo')

  plt.xlabel(r'$j_z / j_c(E)$')
  plt.ylabel(r'Probability')
  plt.legend()

  return

def plot_tests3(PosStars, VelStars, j_z_stars, j_c_stars,
                PosDMs, VelDMs):
  '''
  '''
  r = np.linalg.norm(PosStars, axis=1)

  r_half = 7

  #stars
  mask = np.logical_and(r < r_half + 2 , r > r_half - 2)

  #cartesian
  PosStars = PosStars[mask]
  VelStars = VelStars[mask]
  r        = r[mask]
  v        = np.linalg.norm(VelStars, axis=1)

  dot_product = (PosStars[:,0] * VelStars[:,0] + PosStars[:,1] * VelStars[:,1] +
                 PosStars[:,2] * VelStars[:,2]) / (r * v)

  # _, _, _, v_R, v_phi, v_z = get_cylindrical(PosStars, VelStars)

  #DM
  r_DMs = np.linalg.norm(PosDMs, axis=1)

  mask_DMs = np.logical_and(r_DMs < r_half + 1 , r_DMs > r_half - 1)

  PosDMs = PosDMs[mask_DMs]
  VelDMs = VelDMs[mask_DMs]
  r_DMs  = r_DMs[mask_DMs]
  v_DMs  = np.linalg.norm(VelDMs, axis=1)

  dot_product_DMs = (PosDMs[:,0] * VelDMs[:,0] + PosDMs[:,1] * VelDMs[:,1] +
                     PosDMs[:,2] * VelDMs[:,2]) / (r_DMs * v_DMs)

  plt.figure()
  plt.hist(dot_product)

  plt.figure()
  plt.hist(dot_product_DMs)

  return

if __name__ == '__main__':

  # save = False

  name = 'mu_25/fdisk_0p01_lgMdm_6p5_V200-200kmps'
  # snap = '399'
  # (file_name, MassDM,   PosDMs,   VelDMs,   IDDMs,
  #             MassStar, PosStars, VelStars, IDStars,
  #   j_z_stars, j_p_stars, j_c_stars, energy_stars,
  #   E_interp, j_circ_interp, R_interp
  #   ) = load_nbody.load_and_calculate(name, snap)

  # plot_tests(PosDMs, VelDMs)
  # plot_tests2(PosStars, VelStars, j_z_stars, j_c_stars, energy_stars,
  #             E_interp, j_circ_interp, PosDMs, VelDMs)
  # plot_tests3(PosStars, VelStars, j_z_stars, j_c_stars,
  #             PosDMs, VelDMs)

  # plot_morpholocial_profile(name, save=False)

  plot_morpholocial_corner_less_than(name, save=True)
