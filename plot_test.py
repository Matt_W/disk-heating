#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 27 15:00:23 2021

@author: matt
"""

import numpy as np

import matplotlib.pyplot as plt


# times = np.logspace(-3, 1)

# c = 5

# gamma = 0.3

# sigma_o2_on_sigma_dm2 = 0.15**2

# t_c = 2

# tau_heat = c**(-1/(1 + gamma))

# taus = times / t_c

# linear = c * (taus)**(1 + gamma) + sigma_o2_on_sigma_dm2
# asymptote = (1 - np.exp(- (taus / tau_heat)**(1 + gamma))) + sigma_o2_on_sigma_dm2

# plt.figure()
# plt.plot(taus, linear - sigma_o2_on_sigma_dm2, c='C0')
# plt.plot(taus, asymptote - sigma_o2_on_sigma_dm2, c='C0', ls='-.')
# plt.plot(taus, np.ones(len(times)), c='k', ls=':')
# plt.loglog()
# plt.xlabel(r'$\tau$')
# plt.ylabel(r'$\Delta \sigma_i^2 / \sigma_{DM}^2$')

# plt.figure()
# plt.plot(taus, linear, c='C0')
# plt.plot(taus, asymptote, c='C0', ls='--')
# plt.plot(taus, np.ones(len(times)), c='k', ls=':')
# plt.loglog()
# plt.xlabel(r'$\tau$')
# plt.ylabel(r'$\sigma_i^2 / \sigma_{DM}^2$')



# import numpy as np
# from scipy.optimize import minimize

# def get_data():
#   return np.random.rand(2,100)

# def y_model(x, a, m, c):
#   return a*x**2 + m*x + c

# def log_least_squares(data, a, m, c):
#   return np.log10(np.sum( ( data[1] - y_model(data[0], a, m, c) )**2 ))

# def log_like(args, data, fixed_args):

#   params = np.zeros(len(fixed_args))
#   i = 0

#   for j in range(len(fixed_args)):
#     if fixed_args[j] == None:
#       params[j] = args[i]
#       i += 1
#     else:
#       params[j] = fixed_args[j]

#   return log_least_squares(data, *params)

# def fit(data, fixed_a=None, fixed_m=None, fixed_c=None):

#   fixed_args = []
#   start_loc = []

#   if fixed_a == None: start_loc.append(0)
#   fixed_args.append(fixed_a)

#   if fixed_m == None: start_loc.append(0)
#   fixed_args.append(fixed_m)

#   if fixed_c == None: start_loc.append(0)
#   fixed_args.append(fixed_c)

#   min_result = minimize(log_like, start_loc, args=(data, fixed_args))

#   params = np.zeros(len(fixed_args))
#   i = 0
#   for j in range(len(fixed_args)):
#     if fixed_args[j] == None:
#       params[j] = min_result.x[i]
#       i += 1
#     else:
#       params[j] = fixed_args[j]

#   return(params)

# if __name__ == '__main__':
#   data = get_data()

#   square = fit(data)
#   lin    = fit(data, fixed_a=0)
#   const  = fit(data, fixed_a=0, fixed_m=0)

header = ['this', 'is', 'the', 'header', 'a', 'b', 'c']
data = [[np.random.rand() for _ in range(np.random.randint(4,8))] for _ in range(10)]
data[0] = header

with open('out.csv', 'w') as file:
  for line in data:
    for element in line:
      file.write(str(element))
      file.write(', ')
    file.write('\n')


#   print(square)
#   print(lin)
#   print(const)

#   plt.plot(data[0], data[1], marker='.', linestyle='')

#   x = np.linspace(0, 1)
#   plt.plot(x, y_model(x, *square))
#   plt.plot(x, y_model(x, *lin))
#   plt.plot(x, y_model(x, *const))