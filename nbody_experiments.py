#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 26 Apr 2022

@author: matt
"""

from matplotlib import rcParams

rcParams['ps.useafm'] = True
rcParams['pdf.use14corefonts'] = False
rcParams['text.usetex'] = False
rcParams['font.sans-serif'] = ['cmr10', 'Times-Roman']
rcParams['font.weight'] = 'normal'
rcParams['font.size'] = 20
rcParams['xtick.labelsize'] = 20
rcParams['ytick.labelsize'] = 20
rcParams['axes.labelsize'] = 22
rcParams['axes.linewidth'] = 1
rcParams['axes.unicode_minus'] = False
# rcParams['axes.grid'] = True
rcParams['axes.grid'] = False
rcParams['xtick.minor.width'] = 1
rcParams['ytick.minor.width'] = 1
rcParams['xtick.major.width'] = 1
rcParams['ytick.major.width'] = 1
rcParams['xtick.minor.size'] = 4
rcParams['ytick.minor.size'] = 4
rcParams['xtick.major.size'] = 5
rcParams['ytick.major.size'] = 5
rcParams['xtick.direction'] = 'in'
rcParams['ytick.direction'] = 'in'
rcParams['xtick.top'] = True
rcParams['ytick.right'] = True
rcParams['mathtext.fontset'] = 'cm'

import numpy as np

import matplotlib.pyplot as plt
import matplotlib

import multiprocessing

# Create a function to apply boundary conditions
def apply_boundary(position, grid_size=1, periodic=True):
  '''periodic or not
  '''
  if not periodic:
    return position

  #too high
#  while (len(position[position > grid_size]) > 0):
  position[position > grid_size] = position[position > grid_size] - 2*grid_size

  #too low
#  while (len(position[position < -grid_size]) > 0):
  position[position < -grid_size] = position[position < -grid_size] + 2*grid_size

  return position


def distances(position, grid_size=1, periodic=True):
    '''Make an array of distances to each point
    '''
    # find N x N x Nd matrix of particle separations
    grid_r = position[:, None, :] - position[:, :, None]

    min_r = grid_r

    if periodic:
      plus_r = grid_r + 2*grid_size
      minus_r = grid_r - 2*grid_size
      min_r = np.min((np.abs(grid_r), np.abs(plus_r), np.abs(minus_r)), axis=0)

    return np.power(np.sum(min_r * min_r, axis=0), 0.5)


def energy_0(mass, position, velocity, grid_size=1, soft_length=1e-3, periodic=True):

    # # find N x N x Nd matrix of particle separations
    # grid_r = position[:, np.newaxis, :] - position[:, :, np.newaxis]

    # find N x Nd matrix of particle separations
    grid_r = position[:, 0, None] - position

    #vector of minimum distances
    min_r = grid_r

    if periodic:
        plus_r = grid_r + 2*grid_size
        minus_r= grid_r - 2*grid_size
        min_r = np.min((np.abs(grid_r), np.abs(plus_r), np.abs(minus_r)), axis=0)

        grid_r[np.abs(grid_r) != min_r] = 0
        plus_r[np.abs(plus_r) != min_r] = 0
        minus_r[np.abs(minus_r) != min_r] = 0

        min_r = np.sign(grid_r + plus_r + minus_r) * min_r

    # N x N particle seps
    # N
    r = np.linalg.norm(min_r, axis=0)

    # potential_0 = -np.sum(mass[:, np.newaxis] / np.sqrt(r**2 + soft_length**2))
    # kinetic_0 = np.sum(0.5 * mass * np.linalg.norm(velocity, axis=0)**2)

    potential_0 = -np.sum(mass / np.sqrt(r**2 + soft_length**2))
    kinetic_0 = 0.5 * mass[0] * np.linalg.norm(velocity[:, 0])**2

    energy = potential_0 + kinetic_0

    # print(kinetic_0, potential_0, energy)

    return energy


def force(position, mass, grid_size=1, soft_length=1e-3, periodic=True):
    # # find N x N x Nd matrix of particle separations
    # grid_r = position[:, np.newaxis, :] - position[:, :, np.newaxis]
    #
    # # vector of minimum distances
    # min_r = grid_r
    #
    # if periodic:
    #     plus_r = grid_r + 2 * grid_size
    #     minus_r = grid_r - 2 * grid_size
    #     min_r = np.min((np.abs(grid_r), np.abs(plus_r), np.abs(minus_r)), axis=0)
    #
    #     grid_r[np.abs(grid_r) != min_r] = 0
    #     plus_r[np.abs(plus_r) != min_r] = 0
    #     minus_r[np.abs(minus_r) != min_r] = 0
    #
    #     min_r = np.sign(grid_r + plus_r + minus_r) * min_r
    #
    # # N x N particle seps
    # r = np.linalg.norm(min_r, axis=0)
    #
    # # calculate force between each pair of particles. includes softening length
    # # not the usual definition of softening length
    # # force_i = mass[None, :] * mass[:, None] * min_r / np.power(np.sum(min_r * min_r, axis=0) + soft_length, 1.5)
    # force_i = np.sum(mass[np.newaxis, :] * mass[:, np.newaxis] * min_r / np.power(r**2 + soft_length**2, -3/2), axis=2)
    #
    # out = force_i

    # find N x Nd matrix of particle separations
    grid_r = position[:, 0, None] - position

    #vector of minimum distances
    min_r = grid_r

    if periodic:
        plus_r = grid_r + 2*grid_size
        minus_r= grid_r - 2*grid_size
        min_r = np.min((np.abs(grid_r), np.abs(plus_r), np.abs(minus_r)), axis=0)

        grid_r[np.abs(grid_r) != min_r] = 0
        plus_r[np.abs(plus_r) != min_r] = 0
        minus_r[np.abs(minus_r) != min_r] = 0

        min_r = np.sign(grid_r + plus_r + minus_r) * min_r

    r = np.linalg.norm(min_r, axis=0)

    #calculate force
    # force_i = np.nansum(mass[0] * mass * min_r / (r + soft_length)**3, axis=1)
    #plumber softening baked in
    # force_i = - np.nansum(mass[0] * mass * min_r * np.power(r**2 + soft_length**2, -3/2), axis=1)
    force_i = - np.sum(mass[0] * mass * min_r * np.power(r**2 + soft_length**2, -3/2), axis=1)

    out = np.zeros((np.shape(grid_r)))
    out[:, 0] = force_i

    return out


def update(mass, position, velocity, acceleration, dt=1e-1, grid_size=1, soft_length=1e-3):

    #kick-drift leap frog
    velocity += acceleration * dt / 2

    position += velocity * dt
    position = apply_boundary(position, grid_size=grid_size) # Apply boundary conditions

    acceleration = force(position, mass, grid_size=grid_size, soft_length=soft_length) / mass

    velocity += acceleration * dt / 2

    # # #4th Yoshida
    # thirdroottwo = np.power(2, 1/3)
    #
    # w_0 = - thirdroottwo / (2 - thirdroottwo)
    # w_1 = 1 / (2 - thirdroottwo)
    # c_1 = c_4 = w_1 / 2
    # c_2 = c_3 = (w_0 + w_1) / 2
    # d_1 = d_3 = w_1
    # d_2 = w_0
    #
    # x_1 = position + c_1 * velocity * dt
    # x_1 = apply_boundary(x_1, grid_size=grid_size)  # Apply boundary conditions
    # v_1 = velocity + d_1 * force(x_1, mass, grid_size=grid_size, soft_length=soft_length) / mass * dt
    # x_2 = x_1 + c_2 * v_1 * dt
    # x_2 = apply_boundary(x_2, grid_size=grid_size)  # Apply boundary conditions
    # v_2 = v_1 + d_2 * force(x_2, mass, grid_size=grid_size, soft_length=soft_length) / mass * dt
    # x_3 = x_2 + c_3 * v_2 * dt
    # x_3 = apply_boundary(x_3, grid_size=grid_size)  # Apply boundary conditions
    #
    # acceleration = force(x_3, mass, grid_size=grid_size, soft_length=soft_length) / mass
    # v_3 = v_2 + d_3 * acceleration * dt
    #
    # position = x_3 + c_4 * v_3 * dt
    # position = apply_boundary(position, grid_size=grid_size)  # Apply boundary conditions
    # velocity = v_3

    return mass, position, velocity, acceleration


def iterable_calculation(seed):
    np.random.seed(seed + 100)

    trajs = np.zeros((Nt, Nd))
    vels = np.zeros((Nt, Nd))
    accs = np.zeros((Nt, Nd))
    ener = np.zeros(Nt)

    position = grid_size * (1 - 2 * np.random.random((Nd, Np)))
    # velocity = v_bg * (1 - 2 * np.random.random((Nd, Np)))
    velocity = np.random.normal(0, v_bg * np.sqrt(np.pi/2**Nd), (Nd, Np)) #average speed is v_bg #Maxwellian

    # velocity[:, 0] = 1 - 2 * np.random.random(Nd)
    # velocity[:, 0] = v_part * velocity[:, 0] / np.linalg.norm(velocity[:, 0])

    position[:, 0] = [-grid_size/2] + [0] * (Nd - 1)

    velocity[:, 0] = [v_part] + [0] * (Nd - 1)

    mass = mp * np.ones(Np)

    acceleration = force(position, mass, grid_size=grid_size, soft_length=soft_length) / mass

    # init_trajs[seed, :] = position[:, 0]
    # init_vels[seed, :] = velocity[:, 0]
    # init_accs[seed, :] = acceleration[:, 0]

    for i in range(Nt):
        (mass, position, velocity, acceleration
         ) = update(mass, position, velocity, acceleration,
                    dt=dt, grid_size=grid_size, soft_length=soft_length)

        trajs[i, :] = position[:, 0]
        vels[i, :] = velocity[:, 0]
        accs[i, :] = acceleration[:, 0]
        ener[i] = energy_0(mass, position, velocity, grid_size=grid_size, soft_length=soft_length)

    print(seed)

    return trajs, vels, accs, ener


def main(v_init = 1, rho = 1, pdf=False):
    seeds = 4000 #100

    G = 1 #in units of total_box_mass^-1 side_length^3 / time^2

    # Nd = 3 #number of dimensions
    Nd = 2 #number of dimensions

    # Np = 10 ** Nd #number of particles
    Np = (rho * 10) ** Nd #number of particles

    # total_box_mass = 1
    # mp = total_box_mass / Np #particle mass

    mp = 0.001
    total_box_mass = mp / Np

    grid_size = 1
    L = grid_size

    soft_length = 2e-3 * L #units of side_length

    R = L / Np**(1/3) #1D interparticle distance, units of side_length

    v_c = np.sqrt(G * mp / R)
    # T_c = np.sqrt(R**3 / (G * mp))
    T_c = L ** (3 / 2) * (total_box_mass * G) ** (-1 / 2)

    v_esc = np.sqrt(2 * G * mp / soft_length)

    dt = 1e-3
    # Nt = int(1 * T_c / dt) #needs to be > T_c / dt
    Nt = int(2.2 / v_init / dt) #for all particles to approx reach 1.6 L
    print('Nt = ', Nt)

    v_part = v_init * L
    v_bg = 0 #grid_size / 10 #0#v_part

    print('v_part = ', v_part)

    globals()['Nt'] = Nt
    globals()['Nd'] = Nd
    globals()['Np'] = Np
    globals()['mp'] = mp
    globals()['dt'] = dt
    globals()['v_bg'] = v_bg
    globals()['v_part'] = v_part
    globals()['soft_length'] = soft_length
    globals()['grid_size'] = grid_size

    #do the calculation with 7 cores
    with multiprocessing.Pool(processes=7) as pool:
        out = pool.map(iterable_calculation, range(seeds))#, chunksize=1)

    #results in a useful format
    (test_trajs, test_vels, test_accs, energies
     ) = (np.array([out[i][param] for i in range(len(out))]) for param in range(len(out[0])))

    periodic_threshold = 0.8

    diffs = np.diff(test_trajs, axis=1)
    xs = np.zeros(np.shape(diffs))

    xs[diffs >  periodic_threshold * 2 * grid_size] = -2 * grid_size
    xs[diffs < -periodic_threshold * 2 * grid_size] =  2 * grid_size

    xs = np.cumsum(xs, axis=1)

    test_trajs[:, 1:, :] += xs

    name = '../galaxies/particle-tests/example_scattering'
    ylab = True
    if rho != 1:
        name += str(rho) + '_rho'
        ylab = False
    if v_init != 1:
        name += str(v_init) + '_v'
        ylab = False

    thesis_intro_plot(test_trajs, test_vels, seeds, Nt, Nd, Np, L,
                      ylabels=ylab, name=name + pdf * '.pdf')

    data = np.mean(energies, axis=0)
    expectations_vs_time(data, Nt=Nt, dt=dt, all_trajs=energies, label=r'$E$',
                         name='../galaxies/particle-tests/evolution_energies', save=True)

    # test_trajs = test_trajs - init_trajs[:, None, :]
    x_max = np.amax([np.amax(np.linalg.norm(test_trajs[seed, :, :], axis=1)) for seed in range(seeds)])
    plot_trajectory_projections(test_trajs, seeds=seeds, Nt=Nt, grid_size=x_max,#grid_size,
                                name='../galaxies/particle-tests/proj_x', save=True)
    v_max = np.amax([np.amax(np.linalg.norm(test_vels[seed, :, :], axis=1)) for seed in range(seeds)])
    plot_trajectory_projections(test_vels, seeds=seeds, Nt=Nt, grid_size=v_max,
                                name='../galaxies/particle-tests/proj_v', save=True)
    a_max = np.amax([np.amax(np.linalg.norm(test_accs[seed, :, :], axis=1)) for seed in range(seeds)])
    plot_trajectory_projections(test_accs, seeds=seeds, Nt=Nt, grid_size=a_max,
                                name='../galaxies/particle-tests/proj_a', save=True)
    print('trajs done')

    #velocities
    data = np.mean(test_vels[:, :, 0], axis=0)
    expectations_vs_time(data, Nt=Nt, dt=dt, all_trajs=test_vels[:, :, 0], label=r'$\overline{v}_\parallel$',
                         name='../galaxies/particle-tests/evolution_median_parallel', save=True)
    data = np.mean(test_vels[:, :, 1], axis=0)
    expectations_vs_time(data, Nt=Nt, dt=dt, all_trajs=test_vels[:, :, 1], label=r'$\overline{v}_y$',
                         name='../galaxies/particle-tests/evolution_median_y', save=True)
    if Nd > 2:
        data = np.mean(test_vels[:, :, 2], axis=0)
        expectations_vs_time(data, Nt=Nt, dt=dt, all_trajs=test_vels[:, :, 2], label=r'$\overline{v}_z$',
                             name='../galaxies/particle-tests/evolution_median_z', save=True)

    if Nd > 2:
        perp_vels = np.sqrt(test_vels[:, :, 1]**2 + test_vels[:, :, 2]**2)
    elif Nd == 2:
        perp_vels = test_vels[:, :, 1]
    if Nd >= 2:
        data = np.mean(perp_vels, axis=0)
        expectations_vs_time(data, Nt=Nt, dt=dt, all_trajs=perp_vels, label=r'$\overline{v}_\perp$',
                             name='../galaxies/particle-tests/evolution_median_perp', save=True)
    data = np.mean(np.linalg.norm(test_vels[:, :, :], axis=2), axis=0)
    expectations_vs_time(data, Nt=Nt, dt=dt, all_trajs=np.linalg.norm(test_vels[:, :, :], axis=2), label=r'$\overline{|v|}$',
                         name='../galaxies/particle-tests/evolution_median_abs', save=True)

    data = np.std(test_vels[:, :, 0], axis=0)**2
    expectations_vs_time(data, Nt=Nt, dt=dt, label=r'$\sigma_\parallel^2$',
                         name='../galaxies/particle-tests/evolution_sigma_parallel', save=True)
    data = np.std(test_vels[:, :, 1], axis=0)**2
    expectations_vs_time(data, Nt=Nt, dt=dt, label=r'$\sigma_y^2$',
                         name='../galaxies/particle-tests/evolution_sigma_y', save=True)
    if Nd > 2:
        data = np.std(test_vels[:, :, 2], axis=0)**2
        expectations_vs_time(data, Nt=Nt, dt=dt, label=r'$\sigma_z^2$',
                             name='../galaxies/particle-tests/evolution_sigma_z', save=True)
    if Nd > 2:
        data = np.std(test_vels[:, :, 1], axis=0)**2 + np.std(test_vels[:, :, 2], axis=0)**2
    elif Nd == 2:
        data = np.std(test_vels[:, :, 1], axis=0) ** 2
    if Nd >= 2:
        expectations_vs_time(data, Nt=Nt, dt=dt, label=r'$\sigma_\perp^2$',
                             name='../galaxies/particle-tests/evolution_sigma_perp', save=True)
    data = np.std(np.linalg.norm(test_vels[:, :, :], axis=2), axis=0)**2
    expectations_vs_time(data, Nt=Nt, dt=dt, label=r'$\sigma_{|v|}^2$',
                         name='../galaxies/particle-tests/evolution_sigma_abs', save=True)

    #acceleration
    data = np.mean(test_accs[:, :, 0], axis=0)
    expectations_vs_time(data, Nt=Nt, dt=dt, all_trajs=test_accs[:, :, 0], label=r'$\overline{a}_\parallel$',
                         name='../galaxies/particle-tests/evolution_median_a_parallel', save=True)
    data = np.mean(test_accs[:, :, 1], axis=0)
    expectations_vs_time(data, Nt=Nt, dt=dt, all_trajs=test_accs[:, :, 1], label=r'$\overline{a}_y$',
                         name='../galaxies/particle-tests/evolution_median_a_y', save=True)
    if Nd > 2:
        data = np.mean(test_accs[:, :, 2], axis=0)
        expectations_vs_time(data, Nt=Nt, dt=dt, all_trajs=test_accs[:, :, 2], label=r'$\overline{a}_z$',
                             name='../galaxies/particle-tests/evolution_median_a_z', save=True)

    data = np.std(test_accs[:, :, 0], axis=0)**2
    expectations_vs_time(data, Nt=Nt, dt=dt, label=r'$\sigma_{a,\parallel}^2$',
                         name='../galaxies/particle-tests/evolution_sigma_a_parallel', save=True)
    data = np.std(test_accs[:, :, 1], axis=0)**2
    expectations_vs_time(data, Nt=Nt, dt=dt, label=r'$\sigma_{a,y}^2$',
                         name='../galaxies/particle-tests/evolution_sigma_a_y', save=True)
    if Nd > 2:
        data = np.std(test_accs[:, :, 2], axis=0)**2
        expectations_vs_time(data, Nt=Nt, dt=dt, label=r'$\sigma_{a,z}^2$',
                             name='../galaxies/particle-tests/evolution_sigma_a_z', save=True)
    data = np.mean(np.linalg.norm(test_accs[:, :, :], axis=2), axis=0)
    expectations_vs_time(data, Nt=Nt, dt=dt, all_trajs=np.linalg.norm(test_accs[:, :, :], axis=2), label=r'$\overline{|a|}$',
                         name='../galaxies/particle-tests/evolution_median_abs_a', save=True)

    return


def plot_trajectory_projections(test_trajs, seeds, Nt,
                                grid_size=1, name='', save=False):

    fig = plt.figure(figsize=(10,10)) # Create frame and set size

    ax1 = plt.subplot(111) # For normal 2D projection
    plt.xlim(-grid_size,grid_size)  # Set x-axis limits
    plt.ylim(-grid_size,grid_size)  # Set y-axis limits

    for seed in range(seeds):
        if seeds < 201:
            plt.scatter(test_trajs[seed, :, 0], test_trajs[seed, :, 1],
                        c=np.linspace(0, 1-1e-3, Nt), cmap='turbo', s=10)
        plt.errorbar(test_trajs[seed, :, 0], test_trajs[seed, :, 1])

    ax1.set_xlabel(r'$x$')
    ax1.set_ylabel(r'$y$')
    # plt.show()

    if save:
        plt.savefig(name, bbox_inches='tight')

    return


def expectations_vs_time(data, Nt, dt, all_trajs=None, label='',
                         name='', save=False):

    fig = plt.figure(figsize=(8,5)) # Create frame and set size

    ax1 = plt.subplot(111) # For normal 2D projection

    plt.xlim(0, dt * Nt)  # Set y-axis limits
    plt.ylim(np.amin(data), np.amax(data))  # Set y-axis limits

    if all_trajs is not None:
        plt.ylim(np.amin(all_trajs), np.amax(all_trajs))  # Set y-axis limits

        for seed in range(np.shape(all_trajs)[0]):
            plt.errorbar(np.linspace(0, dt * Nt, Nt), all_trajs[seed, :], lw=1, c='C0', alpha=0.8)

    plt.errorbar(np.linspace(0, dt * Nt, Nt), data, lw=10, c='k', alpha=0.8)

    ax1.set_xlabel(r'$t$')
    ax1.set_ylabel(label)

    if save:
        plt.savefig(name, bbox_inches='tight')

    return


def thesis_intro_plot(test_trajs, test_vels, seeds, Nt, Nd, Np, L,
        ylabels = True, name = '../galaxies/particle-tests/example_scattering'):
    #
    fig = plt.figure(constrained_layout=True)
    fig.subplots_adjust(hspace=0.02, wspace=0.02)
    # fig.set_size_inches(7.2, 7.2, forward=True)
    # ax0 = plt.subplot(1, 1, 1)

    fig.set_size_inches(7.2, 11.3, forward=True)
    ax0 = plt.subplot(3, 1, 1)
    ax1 = plt.subplot(3, 1, 2)
    ax2 = plt.subplot(3, 1, 3)

    xlim = [0, 1.6 * L]
    ax0.set_xlim(xlim)
    ax1.set_xlim(xlim)
    ax2.set_xlim(xlim)
    ax0.set_xticklabels([])
    ax1.set_xticklabels([])
    ax0.set_xticks([0,0.5,1,1.5])
    ax1.set_xticks([0,0.5,1,1.5])
    ax2.set_xticks([0,0.5,1,1.5])
    ax2.set_xticklabels(['0','','1',''])

    ylim = [-0.8*L, 0.8*L]
    ax0.set_ylim(ylim)
    ax1.set_ylim(ylim)
    ax2.set_ylim(ylim)

    if ylabels:
        ax0.set_ylabel(r'$\vec{x}_\perp$')
        ax1.set_ylabel(r'$\vec{x}_\perp$')
        ax2.set_ylabel(r'$\vec{v}_\perp$')
    else:
        ax0.set_ylabel('')
        ax1.set_ylabel('')
        ax2.set_ylabel('')
        ax0.set_yticklabels('')
        ax1.set_yticklabels('')
        ax2.set_yticklabels('')

    ax0.set_xlabel('')
    ax1.set_xlabel('')
    ax2.set_xlabel(r'$\vec{x}_\parallel$')

    ax0.set_aspect('equal')
    ax1.set_aspect('equal')
    ax2.set_aspect('equal')

    ax0.scatter(test_trajs[0, :, 0] + 0.5 * L, test_trajs[0, :, 1],
                c=np.linspace(0, 1-1e-3, Nt), cmap='magma', s=8)
    # plt.errorbar(test_trajs[0, :, 0], test_trajs[0, :, 1])


    np.random.seed(0 + 100)
    position = L * (1 - 2 * np.random.random((Nd, Np)))

    position[:, 0] = np.nan

    ax0.scatter((position[0, :] + 2.5 * L) % (2*L), position[1, :],
                c='k', s=8)

    seed_order = np.argsort(test_trajs[:, -1, 1])

    for seed, order in enumerate(seed_order): #range(seeds):
        c = matplotlib.cm.get_cmap('turbo')((seed + 1)/ (seeds + 2))
        # ax1.scatter(test_trajs[order, :, 0] + 0.5 * L, test_trajs[order, :, 1],
        #             c=c*np.ones(Nt)[:, np.newaxis] , s=8)

        # ax2.scatter(test_trajs[order, :, 0] + 0.5 * L, test_vels[order, :, 1],
        #             c=c*np.ones(Nt)[:, np.newaxis] , s=8, zorder=10)


    # clevel = lambda level: (1.2 * level + 0.2)
    # n_contours = 6
    contours = [0, 0.01, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5]
    #[0, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5]  # np.linspace(0, 0.5, n_contours+1)
    n_contours = len(contours) - 1
    n_x = 201
    x_edges = np.linspace(-0.5 * L, 1.5 * L, n_x)
    x_centres = 0.5 * (x_edges[1:] + x_edges[:-1])

    xy_contours = np.zeros((2 * n_contours - 1, n_x - 1))
    vy_contours = np.zeros((2 * n_contours - 1, n_x - 1))
    xy_binned = np.zeros((seeds, n_x - 1))
    vy_binned = np.zeros((seeds, n_x - 1))

    x_bins = np.digitize(test_trajs[:, :, 0], bins=x_edges)

    for seed in range(seeds):
        for n_i in range(n_x - 1):
            xy_binned[seed, n_i] = np.nanmedian(test_trajs[seed, x_bins[seed, :] == n_i + 1, 1])
            vy_binned[seed, n_i] = np.nanmedian(test_vels[seed, x_bins[seed, :] == n_i + 1, 1])

    xy_contours[n_contours - 1] = np.nanmedian(xy_binned, axis=0)
    vy_contours[n_contours - 1] = np.nanmedian(vy_binned, axis=0)
    for k, level in enumerate(contours[1:-1]):
        xy_contours[k] = np.nanquantile(xy_binned, q=level, axis=0)
        vy_contours[k] = np.nanquantile(vy_binned, q=level, axis=0)
        xy_contours[2 * n_contours - k - 2] = np.nanquantile(xy_binned, q=1 - level, axis=0)
        vy_contours[2 * n_contours - k - 2] = np.nanquantile(vy_binned, q=1 - level, axis=0)

    for k, level in enumerate(contours[1:-1]):
        c1 = matplotlib.cm.get_cmap('turbo')((k + 1e-3)/ (2 * n_contours - 2))
        c2 = matplotlib.cm.get_cmap('turbo')((2 * n_contours - k - 2 - 1e-3)/ (2 * n_contours - 2))

        ax1.fill_between(x_centres + 0.5 * L, xy_contours[k], xy_contours[n_contours-1],
                         color=c1)
        ax1.fill_between(x_centres + 0.5 * L, xy_contours[n_contours-1], xy_contours[2 * n_contours - k - 2],
                         color=c2)
        ax2.fill_between(x_centres + 0.5 * L, vy_contours[k], vy_contours[n_contours-1],
                         color=c1)
        ax2.fill_between(x_centres + 0.5 * L, vy_contours[n_contours-1], vy_contours[2 * n_contours - k - 2],
                         color=c2)

    # plt.savefig('../galaxies/particle-tests/example_scattering.pdf', bbox_inches='tight')
    plt.savefig(name, bbox_inches='tight', pad_inches=0.01)
    plt.close()
    # plt.show()

    return


if __name__ == '__main__':
    # main(pdf=True)
    # main(rho=2, pdf=True)
    main(v_init=2, pdf=True)
    pass